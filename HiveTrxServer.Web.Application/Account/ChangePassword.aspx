<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ChangePassword.aspx.vb" Inherits="Account_ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Cambiar Clave
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>     
    
    <script src="../js/md5-min.js" type="text/javascript"></script>

    <script src="../js/sha1-min.js" type="text/javascript"></script>    
    
	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script>  
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>Cambiar Clave</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Cuenta de Usuario
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este m�dulo le permite cambiar la clave de acceso al sistema:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>             

        <asp:Panel ID="pnlError" runat="server" Visible="False">    
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel> 

	    <div class="titleh">
    	    <h3>Datos de la Cuenta</h3>
        </div>
        <div class="body">
        
            <div class="st-form-line">	
                <span class="st-labeltext">Clave Actual:</span>	
                <asp:TextBox ID="txtCurrentPassword" CssClass="st-forminput" style="width:510px" 
                    runat="server" TabIndex="1" TextMode="Password" MaxLength="20" 
                    ToolTip="Digite la clave actual." onkeydown = "return (event.keyCode!=13);" onpaste="javascript:return false;"></asp:TextBox>
                <div class="clear"></div>
            </div>        
        
            <div class="st-form-line">	
                <span class="st-labeltext">Nueva Clave:</span>	
                <asp:TextBox ID="txtNewPassword" CssClass="st-forminput" style="width:510px" 
                    runat="server" TabIndex="2" TextMode="Password" MaxLength="20" 
                    ToolTip="Digite la nueva clave." onkeydown = "return (event.keyCode!=13);" onpaste="javascript:return false;"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Image ID="imgNewPassword" runat="server" ImageUrl="~/img/icons/16x16/cancel.png" />
                <div class="clear"></div>
            </div>
            <div class="st-form-line">	
                <span class="st-labeltext">Repetir Nueva Clave:</span>	
                <asp:TextBox ID="txtNewPassword2" CssClass="st-forminput" style="width:510px" 
                    runat="server" TabIndex="3" TextMode="Password" MaxLength="20" 
                    ToolTip="Digite otra vez la nueva clave." onkeydown = "return (event.keyCode!=13);" onpaste="javascript:return false;"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Image ID="imgNewPassword2" runat="server" ImageUrl="~/img/icons/16x16/cancel.png" />                    
                <div class="clear"></div>
            </div>   
            
            <div class="button-box">
                <asp:Button ID="btnChange" runat="server" Text="Cambiar" CssClass="button-aqua" TabIndex="3" onclientclick="return validateNewPassword()" />
            </div>
            
        </div>
    </div>
    
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="../Logout.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atr�s"/> Volver a la p�gina anterior</asp:HyperLink>    

    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">
    
    <!-- Validator -->
    <script src="../js/ValidatorUser.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

