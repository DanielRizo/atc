﻿Imports TeleLoader.Users

Partial Class Account_ChangePassword
    Inherits TeleLoader.Web.BasePage

    Public userObj As User

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            txtCurrentPassword.Focus()

            'Validar Parámetro
            If objSessionParams.intPasswordErrorCode > 0 Then
                Select Case objSessionParams.intPasswordErrorCode
                    Case 1
                        lblMsg.Text = "Su clave ha caducado, por favor realice el proceso de cambio. Recuerde que la clave debe contener: <br /><br /><li>10 Caracteres Mínimo.</li><li>Números, Letras Letras minúsculas y mayúsculas.</li><li>Al menos 1 caracter especial (*, /, -, +, etc...)</li>"
                End Select
                pnlMsg.Visible = True
            Else
                pnlMsg.Visible = True
                lblMsg.Text = "Recuerde que la clave debe contener: <br /><br /><li>10 Caracteres Mínimo.</li><li>Números, Letras Letras minúsculas y mayúsculas.</li><li>Al menos 1 caracter especial (*, /, -, +, etc...)</li>"
            End If
        End If
    End Sub

    Protected Sub btnChange_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChange.Click

        Dim respVal As Integer

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Update")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        userObj = New User(strConnectionString, objAccessToken.UserID)

        respVal = userObj.updateUserPassword(txtCurrentPassword.Text, txtNewPassword.Text)

        If respVal = 1 Then
            pnlMsg.Visible = True
            pnlError.Visible = False
            lblMsg.Text = "Nueva clave almacenada correctamente."
        ElseIf respVal = 0 Then
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Clave Actual Errónea, verifique los datos."
        ElseIf respVal = 2 Then
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Clave utilizada anteriormente, por favor ingrese una clave diferente."
        End If

    End Sub
End Class
