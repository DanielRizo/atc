﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="EditAccount.aspx.vb" Inherits="Account_EditAccount" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Editar Cuenta
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   
    
	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script>      
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>Editar Cuenta</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Cuenta de Usuario
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite cambiar los datos de su cuenta de usuario:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>   
            
        <asp:Panel ID="pnlError" runat="server" Visible="False">    
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

	    <div class="titleh">
    	    <h3>Datos de la Cuenta</h3>
        </div>
        <div class="body">
        
            <div class="st-form-line">	
                <span class="st-labeltext">Login:</span>	
                <asp:TextBox ID="txtLogin" CssClass="st-forminput" style="width:510px" 
                    runat="server" Enabled="false" ToolTip="Login de Acceso al Sistema" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>        
        
            <div class="st-form-line">	
                <span class="st-labeltext">Nombre:</span>	
                <asp:TextBox ID="txtUserName" CssClass="st-success-input" style="width:510px" 
                    runat="server" TabIndex="1" MaxLength="100" ToolTip="Nombre del Usuario" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>
            
            <div class="st-form-line">	
                <span class="st-labeltext">Correo Electrónico:</span>	
                <asp:TextBox ID="txtEmail" CssClass="st-success-input" style="width:510px" 
                    runat="server" TabIndex="2" MaxLength="100" 
                    ToolTip="Dirección de Correo Electrónico" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>   
            
            <div class="st-form-line">	
                <span class="st-labeltext">Perfil de Usuario:</span>	                
                <asp:DropDownList ID="ddlUserProfile" runat="server" 
                    DataSourceID="dsUserProfile" DataTextField="perf_nombre" 
                    DataValueField="perf_id" Width="200px" 
                    ToolTip="Perfil del usuario." TabIndex="3" Enabled="false" 
                    ForeColor="Gray" >
                </asp:DropDownList>
                <asp:SqlDataSource ID="dsUserProfile" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" SelectCommand="SELECT -1 AS perf_id, '       ' AS perf_nombre
                        UNION ALL
                        SELECT * FROM Perfil WHERE perf_id NOT IN (5,6,7)"></asp:SqlDataSource>
                <div class="clear"></div>
            </div>            

            <div class="st-form-line">
                <span class="st-labeltext">Cliente:</span>
                <asp:DropDownList ID="ddlCustomer" runat="server"
                    DataSourceID="dsCustomer" DataTextField="cli_nombre"
                    DataValueField="cli_id" Width="200px"
                    ToolTip="Cliente asociado al usuario." TabIndex="4" Enabled="False"
                    ForeColor="Gray">
                </asp:DropDownList>
                <asp:SqlDataSource ID="dsCustomer" runat="server"
                    ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" SelectCommand="SELECT -1 AS cli_id, '       ' AS cli_nombre
                    UNION ALL
                    SELECT cli_id, cli_nombre FROM CLIENTE"></asp:SqlDataSource>
                <div class="clear"></div>
            </div>
            
            <div class="button-box">
                <asp:Button ID="btnSave" runat="server" Text="Guardar" CssClass="button-aqua" 
                    TabIndex="4" onclientclick="return validateEditUser()" />
            </div>
            
        </div>
    </div>
    
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Index.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">
    
    <!-- Validator -->
    <script src="../js/ValidatorUser.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

