﻿Imports TeleLoader.Users

Partial Class Account_EditAccount
    Inherits TeleLoader.Web.BasePage

    Public userObj As User

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Update")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try        

        userObj = New User(strConnectionString, objAccessToken.UserID)

        'Set New Data
        userObj.DisplayName = txtUserName.Text
        userObj.Email = txtEmail.Text
        userObj.PerfilUser = ddlUserProfile.SelectedValue

        'Save Data
        If userObj.saveUserData() Then
            'Data Saved OK
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Update", "Cuenta Editada. Datos[ " & getFormDataLog() & " ]", "")

            objAccessToken.DisplayName = userObj.DisplayName
            objAccessToken.Email = userObj.Email
            Session("AccessToken") = objAccessToken

            pnlMsg.Visible = True
            pnlError.Visible = False
            lblMsg.Text = "Datos almacenados correctamente."
        Else
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Update", "Error Editando Cuenta. Datos[ " & getFormDataLog() & " ]", "")
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error almacenando datos del usuario."
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            userObj = New User(strConnectionString, objAccessToken.UserID)

            'Get Data
            userObj.getUserData()

            'Set Current Data
            txtLogin.Text = userObj.LoginName
            txtUserName.Text = userObj.DisplayName
            txtEmail.Text = userObj.Email
            ddlUserProfile.SelectedValue = userObj.Profile
            ddlCustomer.SelectedValue = userObj.Customer

            txtUserName.Focus()
        End If
    End Sub
End Class
