﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic

Namespace TeleLoader.Security

    ''' <summary>
    ''' Representa una excepción producto de un fallo de acceso
    ''' </summary>
    Public Class AccessException
        Inherits System.Exception

        Private strAttemptedTypeName As String
        Private strAttemptedMethodName As String

        Public Sub New(ByVal attemptedType As System.Type, ByVal attemptedMethodName As String)
            MyBase.New("Acceso denegado a " & attemptedType.Name & "." & attemptedMethodName)
            strAttemptedTypeName = attemptedType.Name
            strAttemptedMethodName = attemptedMethodName
        End Sub

        Public Sub New(ByVal attemptedType As String, ByVal attemptedMethodName As String)
            MyBase.New("Acceso denegado a " & attemptedType & "." & attemptedMethodName)
            strAttemptedTypeName = attemptedType
            strAttemptedMethodName = attemptedMethodName
        End Sub

        Public Sub New(ByVal attemptedFunctionID As Integer)
            MyBase.New("Acceso denegado a función" & attemptedFunctionID.ToString)
        End Sub

        Public ReadOnly Property AttemptedTypeName() As String
            Get
                Return strAttemptedTypeName
            End Get
        End Property

        Public ReadOnly Property AttemptedMethodName() As String
            Get
                Return strAttemptedMethodName
            End Get
        End Property

    End Class

    Public Class AccessToken

        Public Enum LogType
            CriticalError = 1 'Error grave de aplicación
            ApplicationError = 2 'Error de aplicación
            AccessDenied = 3 'Acceso denegado
            Message = 4 'Mensaje informativo
            AccessGranted = 5 'Acceso permitido
            SessionStart = 6 'Inicio de sesión
            SessionEnd = 7 'Fin de sesión
        End Enum

        Private strConnString As String
        Private intUserID As Integer
        Private intUserPerfilID As Integer
        Private intCustomerUserID As Integer
        Private strLoginName As String
        Private strDisplayName As String
        Private strEmail As String
        Private strProfile As String
        Private strCustomer As String
        Private strReferCode As String
        Private strFieldName As String
        Private intLicenseType As Integer
        Private intLicenseStatus As Integer
        Private dateLicenseExpiryDate As DateTime
        Private intSessionID As Integer
        Private intErrorCode As Integer
        Private strException As String
        Private strIP As String
        Private dateLoginInitDateTime As DateTime
        Private dateExpirationDateTime As DateTime
        Private strSessionID As String
        Private bolEnabled As Boolean
        Private bolAuthenticated As Boolean
        Private bolActive As Boolean
        Private cache As New Dictionary(Of String, Boolean)
        Private dttMenu As DataTable

        'Fixed Data
        Private ReadOnly intEntryLoginType As Integer = 2

        Public Sub New()

            Dim configurationSection As ConnectionStringsSection = _
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

            strConnString = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString

        End Sub

        ''' <summary>
        ''' Login del usuario
        ''' </summary>
        Public ReadOnly Property LoginName() As String
            Get
                Return strLoginName
            End Get
        End Property

        ''' <summary>
        ''' Nombre a mostrar del usuario
        ''' </summary>
        Public Property DisplayName() As String
            Get
                Return strDisplayName
            End Get
            Set(ByVal value As String)
                strDisplayName = value
            End Set
        End Property

        ''' <summary>
        ''' Email del usuario
        ''' </summary>
        Public Property Email() As String
            Get
                Return strEmail
            End Get
            Set(ByVal value As String)
                strEmail = value
            End Set
        End Property

        ''' <summary>
        ''' Perfil descriptivo del usuario
        ''' </summary>
        Public ReadOnly Property Profile() As String
            Get
                Return strProfile
            End Get
        End Property

        ''' <summary>
        ''' Cliente asociado al usuario
        ''' </summary>
        Public ReadOnly Property Customer() As String
            Get
                Return strCustomer
            End Get
        End Property
        Public ReadOnly Property FieldName() As String
            Get
                Return strFieldName
            End Get
        End Property

        Public ReadOnly Property ReferCode() As String
            Get
                Return strReferCode
            End Get
        End Property


        ''' <summary>
        ''' Identificador del Estado del Usuario
        ''' </summary>
        Public ReadOnly Property Enabled() As Boolean
            Get
                Return bolEnabled
            End Get
        End Property

        ''' <summary>
        ''' Si el usuario está logueado
        ''' </summary>
        Public ReadOnly Property Authenticated() As Boolean
            Get
                Return bolAuthenticated
            End Get
        End Property

        ''' <summary>
        ''' Si la sesión del usuario está Activa
        ''' </summary>
        Public Property Active() As Boolean
            Get
                Return bolActive
            End Get
            Set(ByVal value As Boolean)
                bolActive = value
            End Set
        End Property

        ''' <summary>
        ''' Código identificador único del usuario
        ''' </summary>
        ''' 
        Public ReadOnly Property UserID() As Integer
            Get
                Return intUserID
            End Get
        End Property

        ''' <summary>
        ''' Código identificador único del usuario
        ''' </summary>
        ''' 
        Public ReadOnly Property UserPerfilID() As Integer
            Get
                Return intUserPerfilID
            End Get
        End Property

        ''' <summary>
        ''' Código identificador del id Cliente del usuario
        ''' </summary>
        ''' 
        Public ReadOnly Property CustomerUserID() As Integer
            Get
                Return intCustomerUserID
            End Get
        End Property

        ''' <summary>
        ''' IP de Conexión para la Sesión
        ''' </summary>
        Public ReadOnly Property IPAddress() As String
            Get
                Return strIP
            End Get
        End Property

        ''' <summary>
        ''' Fecha de Inicio de la Sesión
        ''' </summary>
        Public ReadOnly Property DateLogin() As DateTime
            Get
                Return dateLoginInitDateTime
            End Get
        End Property

        ''' <summary>
        ''' Fecha de Expiración Clave
        ''' </summary>
        Public ReadOnly Property DatePasswordExp() As DateTime
            Get
                Return dateExpirationDateTime
            End Get
        End Property

        ''' <summary>
        ''' Session ID para el usuario
        ''' </summary>
        Public Property SessionID() As String
            Get
                Return strSessionID
            End Get
            Set(ByVal value As String)
                strSessionID = value
            End Set
        End Property

        ''' <summary>
        ''' Tipo de Licencia de Uso para el Cliente/Usuario
        ''' </summary>
        ''' 
        Public ReadOnly Property LicenseType() As Integer
            Get
                Return intLicenseType
            End Get
        End Property

        ''' <summary>
        ''' Fecha de Expiración Licencia
        ''' </summary>
        Public ReadOnly Property LicenseExpiryDate() As DateTime
            Get
                Return dateLicenseExpiryDate
            End Get
        End Property

        ''' <summary>
        ''' Error Code para el Intento de Login
        ''' </summary>
        Public ReadOnly Property LoginError() As Integer
            Get
                Return intErrorCode
            End Get
        End Property

        ''' <summary>
        ''' Exception Encontrada
        ''' </summary>
        Public ReadOnly Property ExceptionMsg() As String
            Get
                Return strException
            End Get
        End Property

        ''' <summary>
        ''' Retornar el Menú correspondiente al usuario
        ''' </summary>
        Public Function GetMenu(ByVal SelectedMenu As String) As DataTable
            If Me.Authenticated And Me.Enabled Then
                If dttMenu Is Nothing Then
                    dttMenu = LoadModules()
                End If
            Else
                dttMenu = Nothing
            End If

            'Establecer Clase CSS
            Dim i As Integer = 0
            For i = 0 To dttMenu.Rows.Count - 1
                If dttMenu.Rows(i)("menuID") = SelectedMenu Then
                    dttMenu.Rows(i)("menuCSSClass") = "active"
                Else
                    dttMenu.Rows(i)("menuCSSClass") = ""
                End If
            Next i

            Return dttMenu

        End Function

        ''' <summary>
        ''' Validar Login del usuario
        ''' </summary>
        Public Sub validateLogin(ByVal loginName As String, ByVal loginPassword As String, _
                                    ByVal loginIP As String, ByVal loginSessionID As String)
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webValidarLogin"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("userID", loginName))
                command.Parameters.Add(New SqlParameter("passwordID", loginPassword))
                command.Parameters.Add(New SqlParameter("IP", loginIP))
                command.Parameters.Add(New SqlParameter("sessionID", loginSessionID))
                command.Parameters.Add(New SqlParameter("webEntryType", intEntryLoginType)) 'Tipo Web

                'Leer Resultado
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        strLoginName = loginName
                        dateLoginInitDateTime = Now()
                        strIP = loginIP
                        intSessionID = results.GetInt32(0)
                        intUserID = results.GetInt32(1)
                        strDisplayName = results.GetString(3)
                        strProfile = results.GetString(4)
                        dateExpirationDateTime = results.GetDateTime(5)
                        intErrorCode = results.GetInt32(6)
                        intCustomerUserID = results.GetInt32(7)
                        intUserPerfilID = results.GetInt32(8)
                        strCustomer = results.GetString(9)
                        intLicenseType = results.GetInt32(10)
                        intLicenseStatus = results.GetInt32(11)
                        dateLicenseExpiryDate = results.GetDateTime(12)
                    End While
                Else
                    strLoginName = ""
                    intUserID = ""
                    strDisplayName = ""
                    strProfile = 0
                    intErrorCode = 0
                    intCustomerUserID = -1
                    intUserPerfilID = -1
                    strCustomer = ""
                    intLicenseType = ""
                    intLicenseStatus = ""
                    dateLicenseExpiryDate = Now
                End If

                'Validar Resultados
                If intErrorCode = 1 Then
                    bolAuthenticated = True
                    bolEnabled = True
                Else
                    bolAuthenticated = False
                    bolEnabled = False
                End If

            Catch ex As Exception
                intErrorCode = 99
                strException = ex.Message
            Finally
                'Cerrar DataReader
                If Not (results Is Nothing) Then
                    results.Close()
                End If

                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

        End Sub

        ''' <summary>
        ''' Retornar número de Intentos de Clave Erróneo por día Actual
        ''' </summary>
        Public Function getWrongPasswordCounter() As Integer
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim intErrorCounter As Integer

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webContarErroresClave"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("userID", intUserID))

                'Leer Resultado
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        intErrorCounter = results.GetInt32(0)
                    End While
                Else
                    intErrorCounter = 0
                End If

                Return intErrorCounter

            Catch ex As Exception

            Finally
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

        End Function

        ''' <summary>
        ''' Cerrar Sesión WEB del Usuario
        ''' </summary>
        Public Sub closeSession()
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webCerrarSesionWeb"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("loginID", intSessionID))

                'Leer Resultado
                command.ExecuteNonQuery()

            Catch ex As Exception

            Finally
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

        End Sub

        ''' <summary>
        ''' Cargar Módulos de la BD según perfil de Usuario
        ''' </summary>
        Private Function LoadModules() As DataTable
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim table As New DataTable

            Try
                'Abrir Conexion
                connection.Open()

                'Recuperar el menú dinámico
                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webConsultarModulosMenu"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("userID", intUserID))

                'Recuperar los resultados y armar una DataTable
                results = command.ExecuteReader()
                If results.HasRows Then
                    table.Columns.Add(New DataColumn("menuID", GetType(String)))
                    table.Columns.Add(New DataColumn("menuLabel", GetType(String)))
                    table.Columns.Add(New DataColumn("menuLnkUrl", GetType(String)))
                    table.Columns.Add(New DataColumn("menuImgUrl", GetType(String)))
                    table.Columns.Add(New DataColumn("menuCSSClass", GetType(String)))

                    While results.Read()
                        Dim drwRow As DataRow
                        drwRow = table.NewRow
                        drwRow.Item("menuID") = results.GetString(0)
                        drwRow.Item("menuLabel") = results.GetString(1)
                        drwRow.Item("menuLnkUrl") = results.GetString(2)
                        drwRow.Item("menuImgUrl") = results.GetString(3)
                        drwRow.Item("menuCSSClass") = ""
                        table.Rows.Add(drwRow)
                    End While
                End If
            Catch ex As Exception

            Finally
                'Cerrar DataReader
                If Not (results Is Nothing) Then
                    results.Close()
                End If

                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return table

        End Function

        ''' <summary>
        ''' Valida el objeto y método indicados, registrando el intento de acceso en la base de datos
        ''' en caso de no ser un acceso válido, genera una excepción de acceso
        ''' </summary>
        ''' <remarks>Tener en cuenta la generación de excepciones, que deben ser manejadas por el llamador</remarks>
        Public Function Validate(ByVal desiredObject As String, ByVal desiredMethod As String) As Long
            Dim conn As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim result As Object

            If Not bolEnabled Then Return False

            'Conectarse
            conn.Open()

            'Armar el comando
            command.Connection = conn
            command.CommandType = Data.CommandType.StoredProcedure
            command.CommandText = "sp_webValidarObjetoPorMetodo"

            command.Parameters.Add(New SqlParameter("@UserID", intUserID))
            command.Parameters.Add(New SqlParameter("@Object", desiredObject))
            command.Parameters.Add(New SqlParameter("@Method", desiredMethod))
            command.Parameters.Add(New SqlParameter("@SessionID", strSessionID))

            'Recuperar los resultados
            result = command.ExecuteScalar()

            'Cerrar
            conn.Close()

            If Not TypeOf result Is System.DBNull Then
                Return result
            Else
                Throw New AccessException(desiredObject, desiredMethod)
            End If

        End Function

        'Modificación Carga de Llaves; EB: 28/Feb/2018

        ''' <summary>
        ''' Valida el objeto y método indicados
        ''' </summary>
        Public Function ValidateInGridView(ByVal desiredObject As String, ByVal desiredMethod As String) As Boolean
            Dim conn As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim result As Boolean

            If Not bolEnabled Then Return False

            'Conectarse
            conn.Open()

            'Armar el comando
            command.Connection = conn
            command.CommandType = Data.CommandType.StoredProcedure
            command.CommandText = "sp_webValidarObjetoPorMetodoGridView"

            command.Parameters.Add(New SqlParameter("@UserID", intUserID))
            command.Parameters.Add(New SqlParameter("@Object", desiredObject))
            command.Parameters.Add(New SqlParameter("@Method", desiredMethod))

            'Recuperar los resultados
            result = command.ExecuteScalar()

            'Cerrar
            conn.Close()

            Return result
            
        End Function

        ''' <summary>
        ''' Valida el objeto y método indicados, registrando el intento de acceso en la base de datos
        ''' en caso de no ser un acceso válido, genera una excepción de acceso, Retorna el LogID generado
        ''' </summary>
        ''' <remarks>Tener en cuenta la generación de excepciones, que deben ser manejadas por el llamador</remarks>
        Public Function ValidateLogID(ByVal desiredObject As String, ByVal desiredMethod As String) As Long
            Dim conn As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim functionID As Long
            Dim logID As Long

            If Not bolEnabled Then Return False

            'Conectarse
            conn.Open()

            'Armar el comando
            command.Connection = conn
            command.CommandType = Data.CommandType.StoredProcedure
            command.CommandText = "sp_webValidarObjetoPorMetodoLogID"

            command.Parameters.Add(New SqlParameter("@UserID", intUserID))
            command.Parameters.Add(New SqlParameter("@Object", desiredObject))
            command.Parameters.Add(New SqlParameter("@Method", desiredMethod))
            command.Parameters.Add(New SqlParameter("@SessionID", strSessionID))

            'Recuperar los resultados
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()

                    If Not results.IsDBNull(0) Then
                        functionID = results.GetInt32(0)
                    Else
                        functionID = -1
                    End If

                    logID = results.GetInt64(1)
                End While
            End If

            'Cerrar DataReader
            If Not (results Is Nothing) Then
                results.Close()
            End If

            'Cerrar conexion por Default
            If Not (conn Is Nothing) Then
                conn.Close()
            End If

            If functionID > 0 Then
                Return logID
            Else
                Throw New AccessException(desiredObject, desiredMethod)
            End If

        End Function

        ''' <summary>
        ''' Inserta/Obtiene en Caché el Objeto y el método requerido
        ''' </summary>
        ''' <remarks></remarks>
        Private Function GetQueryKey(ByVal desiredObject As String, ByVal desiredMethod As String) As String
            Return "om:" & desiredObject & "." & desiredMethod
        End Function

        ''' <summary>
        ''' Valida el objeto y método indicados por el usuario actual
        ''' Verifica en Caché Local o BD
        ''' </summary>
        ''' <remarks></remarks>
        Public Function Peek(ByVal desiredObject As String, ByVal desiredMethod As String) As Long
            If cache.ContainsKey(GetQueryKey(desiredObject, desiredMethod)) Then
                Return CBool(cache.Item(GetQueryKey(desiredObject, desiredMethod)))
            Else
                Peek = Query(desiredObject, desiredMethod)
                cache.Add(GetQueryKey(desiredObject, desiredMethod), Peek)
                Return Peek
            End If
        End Function

        ''' <summary>
        ''' Valida el objeto y método indicados por el usuario actual
        ''' No genera Exception Ni Errores, Ni almacena acceso a BD
        ''' </summary>
        ''' <remarks></remarks>
        Private Function Query(ByVal desiredObject As String, ByVal desiredMethod As String) As Boolean
            Dim conn As New SqlConnection(strConnString)
            Dim retVal As Boolean
            Dim command As New SqlCommand()

            If Not bolEnabled Then Return False

            'Conectarse
            conn.Open()

            'Armar el comando
            command.Connection = conn
            command.CommandType = Data.CommandType.StoredProcedure
            command.CommandText = "sp_webValidarSoloAccesoObjetoPorMetodo"

            command.Parameters.Add(New SqlParameter("@UserID", intUserID))
            command.Parameters.Add(New SqlParameter("@Object", desiredObject))
            command.Parameters.Add(New SqlParameter("@Method", desiredMethod))

            'Recuperar los resultados
            If command.ExecuteScalar() > 0 Then
                retVal = True
            Else
                retVal = False
            End If

            'Cerrar
            conn.Close()

            Return retVal

        End Function

        ''' <summary>
        ''' Inserta un mensaje informativo en el Log de Auditoría
        ''' </summary>
        Public Function LogMessage(ByVal messageType As AccessToken.LogType, ByVal messageText1 As String, ByVal messageText2 As String) As Long
            Dim conn As New SqlConnection(strConnString)
            Dim command As New SqlCommand()

            If Not bolEnabled Then Return False

            'Conectarse
            conn.Open()

            'Armar el comando
            command.Connection = conn
            command.CommandType = Data.CommandType.StoredProcedure
            command.CommandText = "sp_webInsertarMensajeInfoLog"

            command.Parameters.Add(New SqlParameter("@UserID", intUserID))
            command.Parameters.Add(New SqlParameter("@LogTypeID", messageType))
            command.Parameters.Add(New SqlParameter("@Message1", messageText1))
            command.Parameters.Add(New SqlParameter("@Message2", messageText2))
            command.Parameters.Add(New SqlParameter("@SessionID", strSessionID))

            'Ejecutar
            command.ExecuteNonQuery()

            'Cerrar
            conn.Close()

        End Function

        ''' <summary>
        ''' Inserta un mensaje general por Objeto/Método en el Log de Auditoría
        ''' </summary>
        Public Function LogMessageByObjectMethod(ByVal objectStr As String, ByVal methodStr As String, ByVal messageText1 As String, ByVal messageText2 As String) As Long
            Dim conn As New SqlConnection(strConnString)
            Dim command As New SqlCommand()

            If Not bolEnabled Then Return False

            'Conectarse
            conn.Open()

            'Armar el comando
            command.Connection = conn
            command.CommandType = Data.CommandType.StoredProcedure
            command.CommandText = "sp_webInsertarMensajeXObjetoMetodoLog"

            command.Parameters.Add(New SqlParameter("@UserID", intUserID))
            command.Parameters.Add(New SqlParameter("@Object", objectStr))
            command.Parameters.Add(New SqlParameter("@Method", methodStr))
            command.Parameters.Add(New SqlParameter("@Message1", messageText1))
            command.Parameters.Add(New SqlParameter("@Message2", messageText2))
            command.Parameters.Add(New SqlParameter("@SessionID", strSessionID))

            'Ejecutar
            command.ExecuteNonQuery()

            'Cerrar
            conn.Close()

        End Function

    End Class

End Namespace
