﻿Imports Microsoft.VisualBasic

Namespace TeleLoader.MDMRequestResponse
    Public Class ActivateDeviceReq
        Public Property operacion As String = ""
        Public Property login As String = ""
        Public Property pass As String = ""
        Public Property ip As String = ""
        Public Property id_device As String = ""
    End Class

    Public Class ActivateDeviceResp
        Public Property res As String = ""
        Public Property idDevice As String = ""
        Public Property groupName As String = ""
    End Class

End Namespace

