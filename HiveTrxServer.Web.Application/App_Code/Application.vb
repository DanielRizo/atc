﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic

Namespace TeleLoader.Applications

    Public Class Application

        Private strConnString As String
        Private intApplicationID As Integer
        Private intGroupID As Integer
        Private intTerminalModel As String
        Private strPhysicalFilename As String
        Private strTMSFilename As String
        Private strTMSChecksum As String
        Private strAplData As String
        Private strHardDrivePath As String
        Private strDesc As String
        Private strFilename As String
        Private strPackageName As String
        Private strVersion As String
        Private intDeployTypeID As Integer

        Public Sub New(ByVal strConnString As String)
            Me.strConnString = strConnString
        End Sub

        Public Sub New(ByVal strConnString As String, ByVal groupId As Integer)
            Me.strConnString = strConnString
            Me.intGroupID = groupId
        End Sub

        Public Sub New(ByVal strConnString As String, ByVal groupId As Integer, ByVal appId As Integer)
            Me.strConnString = strConnString
            Me.intGroupID = groupId
            Me.intApplicationID = appId
        End Sub

        Public Sub New()

            Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

            strConnString = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString

        End Sub

        ''' <summary>
        ''' Código del Grupo
        ''' </summary>
        Public Property GroupCode() As Integer
            Get
                Return intGroupID
            End Get
            Set(ByVal value As Integer)
                intGroupID = value
            End Set
        End Property
        ''' </summary>
        Public Property Filename() As String
            Get
                Return strFilename
            End Get
            Set(ByVal value As String)
                strFilename = value
            End Set
        End Property

        ''' <summary>
        ''' Nombre del Paquete del APK Desplegado
        ''' </summary>
        Public Property PackageName() As String
            Get
                Return strPackageName
            End Get
            Set(ByVal value As String)
                strPackageName = value
            End Set
        End Property

        ''' <summary>
        ''' Versión del APK Desplegado
        ''' </summary>
        Public Property Version() As String
            Get
                Return strVersion
            End Get
            Set(ByVal value As String)
                strVersion = value
            End Set
        End Property

        ''' <summary>
        ''' Tipo de Despliegue
        ''' </summary>
        Public Property DeployTypeID() As Integer
            Get
                Return intDeployTypeID
            End Get
            Set(ByVal value As Integer)
                intDeployTypeID = value
            End Set
        End Property


        ''' <summary>
        ''' Checksum Interno del TMS
        ''' </summary>
        Public Property TMSChecksum() As String
            Get
                Return strTMSChecksum
            End Get
            Set(ByVal value As String)
                strTMSChecksum = value
            End Set
        End Property

        Public Property AplData() As String
            Get
                Return strAplData
            End Get
            Set(ByVal value As String)
                strAplData = value
            End Set
        End Property
        ''' <summary>
        ''' Nombre Interno del TMS
        ''' </summary>
        Public Property TMSFilename() As String
            Get
                Return strTMSFilename
            End Get
            Set(ByVal value As String)
                strTMSFilename = value
            End Set
        End Property

        ''' <summary>
        ''' Nombre físico del archivo
        ''' </summary>
        Public Property PhysicalFilename() As String
            Get
                Return strPhysicalFilename
            End Get
            Set(ByVal value As String)
                strPhysicalFilename = value
            End Set
        End Property

        ''' <summary>
        ''' Descripción de la Aplicación
        ''' </summary>
        Public Property Desc() As String
            Get
                Return strDesc
            End Get
            Set(ByVal value As String)
                strDesc = value
            End Set
        End Property
        ''' <summary>
        ''' Carga de Aplicaciones Por Modelo
        ''' </summary>
        Public Property TerminalModel() As String
            Get
                Return intTerminalModel
            End Get
            Set(ByVal value As String)
                intTerminalModel = value
            End Set
        End Property
        ''' <summary>
        ''' Ruta Local en disco Duro para cargar Archivo
        ''' </summary>
        Public Property HardDrivePath() As String
            Get
                Return strHardDrivePath
            End Get
            Set(ByVal value As String)
                strHardDrivePath = value
            End Set
        End Property
        Public Function createDeployedFileGroupAndroid() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webDesplegarArchivoGrupoAndroid"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("groupID", intGroupID))
                command.Parameters.Add(New SqlParameter("filename", strFilename))
                command.Parameters.Add(New SqlParameter("packageName", strPackageName))
                command.Parameters.Add(New SqlParameter("version", strVersion))
                command.Parameters.Add(New SqlParameter("deployType", intDeployTypeID))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function


        ''' <summary>
        ''' Crear Aplicación en el grupo
        ''' </summary>
        Public Function createApplicationGroup() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webCrearAplicacionGrupo"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("physicalFilename", strPhysicalFilename))
                command.Parameters.Add(New SqlParameter("tmsFilename", strTMSFilename))
                command.Parameters.Add(New SqlParameter("tmsChecksum", strTMSChecksum))
                command.Parameters.Add(New SqlParameter("hardDrivePath", strHardDrivePath))
                command.Parameters.Add(New SqlParameter("appDesc", strDesc))
                command.Parameters.Add(New SqlParameter("groupID", intGroupID))
                'Carga de Aplicaciones Por Modelo
                command.Parameters.Add(New SqlParameter("terminalModel", intTerminalModel))
                command.Parameters.Add(New SqlParameter("AplData", strAplData))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try
            Return retVal

        End Function

    End Class

End Namespace
