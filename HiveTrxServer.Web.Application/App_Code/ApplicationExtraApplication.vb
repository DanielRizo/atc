﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic

Namespace TeleLoader.Applications

    Public Class ApplicationExtraApplication

        Private strConnString As String
        Private intApplicationID As Integer
        Private StrExtraApplcation As Char
        Private strPhysicalFilename As String
        Private strTMSFilename As String
        Private strReferCode As String
        Private strTMSChecksum As String
        Private strHardDrivePath As String
        Private strDesc As String

        Public Sub New(ByVal strConnString As String)
            Me.strConnString = strConnString
        End Sub

        Public Sub New(ByVal strConnString As String, ByVal APP_PARA_NAME As Char)
            Me.strConnString = strConnString
            Me.StrExtraApplcation = APP_PARA_NAME
        End Sub

        Public Sub New(ByVal strConnString As String, ByVal APP_PARA_NAME As Char, ByVal appId As Integer)
            Me.strConnString = strConnString
            Me.StrExtraApplcation = APP_PARA_NAME
            Me.intApplicationID = appId
        End Sub

        Public Sub New()

            Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

            strConnString = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString

        End Sub

        ''' <summary>
        ''' Código del Grupo
        ''' </summary>
        Public Property StisCode() As Char
            Get
                Return StrExtraApplcation
            End Get
            Set(ByVal value As Char)
                StrExtraApplcation = value
            End Set
        End Property

        ''' <summary>
        ''' Checksum Interno del TMS
        ''' </summary>
        Public Property ReferCode() As String
            Get
                Return strReferCode
            End Get
            Set(ByVal value As String)
                strReferCode = value
            End Set
        End Property

        ''' <summary>
        ''' Nombre Interno del TMS
        ''' </summary>
        Public Property TMSFilename() As String
            Get
                Return strTMSFilename
            End Get
            Set(ByVal value As String)
                strTMSFilename = value
            End Set
        End Property

        ''' <summary>
        ''' Nombre físico del archivo
        ''' </summary>
        Public Property PhysicalFilename() As String
            Get
                Return strPhysicalFilename
            End Get
            Set(ByVal value As String)
                strPhysicalFilename = value
            End Set
        End Property

        ''' <summary>
        ''' Descripción de la Aplicación
        ''' </summary>
        Public Property Desc() As String
            Get
                Return strDesc
            End Get
            Set(ByVal value As String)
                strDesc = value
            End Set
        End Property

        ''' <summary>
        ''' Ruta Local en disco Duro para cargar Archivo
        ''' </summary>
        Public Property HardDrivePath() As String
            Get
                Return strHardDrivePath
            End Get
            Set(ByVal value As String)
                strHardDrivePath = value
            End Set
        End Property

        ''' <summary>
        ''' Crear Aplicación en el grupo
        ''' </summary>
        Public Function createApplicationGroup() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webCrearAplicacionGrupo"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("physicalFilename", strPhysicalFilename))
                command.Parameters.Add(New SqlParameter("tmsFilename", strTMSFilename))
                command.Parameters.Add(New SqlParameter("tmsChecksum", strTMSChecksum))
                command.Parameters.Add(New SqlParameter("hardDrivePath", strHardDrivePath))
                command.Parameters.Add(New SqlParameter("appDesc", strDesc))
                command.Parameters.Add(New SqlParameter("groupID", StrExtraApplcation))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

    End Class
End Namespace
