﻿Imports Microsoft.VisualBasic
Imports TeleLoader.Security
Imports TeleLoader.Sessions
Imports System.Data
Imports System.Data.SqlClient

Namespace TeleLoader.Web

    Public Class BasePage
        Inherits System.Web.UI.Page

        Protected objAccessToken As AccessToken
        Protected objSessionParams As SessionParameters
        Protected relationaldb As String
        Protected strConnectionString As String
        Protected strConnectionStringStis As String
        Protected errorPageName As String
        Protected strPageName As String

        Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

            Try
                relationaldb = ConfigurationSettings.AppSettings.Get("RelationalDB")
                errorPageName = ConfigurationSettings.AppSettings.Get("ErrorPageName")

                'Recuperar desde la sesión:
                'Conexión
                strConnectionString = Session("ConnectionString")
                Dim configurationSection As ConnectionStringsSection =
                            System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
                strConnectionStringStis = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString

                'Access Token para seguridad
                objAccessToken = Session("AccessToken")

                'Parámetros entre Páginas
                objSessionParams = Session("SessionParameters")

                'Datos del Nombre Página Actual
                strPageName = Request.AppRelativeCurrentExecutionFilePath
                strPageName = strPageName.Substring(strPageName.IndexOf("/") + 1)

                If Not objAccessToken Is Nothing Then
                    If objAccessToken.Authenticated = True And objAccessToken.Enabled = True Then
                        If Not IsPostBack Then
                            ' Validación de seguridad de cada página - Método Load
                            objAccessToken.Validate(strPageName, "Load")
                        End If
                    End If
                Else
                    Server.ClearError()
                    Response.Clear()
                    Response.Redirect("Login.aspx", False)
                End If

            Catch ex As Exception
                HandleErrorRedirect(ex)
            End Try

        End Sub

        ' Error handler estándar, redirecciona a página de error
        Protected Sub HandleErrorRedirect(ByVal exception As Exception)

            If Session("Exception") Is Nothing Then
                Session("Exception") = exception
            End If

            Server.ClearError()

            Response.Redirect(errorPageName, True)

        End Sub

        ''' <summary>
        ''' Dibujar Opciones del Módulo Seleccionado
        ''' </summary>
        Protected Sub DrawOptions(ByVal repOpcion As Repeater)
            Dim connection As SqlConnection
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim table As New DataTable

            Try

                connection = New SqlConnection(strConnectionString)

                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = Data.CommandType.StoredProcedure
                command.CommandText = "sp_webConsultarOpcionesSubmenu"

                command.Parameters.Add(New SqlParameter("@userID", objAccessToken.UserID))
                command.Parameters.Add(New SqlParameter("@menuID", objSessionParams.strSelectedMenu))

                'Recuperar los resultados
                results = command.ExecuteReader()

                If results.HasRows Then
                    table.Columns.Add(New DataColumn("opcionID", GetType(String)))
                    table.Columns.Add(New DataColumn("opcionLabel", GetType(String)))
                    table.Columns.Add(New DataColumn("opcionLnkUrl", GetType(String)))
                    table.Columns.Add(New DataColumn("opcionImgUrl", GetType(String)))

                    While results.Read()
                        Dim drwRow As DataRow
                        drwRow = table.NewRow
                        drwRow.Item("opcionID") = results.GetString(0)
                        drwRow.Item("opcionLabel") = results.GetString(1)
                        drwRow.Item("opcionLnkUrl") = results.GetString(2)
                        drwRow.Item("opcionImgUrl") = results.GetString(3)
                        table.Rows.Add(drwRow)
                    End While

                End If

                repOpcion.DataSource = table
                repOpcion.DataBind()

            Catch Ex As Exception
                Session("Exception") = Ex
                HandleErrorRedirect(Ex)
            Finally
                'Cerrar DataReader
                If Not (results Is Nothing) Then
                    results.Close()
                End If

                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try
        End Sub

        ''' <summary>
        ''' Nombre de la Página Actual
        ''' </summary>
        Public ReadOnly Property getCurrentPage() As String
            Get
                Return strPageName
            End Get
        End Property

        ''' <summary>
        ''' String de Conexión a BD
        ''' </summary>
        Public ReadOnly Property ConnectionString() As String
            Get
                Return strConnectionString
            End Get
        End Property
        Public ReadOnly Property ConnectionStringStis() As String
            Get
                Return strConnectionStringStis
            End Get
        End Property


        ''' <summary>
        ''' Retornar Lista de valores de Formulario para Log
        ''' </summary>
        Public Function getFormDataLog() As String
            Dim retVal As String = ""
            Dim c As Control
            Dim childc As Control

            For Each c In Form.Controls
                For Each childc In c.Controls
                    If TypeOf childc Is TextBox Then
                        If Not CType(childc, TextBox).ClientID.ToLower().Contains("password") And _
                            Not CType(childc, TextBox).ClientID.ToLower().Contains("clave") And _
                            Not CType(childc, TextBox).ClientID.ToLower().Contains("pass") Then
                            retVal &= CType(childc, TextBox).ClientID.Substring(CType(childc, TextBox).ClientID.LastIndexOf("_") + 1) & "=" & CType(childc, TextBox).Text & ", "
                        End If
                    ElseIf TypeOf childc Is DropDownList Then
                        If (CType(childc, DropDownList).SelectedValue = Nothing) Then
                            retVal &= CType(childc, DropDownList).ClientID.Substring(CType(childc, DropDownList).ClientID.LastIndexOf("_") + 1) & "=" & "" & ", "
                        Else
                            retVal &= CType(childc, DropDownList).ClientID.Substring(CType(childc, DropDownList).ClientID.LastIndexOf("_") + 1) & "=" & CType(childc, DropDownList).SelectedItem.Text & ", "
                        End If
                    ElseIf TypeOf childc Is CheckBox Then
                        retVal &= CType(childc, CheckBox).ClientID.Substring(CType(childc, CheckBox).ClientID.LastIndexOf("_") + 1) & "=" & CType(childc, CheckBox).Checked & ", "
                    ElseIf TypeOf childc Is Panel Then
                        For Each childp In childc.Controls
                            If TypeOf childp Is TextBox Then
                                If Not CType(childp, TextBox).ClientID.ToLower().Contains("password") And _
                                    Not CType(childp, TextBox).ClientID.ToLower().Contains("clave") And _
                                    Not CType(childp, TextBox).ClientID.ToLower().Contains("pass") Then
                                    retVal &= CType(childp, TextBox).ClientID.Substring(CType(childp, TextBox).ClientID.LastIndexOf("_") + 1) & "=" & CType(childp, TextBox).Text & ", "
                                End If
                            ElseIf TypeOf childp Is DropDownList Then
                                retVal &= CType(childp, DropDownList).ClientID.Substring(CType(childp, DropDownList).ClientID.LastIndexOf("_") + 1) & "=" & CType(childp, DropDownList).SelectedItem.Text & ", "
                            ElseIf TypeOf childp Is CheckBox Then
                                retVal &= CType(childp, CheckBox).ClientID.Substring(CType(childp, CheckBox).ClientID.LastIndexOf("_") + 1) & "=" & CType(childp, CheckBox).Checked & ", "
                            ElseIf TypeOf childc Is HiddenField Then
                                retVal &= CType(childc, HiddenField).ClientID.Substring(CType(childc, HiddenField).ClientID.LastIndexOf("_") + 1) & "=" & CType(childc, HiddenField).Value & ", "
                            End If
                        Next
                    ElseIf TypeOf childc Is HiddenField Then
                        retVal &= CType(childc, HiddenField).ClientID.Substring(CType(childc, HiddenField).ClientID.LastIndexOf("_") + 1) & "=" & CType(childc, HiddenField).Value & ", "
                    End If
                Next
            Next

            Return retVal

        End Function

        Private Sub Page_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
            ScriptManager.RegisterOnSubmitStatement(Me, Me.GetType(), "", "ShowProgressWindow();")
        End Sub
    End Class

End Namespace

