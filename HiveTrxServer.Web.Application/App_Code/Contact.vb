﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic

Namespace TeleLoader

    Public Class Contact

        Private intContactID As Integer
        Private intTypeID As Integer
        Private strIdentificationNumber As String
        Private strName As String
        Private strAddress As String
        Private strMobile As String
        Private strPhone As String
        Private strEmail As String

        ''' <summary>
        ''' Código del Contacto
        ''' </summary>
        Public Property ContactCode() As Integer
            Get
                Return intContactID
            End Get
            Set(ByVal value As Integer)
                intContactID = value
            End Set
        End Property

        ''' <summary>
        ''' Tipo de Identificación del Contacto
        ''' </summary>
        Public Property IdentificationType() As Integer
            Get
                Return intTypeID
            End Get
            Set(ByVal value As Integer)
                intTypeID = value
            End Set
        End Property

        ''' <summary>
        ''' Número de Identificación del Proveedor
        ''' </summary>
        Public Property IdentificationNumber() As String
            Get
                Return strIdentificationNumber
            End Get
            Set(ByVal value As String)
                strIdentificationNumber = value
            End Set
        End Property

        ''' <summary>
        ''' Nombre del Contacto
        ''' </summary>
        Public Property Name() As String
            Get
                Return strName
            End Get
            Set(ByVal value As String)
                strName = value
            End Set
        End Property

        ''' <summary>
        ''' Dirección del Contacto
        ''' </summary>
        Public Property Address() As String
            Get
                Return strAddress
            End Get
            Set(ByVal value As String)
                strAddress = value
            End Set
        End Property

        ''' <summary>
        ''' Número de Celular
        ''' </summary>
        Public Property Mobile() As String
            Get
                Return strMobile
            End Get
            Set(ByVal value As String)
                strMobile = value
            End Set
        End Property

        ''' <summary>
        ''' Número de Tel. Fijo
        ''' </summary>
        Public Property Phone() As String
            Get
                Return strPhone
            End Get
            Set(ByVal value As String)
                strPhone = value
            End Set
        End Property

        ''' <summary>
        ''' Correo Electrónico
        ''' </summary>
        Public Property Email() As String
            Get
                Return strEmail
            End Get
            Set(ByVal value As String)
                strEmail = value
            End Set
        End Property

    End Class

End Namespace
