﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic

Namespace TeleLoader.Customers

    Public Class Customer

        Private strConnString As String
        Private intCustomerID As Integer
        Private strName As String
        Private strDesc As String
        Private strIPAddress As String
        Private strPort As String
        Private intStatus As Integer
        Private objContact As TeleLoader.Contact
        Private boolLearningMode As Boolean

        Public Sub New(ByVal strConnString As String)
            Me.strConnString = strConnString
        End Sub

        Public Sub New(ByVal strConnString As String, ByVal customerId As Integer)
            Me.strConnString = strConnString
            Me.intCustomerID = customerId

            objContact = New TeleLoader.Contact()
        End Sub

        Public Sub New()

            Dim configurationSection As ConnectionStringsSection = _
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

            strConnString = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString

            objContact = New TeleLoader.Contact()

        End Sub

        ''' <summary>
        ''' Código del Cliente
        ''' </summary>
        Public Property CustomerCode() As Integer
            Get
                Return intCustomerID
            End Get
            Set(ByVal value As Integer)
                intCustomerID = value
            End Set
        End Property

        ''' <summary>
        ''' Nombre del Cliente
        ''' </summary>
        Public Property Name() As String
            Get
                Return strName
            End Get
            Set(ByVal value As String)
                strName = value
            End Set
        End Property

        ''' <summary>
        ''' Descripción del Cliente
        ''' </summary>
        Public Property Desc() As String
            Get
                Return strDesc
            End Get
            Set(ByVal value As String)
                strDesc = value
            End Set
        End Property

        ''' <summary>
        ''' IP de Descarga
        ''' </summary>
        Public Property IpAddress() As String
            Get
                Return strIPAddress
            End Get
            Set(ByVal value As String)
                strIPAddress = value
            End Set
        End Property

        ''' <summary>
        ''' Puerto de Descarga
        ''' </summary>
        Public Property Port() As String
            Get
                Return strPort
            End Get
            Set(ByVal value As String)
                strPort = value
            End Set
        End Property

        ''' <summary>
        ''' Estado del Cliente
        ''' </summary>
        Public Property Status() As Integer
            Get
                Return intStatus
            End Get
            Set(ByVal value As Integer)
                intStatus = value
            End Set
        End Property

        ''' <summary>
        ''' Contacto asociado al Cliente
        ''' </summary>
        Public Property Contact() As TeleLoader.Contact
            Get
                Return objContact
            End Get
            Set(ByVal value As TeleLoader.Contact)
                objContact = value
            End Set
        End Property

        Public Property LearningMode() As Boolean
            Get
                Return boolLearningMode
            End Get
            Set(ByVal value As Boolean)
                boolLearningMode = value
            End Set
        End Property

        ''' <summary>
        ''' Crear Cliente
        ''' </summary>
        Public Function createCustomer() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webCrearCliente"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("customerName", strName))
                command.Parameters.Add(New SqlParameter("customerDesc", strDesc))
                command.Parameters.Add(New SqlParameter("customerIP", strIPAddress))
                command.Parameters.Add(New SqlParameter("customerPort", strPort))
                command.Parameters.Add(New SqlParameter("learningMode", boolLearningMode))

                command.Parameters.Add(New SqlParameter("contactIDNumber", objContact.IdentificationNumber))
                command.Parameters.Add(New SqlParameter("contactIDType", objContact.IdentificationType))
                command.Parameters.Add(New SqlParameter("contactName", objContact.Name))
                command.Parameters.Add(New SqlParameter("contactAddress", objContact.Address))
                command.Parameters.Add(New SqlParameter("contactMobile", objContact.Mobile))
                command.Parameters.Add(New SqlParameter("contactPhone", objContact.Phone))
                command.Parameters.Add(New SqlParameter("contactEmail", objContact.email))


                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        ''' <summary>
        ''' Leer Datos del Cliente
        ''' </summary>
        Public Sub getCustomerData()
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webConsultarDatosCliente"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("customerID", intCustomerID))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        strName = results.GetString(0)
                        strDesc = results.GetString(1)
                        strIPAddress = results.GetString(2)
                        strPort = results.GetString(3)
                        intStatus = results.GetInt64(4)
                        'Learning mode
                        boolLearningMode = results.GetBoolean(5)
                        'Datos del Contacto
                        objContact.IdentificationNumber = results.GetString(6)
                        objContact.IdentificationType = results.GetInt64(7)
                        objContact.Name = results.GetString(8)
                        objContact.Address = results.GetString(9)
                        objContact.Mobile = results.GetString(10)
                        objContact.Phone = results.GetString(11)
                        objContact.Email = results.GetString(12)

                        If objContact.IdentificationNumber Is Nothing Then
                            objContact.IdentificationNumber = ""
                        End If

                    End While
                Else
                    strName = ""
                    strDesc = ""
                    strIPAddress = ""
                    strPort = ""
                    intStatus = -1
                    boolLearningMode = False
                    'Datos del Contacto
                    objContact.IdentificationNumber = ""
                    objContact.IdentificationType = -1
                    objContact.Name = ""
                    objContact.Address = ""
                    objContact.Mobile = ""
                    objContact.Phone = ""
                    objContact.Email = ""
                End If

            Catch ex As Exception

            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

        End Sub

        ''' <summary>
        ''' Editar Cliente
        ''' </summary>
        Public Function editCustomer(ByRef msg As String) As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            msg = ""

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webEditarCliente"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("customerID", intCustomerID))
                command.Parameters.Add(New SqlParameter("customerName", strName))
                command.Parameters.Add(New SqlParameter("customerDesc", strDesc))
                command.Parameters.Add(New SqlParameter("customerIP", strIPAddress))
                command.Parameters.Add(New SqlParameter("customerPort", strPort))
                command.Parameters.Add(New SqlParameter("customerStatus", intStatus))
                command.Parameters.Add(New SqlParameter("learningMode", boolLearningMode))

                command.Parameters.Add(New SqlParameter("contactIDNumber", objContact.IdentificationNumber))
                command.Parameters.Add(New SqlParameter("contactIDType", objContact.IdentificationType))
                command.Parameters.Add(New SqlParameter("contactName", objContact.Name))
                command.Parameters.Add(New SqlParameter("contactAddress", objContact.Address))
                command.Parameters.Add(New SqlParameter("contactMobile", objContact.Mobile))
                command.Parameters.Add(New SqlParameter("contactPhone", objContact.Phone))
                command.Parameters.Add(New SqlParameter("contactEmail", objContact.Email))


                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                        msg = results.GetString(1)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

    End Class

End Namespace
