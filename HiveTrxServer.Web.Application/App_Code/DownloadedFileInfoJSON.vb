﻿Imports Microsoft.VisualBasic

Namespace TeleLoader.MDMRequestResponse
    Public Class DownloadedFileInfoReq
        Public Property operacion As String = ""
        Public Property login As String = ""
        Public Property pass As String = ""
        Public Property grupo As String = ""
        Public Property id_device As String = ""
        Public Property apk As String = ""
        Public Property configs As String = ""
        Public Property imagenes As String = ""
        Public Property ip As String = ""
    End Class

    Public Class DownloadedFileInfoResp
        Public Property res As String = ""
        Public Property groupName As String = ""
    End Class

End Namespace

