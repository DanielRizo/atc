﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic

Namespace TeleLoader.EMV

    Public Class EMV_Config

        Private strConnString As String
        Private intEMVConfigId As Integer
        Private intTerminalId As Integer
        Private strType As String
        Private strConfig As String
        Private strThresholdRS As String
        Private strTargetRS As String
        Private strMaxTargetRS As String
        Private strTACDenial As String
        Private strTACOnline As String
        Private strTACDefault As String
        Private strApplicationConfig As String

        Public Sub New()

            Dim configurationSection As ConnectionStringsSection = _
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

            strConnString = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString

        End Sub

        Public Sub New(ByVal strConnString As String, ByVal emvConfigId As Integer, ByVal terminalID As Integer)
            Me.strConnString = strConnString
            Me.intEMVConfigId = emvConfigId
            Me.intTerminalId = terminalID
        End Sub

        ''' <summary>
        ''' EMV Type
        ''' </summary>
        Public Property Type() As String
            Get
                Return strType
            End Get
            Set(ByVal value As String)
                strType = value
            End Set
        End Property

        ''' <summary>
        ''' EMV Config
        ''' </summary>
        Public Property Config() As String
            Get
                Return strConfig
            End Get
            Set(ByVal value As String)
                strConfig = value
            End Set
        End Property

        ''' <summary>
        ''' Threshold for biased random selection
        ''' </summary>
        Public Property ThresholdRS() As String
            Get
                Return strThresholdRS
            End Get
            Set(ByVal value As String)
                strThresholdRS = value
            End Set
        End Property

        ''' <summary>
        ''' Target for Random Selection
        ''' </summary>
        Public Property TargetRS() As String
            Get
                Return strTargetRS
            End Get
            Set(ByVal value As String)
                strTargetRS = value
            End Set
        End Property

        ''' <summary>
        ''' Maximum Target for biased random selection
        ''' </summary>
        Public Property MaxTargetRS() As String
            Get
                Return strMaxTargetRS
            End Get
            Set(ByVal value As String)
                strMaxTargetRS = value
            End Set
        End Property

        ''' <summary>
        '''TAC Denial
        ''' </summary>
        Public Property TACDenial() As String
            Get
                Return strTACDenial
            End Get
            Set(ByVal value As String)
                strTACDenial = value
            End Set
        End Property

        ''' <summary>
        ''' TAC Online
        ''' </summary>
        Public Property TACOnline() As String
            Get
                Return strTACOnline
            End Get
            Set(ByVal value As String)
                strTACOnline = value
            End Set
        End Property

        ''' <summary>
        ''' TAC Default
        ''' </summary>
        Public Property TACDefault() As String
            Get
                Return strTACDefault
            End Get
            Set(ByVal value As String)
                strTACDefault = value
            End Set
        End Property

        ''' <summary>
        ''' Application Config
        ''' </summary>
        Public Property ApplicationConfig() As String
            Get
                Return strApplicationConfig
            End Get
            Set(ByVal value As String)
                strApplicationConfig = value
            End Set
        End Property

        ''' <summary>
        ''' Crear EMV Config
        ''' </summary>
        Public Function createEMVConfig() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webCrearConfigEMVTerminal"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("terminalId", intTerminalId))
                command.Parameters.Add(New SqlParameter("emvType", strType))
                command.Parameters.Add(New SqlParameter("emvConfig", strConfig))
                command.Parameters.Add(New SqlParameter("emvThresholdRS", strThresholdRS))
                command.Parameters.Add(New SqlParameter("emvTargetRS", strTargetRS))
                command.Parameters.Add(New SqlParameter("emvMaxTargetRS", strMaxTargetRS))
                command.Parameters.Add(New SqlParameter("emvTACDenial", strTACDenial))
                command.Parameters.Add(New SqlParameter("emvTACOnline", strTACOnline))
                command.Parameters.Add(New SqlParameter("emvTACDefault", strTACDefault))
                command.Parameters.Add(New SqlParameter("emvApplicationConfig", strApplicationConfig))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        ''' <summary>
        ''' Leer Datos de la Configuración EMV
        ''' </summary>
        Public Sub getEMVConfig()
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webConsultarDatosConfigEMVTerminal"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("emvConfigId", intEMVConfigId))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        strType = results.GetString(0)
                        strConfig = results.GetString(1)
                        strThresholdRS = results.GetString(2)
                        strTargetRS = results.GetString(3)
                        strMaxTargetRS = results.GetString(4)
                        strTACDenial = results.GetString(5)
                        strTACOnline = results.GetString(6)
                        strTACDefault = results.GetString(7)
                        strApplicationConfig = results.GetString(8)
                    End While
                Else
                    strType = ""
                    strConfig = ""
                    strThresholdRS = ""
                    strTargetRS = ""
                    strMaxTargetRS = ""
                    strTACDenial = ""
                    strTACOnline = ""
                    strTACDefault = ""
                    strApplicationConfig = ""
                End If

            Catch ex As Exception

            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

        End Sub

        ''' <summary>
        ''' Editar EMV Config
        ''' </summary>
        Public Function editEMVConfig() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webEditarConfigEMVTerminal"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("emvConfigId", intEMVConfigId))
                command.Parameters.Add(New SqlParameter("terminalId", intTerminalId))
                command.Parameters.Add(New SqlParameter("emvType", strType))
                command.Parameters.Add(New SqlParameter("emvConfig", strConfig))
                command.Parameters.Add(New SqlParameter("emvThresholdRS", strThresholdRS))
                command.Parameters.Add(New SqlParameter("emvTargetRS", strTargetRS))
                command.Parameters.Add(New SqlParameter("emvMaxTargetRS", strMaxTargetRS))
                command.Parameters.Add(New SqlParameter("emvTACDenial", strTACDenial))
                command.Parameters.Add(New SqlParameter("emvTACOnline", strTACOnline))
                command.Parameters.Add(New SqlParameter("emvTACDefault", strTACDefault))
                command.Parameters.Add(New SqlParameter("emvApplicationConfig", strApplicationConfig))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

    End Class

    Public Class EMV_Key

        Private strConnString As String
        Private intEMVKeyId As Integer
        Private intTerminalId As Integer
        Private strIndex As String
        Private strApplicationId As String
        Private strExponent As String
        Private strSize As String
        Private strContent As String
        Private strExpiryDate As String
        Private strEffectiveDate As String
        Private strChecksum As String

        Public Sub New()

            Dim configurationSection As ConnectionStringsSection = _
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

            strConnString = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString

        End Sub

        Public Sub New(ByVal strConnString As String, ByVal emvKeyId As Integer, ByVal terminalID As Integer)
            Me.strConnString = strConnString
            Me.intEMVKeyId = emvKeyId
            Me.intTerminalId = terminalID
        End Sub

        ''' <summary>
        ''' CA Key Index
        ''' </summary>
        Public Property Index() As String
            Get
                Return strIndex
            End Get
            Set(ByVal value As String)
                strIndex = value
            End Set
        End Property

        ''' <summary>
        ''' Application ID
        ''' </summary>
        Public Property ApplicationId() As String
            Get
                Return strApplicationId
            End Get
            Set(ByVal value As String)
                strApplicationId = value
            End Set
        End Property

        ''' <summary>
        ''' CA Key Exponent
        ''' </summary>
        Public Property Exponent() As String
            Get
                Return strExponent
            End Get
            Set(ByVal value As String)
                strExponent = value
            End Set
        End Property

        ''' <summary>
        ''' CA Key Size
        ''' </summary>
        Public Property Size() As String
            Get
                Return strSize
            End Get
            Set(ByVal value As String)
                strSize = value
            End Set
        End Property

        ''' <summary>
        ''' CA Key content
        ''' </summary>
        Public Property Content() As String
            Get
                Return strContent
            End Get
            Set(ByVal value As String)
                strContent = value
            End Set
        End Property

        ''' <summary>
        ''' CA Key Expiry Date
        ''' </summary>
        Public Property ExpiryDate() As String
            Get
                Return strExpiryDate
            End Get
            Set(ByVal value As String)
                strExpiryDate = value
            End Set
        End Property

        ''' <summary>
        ''' CA Key Effective Date
        ''' </summary>
        Public Property EffectiveDate() As String
            Get
                Return strEffectiveDate
            End Get
            Set(ByVal value As String)
                strEffectiveDate = value
            End Set
        End Property

        ''' <summary>
        ''' Checksum
        ''' </summary>
        Public Property Checksum() As String
            Get
                Return strChecksum
            End Get
            Set(ByVal value As String)
                strChecksum = value
            End Set
        End Property

        ''' <summary>
        ''' Crear EMV Key
        ''' </summary>
        Public Function createEMVKey() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webCrearKeyEMVTerminal"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("terminalId", intTerminalId))
                command.Parameters.Add(New SqlParameter("keyIndex", strIndex))
                command.Parameters.Add(New SqlParameter("keyApplicationId", strApplicationId))
                command.Parameters.Add(New SqlParameter("keyExponent", strExponent))
                command.Parameters.Add(New SqlParameter("keySize", strSize))
                command.Parameters.Add(New SqlParameter("keyContent", strContent))
                command.Parameters.Add(New SqlParameter("keyExpiryDate", strExpiryDate))
                command.Parameters.Add(New SqlParameter("keyEffectiveDate", strEffectiveDate))
                command.Parameters.Add(New SqlParameter("keyChecksum", strChecksum))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        ''' <summary>
        ''' Leer Datos de la Llave EMV
        ''' </summary>
        Public Sub getEMVKey()
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webConsultarDatosKeyEMVTerminal"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("keyEMVId", intEMVKeyId))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        strIndex = results.GetString(0)
                        strApplicationId = results.GetString(1)
                        strExponent = results.GetString(2)
                        strSize = results.GetString(3)
                        strContent = results.GetString(4)
                        strExpiryDate = results.GetString(5)
                        strEffectiveDate = results.GetString(6)
                        strChecksum = results.GetString(7)
                    End While
                Else
                    strIndex = ""
                    strApplicationId = ""
                    strExponent = ""
                    strSize = ""
                    strContent = ""
                    strExpiryDate = ""
                    strEffectiveDate = ""
                    strChecksum = ""
                End If

            Catch ex As Exception

            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

        End Sub

        ''' <summary>
        ''' Editar EMV Key
        ''' </summary>
        Public Function editEMVKey() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webEditarKeyEMVTerminal"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("emvKeyId", intEMVKeyId))
                command.Parameters.Add(New SqlParameter("terminalId", intTerminalId))
                command.Parameters.Add(New SqlParameter("keyIndex", strIndex))
                command.Parameters.Add(New SqlParameter("keyApplicationId", strApplicationId))
                command.Parameters.Add(New SqlParameter("keyExponent", strExponent))
                command.Parameters.Add(New SqlParameter("keySize", strSize))
                command.Parameters.Add(New SqlParameter("keyContent", strContent))
                command.Parameters.Add(New SqlParameter("keyExpiryDate", strExpiryDate))
                command.Parameters.Add(New SqlParameter("keyEffectiveDate", strEffectiveDate))
                command.Parameters.Add(New SqlParameter("keyChecksum", strChecksum))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

    End Class

End Namespace
