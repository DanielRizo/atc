﻿Imports System.IO
Imports System.Text
Imports System.Security.Cryptography

Namespace TeleLoader.Security

    Public Class Encryptor

#Region "Variables"
        'HardCoded Key
        Public Shared _encryptionkey() As Byte = {&H49, &HB, &H39, &HAA, &HEE, &HB3, &H3A, &HEF, &H2, &H42, &HE1, &HD8, &H2D, &H46, &H7C, &HFF, &H9A, &HB3, &HE1, &HA7, &H45, &HAF, &H69, &HCD}

        'Key for 3DES Encryption
        Private _key() As Byte

        'Init Vector
        Private _iv() As Byte

        'Data Buffer
        Private _data() As Byte

        'Encryptor Object
        Private _tripleDES As TripleDESCryptoServiceProvider

        'Key Sizes
        Private Shared ReadOnly SINGLE_KEY As Integer = 8
        Private Shared ReadOnly DOUBLE_KEY As Integer = 16
        Private Shared ReadOnly TRIPLE_KEY As Integer = 24
#End Region

#Region "Constructor"

        ''' <summary>
        ''' Constructor, inicializa valores para cifrado
        ''' </summary>
        Public Sub New(ByVal key() As Byte, ByVal iv() As Byte, ByVal data() As Byte)

            Me._key = key
            Me._iv = iv
            Me._data = data

        End Sub

        ''' <summary>
        ''' Constructor, inicializa valores para cifrado
        ''' </summary>
        Public Sub New(ByVal data() As Byte)

            Me._data = data

        End Sub
#End Region

#Region "Métodos Utilitarios Públicos"
        ''' <summary>
        ''' Convierte un String con caracteres HEX a un arreglo de Bytes
        ''' </summary>
        Public Shared Function hexStringToByteArray(ByVal text As String) As Byte()
            Dim bytes As Byte() = New Byte(text.Length \ 2 - 1) {}

            For i As Integer = 0 To text.Length - 1 Step 2
                bytes(i \ 2) = Byte.Parse(text(i).ToString() & text(i + 1).ToString(), System.Globalization.NumberStyles.HexNumber)
            Next

            Return bytes
        End Function

        ''' <summary>
        ''' Convierte un String con caracter HEX a un Byte
        ''' </summary>
        Public Shared Function hexStringToByte(ByVal text As String) As Byte
            Dim single_byte As New Byte

            single_byte = Byte.Parse(text, System.Globalization.NumberStyles.HexNumber)

            Return single_byte
        End Function

        ''' <summary>
        ''' Convierte un Byte Array en String HEX
        ''' </summary>
        Public Shared Function byteArrayToHexString(ByVal dataArray() As Byte) As String
            Dim hexString As String = ""
            Dim i As Integer

            For i = 0 To dataArray.Length - 1
                hexString &= Strings.Right("00" & Hex(dataArray(i)), 2)
            Next i

            Return hexString
        End Function

#End Region

#Region "Métodos Públicos"
        ''' <summary>
        ''' Realiza Operación de Encriptación 3DES con los valores inicializados en Constructor
        ''' </summary>
        Public Function doEncryption() As Byte()
            Dim result() As Byte

            Try

                _tripleDES = New TripleDESCryptoServiceProvider()
                _tripleDES.IV = Me._iv
                _tripleDES.Key = Me._key

                'Get Result
                result = _tripleDES.CreateEncryptor().TransformFinalBlock(Me._data, 0, Me._data.Length)

            Catch ex As Exception
                result = Nothing
            End Try

            Return result

        End Function

        ''' <summary>
        ''' Realiza Operación de Desencriptación 3DES con los valores inicializados en Constructor
        ''' </summary>
        Public Function doDecryption() As Byte()
            Dim result() As Byte

            Try

                _tripleDES = New TripleDESCryptoServiceProvider()
                _tripleDES.IV = Me._iv
                _tripleDES.Key = Me._key

                ' Create the stream.
                Dim ms As New System.IO.MemoryStream
                ' Create the decoder to write to the stream.
                Dim decStream As New CryptoStream(ms, _tripleDES.CreateDecryptor(), System.Security.Cryptography.CryptoStreamMode.Write)

                ' Use the crypto stream to write the byte array to the stream.
                decStream.Write(Me._data, 0, Me._data.Length)
                decStream.FlushFinalBlock()

                ' Convert the plaintext stream to a string.
                result = ms.ToArray

            Catch ex As Exception
                result = Nothing
            End Try

            Return result

        End Function


        ''' <summary>
        ''' Calcula el KCV sobre la llave inyectada
        ''' </summary>
        Public Function calculateKCV(ByVal numberOfLeadingBytes As Integer) As Byte()

            Dim result As Byte()
            Dim KCV(numberOfLeadingBytes) As Byte

            'Data to Calculate KCV zeros
            Dim dataKCV() As Byte = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

            'Init Vector
            Dim ivKCV() As Byte = {0, 0, 0, 0, 0, 0, 0, 0}

            Try

                _tripleDES = New TripleDESCryptoServiceProvider()

                'Init Vector Zeros
                _tripleDES.IV = ivKCV
                'Use Data injected as Key for KCV calculation
                _tripleDES.Key = Me._data

                'Get Result
                result = _tripleDES.CreateEncryptor().TransformFinalBlock(dataKCV, 0, dataKCV.Length)

                'Get KCV
                Array.Copy(result, KCV, numberOfLeadingBytes)

            Catch ex As Exception
                KCV = Nothing
            End Try

            Return KCV

        End Function

        ''' <summary>
        ''' Calcula Llave Combinada sobre componentes 1 y 2
        ''' </summary>
        Public Shared Function calculateCombinedKey(ByVal component1 As Byte(), ByVal component2 As Byte()) As Byte()

            Dim result(DOUBLE_KEY - 1) As Byte
            Dim i As Integer

            Try

                If (component1.Length = component2.Length) Then

                    For i = 0 To component1.Length - 1
                        result(i) = component1(i) Xor component2(i)
                    Next i

                Else
                    'Components With Different Size
                    result = Nothing
                End If

            Catch ex As Exception
                result = Nothing
            End Try

            Return result

        End Function
        ''' <summary>
        ''' Calcula Llave Combinada sobre componentes 1, 2 y 3
        ''' </summary>
        Public Shared Function calculateCombinedKey(ByVal component1 As Byte(), ByVal component2 As Byte(), ByVal component3 As Byte()) As Byte()

            Dim result(DOUBLE_KEY) As Byte
            Dim i As Integer

            Try

                If (component1.Length = component2.Length And component1.Length = component3.Length And component2.Length = component3.Length) Then

                    For i = 0 To component1.Length
                        result(i) = component1(i) Xor component1(i) Xor component1(i)
                    Next i

                Else
                    'Components With Different Size
                    result = Nothing
                End If

            Catch ex As Exception
                result = Nothing
            End Try

            Return result

        End Function
#End Region

    End Class

End Namespace
