﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic

Namespace TeleLoader.Applications

    Public Class FileDeployedTerminal

        Private strConnString As String
        Private intDeployedFileID As Integer
        Private intterminalId As Integer
        Private strFilename As String
        Private strPackageName As String
        Private strVersion As String
        Private intDeployTypeID As Integer
        Private strBaseFileName As String

        Public Sub New(ByVal strConnString As String)
            Me.strConnString = strConnString
        End Sub

        Public Sub New(ByVal strConnString As String, ByVal terminalId As Integer)
            Me.strConnString = strConnString
            Me.intterminalId = terminalId
        End Sub

        Public Sub New(ByVal strConnString As String, ByVal terminalId As Integer, ByVal deployedFileId As Integer)
            Me.strConnString = strConnString
            Me.intterminalId = terminalId
            Me.intDeployedFileID = deployedFileId
        End Sub

        Public Sub New()

            Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

            strConnString = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString

        End Sub

        ''' <summary>
        ''' Código del Grupo
        ''' </summary>
        Public Property TerminalCode() As Integer
            Get
                Return intterminalId
            End Get
            Set(ByVal value As Integer)
                intterminalId = value
            End Set
        End Property

        ''' <summary>
        ''' Nombre del Archivo Desplegado
        ''' </summary>
        Public Property Filename() As String
            Get
                Return strFilename
            End Get
            Set(ByVal value As String)
                strFilename = value
            End Set
        End Property

        ''' <summary>
        ''' Nombre del Paquete del APK Desplegado
        ''' </summary>
        Public Property PackageName() As String
            Get
                Return strPackageName
            End Get
            Set(ByVal value As String)
                strPackageName = value
            End Set
        End Property

        ''' <summary>
        ''' Versión del APK Desplegado
        ''' </summary>
        Public Property Version() As String
            Get
                Return strVersion
            End Get
            Set(ByVal value As String)
                strVersion = value
            End Set
        End Property

        ''' <summary>
        ''' Tipo de Despliegue
        ''' </summary>
        Public Property DeployTypeID() As Integer
            Get
                Return intDeployTypeID
            End Get
            Set(ByVal value As Integer)
                intDeployTypeID = value
            End Set
        End Property

        ''' <summary>
        ''' Nombre del Archivo Base (XML's e IMG's)
        ''' </summary>
        Public Property BaseFileName() As String
            Get
                Return strBaseFileName
            End Get
            Set(ByVal value As String)
                strBaseFileName = value
            End Set
        End Property

        ''' <summary>
        ''' Crear Archivo Desplegado en el grupo
        ''' </summary>
        Public Function createDeployedFileTerminalAndroid() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webDesplegarArchivoTerminalAndroid"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("terminalID", intterminalId))
                command.Parameters.Add(New SqlParameter("filename", strFilename))
                command.Parameters.Add(New SqlParameter("packageName", strPackageName))
                command.Parameters.Add(New SqlParameter("version", strVersion))
                command.Parameters.Add(New SqlParameter("deployType", intDeployTypeID))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        ''' <summary>
        ''' Validar nombre de archivo base
        ''' </summary>
        Public Function validateBaseFileName() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webValidarTerminalAndroidArchivoBaseDesplegado"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("terminalID", intterminalId))
                command.Parameters.Add(New SqlParameter("baseFileName", strBaseFileName))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

    End Class

End Namespace
