﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic

Namespace TeleLoader.Groups

    Public Class Group

        Private strConnString As String
        Private intGroupID As Integer
        Private strName As String
        Private strCodeExtern As String
        Private strDesc As String
        Private strEfectivoSi As Boolean
        Private strEfectivoNo As Boolean
        Private strNii_pago As String
        Private strNii_Soat As String
        Private strNii_promociones As String
        Private strIp_Polaris As String
        Private strPort_Polaris As String
        Private strIpIni_Polaris As String
        Private strPortIni_Polaris As String
        Private strTidIni_Polaris As String
        Private strMontoIni_Polaris As String
        Private strActivar_credito As Boolean
        Private strActivar_creditoNo As Boolean
        Private strActivar_Banca_joven_Porcentaje As String
        Private strActivar_Banca_jovenNo As Boolean
        Private strActivar_Banca_jovenSi As Boolean
        Private strActivar_ValidacionSi As Boolean
        Private strActivar_BcpSi As Boolean
        Private strActivar_BcpNo As Boolean
        Private strActivar_BnbSi As Boolean
        Private strActivar_bisaSi As Boolean
        Private strActivar_bisaNo As Boolean
        Private strActivar_AnulacionSi As Boolean
        Private strActivar_AnulacionNo As Boolean
        Private strActivar_DepositoSi As Boolean
        Private strActivar_DepositoNo As Boolean
        Private strActivar_PropinaSi As Boolean
        Private strActivar_PropinaNo As Boolean
        Private strActivar_EchoSi As Boolean
        Private strActivar_EchoNo As Boolean
        Private strActivar_CashSi As Boolean
        Private strActivar_CashNo As Boolean
        Private strActivar_ReservasSi As Boolean
        Private strActivar_ReservasNo As Boolean
        Private strActivar_FidelizacionSi As Boolean
        Private strActivar_FidelizacionNo As Boolean
        Private strActivar_CmbSi As Boolean
        Private strActivar_CmbNo As Boolean
        Private strActivar_CtlSi As Boolean
        Private strActivar_CtlNo As Boolean
        Private strActivar_Logo1Si As Boolean
        Private strActivar_Logo1No As Boolean
        Private strActivar_Logo2Si As Boolean
        Private strActivar_Logo2No As Boolean
        Private strActivar_CajasSi As Boolean
        Private strActivar_CajasNo As Boolean
        Private strActivar_UsbSi As Boolean
        Private strActivar_TcpSi As Boolean
        Private strIpCajas As String
        Private strPuertoCajas As String
        Private strActivar_BinSi As Boolean
        Private strActivar_BinNo As Boolean
        Private strActivar_VoucherSi As Boolean
        Private strActivar_serialSi As Boolean
        Private strActivar_serialNo As Boolean
        Private strActivar_VoucherNo As Boolean
        Private strActivar_SoatSi As Boolean
        Private strActivar_SoatNo As Boolean
        Private strActivar_PromocionesSi As Boolean
        Private strActivar_PromocionesNo As Boolean
        Private strActivar_BnbNo As Boolean
        Private strActivar_ValidacionNo As Boolean
        Private strActivar_InteresesSi As Boolean
        Private strActivar_InteresesNo As Boolean
        Private strCuotas As String
        Private strIPAddress As String
        Private strPort As String
        Private boolActualizar As Boolean
        Private boolProgramarXfecha As Boolean
        Private dateUpdateIni As DateTime
        Private dateUpdateEnd As DateTime
        Private timeUpdateRangeIni As DateTime
        Private timeUpdateRangeEnd As DateTime
        Private intCustomerID As Integer
        'Modificación MDM; EB: 27/Ene/2018
        Private boolAndroidGroup As Boolean = False 'Default Value
        Private strApplicationsDefault As String = False 'Default Value
        'Modificación MDM; EB: 28/Ene/2018
        Private intQueriesNumber As Integer
        Private intFrequency As Integer
        Private strAllowedApplications As String
        'Modificación MDM; EB: 29/Ene/2018
        Private boolChangePasswordTerminals As Boolean
        Private strNewPasswordTerminals As String
        Private strNewMessageTerminals As String
        Private boolLockTerminals As Boolean
        'Modificacion MDM; OG: 7/May/2018
        Private strKioskoApplication As String
        'Modificacion MDM; OG: 7/May/2018
        Private strActualMessage As String
        'Modificacion RD; OG: 7/May/2018
        Private boolBlockingMode As Boolean
        Private boolUpdateNow As Boolean
        Private boolInitialize As Boolean
        Private boolDownloadKey As Boolean
        'Agregado 9/04/2021
        Private RadioFirmaSi As Boolean
        Private RadioFirmaNo As Boolean
        Private RadioDocSi As Boolean
        Private RadioDocNo As Boolean
        Private RadioTelefonoSi As Boolean
        Private RadioTelefonoNo As Boolean
        Private RadioEmisorSi As Boolean
        Private RadioEmisorNo As Boolean


        Private lstTerminalsLocation As New List(Of String())
        '----------------------- Modal

        Private NewstrEfectivoSi As Boolean
        Private NewstrEfectivoNo As Boolean
        Private NewstrNii_pago As String
        Private NewstrNii_Soat As String
        Private NewstrNii_promociones As String
        Private NewstrIp_Polaris As String
        Private NewstrPort_Polaris As String
        Private NewstrIpIni_Polaris As String
        Private NewstrPortIni_Polaris As String
        Private NewstrTidIni_Polaris As String
        Private NewstrMontoIni_Polaris As String
        Private NewstrActivar_credito As Boolean
        Private NewstrActivar_creditoNo As Boolean
        Private NewstrActivar_Banca_joven_Porcentaje As String
        Private NewstrActivar_Banca_jovenNo As Boolean
        Private NewstrActivar_Banca_jovenSi As Boolean
        Private NewstrActivar_ValidacionSi As Boolean
        Private NewstrActivar_BcpSi As Boolean
        Private NewstrActivar_BcpNo As Boolean
        Private NewstrActivar_BnbSi As Boolean
        Private NewstrActivar_bisaSi As Boolean
        Private NewstrActivar_bisaNo As Boolean
        Private NewstrActivar_AnulacionSi As Boolean
        Private NewstrActivar_AnulacionNo As Boolean
        Private NewstrActivar_DepositoSi As Boolean
        Private NewstrActivar_DepositoNo As Boolean
        Private NewstrActivar_PropinaSi As Boolean
        Private NewstrActivar_PropinaNo As Boolean
        Private NewstrActivar_EchoSi As Boolean
        Private NewstrActivar_EchoNo As Boolean
        Private NewstrActivar_CashSi As Boolean
        Private NewstrActivar_CashNo As Boolean
        Private NewstrActivar_ReservasSi As Boolean
        Private NewstrActivar_ReservasNo As Boolean
        Private NewstrActivar_FidelizacionSi As Boolean
        Private NewstrActivar_FidelizacionNo As Boolean
        Private NewstrActivar_CmbSi As Boolean
        Private NewstrActivar_CmbNo As Boolean
        Private NewstrActivar_CtlSi As Boolean
        Private NewstrActivar_CtlNo As Boolean
        Private NewstrActivar_Logo1Si As Boolean
        Private NewstrActivar_Logo1No As Boolean
        Private NewstrActivar_Logo2Si As Boolean
        Private NewstrActivar_Logo2No As Boolean
        Private NewstrActivar_CajasSi As Boolean
        Private NewstrActivar_CajasNo As Boolean
        Private NewstrActivar_UsbSi As Boolean
        Private NewstrActivar_TcpSi As Boolean
        Private NewstrIpCajas As String
        Private NewstrPuertoCajas As String
        Private NewstrActivar_BinSi As Boolean
        Private NewstrActivar_BinNo As Boolean
        Private NewstrActivar_VoucherSi As Boolean
        Private NewstrActivar_serialSi As Boolean
        Private NewstrActivar_serialNo As Boolean
        Private NewstrActivar_VoucherNo As Boolean
        Private NewstrActivar_SoatSi As Boolean
        Private NewstrActivar_SoatNo As Boolean
        Private NewstrActivar_PromocionesSi As Boolean
        Private NewstrActivar_PromocionesNo As Boolean
        Private NewstrActivar_BnbNo As Boolean
        Private NewstrActivar_ValidacionNo As Boolean
        Private NewstrActivar_InteresesSi As Boolean
        Private NewstrActivar_InteresesNo As Boolean
        Private NewstrCuotas As String
        Private NewstrIPAddress As String
        Private NewstrPort As String
        'Agregado 9/04/2021
        Private RadioNewFirmaSi As Boolean
        Private RadioNewFirmaNo As Boolean
        Private RadioNewDocSi As Boolean
        Private RadioNewDocNo As Boolean
        Private RadioNewTelefonoSi As Boolean
        Private RadioNewTelefonoNo As Boolean
        Private RadioNewEmisorSi As Boolean
        Private RadioNewEmisorNo As Boolean
        '------------------------------------ end Modal

        Public Sub New(ByVal strConnString As String)
            Me.strConnString = strConnString
        End Sub

        Public Sub New(ByVal strConnString As String, ByVal groupId As Integer)
            Me.strConnString = strConnString
            Me.intGroupID = groupId
        End Sub

        Public Sub New()

            Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

            strConnString = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString

        End Sub

        ''' <summary>
        ''' Código del Grupo
        ''' </summary>
        Public Property GroupCode() As Integer
            Get
                Return intGroupID
            End Get
            Set(ByVal value As Integer)
                intGroupID = value
            End Set
        End Property

        ''' <summary>
        ''' Nombre del Grupo
        ''' </summary>
        Public Property Name() As String
            Get
                Return strName
            End Get
            Set(ByVal value As String)
                strName = value
            End Set
        End Property
        Public Property EfectivoSi() As Boolean
            Get
                Return strEfectivoSi
            End Get
            Set(ByVal value As Boolean)
                strEfectivoSi = value
            End Set
        End Property
        Public Property EfectivoNo() As Boolean
            Get
                Return strEfectivoNo
            End Get
            Set(ByVal value As Boolean)
                strEfectivoNo = value
            End Set
        End Property
        Public Property NII_Pago() As String
            Get
                Return strNii_pago
            End Get
            Set(ByVal value As String)
                strNii_pago = value
            End Set
        End Property
        Public Property NII_Soat() As String
            Get
                Return strNii_Soat
            End Get
            Set(ByVal value As String)
                strNii_Soat = value
            End Set
        End Property
        Public Property NII_Promociones() As String
            Get
                Return strNii_promociones
            End Get
            Set(ByVal value As String)
                strNii_promociones = value
            End Set
        End Property
        Public Property Ip_Polaris() As String
            Get
                Return strIp_Polaris
            End Get
            Set(ByVal value As String)
                strIp_Polaris = value
            End Set
        End Property
        Public Property Port_Polaris() As String
            Get
                Return strPort_Polaris
            End Get
            Set(ByVal value As String)
                strPort_Polaris = value
            End Set
        End Property
        Public Property ipIni_Polaris() As String
            Get
                Return strIpIni_Polaris
            End Get
            Set(ByVal value As String)
                strIpIni_Polaris = value
            End Set
        End Property
        Public Property PortIni_Polaris() As String
            Get
                Return strPortIni_Polaris
            End Get
            Set(ByVal value As String)
                strPortIni_Polaris = value
            End Set
        End Property
        Public Property TidIni_Polaris() As String
            Get
                Return strTidIni_Polaris
            End Get
            Set(ByVal value As String)
                strTidIni_Polaris = value
            End Set
        End Property
        Public Property MontoIni_Polaris() As String
            Get
                Return strMontoIni_Polaris
            End Get
            Set(ByVal value As String)
                strMontoIni_Polaris = value
            End Set
        End Property
        Public Property Activar_creditoSi() As Boolean
            Get
                Return strActivar_credito
            End Get
            Set(ByVal value As Boolean)
                strActivar_credito = value
            End Set
        End Property
        Public Property Activar_creditoNo() As Boolean
            Get
                Return strActivar_creditoNo
            End Get
            Set(ByVal value As Boolean)
                strActivar_creditoNo = value
            End Set
        End Property
        Public Property Activar_Banca_Joven_Porcentaje() As String
            Get
                Return strActivar_Banca_joven_Porcentaje
            End Get
            Set(ByVal value As String)
                strActivar_Banca_joven_Porcentaje = value
            End Set
        End Property
        Public Property Activar_Banca_JovenNo() As Boolean
            Get
                Return strActivar_Banca_jovenNo
            End Get
            Set(ByVal value As Boolean)
                strActivar_Banca_jovenNo = value
            End Set
        End Property


        Public Property Activar_Banca_JovenSi() As Boolean
            Get
                Return strActivar_Banca_jovenSi
            End Get
            Set(ByVal value As Boolean)
                strActivar_Banca_jovenSi = value
            End Set
        End Property
        Public Property Activar_ValidacionSi() As Boolean
            Get
                Return strActivar_ValidacionSi
            End Get
            Set(ByVal value As Boolean)
                strActivar_ValidacionSi = value
            End Set
        End Property
        Public Property Activar_ValidacionNo() As Boolean
            Get
                Return strActivar_ValidacionNo
            End Get
            Set(ByVal value As Boolean)
                strActivar_ValidacionNo = value
            End Set
        End Property
        Public Property BcpSi() As Boolean
            Get
                Return strActivar_BcpSi
            End Get
            Set(ByVal value As Boolean)
                strActivar_BcpSi = value
            End Set
        End Property
        Public Property BcpNo() As Boolean
            Get
                Return strActivar_BcpNo
            End Get
            Set(ByVal value As Boolean)
                strActivar_BcpNo = value
            End Set
        End Property
        Public Property BnbSi() As Boolean
            Get
                Return strActivar_BnbSi
            End Get
            Set(ByVal value As Boolean)
                strActivar_BnbSi = value
            End Set
        End Property
        Public Property BnbNo() As Boolean
            Get
                Return strActivar_BnbNo
            End Get
            Set(ByVal value As Boolean)
                strActivar_BnbNo = value
            End Set
        End Property
        Public Property BisaSi() As Boolean
            Get
                Return strActivar_bisaSi
            End Get
            Set(ByVal value As Boolean)
                strActivar_bisaSi = value
            End Set
        End Property
        Public Property BisaNo() As Boolean
            Get
                Return strActivar_bisaNo
            End Get
            Set(ByVal value As Boolean)
                strActivar_bisaNo = value
            End Set
        End Property
        Public Property AnulacionSi() As Boolean
            Get
                Return strActivar_AnulacionSi
            End Get
            Set(ByVal value As Boolean)
                strActivar_AnulacionSi = value
            End Set
        End Property
        Public Property AnulacionNo() As Boolean
            Get
                Return strActivar_AnulacionNo
            End Get
            Set(ByVal value As Boolean)
                strActivar_AnulacionNo = value
            End Set
        End Property
        Public Property DepositoSi() As Boolean
            Get
                Return strActivar_DepositoSi
            End Get
            Set(ByVal value As Boolean)
                strActivar_DepositoSi = value
            End Set
        End Property
        Public Property DepositoNo() As Boolean
            Get
                Return strActivar_DepositoNo
            End Get
            Set(ByVal value As Boolean)
                strActivar_DepositoNo = value
            End Set
        End Property
        Public Property PropinaSi() As Boolean
            Get
                Return strActivar_PropinaSi
            End Get
            Set(ByVal value As Boolean)
                strActivar_PropinaSi = value
            End Set
        End Property
        Public Property PropinaNo() As Boolean
            Get
                Return strActivar_PropinaNo
            End Get
            Set(ByVal value As Boolean)
                strActivar_PropinaNo = value
            End Set
        End Property
        Public Property EchoSi() As Boolean
            Get
                Return strActivar_EchoSi
            End Get
            Set(ByVal value As Boolean)
                strActivar_EchoSi = value
            End Set
        End Property
        Public Property EchoNo() As Boolean
            Get
                Return strActivar_EchoNo
            End Get
            Set(ByVal value As Boolean)
                strActivar_EchoNo = value
            End Set
        End Property
        Public Property CashSi() As Boolean
            Get
                Return strActivar_CashSi
            End Get
            Set(ByVal value As Boolean)
                strActivar_CashSi = value
            End Set
        End Property
        Public Property CashNo() As Boolean
            Get
                Return strActivar_CashNo
            End Get
            Set(ByVal value As Boolean)
                strActivar_CashNo = value
            End Set
        End Property
        Public Property ReservasSi() As Boolean
            Get
                Return strActivar_ReservasSi
            End Get
            Set(ByVal value As Boolean)
                strActivar_ReservasSi = value
            End Set
        End Property
        Public Property ReservasNo() As Boolean
            Get
                Return strActivar_ReservasNo
            End Get
            Set(ByVal value As Boolean)
                strActivar_ReservasNo = value
            End Set
        End Property
        Public Property FidelizacionSi() As Boolean
            Get
                Return strActivar_FidelizacionSi
            End Get
            Set(ByVal value As Boolean)
                strActivar_FidelizacionSi = value
            End Set
        End Property
        Public Property FidelizacionNo() As Boolean
            Get
                Return strActivar_FidelizacionNo
            End Get
            Set(ByVal value As Boolean)
                strActivar_FidelizacionNo = value
            End Set
        End Property
        Public Property SoatSi() As Boolean
            Get
                Return strActivar_SoatSi
            End Get
            Set(ByVal value As Boolean)
                strActivar_SoatSi = value
            End Set
        End Property
        Public Property SoatNo() As Boolean
            Get
                Return strActivar_SoatNo
            End Get
            Set(ByVal value As Boolean)
                strActivar_SoatNo = value
            End Set
        End Property
        Public Property PromocionesSi() As Boolean
            Get
                Return strActivar_PromocionesSi
            End Get
            Set(ByVal value As Boolean)
                strActivar_PromocionesSi = value
            End Set
        End Property
        Public Property PromocionesNo() As Boolean
            Get
                Return strActivar_PromocionesNo
            End Get
            Set(ByVal value As Boolean)
                strActivar_PromocionesNo = value
            End Set
        End Property
        Public Property CmbSi() As Boolean
            Get
                Return strActivar_CmbSi
            End Get
            Set(ByVal value As Boolean)
                strActivar_CmbSi = value
            End Set
        End Property
        Public Property CmbNo() As Boolean
            Get
                Return strActivar_CmbNo
            End Get
            Set(ByVal value As Boolean)
                strActivar_CmbNo = value
            End Set
        End Property
        Public Property CTLSi() As Boolean
            Get
                Return strActivar_CtlSi
            End Get
            Set(ByVal value As Boolean)
                strActivar_CtlSi = value
            End Set
        End Property
        Public Property CTLNo() As Boolean
            Get
                Return strActivar_CtlNo
            End Get
            Set(ByVal value As Boolean)
                strActivar_CtlNo = value
            End Set
        End Property
        Public Property Logo1Si() As Boolean
            Get
                Return strActivar_Logo1Si
            End Get
            Set(ByVal value As Boolean)
                strActivar_Logo1Si = value
            End Set
        End Property
        Public Property Logo1No() As Boolean
            Get
                Return strActivar_Logo1No
            End Get
            Set(ByVal value As Boolean)
                strActivar_Logo1No = value
            End Set
        End Property
        Public Property Logo2Si() As Boolean
            Get
                Return strActivar_Logo2Si
            End Get
            Set(ByVal value As Boolean)
                strActivar_Logo2Si = value
            End Set
        End Property
        Public Property Logo2No() As Boolean
            Get
                Return strActivar_Logo2No
            End Get
            Set(ByVal value As Boolean)
                strActivar_Logo2No = value
            End Set
        End Property
        Public Property CajasSi() As Boolean
            Get
                Return strActivar_CajasSi
            End Get
            Set(ByVal value As Boolean)
                strActivar_CajasSi = value
            End Set
        End Property
        Public Property CajasNo() As Boolean
            Get
                Return strActivar_CajasNo
            End Get
            Set(ByVal value As Boolean)
                strActivar_CajasNo = value
            End Set
        End Property
        Public Property USBSi() As Boolean
            Get
                Return strActivar_UsbSi
            End Get
            Set(ByVal value As Boolean)
                strActivar_UsbSi = value
            End Set
        End Property
        Public Property TCPSi() As Boolean
            Get
                Return strActivar_TcpSi
            End Get
            Set(ByVal value As Boolean)
                strActivar_TcpSi = value
            End Set
        End Property
        Public Property IpCajas() As String
            Get
                Return strIpCajas
            End Get
            Set(ByVal value As String)
                strIpCajas = value
            End Set
        End Property
        Public Property PuertoCajas() As String
            Get
                Return strPuertoCajas
            End Get
            Set(ByVal value As String)
                strPuertoCajas = value
            End Set
        End Property
        Public Property BinSi() As Boolean
            Get
                Return strActivar_BinSi
            End Get
            Set(ByVal value As Boolean)
                strActivar_BinSi = value
            End Set
        End Property
        Public Property BinNo() As Boolean
            Get
                Return strActivar_BinNo
            End Get
            Set(ByVal value As Boolean)
                strActivar_BinNo = value
            End Set
        End Property
        Public Property VoucherSi() As Boolean
            Get
                Return strActivar_VoucherSi
            End Get
            Set(ByVal value As Boolean)
                strActivar_VoucherSi = value
            End Set
        End Property
        Public Property serialSi() As Boolean
            Get
                Return strActivar_serialSi
            End Get
            Set(ByVal value As Boolean)
                strActivar_serialSi = value
            End Set
        End Property
        Public Property serialNo() As Boolean
            Get
                Return strActivar_serialNo
            End Get
            Set(ByVal value As Boolean)
                strActivar_serialNo = value
            End Set
        End Property
        Public Property VoucherNo() As Boolean
            Get
                Return strActivar_VoucherNo
            End Get
            Set(ByVal value As Boolean)
                strActivar_VoucherNo = value
            End Set
        End Property
        Public Property Activar_InteresesSi() As Boolean
            Get
                Return strActivar_InteresesSi
            End Get
            Set(ByVal value As Boolean)
                strActivar_InteresesSi = value
            End Set
        End Property
        Public Property Activar_InteresesNo() As Boolean
            Get
                Return strActivar_InteresesNo
            End Get
            Set(ByVal value As Boolean)
                strActivar_InteresesNo = value
            End Set
        End Property
        Public Property Cuotas() As String
            Get
                Return strCuotas
            End Get
            Set(ByVal value As String)
                strCuotas = value
            End Set
        End Property
        Public Property Desc() As String
            Get
                Return strDesc
            End Get
            Set(ByVal value As String)
                strDesc = value
            End Set
        End Property

        ''' <summary>
        ''' IP de Descarga
        ''' </summary>
        Public Property IpAddress() As String
            Get
                Return strIPAddress
            End Get
            Set(ByVal value As String)
                strIPAddress = value
            End Set
        End Property

        ''' <summary>
        ''' Puerto de Descarga
        ''' </summary>
        Public Property Port() As String
            Get
                Return strPort
            End Get
            Set(ByVal value As String)
                strPort = value
            End Set
        End Property

        ''' <summary>
        ''' Actualizar Terminales del grupo ?
        ''' </summary>
        Public Property allowUpdate() As Boolean
            Get
                Return boolActualizar
            End Get
            Set(ByVal value As Boolean)
                boolActualizar = value
            End Set
        End Property

        Public Property BlockingMode() As Boolean
            Get
                Return boolBlockingMode
            End Get
            Set(ByVal value As Boolean)
                boolBlockingMode = value
            End Set
        End Property
        Public Property Initialize() As Boolean
            Get
                Return boolInitialize
            End Get
            Set(ByVal value As Boolean)
                boolInitialize = value
            End Set
        End Property

        Public Property DownloadKey() As Boolean
            Get
                Return boolDownloadKey
            End Get
            Set(ByVal value As Boolean)
                boolDownloadKey = value
            End Set
        End Property

        Public Property UpdateNow() As Boolean
            Get
                Return boolUpdateNow
            End Get
            Set(ByVal value As Boolean)
                boolUpdateNow = value
            End Set
        End Property

        ''' <summary>
        ''' Usar rango con fechas ?
        ''' </summary>
        Public Property useDateRange() As Boolean
            Get
                Return boolProgramarXfecha
            End Get
            Set(ByVal value As Boolean)
                boolProgramarXfecha = value
            End Set
        End Property

        ''' <summary>
        ''' Fecha Inicial - Rango
        ''' </summary>
        Public Property RangeDateIni() As DateTime
            Get
                Return dateUpdateIni
            End Get
            Set(ByVal value As DateTime)
                dateUpdateIni = value
            End Set
        End Property

        ''' <summary>
        ''' Fecha Final - Rango
        ''' </summary>
        Public Property RangeDateEnd() As DateTime
            Get
                Return dateUpdateEnd
            End Get
            Set(ByVal value As DateTime)
                dateUpdateEnd = value
            End Set
        End Property

        ''' <summary>
        ''' Hora Inicial - Rango
        ''' </summary>
        Public Property RangeHourIni() As DateTime
            Get
                Return timeUpdateRangeIni
            End Get
            Set(ByVal value As DateTime)
                timeUpdateRangeIni = value
            End Set
        End Property

        ''' <summary>
        ''' Hora Final - Rango
        ''' </summary>
        Public Property RangeHourEnd() As DateTime
            Get
                Return timeUpdateRangeEnd
            End Get
            Set(ByVal value As DateTime)
                timeUpdateRangeEnd = value
            End Set
        End Property

        ''' <summary>
        ''' Código del Cliente
        ''' </summary>
        Public Property CustomerCode() As Integer
            Get
                Return intCustomerID
            End Get
            Set(ByVal value As Integer)
                intCustomerID = value
            End Set
        End Property

        ''' <summary>
        ''' Lista Ubicación Terminales
        ''' </summary>
        Public ReadOnly Property TerminalsLocation() As List(Of String())
            Get
                Return lstTerminalsLocation
            End Get
        End Property

        'Modificación MDM; EB: 27/Ene/2018
        ''' <summary>
        ''' Es grupo tipo Android ?
        ''' </summary>
        Public Property AndroidGroup() As Boolean
            Get
                Return boolAndroidGroup
            End Get
            Set(ByVal value As Boolean)
                boolAndroidGroup = value
            End Set
        End Property

        'Modificación MDM; EB: 28/Ene/2018
        ''' <summary>
        ''' Aplicaciones por Defecto Grupo Android
        ''' </summary>
        Public Property ApplicationsDefault() As String
            Get
                Return strApplicationsDefault
            End Get
            Set(ByVal value As String)
                strApplicationsDefault = value
            End Set
        End Property

        'Modificación MDM; EB: 28/Ene/2018
        ''' <summary>
        ''' Número de Consultas
        ''' </summary>
        Public Property QueriesNumber() As Integer
            Get
                Return intQueriesNumber
            End Get
            Set(ByVal value As Integer)
                intQueriesNumber = value
            End Set
        End Property

        'Modificación MDM; EB: 28/Ene/2018
        ''' <summary>
        ''' Frecuencia de Consultas
        ''' </summary>
        Public Property Frequency() As Integer
            Get
                Return intFrequency
            End Get
            Set(ByVal value As Integer)
                intFrequency = value
            End Set
        End Property

        'Modificación MDM; EB: 28/Ene/2018
        ''' <summary>
        ''' Aplicaciones Permitidas
        ''' </summary>
        Public Property AllowedApplications() As String
            Get
                Return strAllowedApplications
            End Get
            Set(ByVal value As String)
                strAllowedApplications = value
            End Set
        End Property

        'Modificación MDM; OG: 7/May/2018
        ''' <summary>
        ''' Aplicaciones Kiosko
        ''' </summary>
        Public Property KioskoApplication() As String
            Get
                Return strKioskoApplication
            End Get
            Set(ByVal value As String)
                strKioskoApplication = value
            End Set
        End Property

        'Modificación MDM; EB: 29/Ene/2018
        ''' <summary>
        ''' Cambiar Clave a Terminales ?
        ''' </summary>
        Public Property ChangePasswordTerminals() As Boolean
            Get
                Return boolChangePasswordTerminals
            End Get
            Set(ByVal value As Boolean)
                boolChangePasswordTerminals = value
            End Set
        End Property

        'Modificación MDM; EB: 29/Ene/2018
        ''' <summary>
        ''' Nueva clave para terminales
        ''' </summary>
        Public Property NewPasswordTerminals() As String
            Get
                Return strNewPasswordTerminals
            End Get
            Set(ByVal value As String)
                strNewPasswordTerminals = value
            End Set
        End Property

        'Modificación MDM; EB: 29/Ene/2018
        ''' <summary>
        ''' Nuevo mensaje a desplegar en terminales
        ''' </summary>
        Public Property NewMessageTerminals() As String
            Get
                Return strNewMessageTerminals
            End Get
            Set(ByVal value As String)
                strNewMessageTerminals = value
            End Set
        End Property

        'Modificación MDM; OG: 7/May/2018
        ''' <summary>
        ''' Mensaje actual para el terminal
        ''' </summary>
        Public Property ActualMessage() As String
            Get
                Return strActualMessage
            End Get
            Set(ByVal value As String)
                strActualMessage = value
            End Set
        End Property

        'Modificación MDM; EB: 29/Ene/2018
        ''' <summary>
        ''' Bloquear Terminales ?
        ''' </summary>
        Public Property LockTerminals() As Boolean
            Get
                Return boolLockTerminals
            End Get
            Set(ByVal value As Boolean)
                boolLockTerminals = value
            End Set
        End Property

        Public Property NewstrEfectivoSi1 As Boolean
            Get
                Return NewstrEfectivoSi
            End Get
            Set(value As Boolean)
                NewstrEfectivoSi = value
            End Set
        End Property

        Public Property NewstrEfectivoNo1 As Boolean
            Get
                Return NewstrEfectivoNo
            End Get
            Set(value As Boolean)
                NewstrEfectivoNo = value
            End Set
        End Property

        Public Property NewstrNii_pago1 As String
            Get
                Return NewstrNii_pago
            End Get
            Set(value As String)
                NewstrNii_pago = value
            End Set
        End Property

        Public Property NewstrNii_Soat1 As String
            Get
                Return NewstrNii_Soat
            End Get
            Set(value As String)
                NewstrNii_Soat = value
            End Set
        End Property

        Public Property NewstrNii_promociones1 As String
            Get
                Return NewstrNii_promociones
            End Get
            Set(value As String)
                NewstrNii_promociones = value
            End Set
        End Property

        Public Property NewstrIp_Polaris1 As String
            Get
                Return NewstrIp_Polaris
            End Get
            Set(value As String)
                NewstrIp_Polaris = value
            End Set
        End Property

        Public Property NewstrPort_Polaris1 As String
            Get
                Return NewstrPort_Polaris
            End Get
            Set(value As String)
                NewstrPort_Polaris = value
            End Set
        End Property

        Public Property NewstrIpIni_Polaris1 As String
            Get
                Return NewstrIpIni_Polaris
            End Get
            Set(value As String)
                NewstrIpIni_Polaris = value
            End Set
        End Property

        Public Property NewstrPortIni_Polaris1 As String
            Get
                Return NewstrPortIni_Polaris
            End Get
            Set(value As String)
                NewstrPortIni_Polaris = value
            End Set
        End Property

        Public Property NewstrTidIni_Polaris1 As String
            Get
                Return NewstrTidIni_Polaris
            End Get
            Set(value As String)
                NewstrTidIni_Polaris = value
            End Set
        End Property

        Public Property NewstrMontoIni_Polaris1 As String
            Get
                Return NewstrMontoIni_Polaris
            End Get
            Set(value As String)
                NewstrMontoIni_Polaris = value
            End Set
        End Property

        Public Property NewstrActivar_credito1 As Boolean
            Get
                Return NewstrActivar_credito
            End Get
            Set(value As Boolean)
                NewstrActivar_credito = value
            End Set
        End Property

        Public Property NewstrActivar_creditoNo1 As Boolean
            Get
                Return NewstrActivar_creditoNo
            End Get
            Set(value As Boolean)
                NewstrActivar_creditoNo = value
            End Set
        End Property

        Public Property NewstrActivar_Banca_joven_Porcentaje1 As String
            Get
                Return NewstrActivar_Banca_joven_Porcentaje
            End Get
            Set(value As String)
                NewstrActivar_Banca_joven_Porcentaje = value
            End Set
        End Property

        Public Property NewstrActivar_Banca_jovenNo1 As Boolean
            Get
                Return NewstrActivar_Banca_jovenNo
            End Get
            Set(value As Boolean)
                NewstrActivar_Banca_jovenNo = value
            End Set
        End Property

        Public Property NewstrActivar_Banca_jovenSi1 As Boolean
            Get
                Return NewstrActivar_Banca_jovenSi
            End Get
            Set(value As Boolean)
                NewstrActivar_Banca_jovenSi = value
            End Set
        End Property

        Public Property NewstrActivar_ValidacionSi1 As Boolean
            Get
                Return NewstrActivar_ValidacionSi
            End Get
            Set(value As Boolean)
                NewstrActivar_ValidacionSi = value
            End Set
        End Property

        Public Property NewstrActivar_BcpSi1 As Boolean
            Get
                Return NewstrActivar_BcpSi
            End Get
            Set(value As Boolean)
                NewstrActivar_BcpSi = value
            End Set
        End Property

        Public Property NewstrActivar_BcpNo1 As Boolean
            Get
                Return NewstrActivar_BcpNo
            End Get
            Set(value As Boolean)
                NewstrActivar_BcpNo = value
            End Set
        End Property

        Public Property NewstrActivar_BnbSi1 As Boolean
            Get
                Return NewstrActivar_BnbSi
            End Get
            Set(value As Boolean)
                NewstrActivar_BnbSi = value
            End Set
        End Property

        Public Property NewstrActivar_bisaSi1 As Boolean
            Get
                Return NewstrActivar_bisaSi
            End Get
            Set(value As Boolean)
                NewstrActivar_bisaSi = value
            End Set
        End Property

        Public Property NewstrActivar_bisaNo1 As Boolean
            Get
                Return NewstrActivar_bisaNo
            End Get
            Set(value As Boolean)
                NewstrActivar_bisaNo = value
            End Set
        End Property

        Public Property NewstrActivar_AnulacionSi1 As Boolean
            Get
                Return NewstrActivar_AnulacionSi
            End Get
            Set(value As Boolean)
                NewstrActivar_AnulacionSi = value
            End Set
        End Property

        Public Property NewstrActivar_AnulacionNo1 As Boolean
            Get
                Return NewstrActivar_AnulacionNo
            End Get
            Set(value As Boolean)
                NewstrActivar_AnulacionNo = value
            End Set
        End Property

        Public Property NewstrActivar_DepositoSi1 As Boolean
            Get
                Return NewstrActivar_DepositoSi
            End Get
            Set(value As Boolean)
                NewstrActivar_DepositoSi = value
            End Set
        End Property

        Public Property NewstrActivar_DepositoNo1 As Boolean
            Get
                Return NewstrActivar_DepositoNo
            End Get
            Set(value As Boolean)
                NewstrActivar_DepositoNo = value
            End Set
        End Property

        Public Property NewstrActivar_PropinaSi1 As Boolean
            Get
                Return NewstrActivar_PropinaSi
            End Get
            Set(value As Boolean)
                NewstrActivar_PropinaSi = value
            End Set
        End Property

        Public Property NewstrActivar_PropinaNo1 As Boolean
            Get
                Return NewstrActivar_PropinaNo
            End Get
            Set(value As Boolean)
                NewstrActivar_PropinaNo = value
            End Set
        End Property

        Public Property NewstrActivar_EchoSi1 As Boolean
            Get
                Return NewstrActivar_EchoSi
            End Get
            Set(value As Boolean)
                NewstrActivar_EchoSi = value
            End Set
        End Property

        Public Property NewstrActivar_EchoNo1 As Boolean
            Get
                Return NewstrActivar_EchoNo
            End Get
            Set(value As Boolean)
                NewstrActivar_EchoNo = value
            End Set
        End Property

        Public Property NewstrActivar_CashSi1 As Boolean
            Get
                Return NewstrActivar_CashSi
            End Get
            Set(value As Boolean)
                NewstrActivar_CashSi = value
            End Set
        End Property

        Public Property NewstrActivar_CashNo1 As Boolean
            Get
                Return NewstrActivar_CashNo
            End Get
            Set(value As Boolean)
                NewstrActivar_CashNo = value
            End Set
        End Property

        Public Property NewstrActivar_ReservasSi1 As Boolean
            Get
                Return NewstrActivar_ReservasSi
            End Get
            Set(value As Boolean)
                NewstrActivar_ReservasSi = value
            End Set
        End Property

        Public Property NewstrActivar_ReservasNo1 As Boolean
            Get
                Return NewstrActivar_ReservasNo
            End Get
            Set(value As Boolean)
                NewstrActivar_ReservasNo = value
            End Set
        End Property

        Public Property NewstrActivar_FidelizacionSi1 As Boolean
            Get
                Return NewstrActivar_FidelizacionSi
            End Get
            Set(value As Boolean)
                NewstrActivar_FidelizacionSi = value
            End Set
        End Property

        Public Property NewstrActivar_FidelizacionNo1 As Boolean
            Get
                Return NewstrActivar_FidelizacionNo
            End Get
            Set(value As Boolean)
                NewstrActivar_FidelizacionNo = value
            End Set
        End Property

        Public Property NewstrActivar_CmbSi1 As Boolean
            Get
                Return NewstrActivar_CmbSi
            End Get
            Set(value As Boolean)
                NewstrActivar_CmbSi = value
            End Set
        End Property

        Public Property NewstrActivar_CmbNo1 As Boolean
            Get
                Return NewstrActivar_CmbNo
            End Get
            Set(value As Boolean)
                NewstrActivar_CmbNo = value
            End Set
        End Property

        Public Property NewstrActivar_CtlSi1 As Boolean
            Get
                Return NewstrActivar_CtlSi
            End Get
            Set(value As Boolean)
                NewstrActivar_CtlSi = value
            End Set
        End Property

        Public Property NewstrActivar_CtlNo1 As Boolean
            Get
                Return NewstrActivar_CtlNo
            End Get
            Set(value As Boolean)
                NewstrActivar_CtlNo = value
            End Set
        End Property

        Public Property NewstrActivar_Logo1Si1 As Boolean
            Get
                Return NewstrActivar_Logo1Si
            End Get
            Set(value As Boolean)
                NewstrActivar_Logo1Si = value
            End Set
        End Property

        Public Property NewstrActivar_Logo1No1 As Boolean
            Get
                Return NewstrActivar_Logo1No
            End Get
            Set(value As Boolean)
                NewstrActivar_Logo1No = value
            End Set
        End Property

        Public Property NewstrActivar_Logo2Si1 As Boolean
            Get
                Return NewstrActivar_Logo2Si
            End Get
            Set(value As Boolean)
                NewstrActivar_Logo2Si = value
            End Set
        End Property

        Public Property NewstrActivar_Logo2No1 As Boolean
            Get
                Return NewstrActivar_Logo2No
            End Get
            Set(value As Boolean)
                NewstrActivar_Logo2No = value
            End Set
        End Property

        Public Property NewstrActivar_CajasSi1 As Boolean
            Get
                Return NewstrActivar_CajasSi
            End Get
            Set(value As Boolean)
                NewstrActivar_CajasSi = value
            End Set
        End Property

        Public Property NewstrActivar_CajasNo1 As Boolean
            Get
                Return NewstrActivar_CajasNo
            End Get
            Set(value As Boolean)
                NewstrActivar_CajasNo = value
            End Set
        End Property

        Public Property NewstrActivar_UsbSi1 As Boolean
            Get
                Return NewstrActivar_UsbSi
            End Get
            Set(value As Boolean)
                NewstrActivar_UsbSi = value
            End Set
        End Property

        Public Property NewstrActivar_TcpSi1 As Boolean
            Get
                Return NewstrActivar_TcpSi
            End Get
            Set(value As Boolean)
                NewstrActivar_TcpSi = value
            End Set
        End Property

        Public Property NewstrIpCajas1 As String
            Get
                Return NewstrIpCajas
            End Get
            Set(value As String)
                NewstrIpCajas = value
            End Set
        End Property

        Public Property NewstrPuertoCajas1 As String
            Get
                Return NewstrPuertoCajas
            End Get
            Set(value As String)
                NewstrPuertoCajas = value
            End Set
        End Property

        Public Property NewstrActivar_BinSi1 As Boolean
            Get
                Return NewstrActivar_BinSi
            End Get
            Set(value As Boolean)
                NewstrActivar_BinSi = value
            End Set
        End Property

        Public Property NewstrActivar_BinNo1 As Boolean
            Get
                Return NewstrActivar_BinNo
            End Get
            Set(value As Boolean)
                NewstrActivar_BinNo = value
            End Set
        End Property

        Public Property NewstrActivar_VoucherSi1 As Boolean
            Get
                Return NewstrActivar_VoucherSi
            End Get
            Set(value As Boolean)
                NewstrActivar_VoucherSi = value
            End Set
        End Property

        Public Property NewstrActivar_serialSi1 As Boolean
            Get
                Return NewstrActivar_serialSi
            End Get
            Set(value As Boolean)
                NewstrActivar_serialSi = value
            End Set
        End Property

        Public Property NewstrActivar_serialNo1 As Boolean
            Get
                Return NewstrActivar_serialNo
            End Get
            Set(value As Boolean)
                NewstrActivar_serialNo = value
            End Set
        End Property

        Public Property NewstrActivar_VoucherNo1 As Boolean
            Get
                Return NewstrActivar_VoucherNo
            End Get
            Set(value As Boolean)
                NewstrActivar_VoucherNo = value
            End Set
        End Property

        Public Property NewstrActivar_SoatSi1 As Boolean
            Get
                Return NewstrActivar_SoatSi
            End Get
            Set(value As Boolean)
                NewstrActivar_SoatSi = value
            End Set
        End Property

        Public Property NewstrActivar_SoatNo1 As Boolean
            Get
                Return NewstrActivar_SoatNo
            End Get
            Set(value As Boolean)
                NewstrActivar_SoatNo = value
            End Set
        End Property

        Public Property NewstrActivar_PromocionesSi1 As Boolean
            Get
                Return NewstrActivar_PromocionesSi
            End Get
            Set(value As Boolean)
                NewstrActivar_PromocionesSi = value
            End Set
        End Property

        Public Property NewstrActivar_PromocionesNo1 As Boolean
            Get
                Return NewstrActivar_PromocionesNo
            End Get
            Set(value As Boolean)
                NewstrActivar_PromocionesNo = value
            End Set
        End Property

        Public Property NewstrActivar_BnbNo1 As Boolean
            Get
                Return NewstrActivar_BnbNo
            End Get
            Set(value As Boolean)
                NewstrActivar_BnbNo = value
            End Set
        End Property

        Public Property NewstrActivar_ValidacionNo1 As Boolean
            Get
                Return NewstrActivar_ValidacionNo
            End Get
            Set(value As Boolean)
                NewstrActivar_ValidacionNo = value
            End Set
        End Property

        Public Property NewstrActivar_InteresesSi1 As Boolean
            Get
                Return NewstrActivar_InteresesSi
            End Get
            Set(value As Boolean)
                NewstrActivar_InteresesSi = value
            End Set
        End Property

        Public Property NewstrActivar_InteresesNo1 As Boolean
            Get
                Return NewstrActivar_InteresesNo
            End Get
            Set(value As Boolean)
                NewstrActivar_InteresesNo = value
            End Set
        End Property

        Public Property NewstrCuotas1 As String
            Get
                Return NewstrCuotas
            End Get
            Set(value As String)
                NewstrCuotas = value
            End Set
        End Property

        Public Property NewstrIPAddress1 As String
            Get
                Return NewstrIPAddress
            End Get
            Set(value As String)
                NewstrIPAddress = value
            End Set
        End Property

        Public Property NewstrPort1 As String
            Get
                Return NewstrPort
            End Get
            Set(value As String)
                NewstrPort = value
            End Set
        End Property

        Public Property RadioNewFirmaSi1 As Boolean
            Get
                Return RadioNewFirmaSi
            End Get
            Set(value As Boolean)
                RadioNewFirmaSi = value
            End Set
        End Property

        Public Property RadioNewFirmaNo1 As Boolean
            Get
                Return RadioNewFirmaNo
            End Get
            Set(value As Boolean)
                RadioNewFirmaNo = value
            End Set
        End Property

        Public Property RadioNewDocSi1 As Boolean
            Get
                Return RadioNewDocSi
            End Get
            Set(value As Boolean)
                RadioNewDocSi = value
            End Set
        End Property

        Public Property RadioNewDocNo1 As Boolean
            Get
                Return RadioNewDocNo
            End Get
            Set(value As Boolean)
                RadioNewDocNo = value
            End Set
        End Property

        Public Property RadioNewTelefonoSi1 As Boolean
            Get
                Return RadioNewTelefonoSi
            End Get
            Set(value As Boolean)
                RadioNewTelefonoSi = value
            End Set
        End Property

        Public Property RadioNewTelefonoNo1 As Boolean
            Get
                Return RadioNewTelefonoNo
            End Get
            Set(value As Boolean)
                RadioNewTelefonoNo = value
            End Set
        End Property

        Public Property RadioNewEmisorSi1 As Boolean
            Get
                Return RadioNewEmisorSi
            End Get
            Set(value As Boolean)
                RadioNewEmisorSi = value
            End Set
        End Property

        Public Property RadioNewEmisorNo1 As Boolean
            Get
                Return RadioNewEmisorNo
            End Get
            Set(value As Boolean)
                RadioNewEmisorNo = value
            End Set
        End Property

        Public Property RadioFirmaSi1 As Boolean
            Get
                Return RadioFirmaSi
            End Get
            Set(value As Boolean)
                RadioFirmaSi = value
            End Set
        End Property

        Public Property RadioFirmaNo1 As Boolean
            Get
                Return RadioFirmaNo
            End Get
            Set(value As Boolean)
                RadioFirmaNo = value
            End Set
        End Property

        Public Property RadioDocSi1 As Boolean
            Get
                Return RadioDocSi
            End Get
            Set(value As Boolean)
                RadioDocSi = value
            End Set
        End Property

        Public Property RadioDocNo1 As Boolean
            Get
                Return RadioDocNo
            End Get
            Set(value As Boolean)
                RadioDocNo = value
            End Set
        End Property

        Public Property RadioTelefonoSi1 As Boolean
            Get
                Return RadioTelefonoSi
            End Get
            Set(value As Boolean)
                RadioTelefonoSi = value
            End Set
        End Property

        Public Property RadioTelefonoNo1 As Boolean
            Get
                Return RadioTelefonoNo
            End Get
            Set(value As Boolean)
                RadioTelefonoNo = value
            End Set
        End Property

        Public Property RadioEmisorSi1 As Boolean
            Get
                Return RadioEmisorSi
            End Get
            Set(value As Boolean)
                RadioEmisorSi = value
            End Set
        End Property

        Public Property RadioEmisorNo1 As Boolean
            Get
                Return RadioEmisorNo
            End Get
            Set(value As Boolean)
                RadioEmisorNo = value
            End Set
        End Property

        'Modificación MDM; EB: 27/Ene/2018
        ''' <summary>
        ''' Crear Grupo
        ''' </summary>
        Public Function createGroup() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webCrearGrupo"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("groupName", strName))
                'command.Parameters.Add(New SqlParameter("CodeExtern", strCodeExtern))
                command.Parameters.Add(New SqlParameter("groupDesc", strDesc))
                command.Parameters.Add(New SqlParameter("groupIP", strIPAddress))
                command.Parameters.Add(New SqlParameter("groupPort", strPort))
                command.Parameters.Add(New SqlParameter("allowUpdate", boolActualizar))
                command.Parameters.Add(New SqlParameter("useDateRange", boolProgramarXfecha))
                command.Parameters.Add(New SqlParameter("RangeDateIni", dateUpdateIni))
                command.Parameters.Add(New SqlParameter("RangeDateEnd", dateUpdateEnd))
                command.Parameters.Add(New SqlParameter("RangeHourIni", timeUpdateRangeIni))
                command.Parameters.Add(New SqlParameter("RangeHourEnd", timeUpdateRangeEnd))
                command.Parameters.Add(New SqlParameter("customerID", intCustomerID))
                command.Parameters.Add(New SqlParameter("blockingMode", boolBlockingMode))
                command.Parameters.Add(New SqlParameter("updateNow", boolUpdateNow))
                command.Parameters.Add(New SqlParameter("isAndroidGroup", boolAndroidGroup))
                command.Parameters.Add(New SqlParameter("defaultApplications", strApplicationsDefault))
                command.Parameters.Add(New SqlParameter("numberOfQueries", intQueriesNumber))
                command.Parameters.Add(New SqlParameter("frequency", intFrequency))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function
        'Modificación MDM; EB: 27/Ene/2018
        ''' <summary>
        ''' Crear Grupo
        ''' </summary>
        Public Function createGroupAndroid() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webCrearGrupoAndroid"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("groupName", strName))
                command.Parameters.Add(New SqlParameter("groupDesc", strDesc))
                command.Parameters.Add(New SqlParameter("groupIP", strIPAddress))
                command.Parameters.Add(New SqlParameter("groupPort", strPort))
                command.Parameters.Add(New SqlParameter("allowUpdate", boolActualizar))
                command.Parameters.Add(New SqlParameter("useDateRange", boolProgramarXfecha))
                command.Parameters.Add(New SqlParameter("RangeDateIni", dateUpdateIni))
                command.Parameters.Add(New SqlParameter("RangeDateEnd", dateUpdateEnd))
                command.Parameters.Add(New SqlParameter("RangeHourIni", timeUpdateRangeIni))
                command.Parameters.Add(New SqlParameter("RangeHourEnd", timeUpdateRangeEnd))
                command.Parameters.Add(New SqlParameter("customerID", intCustomerID))
                command.Parameters.Add(New SqlParameter("blockingMode", boolBlockingMode))
                command.Parameters.Add(New SqlParameter("updateNow", boolUpdateNow))
                command.Parameters.Add(New SqlParameter("isAndroidGroup", boolAndroidGroup))
                command.Parameters.Add(New SqlParameter("defaultApplications", strApplicationsDefault))
                command.Parameters.Add(New SqlParameter("numberOfQueries", intQueriesNumber))
                command.Parameters.Add(New SqlParameter("frequency", intFrequency))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        ''' <summary>
        ''' Leer Datos del Grupo
        ''' </summary>
        Public Sub getGroupData()
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webConsultarDatosGrupo"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("groupID", intGroupID))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        strName = results.GetString(0)
                        strDesc = results.GetString(1)
                        'strCodeExtern = results.GetInt64(2)
                        strIPAddress = results.GetString(2)
                        strPort = results.GetString(3)
                        boolBlockingMode = results.GetBoolean(4)
                        'Modificación RD; OG: 27/Octubre/2018 Carga de Flag's en la plataforma Polaris Cloud Service
                        boolUpdateNow = results.GetBoolean(5)
                        boolActualizar = results.GetBoolean(6)
                        boolProgramarXfecha = results.GetBoolean(7)
                        dateUpdateIni = results.GetDateTime(8)
                        dateUpdateEnd = results.GetDateTime(9)
                        timeUpdateRangeIni = results.GetDateTime(10)
                        timeUpdateRangeEnd = results.GetDateTime(11)
                        'Modificación MDM; EB: 28/Ene/2018
                        strAllowedApplications = results.GetString(12)
                        intQueriesNumber = results.GetInt16(13)
                        intFrequency = results.GetInt16(14)
                        'Modificación MDM; OG: 7/May/2018
                        strKioskoApplication = results.GetString(15)
                        strActualMessage = results.GetString(16)
                        boolInitialize = results.GetBoolean(17)
                        boolDownloadKey = results.GetBoolean(18)
                    End While
                Else
                    strName = ""
                    strDesc = ""
                    'strCodeExtern = ""
                    strIPAddress = ""
                    strPort = ""
                    'Modificación RD; OG: 9/May/2018
                    boolBlockingMode = False
                    boolUpdateNow = False
                    boolActualizar = False
                    boolProgramarXfecha = False
                    dateUpdateIni = Nothing
                    dateUpdateEnd = Nothing
                    timeUpdateRangeIni = Nothing
                    timeUpdateRangeEnd = Nothing
                    'Modificación MDM; EB: 28/Ene/2018
                    strAllowedApplications = ""
                    intQueriesNumber = 0
                    intFrequency = 0
                    strActualMessage = ""

                End If

            Catch ex As Exception

            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

        End Sub
        ''' <summary>
        ''' Leer Datos del Android
        ''' </summary>
        Public Sub getGroupDataAndroid()
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webConsultarDatosGrupoAndroid"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("groupID", intGroupID))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        strName = results.GetString(0)
                        strDesc = results.GetString(1)
                        strIPAddress = results.GetString(2)
                        strPort = results.GetString(3)
                        boolBlockingMode = results.GetBoolean(4)
                        'Modificación RD; OG: 27/Octubre/2018 Carga de Flag's en la plataforma Polaris Cloud Service
                        boolUpdateNow = results.GetBoolean(5)
                        boolActualizar = results.GetBoolean(6)
                        boolProgramarXfecha = results.GetBoolean(7)
                        dateUpdateIni = results.GetDateTime(8)
                        dateUpdateEnd = results.GetDateTime(9)
                        timeUpdateRangeIni = results.GetDateTime(10)
                        timeUpdateRangeEnd = results.GetDateTime(11)
                        'Modificación MDM; EB: 28/Ene/2018
                        strAllowedApplications = results.GetString(12)
                        intQueriesNumber = results.GetInt16(13)
                        intFrequency = results.GetInt16(14)
                        'Modificación MDM; OG: 7/May/2018
                        strKioskoApplication = results.GetString(15)
                        strActualMessage = results.GetString(16)
                    End While
                Else
                    strName = ""
                    strDesc = ""
                    strIPAddress = ""
                    strPort = ""
                    'Modificación RD; OG: 9/May/2018
                    boolBlockingMode = False
                    boolUpdateNow = False
                    boolActualizar = False
                    boolProgramarXfecha = False
                    dateUpdateIni = Nothing
                    dateUpdateEnd = Nothing
                    timeUpdateRangeIni = Nothing
                    timeUpdateRangeEnd = Nothing
                    'Modificación MDM; EB: 28/Ene/2018
                    strAllowedApplications = ""
                    intQueriesNumber = 0
                    intFrequency = 0
                    strActualMessage = ""

                End If

            Catch ex As Exception

            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

        End Sub

        ''' <summary>
        ''' Editar Grupo
        ''' </summary>
        Public Function editGroup() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webEditarGrupo"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("groupID", intGroupID))
                command.Parameters.Add(New SqlParameter("groupName", strName))
                command.Parameters.Add(New SqlParameter("groupDesc", strDesc))
                command.Parameters.Add(New SqlParameter("groupIP", strIPAddress))
                command.Parameters.Add(New SqlParameter("groupPort", strPort))
                command.Parameters.Add(New SqlParameter("allowUpdate", boolActualizar))
                command.Parameters.Add(New SqlParameter("useDateRange", boolProgramarXfecha))
                command.Parameters.Add(New SqlParameter("RangeDateIni", dateUpdateIni))
                command.Parameters.Add(New SqlParameter("RangeDateEnd", dateUpdateEnd))
                command.Parameters.Add(New SqlParameter("RangeHourIni", timeUpdateRangeIni))
                command.Parameters.Add(New SqlParameter("RangeHourEnd", timeUpdateRangeEnd))
                command.Parameters.Add(New SqlParameter("blockingMode", boolBlockingMode))
                command.Parameters.Add(New SqlParameter("updateNow", boolUpdateNow))
                command.Parameters.Add(New SqlParameter("Inicializacion", boolInitialize))
                command.Parameters.Add(New SqlParameter("DownloadKey", boolDownloadKey))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        'Modificación MDM; EB: 28/Ene/2018
        ''' <summary>
        ''' Actualizar Parámetros de Consulta
        ''' </summary>
        Public Function SetQueryParameters() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webEditarGrupoAndroidFrecuenciaConsultas"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("groupID", intGroupID))
                command.Parameters.Add(New SqlParameter("numberOfQueries", intQueriesNumber))
                command.Parameters.Add(New SqlParameter("frequency", intFrequency))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        'Modificación MDM; EB: 28/Ene/2018
        ''' <summary>
        ''' Actualizar Aplicaciones Permitidas
        ''' </summary>
        Public Function SetAllowedApps() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webEditarGrupoAndroidAppsPermitidas"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("groupID", intGroupID))
                command.Parameters.Add(New SqlParameter("allowedApps", strAllowedApplications))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function
        'Modificación MDM; EB: 28/Ene/2018
        ''' <summary>
        ''' Actualizar Aplicaciones Permitidas
        ''' </summary>
        Public Function SetAllowedAppsAndroid() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webEditarGrupoAndroidAplicacionesPermitidas"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("groupID", intGroupID))
                command.Parameters.Add(New SqlParameter("allowedApps", strAllowedApplications))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function


        'Modificación MDM; OG: 7/May/2018
        ''' <summary>
        ''' Actualizar Aplicacion Kiosko
        ''' </summary>
        Public Function SetKioskoApp() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webEditarGrupoAndroidAppKiosko"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("groupID", intGroupID))
                command.Parameters.Add(New SqlParameter("kioskoApp", strKioskoApplication))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function
        'Modificación MDM; OG: 7/May/2018
        ''' <summary>
        ''' Actualizar Aplicacion Kiosko
        ''' </summary>
        Public Function SetKioskoAplicacion() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webEditarGrupoAndroidAplicacionKiosko"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("groupID", intGroupID))
                command.Parameters.Add(New SqlParameter("kioskoApp", strKioskoApplication))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function


        ''' <summary>
        ''' Consultar Datos de Ubicación de Terminales del Grupo
        ''' </summary>
        Public Sub getTerminalsLocationData()
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webConsultarUbicacionTerminalesGrupo"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("groupID", intGroupID))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        Dim dataTerm(4) As String
                        dataTerm(0) = results.GetInt64(0).ToString()
                        dataTerm(1) = results.GetString(1)
                        dataTerm(2) = results.GetString(2)
                        dataTerm(3) = results.GetString(3)
                        dataTerm(4) = results.GetString(4)

                        lstTerminalsLocation.Add(dataTerm)
                    End While
                Else
                    lstTerminalsLocation = Nothing
                End If

            Catch ex As Exception

            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

        End Sub

        'Modificación MDM; EB: 28/Ene/2018
        ''' <summary>
        ''' Eliminar Grupo
        ''' </summary>
        Public Function deleteGroup() As Integer
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webEliminarGrupo"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("groupID", intGroupID))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    status = 0
                End If

            Catch ex As Exception
                status = 0
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return status

        End Function

        'Modificación MDM; EB: 28/Ene/2018
        ''' <summary>
        ''' Validar Nombre de Grupo
        ''' </summary>
        Public Function validateGroupName() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webValidarNombreGrupoAndroid"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("groupName", strName))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        'Modificación MDM; EB: 29/Ene/2018
        ''' <summary>
        ''' Ejecutar acciones en terminales
        ''' </summary>
        Public Function ExecuteActionsOnTerminals() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webEditarGrupoAndroidAcciones"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("groupID", intGroupID))
                command.Parameters.Add(New SqlParameter("flagchangePassword", boolChangePasswordTerminals))
                command.Parameters.Add(New SqlParameter("newPassword", strNewPasswordTerminals))
                command.Parameters.Add(New SqlParameter("newMessage", strNewMessageTerminals))
                command.Parameters.Add(New SqlParameter("lockTerminals", boolLockTerminals))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

    End Class

End Namespace
