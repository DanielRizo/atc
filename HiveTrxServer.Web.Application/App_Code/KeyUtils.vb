﻿Imports System
Imports System.Collections.Generic
Imports System.Security.Cryptography
Imports System.IO
Imports System.Globalization
Imports System.Diagnostics

Namespace ExportRSAKey
    Public Class KeyUtils
#Region "PUBLIC KEY TO X509 BLOB"
        Friend Shared Function PublicKeyToX509(ByVal publicKey As RSAParameters) As Byte()

            ' 
            '  SEQUENCE                          * +- SEQUENCE                       *    +- OID                         *    +- Null                        * +- BITSTRING                      *    +- SEQUENCE                    *       +- INTEGER(N)               *       +- INTEGER(E)               * 

            Dim oid As AsnType = CreateOid("1.2.840.113549.1.1.1")
            Dim algorithmID As AsnType = CreateSequence(New AsnType() {oid, CreateNull()})
            Dim n As AsnType = CreateIntegerPos(publicKey.Modulus)
            Dim e As AsnType = CreateIntegerPos(publicKey.Exponent)
            Dim key As AsnType = CreateBitString(CreateSequence(New AsnType() {n, e}))
            Dim publicKeyInfo As AsnType = CreateSequence(New AsnType() {algorithmID, key})
            Return New AsnMessage(publicKeyInfo.GetBytes(), "X.509").GetBytes()
        End Function

#End Region

#Region "PRIVATE KEY TO PKCS8 BLOB"

        Friend Shared Function PrivateKeyToPKCS8(ByVal privateKey As RSAParameters) As Byte()
            Dim n As AsnType = CreateIntegerPos(privateKey.Modulus)
            Dim e As AsnType = CreateIntegerPos(privateKey.Exponent)
            Dim d As AsnType = CreateIntegerPos(privateKey.D)
            Dim p As AsnType = CreateIntegerPos(privateKey.P)
            Dim q As AsnType = CreateIntegerPos(privateKey.Q)
            Dim dp As AsnType = CreateIntegerPos(privateKey.DP)
            Dim dq As AsnType = CreateIntegerPos(privateKey.DQ)
            Dim iq As AsnType = CreateIntegerPos(privateKey.InverseQ)
            Dim version As AsnType = CreateInteger(New Byte() {0})
            Dim key As AsnType = CreateOctetString(CreateSequence(New AsnType() {version, n, e, d, p, q, dp, dq, iq}))
            Dim algorithmID As AsnType = CreateSequence(New AsnType() {CreateOid("1.2.840.113549.1.1.1"), CreateNull()})
            Dim privateKeyInfo As AsnType = CreateSequence(New AsnType() {version, algorithmID, key})
            Return New AsnMessage(privateKeyInfo.GetBytes(), "PKCS#8").GetBytes()
        End Function

#End Region

#Region "X509 PUBLIC KEY BLOB TO RSACRYPTOPROVIDER"
        Friend Shared Function DecodePublicKey(ByVal publicKeyBytes As Byte()) As RSACryptoServiceProvider
            Dim ms As MemoryStream = New MemoryStream(publicKeyBytes)
            Dim rd As BinaryReader = New BinaryReader(ms)
            Dim SeqOID As Byte() = {&H30, &HD, &H6, &H9, &H2A, &H86, &H48, &H86, &HF7, &HD, &H1, &H1, &H1, &H5, &H0}
            Dim seq As Byte() = New Byte(14) {}

            Try
                Dim byteValue As Byte
                Dim shortValue As UShort
                shortValue = rd.ReadUInt16()

                Select Case shortValue
                    Case &H8130
                        rd.ReadByte()
                    Case &H8230
                        rd.ReadInt16()
                    Case Else
                        Return Nothing
                End Select

                seq = rd.ReadBytes(15)
                If Not Helpers.CompareBytearrays(seq, SeqOID) Then Return Nothing
                shortValue = rd.ReadUInt16()

                If shortValue = &H8103 Then
                    rd.ReadByte()
                ElseIf shortValue = &H8203 Then
                    rd.ReadInt16()
                Else
                    Return Nothing
                End If

                byteValue = rd.ReadByte()
                If byteValue <> &H0 Then Return Nothing
                shortValue = rd.ReadUInt16()

                If shortValue = &H8130 Then
                    rd.ReadByte()
                ElseIf shortValue = &H8230 Then
                    rd.ReadInt16()
                Else
                    Return Nothing
                End If

                Dim parms As CspParameters = New CspParameters()
                parms.Flags = CspProviderFlags.NoFlags
                parms.KeyContainerName = Guid.NewGuid().ToString().ToUpperInvariant()
                parms.ProviderType = If(Environment.OSVersion.Version.Major > 5 OrElse Environment.OSVersion.Version.Major = 5 AndAlso Environment.OSVersion.Version.Minor >= 1, &H18, 1)
                Dim rsa As RSACryptoServiceProvider = New RSACryptoServiceProvider(parms)
                Dim rsAparams As RSAParameters = New RSAParameters()
                rsAparams.Modulus = rd.ReadBytes(Helpers.DecodeIntegerSize(rd))
                Dim traits As RSAParameterTraits = New RSAParameterTraits(rsAparams.Modulus.Length * 8)
                rsAparams.Modulus = Helpers.AlignBytes(rsAparams.Modulus, traits.size_Mod)
                rsAparams.Exponent = Helpers.AlignBytes(rd.ReadBytes(Helpers.DecodeIntegerSize(rd)), traits.size_Exp)
                rsa.ImportParameters(rsAparams)
                Return rsa
            Catch __unusedException1__ As Exception
                Return Nothing
            Finally
                rd.Close()
            End Try
        End Function


#End Region

#Region "PKCS8 PRIVATE KEY BLOB TO RSACRYPTOPROVIDER"
        Public Shared Function DecodePrivateKeyInfo(ByVal pkcs8 As Byte()) As RSACryptoServiceProvider
            Dim SeqOID As Byte() = {&H30, &HD, &H6, &H9, &H2A, &H86, &H48, &H86, &HF7, &HD, &H1, &H1, &H1, &H5, &H0}
            Dim seq As Byte() = New Byte(14) {}
            Dim mem As MemoryStream = New MemoryStream(pkcs8)
            Dim lenstream As Integer = mem.Length
            Dim binr As BinaryReader = New BinaryReader(mem)
            Dim bt As Byte = 0
            Dim twobytes As UShort = 0

            Try
                twobytes = binr.ReadUInt16()

                If twobytes = &H8130 Then
                    binr.ReadByte()
                ElseIf twobytes = &H8230 Then
                    binr.ReadInt16()
                Else
                    Return Nothing
                End If

                bt = binr.ReadByte()
                If bt <> &H2 Then Return Nothing
                twobytes = binr.ReadUInt16()
                If twobytes <> &H1 Then Return Nothing
                seq = binr.ReadBytes(15)
                If Not CompareBytearrays(seq, SeqOID) Then Return Nothing
                bt = binr.ReadByte()
                If bt <> &H4 Then Return Nothing
                bt = binr.ReadByte()

                If bt = &H81 Then
                    binr.ReadByte()
                ElseIf bt = &H82 Then
                    binr.ReadUInt16()
                End If

                Dim rsaprivkey As Byte() = binr.ReadBytes(lenstream - mem.Position)
                Dim rsacsp As RSACryptoServiceProvider = DecodeRSAPrivateKey(rsaprivkey)
                Return rsacsp
            Catch __unusedException1__ As Exception
                Return Nothing
            Finally
                binr.Close()
            End Try
        End Function

#End Region

#Region "UTIL CLASSES"
        Private Class Helpers
            Public Shared Function CompareBytearrays(ByVal a As Byte(), ByVal b As Byte()) As Boolean
                If a.Length <> b.Length Then Return False
                Dim i As Integer = 0

                For Each c As Byte In a
                    If c <> b(i) Then Return False
                    i += 1
                Next

                Return True
            End Function

            Public Shared Function AlignBytes(ByVal inputBytes As Byte(), ByVal alignSize As Integer) As Byte()
                Dim inputBytesSize As Integer = inputBytes.Length

                If alignSize <> -1 AndAlso inputBytesSize < alignSize Then
                    Dim buf As Byte() = New Byte(alignSize - 1) {}
                    Dim i As Integer = 0

                    While i < inputBytesSize
                        buf(i + (alignSize - inputBytesSize)) = inputBytes(i)
                        Threading.Interlocked.Increment(i)
                    End While

                    Return buf
                Else
                    Return inputBytes
                End If
            End Function

            Public Shared Function DecodeIntegerSize(ByVal rd As BinaryReader) As Integer
                Dim byteValue As Byte
                Dim count As Integer
                byteValue = rd.ReadByte()
                If byteValue <> &H2 Then Return 0
                byteValue = rd.ReadByte()

                If byteValue = &H81 Then
                    count = rd.ReadByte()
                ElseIf byteValue = &H82 Then
                    Dim hi As Byte = rd.ReadByte()
                    Dim lo As Byte = rd.ReadByte()
                    count = BitConverter.ToUInt16({lo, hi}, 0)
                Else
                    count = byteValue
                End If

                While rd.ReadByte() = &H0
                    count -= 1
                End While

                rd.BaseStream.Seek(-1, SeekOrigin.Current)
                Return count
            End Function
        End Class

        Private Class RSAParameterTraits
            Public Sub New(ByVal modulusLengthInBits As Integer)
                Dim assumedLength As Integer = -1
                Dim logbase As Double = Math.Log(modulusLengthInBits, 2)

                If logbase = CInt(logbase) Then
                    assumedLength = modulusLengthInBits
                Else
                    assumedLength = CInt(logbase + 1.0)
                    assumedLength = CInt(Math.Pow(2, assumedLength))
                    Debug.Assert(False)
                End If

                Select Case assumedLength
                    Case 512
                        size_Mod = &H40
                        size_Exp = -1
                        size_D = &H40
                        size_P = &H20
                        size_Q = &H20
                        size_DP = &H20
                        size_DQ = &H20
                        size_InvQ = &H20
                    Case 1024
                        size_Mod = &H80
                        size_Exp = -1
                        size_D = &H80
                        size_P = &H40
                        size_Q = &H40
                        size_DP = &H40
                        size_DQ = &H40
                        size_InvQ = &H40
                    Case 2048
                        size_Mod = &H100
                        size_Exp = -1
                        size_D = &H100
                        size_P = &H80
                        size_Q = &H80
                        size_DP = &H80
                        size_DQ = &H80
                        size_InvQ = &H80
                    Case 4096
                        size_Mod = &H200
                        size_Exp = -1
                        size_D = &H200
                        size_P = &H100
                        size_Q = &H100
                        size_DP = &H100
                        size_DQ = &H100
                        size_InvQ = &H100
                    Case Else
                        Debug.Assert(False)
                End Select
            End Sub

            Public size_Mod As Integer = -1
            Public size_Exp As Integer = -1
            Public size_D As Integer = -1
            Public size_P As Integer = -1
            Public size_Q As Integer = -1
            Public size_DP As Integer = -1
            Public size_DQ As Integer = -1
            Public size_InvQ As Integer = -1
        End Class

        Private Class AsnMessage
            Private m_octets As Byte()
            Private m_format As String

            Friend ReadOnly Property Length As Integer
                Get

                    If Nothing Is m_octets Then
                        Return 0
                    End If

                    Return m_octets.Length
                End Get
            End Property

            Friend Sub New(ByVal octets As Byte(), ByVal format As String)
                m_octets = octets
                m_format = format
            End Sub

            Friend Function GetBytes() As Byte()
                If Nothing Is m_octets Then
                    Return New Byte() {}
                End If

                Return m_octets
            End Function

            Friend Function GetFormat() As String
                Return m_format
            End Function
        End Class

        Private Class AsnType
            Public Sub New(ByVal tag As Byte, ByVal octet As Byte)
                m_raw = False
                m_tag = New Byte() {tag}
                m_octets = New Byte() {octet}
            End Sub

            Public Sub New(ByVal tag As Byte, ByVal octets As Byte())
                m_raw = False
                m_tag = New Byte() {tag}
                m_octets = octets
            End Sub

            Public Sub New(ByVal tag As Byte, ByVal length As Byte(), ByVal octets As Byte())
                m_raw = True
                m_tag = New Byte() {tag}
                m_length = length
                m_octets = octets
            End Sub

            Private m_raw As Boolean

            Private Property Raw As Boolean
                Get
                    Return m_raw
                End Get
                Set(ByVal value As Boolean)
                    m_raw = value
                End Set
            End Property

            Private m_tag As Byte()

            Public ReadOnly Property Tag As Byte()
                Get
                    If Nothing Is m_tag Then Return EMPTY
                    Return m_tag
                End Get
            End Property

            Private m_length As Byte()

            Public ReadOnly Property Length As Byte()
                Get
                    If Nothing Is m_length Then Return EMPTY
                    Return m_length
                End Get
            End Property

            Private m_octets As Byte()

            Public Property Octets As Byte()
                Get

                    If Nothing Is m_octets Then
                        Return EMPTY
                    End If

                    Return m_octets
                End Get
                Set(ByVal value As Byte())
                    m_octets = value
                End Set
            End Property

            Friend Function GetBytes() As Byte()
                If True = m_raw Then
                    Return Concatenate(New Byte()() {m_tag, m_length, m_octets})
                End If

                SetLength()

                If &H5 = m_tag(0) Then
                    Return Concatenate(New Byte()() {m_tag, m_octets})
                End If

                Return Concatenate(New Byte()() {m_tag, m_length, m_octets})
            End Function

            Private Sub SetLength()
                If Nothing Is m_octets Then
                    m_length = ZERO
                    Return
                End If

                If &H5 = m_tag(0) Then
                    m_length = EMPTY
                    Return
                End If

                Dim length As Byte() = Nothing

                If m_octets.Length < &H80 Then
                    length = New Byte(0) {}
                    length(0) = CByte(m_octets.Length)
                ElseIf m_octets.Length <= &HFF Then
                    length = New Byte(1) {}
                    length(0) = &H81
                    length(1) = CByte(m_octets.Length And &HFF)
                ElseIf m_octets.Length <= &HFFFF Then
                    length = New Byte(2) {}
                    length(0) = &H82
                    length(1) = CByte((m_octets.Length And &HFF00) >> 8)
                    length(2) = CByte(m_octets.Length And &HFF)
                ElseIf m_octets.Length <= &HFFFFFF Then
                    length = New Byte(3) {}
                    length(0) = &H83
                    length(1) = CByte((m_octets.Length And &HFF0000) >> 16)
                    length(2) = CByte((m_octets.Length And &HFF00) >> 8)
                    length(3) = CByte(m_octets.Length And &HFF)
                Else
                    length = New Byte(4) {}
                    length(0) = &H84
                    length(1) = CByte((m_octets.Length And &HFF000000) >> 24)
                    length(2) = CByte((m_octets.Length And &HFF0000) >> 16)
                    length(3) = CByte((m_octets.Length And &HFF00) >> 8)
                    length(4) = CByte(m_octets.Length And &HFF)
                End If

                m_length = length
            End Sub

            Private Function Concatenate(ByVal values As Byte()()) As Byte()
                If IsEmpty(values) Then Return New Byte() {}
                Dim length As Integer = 0

                For Each b As Byte() In values
                    If Nothing IsNot b Then length += b.Length
                Next

                Dim cated As Byte() = New Byte(length - 1) {}
                Dim current As Integer = 0

                For Each b As Byte() In values

                    If Nothing IsNot b Then
                        Array.Copy(b, 0, cated, current, b.Length)
                        current += b.Length
                    End If
                Next

                Return cated
            End Function
        End Class


#End Region

#Region "UTIL METHODS"

        Private Shared Function DecodeRSAPrivateKey(ByVal privkey As Byte()) As RSACryptoServiceProvider
            Dim MODULUS, E, D, P, Q, DP, DQ, IQ As Byte()
            Dim mem As MemoryStream = New MemoryStream(privkey)
            Dim binr As BinaryReader = New BinaryReader(mem)
            Dim bt As Byte = 0
            Dim twobytes As UShort = 0
            Dim elems As Integer = 0

            Try
                twobytes = binr.ReadUInt16()

                If twobytes = &H8130 Then
                    binr.ReadByte()
                ElseIf twobytes = &H8230 Then
                    binr.ReadInt16()
                Else
                    Return Nothing
                End If

                twobytes = binr.ReadUInt16()
                If twobytes <> &H102 Then Return Nothing
                bt = binr.ReadByte()
                If bt <> &H0 Then Return Nothing
                elems = GetIntegerSize(binr)
                MODULUS = binr.ReadBytes(elems)
                elems = GetIntegerSize(binr)
                E = binr.ReadBytes(elems)
                elems = GetIntegerSize(binr)
                D = binr.ReadBytes(elems)
                elems = GetIntegerSize(binr)
                P = binr.ReadBytes(elems)
                elems = GetIntegerSize(binr)
                Q = binr.ReadBytes(elems)
                elems = GetIntegerSize(binr)
                DP = binr.ReadBytes(elems)
                elems = GetIntegerSize(binr)
                DQ = binr.ReadBytes(elems)
                elems = GetIntegerSize(binr)
                IQ = binr.ReadBytes(elems)
                Dim RSA As RSACryptoServiceProvider = New RSACryptoServiceProvider()
                Dim RSAparams As RSAParameters = New RSAParameters()
                RSAparams.Modulus = MODULUS
                RSAparams.Exponent = E
                RSAparams.D = D
                RSAparams.P = P
                RSAparams.Q = Q
                RSAparams.DP = DP
                RSAparams.DQ = DQ
                RSAparams.InverseQ = IQ
                RSA.ImportParameters(RSAparams)
                Return RSA
            Catch __unusedException1__ As Exception
                Return Nothing
            Finally
                binr.Close()
            End Try
        End Function

        Private Shared Function GetIntegerSize(ByVal binr As BinaryReader) As Integer
            Dim bt As Byte = 0
            Dim lowbyte As Byte = &H0
            Dim highbyte As Byte = &H0
            Dim count As Integer = 0
            bt = binr.ReadByte()
            If bt <> &H2 Then Return 0
            bt = binr.ReadByte()

            If bt = &H81 Then
                count = binr.ReadByte()
            ElseIf bt = &H82 Then
                highbyte = binr.ReadByte()
                lowbyte = binr.ReadByte()
                Dim modint As Byte() = {lowbyte, highbyte, &H0, &H0}
                count = BitConverter.ToInt32(modint, 0)
            Else
                count = bt
            End If

            While binr.ReadByte() = &H0
                count -= 1
            End While

            binr.BaseStream.Seek(-1, SeekOrigin.Current)
            Return count
        End Function

        Private Shared Function CompareBytearrays(ByVal a As Byte(), ByVal b As Byte()) As Boolean
            If a.Length <> b.Length Then Return False
            Dim i As Integer = 0

            For Each c As Byte In a
                If c <> b(i) Then Return False
                i += 1
            Next

            Return True
        End Function

        Private Shared Function CreateOctetString(ByVal value As Byte()) As AsnType
            If IsEmpty(value) Then
                Return New AsnType(&H4, EMPTY)
            End If

            Return New AsnType(&H4, value)
        End Function

        Private Shared Function CreateOctetString(ByVal value As AsnType) As AsnType
            If IsEmpty(value) Then
                Return New AsnType(&H4, &H0)
            End If

            Return New AsnType(&H4, value.GetBytes())
        End Function

        Private Shared Function CreateOctetString(ByVal values As AsnType()) As AsnType
            If IsEmpty(values) Then
                Return New AsnType(&H4, &H0)
            End If

            Return New AsnType(&H4, Concatenate(values))
        End Function

        Private Shared Function CreateOctetString(ByVal value As String) As AsnType
            If IsEmpty(value) Then
                Return CreateOctetString(EMPTY)
            End If

            Dim len As Integer = (value.Length + 255) / 256
            Dim octets As List(Of Byte) = New List(Of Byte)()

            For i As Integer = 0 To len - 1
                Dim s As String = value.Substring(i * 2, 2)
                Dim b As Byte = &H0

                Try
                    b = Convert.ToByte(s, 16)
                Catch __unusedFormatException1__ As FormatException
                    Exit For
                Catch __unusedOverflowException2__ As OverflowException
                    Exit For
                End Try

                octets.Add(b)
            Next

            Return CreateOctetString(octets.ToArray())
        End Function

        Private Shared Function CreateBitString(ByVal octets As Byte()) As AsnType
            Return CreateBitString(octets, 0)
        End Function

        Private Shared Function CreateBitString(ByVal octets As Byte(), ByVal unusedBits As UInteger) As AsnType
            If IsEmpty(octets) Then
                Return New AsnType(&H3, EMPTY)
            End If

            If Not unusedBits < 8 Then
                Throw New ArgumentException("Unused bits must be less than 8.")
            End If

            Dim b As Byte() = Concatenate(New Byte() {unusedBits}, octets)
            Return New AsnType(&H3, b)
        End Function

        Private Shared Function CreateBitString(ByVal value As AsnType) As AsnType
            If IsEmpty(value) Then
                Return New AsnType(&H3, EMPTY)
            End If

            Return CreateBitString(value.GetBytes(), &H0)
        End Function

        Private Shared Function CreateBitString(ByVal values As AsnType()) As AsnType
            If IsEmpty(values) Then
                Return New AsnType(&H3, EMPTY)
            End If

            Return CreateBitString(Concatenate(values), &H0)
        End Function

        Private Shared Function CreateBitString(ByVal value As String) As AsnType
            If IsEmpty(value) Then
                Return CreateBitString(EMPTY)
            End If

            Dim lstrlen As Integer = value.Length
            Dim unusedBits As Integer = 8 - lstrlen Mod 8

            If 8 = unusedBits Then
                unusedBits = 0
            End If

            For i As Integer = 0 To unusedBits - 1
                value += "0"
            Next

            Dim loctlen As Integer = (lstrlen + 7) / 8
            Dim octets As List(Of Byte) = New List(Of Byte)()

            For i As Integer = 0 To loctlen - 1
                Dim s As String = value.Substring(i * 8, 8)
                Dim b As Byte = &H0

                Try
                    b = Convert.ToByte(s, 2)
                Catch __unusedFormatException1__ As FormatException
                    unusedBits = 0
                    Exit For
                Catch __unusedOverflowException2__ As OverflowException
                    unusedBits = 0
                    Exit For
                End Try

                octets.Add(b)
            Next

            Return CreateBitString(octets.ToArray(), unusedBits)
        End Function

        Private Shared ZERO As Byte() = New Byte() {0}
        Private Shared EMPTY As Byte() = New Byte() {}

        Private Shared Function IsZero(ByVal octets As Byte()) As Boolean
            If IsEmpty(octets) Then
                Return False
            End If

            Dim allZeros As Boolean = True

            For i As Integer = 0 To octets.Length - 1

                If 0 <> octets(i) Then
                    allZeros = False
                    Exit For
                End If
            Next

            Return allZeros
        End Function

        Private Shared Function IsEmpty(ByVal octets As Byte()) As Boolean
            If Nothing Is octets OrElse 0 = octets.Length Then
                Return True
            End If

            Return False
        End Function

        Private Shared Function IsEmpty(ByVal s As String) As Boolean
            If Equals(Nothing, s) OrElse 0 = s.Length Then
                Return True
            End If

            Return False
        End Function

        Private Shared Function IsEmpty(ByVal strings As String()) As Boolean
            If Nothing Is strings OrElse 0 = strings.Length Then Return True
            Return False
        End Function

        Private Shared Function IsEmpty(ByVal value As AsnType) As Boolean
            If Nothing Is value Then
                Return True
            End If

            Return False
        End Function

        Private Shared Function IsEmpty(ByVal values As AsnType()) As Boolean
            If Nothing Is values OrElse 0 = values.Length Then Return True
            Return False
        End Function

        Private Shared Function IsEmpty(ByVal arrays As Byte()()) As Boolean
            If Nothing Is arrays OrElse 0 = arrays.Length Then Return True
            Return False
        End Function

        Private Shared Function CreateInteger(ByVal value As Byte()) As AsnType
            If IsEmpty(value) Then
                Return CreateInteger(ZERO)
            End If

            Return New AsnType(&H2, value)
        End Function

        Private Shared Function CreateNull() As AsnType
            Return New AsnType(&H5, New Byte() {&H0})
        End Function

        Private Shared Function Duplicate(ByVal b As Byte()) As Byte()
            If IsEmpty(b) Then
                Return EMPTY
            End If

            Dim d As Byte() = New Byte(b.Length - 1) {}
            Array.Copy(b, d, b.Length)
            Return d
        End Function

        Private Shared Function CreateIntegerPos(ByVal value As Byte()) As AsnType
            Dim i As Byte() = Nothing, d As Byte() = Duplicate(value)

            If IsEmpty(d) Then
                d = ZERO
            End If

            If d.Length > 0 AndAlso d(0) > &H7F Then
                i = New Byte(d.Length + 1 - 1) {}
                i(0) = &H0
                Array.Copy(d, 0, i, 1, value.Length)
            Else
                i = d
            End If

            Return CreateInteger(i)
        End Function

        Private Shared Function Concatenate(ByVal values As AsnType()) As Byte()
            If IsEmpty(values) Then Return New Byte() {}
            Dim length As Integer = 0

            For Each t As AsnType In values

                If Nothing IsNot t Then
                    length += t.GetBytes().Length
                End If
            Next

            Dim cated As Byte() = New Byte(length - 1) {}
            Dim current As Integer = 0

            For Each t As AsnType In values

                If Nothing IsNot t Then
                    Dim b As Byte() = t.GetBytes()
                    Array.Copy(b, 0, cated, current, b.Length)
                    current += b.Length
                End If
            Next

            Return cated
        End Function

        Private Shared Function Concatenate(ByVal first As Byte(), ByVal second As Byte()) As Byte()
            Return Concatenate(New Byte()() {first, second})
        End Function

        Private Shared Function Concatenate(ByVal values As Byte()()) As Byte()
            If IsEmpty(values) Then Return New Byte() {}
            Dim length As Integer = 0

            For Each b As Byte() In values

                If Nothing IsNot b Then
                    length += b.Length
                End If
            Next

            Dim cated As Byte() = New Byte(length - 1) {}
            Dim current As Integer = 0

            For Each b As Byte() In values

                If Nothing IsNot b Then
                    Array.Copy(b, 0, cated, current, b.Length)
                    current += b.Length
                End If
            Next

            Return cated
        End Function

        Private Shared Function CreateSequence(ByVal values As AsnType()) As AsnType
            If IsEmpty(values) Then
                Throw New ArgumentException("A sequence requires at least one value.")
            End If

            Return New AsnType(&H10 Or &H20, Concatenate(values))
        End Function

        Private Shared Function CreateOid(ByVal value As String) As AsnType
            If IsEmpty(value) Then Return Nothing
            Dim tokens As String() = value.Split(New Char() {" "c, "."c})
            If IsEmpty(tokens) Then Return Nothing
            Dim a As ULong = 0
            Dim arcs As List(Of ULong) = New List(Of ULong)()

            For Each t As String In tokens

                If t.Length = 0 Then
                    Exit For
                End If

                Try
                    a = Convert.ToUInt64(t, CultureInfo.InvariantCulture)
                Catch __unusedFormatException1__ As FormatException
                    Exit For
                Catch __unusedOverflowException2__ As OverflowException
                    Exit For
                End Try

                arcs.Add(a)
            Next

            If 0 = arcs.Count Then Return Nothing
            Dim octets As List(Of Byte) = New List(Of Byte)()

            If arcs.Count >= 1 Then
                a = arcs(0) * 40
            End If

            If arcs.Count >= 2 Then
                a += arcs(1)
            End If

            octets.Add(a)

            For i As Integer = 2 To arcs.Count - 1
                Dim temp As List(Of Byte) = New List(Of Byte)()
                Dim arc As ULong = arcs(i)

                Do
                    temp.Add(&H80 Or arc And &H7F)
                    arc >>= 7
                Loop While 0 <> arc

                Dim t As Byte() = temp.ToArray()
                t(0) = CByte(&H7F And t(0))
                Array.Reverse(t)

                For Each b As Byte In t
                    octets.Add(b)
                Next
            Next

            Return CreateOid(octets.ToArray())
        End Function

        Private Shared Function CreateOid(ByVal value As Byte()) As AsnType
            If IsEmpty(value) Then
                Return Nothing
            End If

            Return New AsnType(&H6, value)
        End Function

        Private Shared Function Compliment1s(ByVal value As Byte()) As Byte()
            If IsEmpty(value) Then
                Return EMPTY
            End If

            Dim c As Byte() = Duplicate(value)

            For i As Integer = c.Length - 1 To 0 Step -1
                c(i) = Not c(i)
            Next

            Return c
        End Function

#End Region
    End Class
End Namespace
