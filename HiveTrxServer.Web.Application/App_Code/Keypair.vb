﻿Imports System
Imports System.Collections.Generic
Imports System.Globalization
Imports System.IO
Imports System.Linq
Imports System.Security.Cryptography
Imports System.Text
Imports System.Threading.Tasks

Public Class Keypair
    Private _Publickey As String = String.Empty
    Private _Privatekey As String = String.Empty
    Public Const DWKEYSIZE As Integer = 1024

    Public Property Publickey() As String
        Get
            Return _Publickey
        End Get
        Set(ByVal value As String)
            _Publickey = value
        End Set
    End Property
    Public Property Privatekey() As String
        Get
            Return _Privatekey
        End Get
        Set(ByVal value As String)
            _Privatekey = value
        End Set
    End Property
    Public Shared Function CreateNewKeys() As Keypair
        Try
            Using RSA As New RSACryptoServiceProvider(4096)
                Dim Keys As New Keypair
                'MIIJRAIBADANBgkqhkiG9w0BAQEFAASCCS4wggkqAgEAAoICAQDRZDrZcVZZAZrRUNPGbvNubamQMDinLGz1NPDiR7+jHEylvRW8R5sINH0OKs6q7RN/mGBW4u3AaHmwy40HMMxISlq2rq7wnqsU8CqGhkpsuwndQXX3xDdCen8FhJJ6rFdhPuLifJKn2lS4T7TjQHEDi/K4U6Hd4vJpUeaVucUO4HbH2NfU5afy8WgRDLCH7CUZuwSOE5Hzw3sEQ9+TVywaanvY4YjEGs44MFs90loEz62DCCetUG+GkrW0G8jf9If+aTMryDv/2O5guf73y7zd7wzER2PqecpZME4rWU0xQMONNJCCxqCriD7dRVXe+Mplz0WBASlZwWLOGT8hgpK1mhO5lFVr3noVOn0gsZctuWkI6Bb2FAJ7sNtrAA2inBOej0ERXnLkXaU2uQE0628Nb8NnT+7y0TR5/pGEM8Q3qoReTDWNHUrm0WHmwB0pnn7AQfyUTzIKHo8r992DYAm+wLUb1YScx5q5bOXQz/VqImQ9mTlv4tiaNZYzgcLjEUwGPUKTvXLT5Zu69/yadriebCakZdZ0dAcidYZtHUjRtkPjwCV3HdEpz55aMLbdGJ21eZHGZgnhvW46MdV9/jRgFOOZDNhMTf2GK5zvCQNbKXRp4gBTE9MQFCU/n3bAli3D8ZhiSRXGr+iRCOBG3qIBbeFphfP4yC1rIiALHBQN0wIDAQABAoICADi/IhfF2HHctnldn1cawIw/2gFCoEfmCU1W4KVibnQk+jy8hsR033YUy4NoZIP8JhmhURKbA3XumnQoZfr55ZL926HAq5Z4WcMd51h30nmvItg8olzYZ2VzzzLOn+fxBulK4rAgtJoa2HTu3QXtc/NTHR+JQsfgdnPGh2gyZU5H1MkKYkvHRYHYnKxqNPlRLSjQ/0YF1bsrUg5w2omSyVgLXa8ESE1lOlgN+txBQrYlFk/5iM82ilL9NZMvKHxRR/3RwtVtuZz3zHqYTZoYNAItg9c5xcFI6mbLaZPj6sCbXaPKCBP5ApgaH3dWEBSSdj+rMZnpl6xEdaOHQg2x1lI1TrEBQdqjw4it2wEE9VfddsezRny6BuRjP+/d4vV3e12ij26TXTk4DGbEWlnAZ1v7JlKf0GMKx6xAF2GAGRob1Oh3uxkWdtoBGjHBOgQpkGAsDr3g75a1p2N5a22vyUk46enNcNlaXv+4oXx5dX1Cyl25BXE146dfwRSqxQCi97EIgVb10LkqTvQ11Bj6uB2CpVTGXXb9y5qEah1tB9egyN+IAGl5u6NhqqKZE40hk0bC/CXlN+NB5JJ1WOXZrvMLA7JPZvWj4819TLHKBIyBMu8PspPUS5FXzA/QeuoGi3mZHdmbiL5kKpHyK+N89MouEbe8Hoh87cVqRkf4fC05AoIBAQD1k5Rnx37JkZuzwUiYhV/hF4ZsfVciNc0/kCVm4kfBQt4L1bJUgKMuTh3sZGOSV+IbVoLaQpDJmVhk1UQKaX9wHbT+UakB2L2Aj1anL8izBQdVjBxZw5L7xeALIYhlCXw5TKgspRLnjtyKgLwEhyohAwE+MdK1Ig1EK+8yhsii/A0QDVwfD2SbqE2JrPW9PKXmqH4vGTls9rBtXt2J4q4ndSNCy8JI9QLCoAV/LJjfaxGYjyIy1klgcsP9qmcUC7xdeVOWtLum9+W5d7NFPHo1P36GFoGW1trZ0+vn5hxEpFVXtSTrtlqxbiDeQMYvUnLRghfba2xDRDy8sS072WunAoIBAQDaR3dnhcFMdKP/YVQZRB/gJmzRgKCuYNGF8azcIzET6P52znorDqfKm+j6fUd/9l9teMlyVEn/9NsBj9VzY2wncmd0JnD2l1ssS6sEEvayl3Yf7QvY0FMOHaSkqu11GVqVFaZVMV4ZD70fQrBY19G8v2R2iv2ZzXqS/9wLLbK9aNY4hGYdYHc+ph6tmMfleSR1Ire86XX2CH/A2xH1iY7bqwt8RSytslKXAOSEQBsDW20AqfTLq/iBTpgmI4T/VdDRML3xgEXRtWyKgHSz37EA5hZupE7ugEGTM1aBZxDU9585XiJRvGQCnZOvcmDrYfYWjtQ9x2Xa8SHxNMa+1KH1AoIBAQDK1inHCkUT0nvrniMLL2bBMcArDeARdqTf8nyiHRatb0CeP9S67UFbNtSKc2S29HXipLMR+ddDXLBhILHBc9/5x9++C87voojrjflBwDsiem3TYBXuopZd9NOX/gt9gLclUMa7kWEq95n7oFcr2kSW3beCGn7yc3zHqQ46Co/f5xYXAyHq2bj8xKe+pqiWtGhHOzZ2SnxfHuccfzW29Q9psdv9CPWhcxXjkW7Y2wotx27SNq9FlC8pRcHSXJQlP4hHILZFDWTNhYYuicr9zfUW1Cn6MM66ux4qu1rnwO3qWF3RQuKK+qSwVsk5KgFIBu6lU7HG31Sdvtf39tPo0i2TAoIBAQDAH69Fe0FMXk4K9kvjgUbdMHUPYbwMQEOrNbEEgiUsdSOkxtBZHakqeFh0QdixFNWhsXL+IpqPo6Ewmr0xtwz3G4dbgIplQYuKTX/VMivZvd7G8wo+Z/7yqbzFA33GV+6W0lt5GN7OCEBaR4SYbj25eGygum3E9ksxH+s6G5r7hE8pp7+Jk+LhTa4q9hLcM+iXAVvf901QpOJtiWUhTVZkmwSc+PX+XLq9FpgMn7AnIE2tC+rfahptFhRtXU4uwAXwWoqVCmZp0toafj46JGXTeLG96Plgi5ssST+W3nS+PsXy5JwofT054CChiHcuNyc3a7f8OIm6UAukFuPoC5gRAoIBAQCIuqgGpilAJwx+iPIKm76TnBUxjt1MS+oqePWf27uvNLL6AnKmCmo7uS0gp1iH7LlnD7WcfZIhRyuAvztCoTZ8BmcZw+IovtMQgCPjJqbjoA29GtTXQ0jIaLMHTObCIAiTlUgcQRt6nFWZFBxsvBPHhaM4vRPPIBHqdlBsvCZdWBKRUYDSlKcNH3gIisoIa3gkFF6nWxhqi/emw+r3eEcTaBvttcS7UG6ken9EvePVrBe0TilIs6WsGSRF+un0JcaWw79V6/xcxiSfpdLevdBNZTRaTPOT4TWMRAY5Q97YmpM9t9BnDB+jFAL1/Ol5NpOwdXZIU0TQgLotmX04ODCu
                Keys.Privatekey = "<RSAKeyValue><Modulus>0WQ62XFWWQGa0VDTxm7zbm2pkDA4pyxs9TTw4ke/oxxMpb0VvEebCDR9DirOqu0Tf5hgVuLtwGh5sMuNBzDMSEpatq6u8J6rFPAqhoZKbLsJ3UF198Q3Qnp/BYSSeqxXYT7i4nySp9pUuE+040BxA4vyuFOh3eLyaVHmlbnFDuB2x9jX1OWn8vFoEQywh+wlGbsEjhOR88N7BEPfk1csGmp72OGIxBrOODBbPdJaBM+tgwgnrVBvhpK1tBvI3/SH/mkzK8g7/9juYLn+98u83e8MxEdj6nnKWTBOK1lNMUDDjTSQgsagq4g+3UVV3vjKZc9FgQEpWcFizhk/IYKStZoTuZRVa956FTp9ILGXLblpCOgW9hQCe7DbawANopwTno9BEV5y5F2lNrkBNOtvDW/DZ0/u8tE0ef6RhDPEN6qEXkw1jR1K5tFh5sAdKZ5+wEH8lE8yCh6PK/fdg2AJvsC1G9WEnMeauWzl0M/1aiJkPZk5b+LYmjWWM4HC4xFMBj1Ck71y0+Wbuvf8mna4nmwmpGXWdHQHInWGbR1I0bZD48Aldx3RKc+eWjC23RidtXmRxmYJ4b1uOjHVff40YBTjmQzYTE39hiuc7wkDWyl0aeIAUxPTEBQlP592wJYtw/GYYkkVxq/okQjgRt6iAW3haYXz+MgtayIgCxwUDdM=</Modulus><Exponent>AQAB</Exponent><P>9ZOUZ8d+yZGbs8FImIVf4ReGbH1XIjXNP5AlZuJHwULeC9WyVICjLk4d7GRjklfiG1aC2kKQyZlYZNVECml/cB20/lGpAdi9gI9Wpy/IswUHVYwcWcOS+8XgCyGIZQl8OUyoLKUS547cioC8BIcqIQMBPjHStSINRCvvMobIovwNEA1cHw9km6hNiaz1vTyl5qh+Lxk5bPawbV7dieKuJ3UjQsvCSPUCwqAFfyyY32sRmI8iMtZJYHLD/apnFAu8XXlTlrS7pvfluXezRTx6NT9+hhaBltba2dPr5+YcRKRVV7Uk67ZasW4g3kDGL1Jy0YIX22tsQ0Q8vLEtO9lrpw==</P><Q>2kd3Z4XBTHSj/2FUGUQf4CZs0YCgrmDRhfGs3CMxE+j+ds56Kw6nypvo+n1Hf/ZfbXjJclRJ//TbAY/Vc2NsJ3JndCZw9pdbLEurBBL2spd2H+0L2NBTDh2kpKrtdRlalRWmVTFeGQ+9H0KwWNfRvL9kdor9mc16kv/cCy2yvWjWOIRmHWB3PqYerZjH5XkkdSK3vOl19gh/wNsR9YmO26sLfEUsrbJSlwDkhEAbA1ttAKn0y6v4gU6YJiOE/1XQ0TC98YBF0bVsioB0s9+xAOYWbqRO7oBBkzNWgWcQ1PefOV4iUbxkAp2Tr3Jg62H2Fo7UPcdl2vEh8TTGvtSh9Q==</Q><DP>ytYpxwpFE9J7654jCy9mwTHAKw3gEXak3/J8oh0WrW9Anj/Uuu1BWzbUinNktvR14qSzEfnXQ1ywYSCxwXPf+cffvgvO76KI6435QcA7Inpt02AV7qKWXfTTl/4LfYC3JVDGu5FhKveZ+6BXK9pElt23ghp+8nN8x6kOOgqP3+cWFwMh6tm4/MSnvqaolrRoRzs2dkp8Xx7nHH81tvUPabHb/Qj1oXMV45Fu2NsKLcdu0javRZQvKUXB0lyUJT+IRyC2RQ1kzYWGLonK/c31FtQp+jDOurseKrta58Dt6lhd0ULiivqksFbJOSoBSAbupVOxxt9Unb7X9/bT6NItkw==</DP><DQ>wB+vRXtBTF5OCvZL44FG3TB1D2G8DEBDqzWxBIIlLHUjpMbQWR2pKnhYdEHYsRTVobFy/iKaj6OhMJq9MbcM9xuHW4CKZUGLik1/1TIr2b3exvMKPmf+8qm8xQN9xlfultJbeRjezghAWkeEmG49uXhsoLptxPZLMR/rOhua+4RPKae/iZPi4U2uKvYS3DPolwFb3/dNUKTibYllIU1WZJsEnPj1/ly6vRaYDJ+wJyBNrQvq32oabRYUbV1OLsAF8FqKlQpmadLaGn4+OiRl03ixvej5YIubLEk/lt50vj7F8uScKH09OeAgoYh3LjcnN2u3/DiJulALpBbj6AuYEQ==</DQ><InverseQ>iLqoBqYpQCcMfojyCpu+k5wVMY7dTEvqKnj1n9u7rzSy+gJypgpqO7ktIKdYh+y5Zw+1nH2SIUcrgL87QqE2fAZnGcPiKL7TEIAj4yam46ANvRrU10NIyGizB0zmwiAIk5VIHEEbepxVmRQcbLwTx4WjOL0TzyAR6nZQbLwmXVgSkVGA0pSnDR94CIrKCGt4JBRep1sYaov3psPq93hHE2gb7bXEu1BupHp/RL3j1awXtE4pSLOlrBkkRfrp9CXGlsO/Vev8XMYkn6XS3r3QTWU0Wkzzk+E1jEQGOUPe2JqTPbfQZwwfoxQC9fzpeTaTsHV2SFNE0IC6LZl9ODgwrg==</InverseQ><D>OL8iF8XYcdy2eV2fVxrAjD/aAUKgR+YJTVbgpWJudCT6PLyGxHTfdhTLg2hkg/wmGaFREpsDde6adChl+vnlkv3bocCrlnhZwx3nWHfSea8i2DyiXNhnZXPPMs6f5/EG6UrisCC0mhrYdO7dBe1z81MdH4lCx+B2c8aHaDJlTkfUyQpiS8dFgdicrGo0+VEtKND/RgXVuytSDnDaiZLJWAtdrwRITWU6WA363EFCtiUWT/mIzzaKUv01ky8ofFFH/dHC1W25nPfMephNmhg0Ai2D1znFwUjqZstpk+PqwJtdo8oIE/kCmBofd1YQFJJ2P6sxmemXrER1o4dCDbHWUjVOsQFB2qPDiK3bAQT1V912x7NGfLoG5GM/793i9Xd7XaKPbpNdOTgMZsRaWcBnW/smUp/QYwrHrEAXYYAZGhvU6He7GRZ22gEaMcE6BCmQYCwOveDvlrWnY3lrba/JSTjp6c1w2Vpe/7ihfHl1fULKXbkFcTXjp1/BFKrFAKL3sQiBVvXQuSpO9DXUGPq4HYKlVMZddv3LmoRqHW0H16DI34gAaXm7o2GqopkTjSGTRsL8JeU340HkknVY5dmu8wsDsk9m9aPjzX1MscoEjIEy7w+yk9RLkVfMD9B66gaLeZkd2ZuIvmQqkfIr43z0yi4Rt7weiHztxWpGR/h8LTk=</D></RSAKeyValue>"
                'MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA0WQ62XFWWQGa0VDTxm7zbm2pkDA4pyxs9TTw4ke/oxxMpb0VvEebCDR9DirOqu0Tf5hgVuLtwGh5sMuNBzDMSEpatq6u8J6rFPAqhoZKbLsJ3UF198Q3Qnp/BYSSeqxXYT7i4nySp9pUuE+040BxA4vyuFOh3eLyaVHmlbnFDuB2x9jX1OWn8vFoEQywh+wlGbsEjhOR88N7BEPfk1csGmp72OGIxBrOODBbPdJaBM+tgwgnrVBvhpK1tBvI3/SH/mkzK8g7/9juYLn+98u83e8MxEdj6nnKWTBOK1lNMUDDjTSQgsagq4g+3UVV3vjKZc9FgQEpWcFizhk/IYKStZoTuZRVa956FTp9ILGXLblpCOgW9hQCe7DbawANopwTno9BEV5y5F2lNrkBNOtvDW/DZ0/u8tE0ef6RhDPEN6qEXkw1jR1K5tFh5sAdKZ5+wEH8lE8yCh6PK/fdg2AJvsC1G9WEnMeauWzl0M/1aiJkPZk5b+LYmjWWM4HC4xFMBj1Ck71y0+Wbuvf8mna4nmwmpGXWdHQHInWGbR1I0bZD48Aldx3RKc+eWjC23RidtXmRxmYJ4b1uOjHVff40YBTjmQzYTE39hiuc7wkDWyl0aeIAUxPTEBQlP592wJYtw/GYYkkVxq/okQjgRt6iAW3haYXz+MgtayIgCxwUDdMCAwEAAQ==
                Keys.Publickey = "<RSAKeyValue><Modulus>0WQ62XFWWQGa0VDTxm7zbm2pkDA4pyxs9TTw4ke/oxxMpb0VvEebCDR9DirOqu0Tf5hgVuLtwGh5sMuNBzDMSEpatq6u8J6rFPAqhoZKbLsJ3UF198Q3Qnp/BYSSeqxXYT7i4nySp9pUuE+040BxA4vyuFOh3eLyaVHmlbnFDuB2x9jX1OWn8vFoEQywh+wlGbsEjhOR88N7BEPfk1csGmp72OGIxBrOODBbPdJaBM+tgwgnrVBvhpK1tBvI3/SH/mkzK8g7/9juYLn+98u83e8MxEdj6nnKWTBOK1lNMUDDjTSQgsagq4g+3UVV3vjKZc9FgQEpWcFizhk/IYKStZoTuZRVa956FTp9ILGXLblpCOgW9hQCe7DbawANopwTno9BEV5y5F2lNrkBNOtvDW/DZ0/u8tE0ef6RhDPEN6qEXkw1jR1K5tFh5sAdKZ5+wEH8lE8yCh6PK/fdg2AJvsC1G9WEnMeauWzl0M/1aiJkPZk5b+LYmjWWM4HC4xFMBj1Ck71y0+Wbuvf8mna4nmwmpGXWdHQHInWGbR1I0bZD48Aldx3RKc+eWjC23RidtXmRxmYJ4b1uOjHVff40YBTjmQzYTE39hiuc7wkDWyl0aeIAUxPTEBQlP592wJYtw/GYYkkVxq/okQjgRt6iAW3haYXz+MgtayIgCxwUDdM=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>"

                Return Keys
            End Using
        Catch ex As Exception
            Throw New Exception("Keypair.CreateNewKeys():" & ex.Message, ex)
        End Try
    End Function
    Public Shared Function CheckSourceValidate(ByVal source As String) As Boolean
        Return DWKEYSIZE / 8 - 11 >= source.Length
    End Function

    Private Shared Function FromBase64Key(ByVal key As String) As String
        Dim b As Byte() = Convert.FromBase64String(key)
        Return Encoding.ASCII.GetString(b)
    End Function

    Private Shared Function BytesToHexString(ByVal input As Byte()) As String
        Dim hexString As StringBuilder = New StringBuilder(64)

        For i As Integer = 0 To input.Length - 1
            hexString.Append(String.Format("{0:X2}", input(i)))
        Next

        Return hexString.ToString()
    End Function

    Private Shared Function HexStringToBytes(ByVal hex As String) As Byte()
        If hex.Length = 0 Then
            Return New Byte() {0}
        End If

        If hex.Length Mod 2 = 1 Then
            hex = "0" & hex
        End If

        Dim result As Byte() = New Byte(hex.Length / 2 - 1) {}

        For i As Integer = 0 To hex.Length / 2 - 1
            result(i) = Byte.Parse(hex.Substring(2 * i, 2), NumberStyles.AllowHexSpecifier)
        Next

        Return result
    End Function

    Private Shared Function ToBase64Key(ByVal key As String) As String
        Dim b As Byte() = Encoding.ASCII.GetBytes(key)
        Return Convert.ToBase64String(b)
    End Function

    Public Shared Function EncryptString(ByVal source As String, ByVal key As String) As String
        Dim lEncryptString As String = String.Empty

        Try

            If Not CheckSourceValidate(source) Then
                Throw New Exception("source string too long")
            End If

            RSACryptoServiceProvider.UseMachineKeyStore = True
            Dim rsaProvider As RSACryptoServiceProvider = New RSACryptoServiceProvider()
            rsaProvider.FromXmlString(FromBase64Key(key))
            Dim data As Byte() = rsaProvider.Encrypt(Encoding.ASCII.GetBytes(source), False)
            lEncryptString = BytesToHexString(data)
        Catch
        End Try

        Return lEncryptString
    End Function

    Public Shared Function ExportPublicKeyToPEMFormat(ByVal csp As RSACryptoServiceProvider) As String
        Dim outputStream As TextWriter = New StringWriter()
        Dim parameters = csp.ExportParameters(False)

        Using stream = New MemoryStream()
            Dim writer = New BinaryWriter(stream)
            writer.Write(CByte(&H30)) ' SEQUENCE
            Using innerStream = New MemoryStream()
                Dim innerWriter = New BinaryWriter(innerStream)
                EncodeIntegerBigEndian(innerWriter, New Byte() {&H0}) ' Version
                EncodeIntegerBigEndian(innerWriter, parameters.Modulus)
                EncodeIntegerBigEndian(innerWriter, parameters.Exponent)

                'All Parameter Must Have Value so Set Other Parameter Value Whit Invalid Data  (for keeping Key Structure  use "parameters.Exponent" value for invalid data)
                EncodeIntegerBigEndian(innerWriter, parameters.Exponent) ' instead of parameters.D
                EncodeIntegerBigEndian(innerWriter, parameters.Exponent) ' instead of parameters.P
                EncodeIntegerBigEndian(innerWriter, parameters.Exponent) ' instead of parameters.Q
                EncodeIntegerBigEndian(innerWriter, parameters.Exponent) ' instead of parameters.DP
                EncodeIntegerBigEndian(innerWriter, parameters.Exponent) ' instead of parameters.DQ
                EncodeIntegerBigEndian(innerWriter, parameters.Exponent) ' instead of parameters.InverseQ
                Dim length = CInt(innerStream.Length)
                EncodeLength(writer, length)
                writer.Write(innerStream.GetBuffer(), 0, length)
            End Using

            Dim base64 = Convert.ToBase64String(stream.GetBuffer(), 0, stream.Length).ToCharArray()
            'outputStream.WriteLine("-----BEGIN PUBLIC KEY-----")

            ' Output as Base64 with lines chopped at 64 characters
            For i = 0 To base64.Length - 1 Step 64
                outputStream.WriteLine(base64, i, Math.Min(64, base64.Length - i))
            Next

            'outputStream.WriteLine("-----END PUBLIC KEY-----")

            Dim result As String = outputStream.ToString

            Dim importedPublicKeyBytes As Byte() = Convert.FromBase64String(result)

            Using providerFromX509pubKey = ExportRSAKey.KeyUtils.DecodePublicKey(importedPublicKeyBytes)
                providerFromX509pubKey.PersistKeyInCsp = False 'DO NOT STORE IN KEYSTORE

                'EXPORT TO X509 PUBLIC KEY BLOB
                Dim x509pubKeyBytes As Byte() = ExportRSAKey.KeyUtils.PublicKeyToX509(providerFromX509pubKey.ExportParameters(False))

                'CONVERT TO BASE64
                Dim x509pubKeyBase64 As String = Convert.ToBase64String(x509pubKeyBytes)

                'PRINT INFO
                Console.WriteLine("------   PUBLIC KEY TO EXPORT   ------")
                Console.WriteLine(x509pubKeyBase64 & vbLf & vbLf)
            End Using

            Return outputStream.ToString()
        End Using
    End Function

    Private Shared Function ExportPrivateKeyToPEMFormat(ByVal csp As RSACryptoServiceProvider) As String
        Dim outputStream As TextWriter = New StringWriter()
        If csp.PublicOnly Then Throw New ArgumentException("CSP does not contain a private key", "csp")
        Dim parameters = csp.ExportParameters(True)

        Using stream = New MemoryStream()
            Dim writer = New BinaryWriter(stream)
            writer.Write(CByte(&H30)) ' SEQUENCE
            Using innerStream = New MemoryStream()
                Dim innerWriter = New BinaryWriter(innerStream)
                EncodeIntegerBigEndian(innerWriter, New Byte() {&H0}) ' Version
                EncodeIntegerBigEndian(innerWriter, parameters.Modulus)
                EncodeIntegerBigEndian(innerWriter, parameters.Exponent)
                EncodeIntegerBigEndian(innerWriter, parameters.D)
                EncodeIntegerBigEndian(innerWriter, parameters.P)
                EncodeIntegerBigEndian(innerWriter, parameters.Q)
                EncodeIntegerBigEndian(innerWriter, parameters.DP)
                EncodeIntegerBigEndian(innerWriter, parameters.DQ)
                EncodeIntegerBigEndian(innerWriter, parameters.InverseQ)
                Dim length = CInt(innerStream.Length)
                EncodeLength(writer, length)
                writer.Write(innerStream.GetBuffer(), 0, length)
            End Using

            Dim base64 = Convert.ToBase64String(stream.GetBuffer(), 0, stream.Length).ToCharArray()
            outputStream.WriteLine("-----BEGIN RSA PRIVATE KEY-----")

            ' Output as Base64 with lines chopped at 64 characters
            For i = 0 To base64.Length - 1 Step 64
                outputStream.WriteLine(base64, i, Math.Min(64, base64.Length - i))
            Next

            outputStream.WriteLine("-----END RSA PRIVATE KEY-----")
            Return outputStream.ToString()
        End Using
    End Function

    Private Shared Sub EncodeIntegerBigEndian(ByVal stream As BinaryWriter, ByVal value As Byte(), ByVal Optional forceUnsigned As Boolean = True)
        stream.Write(CByte(&H2)) ' INTEGER
        Dim prefixZeros = 0

        For i = 0 To value.Length - 1
            If value(i) <> 0 Then Exit For
            prefixZeros += 1
        Next

        If value.Length - prefixZeros = 0 Then
            EncodeLength(stream, 1)
            stream.Write(CByte(0))
        Else

            If forceUnsigned AndAlso value(prefixZeros) > &H7F Then
                ' Add a prefix zero to force unsigned if the MSB is 1
                EncodeLength(stream, value.Length - prefixZeros + 1)
                stream.Write(CByte(0))
            Else
                EncodeLength(stream, value.Length - prefixZeros)
            End If

            For i = prefixZeros To value.Length - 1
                stream.Write(value(i))
            Next
        End If
    End Sub

    Private Shared Sub EncodeLength(ByVal stream As BinaryWriter, ByVal length As Integer)
        If length < 0 Then Throw New ArgumentOutOfRangeException("length", "Length must be non-negative")

        If length < &H80 Then
            ' Short form
            stream.Write(CByte(length))
        Else
            ' Long form
            Dim temp = length
            Dim bytesRequired = 0

            While temp > 0
                temp >>= 8
                bytesRequired += 1
            End While

            stream.Write(CByte(bytesRequired Or &H80))

            For i = bytesRequired - 1 To 0 Step -1
                stream.Write(CByte(length >> 8 * i And &HFF))
            Next
        End If
    End Sub
End Class
