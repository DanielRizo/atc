﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic

Namespace TeleLoader.Licenses

    Public Class License

        Private strConnString As String
        Private intCustomerID As Integer
        Private intLicenseID As Integer
        Private intLicenseType As Integer
        Private strLicenseKey As String
        Private strCntTerminales As String
        Private strCntTerminalesmdm As String
        Private strCntTerminalesDr As String

        Public Sub New(ByVal strConnString As String)
            Me.strConnString = strConnString
        End Sub

        Public Sub New(ByVal strConnString As String, ByVal customerId As Integer)
            Me.strConnString = strConnString
            Me.intCustomerID = customerId
        End Sub

        Public Sub New()

            Dim configurationSection As ConnectionStringsSection = _
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

            strConnString = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString

        End Sub

        ''' <summary>
        ''' Código de la Licencia
        ''' </summary>
        Public Property LicenseCode() As Integer
            Get
                Return intLicenseID
            End Get
            Set(ByVal value As Integer)
                intLicenseID = value
            End Set
        End Property

        ''' <summary>
        ''' Serial de la Licencia
        ''' </summary>
        Public Property LicenseKey() As String
            Get
                Return strLicenseKey
            End Get
            Set(ByVal value As String)
                strLicenseKey = value
            End Set
        End Property

        ''' <summary>
        ''' Cantidad de terminales
        ''' </summary>
        Public Property CntTerminales() As String
            Get
                Return strCntTerminales
            End Get
            Set(ByVal value As String)
                strCntTerminales = value
            End Set
        End Property

        ''' <summary>
        ''' Cantidad de terminales MDM
        ''' </summary>
        Public Property CntTerminalesMdm() As String
            Get
                Return strCntTerminalesmdm
            End Get
            Set(ByVal value As String)
                strCntTerminalesmdm = value
            End Set
        End Property

        ''' <summary>
        ''' Cantidad de terminales Descarga Remota
        ''' </summary>
        Public Property CntTerminalesDr() As String
            Get
                Return strCntTerminalesDr
            End Get
            Set(ByVal value As String)
                strCntTerminalesDr = value
            End Set
        End Property

        ''' <summary>
        ''' Tipo de la Licencia
        ''' </summary>
        Public Property LicenseType() As Integer
            Get
                Return intLicenseType
            End Get
            Set(ByVal value As Integer)
                intLicenseType = value
            End Set
        End Property

        ''' <summary>
        ''' Crear Licencia para el Cliente
        ''' </summary>
        Public Function createLicenseCustomer() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webCrearLicenciaCliente"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("customerId", intCustomerID))
                command.Parameters.Add(New SqlParameter("licenseType", intLicenseType))
                command.Parameters.Add(New SqlParameter("licenseKey", strLicenseKey))
                'command.Parameters.Add(New SqlParameter("CntTerminales", strCntTerminales))
                'command.Parameters.Add(New SqlParameter("CntTerminalesMdm", strCntTerminalesmdm))
                'command.Parameters.Add(New SqlParameter("CntTerminalesDr", strCntTerminalesDr))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

    End Class

End Namespace
