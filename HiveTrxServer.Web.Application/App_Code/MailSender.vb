﻿Imports System.Net.Mail
Imports System.IO
Imports System.Data.SqlClient
Imports System.Data

Namespace TeleLoader.Security

    Public Class MailSender
#Region "Definicion de Variables y Propiedades"
        Private _StrDe As String
        Private _StrPasswordDe As String
        Private _StrPara As String
        Private _StrAsunto As String
        Private _StrMensaje As String
        Private _strAdjunto As String

        Property StrDe() As String
            Get
                Return _StrDe
            End Get
            Set(ByVal value As String)
                _StrDe = value
            End Set
        End Property

        Property StrPasswordDe() As String
            Get
                Return _StrPasswordDe
            End Get
            Set(ByVal value As String)
                _StrPasswordDe = value
            End Set
        End Property

        Property StrPara() As String
            Get
                Return _StrPara
            End Get
            Set(ByVal value As String)
                _StrPara = value
            End Set
        End Property

        Property StrAsunto() As String
            Get
                Return _StrAsunto
            End Get
            Set(ByVal value As String)
                _StrAsunto = value
            End Set
        End Property

        Property StrMensaje() As String
            Get
                Return _StrMensaje
            End Get
            Set(ByVal value As String)
                _StrMensaje = value
            End Set
        End Property

        Property strAdjunto() As String
            Get
                Return _strAdjunto
            End Get
            Set(ByVal value As String)
                _strAdjunto = value
            End Set
        End Property
#End Region

        Public Shared Function SendEmail(ByVal emailTo As String, ByVal activationCode As String, ByVal operationType As Integer, ByVal loginName As String, ByVal EmailAdmin As String, ByVal Password As String) As Boolean
            Dim retVal As Boolean = False

            Try
                Dim mailServer As String = ConfigurationSettings.AppSettings.Get("MailServer")
                Dim smtpServer As New SmtpClient
                Dim email As New MailMessage()
                Dim message As String = ""

                smtpServer.UseDefaultCredentials = False
                smtpServer.Credentials = New Net.NetworkCredential(EmailAdmin, Password)
                smtpServer.Host = ConfigurationSettings.AppSettings.Get("SMTPServer")

                'Check Mail Server Type
                If mailServer = "GMAIL" Then
                    smtpServer.Port = 587
                    smtpServer.EnableSsl = True
                ElseIf mailServer = "PRIVATE" Then
                    smtpServer.Port = 25
                End If

                email.From = New MailAddress(ConfigurationSettings.AppSettings.Get("MailUserName"))
                email.To.Add(emailTo)
                email.IsBodyHtml = True

                If operationType = 1 Then
                    'Reset Password from Web
                    email.Subject = "Olvidó Su Clave - Sistema TeleLoader"
                    message = "<h2>Proceso de Restauración de Clave</h2>" & "<br />" &
                                "Señor usuario, se ha iniciado el proceso de restauraci&oacute;n de su clave. Por favor continue con los siguientes pasos:" & "<br/><br/>" &
                                "<ul><li>Ingrese al siguiente link: <a href='" & ConfigurationSettings.AppSettings.Get("UrlRestorePassword") & activationCode & "'>Restaurar mi Clave!</a>.</li>" &
                                "<li>Digite su nueva clave en el formulario mostrado.</li>" &
                                "<li>Inicie sesi&oacute;n con su nueva clave.</li></ul>" &
                                "<br /><br />" &
                                "<b>&lt;&lt; Correo Generado Autom&aacute;ticamente. No Responder &gt;&gt;</b>"
                ElseIf operationType = 2 Then
                    'Set New Password - New User
                    email.Subject = "Nuevo Usuario - Sistema TeleLoader"
                    message = "<h2>Proceso de Activación de Usuario</h2>" & "<br />" &
                                "Se ha dado de alta en el Sistema TeleLoader un nuevo usuario = <b>" + loginName + "</b>, con esta cuenta de correo. Por favor continue con los siguientes pasos para terminar su proceso de activación:" & "<br/><br/>" &
                                "<ul><br /><li>Ingrese al siguiente link: <a href='" & ConfigurationSettings.AppSettings.Get("UrlRestorePassword") & activationCode & "'>Activar mi Usuario!</a>.</li>" &
                                "<li>Digite su clave en el formulario mostrado.</li>" &
                                "<li>Inicie sesi&oacute;n con su clave en la plataforma TeleLoader.</li></ul>" &
                                "<br /><br />" &
                                "<b>&lt;&lt; Correo Generado Autom&aacute;ticamente. No Responder &gt;&gt;</b>"
                End If

                email.Body = message

                'Send Restore Password Email
                smtpServer.Send(email)

                retVal = True

            Catch ex As Exception
                retVal = False
            End Try

            Return retVal

        End Function

        Public Shared Function SendEmail(email As String, activationCode As String, v1 As Integer, v2 As String) As Boolean
            Throw New NotImplementedException()
        End Function
    End Class
End Namespace
