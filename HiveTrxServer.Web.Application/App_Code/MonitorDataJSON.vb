﻿Imports Microsoft.VisualBasic

Namespace TeleLoader.MDMRequestResponse
    Public Class MonitorDataReq
        Public Property operacion As String = ""
        Public Property login As String = ""
        Public Property pass As String = ""
        Public Property ram As String = ""
        Public Property bateria As String = ""
        Public Property lat As String = ""
        Public Property lng As String = ""
        Public Property apks As String = ""
        Public Property grupo As String = ""
        Public Property id_device As String = ""
        Public Property ip As String = ""
    End Class

    Public Class MonitorDataResp
        Public Property res As String = ""
        Public Property user As String = ""
        Public Property pass As String = ""
        Public Property mensaje As String = ""
        Public Property bloqueo As String = ""
        Public Property clave As String = ""
        Public Property idDevice As String = ""
        Public Property queries As String = ""
        Public Property frecuency As String = ""
        Public Property apks As String = ""
        Public Property xml As String = ""
        Public Property xmlTerminal As String = ""
        Public Property zip As String = ""
        Public Property archivo As String = ""
        Public Property paquete As String = ""
        Public Property version As String = ""
        Public Property groupName As String = ""
        'Modificación Carga de Llaves; EB: 01/Mar/2018
        Public Property recargar_llave As String = ""
        'Modificación Definir aplicacion en modo kiosko; FM: 04/May/2018
        Public Property aplicacion_kiosko As String = ""
    End Class

End Namespace

