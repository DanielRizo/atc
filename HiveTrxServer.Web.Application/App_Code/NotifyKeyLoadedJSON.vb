﻿Imports Microsoft.VisualBasic

Namespace TeleLoader.MDMRequestResponse
    Public Class NotifyKeyLoadedReq
        Public Property operacion As String = ""
        Public Property id_device As String = ""
        Public Property ip As String = ""
        Public Property kcv As String = ""
    End Class

    Public Class NotifyKeyLoadedResp
        Public Property res As String = ""
    End Class

End Namespace

