﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic

Namespace TeleLoader.Groups

    Public Class Paramter

        Private strConnectionString As String
        Private StrFIeldName As String
        Private strRecorNo As String
        Private strTableType As String
        Private strNameParameter As String
        Private strValor As String
        Private strNote As String

        Public Sub New(ByVal strConnectionStringStis As String)
            Me.strConnectionString = strConnectionString
        End Sub

        Public Sub New(ByVal strConnectionString As String, ByVal FIELD_DISPLAY_NAME As String)
            Me.strConnectionString = strConnectionString
            Me.StrFIeldName = FIELD_DISPLAY_NAME
        End Sub

        Public Sub New()

            Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
            strConnectionString = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        End Sub

        ''' <summary>
        ''' Código del Parametro
        ''' </summary>
        Public Property ParameterCode() As String
            Get
                Return StrFIeldName
            End Get
            Set(ByVal value As String)
                StrFIeldName = value
            End Set
        End Property

        ''' <summary>
        ''' Nombre del Terminal Record
        ''' </summary>
        Public Property Record() As String
            Get
                Return strRecorNo
            End Get
            Set(ByVal value As String)
                strRecorNo = value
            End Set
        End Property

        ''' <summary>
        ''' Tipo de Tabla
        ''' </summary>
        Public Property TableType() As String
            Get
                Return strTableType
            End Get
            Set(ByVal value As String)
                strTableType = value
            End Set
        End Property

        ''' <summary>
        ''' Nombre del Parametro
        ''' </summary>
        Public Property NameParameter() As String
            Get
                Return strNameParameter
            End Get
            Set(ByVal value As String)
                strNameParameter = value
            End Set
        End Property

        ''' <summary>
        ''' Valor Actual del Parametro
        ''' </summary>
        Public Property Value() As String
            Get
                Return strValor
            End Get
            Set(ByVal value As String)
                strValor = value
            End Set
        End Property

        ''' <summary>
        ''' Nota actual del Parametro
        ''' </summary>
        Public Property Note() As String
            Get
                Return strNote
            End Get
            Set(ByVal value As String)
                strNote = value
            End Set
        End Property

        ''' <summary>
        ''' Leer Datos del Parametro
        ''' </summary>
        Public Sub getGroupData()
            Dim connection As New SqlConnection(strConnectionString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webConsultarDatosParametros"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("FIELD_DISPLAY_NAME", StrFIeldName))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        strRecorNo = results.GetString(0)
                        strTableType = results.GetString(1)
                        strNameParameter = results.GetString(2)
                        strValor = results.GetString(3)
                        strNote = results.GetBoolean(4)

                    End While
                Else
                    strRecorNo = ""
                    strTableType = ""
                    strNameParameter = ""
                    strValor = ""
                    strNote = ""

                End If

            Catch ex As Exception

            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

        End Sub

        ''' <summary>
        ''' Editar Parametros Stis  OG
        ''' </summary>
        Public Function editParameters() As Boolean
            Dim connection As New SqlConnection(strConnectionString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webEditarParameters"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("ParameterID", StrFIeldName))
                command.Parameters.Add(New SqlParameter("RecordName", strRecorNo))
                command.Parameters.Add(New SqlParameter("TableType", strTableType))
                command.Parameters.Add(New SqlParameter("NameParameter", strNameParameter))
                command.Parameters.Add(New SqlParameter("Valor", strValor))
                command.Parameters.Add(New SqlParameter("Nota", strNote))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function
    End Class
End Namespace
