﻿Public Class ReCaptcha
    Public Shared Function Validate(ByVal EncodedResponse As String) As String
        Dim client = New System.Net.WebClient()

        Dim PrivateKey As String = ConfigurationSettings.AppSettings.Get("reCaptchaPrivateKey")

        Dim GoogleReply As String = client.DownloadString(String.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", PrivateKey, EncodedResponse)).ToLower()

        'Validate Json Response
        If (GoogleReply.Contains("""success"": true")) Then
            Return "True"
        End If

        Return "False"
    End Function
End Class