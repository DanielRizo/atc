﻿Imports Microsoft.VisualBasic

Namespace TeleLoader.MDMRequestResponse
    Public Class RequestKeyReq
        Public Property operacion As String = ""
        Public Property id_device As String = ""
        Public Property ip As String = ""
        Public Property Key_device As String = ""
    End Class

    Public Class RequestKeyResp
        Public Property res As String = ""
        Public Property encrypted_key As String = ""
    End Class

End Namespace

