﻿Imports Microsoft.VisualBasic

Namespace TeleLoader.Sessions

    Public Class SessionParameters

        Public strSelectedMenu As String
        Public strSelectedOption As String
        Public intPasswordErrorCode As Integer
        Public strUserLogin As String
        Public intSelectedProfile As Integer
        Public strSelectedProfile As String
        Public intSelectedCustomer As Integer
        Public strSelectedCustomer As String
        Public intSelectedGroup As Integer
        Public strtSelectedGroup As String
        Public StrTerminalID As String
        Public StrTerminalRecord As String
        Public StrNameKey As String
        Public StrFIeldName As String
        Public StrCode As String
        Public intTABLE_KEY_CODE As String
        Public strEmvApplication As String
        Public StrAcquirerCode As String
        Public StrIsserCode As String
        Public StrIsExtended As Char
        Public intAcquirerCode As String
        Public StrFieldDisplayName As String
        Public StrContentDesc As String
        Public StrIssuerName As String
        Public StrAcquirerName As String
        Public StrParameterType As String
        Public StrParamterEMV As String
        Public StrCardRangeCode As String
        Public StrCardRangeName As String
        Public StrStisCode As String
        Public intTERMINAL_REC_NO As Char
        Public intACQUIRER_CODE As Char
        Public intEmvApplication As Char
        Public StrExtraParameter As String

        Public intExtraApplication As Char

        Public intISSUER_CODE As Char
        Public intCARD_CODE As Char
        Public strSelectedModule As String
        Public strSelectedTerminalSerial As String
        Public intSelectedModule As Integer
        Public strAppFilename As String
        Public intEMVConfigId As Integer
        Public intEMVKeyId As Integer

        'Reports
        Public intReportType As Integer
        Public intSelectTID As String
        Public intSelectedMerchantID As String
        Public CodeTranMode As String
        Public UsuarioID As String
        Public strReportName As String
        Public dateStartDate As Date
        Public dateEndDate As Date
        Public dateFechaInicial As Date
        Public dateFechaFinal As Date
        Public intSelectedUserID As Integer
        Public strSelectedUserName As String
        Public intSelectedGroupID As Integer
        Public strSelectedGroupName As String
        Public intSelectedTerminalID As Integer
        Public strSelectedTerminalName As String
        Public strTipoReporte As String
        Public intSelectedTipoID As Integer
        Public strSelectedTipoName As String
        Public strSerialTerminal As String

        Public intSelectTIDHis As String
        Public intSelectedMerchantIDHis As String
        Public CodeTranModeHis As String
        Public UsuarioIDHis As String

        Public srtSelectTerminalImeiName As String

        'Deploy file one terminal
        Public intSelectedTerminal As Integer
        Public strSelectedSerialTerminal As String
        Public booleanIsAddFileTerminal As Boolean
        Public codigoConfiguracion As String
        Public codigoConfiguracionCodigoBarras As String


    End Class

End Namespace
