﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic

Namespace TeleLoader.Suppliers

    Public Class Supplier

        Private strConnString As String
        Private intSupplierID As Integer
        Private strCodigo As String
        Private strName As String
        Private strEANCode As String
        Private strNIT As String
        Private strBillCompanyId As String
        Private strDescription As String
        Private strValidarFecha As Boolean
        Private strPermitirTxBanco As String
        Private strPermitirMontoVariable As String
        Private strValidarFechaVencimiento As Boolean
        Private strPermiteRecaudoManual As Boolean
        Private strPermiteMontoVariable As String
        Private proveedorID As Integer
        Private codigoBanco As String
        Private flagBanco As String
        Private entidadID As Integer
        'Bar Code Stuff
        Private intBarCodeLength As Integer
        Private intRef1Length As Integer
        Private intRef2Length As Integer
        Private boolCompleteReferences As Boolean
        Private intBillNumberInitPos As Integer
        Private intBillNumberLength As Integer
        Private intClientInitPos As Integer
        Private intClientLength As Integer
        Private intAmountInitPos As Integer
        Private intAmountLength As Integer
        'Send Files Stuff
        Private strStartTimePartials As String
        Private strIntervalPartials As String
        Private strEndTimePartials As String
        Private strEndTimeTotals As String
        Private strWorkingDays As String
        Private intFileFormatType As Integer
        Private intServerType As Integer
        Private strServerIp As String
        Private strServerPort As String
        Private strUser As String
        Private strPassword As String
        Private strTotalsFolder As String
        Private strPartialsFolder As String
        Private strEmail As String
        Private strSerial As String
        Private strObservacion As String

        Public Sub New(ByVal strConnString As String)
            Me.strConnString = strConnString
        End Sub

        Public Sub New(ByVal strConnString As String, ByVal supplierId As Integer)
            Me.strConnString = strConnString
            Me.intSupplierID = supplierId
        End Sub

        Public Sub New()

            Dim configurationSection As ConnectionStringsSection = _
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

            strConnString = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString
        End Sub

        ''' <summary>
        ''' Código del Proveedor
        ''' </summary>
        Public Property SupplierCode() As String
            Get
                Return strCodigo
            End Get
            Set(ByVal value As String)
                strCodigo = value
            End Set
        End Property

        ''' <summary>
        ''' Nombre del Proveedor
        ''' </summary>
        Public Property SupplierName() As String
            Get
                Return strName
            End Get
            Set(ByVal value As String)
                strName = value
            End Set
        End Property

        ''' <summary>
        ''' Código EAN del Proveedor
        ''' </summary>
        Public Property SupplierEANCode() As String
            Get
                Return strEANCode
            End Get
            Set(ByVal value As String)
                strEANCode = value
            End Set
        End Property

        ''' <summary>
        ''' NIT del Proveedor
        ''' </summary>
        Public Property SupplierNIT() As String
            Get
                Return strNIT
            End Get
            Set(ByVal value As String)
                strNIT = value
            End Set
        End Property

        ''' <summary>
        ''' Código Empresa Recaudadora
        ''' </summary>
        Public Property SupplierBillCompanyId() As String
            Get
                Return strBillCompanyId
            End Get
            Set(ByVal value As String)
                strBillCompanyId = value
            End Set
        End Property

        ''' <summary>
        ''' Descripción del Proveedor
        ''' </summary>
        Public Property SupplierDescription() As String
            Get
                Return strDescription
            End Get
            Set(ByVal value As String)
                strDescription = value
            End Set
        End Property

        ''' <summary>
        ''' Permitir Validar la Fecha de vencimiento de la factura
        ''' </summary>
        Public Property SupplierValidarFecha() As Boolean
            Get
                Return strValidarFecha
            End Get
            Set(ByVal value As Boolean)
                strValidarFecha = value
            End Set
        End Property

        ''' <summary>
        ''' Permitir Transar el Proveedor por el Banco
        ''' </summary>
        Public Property SupplierPermitirTxBanco() As String
            Get
                Return strPermitirTxBanco
            End Get
            Set(ByVal value As String)
                strPermitirTxBanco = value
            End Set
        End Property

        ''' <summary>
        ''' Permitir Transar el Proveedor por el Banco
        ''' </summary>
        Public Property SupplierPermitirMontoVariable() As String
            Get
                Return strPermitirMontoVariable
            End Get
            Set(ByVal value As String)
                strPermitirMontoVariable = value
            End Set
        End Property

        ''' <summary>
        ''' Permitir Validar la fecha de vencimiento de la factura
        ''' </summary>
        Public Property SupplierValidarFechaVencimiento() As Boolean
            Get
                Return strValidarFechaVencimiento
            End Get
            Set(ByVal value As Boolean)
                strValidarFechaVencimiento = value
            End Set
        End Property

        ''' <summary>
        ''' Permitir Validar la fecha de vencimiento de la factura
        ''' </summary>
        ''' 
        Public Property SupplierPermiteRecaudoManual() As Boolean
            Get
                Return strPermiteRecaudoManual
            End Get
            Set(ByVal value As Boolean)
                strPermiteRecaudoManual = value
            End Set
        End Property

        ''' <summary>
        ''' Permitir Validar la fecha de vencimiento de la factura
        ''' </summary>
        ''' 
        Public Property SupplierPermiteMontoVariable() As String
            Get
                Return strPermiteMontoVariable
            End Get
            Set(ByVal value As String)
                strPermiteMontoVariable = value
            End Set
        End Property

        ''' <summary>
        ''' Permitir Transar el Proveedor por el Banco
        ''' </summary>
        Public Property SupplierProveedorId() As Integer
            Get
                Return proveedorID
            End Get
            Set(ByVal value As Integer)
                proveedorID = value
            End Set
        End Property

        ''' <summary>
        ''' Permitir Transar el Proveedor por el Banco
        ''' </summary>
        Public Property SupplierCodigoBanco() As String
            Get
                Return codigoBanco
            End Get
            Set(ByVal value As String)
                codigoBanco = value
            End Set
        End Property

        ''' <summary>
        ''' Permitir Transar el Proveedor por el Banco
        ''' </summary>
        Public Property SupplierFlagBanco() As String
            Get
                Return flagBanco
            End Get
            Set(ByVal value As String)
                flagBanco = value
            End Set
        End Property

        ''' <summary>
        ''' Permitir Transar el Proveedor por el Banco
        ''' </summary>
        Public Property SupplierEntidadId() As Integer
            Get
                Return entidadID
            End Get
            Set(ByVal value As Integer)
                entidadID = value
            End Set
        End Property

        ''' <summary>
        ''' Serial del punto de venta
        ''' </summary>
        Public Property PointOfSaleSerial() As String
            Get
                Return strSerial
            End Get
            Set(ByVal value As String)
                strSerial = value
            End Set
        End Property

        ''' <summary>
        ''' Observacion del punto de venta
        ''' </summary>
        Public Property PointOfSaleObservation() As String
            Get
                Return strObservacion
            End Get
            Set(ByVal value As String)
                strObservacion = value
            End Set
        End Property

        ''' <summary>
        ''' Código del Proveedor
        ''' </summary>
        Public Property SupplierID() As Integer
            Get
                Return intSupplierID
            End Get
            Set(ByVal value As Integer)
                intSupplierID = value
            End Set
        End Property

#Region "Bar Code Stuff"

        ''' <summary>
        ''' Longitud Código de Barras
        ''' </summary>
        Public Property BarCodeLength() As Integer
            Get
                Return intBarCodeLength
            End Get
            Set(ByVal value As Integer)
                intBarCodeLength = value
            End Set
        End Property

        ''' <summary>
        ''' Longitud Referencia 1
        ''' </summary>
        Public Property Ref1Length() As Integer
            Get
                Return intRef1Length
            End Get
            Set(ByVal value As Integer)
                intRef1Length = value
            End Set
        End Property

        ''' <summary>
        ''' Longitud Referencia 2
        ''' </summary>
        Public Property Ref2Length() As Integer
            Get
                Return intRef2Length
            End Get
            Set(ByVal value As Integer)
                intRef2Length = value
            End Set
        End Property

        ''' <summary>
        ''' Se deben completar las Referencias?
        ''' </summary>
        Public Property CompleteReferences() As Boolean
            Get
                Return boolCompleteReferences
            End Get
            Set(ByVal value As Boolean)
                boolCompleteReferences = value
            End Set
        End Property

        ''' <summary>
        ''' Posición Inicial Campo Factura
        ''' </summary>
        Public Property BillNumberInitPos() As Integer
            Get
                Return intBillNumberInitPos
            End Get
            Set(ByVal value As Integer)
                intBillNumberInitPos = value
            End Set
        End Property

        ''' <summary>
        ''' Longitud Campo Factura
        ''' </summary>
        Public Property BillNumberLength() As Integer
            Get
                Return intBillNumberLength
            End Get
            Set(ByVal value As Integer)
                intBillNumberLength = value
            End Set
        End Property

        ''' <summary>
        ''' Posición Inicial Campo Cliente
        ''' </summary>
        Public Property ClientInitPos() As Integer
            Get
                Return intClientInitPos
            End Get
            Set(ByVal value As Integer)
                intClientInitPos = value
            End Set
        End Property

        ''' <summary>
        ''' Longitud Campo Cliente
        ''' </summary>
        Public Property ClientLength() As Integer
            Get
                Return intClientLength
            End Get
            Set(ByVal value As Integer)
                intClientLength = value
            End Set
        End Property

        ''' <summary>
        ''' Posición Inicial Campo Valor
        ''' </summary>
        Public Property AmountInitPos() As Integer
            Get
                Return intAmountInitPos
            End Get
            Set(ByVal value As Integer)
                intAmountInitPos = value
            End Set
        End Property

        ''' <summary>
        ''' Longitud Campo Valor
        ''' </summary>
        Public Property AmountLength() As Integer
            Get
                Return intAmountLength
            End Get
            Set(ByVal value As Integer)
                intAmountLength = value
            End Set
        End Property

#End Region

#Region "Send File Stuff"

        ''' <summary>
        ''' Hora Inicio Envío de Parciales
        ''' </summary>
        Public Property StartTimePartials() As String
            Get
                Return strStartTimePartials
            End Get
            Set(ByVal value As String)
                strStartTimePartials = value
            End Set
        End Property

        ''' <summary>
        ''' Intervalo en Minutos para el Envío de Parciales
        ''' </summary>
        Public Property IntervalPartials() As String
            Get
                Return strIntervalPartials
            End Get
            Set(ByVal value As String)
                strIntervalPartials = value
            End Set
        End Property

        ''' <summary>
        ''' Hora Fin Envío de Parciales
        ''' </summary>
        Public Property EndTimePartials() As String
            Get
                Return strEndTimePartials
            End Get
            Set(ByVal value As String)
                strEndTimePartials = value
            End Set
        End Property

        ''' <summary>
        ''' Hora Envío de Totales
        ''' </summary>
        Public Property EndTimeTotals() As String
            Get
                Return strEndTimeTotals
            End Get
            Set(ByVal value As String)
                strEndTimeTotals = value
            End Set
        End Property

        ''' <summary>
        ''' Días de Trabajo para envío de Archivos
        ''' </summary>
        Public Property WorkingDays() As String
            Get
                Return strWorkingDays
            End Get
            Set(ByVal value As String)
                strWorkingDays = value
            End Set
        End Property

        ''' <summary>
        ''' Tipo de Formato para Crear el Archivo
        ''' </summary>
        Public Property FileFormatType() As Integer
            Get
                Return intFileFormatType
            End Get
            Set(ByVal value As Integer)
                intFileFormatType = value
            End Set
        End Property

        ''' <summary>
        ''' Tipo de Servidor para Envío de Archivos
        ''' </summary>
        Public Property ServerType() As Integer
            Get
                Return intServerType
            End Get
            Set(ByVal value As Integer)
                intServerType = value
            End Set
        End Property

        ''' <summary>
        ''' IP del Servidor FTP / SFTP
        ''' </summary>
        Public Property ServerIp() As String
            Get
                Return strServerIp
            End Get
            Set(ByVal value As String)
                strServerIp = value
            End Set
        End Property

        ''' <summary>
        ''' Puerto del Servidor FTP / SFTP
        ''' </summary>
        Public Property ServerPort() As String
            Get
                Return strServerPort
            End Get
            Set(ByVal value As String)
                strServerPort = value
            End Set
        End Property

        ''' <summary>
        ''' Usuario de Login FTP / SFTP
        ''' </summary>
        Public Property User() As String
            Get
                Return strUser
            End Get
            Set(ByVal value As String)
                strUser = value
            End Set
        End Property

        ''' <summary>
        ''' Clave de Login FTP / SFTP
        ''' </summary>
        Public Property Password() As String
            Get
                Return strPassword
            End Get
            Set(ByVal value As String)
                strPassword = value
            End Set
        End Property

        ''' <summary>
        ''' Carpeta Destino de Totales FTP / SFTP
        ''' </summary>
        Public Property TotalsFolder() As String
            Get
                Return strTotalsFolder
            End Get
            Set(ByVal value As String)
                strTotalsFolder = value
            End Set
        End Property

        ''' <summary>
        ''' Carpeta Destino de Parciales FTP / SFTP
        ''' </summary>
        Public Property PartialsFolder() As String
            Get
                Return strPartialsFolder
            End Get
            Set(ByVal value As String)
                strPartialsFolder = value
            End Set
        End Property

        ''' <summary>
        ''' Correo Electrónico Destino
        ''' </summary>
        Public Property Email() As String
            Get
                Return strEmail
            End Get
            Set(ByVal value As String)
                strEmail = value
            End Set
        End Property

#End Region

        ''' <summary>
        ''' Crear Proveedor
        ''' </summary>
        Public Function createSupplier() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webCrearProveedor"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("supplierName", strName))
                command.Parameters.Add(New SqlParameter("supplierEANCode", strEANCode))
                command.Parameters.Add(New SqlParameter("supplierNIT", strNIT))
                command.Parameters.Add(New SqlParameter("supplierBillCompanyId", strBillCompanyId))
                command.Parameters.Add(New SqlParameter("supplierDesc", strDescription))
                command.Parameters.Add(New SqlParameter("permitirTxBanco", strPermitirTxBanco))
                command.Parameters.Add(New SqlParameter("validarFechaVencimiento", strValidarFechaVencimiento))
                command.Parameters.Add(New SqlParameter("permiteRecaudoManual", strPermiteRecaudoManual))
                command.Parameters.Add(New SqlParameter("permiteMontoVariable", strPermiteMontoVariable))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        Public Function createCodeBank() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webCrearCodigoBancoProveedor"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("proveedorID", proveedorID))
                command.Parameters.Add(New SqlParameter("codigoBanco", codigoBanco))
                command.Parameters.Add(New SqlParameter("flagBanco", flagBanco))
                command.Parameters.Add(New SqlParameter("entidadId", entidadID))

                'Ejecutar SP
                results = command.ExecuteReader()

                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        ''' <summary>
        ''' Leer Datos del Proveedor
        ''' </summary>
        Public Sub getSupplierData()
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webConsultarDatosProveedor"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("supplierId", intSupplierID))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        strName = results.GetString(0)
                        strEANCode = results.GetString(1)
                        strNIT = results.GetString(2)
                        strBillCompanyId = results.GetString(3)
                        strDescription = results.GetString(4)
                        strValidarFechaVencimiento = results.GetBoolean(5)
                        strPermiteRecaudoManual = results.GetBoolean(6)
                        strPermitirTxBanco = results.GetString(7)
                        strPermitirMontoVariable = results.GetString(8)
                    End While
                Else
                    strName = ""
                    strEANCode = ""
                    strNIT = ""
                    strBillCompanyId = ""
                    strDescription = ""
                    strPermitirTxBanco = ""
                    strPermitirMontoVariable = ""
                    strValidarFechaVencimiento = False
                    strPermiteRecaudoManual = False
                End If

            Catch ex As Exception

            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

        End Sub

        ''' <summary>
        ''' Leer Datos del Proveedor Referentes al Código de Barras
        ''' </summary>
        Public Sub getSupplierBarCodeData()
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webConsultarDatosProveedorCodigoBarras"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("supplierId", intSupplierID))

                'Ejecutar SP
                results = command.ExecuteReader()

                If results.HasRows Then
                    While results.Read()
                        strEANCode = results.GetString(0)
                        intBarCodeLength = results.GetInt16(1)
                        intRef1Length = results.GetInt16(2)
                        intRef2Length = results.GetInt16(3)
                        boolCompleteReferences = results.GetBoolean(4)
                        intBillNumberInitPos = results.GetInt16(5)
                        intBillNumberLength = results.GetInt16(6)
                        intClientInitPos = results.GetInt16(7)
                        intClientLength = results.GetInt16(8)
                        intAmountInitPos = results.GetInt16(9)
                        intAmountLength = results.GetInt16(10)
                    End While
                Else
                    strEANCode = ""
                    intBarCodeLength = 0
                    intRef1Length = 0
                    intRef2Length = 0
                    boolCompleteReferences = False
                    intBillNumberInitPos = 0
                    intBillNumberLength = 0
                    intClientInitPos = 0
                    intClientLength = 0
                    intAmountInitPos = 0
                    intAmountLength = 0
                End If

            Catch ex As Exception

            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

        End Sub

        ''' <summary>
        ''' Leer Datos del Proveedor Referentes al Envío de Archivos
        ''' </summary>
        Public Sub getSupplierOutputFilesData()
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webConsultarDatosProveedorEnvioArchivos"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("supplierId", intSupplierID))

                'Ejecutar SP
                results = command.ExecuteReader()

                If results.HasRows Then
                    While results.Read()
                        strStartTimePartials = results.GetString(0)
                        strIntervalPartials = results.GetString(1)
                        strEndTimePartials = results.GetString(2)
                        strEndTimeTotals = results.GetString(3)
                        strWorkingDays = results.GetString(4)
                        intFileFormatType = results.GetInt64(5)
                        intServerType = results.GetInt64(6)
                        strServerIp = results.GetString(7)
                        strServerPort = results.GetString(8)
                        strUser = results.GetString(9)
                        strPassword = results.GetString(10)
                        strTotalsFolder = results.GetString(11)
                        strPartialsFolder = results.GetString(12)
                        strEmail = results.GetString(13)
                    End While
                Else
                    strStartTimePartials = "06:00"
                    strIntervalPartials = "5"
                    strEndTimePartials = "06:00"
                    strEndTimeTotals = "06:00"
                    strWorkingDays = ""
                    intFileFormatType = -1
                    intServerType = -1
                End If

            Catch ex As Exception

            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

        End Sub

        ''' <summary>
        ''' Editar Proveedor
        ''' </summary>
        Public Function editSupplier() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webActualizarProveedor"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("supplierId", intSupplierID))
                command.Parameters.Add(New SqlParameter("supplierName", strName))
                command.Parameters.Add(New SqlParameter("supplierEANCode", strEANCode))
                command.Parameters.Add(New SqlParameter("supplierNIT", strNIT))
                command.Parameters.Add(New SqlParameter("supplierBillCompanyId", strBillCompanyId))
                command.Parameters.Add(New SqlParameter("supplierDesc", strDescription))
                command.Parameters.Add(New SqlParameter("permitirTxBanco", strPermitirTxBanco))
                command.Parameters.Add(New SqlParameter("validarFechaVencimiento", strValidarFechaVencimiento))
                command.Parameters.Add(New SqlParameter("permiteRecaudoManual", strPermiteRecaudoManual))
                command.Parameters.Add(New SqlParameter("permiteMontoVariable", strPermiteMontoVariable))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        ''' <summary>
        ''' Grabar Parámetros Código de Barras Proveedor
        ''' </summary>
        Public Function saveBarCodeParametersSupplier() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webCrearParametrosCodigoBarrasProveedor"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("supplierId", intSupplierID))
                command.Parameters.Add(New SqlParameter("barCodeLength", intBarCodeLength))
                command.Parameters.Add(New SqlParameter("ref1Length", intRef1Length))
                command.Parameters.Add(New SqlParameter("ref2Length", intRef2Length))
                command.Parameters.Add(New SqlParameter("completeReferences", boolCompleteReferences))
                command.Parameters.Add(New SqlParameter("billNumberInitPos", intBillNumberInitPos))
                command.Parameters.Add(New SqlParameter("billNumberLength", intBillNumberLength))
                command.Parameters.Add(New SqlParameter("clientInitPos", intClientInitPos))
                command.Parameters.Add(New SqlParameter("clientLength", intClientLength))
                command.Parameters.Add(New SqlParameter("amountInitPos", intAmountInitPos))
                command.Parameters.Add(New SqlParameter("amountLength", intAmountLength))

                'Ejecutar SP
                results = command.ExecuteReader()

                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        ''' <summary>
        ''' Grabar Parámetros Envío de archivos
        ''' </summary>
        Public Function saveOutputFilesParametersSupplier() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webCrearParametrosEnvioArchivosProveedor"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("supplierId", intSupplierID))
                command.Parameters.Add(New SqlParameter("startTimePartials", strStartTimePartials))
                command.Parameters.Add(New SqlParameter("intervalPartials", strIntervalPartials))
                command.Parameters.Add(New SqlParameter("endTimePartials", strEndTimePartials))
                command.Parameters.Add(New SqlParameter("endTimeTotals", strEndTimeTotals))
                command.Parameters.Add(New SqlParameter("workingDays", strWorkingDays))
                command.Parameters.Add(New SqlParameter("fileFormatType", intFileFormatType))
                command.Parameters.Add(New SqlParameter("serverType", intServerType))
                command.Parameters.Add(New SqlParameter("serverIp", strServerIp))
                command.Parameters.Add(New SqlParameter("serverPort", strServerPort))
                command.Parameters.Add(New SqlParameter("user", strUser))
                command.Parameters.Add(New SqlParameter("password", strPassword))
                command.Parameters.Add(New SqlParameter("totalsFolder", strTotalsFolder))
                command.Parameters.Add(New SqlParameter("partialsFolder", strPartialsFolder))
                command.Parameters.Add(New SqlParameter("email", strEmail))

                'Ejecutar SP
                results = command.ExecuteReader()

                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        ''' <summary>
        ''' Leer Lista de Proveedores
        ''' </summary>
        Public Function getSuppliersList() As Dictionary(Of Long, String)
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim retList As New Dictionary(Of Long, String)

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webConsultarListaProveedores"

                'Ejecutar SP
                results = command.ExecuteReader()

                If results.HasRows Then
                    While results.Read()
                        retList.Add(results.GetInt64(0), results.GetString(1))
                    End While
                End If

            Catch ex As Exception

            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retList

        End Function

        ''' <summary>
        ''' Leer Nombre del Proveedor, Filtrado por Cuenta Banco Id
        ''' </summary>
        Public Sub getSupplierNameByAccountId(ByVal intAccountId As Integer)
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webConsultarNombreProveedorPorCuentaBancoId"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("bankAccountId", intAccountId))

                'Ejecutar SP
                results = command.ExecuteReader()

                If results.HasRows Then
                    While results.Read()
                        strName = results.GetString(0)
                    End While
                Else
                    strName = ""
                End If

            Catch ex As Exception

            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

        End Sub

        ''' <summary>
        ''' Mostrar la información de convenios
        ''' </summary>
        Public Function GetLstConvenios() As DataTable
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim adapter As New SqlDataAdapter()
            Dim TheDataReader As SqlDataReader
            Dim TablaDatos As New DataTable()
            TablaDatos.TableName = "DtDatos"
            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webConsultarProveedores"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("p_in_codigo", SupplierCode))
                command.Parameters.Add(New SqlParameter("p_in_nombre", strName))
                command.Parameters.Add(New SqlParameter("p_in_nit", strNIT))
                'Leer Resultado                 
                adapter = New System.Data.SqlClient.SqlDataAdapter
                adapter.SelectCommand = command
                adapter.Fill(TablaDatos)
            Catch ex As Exception

            Finally
                'Cerrar DataReader
                If Not (TheDataReader Is Nothing) Then
                    TheDataReader.Close()
                End If

                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return TablaDatos
        End Function

        ''' <summary>
        ''' Mostrar la información de punto de ventas
        ''' </summary>
        Public Function GetLstPuntoVenta() As DataTable
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim adapter As New SqlDataAdapter()
            Dim TheDataReader As SqlDataReader
            Dim TablaDatos As New DataTable()
            TablaDatos.TableName = "DtDatos"
            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webConsultarPuntoVenta"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("p_in_serial", PointOfSaleSerial))
                command.Parameters.Add(New SqlParameter("p_in_observacion", PointOfSaleObservation))
                'Leer Resultado                 
                adapter = New System.Data.SqlClient.SqlDataAdapter
                adapter.SelectCommand = command
                adapter.Fill(TablaDatos)
            Catch ex As Exception

            Finally
                'Cerrar DataReader
                If Not (TheDataReader Is Nothing) Then
                    TheDataReader.Close()
                End If

                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return TablaDatos
        End Function

    End Class

End Namespace
