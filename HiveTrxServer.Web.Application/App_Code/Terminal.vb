﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic

Namespace TeleLoader.Terminals

    Public Class Terminal

        Private strConnString As String

        Private strTerminalSerial As String

        Private strTerminalRecord As String
        Private strTerminalTID As String
        Private strCode As String

        Private intTerminalType As Integer
        Private intTerminalBrand As Integer
        Private intTerminalModel As Integer
        Private intTerminalInterface As Integer
        Private strSerialSIM As String
        Private strMobileSIM As String
        Private strIMEI As String
        Private strChargerSerial As String
        Private strBatterySerial As String
        Private strBiometricSerial As String
        Private strDesc As String
        Private boolGroupPriority As Boolean
        Private boolUpdateTerminal As Boolean
        Private dateScheduleUpdate As DateTime
        Private strIPAddress As String
        Private strPort As String
        Private boolValidateIMEI As Boolean
        Private boolValidateSIM As Boolean
        Private intTerminalStatus As Integer
        Private intGroupID As Integer
        Private intTerminalID As Integer
        Private intTERMINAL_REC_NO As Integer
        Private intTABLE_KEY_CODE As Char
        Private intEmvApplication As Char
        Private strLatitude As String
        Private strLongitude As String
        Private strAPN As String
        Private strBatteryLevel As String
        Private strSignalLevel As String
        Private dateUpdatedAuto As DateTime
        Private objContact As TeleLoader.Contact
        'Modificación MDM; EB: 30/Ene/2018
        'Campos MDM
        Private strDeviceIP As String
        Private strHeraclesID As String
        Private strHeraclesName As String
        Private strHeraclesCity As String
        Private strHeraclesRegion As String
        Private strHeraclesAgency As String
        Private boolAndroidGroup As Boolean
        'Modificación MDM; EB: 31/Ene/2018
        Private boolChangePasswordTerminal As Boolean
        Private strNewPasswordTerminal As String
        Private strNewMessageTerminal As String
        Private boolLockTerminal As Boolean
        Private strAllowedApps As String
        Private strInstalledApps As String
        'Modificación Carga de Llaves; EB: 28/Feb/2018
        Private intComponentType As Integer
        Private strComponent1 As String
        Private strComponent2 As String
        Private strWorkingKey As String
        Private strEncryptionKey As String
        'Modificación Carga de Llaves; EB: 01/Mar/2018
        Private boolComponentsInjected As Boolean
        Private boolKeyLoaded As Boolean
        Private boolReloadKey As Boolean
        'Modificacion MDM; FM: 4/May/2018
        Private strKioskoApp As String
        'Modificacion MDM; OG: 7/May/2018
        Private strActualMessage As String
        'Modificacion RD; OG: 7/May/2018
        Private boolBlockingMode As Boolean
        Private boolUpdateNow As Boolean

        Private boolInitialize As Boolean
        Private boolDownloadKey As Boolean
        Private strMCC As String
        Private strMNC As String
        Private strLCA As String
        Private strCID As String

        'Datos Pstis
        Private ter_fecha_ultima_inicializacion As String
        Private TER_NUMERO_COMERCIO_1 As String
        Private TER_TID_1 As String
        Private TER_NOMBRE_COMERCIO_1 As String
        Private TER_TEL_COMERCIO_1 As String
        Private TER_NUMERO_COMERCIO_2 As String
        Private TER_TID_2 As String
        Private TER_NOMBRE_COMERCIO_2 As String
        Private TER_TEL_COMERCIO_2 As String

        Public Sub New(ByVal strConnString As String)
            Me.strConnString = strConnString
        End Sub

        Public Sub New(ByVal strConnString As String, ByVal groupId As Integer)
            Me.strConnString = strConnString
            Me.intGroupID = groupId
        End Sub

        Public Sub New()

            Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

            strConnString = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString

        End Sub
        Public Property FechaInicializacion() As String
            Get
                Return ter_fecha_ultima_inicializacion
            End Get
            Set(ByVal value As String)
                ter_fecha_ultima_inicializacion = value
            End Set
        End Property
        Public Property Comercio1() As String
            Get
                Return TER_NUMERO_COMERCIO_1
            End Get
            Set(ByVal value As String)
                TER_NUMERO_COMERCIO_1 = value
            End Set
        End Property
        Public Property Tid1() As String
            Get
                Return TER_TID_1
            End Get
            Set(ByVal value As String)
                TER_TID_1 = value
            End Set
        End Property
        Public Property NombreComercio1() As String
            Get
                Return TER_NOMBRE_COMERCIO_1
            End Get
            Set(ByVal value As String)
                TER_NOMBRE_COMERCIO_1 = value
            End Set
        End Property
        Public Property NumeroComercio1() As String
            Get
                Return TER_TEL_COMERCIO_1

            End Get
            Set(ByVal value As String)
                TER_TEL_COMERCIO_1 = value
            End Set
        End Property
        'Multicomercio
        Public Property Comercio2() As String
            Get
                Return TER_NUMERO_COMERCIO_2
            End Get
            Set(ByVal value As String)
                TER_NUMERO_COMERCIO_2 = value
            End Set
        End Property
        Public Property Tid2() As String
            Get
                Return TER_TID_2
            End Get
            Set(ByVal value As String)
                TER_TID_2 = value
            End Set
        End Property
        Public Property NombreComercio2() As String
            Get
                Return TER_NOMBRE_COMERCIO_2
            End Get
            Set(ByVal value As String)
                TER_NOMBRE_COMERCIO_2 = value
            End Set
        End Property
        Public Property NumeroComercio2() As String
            Get
                Return TER_TEL_COMERCIO_2

            End Get
            Set(ByVal value As String)
                TER_TEL_COMERCIO_2 = value
            End Set
        End Property



        Public Property TerminalSerial() As String
            Get
                Return strTerminalSerial
            End Get
            Set(ByVal value As String)
                strTerminalSerial = value
            End Set
        End Property

        Public Property TerminalRecord() As String
            Get
                Return strTerminalRecord
            End Get
            Set(ByVal value As String)
                strTerminalRecord = value
            End Set
        End Property
        Public Property MCC() As String
            Get
                Return strMCC
            End Get
            Set(ByVal value As String)
                strMCC = value
            End Set
        End Property

        Public Property MNC() As String
            Get
                Return strMNC
            End Get
            Set(ByVal value As String)
                strMNC = value
            End Set
        End Property

        Public Property LCA() As String
            Get
                Return strLCA
            End Get
            Set(ByVal value As String)
                strLCA = value
            End Set
        End Property

        Public Property CID() As String
            Get
                Return strCID
            End Get
            Set(ByVal value As String)
                strCID = value
            End Set
        End Property

        Public Property TerminalTID() As Integer
            Get
                Return strTerminalTID
            End Get
            Set(ByVal value As Integer)
                strTerminalTID = value
            End Set
        End Property
        Public Property ReferCode() As Integer
            Get
                Return strCode
            End Get
            Set(ByVal value As Integer)
                strCode = value
            End Set
        End Property

        Public Property SerialSIM() As String
            Get
                Return strSerialSIM
            End Get
            Set(ByVal value As String)
                strSerialSIM = value
            End Set
        End Property

        Public Property MobileSIM() As String
            Get
                Return strMobileSIM
            End Get
            Set(ByVal value As String)
                strMobileSIM = value
            End Set
        End Property

        Public Property IMEI() As String
            Get
                Return strIMEI
            End Get
            Set(ByVal value As String)
                strIMEI = value
            End Set
        End Property

        Public Property ChargerSerial() As String
            Get
                Return strChargerSerial
            End Get
            Set(ByVal value As String)
                strChargerSerial = value
            End Set
        End Property

        Public Sub SetFlagComponentsInjected()
            Throw New NotImplementedException()
        End Sub

        Public Property BatterySerial() As String
            Get
                Return strBatterySerial
            End Get
            Set(ByVal value As String)
                strBatterySerial = value
            End Set
        End Property

        Public Property BiometricSerial() As String
            Get
                Return strBiometricSerial
            End Get
            Set(ByVal value As String)
                strBiometricSerial = value
            End Set
        End Property

        Public Property Desc() As String
            Get
                Return strDesc
            End Get
            Set(ByVal value As String)
                strDesc = value
            End Set
        End Property

        Public Property IPAddress() As String
            Get
                Return strIPAddress
            End Get
            Set(ByVal value As String)
                strIPAddress = value
            End Set
        End Property

        Public Property Port() As String
            Get
                Return strPort
            End Get
            Set(ByVal value As String)
                strPort = value
            End Set
        End Property

        Public Property TerminalType() As Integer
            Get
                Return intTerminalType
            End Get
            Set(ByVal value As Integer)
                intTerminalType = value
            End Set
        End Property

        Public Property TerminalBrand() As Integer
            Get
                Return intTerminalBrand
            End Get
            Set(ByVal value As Integer)
                intTerminalBrand = value
            End Set
        End Property

        Public Property TerminalModel() As Integer
            Get
                Return intTerminalModel
            End Get
            Set(ByVal value As Integer)
                intTerminalModel = value
            End Set
        End Property

        Public Property TerminalInterface() As Integer
            Get
                Return intTerminalInterface
            End Get
            Set(ByVal value As Integer)
                intTerminalInterface = value
            End Set
        End Property

        Public Property TerminalStatus() As Integer
            Get
                Return intTerminalStatus
            End Get
            Set(ByVal value As Integer)
                intTerminalStatus = value
            End Set
        End Property

        Public Property Contact() As TeleLoader.Contact
            Get
                Return objContact
            End Get
            Set(ByVal value As TeleLoader.Contact)
                objContact = value
            End Set
        End Property

        Public Property GroupPriority() As Boolean
            Get
                Return boolGroupPriority
            End Get
            Set(ByVal value As Boolean)
                boolGroupPriority = value
            End Set
        End Property

        Public Property BlockingMode() As Boolean
            Get
                Return boolBlockingMode
            End Get
            Set(ByVal value As Boolean)
                boolBlockingMode = value
            End Set
        End Property

        Public Property UpdateNow() As Boolean
            Get
                Return boolUpdateNow
            End Get
            Set(ByVal value As Boolean)
                boolUpdateNow = value
            End Set
        End Property

        Public Property UpdateTerminal() As Boolean
            Get
                Return boolUpdateTerminal
            End Get
            Set(ByVal value As Boolean)
                boolUpdateTerminal = value
            End Set
        End Property

        Public Property ValidateIMEI() As Boolean
            Get
                Return boolValidateIMEI
            End Get
            Set(ByVal value As Boolean)
                boolValidateIMEI = value
            End Set
        End Property

        Public Property ValidateSIM() As Boolean
            Get
                Return boolValidateSIM
            End Get
            Set(ByVal value As Boolean)
                boolValidateSIM = value
            End Set
        End Property

        Public Property Latitude() As String
            Get
                Return strLatitude
            End Get
            Set(ByVal value As String)
                strLatitude = value
            End Set
        End Property

        Public Property Longitude() As String
            Get
                Return strLongitude
            End Get
            Set(ByVal value As String)
                strLongitude = value
            End Set
        End Property

        Public Property APN() As String
            Get
                Return strAPN
            End Get
            Set(ByVal value As String)
                strAPN = value
            End Set
        End Property

        Public Property BatteryLevel() As String
            Get
                Return strBatteryLevel
            End Get
            Set(ByVal value As String)
                strBatteryLevel = value
            End Set
        End Property

        Public Property SignalLevel() As String
            Get
                Return strSignalLevel
            End Get
            Set(ByVal value As String)
                strSignalLevel = value
            End Set
        End Property

        Public Property DatetimeAutoUpdate() As DateTime
            Get
                Return dateUpdatedAuto
            End Get
            Set(ByVal value As DateTime)
                dateUpdatedAuto = value
            End Set
        End Property

        Public Property ScheduleUpdate() As DateTime
            Get
                Return dateScheduleUpdate
            End Get
            Set(ByVal value As DateTime)
                dateScheduleUpdate = value
            End Set
        End Property

        Public Property TerminalID() As Integer
            Get
                Return intTerminalID
            End Get
            Set(ByVal value As Integer)
                intTerminalID = value
            End Set
        End Property

        Public Property GroupID() As Integer
            Get
                Return intGroupID
            End Get
            Set(ByVal value As Integer)
                intGroupID = value
            End Set

        End Property


        'Modificación MDM; EB: 30/Ene/2018
        Public Property DeviceIP() As String
            Get
                Return strDeviceIP
            End Get
            Set(ByVal value As String)
                strDeviceIP = value
            End Set
        End Property

        'Modificación MDM; EB: 30/Ene/2018
        Public Property HeraclesID() As String
            Get
                Return strHeraclesID
            End Get
            Set(ByVal value As String)
                strHeraclesID = value
            End Set
        End Property

        'Modificación MDM; EB: 30/Ene/2018
        Public Property HeraclesName() As String
            Get
                Return strHeraclesName
            End Get
            Set(ByVal value As String)
                strHeraclesName = value
            End Set
        End Property

        'Modificación MDM; EB: 30/Ene/2018
        Public Property HeraclesCity() As String
            Get
                Return strHeraclesCity
            End Get
            Set(ByVal value As String)
                strHeraclesCity = value
            End Set
        End Property

        'Modificación MDM; EB: 30/Ene/2018
        Public Property HeraclesRegion() As String
            Get
                Return strHeraclesRegion
            End Get
            Set(ByVal value As String)
                strHeraclesRegion = value
            End Set
        End Property

        'Modificación MDM; EB: 30/Ene/2018
        Public Property HeraclesAgency() As String
            Get
                Return strHeraclesAgency
            End Get
            Set(ByVal value As String)
                strHeraclesAgency = value
            End Set
        End Property

        'Modificación MDM; EB: 30/Ene/2018
        Public Property isAndroidGroup() As Boolean
            Get
                Return boolAndroidGroup
            End Get
            Set(ByVal value As Boolean)
                boolAndroidGroup = value
            End Set
        End Property

        'Modificación MDM; EB: 31/Ene/2018
        ''' <summary>
        ''' Cambiar Clave a Terminal ?
        ''' </summary>
        Public Property ChangePasswordTerminal() As Boolean
            Get
                Return boolChangePasswordTerminal
            End Get
            Set(ByVal value As Boolean)
                boolChangePasswordTerminal = value
            End Set
        End Property

        'Modificación MDM; EB: 31/Ene/2018
        ''' <summary>
        ''' Nueva clave para terminal
        ''' </summary>
        Public Property NewPasswordTerminal() As String
            Get
                Return strNewPasswordTerminal
            End Get
            Set(ByVal value As String)
                strNewPasswordTerminal = value
            End Set
        End Property

        'Modificación MDM; EB: 31/Ene/2018
        ''' <summary>
        ''' Nuevo mensaje a desplegar en terminal
        ''' </summary>
        Public Property NewMessageTerminal() As String
            Get
                Return strNewMessageTerminal
            End Get
            Set(ByVal value As String)
                strNewMessageTerminal = value
            End Set
        End Property

        'Modificación MDM; OG: 7/May/2018
        ''' <summary>
        ''' Mensaje actual para el terminal
        ''' </summary>
        Public Property ActualMessage() As String
            Get
                Return strActualMessage
            End Get
            Set(ByVal value As String)
                strActualMessage = value
            End Set
        End Property

        'Modificación MDM; EB: 31/Ene/2018
        ''' <summary>
        ''' Bloquear Terminal ?
        ''' </summary>
        Public Property LockTerminal() As Boolean
            Get
                Return boolLockTerminal
            End Get
            Set(ByVal value As Boolean)
                boolLockTerminal = value
            End Set
        End Property

        'Modificación MDM; EB: 31/Ene/2018
        ''' <summary>
        ''' Aplicaciones Permitidas en terminal
        ''' </summary>
        Public Property AllowedApps() As String
            Get
                Return strAllowedApps
            End Get
            Set(ByVal value As String)
                strAllowedApps = value
            End Set
        End Property

        'Modificación MDM; FM: 4/May/2018
        ''' <summary>
        ''' Aplicacion modo Kiosko
        ''' </summary>
        Public Property KioskoApp() As String
            Get
                Return strKioskoApp
            End Get
            Set(ByVal value As String)
                strKioskoApp = value
            End Set
        End Property

        'Modificación MDM; EB: 31/Ene/2018
        ''' <summary>
        ''' Aplicaciones Instaladas en terminal
        ''' </summary>
        Public Property InstalledApps() As String
            Get
                Return strInstalledApps
            End Get
            Set(ByVal value As String)
                strInstalledApps = value
            End Set
        End Property

        'Modificación Carga de Llaves; EB: 28/Feb/2018        
        Public Property ComponentType() As Integer
            Get
                Return intComponentType
            End Get
            Set(ByVal value As Integer)
                intComponentType = value
            End Set
        End Property

        'Modificación Carga de Llaves; EB: 28/Feb/2018
        Public Property Component1() As String
            Get
                Return strComponent1
            End Get
            Set(ByVal value As String)
                strComponent1 = value
            End Set
        End Property

        'Modificación Carga de Llaves; EB: 28/Feb/2018
        Public Property Component2() As String
            Get
                Return strComponent2
            End Get
            Set(ByVal value As String)
                strComponent2 = value
            End Set
        End Property
        Public Property WorkingKey() As String
            Get
                Return strWorkingKey
            End Get
            Set(ByVal value As String)
                strWorkingKey = value
            End Set
        End Property


        'Modificación Carga de Llaves; EB: 28/Feb/2018
        Public Property EncryptionKey() As String
            Get
                Return strEncryptionKey
            End Get
            Set(ByVal value As String)
                strEncryptionKey = value
            End Set
        End Property

        'Modificación Carga de Llaves; EB: 01/Mar/2018
        Public Property ComponentsInjected() As Boolean
            Get
                Return boolComponentsInjected
            End Get
            Set(ByVal value As Boolean)
                boolComponentsInjected = value
            End Set
        End Property

        'Modificación Carga de Llaves; EB: 01/Mar/2018
        Public Property KeyLoaded() As Boolean
            Get
                Return boolKeyLoaded
            End Get
            Set(ByVal value As Boolean)
                boolKeyLoaded = value
            End Set
        End Property

        'Modificación Carga de Llaves; EB: 01/Mar/2018
        Public Property ReloadKey() As Boolean
            Get
                Return boolReloadKey
            End Get
            Set(ByVal value As Boolean)
                boolReloadKey = value
            End Set
        End Property

        ''' <summary>
        ''' Crear Terminal
        ''' </summary>
        Public Function createTerminal(ByRef rspCode As Int16, ByRef rspMsg As String) As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean = False

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webCrearTerminal"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("groupID", intGroupID))
                command.Parameters.Add(New SqlParameter("terminalSerial", strTerminalSerial))
                command.Parameters.Add(New SqlParameter("terminalType", intTerminalType))
                command.Parameters.Add(New SqlParameter("terminalBrand", intTerminalBrand))
                command.Parameters.Add(New SqlParameter("terminalModel", intTerminalModel))
                command.Parameters.Add(New SqlParameter("terminalInterface", intTerminalInterface))
                command.Parameters.Add(New SqlParameter("serialSIM", strSerialSIM))
                command.Parameters.Add(New SqlParameter("mobileNumberSIM", strMobileSIM))
                command.Parameters.Add(New SqlParameter("IMEI", strIMEI))
                command.Parameters.Add(New SqlParameter("chargerSerial", strChargerSerial))
                command.Parameters.Add(New SqlParameter("batterySerial", strBatterySerial))
                command.Parameters.Add(New SqlParameter("biometricSerial", strBiometricSerial))
                command.Parameters.Add(New SqlParameter("terminalDesc", strDesc))
                command.Parameters.Add(New SqlParameter("groupPriority", boolGroupPriority))
                command.Parameters.Add(New SqlParameter("updateTerminal", boolUpdateTerminal))
                command.Parameters.Add(New SqlParameter("scheduleDate", dateScheduleUpdate))
                command.Parameters.Add(New SqlParameter("updateIP", strIPAddress))
                command.Parameters.Add(New SqlParameter("updatePort", strPort))
                command.Parameters.Add(New SqlParameter("updateBlockingMode", boolBlockingMode))
                command.Parameters.Add(New SqlParameter("updateUpdateNow", boolUpdateNow))
                command.Parameters.Add(New SqlParameter("validateIMEI", boolValidateIMEI))
                command.Parameters.Add(New SqlParameter("validateSerialSIM", boolValidateSIM))
                command.Parameters.Add(New SqlParameter("contactIDNumber", objContact.IdentificationNumber))
                command.Parameters.Add(New SqlParameter("contactIDType", objContact.IdentificationType))
                command.Parameters.Add(New SqlParameter("contactName", objContact.Name))
                command.Parameters.Add(New SqlParameter("contactAddress", objContact.Address))
                command.Parameters.Add(New SqlParameter("contactMobile", objContact.Mobile))
                command.Parameters.Add(New SqlParameter("contactPhone", objContact.Phone))
                command.Parameters.Add(New SqlParameter("contactEmail", objContact.Email))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        rspCode = results.GetInt32(0)
                        rspMsg = results.GetString(1)
                    End While
                Else
                    rspCode = 0
                    rspMsg = "Terminal no creado en forma correcta."
                    retVal = False
                End If

                If rspCode = 1 Then
                    retVal = True
                End If

            Catch ex As Exception
                rspCode = 0
                rspMsg = "Terminal no creado en forma correcta (Error DB)"
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        ''' <summary>
        ''' Leer Datos de la Terminal
        ''' </summary>
        Public Sub getTerminalData()
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webConsultarDatosTerminal"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("terminalID", intTerminalID))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        strTerminalSerial = results.GetString(0)
                        intTerminalType = results.GetInt64(1)
                        intTerminalBrand = results.GetInt64(2)
                        intTerminalModel = results.GetInt64(3)
                        intTerminalInterface = results.GetInt64(4)
                        strSerialSIM = results.GetString(5)
                        strMobileSIM = results.GetString(6)
                        strIMEI = results.GetString(7)
                        strChargerSerial = results.GetString(8)
                        strBatterySerial = results.GetString(9)
                        strBiometricSerial = results.GetString(10)
                        strDesc = results.GetString(11)
                        boolGroupPriority = results.GetBoolean(12)
                        boolUpdateTerminal = results.GetBoolean(13)
                        dateScheduleUpdate = results.GetDateTime(14)
                        strIPAddress = results.GetString(15)
                        strPort = results.GetString(16)
                        intTerminalStatus = results.GetInt64(17)
                        boolValidateIMEI = results.GetBoolean(18)
                        boolValidateSIM = results.GetBoolean(19)
                        strLatitude = results.GetString(20)
                        strLongitude = results.GetString(21)
                        strAPN = results.GetString(22)
                        strBatteryLevel = results.GetInt32(23)
                        strSignalLevel = results.GetInt32(24)
                        dateUpdatedAuto = results.GetDateTime(25)
                        intGroupID = results.GetInt64(26)
                        objContact = New Contact()
                        objContact.IdentificationNumber = results.GetString(27)
                        objContact.IdentificationType = results.GetInt64(28)
                        objContact.Name = results.GetString(29)
                        objContact.Address = results.GetString(30)
                        objContact.Mobile = results.GetString(31)
                        objContact.Phone = results.GetString(32)
                        objContact.Email = results.GetString(33)
                        'Modificación MDM; EB: 30/Ene/2018
                        'Campos MDM
                        strDeviceIP = results.GetString(34)
                        strHeraclesID = results.GetString(35)
                        strHeraclesName = results.GetString(36)
                        strHeraclesCity = results.GetString(37)
                        strHeraclesRegion = results.GetString(38)
                        strHeraclesAgency = results.GetString(39)
                        boolAndroidGroup = results.GetBoolean(40)
                        'Modificación MDM; EB: 31/Ene/2018
                        strAllowedApps = results.GetString(41)
                        strInstalledApps = results.GetString(42)
                        'Modificación MDM; FM: 4/May/2018
                        strKioskoApp = results.GetString(43)
                        'Modificación MDM; OG: 7/May/2018
                        strActualMessage = results.GetString(44)
                        'Modificación RD; OG: 7/May/2018
                        boolBlockingMode = results.GetBoolean(45)
                        boolUpdateNow = results.GetBoolean(46)
                        boolInitialize = results.GetBoolean(47)
                        ter_fecha_ultima_inicializacion = results.GetString(48)
                        TER_NUMERO_COMERCIO_1 = results.GetString(49)
                        TER_TID_1 = results.GetString(50)
                        TER_NOMBRE_COMERCIO_1 = results.GetString(51)
                        TER_TEL_COMERCIO_1 = results.GetString(52)
                        TER_NUMERO_COMERCIO_2 = results.GetString(53)
                        TER_TID_2 = results.GetString(54)
                        TER_NOMBRE_COMERCIO_2 = results.GetString(55)
                        TER_TEL_COMERCIO_2 = results.GetString(56)
                        strMCC = results.GetString(57)
                        strMNC = results.GetString(58)
                        strLCA = results.GetString(59)
                        strCID = results.GetString(60)
                        boolDownloadKey = results.GetBoolean(61)

                    End While
                Else
                    strTerminalSerial = ""
                    intTerminalType = -1
                    intTerminalBrand = -1
                    intTerminalModel = -1
                    intTerminalInterface = -1
                    strSerialSIM = ""
                    strMobileSIM = ""
                    strIMEI = ""
                    strChargerSerial = ""
                    strBatterySerial = ""
                    strBiometricSerial = ""
                    strDesc = ""
                    boolGroupPriority = False
                    boolUpdateTerminal = False
                    dateScheduleUpdate = Nothing
                    strIPAddress = ""
                    strPort = ""
                    intTerminalStatus = 0
                    boolValidateIMEI = False
                    boolValidateSIM = False
                    strLatitude = ""
                    strLongitude = ""
                    strAPN = ""
                    strBatteryLevel = ""
                    strSignalLevel = ""
                    dateUpdatedAuto = Nothing
                    objContact = New Contact()
                    objContact.IdentificationNumber = ""
                    objContact.IdentificationType = -1
                    objContact.Name = ""
                    objContact.Address = ""
                    objContact.Mobile = ""
                    objContact.Phone = ""
                    objContact.Email = ""
                    'Modificación MDM; EB: 30/Ene/2018
                    'Campos MDM
                    strDeviceIP = ""
                    strHeraclesID = ""
                    strHeraclesName = ""
                    strHeraclesCity = ""
                    strHeraclesRegion = ""
                    strHeraclesAgency = ""
                    boolAndroidGroup = False
                    'Modificación MDM; EB: 31/Ene/2018
                    strAllowedApps = ""
                    strInstalledApps = ""
                    'Modificación MDM; FM: 4/May/2018
                    strKioskoApp = ""
                    'Modificación MDM; OG: 7/May/2018
                    strActualMessage = ""
                    'Modificación RD; OG: 7/May/2018
                    boolBlockingMode = False
                    boolUpdateNow = False
                    boolInitialize = False
                    boolDownloadKey = False

                End If

            Catch ex As Exception
                Dim mensahe As String = ex.Message()
                Console.WriteLine(mensahe)
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

        End Sub

        ''' <summary>
        ''' Leer Datos de la Terminal
        ''' </summary>
        Public Sub getTerminalDataSTIS()
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webConsultarDatosTerminalSTIS"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("TERMINAL_REC_NO", intTERMINAL_REC_NO))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        strTerminalRecord = results.GetString(0)
                        strTerminalTID = results.GetString(1)
                        strCode = results.GetString(2)
                    End While
                Else
                    strTerminalRecord = ""
                    strTerminalTID = ""

                End If

            Catch ex As Exception

            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

        End Sub

        ''' <summary>
        ''' Editar Terminal
        ''' </summary>
        Public Function editTerminal(ByRef rspCode As Int16, ByRef rspMsg As String) As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim retVal As Boolean = False

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webEditarTerminal"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("terminalID", intTerminalID))
                command.Parameters.Add(New SqlParameter("terminalSerial", strTerminalSerial))
                command.Parameters.Add(New SqlParameter("terminalType", intTerminalType))
                command.Parameters.Add(New SqlParameter("terminalBrand", intTerminalBrand))
                command.Parameters.Add(New SqlParameter("terminalModel", intTerminalModel))
                command.Parameters.Add(New SqlParameter("terminalInterface", intTerminalInterface))
                command.Parameters.Add(New SqlParameter("serialSIM", strSerialSIM))
                command.Parameters.Add(New SqlParameter("mobileNumberSIM", strMobileSIM))
                command.Parameters.Add(New SqlParameter("IMEI", strIMEI))
                command.Parameters.Add(New SqlParameter("chargerSerial", strChargerSerial))
                command.Parameters.Add(New SqlParameter("batterySerial", strBatterySerial))
                command.Parameters.Add(New SqlParameter("biometricSerial", strBiometricSerial))
                command.Parameters.Add(New SqlParameter("terminalDesc", strDesc))
                command.Parameters.Add(New SqlParameter("groupPriority", boolGroupPriority))
                command.Parameters.Add(New SqlParameter("updateTerminal", boolUpdateTerminal))
                command.Parameters.Add(New SqlParameter("scheduleDate", dateScheduleUpdate))
                command.Parameters.Add(New SqlParameter("updateIP", strIPAddress))
                command.Parameters.Add(New SqlParameter("updatePort", strPort))
                command.Parameters.Add(New SqlParameter("updateBlockingMode", boolBlockingMode))
                command.Parameters.Add(New SqlParameter("updateUpdateNow", boolUpdateNow))
                command.Parameters.Add(New SqlParameter("validateIMEI", boolValidateIMEI))
                command.Parameters.Add(New SqlParameter("validateSerialSIM", boolValidateSIM))
                command.Parameters.Add(New SqlParameter("terminalStatus", intTerminalStatus))
                command.Parameters.Add(New SqlParameter("terminalGroupId", intGroupID))
                command.Parameters.Add(New SqlParameter("contactIDNumber", objContact.IdentificationNumber))
                command.Parameters.Add(New SqlParameter("contactIDType", objContact.IdentificationType))
                command.Parameters.Add(New SqlParameter("contactName", objContact.Name))
                command.Parameters.Add(New SqlParameter("contactAddress", objContact.Address))
                command.Parameters.Add(New SqlParameter("contactMobile", objContact.Mobile))
                command.Parameters.Add(New SqlParameter("contactPhone", objContact.Phone))
                command.Parameters.Add(New SqlParameter("contactEmail", objContact.Email))
                'Modificación MDM; EB: 01/Feb/2018
                command.Parameters.Add(New SqlParameter("allowedApps", AllowedApps))
                'Modificación MDM; FM: 4/May/2018
                command.Parameters.Add(New SqlParameter("kioskoApp", KioskoApp))

                command.Parameters.Add(New SqlParameter("Inicializacion", boolInitialize))
                'Ejecutar SP

                'UseTls por defecto es 0 A espera de definir
                command.Parameters.Add(New SqlParameter("UseTls", 0))
                command.Parameters.Add(New SqlParameter("DownloadKey", boolDownloadKey))

                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        rspCode = results.GetInt32(0)
                        rspMsg = results.GetString(1)
                    End While
                Else
                    rspCode = 0
                    rspMsg = "Terminal no actualizado en forma correcta."
                    retVal = False
                End If

                If rspCode = 1 Then
                    retVal = True
                End If


            Catch ex As Exception
                rspCode = 0
                rspMsg = "Terminal no actualizado en forma correcta (Error DB)"
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        'Modificación MDM; EB: 31/Ene/2018
        'Modificación Carga de Llaves; EB: 01/Mar/2018
        ''' <summary>
        ''' Ejecutar acciones en terminal
        ''' </summary>
        Public Function ExecuteActionsOnTerminal() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webEditarTerminalAndroidAcciones"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("terminalID", intTerminalID))
                command.Parameters.Add(New SqlParameter("flagchangePassword", boolChangePasswordTerminal))
                command.Parameters.Add(New SqlParameter("newPassword", strNewPasswordTerminal))
                command.Parameters.Add(New SqlParameter("newMessage", strNewMessageTerminal))
                command.Parameters.Add(New SqlParameter("lockTerminal", boolLockTerminal))
                'Modificación Carga de Llaves; EB: 01/Mar/2018
                command.Parameters.Add(New SqlParameter("reloadKey", boolReloadKey))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        'Modificación Carga de Llaves; EB: 28/Feb/2018
        ''' <summary>
        ''' Almacenar Componente Cifrado en BD
        ''' </summary>
        Public Function InjectComponent() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webInyectarComponenteTerminal"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("componentType", intComponentType))
                If intComponentType = 1 Then
                    command.Parameters.Add(New SqlParameter("componentData", strComponent1))
                ElseIf intComponentType = 2 Then
                    command.Parameters.Add(New SqlParameter("componentData", strComponent2))
                ElseIf intComponentType = 3 Then
                    command.Parameters.Add(New SqlParameter("componentData", strWorkingKey))
                End If

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        'Modificación Carga de Llaves; EB: 28/Feb/2018
        ''' <summary>
        ''' Consultar EncryptionKey en BD
        ''' </summary>
        Public Function GetEncriptionKey() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webConsultarLlaveEncripcionGenerica"
                command.Parameters.Clear()

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        strEncryptionKey = "540E284C45DA5731B4BE375B3C9F3287FCA7DE84A21F37CEBF935AE44CFF8FEA"
                        retVal = True
                    End While
                Else
                    retVal = False
                    strEncryptionKey = ""
                End If

            Catch ex As Exception
                retVal = False
                strEncryptionKey = ""
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        'Modificación Carga de Llaves; EB: 01/Mar/2018
        ''' <summary>
        ''' Consultar Datos Relacionados con Llave y Componentes de la Terminal
        ''' </summary>
        Public Function GetKeyAndComponentsInformation() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webConsultarInformacionSeguridadTerminal"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("terminalID", ""))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        strComponent1 = results.GetString(0)    'If Null return text '-1'
                        strComponent2 = results.GetString(1)    'If Null return text '-1'
                        boolComponentsInjected = results.GetBoolean(2)
                        boolKeyLoaded = results.GetBoolean(3)
                        retVal = True
                    End While
                Else
                    retVal = False
                    strComponent1 = ""
                    strComponent2 = ""
                    boolComponentsInjected = Nothing
                    boolKeyLoaded = Nothing
                End If

            Catch ex As Exception
                retVal = False
                strComponent1 = ""
                strComponent2 = ""
                boolComponentsInjected = Nothing
                boolKeyLoaded = Nothing
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        'Modificación Carga de Llaves; EB: 01/Mar/2018
        ''' <summary>
        ''' Activar Flag de Componentes Inyectados
        ''' </summary>
        Public Function SetFlagComponentsInjected(ByVal LlaveCombinada As String) As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webActualizarFlagComponentesInyectadosTerminal"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("terminalID", intTerminalID))
                command.Parameters.Add(New SqlParameter("Key", LlaveCombinada))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function
        Public Property Initialize() As Boolean
            Get
                Return boolInitialize
            End Get
            Set(ByVal value As Boolean)
                boolInitialize = value
            End Set
        End Property
        Public Property DownloadKey() As Boolean
            Get
                Return boolDownloadKey
            End Get
            Set(ByVal value As Boolean)
                boolDownloadKey = value
            End Set
        End Property


        'Modificación Carga de Llaves; EB: 01/Mar/2018
        ''' <summary>
        ''' Activar Flag de Llave cargada en Terminal
        ''' </summary>
        Public Function SetFlagKeyLoaded() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webActualizarFlagLlaveCargadaTerminal"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("terminalID", intTerminalID))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

    End Class

End Namespace
