﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Namespace TeleLoader.MDMTransaction
    Public Class TransactionMDM
        Public Property operationName As String = ""
        Public Property login As String = ""
        Public Property pass As String = ""
        Public Property terminalRam As String = ""
        Public Property terminalBattery As String = ""
        Public Property terminalLatitude As String = ""
        Public Property terminalLongitude As String = ""
        Public Property installedAPKs As String = ""
        Public Property groupName As String = ""
        Public Property terminalDeviceId As String = ""
        Public Property terminalIP As String = ""
        Public Property key As String = ""
        Public Property terminalDownloadedAPK As String = ""
        Public Property terminalDownloadedXML As String = ""
        Public Property terminalDownloadedIMG As String = ""
        'Modificación Carga de Llaves; EB: 01/Mar/2018
        Public Property operationId As Integer = 0
        Public Property finalStatus As String = ""

        'Modificación Carga de Llaves; EB: 01/Mar/2018
        Public Property FinalStatusTrx() As String
            Get
                Return finalStatus
            End Get
            Set(ByVal value As String)
                finalStatus = value
            End Set
        End Property

        ''' <summary>
        ''' Almacenar Información de la Transacción MDM
        ''' </summary>
        Public Function registerMDMTransaction() As Boolean
            Dim configurationSection As ConnectionStringsSection = _
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
            Dim connection As New SqlConnection(configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webNotificarOperacionMDM"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("operationName", operationName))
                command.Parameters.Add(New SqlParameter("login", login))
                command.Parameters.Add(New SqlParameter("pass", pass))
                command.Parameters.Add(New SqlParameter("terminalRam", terminalRam))
                command.Parameters.Add(New SqlParameter("terminalBattery", terminalBattery))
                command.Parameters.Add(New SqlParameter("terminalLatitude", terminalLatitude))
                command.Parameters.Add(New SqlParameter("terminalLongitude", terminalLongitude))
                command.Parameters.Add(New SqlParameter("installedAPKs", installedAPKs))
                command.Parameters.Add(New SqlParameter("groupName", groupName))
                command.Parameters.Add(New SqlParameter("terminalDeviceId", terminalDeviceId))
                command.Parameters.Add(New SqlParameter("terminalIP", terminalIP))
                command.Parameters.Add(New SqlParameter("terminalDownloadedAPK", terminalDownloadedAPK))
                command.Parameters.Add(New SqlParameter("terminalDownloadedXML", terminalDownloadedXML))
                command.Parameters.Add(New SqlParameter("terminalDownloadedIMG", terminalDownloadedIMG))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                        'Modificación Carga de Llaves; EB: 01/Mar/2018
                        operationId = results.GetInt64(1)
                    End While
                Else
                    retVal = False
                    'Modificación Carga de Llaves; EB: 01/Mar/2018
                    operationId = -1
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
                'Modificación Carga de Llaves; EB: 01/Mar/2018
                operationId = -1
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        'Modificación Carga de Llaves; EB: 01/Mar/2018
        ''' <summary>
        ''' Actualizar Resultado de la Transacción MDM
        ''' </summary>
        Public Function updateStatusMDMTransaction() As Boolean
            Dim configurationSection As ConnectionStringsSection = _
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
            Dim connection As New SqlConnection(configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webActualizarEstadoOperacionMDM"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("operacionId", operationId))
                command.Parameters.Add(New SqlParameter("finalstatus", finalStatus))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

    End Class

End Namespace

