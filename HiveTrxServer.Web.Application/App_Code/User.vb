﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic

Namespace TeleLoader.Users

    Public Class User
        Private strConnString As String
        Private intUserID As Integer
        Private strNameUser As String
        Private strLoginName As String
        Private strPerfilUser As String
        Private strDisplayName As String
        Private strEmail As String
        Private strEmailpassword As String
        Private strEmailpasswordold As String
        Private strEmailAdmin As String
        Private strEmailAdminold As String
        Private strPasswordHash As String
        Private intProfile As Integer
        Private intStatus As Integer
        Private intCustomer As Integer
        'Encryption Vectors
        Private strVector1 As String
        Private strVector2 As String
        'Reset Password
        Private strActivationCode As String

        Public Sub New(ByVal strConnString As String, ByVal intUserID As Integer)
            Me.strConnString = strConnString
            Me.intUserID = intUserID
        End Sub

        Public Sub New(ByVal strConnString As String)
            Me.strConnString = strConnString
        End Sub

        Public Sub New()

            Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

            strConnString = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString
        End Sub

        'Sub New()
        '    ' TODO: Complete member initialization 
        'End Sub

        ''' <summary>
        ''' Login del usuario
        ''' </summary>
        Public Property NameUser() As String
            Get
                Return strNameUser
            End Get
            Set(ByVal value As String)
                strNameUser = value
            End Set
        End Property

        ''' <summary>
        ''' Login del usuario
        ''' </summary>
        Public Property PerfilUser() As String
            Get
                Return strPerfilUser
            End Get
            Set(ByVal value As String)
                strPerfilUser = value
            End Set
        End Property

        ''' <summary>
        ''' Login del usuario
        ''' </summary>
        Public Property LoginName() As String
            Get
                Return strLoginName
            End Get
            Set(ByVal value As String)
                strLoginName = value
            End Set
        End Property
        'Modificacion O.Seguridad; OG 04/08/2018.
        'DeleteUsers
        ''</summary>
        Public Function DeleteUser() As Integer
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer

            Try
                ' Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_WebEliminarUser"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("UserID", intUserID))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    status = 0
                End If

            Catch ex As Exception
                status = 0
            Finally
                'Cerrar Dta Reader por Default
                If Not (connection Is Nothing) Then
                    connection.Close()

                End If
            End Try

            Return status

        End Function

        ''' <summary>
        ''' Nombre a mostrar del usuario
        ''' </summary>
        Public Property DisplayName() As String
            Get
                Return strDisplayName
            End Get
            Set(ByVal value As String)
                strDisplayName = value
            End Set
        End Property

        ''' <summary>
        ''' Correo Electrónico del usuario
        ''' </summary>
        Public Property Email() As String
            Get
                Return strEmail
            End Get
            Set(ByVal value As String)
                strEmail = value
            End Set
        End Property

        ''' <summary>
        ''' Perfil del usuario
        ''' </summary>
        Public Property Profile() As Integer
            Get
                Return intProfile
            End Get
            Set(ByVal value As Integer)
                intProfile = value
            End Set
        End Property

        ''' <summary>
        ''' Estado del usuario
        ''' </summary>
        Public Property Status() As Integer
            Get
                Return intStatus
            End Get
            Set(ByVal value As Integer)
                intStatus = value
            End Set
        End Property

        ''' <summary>
        ''' Cliente asociado al usuario
        ''' </summary>
        Public Property Customer() As Integer
            Get
                Return intCustomer
            End Get
            Set(ByVal value As Integer)
                intCustomer = value
            End Set
        End Property

        ''' <summary>
        ''' Password del usuario
        ''' </summary>
        Public Property Password() As String
            Get
                Return strPasswordHash
            End Get
            Set(ByVal value As String)
                strPasswordHash = value
            End Set
        End Property

        ''' <summary>
        ''' Vector de Encripción # 1
        ''' </summary>
        Public Property Vector1() As String
            Get
                Return strVector1
            End Get
            Set(ByVal value As String)
                strVector1 = value
            End Set
        End Property

        ''' <summary>
        ''' Vector de Encripción # 2
        ''' </summary>
        Public Property Vector2() As String
            Get
                Return strVector2
            End Get
            Set(ByVal value As String)
                strVector2 = value
            End Set
        End Property

        ''' <summary>
        ''' Código de Activación Nueva Clave
        ''' </summary>
        Public Property ActivationCode() As String
            Get
                Return strActivationCode
            End Get
            Set(ByVal value As String)
                strActivationCode = value
            End Set
        End Property

        ''' <summary>
        ''' Retornar datos del Usuario
        ''' </summary>
        Public Sub getUserData()
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webConsultarDatosUsuario"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("userID", intUserID))

                'Leer Resultado
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        strLoginName = results.GetString(0)
                        strDisplayName = results.GetString(1)
                        strEmail = results.GetString(2)
                        intStatus = results.GetInt64(3)
                        intProfile = results.GetInt64(4)
                        intCustomer = results.GetInt64(5)
                    End While
                Else
                    strLoginName = ""
                    strDisplayName = ""
                    strEmail = ""
                    intStatus = -1
                    intProfile = -1
                End If

            Catch ex As Exception

            Finally
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

        End Sub
        'Modificacion O.Seguridad; OG 02/25/2020.
        'datos correo'
        ''</summary>
        Public Function getDatosCorreo() As String
            Dim Json As String
            Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
            Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString
            Dim db As SqlConnection = New SqlConnection(conexion)
            Dim cmd As SqlCommand
            Dim sqlBuilder As StringBuilder = New StringBuilder
            Dim results As SqlDataReader

            Dim retVal As Boolean

            Dim strRsp As String
            strRsp = ""
            sqlBuilder.Append("SELECT CAST(DecryptByPassPhrase( 'Clave',correo_clave ) as varchar(max)) as 'password' ,CAST(DecryptByPassPhrase( 'Clave',correo_nombre ) as varchar(max)) as 'correo'  FROM dbo.correo")

            Try
                db.Open()
            Catch ex As Exception

                db.Close()
            End Try

            Try
                cmd = New SqlCommand()
                cmd.Connection = db
                cmd.CommandType = CommandType.Text
                cmd.CommandText = sqlBuilder.ToString


                Try

                Catch ex As Exception
                End Try
                'Ejecutar SP
                results = cmd.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        strEmailpasswordold = results.GetString(0)
                        strEmailAdminold = results.GetString(1)
                    End While
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
            End Try

            Return retVal
        End Function

        ''' <summary>
        ''' Correo Electrónico Administrador       
        Public Property EmailAdmin() As String
            Get
                Return strEmailAdmin
            End Get
            Set(ByVal value As String)
                strEmailAdmin = value
            End Set
        End Property

        ''' <summary>
        ''' Correo Electrónico del usuario
        ''' </summary>
        Public Property Emailold() As String
            Get
                Return strEmailAdminold
            End Get
            Set(ByVal value As String)
                strEmailAdminold = value
            End Set
        End Property
        ''' <summary>
        ''' Correo Electrónico Administrador       
        Public Property passwordEmailold() As String
            Get
                Return strEmailpasswordold
            End Get
            Set(ByVal value As String)
                strEmailpasswordold = value
            End Set
        End Property
        ''' <summary>
        ''' password Electrónico Administrador      
        Public Property passwordEmail() As String
            Get
                Return strEmailpassword
            End Get
            Set(ByVal value As String)
                strEmailpassword = value
            End Set
        End Property
        ''' <summary>
        ''' Editar Correo Admin
        ''' </summary>
        Public Function editEmail() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webActualizarEmail"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("Mail", strEmailAdmin))
                command.Parameters.Add(New SqlParameter("Password", strEmailpassword))

                'Ejecutar SP
                results = command.ExecuteReader()

                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        ''' <summary>
        ''' Retornar datos del Usuario
        ''' </summary>
        Public Sub getUserData(ByVal loginID As String)
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webConsultarDatosUsuarioPorLogin"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("userID", loginID))

                'Leer Resultado
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        strLoginName = results.GetString(0)
                        strDisplayName = results.GetString(1)
                        strEmail = results.GetString(2)
                        intStatus = results.GetInt64(3)
                        intProfile = results.GetInt64(4)
                        intCustomer = results.GetInt64(5)
                    End While
                Else
                    strLoginName = ""
                    strDisplayName = ""
                    strEmail = ""
                    intStatus = -1
                    intProfile = -1
                    intCustomer = -1
                End If

            Catch ex As Exception

            Finally
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

        End Sub

        ''' <summary>
        ''' Grabar Datos del Usuario
        ''' </summary>
        Public Function saveUserData() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webGrabarDatosUsuario"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("userID", intUserID))
                command.Parameters.Add(New SqlParameter("userName", strDisplayName))
                command.Parameters.Add(New SqlParameter("userEmail", strEmail))
                command.Parameters.Add(New SqlParameter("PerfilUsuario", strPerfilUser))

                'Ejecutar SP
                command.ExecuteNonQuery()

                retVal = True

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        ''' <summary>
        ''' Actualizar Clave del Usuario
        ''' </summary>
        Public Function updateUserPassword(ByVal currentPassword As String, ByVal newPassword As String) As Integer
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Integer

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webCambiarClave"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("userID", intUserID))
                command.Parameters.Add(New SqlParameter("actualHASH", currentPassword))
                command.Parameters.Add(New SqlParameter("newHASH", newPassword))

                'Ejecutar SP
                results = command.ExecuteReader()

                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = 0
                End If

                retVal = status

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        ''' <summary>
        ''' Crear Usuario
        ''' </summary>
        Public Function createUser() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webCrearUsuario"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("userName", strDisplayName))
                command.Parameters.Add(New SqlParameter("userID", strLoginName))
                command.Parameters.Add(New SqlParameter("userEmail", strEmail))
                command.Parameters.Add(New SqlParameter("userHash", strPasswordHash))
                command.Parameters.Add(New SqlParameter("userProfileID", intProfile))
                command.Parameters.Add(New SqlParameter("userCustomerID", intCustomer))

                'Ejecutar SP
                results = command.ExecuteReader()

                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        ''' <summary>
        ''' Editar Usuario
        ''' </summary>
        Public Function editUser() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webActualizarUsuario"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("userName", strDisplayName))
                command.Parameters.Add(New SqlParameter("userID", strLoginName))
                command.Parameters.Add(New SqlParameter("userEmail", strEmail))
                command.Parameters.Add(New SqlParameter("userHash", strPasswordHash))
                command.Parameters.Add(New SqlParameter("userStatusID", intStatus))
                command.Parameters.Add(New SqlParameter("PerfilUsuario", strPerfilUser))

                'Ejecutar SP
                results = command.ExecuteReader()

                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        ''' <summary>
        ''' Cerrar Sesión WEB de un Usuario Específico
        ''' </summary>
        Public Sub closeUserSession(ByVal UserLogin As String)
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webCerrarSesionWebSeguridad"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("UserLogin", UserLogin))

                'Leer Resultado
                command.ExecuteNonQuery()

            Catch ex As Exception

            Finally
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

        End Sub

        ''' <summary>
        ''' Retornar Vectores de Cifrado para el usuario
        ''' </summary>
        Public Sub getUserEncryptionVectors()
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader = Nothing

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webConsultarVectoresCifradoUsuario"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("userID", intUserID))

                'Leer Resultado
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        strVector1 = results.GetString(0)
                        strVector2 = results.GetString(1)
                    End While
                Else
                    strVector1 = ""
                    strVector2 = ""
                End If

            Catch ex As Exception

            Finally
                'Cerrar Data Reader
                If Not (results Is Nothing) Then
                    results.Close()
                End If

                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

        End Sub

        ''' <summary>
        ''' Verificar Si existe un usuario Dado un Correo Electrónico
        ''' </summary>
        Public Function CheckUserByEmail() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webValidarUsuarioXCorreoElectronico"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("emailID", strEmail))

                'Ejecutar SP
                results = command.ExecuteReader()

                If results.HasRows Then
                    While results.Read()
                        intUserID = results.GetInt32(0)
                    End While
                Else
                    intUserID = -1
                    retVal = False
                End If

                If Status > -1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        ''' <summary>
        ''' 1. Iniciar proceso de restauración de Clave
        ''' </summary>
        Public Function StartResetPasswordProcess(ByVal ipAddress As String) As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webIniciarProcesoRestauracionClave"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("emailID", strEmail))
                command.Parameters.Add(New SqlParameter("IP", ipAddress))

                'Ejecutar SP
                results = command.ExecuteReader()

                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                        strActivationCode = results.GetString(1)
                    End While
                Else
                    status = -1
                    strActivationCode = ""
                    retVal = False
                End If

                If status > 0 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        ''' <summary>
        ''' 2. Continuar proceso de restauración de Clave
        ''' </summary>
        Public Function ContinueResetPasswordProcess() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webContinuarProcesoRestauracionClave"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("activationCode", strActivationCode))

                'Ejecutar SP
                results = command.ExecuteReader()

                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                        strActivationCode = results.GetString(1)
                    End While
                Else
                    status = -1
                    strActivationCode = ""
                    retVal = False
                End If

                If status > 0 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        ''' <summary>
        ''' 3. Terminar proceso de restauración de Clave
        ''' </summary>
        Public Function FinishResetPasswordProcess() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webTerminarProcesoRestauracionClave"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("activationCode", strActivationCode))
                command.Parameters.Add(New SqlParameter("passwordID", strPasswordHash))

                'Ejecutar SP
                results = command.ExecuteReader()

                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    status = -1
                    retVal = False
                End If

                If status > 0 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        ''' <summary>
        ''' Mostrar la información de usuarios
        ''' </summary>
        Public Function GetLstUsuarios() As DataTable
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim adapter As New SqlDataAdapter()
            Dim TheDataReader As SqlDataReader
            Dim TablaDatos As New DataTable()
            TablaDatos.TableName = "DtDatos"
            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webConsultarUsuarios"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("p_in_nombre_user", NameUser))
                command.Parameters.Add(New SqlParameter("p_in_login_user", LoginName))
                command.Parameters.Add(New SqlParameter("p_in_perfil_user", PerfilUser))
                'Leer Resultado                 
                adapter = New System.Data.SqlClient.SqlDataAdapter
                adapter.SelectCommand = command
                adapter.Fill(TablaDatos)
            Catch ex As Exception

            Finally
                'Cerrar DataReader
                If Not (TheDataReader Is Nothing) Then
                    TheDataReader.Close()
                End If

                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return TablaDatos
        End Function

    End Class
End Namespace
