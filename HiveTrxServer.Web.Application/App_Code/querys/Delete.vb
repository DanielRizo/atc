﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Public Class Delete


    Public Shared Function acquirer(ByVal conexion As String, terminal As String, acqAppKey As String) As Boolean

        Dim CMD As New SqlCommand("sp_webDeleteAcquirer")
        CMD.Parameters.AddWithValue("@REFER_CODE", acqAppKey)
        CMD.Parameters.AddWithValue("@ACQUIRER_CODE", acqAppKey.Replace("ACQ", ""))
        CMD.Parameters.AddWithValue("@TERMINAL_REC_NO", terminal)

        Dim connection As New SqlConnection(conexion)
        CMD.Connection = connection
        CMD.CommandType = CommandType.StoredProcedure

        Dim adapter As New SqlDataAdapter(CMD)
        adapter.SelectCommand.CommandTimeout = 300
        Try
            connection.Open()
        Catch ex As Exception

            connection.Close()
            Return False
        End Try
        CMD.ExecuteNonQuery()
        connection.Close()

        'Now, read through your data:

        Return True
    End Function

    Public Shared Function issuer(ByVal conexion As String, terminal As String, acqAppKey As String, iss As String) As Boolean

        Dim issuerCode As String
        Dim acqAppKeyCode As String

        issuerCode = iss.Substring(0, iss.IndexOf("ACQ"))
        acqAppKeyCode = acqAppKey

        Dim CMD As New SqlCommand("sp_webDeleteIssuer")
        CMD.Parameters.AddWithValue("@REFER_CODE", issuerCode + acqAppKeyCode)
        CMD.Parameters.AddWithValue("@ISSUER_CODE", issuerCode.Replace("ISS", ""))
        CMD.Parameters.AddWithValue("@ACQUIRER_CODE", acqAppKeyCode.Replace("ACQ", ""))
        CMD.Parameters.AddWithValue("@TERMINAL_REC_NO", terminal)

        Dim connection As New SqlConnection(conexion)
        CMD.Connection = connection
        CMD.CommandType = CommandType.StoredProcedure

        Dim adapter As New SqlDataAdapter(CMD)
        adapter.SelectCommand.CommandTimeout = 300

        Try
            connection.Open()
        Catch ex As Exception

            connection.Close()
            Return False
        End Try
        CMD.ExecuteNonQuery()
        connection.Close()
        'Now, read through your data:

        Return True
    End Function


    Public Shared Function cardRange(ByVal conexion As String, terminal As String, acqAppKey As String, iss As String, cdr As String) As Boolean

        Dim CMD As New SqlCommand("sp_webDeleteCardRange")


        Dim cardCode As String
        Dim issuerCode As String
        Dim acqAppKeyCode As String

        cardCode = cdr.Substring(0, cdr.IndexOf("ISS"))
        issuerCode = iss.Substring(0, iss.IndexOf("ACQ"))
        acqAppKeyCode = acqAppKey


        CMD.Parameters.AddWithValue("@REFER_CODE", cardCode + issuerCode + acqAppKeyCode)
        CMD.Parameters.AddWithValue("@CARD_CODE", cardCode.Replace("CDR", ""))
        CMD.Parameters.AddWithValue("@ISSUER_CODE", issuerCode.Replace("ISS", ""))
        CMD.Parameters.AddWithValue("@ACQUIRER_CODE", acqAppKeyCode.Replace("ACQ", ""))
        CMD.Parameters.AddWithValue("@TERMINAL_REC_NO", terminal)

        Dim connection As New SqlConnection(conexion)
        CMD.Connection = connection
        CMD.CommandType = CommandType.StoredProcedure

        Dim adapter As New SqlDataAdapter(CMD)
        adapter.SelectCommand.CommandTimeout = 300

        Try
            connection.Open()
        Catch ex As Exception

            connection.Close()
            Return False
        End Try
        Dim a As Integer
        a = CMD.ExecuteNonQuery()

        connection.Close()

        'Now, read through your data:

        Return True
    End Function


    Public Shared Function application(ByVal conexion As String, terminal As String, acqAppKey As String) As Boolean

        Dim CMD As New SqlCommand("sp_webDeleteApplication")
        CMD.Parameters.AddWithValue("@REFER_CODE", acqAppKey)
        CMD.Parameters.AddWithValue("@EMVL2APP_KEY_NAME", acqAppKey.Replace("APP", ""))
        CMD.Parameters.AddWithValue("@TERMINAL_REC_NO", terminal)

        Dim connection As New SqlConnection(conexion)
        CMD.Connection = connection
        CMD.CommandType = CommandType.StoredProcedure

        Dim adapter As New SqlDataAdapter(CMD)
        adapter.SelectCommand.CommandTimeout = 300

        Try
            connection.Open()
        Catch ex As Exception

            connection.Close()
            Return False
        End Try
        Dim a As Integer
        a = CMD.ExecuteNonQuery()

        connection.Close()

        'Now, read through your data:

        Return True
    End Function

    Public Shared Function key(ByVal conexion As String, terminal As String, acqAppKey As String) As Boolean

        Dim CMD As New SqlCommand("sp_webDeleteKey")
        CMD.Parameters.AddWithValue("@REFER_CODE", acqAppKey)
        CMD.Parameters.AddWithValue("@EMVL2KEY_KEY_NAME", acqAppKey.Replace("KEY", ""))
        CMD.Parameters.AddWithValue("@TERMINAL_REC_NO", terminal)

        Dim connection As New SqlConnection(conexion)
        CMD.Connection = connection
        CMD.CommandType = CommandType.StoredProcedure

        Dim adapter As New SqlDataAdapter(CMD)
        adapter.SelectCommand.CommandTimeout = 300

        Try
            connection.Open()
        Catch ex As Exception

            connection.Close()
            Return False
        End Try
        Dim a As Integer
        a = CMD.ExecuteNonQuery()

        connection.Close()

        'Now, read through your data:

        Return True
    End Function

    Public Shared Function extraApplication(ByVal conexion As String, terminal As String, acqAppKey As String) As Boolean

        Dim CMD As New SqlCommand("sp_webDeleteExtraApplication")
        CMD.Parameters.AddWithValue("@REFER_CODE", acqAppKey)
        CMD.Parameters.AddWithValue("@XAPP_NAME", acqAppKey.Replace("EXT", ""))
        CMD.Parameters.AddWithValue("@TERMINAL_REC_NO", terminal)

        Dim connection As New SqlConnection(conexion)
        CMD.Connection = connection
        CMD.CommandType = CommandType.StoredProcedure

        Dim adapter As New SqlDataAdapter(CMD)
        adapter.SelectCommand.CommandTimeout = 300

        Try
            connection.Open()
        Catch ex As Exception

            connection.Close()
            Return False
        End Try
        Dim a As Integer
        a = CMD.ExecuteNonQuery()

        connection.Close()

        'Now, read through your data:

        Return True
    End Function

End Class
