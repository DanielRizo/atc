﻿Imports System.Data.SqlClient
Imports System.Data

Imports System.Text

Public Class TerminalRecordNumber

    Public Shared Function Exec(ByVal conexion As String, ByRef terminal As TerminalStruc) As Boolean

        Dim procedimientoOk As Boolean = False
        Dim db As SqlConnection = New SqlConnection(conexion)
        Dim cmd As SqlCommand
        Dim sqlBuilder As StringBuilder = New StringBuilder

        sqlBuilder.Append("SELECT TERMINAL_REC_NO ")
        sqlBuilder.Append("FROM TERMINAL ")
        sqlBuilder.Append("WHERE TID = @t and status = @s")

        Try
            db.Open()
        Catch ex As Exception

            db.Close()
            Return procedimientoOk
        End Try

        Try
            cmd = New SqlCommand()
            cmd.Connection = db
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sqlBuilder.ToString
            cmd.Parameters.AddWithValue("@t", terminal.getId)
            cmd.Parameters.AddWithValue("@s", "Active")


            Dim dr As SqlDataReader = cmd.ExecuteReader()

            While (dr.Read())
                terminal.setRecord(dr.GetString(0))
            End While


            procedimientoOk = True
        Catch ex As Exception


        Finally
            cmd = Nothing
            db.Close()
        End Try

        Return procedimientoOk
    End Function


End Class
