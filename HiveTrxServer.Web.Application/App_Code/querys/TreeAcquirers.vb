﻿
Imports System.Data.SqlClient
Imports System.Data
Imports System.Text

Public Class TreeAcquirers



    Public Shared Function Exec(ByVal conexion As String, ByVal terminal As TerminalStruc) As List(Of Acquirer)

        Dim db As SqlConnection = New SqlConnection(conexion)
        Dim cmd As SqlCommand
        Dim procedimientoOK As Boolean = False
        Dim sqlBuilder As StringBuilder = New StringBuilder
        sqlBuilder.Append("select ACQUIRER_CODE, ACQUIRER_KEY_NAME, ORDER_INDEX  ")
        sqlBuilder.Append("from TERMINAL_ACQUIRER ")
        sqlBuilder.Append("where terminal_rec_no =  @trc order by order_index ")
        Dim acquirers As List(Of Acquirer) = New List(Of Acquirer)
        Dim registerCounter As Integer = 0

        Try
            db.Open()
        Catch ex As Exception

            db.Close()
            Return acquirers
        End Try

        Try
            cmd = New SqlCommand()
            cmd.Connection = db
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sqlBuilder.ToString
            cmd.Parameters.AddWithValue("@trc", terminal.getRecord)


            Dim dr As SqlDataReader = cmd.ExecuteReader()

            Dim acquirer As Acquirer
            While (dr.Read())
                acquirer = New Acquirer
                acquirer.setAcquirer_code(dr.GetValue(0).ToString)
                acquirer.setAcquirer_key_name(dr.GetValue(1).ToString)
                acquirer.setOrder_index(dr.GetValue(2).ToString)
                acquirers.Add(acquirer)
                'Console.WriteLine((dr.GetValue(0).ToString + ("-" + dr.GetValue(1).ToString)))
                registerCounter = (registerCounter + 1)
            End While


            procedimientoOK = True
        Catch ex As Exception


        Finally
            cmd = Nothing
            db.Close()
        End Try

        Return acquirers
    End Function


End Class