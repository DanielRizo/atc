﻿Imports System.Data.SqlClient
Imports System.Data

Imports System.Text

Public Class TreeCapks

    Public Shared Function Exec(ByVal conexion As String, ByVal terminal As TerminalStruc) As List(Of CAPK)

        Dim db As SqlConnection = New SqlConnection(conexion)
        Dim cmd As SqlCommand
        Dim procedimientoOK As Boolean = False
        Dim sqlBuilder As StringBuilder = New StringBuilder

        sqlBuilder.Append("Select EMVL2KEY_KEY_NAME   ")
        sqlBuilder.Append("from TERMINAL_EMVL2_KEY where   ")
        sqlBuilder.Append("terminal_rec_no =  @trc  ")

        Dim capks As List(Of CAPK) = New List(Of CAPK)
        Dim registerCounter As Integer = 0

        Try
            db.Open()
        Catch ex As Exception

            db.Close()
            Return capks
        End Try

        Try
            cmd = New SqlCommand()
            cmd.Connection = db
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sqlBuilder.ToString
            cmd.Parameters.AddWithValue("@trc", terminal.getRecord)


            Dim dr As SqlDataReader = cmd.ExecuteReader()

            Dim capk As CAPK
            While (dr.Read())

                capk = New CAPK()
                capk.setCapk_code(dr.GetValue(0).ToString)
                capks.Add(capk)
                'Console.WriteLine(dr.GetValue(0).ToString)
                registerCounter = (registerCounter + 1)

            End While


            procedimientoOK = True
        Catch ex As Exception


        Finally
            cmd = Nothing
            db.Close()
        End Try

        Return capks
    End Function

End Class
