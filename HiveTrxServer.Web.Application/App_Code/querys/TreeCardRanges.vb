﻿Imports System.Data.SqlClient
Imports System.Data

Imports System.Text

Public Class TreeCardRanges

    Public Shared Function Exec(ByVal conexion As String, ByVal terminal As TerminalStruc) As List(Of Card)

        Dim db As SqlConnection = New SqlConnection(conexion)
        Dim cmd As SqlCommand
        Dim procedimientoOK As Boolean = False
        Dim sqlBuilder As StringBuilder = New StringBuilder

        sqlBuilder.Append("select ACQUIRER_CODE, ISSUER_CODE, CARD_CODE, CARD_KEY_NAME, ORDER_INDEX  ")
        sqlBuilder.Append("from TERMINAL_CARDRANGE ")
        sqlBuilder.Append("where terminal_rec_no =  @trc order by order_index ")

        Dim cards As List(Of Card) = New List(Of Card)
        Dim registerCounter As Integer = 0

        Try
            db.Open()
        Catch ex As Exception

            db.Close()
            Return cards
        End Try

        Try
            cmd = New SqlCommand()
            cmd.Connection = db
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sqlBuilder.ToString
            cmd.Parameters.AddWithValue("@trc", terminal.getRecord)


            Dim dr As SqlDataReader = cmd.ExecuteReader()

            Dim card As Card
            While (dr.Read())

                card = New Card()
                card.setAcquirer_code(dr.GetValue(0).ToString)
                card.setIssuer_code(dr.GetValue(1).ToString)
                card.setCard_code(dr.GetValue(2).ToString)
                card.setCard_key_name(dr.GetValue(3).ToString)
                card.setOrder_index(dr.GetValue(4).ToString)
                cards.Add(card)
                'Console.WriteLine(dr.GetValue(0).ToString + "-" + dr.GetValue(1).ToString + "-" + dr.GetValue(2).ToString + "-" + dr.GetValue(3).ToString)
                registerCounter = (registerCounter + 1)

            End While


            procedimientoOK = True
        Catch ex As Exception


        Finally
            cmd = Nothing
            db.Close()
        End Try

        Return cards
    End Function


End Class
