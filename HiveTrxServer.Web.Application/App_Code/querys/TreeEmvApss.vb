﻿Imports System.Data.SqlClient
Imports System.Data

Imports System.Text

Public Class TreeEmvApss

    Public Shared Function Exec(ByVal conexion As String, ByVal terminal As TerminalStruc) As List(Of EMVApp)

        Dim db As SqlConnection = New SqlConnection(conexion)
        Dim cmd As SqlCommand
        Dim procedimientoOK As Boolean = False
        Dim sqlBuilder As StringBuilder = New StringBuilder


        sqlBuilder.Append("Select EMVL2APP_KEY_NAME  ")
        sqlBuilder.Append("from TERMINAL_EMVL2_APPLICATION  ")
        sqlBuilder.Append("where terminal_rec_no =  @trc  ")

        Dim emvapps As List(Of EMVApp) = New List(Of EMVApp)
        Dim registerCounter As Integer = 0

        Try
            db.Open()
        Catch ex As Exception

            db.Close()
            Return emvapps
        End Try

        Try
            cmd = New SqlCommand()
            cmd.Connection = db
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sqlBuilder.ToString
            cmd.Parameters.AddWithValue("@trc", terminal.getRecord)


            Dim dr As SqlDataReader = cmd.ExecuteReader()

            Dim emvapp As EMVApp
            While (dr.Read())


                emvapp = New EMVApp()
                emvapp.setEmvapp_code(dr.GetValue(0).ToString)
                emvapps.Add(emvapp)
                'Console.WriteLine(dr.GetValue(0).ToString)
                registerCounter = (registerCounter + 1)

            End While


            procedimientoOK = True
        Catch ex As Exception


        Finally
            cmd = Nothing
            db.Close()
        End Try

        Return emvapps
    End Function

End Class
