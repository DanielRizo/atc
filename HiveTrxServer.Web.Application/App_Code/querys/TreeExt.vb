﻿Imports System.Data

Imports System.Data.SqlClient
Imports System.Text

Public Class TreeExt

    Public Shared Function Exec(ByVal conexion As String, ByVal terminal As TerminalStruc) As List(Of Ext)

        Dim db As SqlConnection = New SqlConnection(conexion)
        Dim cmd As SqlCommand
        Dim procedimientoOK As Boolean = False
        Dim sqlBuilder As StringBuilder = New StringBuilder

        sqlBuilder.Append("select XAPP_NAME, XAPP_ID, PARA_INDEX from   ")
        sqlBuilder.Append("TERMINAL_EXTRA_APPLICATION   ")
        sqlBuilder.Append("where terminal_rec_no =  @trc order by para_index;  ")

        Dim exts As List(Of Ext) = New List(Of Ext)
        Dim registerCounter As Integer = 0

        Try
            db.Open()
        Catch ex As Exception

            db.Close()
            Return exts
        End Try

        Try
            cmd = New SqlCommand()
            cmd.Connection = db
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sqlBuilder.ToString
            cmd.Parameters.AddWithValue("@trc", terminal.getRecord)


            Dim dr As SqlDataReader = cmd.ExecuteReader()

            Dim ext As Ext
            While (dr.Read())

                ext = New Ext()
                ext.setExt_code(dr.GetValue(0).ToString)
                ext.setExt_name(dr.GetValue(1).ToString)
                ext.setOrder_index(dr.GetValue(2).ToString)
                exts.Add(ext)
                'Console.WriteLine(dr.GetString(0))
                registerCounter = (registerCounter + 1)

            End While


            procedimientoOK = True
        Catch ex As Exception


        Finally
            cmd = Nothing
            db.Close()
        End Try

        Return exts
    End Function


End Class
