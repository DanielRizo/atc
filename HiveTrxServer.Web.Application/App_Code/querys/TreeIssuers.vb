﻿Imports System.Data.SqlClient
Imports System.Data

Imports System.Text

Public Class TreeIssuers

    Public Shared Function Exec(ByVal conexion As String, ByVal terminal As TerminalStruc) As List(Of Issuer)

        Dim db As SqlConnection = New SqlConnection(conexion)
        Dim cmd As SqlCommand
        Dim procedimientoOK As Boolean = False
        Dim sqlBuilder As StringBuilder = New StringBuilder

        sqlBuilder.Append("select ACQUIRER_CODE, ISSUER_CODE, ISSUER_KEY_NAME, ORDER_INDEX  ")
        sqlBuilder.Append("from TERMINAL_ISSUER ")
        sqlBuilder.Append("where terminal_rec_no =  @trc order by order_index ")

        Dim issuers As List(Of Issuer) = New List(Of Issuer)
        Dim registerCounter As Integer = 0

        Try
            db.Open()
        Catch ex As Exception

            db.Close()
            Return issuers
        End Try

        Try
            cmd = New SqlCommand()
            cmd.Connection = db
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sqlBuilder.ToString
            cmd.Parameters.AddWithValue("@trc", terminal.getRecord)


            Dim dr As SqlDataReader = cmd.ExecuteReader()

            Dim issuer As Issuer
            While (dr.Read())

                issuer = New Issuer()
                issuer.setAcquirer_code(dr.GetString(0))
                issuer.setIssuer_code(dr.GetString(1))
                issuer.setIssuer_key_name(dr.GetString(2))
                issuer.setOrder_index(dr.GetString(3))
                issuers.Add(issuer)
                'Console.WriteLine(dr.GetString(0) + "-" + dr.GetString(1) + "-" + dr.GetString(2))
                registerCounter = (registerCounter + 1)

            End While


            procedimientoOK = True
        Catch ex As Exception


        Finally
            cmd = Nothing
            db.Close()
        End Try

        Return issuers
    End Function

End Class
