﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Public Class TreeNames

    Public Shared Function getTermianlName(ByVal conexion As String, ByVal terminal As TerminalStruc) As List(Of Issuer)

        Dim db As SqlConnection = New SqlConnection(conexion)
        Dim cmd As SqlCommand
        Dim procedimientoOK As Boolean = False
        Dim sqlBuilder As StringBuilder = New StringBuilder

        sqlBuilder.Append("SELECT TERMINAL_REC_NO")
        sqlBuilder.Append("FROM TERMINAL")
        sqlBuilder.Append("WHERE TID = @t and status = @s")

        Dim issuers As List(Of Issuer) = New List(Of Issuer)
        Dim registerCounter As Integer = 0

        Try
            db.Open()
        Catch ex As Exception

            db.Close()
            Return issuers
        End Try

        Try
            cmd = New SqlCommand()
            cmd.Connection = db
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sqlBuilder.ToString
            cmd.Parameters.AddWithValue("@trc", terminal.getRecord)


            Dim dr As SqlDataReader = cmd.ExecuteReader()

            Dim issuer As Issuer
            While (dr.Read())

                issuer = New Issuer()
                issuer.setAcquirer_code(dr.GetString(0))
                issuer.setIssuer_code(dr.GetString(1))
                issuer.setOrder_index(dr.GetString(1))
                issuers.Add(issuer)
                'Console.WriteLine(dr.GetString(0) + "-" + dr.GetString(1) + "-" + dr.GetString(2))
                registerCounter = (registerCounter + 1)

            End While


            procedimientoOK = True
        Catch ex As Exception


        Finally
            cmd = Nothing
            db.Close()
        End Try

        Return issuers
    End Function


End Class
