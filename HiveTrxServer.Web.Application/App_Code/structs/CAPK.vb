﻿Imports System.Text

Public Class CAPK

    Private capk_code As String

    Private order_index As String

    Public Function To_String() As String
        Try
            Dim sb As StringBuilder = New StringBuilder
            sb.Append("" & vbLf & "CAPK: ")
            sb.Append("capk_code:")
            sb.Append(Me.capk_code)
            sb.Append("-order_index:")
            sb.Append(Me.order_index)
            sb.Append("" & vbLf)
            Return sb.ToString
        Catch ex As Exception
            Return ""
        End Try

    End Function

    Public Function getCapk_code() As String
        Return Me.capk_code
    End Function

    Public Sub setCapk_code(ByVal capk_code As String)
        Me.capk_code = capk_code
    End Sub

    Public Function getOrder_index() As String
        Return Me.order_index
    End Function

    Public Sub setOrder_index(ByVal order_index As String)
        Me.order_index = order_index
    End Sub
End Class