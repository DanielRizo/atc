﻿Imports System.Text

Public Class Card

    Private acquirer_code As String

    Private issuer_code As String

    Private card_code As String

    Private card_key_name As String


    Private order_index As String

    Public Function To_String() As String
        Try
            Dim sb As StringBuilder = New StringBuilder
            sb.Append("" & vbLf & "Card: ")
            sb.Append("acquirer_code:")
            sb.Append(Me.acquirer_code)
            sb.Append("issuer_code:")
            sb.Append(Me.issuer_code)
            sb.Append("card_code:")
            sb.Append(Me.card_code)
            sb.Append("card_key_name:")
            sb.Append(Me.card_key_name)
            sb.Append("-order_index:")
            sb.Append(Me.order_index)
            sb.Append("" & vbLf)
            Return sb.ToString
        Catch ex As Exception
            Return ""
        End Try

    End Function

    Public Function getAcquirer_code() As String
        Return Me.acquirer_code
    End Function

    Public Sub setAcquirer_code(ByVal acquirer_code As String)
        Me.acquirer_code = acquirer_code
    End Sub

    Public Function getIssuer_code() As String
        Return Me.issuer_code
    End Function

    Public Sub setIssuer_code(ByVal issuer_code As String)
        Me.issuer_code = issuer_code
    End Sub

    Public Function getCard_code() As String
        Return Me.card_code
    End Function

    Public Sub setCard_code(ByVal card_code As String)
        Me.card_code = card_code
    End Sub


    Public Function getCard_key_name() As String
        Return Me.card_key_name
    End Function

    Public Sub setCard_key_name(ByVal card_key_name As String)
        Me.card_key_name = card_key_name
    End Sub
    Public Function getOrder_index() As String
        Return Me.order_index
    End Function

    Public Sub setOrder_index(ByVal order_index As String)
        Me.order_index = order_index
    End Sub
End Class
