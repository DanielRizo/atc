﻿Imports System.Text

Public Class EMVApp

    Private emvapp_code As String

    Private order_index As String

    Public Function To_String() As String
        Try
            Dim sb As StringBuilder = New StringBuilder
            sb.Append("" & vbLf & "EMVApp: ")
            sb.Append("emvapp_code:")
            sb.Append(Me.emvapp_code)
            sb.Append("-order_index:")
            sb.Append(Me.order_index)
            sb.Append("" & vbLf)
            Return sb.ToString
        Catch ex As Exception
            Return ""
        End Try

    End Function

    Public Function getEmvapp_code() As String
        Return Me.emvapp_code
    End Function

    Public Sub setEmvapp_code(ByVal emvapp_code As String)
        Me.emvapp_code = emvapp_code
    End Sub

    Public Function getOrder_index() As String
        Return Me.order_index
    End Function

    Public Sub setOrder_index(ByVal order_index As String)
        Me.order_index = order_index
    End Sub
End Class
