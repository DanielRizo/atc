﻿Imports System.Text

Public Class Ext

    Private ext_code As String
    Private ext_name As String

    Private order_index As String

    Public Function To_String() As String
        Try
            Dim sb As StringBuilder = New StringBuilder
            sb.Append("" & vbLf & "Ext: ")
            sb.Append("ext_code:")
            sb.Append(Me.ext_code)
            sb.Append("ext_name:")
            sb.Append(Me.ext_name)
            sb.Append("-order_index:")
            sb.Append(Me.order_index)
            sb.Append("" & vbLf)
            Return sb.ToString
        Catch ex As Exception
            Return ""
        End Try

    End Function

    Public Function getExt_code() As String
        Return Me.ext_code
    End Function

    Public Sub setExt_code(ByVal ext_code As String)
        Me.ext_code = ext_code
    End Sub
    Public Function getExt_name() As String
        Return Me.ext_name
    End Function

    Public Sub setExt_name(ByVal ext_name As String)
        Me.ext_name = ext_name
    End Sub


    Public Function getOrder_index() As String
        Return Me.order_index
    End Function

    Public Sub setOrder_index(ByVal order_index As String)
        Me.order_index = order_index
    End Sub
End Class