﻿Imports System.Text

Public Class Issuer

    Private acquirer_code As String

    Private issuer_code As String

    Private issuer_key_name As String

    Private order_index As String

    Public Function To_String() As String
        Try
            Dim sb As StringBuilder = New StringBuilder
            sb.Append("" & vbLf & "Issuer: ")
            sb.Append("acquirer_code:")
            sb.Append(Me.acquirer_code)
            sb.Append("issuer_code:")
            sb.Append(Me.issuer_code)
            sb.Append("issuer_key_name:")
            sb.Append(Me.issuer_key_name)
            sb.Append("-order_index:")
            sb.Append(Me.order_index)
            sb.Append("" & vbLf)
            Return sb.ToString
        Catch ex As Exception
            Return ""
        End Try

    End Function

    Public Function getAcquirer_code() As String
        Return Me.acquirer_code
    End Function

    Public Sub setAcquirer_code(ByVal acquirer_code As String)
        Me.acquirer_code = acquirer_code
    End Sub

    Public Function getIssuer_code() As String
        Return Me.issuer_code
    End Function

    Public Sub setIssuer_code(ByVal issuer_code As String)
        Me.issuer_code = issuer_code
    End Sub
    Public Function getIssuer_key_name() As String
        Return Me.issuer_key_name
    End Function

    Public Sub setIssuer_key_name(ByVal issuer_key_name As String)
        Me.issuer_key_name = issuer_key_name
    End Sub

    Public Function getOrder_index() As String
        Return Me.order_index
    End Function

    Public Sub setOrder_index(ByVal order_index As String)
        Me.order_index = order_index
    End Sub
End Class