﻿Imports System.Text
Public Class Packet

    Private sequence As String

    Private table As String

    Private code As String

    Private name As String

    Private id As String

    Private acquirerFather As String

    Private issuerFather As String

    Private lastParameter As Boolean = False

    Private lastEmvApp As Boolean = False

    Private lastCapk As Boolean = False

    Private lastExt As Boolean = False

    Public Function To_String() As String
        Try
            Dim sb As StringBuilder = New StringBuilder
            sb.Append("" & vbLf & "Packet: ")
            sb.Append("sequence:")
            sb.Append(Me.sequence)
            sb.Append("-table:")
            sb.Append(Me.table)
            sb.Append("-code:")
            sb.Append(Me.code)
            sb.Append("-id1:")
            sb.Append(Me.id)
            sb.Append("-acquirerFather:")
            sb.Append(Me.acquirerFather)
            sb.Append("-issuerFather:")
            sb.Append(Me.issuerFather)
            sb.Append("-lastParameter:")
            sb.Append((Me.lastParameter = True))
            'TODO: Warning!!!, inline IF is not supported ?
            sb.Append("-lastEmvApp:")
            sb.Append((Me.lastEmvApp = True))
            'TODO: Warning!!!, inline IF is not supported ?
            sb.Append("-lastCapk:")
            sb.Append((Me.lastCapk = True))
            'TODO: Warning!!!, inline IF is not supported ?
            sb.Append("-lastExt:")
            sb.Append((Me.lastExt = True))
            'TODO: Warning!!!, inline IF is not supported ?
            sb.Append("" & vbLf)
            Return sb.ToString
        Catch ex As Exception
            Return ""
        End Try

    End Function

    Public Function isLastParameter() As Boolean
        Return Me.lastParameter
    End Function

    Public Sub setLastParameter(ByVal lastParameter As Boolean)
        Me.lastParameter = lastParameter
    End Sub

    Public Function isLastEmvApp() As Boolean
        Return Me.lastEmvApp
    End Function

    Public Sub setLastEmvApp(ByVal lastEmvApp As Boolean)
        Me.lastEmvApp = lastEmvApp
    End Sub

    Public Function isLastCapk() As Boolean
        Return Me.lastCapk
    End Function

    Public Sub setLastCapk(ByVal lastCapk As Boolean)
        Me.lastCapk = lastCapk
    End Sub

    Public Function isLastExt() As Boolean
        Return Me.lastExt
    End Function

    Public Sub setLastExt(ByVal lastExt As Boolean)
        Me.lastExt = lastExt
    End Sub

    Public Function getId() As String
        Return Me.id
    End Function

    Public Sub setId(ByVal id As String)
        Me.id = id
    End Sub

    Public Function getAcquirerFather() As String
        Return Me.acquirerFather
    End Function

    Public Sub setAcquirerFather(ByVal acquirerFather As String)
        Me.acquirerFather = acquirerFather
    End Sub

    Public Function getIssuerFather() As String
        Return Me.issuerFather
    End Function

    Public Sub setIssuerFather(ByVal issuerFather As String)
        Me.issuerFather = issuerFather
    End Sub

    Public Function getSequence() As String
        Return Me.sequence
    End Function

    Public Sub setSequence(ByVal sequence As String)
        Me.sequence = sequence
    End Sub

    Public Function getTable() As String
        Return Me.table
    End Function

    Public Sub setTable(ByVal table As String)
        Me.table = table
    End Sub

    Public Function getCode() As String
        Return Me.code
    End Function
    Public Function getName() As String
        Return Me.name
    End Function

    Public Sub setCode(ByVal code As String)
        Me.code = code
    End Sub
    Public Sub setName(ByVal name As String)
        Me.name = name
    End Sub
End Class
