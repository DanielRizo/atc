﻿Imports System.Text

Public Class TerminalStruc

    Private id As String = ""

    Private pcc As String = ""

    Private stan As String = ""

    Private record As String = ""

    Private rspCode As String = ""

    Private responseTime As String = ""

    Private referCode As String = ""

    Private output() As Byte = Nothing

    Public Function To_String() As String
        Try
            Dim sb As StringBuilder = New StringBuilder
            sb.Append("Terminal: ")
            sb.Append(("id:" + Me.id))
            sb.Append(("-pcc:" + Me.pcc))
            sb.Append(("-stan:" + Me.stan))
            sb.Append(("-record:" + Me.record))
            sb.Append(("-responseTime:" + Me.responseTime))
            sb.Append(("-referCode:" + Me.referCode))
            'If (Not (Me.output) Is Nothing) Then
            '    sb.Append(ISOUtil.dumpString(Me.output))
            'End If

            sb.Append("|")
            Return sb.ToString
        Catch ex As Exception
            Return ""
        End Try

    End Function

    Public Function getResponseTime() As String
        Return Me.responseTime
    End Function

    Public Sub setResponseTime(ByVal responseTime As String)
        Me.responseTime = Me.responseTime
    End Sub

    Public Function getRecord() As String
        Return Me.record
    End Function

    Public Sub setRecord(ByVal record As String)
        Me.record = record
    End Sub

    Public Function getRspCode() As String
        Return Me.rspCode
    End Function

    Public Sub setRspCode(ByVal rspCode As String)
        Me.rspCode = rspCode
    End Sub

    Public Function getId() As String
        Return Me.id
    End Function

    Public Sub setId(ByVal id As String)
        Me.id = id
    End Sub

    Public Function getPcc() As String
        Return Me.pcc
    End Function

    Public Sub setPcc(ByVal pcc As String)
        Me.pcc = pcc
    End Sub

    Public Function getStan() As String
        Return Me.stan
    End Function

    Public Sub setStan(ByVal stan As String)
        Me.stan = stan
    End Sub

    Public Function getOutput() As Byte()
        Return Me.output
    End Function

    Public Sub setOutput(ByVal output() As Byte)
        Me.output = output
    End Sub

    Public Function getReferCode() As String
        Return Me.referCode
    End Function

    Public Sub setReferCode(ByVal referCode As String)
        Me.referCode = referCode
    End Sub
End Class