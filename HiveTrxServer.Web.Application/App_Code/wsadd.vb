﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.ServiceModel.Web
Imports System.Web
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Text
Imports System.Security.Cryptography
Imports Newtonsoft.Json



' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()>
<WebService(Namespace:="http://tempuri.org/")>
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Public Class wsadd
    Inherits System.Web.Services.WebService

    <WebMethod()>
    Public Function addAcquirerTbl(data As String) As String

        Dim rawresp As String = data

        Dim jss As New JavaScriptSerializer()
        Dim dict As Dictionary(Of String, String) = jss.Deserialize(Of Dictionary(Of String, String))(rawresp)
        Dim code As String
        Dim displayName As String
        Dim nii As String
        Dim isExtended As String
        Dim fieldDisplayName As String
        Dim contentDesc As String
        Dim contentType As String
        Dim note As String
        Dim contentValue As String
        Dim ParameterDef As String


        Dim strRsp As String
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)

        contentValue = ""
        code = dict("Code")
        displayName = dict("DisplayName")
        nii = dict("Nii")

        isExtended = dict("Parameter Type")
        fieldDisplayName = dict("Parameter Name")
        contentDesc = dict("Value")
        contentType = dict("Value Type")
        note = dict("Note")

        contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
        If (contentValue Is "VALUENULL") Then
            contentValue = dict("Value")
        End If
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webCrearAcquirer"
            command.Parameters.Clear()
            Try


                contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
                If (contentValue Is "VALUENULL") Then
                    contentValue = dict("Value")
                End If



            Catch ex As Exception

            End Try

            If isExtended Is "EXTENDED" Then
                ParameterDef = "1"
            Else
                ParameterDef = "0"
            End If
            command.Parameters.Add(New SqlParameter("Code", code))
            command.Parameters.Add(New SqlParameter("DisplayName", displayName))
            command.Parameters.Add(New SqlParameter("Nii", nii))
            command.Parameters.Add(New SqlParameter("IS_EXTENDED", ParameterDef))
            command.Parameters.Add(New SqlParameter("FIELD_DISPLAY_NAME", fieldDisplayName))
            command.Parameters.Add(New SqlParameter("CONTENT_DESC", contentDesc))
            command.Parameters.Add(New SqlParameter("CONTENT_TYPE", contentType))
            command.Parameters.Add(New SqlParameter("NOTE", note))
            command.Parameters.Add(New SqlParameter("CONTENT_VALUE", contentValue))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try


        Return strRsp

    End Function
    <WebMethod()>
    Public Function EditGroupPrompts(data As String) As String

        Dim data2() As String = data.Split("@")
        Dim val As Integer = data2.Length
        Dim code As Int64
        Dim Ter_id As String = ""
        Dim Ter_Grupo_Destino As String = ""
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)

        code = Int64.Parse(data2(val - 2).Trim)
        Ter_Grupo_Destino = data2(val - 1).Trim
        Dim cont As Integer = 1
        If (cont < val - 2) Then
            Ter_id = data2(cont).Trim
            cont = cont + 1
        End If
        While (cont < val - 2)
            Ter_id = Ter_id + "-" + data2(cont).Trim
            cont = cont + 1
        End While
        Try
            'Abrir Conexion
            connection.Open()
            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webEditarPosGrupo"
            command.Parameters.Clear()
            Try



            Catch ex As Exception

            End Try

            command.Parameters.Add(New SqlParameter("code", code))
            command.Parameters.Add(New SqlParameter("codigosPrompts", Ter_id))
            command.Parameters.Add(New SqlParameter("CodeGroup", Ter_Grupo_Destino))
            'Varibles Output Sql 
            command.Parameters.Add(New SqlParameter("db_rsp_code", ""))
            command.Parameters.Add(New SqlParameter("db_msg", ""))
            command.Parameters.Add(New SqlParameter("db_error_code", ""))
            command.Parameters.Add(New SqlParameter("db_error_msg", ""))



            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    retVal = True
                End While
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try


        Return ""
    End Function
    <WebMethod()>
    Public Function GetTerminalesGrupo(data As String) As String
        Dim Json As String
        Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString
        Dim db As SqlConnection = New SqlConnection(conexion)
        Dim cmd As SqlCommand
        Dim sqlBuilder As StringBuilder = New StringBuilder
        Dim GrupoId As String = data

        Dim strRsp As String
        strRsp = ""
        sqlBuilder.Append("select cast(ter_id as varchar(50)),ter_Serial from terminal  where ter_grupo_id =" + GrupoId)

        Try
            db.Open()
        Catch ex As Exception

            db.Close()
        End Try

        Try
            cmd = New SqlCommand()
            cmd.Connection = db
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sqlBuilder.ToString


            Dim dr As SqlDataReader = cmd.ExecuteReader()
            Json = "{ ""registroTerminal"":["


            While (dr.Read())
                strRsp = strRsp + "{""Codigo"":""" + dr.GetString(0) + """,""Terminal"":""" + dr.GetString(1) + """},"

            End While
            strRsp = strRsp.Substring(0, strRsp.Length - 1)

            Json = Json + strRsp + "]}"


        Catch ex As Exception
            Console.WriteLine(ex)

        Finally
            cmd = Nothing
            db.Close()
        End Try


        Return Json
    End Function

    <WebMethod()>
    Public Function addIssuerTbl(data As String) As String

        Dim rawresp As String = data

        Dim jss As New JavaScriptSerializer()
        Dim dict As Dictionary(Of String, String) = jss.Deserialize(Of Dictionary(Of String, String))(rawresp)
        Dim code As String
        Dim displayName As String
        Dim isExtended As String
        Dim fieldDisplayName As String
        Dim contentDesc As String
        Dim contentType As String
        Dim note As String
        Dim contentValue As String
        Dim ParameterDef As String



        Dim strRsp As String
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)

        contentValue = ""
        code = dict("Code")
        displayName = dict("DisplayName")

        isExtended = dict("Parameter Type")
        fieldDisplayName = dict("Parameter Name")
        contentDesc = dict("Value")
        contentType = dict("Value Type")
        note = dict("Note")

        contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
        If (contentValue Is "VALUENULL") Then
            contentValue = dict("Value")
        End If
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webCrearIssuer"
            command.Parameters.Clear()
            Try


                contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
                If (contentValue Is "VALUENULL") Then
                    contentValue = dict("Value")
                End If



            Catch ex As Exception

            End Try
            If isExtended Is "EXTENDED" Then
                ParameterDef = "1"
            Else
                ParameterDef = "0"
            End If
            command.Parameters.Add(New SqlParameter("Code", code))
            command.Parameters.Add(New SqlParameter("DisplayName", displayName))
            command.Parameters.Add(New SqlParameter("IS_EXTENDED", ParameterDef))
            command.Parameters.Add(New SqlParameter("FIELD_DISPLAY_NAME", fieldDisplayName))
            command.Parameters.Add(New SqlParameter("CONTENT_DESC", contentDesc))
            command.Parameters.Add(New SqlParameter("CONTENT_TYPE", contentType))
            command.Parameters.Add(New SqlParameter("NOTE", note))
            command.Parameters.Add(New SqlParameter("CONTENT_VALUE", contentValue))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try


        Return strRsp
    End Function
    'Graficas Perzonalizadas
    <WebMethod()>
    Public Function getDatosDescargasApk() As String
        Dim Json As String
        Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString
        Dim db As SqlConnection = New SqlConnection(conexion)
        Dim cmd As SqlCommand
        Dim sqlBuilder As StringBuilder = New StringBuilder

        Dim strRsp As String
        strRsp = ""
        sqlBuilder.Append("select  top 10 por_serialTerminal, por_tra_fecha, por_porcentaje_descarga,por_nombre_aplicacion from porcentajeDescarga order by por_tra_fecha desc")

        Try
            db.Open()
        Catch ex As Exception

            db.Close()
        End Try

        Try
            cmd = New SqlCommand()
            cmd.Connection = db
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sqlBuilder.ToString
            'cmd.Parameters.AddWithValue("@TRN", Data)


            Dim dr As SqlDataReader = cmd.ExecuteReader()
            Json = "{ ""registroTerminal"":["


            While (dr.Read())

                'strRsp = strRsp + "{""fecha"":""" + dr.GetDateTime(0) + """,""tipo"":""" + dr.GetString(1) + """, ""contador"":""" + dr.GetInt32(2) + """},"

                strRsp = strRsp + "{""por_serialTerminal"":""" + dr.GetString(0) + """, ""por_tra_fecha"":""" + dr.GetDateTime(1) + """,""por_porcentaje_descarga"":""" + dr.GetString(2) + """,""por_nombre_aplicacion"":""" + dr.GetString(3) + """},"



            End While
            strRsp = strRsp.Substring(0, strRsp.Length - 1)

            Json = Json + strRsp + "]}"


        Catch ex As Exception

            Json = ""
        Finally
            cmd = Nothing
            db.Close()
        End Try


        Return Json
    End Function

    <WebMethod()>
    Public Function getDatosDescargas() As String
        Dim Json As String
        Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString
        Dim db As SqlConnection = New SqlConnection(conexion)
        Dim cmd As SqlCommand
        Dim sqlBuilder As StringBuilder = New StringBuilder

        Dim strRsp As String
        strRsp = ""
        sqlBuilder.Append("select  cast(count(*)as varchar(max)) as tipo ,g.gru_nombre as fecha from GrupoXArchivosDesplegados GX INNER JOIN Grupo G ON G.gru_id = Gx.desp_grupo_id group by g.gru_nombre")

        Try
            db.Open()
        Catch ex As Exception

            db.Close()
        End Try

        Try
            cmd = New SqlCommand()
            cmd.Connection = db
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sqlBuilder.ToString
            'cmd.Parameters.AddWithValue("@TRN", Data)


            Dim dr As SqlDataReader = cmd.ExecuteReader()
            Json = "{ ""registroTerminal"":["


            While (dr.Read())

                'strRsp = strRsp + "{""fecha"":""" + dr.GetDateTime(0) + """,""tipo"":""" + dr.GetString(1) + """, ""contador"":""" + dr.GetInt32(2) + """},"

                strRsp = strRsp + "{""tipo"":""" + dr.GetString(0) + """, ""fecha"":""" + dr.GetString(1) + """},"



            End While
            strRsp = strRsp.Substring(0, strRsp.Length - 1)

            Json = Json + strRsp + "]}"


        Catch ex As Exception

            Json = ""
        Finally
            cmd = Nothing
            db.Close()
        End Try


        Return Json
    End Function
    <WebMethod()>
    Public Function getDatosTransacciones() As String
        Dim Json As String
        Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString
        Dim connection As SqlConnection = New SqlConnection(conexion)
        Dim command As New SqlCommand()
        Dim sqlBuilder As StringBuilder = New StringBuilder
        Dim results As SqlDataReader
        Dim status As Integer
        Dim strRsp As String
        strRsp = ""
        Try
            'Abrir Conexion
            connection.Open()
            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_Graficos"
            command.Parameters.Clear()

            'Ejecutar SP
            results = command.ExecuteReader()
            Json = "{ ""registroTerminal"":["


            While (results.Read())

                'strRsp = strRsp + "{""fecha"":""" + dr.GetDateTime(0) + """,""tipo"":""" + dr.GetString(1) + """, ""contador"":""" + dr.GetInt32(2) + """},"

                strRsp = strRsp + "{""tipo"":""" + results.GetString(0) + """, ""valor"":""" + results.GetString(1) + """},"



            End While
            strRsp = strRsp.Substring(0, strRsp.Length - 1)

            Json = Json + strRsp + "]}"


        Catch ex As Exception
            Console.WriteLine(ex)

        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try
        Return Json
    End Function

    <WebMethod()>
    Public Function getDatosDescargasStis() As String
        Dim Json As String
        Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim db As SqlConnection = New SqlConnection(conexion)
        Dim cmd As SqlCommand
        Dim sqlBuilder As StringBuilder = New StringBuilder

        Dim strRsp As String
        strRsp = ""
        sqlBuilder.Append("select  top 10 TERMINAL_ID, FECHA, PORCENTAJE,ARCHIVO_DESCARGA from dbo.estado_transacciones order by FECHA desc")

        Try
            db.Open()
        Catch ex As Exception

            db.Close()
        End Try

        Try
            cmd = New SqlCommand()
            cmd.Connection = db
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sqlBuilder.ToString
            'cmd.Parameters.AddWithValue("@TRN", Data)


            Dim dr As SqlDataReader = cmd.ExecuteReader()
            Json = "{ ""registroTerminal"":["


            While (dr.Read())

                'strRsp = strRsp + "{""fecha"":""" + dr.GetDateTime(0) + """,""tipo"":""" + dr.GetString(1) + """, ""contador"":""" + dr.GetInt32(2) + """},"

                strRsp = strRsp + "{""TERMINAL_ID"":""" + dr.GetString(0) + """, ""FECHA"":""" + dr.GetDateTime(1) + """,""PORCENTAJE"":""" + dr.GetString(2) + """,""ARCHIVO_DESCARGA"":""" + dr.GetString(3) + """},"



            End While
            strRsp = strRsp.Substring(0, strRsp.Length - 1)

            Json = Json + strRsp + "]}"


        Catch ex As Exception

            Json = ""
        Finally
            cmd = Nothing
            db.Close()
        End Try


        Return Json
    End Function
    <WebMethod()>
    Public Function getDatosTerminalesActualizadasTotal() As String
        Dim Json As String
        Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString
        Dim db As SqlConnection = New SqlConnection(conexion)
        Dim cmd As SqlCommand
        Dim sqlBuilder As StringBuilder = New StringBuilder

        Dim strRsp As String
        strRsp = ""
        sqlBuilder.Append("SELECT 'DESCARGAS' AS TIPO ,CAST(COUNT(TRA_TERMINAL_ID)AS VARCHAR(50)) AS VALOR FROM dbo.TransaccionDescarga WHERE TRA_ESTADO_ACTUALIZACION_ID = '1' UNION  SELECT 'CONSULTAS',CAST(COUNT(TRA_ID)AS VARCHAR (50)) FROM dbo.TransaccionInventario")

        Try
            db.Open()
        Catch ex As Exception

            db.Close()
        End Try

        Try
            cmd = New SqlCommand()
            cmd.Connection = db
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sqlBuilder.ToString
            'cmd.Parameters.AddWithValue("@TRN", Data)


            Dim dr As SqlDataReader = cmd.ExecuteReader()
            Json = "{ ""registroTerminal"":["


            While (dr.Read())

                strRsp = strRsp + "{""TIPO"":""" + dr.GetString(0) + """, ""VALOR"":""" + dr.GetString(1) + """},"



            End While
            strRsp = strRsp.Substring(0, strRsp.Length - 1)

            Json = Json + strRsp + "]}"


        Catch ex As Exception

            Json = ""
        Finally
            cmd = Nothing
            db.Close()
        End Try


        Return Json
    End Function
    <WebMethod()>
    Public Function getDatosTerminalesFrecuenciaSeñal() As String
        Dim Json As String
        Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString
        Dim db As SqlConnection = New SqlConnection(conexion)
        Dim cmd As SqlCommand
        Dim sqlBuilder As StringBuilder = New StringBuilder

        Dim strRsp As String
        strRsp = ""
        sqlBuilder.Append("SELECT 'SEÑAL' AS TIPO, CAST(AVG(ter_nivel_gprs)AS VARCHAR(50))  AS VALOR  from terminal UNION  SELECT 'BATERIA',CAST(AVG(ter_nivel_bateria)  AS varchar(50)) from terminal")

        Try
            db.Open()
        Catch ex As Exception

            db.Close()
        End Try

        Try
            cmd = New SqlCommand()
            cmd.Connection = db
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sqlBuilder.ToString
            'cmd.Parameters.AddWithValue("@TRN", Data)


            Dim dr As SqlDataReader = cmd.ExecuteReader()
            Json = "{ ""registroTerminal"":["


            While (dr.Read())

                strRsp = strRsp + "{""TIPO"":""" + dr.GetString(0) + """, ""VALOR"":""" + dr.GetString(1) + """},"



            End While
            strRsp = strRsp.Substring(0, strRsp.Length - 1)

            Json = Json + strRsp + "]}"


        Catch ex As Exception

            Json = ""
        Finally
            cmd = Nothing
            db.Close()
        End Try


        Return Json
    End Function
    <WebMethod()>
    Public Function getDatosTerminalesActualizadas() As String
        Dim Json As String
        Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString
        Dim db As SqlConnection = New SqlConnection(conexion)
        Dim cmd As SqlCommand
        Dim sqlBuilder As StringBuilder = New StringBuilder

        Dim strRsp As String
        strRsp = ""
        sqlBuilder.Append("SELECT MT.descripcion AS terminal_tipo, CAST(COUNT(*) AS VARCHAR(10)) AS ter_totales FROM Terminal T INNER JOIN Grupo G ON (T.ter_grupo_id = G.gru_id) INNER JOIN ModeloTerminal MT ON (T.ter_modelo_terminal_id = MT.id) INNER JOIN TipoEstado TE On (T.ter_tipo_estado_id = TE.id) GROUP BY MT.descripcion")

        Try
            db.Open()
        Catch ex As Exception

            db.Close()
        End Try

        Try
            cmd = New SqlCommand()
            cmd.Connection = db
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sqlBuilder.ToString
            'cmd.Parameters.AddWithValue("@TRN", Data)


            Dim dr As SqlDataReader = cmd.ExecuteReader()
            Json = "{ ""registroTerminal"":["


            While (dr.Read())

                strRsp = strRsp + "{""terminal_tipo"":""" + dr.GetString(0) + """, ""ter_totales"":""" + dr.GetString(1) + """},"



            End While
            strRsp = strRsp.Substring(0, strRsp.Length - 1)

            Json = Json + strRsp + "]}"


        Catch ex As Exception

            Json = ""
        Finally
            cmd = Nothing
            db.Close()
        End Try


        Return Json
    End Function

    <WebMethod()>
    Public Function addCardRangeTbl(data As String) As String

        Dim rawresp As String = data

        Dim jss As New JavaScriptSerializer()
        Dim dict As Dictionary(Of String, String) = jss.Deserialize(Of Dictionary(Of String, String))(rawresp)
        Dim code As String
        Dim displayName As String
        Dim description As String
        Dim isExtended As String
        Dim fieldDisplayName As String
        Dim contentDesc As String
        Dim contentType As String
        Dim note As String
        Dim contentValue As String
        Dim ParameterDef As String



        Dim strRsp As String
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)

        contentValue = ""
        code = dict("Code")
        displayName = dict("DisplayName")
        description = dict("Description")

        isExtended = dict("Parameter Type")
        fieldDisplayName = dict("Parameter Name")
        contentDesc = dict("Value")
        contentType = dict("Value Type")
        note = dict("Note")

        contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
        If (contentValue Is "VALUENULL") Then
            contentValue = dict("Value")
        End If
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webCrearCardRange"
            command.Parameters.Clear()
            Try


                contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
                If (contentValue Is "VALUENULL") Then
                    contentValue = dict("Value")
                End If



            Catch ex As Exception

            End Try
            If isExtended Is "EXTENDED" Then
                ParameterDef = "1"
            Else
                ParameterDef = "0"
            End If
            command.Parameters.Add(New SqlParameter("Code", code))
            command.Parameters.Add(New SqlParameter("DisplayName", displayName))
            command.Parameters.Add(New SqlParameter("Description", description))
            command.Parameters.Add(New SqlParameter("IS_EXTENDED", ParameterDef))
            command.Parameters.Add(New SqlParameter("FIELD_DISPLAY_NAME", fieldDisplayName))
            command.Parameters.Add(New SqlParameter("CONTENT_DESC", contentDesc))
            command.Parameters.Add(New SqlParameter("CONTENT_TYPE", contentType))
            command.Parameters.Add(New SqlParameter("NOTE", note))
            command.Parameters.Add(New SqlParameter("CONTENT_VALUE", contentValue))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try


        Return strRsp
    End Function


    <WebMethod()>
    Public Function AddEmvApplicationTbl(data As String) As String


        Dim rawresp As String = data

        Dim jss As New JavaScriptSerializer()
        Dim dict As Dictionary(Of String, String) = jss.Deserialize(Of Dictionary(Of String, String))(rawresp)
        Dim displayName As String
        Dim isExtended As String
        Dim fieldDisplayName As String
        Dim contentDesc As String
        Dim contentType As String
        Dim note As String
        Dim contentValue As String
        Dim ParameterDef As String


        Dim strRsp As String
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)

        contentValue = ""
        displayName = dict("DisplayName")

        isExtended = dict("Parameter Type")
        fieldDisplayName = dict("Parameter Name")
        contentDesc = dict("Value")
        contentType = dict("Value Type")
        note = dict("Note")

        contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
        If (contentValue Is "VALUENULL") Then
            contentValue = dict("Value")
        End If
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webCrearEmvApplication"
            command.Parameters.Clear()
            Try


                contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
                If (contentValue Is "VALUENULL") Then
                    contentValue = dict("Value")
                End If



            Catch ex As Exception

            End Try
            If isExtended Is "EXTENDED" Then
                ParameterDef = "1"
            Else
                ParameterDef = "0"
            End If
            command.Parameters.Add(New SqlParameter("DisplayName", displayName))
            command.Parameters.Add(New SqlParameter("IS_EXTENDED", ParameterDef))
            command.Parameters.Add(New SqlParameter("FIELD_DISPLAY_NAME", fieldDisplayName))
            command.Parameters.Add(New SqlParameter("CONTENT_DESC", contentDesc))
            command.Parameters.Add(New SqlParameter("CONTENT_TYPE", contentType))
            command.Parameters.Add(New SqlParameter("NOTE", note))
            command.Parameters.Add(New SqlParameter("CONTENT_VALUE", contentValue))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try


        Return strRsp
    End Function


    <WebMethod()>
    Public Function AddEmvKeyTbl(data As String) As String

        Dim rawresp As String = data

        Dim jss As New JavaScriptSerializer()
        Dim dict As Dictionary(Of String, String) = jss.Deserialize(Of Dictionary(Of String, String))(rawresp)
        Dim displayName As String
        Dim revokeDate As String
        Dim isExtended As String
        Dim fieldDisplayName As String
        Dim contentDesc As String
        Dim contentType As String
        Dim note As String
        Dim contentValue As String
        Dim ParameterDef As String



        Dim strRsp As String
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)

        contentValue = ""
        displayName = dict("DisplayName")
        revokeDate = dict("Date")

        isExtended = dict("Parameter Type")
        fieldDisplayName = dict("Parameter Name")
        contentDesc = dict("Value")
        contentType = dict("Value Type")
        note = dict("Note")

        contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
        If (contentValue Is "VALUENULL") Then
            contentValue = dict("Value")
        End If
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webCrearEmvKey"
            command.Parameters.Clear()
            Try


                contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
                If (contentValue Is "VALUENULL") Then
                    contentValue = dict("Value")
                End If



            Catch ex As Exception

            End Try
            If isExtended Is "EXTENDED" Then
                ParameterDef = "1"
            Else
                ParameterDef = "0"
            End If
            command.Parameters.Add(New SqlParameter("KeyName", displayName))
            command.Parameters.Add(New SqlParameter("IS_EXTENDED", ParameterDef))
            command.Parameters.Add(New SqlParameter("FIELD_DISPLAY_NAME", fieldDisplayName))
            command.Parameters.Add(New SqlParameter("CONTENT_DESC", contentDesc))
            command.Parameters.Add(New SqlParameter("CONTENT_TYPE", contentType))
            command.Parameters.Add(New SqlParameter("NOTE", note))
            command.Parameters.Add(New SqlParameter("Revoke_Date", revokeDate))
            command.Parameters.Add(New SqlParameter("CONTENT_VALUE", contentValue))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try


        Return strRsp
    End Function



    <WebMethod()>
    Public Function addExtraApp(data As String) As String

        Dim rawresp As String = data

        Dim jss As New JavaScriptSerializer()
        Dim dict As Dictionary(Of String, String) = jss.Deserialize(Of Dictionary(Of String, String))(rawresp)
        Dim displayName As String
        Dim revokeDate As String
        Dim isExtended As String
        Dim fieldDisplayName As String
        Dim contentDesc As String
        Dim contentType As String
        Dim note As String
        Dim contentValue As String
        Dim ParameterDef As String
        Dim strRsp As String
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim appId As String
        Dim appName As String
        Dim inputMask As String
        Dim index As String
        Dim name As String
        Dim description As String
        Dim value As String
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)

        contentValue = ""
        appId = dict("AppId")
        appName = dict("AppName")
        description = dict("Desc")
        inputMask = dict("InputMask")

        index = dict("Index")
        name = dict("Name")
        contentDesc = dict("Description")
        value = dict("Value")


        'contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
        'If (contentValue Is "VALUENULL") Then
        '    contentValue = dict("Value")
        'End If
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webCrearExtraApp"
            command.Parameters.Clear()
            Try


                contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
                If (contentValue Is "VALUENULL") Then
                    contentValue = dict("Value")
                End If



            Catch ex As Exception

            End Try
            'If isExtended Is "EXTENDED" Then
            '    ParameterDef = "1"
            'Else
            '    ParameterDef = "0"
            'End If
            command.Parameters.Add(New SqlParameter("APP_PARA_ID", appId))
            command.Parameters.Add(New SqlParameter("APP_PARA_NAME", appName))
            command.Parameters.Add(New SqlParameter("APP_PARA_DESC", description))
            command.Parameters.Add(New SqlParameter("APP_NAME_EXT", name))
            command.Parameters.Add(New SqlParameter("INDEX_EXT", index))
            command.Parameters.Add(New SqlParameter("DESC_EXT", contentDesc))
            command.Parameters.Add(New SqlParameter("VALUE_EXT", value))
            command.Parameters.Add(New SqlParameter("MAX_EXT", inputMask))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try


        Return strRsp
    End Function

    Function getFieldName(data As String) As String

        Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim db As SqlConnection = New SqlConnection(conexion)
        Dim cmd As SqlCommand
        Dim sqlBuilder As StringBuilder = New StringBuilder

        Dim strRsp As String
        strRsp = ""
        sqlBuilder.Append("SELECT FIELD_NAME FROM MASTER_PARAMETER_DEF WHERE FIELD_DISPLAY_NAME=@f")

        Try
            db.Open()
        Catch ex As Exception

            db.Close()
        End Try

        Try
            cmd = New SqlCommand()
            cmd.Connection = db
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sqlBuilder.ToString
            cmd.Parameters.AddWithValue("@f", data)


            Dim dr As SqlDataReader = cmd.ExecuteReader()

            While (dr.Read())
                strRsp = dr.GetString(0)

            End While


        Catch ex As Exception


        Finally
            cmd = Nothing
            db.Close()
        End Try

        Return strRsp
    End Function


    Public Function getContenValue(fieldName As String, contentDes As String) As String

        Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim db As SqlConnection = New SqlConnection(conexion)
        Dim cmd As SqlCommand
        Dim sqlBuilder As StringBuilder = New StringBuilder

        Dim contentDesc As String
        Dim contentSeq As String
        Dim contentValue As String
        Dim strRsp As String
        Dim strMenu As String

        Dim field_Name As String
        Dim content_desc As String

        field_Name = getFieldName(fieldName)
        content_desc = contentDes
        sqlBuilder.Append("SELECT CONTENT_VALUE FROM MASTER_PARAMETER_DEF WHERE IS_FIELD_HEADER <> 1 AND IS_RESERVED = 0 AND FIELD_NAME=@f AND CONTENT_DESC=@c ORDER BY CONTENT_SEQ")
        strRsp = ""

        Try
            db.Open()
        Catch ex As Exception

            db.Close()
        End Try

        Try
            cmd = New SqlCommand()
            cmd.Connection = db
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sqlBuilder.ToString
            cmd.Parameters.AddWithValue("@f", field_Name)
            cmd.Parameters.AddWithValue("@c", content_desc)


            Dim dr As SqlDataReader = cmd.ExecuteReader()

            '{"1": "@mdo", "2": "@fat", "3": "@twitter"}'
            '{"menu":{"1": "@mdo", "2": "@fat", "3": "@twitter"},"values":{"1": "@mdo", "2": "@fat", "3": "@twitter"}}

            While (dr.Read())
                strRsp = dr.GetString(0)

            End While
            If (strRsp.Length <= 0) Then
                strRsp = "VALUENULL"
            End If

        Catch ex As Exception


        Finally
            cmd = Nothing
            db.Close()
        End Try

        Return strRsp
    End Function
    <WebMethod()>
    Public Function getListValues(data As String) As String

        Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim db As SqlConnection = New SqlConnection(conexion)
        Dim cmd As SqlCommand
        Dim sqlBuilder As StringBuilder = New StringBuilder

        Dim contentDesc As String
        Dim contentSeq As String
        Dim contentValue As String
        Dim strRsp As String
        Dim strMenu As String

        Dim field_Name As String
        field_Name = getFieldName(data)
        sqlBuilder.Append("SELECT CONTENT_DESC ,CONTENT_SEQ, CONTENT_VALUE FROM MASTER_PARAMETER_DEF WHERE IS_FIELD_HEADER <> 1 AND IS_RESERVED = 0 AND FIELD_NAME=@f ORDER BY CONTENT_SEQ")

        Try
            db.Open()
        Catch ex As Exception

            db.Close()
        End Try

        Try
            cmd = New SqlCommand()
            cmd.Connection = db
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sqlBuilder.ToString
            cmd.Parameters.AddWithValue("@f", field_Name)


            Dim dr As SqlDataReader = cmd.ExecuteReader()

            '{"1": "@mdo", "2": "@fat", "3": "@twitter"}'
            '{"menu":{"1": "@mdo", "2": "@fat", "3": "@twitter"},"values":{"1": "@mdo", "2": "@fat", "3": "@twitter"}}

            strMenu = "{"
            While (dr.Read())
                contentDesc = dr.GetString(0)
                contentSeq = dr.GetInt32(1)
                contentValue = dr.GetString(2)
                strMenu += """" + contentSeq + """:""" + contentDesc + ""","
            End While
            If (strMenu.Length > 1 And strMenu.Length > 1) Then
                strMenu = strMenu.Remove(strMenu.Length - 1)
                strMenu += "}"
                strRsp = strMenu

            Else
                strRsp = "LISTNULL"
            End If

        Catch ex As Exception


        Finally
            cmd = Nothing
            db.Close()
        End Try

        Return strRsp
    End Function



    <WebMethod()>
    Public Function editTerminalStis(data As String) As String
        Dim strRsp As String
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)
        Dim rawresp As String = data

        Dim jss As New JavaScriptSerializer()
        Dim dict As Dictionary(Of String, String) = jss.Deserialize(Of Dictionary(Of String, String))(rawresp)
        Dim contentValue As String
        contentValue = ""
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webEditTerminalParameter_Stis"
            command.Parameters.Clear()
            Dim s As String
            s = ""
            Try


                contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
                If (contentValue Is "VALUENULL") Then
                    contentValue = dict("Value")
                End If



            Catch ex As Exception

            End Try

            command.Parameters.Add(New SqlParameter("TERMINAL_REC_NO", dict("Terminal Rec No")))
            command.Parameters.Add(New SqlParameter("FIELD_DISPLAY_NAME", dict("Parameter Name")))
            command.Parameters.Add(New SqlParameter("CONTENT_DESC", dict("Value")))
            command.Parameters.Add(New SqlParameter("CONTENT_VALUE", contentValue))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try


        Return strRsp
    End Function

    <WebMethod()>
    Public Function editAcquirer(data As String) As String
        Dim strRsp As String
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)
        Dim rawresp As String = data

        Dim jss As New JavaScriptSerializer()
        Dim dict As Dictionary(Of String, String) = jss.Deserialize(Of Dictionary(Of String, String))(rawresp)
        Dim contentValue As String
        contentValue = ""
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webEditDatosAcquirer_Stis"
            command.Parameters.Clear()
            Dim s As String
            s = ""
            Try

                contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
                If (contentValue Is "VALUENULL") Then
                    contentValue = dict("Value")
                End If



            Catch ex As Exception

            End Try

            command.Parameters.Add(New SqlParameter("TERMINAL_REC_NO", dict("Terminal Rec No")))
            command.Parameters.Add(New SqlParameter("FIELD_DISPLAY_NAME", dict("Parameter Name")))
            command.Parameters.Add(New SqlParameter("REFER_CODE", dict("Refer Code")))
            command.Parameters.Add(New SqlParameter("CONTENT_DESC", dict("Value")))
            command.Parameters.Add(New SqlParameter("CONTENT_VALUE", contentValue))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try


        Return strRsp
    End Function

    <WebMethod()>
    Public Function editIssuer(data As String) As String
        Dim strRsp As String
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)
        Dim rawresp As String = data

        Dim jss As New JavaScriptSerializer()
        Dim dict As Dictionary(Of String, String) = jss.Deserialize(Of Dictionary(Of String, String))(rawresp)
        Dim contentValue As String
        contentValue = ""
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webEditDatosIssuer_Stis"
            command.Parameters.Clear()
            Dim s As String
            s = ""
            Try

                contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
                If (contentValue Is "VALUENULL") Then
                    contentValue = dict("Value")
                End If



            Catch ex As Exception

            End Try

            command.Parameters.Add(New SqlParameter("TERMINAL_REC_NO", dict("Terminal Rec No")))
            command.Parameters.Add(New SqlParameter("FIELD_DISPLAY_NAME", dict("Parameter Name")))
            command.Parameters.Add(New SqlParameter("REFER_CODE", dict("Refer Code")))
            command.Parameters.Add(New SqlParameter("CONTENT_DESC", dict("Value")))
            command.Parameters.Add(New SqlParameter("CONTENT_VALUE", contentValue))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try


        Return strRsp
    End Function

    <WebMethod()>
    Public Function editCardRange(data As String) As String
        Dim strRsp As String
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)
        Dim rawresp As String = data

        Dim jss As New JavaScriptSerializer()
        Dim dict As Dictionary(Of String, String) = jss.Deserialize(Of Dictionary(Of String, String))(rawresp)
        Dim contentValue As String
        contentValue = ""
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webEditDatosCardRange_Stis"
            command.Parameters.Clear()
            Dim s As String
            s = ""
            Try

                contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
                If (contentValue Is "VALUENULL") Then
                    contentValue = dict("Value")
                End If



            Catch ex As Exception

            End Try

            command.Parameters.Add(New SqlParameter("TERMINAL_REC_NO", dict("Terminal Rec No")))
            command.Parameters.Add(New SqlParameter("FIELD_DISPLAY_NAME", dict("Parameter Name")))
            command.Parameters.Add(New SqlParameter("REFER_CODE", dict("Refer Code")))
            command.Parameters.Add(New SqlParameter("CONTENT_DESC", dict("Value")))
            command.Parameters.Add(New SqlParameter("CONTENT_VALUE", contentValue))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try


        Return strRsp
    End Function
    <WebMethod()>
    Public Function terminalParameterEmv(data As String) As String
        Dim strRsp As String
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)
        Dim rawresp As String = data

        Dim jss As New JavaScriptSerializer()
        Dim dict As Dictionary(Of String, String) = jss.Deserialize(Of Dictionary(Of String, String))(rawresp)
        Dim contentValue As String
        contentValue = ""
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webEditDatosEmvApplication_Stis"
            command.Parameters.Clear()
            Dim s As String
            s = ""
            Try

                contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
                If (contentValue Is "VALUENULL") Then
                    contentValue = dict("Value")
                End If



            Catch ex As Exception

            End Try

            command.Parameters.Add(New SqlParameter("TERMINAL_REC_NO", dict("Terminal Rec No")))
            command.Parameters.Add(New SqlParameter("FIELD_DISPLAY_NAME", dict("Parameter Name")))
            command.Parameters.Add(New SqlParameter("REFER_CODE", dict("Refer Code")))
            command.Parameters.Add(New SqlParameter("CONTENT_DESC", dict("Value")))
            command.Parameters.Add(New SqlParameter("CONTENT_VALUE", contentValue))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try


        Return strRsp
    End Function
    <WebMethod()>
    Public Function listarEmvLevel2KeyParameter(data As String) As String
        Dim strRsp As String
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)
        Dim rawresp As String = data

        Dim jss As New JavaScriptSerializer()
        Dim dict As Dictionary(Of String, String) = jss.Deserialize(Of Dictionary(Of String, String))(rawresp)
        Dim contentValue As String
        Dim contentType As String

        contentValue = ""
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webEditParameterKey_Stis"
            command.Parameters.Clear()
            Dim s As String
            s = ""
            Try

                contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
                If (contentValue Is "VALUENULL") Then
                    contentValue = dict("Value")
                End If



            Catch ex As Exception

            End Try

            command.Parameters.Add(New SqlParameter("TABLE_KEY_CODE", dict("Display Name")))
            command.Parameters.Add(New SqlParameter("FIELD_DISPLAY_NAME", dict("Parameter Name")))
            command.Parameters.Add(New SqlParameter("CONTENT_DESC", dict("Value")))
            command.Parameters.Add(New SqlParameter("CONTENT_VALUE", contentValue))
            command.Parameters.Add(New SqlParameter("CONTENT_TYPE", dict("Value Type")))


            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try


        Return strRsp
    End Function
    <WebMethod()>
    Public Function terminalExtraAplicationParameter(data As String) As String
        Dim strRsp As String
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)
        Dim rawresp As String = data

        Dim jss As New JavaScriptSerializer()
        Dim dict As Dictionary(Of String, String) = jss.Deserialize(Of Dictionary(Of String, String))(rawresp)
        Dim contentValue As String
        Dim contentType As String
        Dim TerminalRecNo As String
        contentValue = ""
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webEditDatosExtraApplication_Stis"
            command.Parameters.Clear()
            Dim s As String
            s = ""
            Try

                contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
                If (contentValue Is "VALUENULL") Then
                    contentValue = dict("Value")
                End If



            Catch ex As Exception

            End Try
            TerminalRecNo = dict("Terminal Rec No")

            command.Parameters.Add(New SqlParameter("TERMINAL_REC_NO", TerminalRecNo))
            command.Parameters.Add(New SqlParameter("FIELD_DISPLAY_NAME", dict("Parameter Name")))
            command.Parameters.Add(New SqlParameter("CONTENT_VALUE", contentValue))
            command.Parameters.Add(New SqlParameter("REFER_CODE", dict("Refer Code")))


            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try


        Return strRsp
    End Function

    <WebMethod()>
    Public Function createNewAcquirer(data As String) As String
        Dim rawresp As String = data

        Dim jss As New JavaScriptSerializer()
        Dim dict As Dictionary(Of String, String) = jss.Deserialize(Of Dictionary(Of String, String))(rawresp)
        Dim code As String
        Dim displayName As String
        Dim isExtended As String
        Dim fieldDisplayName As String
        Dim contentDesc As String
        Dim contentType As String
        Dim note As String
        Dim contentValue As String
        Dim ParameterDef As String
        Dim TerminalRecNo As String


        Dim strRsp As String
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)

        contentValue = ""
        code = dict("Code")
        displayName = dict("DisplayName")
        isExtended = dict("Parameter Type")
        fieldDisplayName = dict("Parameter Name")
        contentDesc = dict("Value")
        contentType = dict("Value Type")
        TerminalRecNo = dict("TerminalRecNo")
        note = dict("Note")

        contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
        If (contentValue Is "VALUENULL") Then
            contentValue = dict("Value")
        End If
        Try
            'Abrir Conexion
            connection.Open()
            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webCrearAcquirerTerminal"
            command.Parameters.Clear()
            Try


                contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
                If (contentValue Is "VALUENULL") Then
                    contentValue = dict("Value")
                End If



            Catch ex As Exception

            End Try

            If isExtended Is "EXTENDED" Then
                ParameterDef = "1"
            Else
                ParameterDef = "0"
            End If
            command.Parameters.Add(New SqlParameter("Code", code))
            command.Parameters.Add(New SqlParameter("DisplayName", displayName))
            command.Parameters.Add(New SqlParameter("IS_EXTENDED", ParameterDef))
            command.Parameters.Add(New SqlParameter("FIELD_DISPLAY_NAME", fieldDisplayName))
            command.Parameters.Add(New SqlParameter("CONTENT_DESC", contentDesc))
            command.Parameters.Add(New SqlParameter("NOTE", note))
            command.Parameters.Add(New SqlParameter("CONTENT_VALUE", contentValue))
            command.Parameters.Add(New SqlParameter("TERMINAL_REC_NO", TerminalRecNo))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try


        Return strRsp
    End Function

    <WebMethod()>
    Public Function createNewIssuer(data As String) As String
        Dim rawresp As String = data

        Dim jss As New JavaScriptSerializer()
        Dim dict As Dictionary(Of String, String) = jss.Deserialize(Of Dictionary(Of String, String))(rawresp)
        Dim code As String
        Dim displayName As String
        Dim isExtended As String
        Dim fieldDisplayName As String
        Dim contentDesc As String
        Dim contentType As String
        Dim note As String
        Dim contentValue As String
        Dim ParameterDef As String
        Dim TerminalRecNo As String
        Dim acq As String


        Dim strRsp As String
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)

        contentValue = ""
        code = dict("Code")
        displayName = dict("DisplayName")
        isExtended = dict("Parameter Type")
        fieldDisplayName = dict("Parameter Name")
        contentDesc = dict("Value")
        contentType = dict("Value Type")
        TerminalRecNo = dict("TerminalRecNo")
        note = dict("Note")
        acq = dict("ACQ")

        contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
        If (contentValue Is "VALUENULL") Then
            contentValue = dict("Value")
        End If
        Try
            'Abrir Conexion
            connection.Open()
            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webCrearIssuerTerminal"
            command.Parameters.Clear()
            Try


                contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
                If (contentValue Is "VALUENULL") Then
                    contentValue = dict("Value")
                End If



            Catch ex As Exception

            End Try

            If isExtended Is "EXTENDED" Then
                ParameterDef = "1"
            Else
                ParameterDef = "0"
            End If
            command.Parameters.Add(New SqlParameter("Code", code))
            command.Parameters.Add(New SqlParameter("ACQ_CODE", acq))

            command.Parameters.Add(New SqlParameter("DisplayName", displayName))
            command.Parameters.Add(New SqlParameter("IS_EXTENDED", ParameterDef))
            command.Parameters.Add(New SqlParameter("FIELD_DISPLAY_NAME", fieldDisplayName))
            command.Parameters.Add(New SqlParameter("CONTENT_DESC", contentDesc))
            command.Parameters.Add(New SqlParameter("NOTE", note))
            command.Parameters.Add(New SqlParameter("CONTENT_VALUE", contentValue))
            command.Parameters.Add(New SqlParameter("TERMINAL_REC_NO", TerminalRecNo))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try


        Return strRsp
    End Function

    <WebMethod()>
    Public Function createNewCardRange(data As String) As String
        Dim rawresp As String = data

        Dim jss As New JavaScriptSerializer()
        Dim dict As Dictionary(Of String, String) = jss.Deserialize(Of Dictionary(Of String, String))(rawresp)
        Dim code As String
        Dim acq As String
        Dim iss As String

        Dim displayName As String
        Dim isExtended As String
        Dim fieldDisplayName As String
        Dim contentDesc As String
        Dim contentType As String
        Dim note As String
        Dim contentValue As String
        Dim ParameterDef As String
        Dim TerminalRecNo As String


        Dim strRsp As String
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)

        contentValue = ""
        code = dict("Code")
        acq = dict("ACQ")
        iss = dict("ISS")

        displayName = dict("DisplayName")
        isExtended = dict("Parameter Type")
        fieldDisplayName = dict("Parameter Name")
        contentDesc = dict("Value")
        contentType = dict("Value Type")
        TerminalRecNo = dict("TerminalRecNo")
        note = dict("Note")

        contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
        If (contentValue Is "VALUENULL") Then
            contentValue = dict("Value")
        End If
        Try
            'Abrir Conexion
            connection.Open()
            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webCrearCardRangeTerminal"
            command.Parameters.Clear()
            Try


                contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
                If (contentValue Is "VALUENULL") Then
                    contentValue = dict("Value")
                End If



            Catch ex As Exception

            End Try

            If isExtended Is "EXTENDED" Then
                ParameterDef = "1"
            Else
                ParameterDef = "0"
            End If
            command.Parameters.Add(New SqlParameter("Code", code))
            command.Parameters.Add(New SqlParameter("ACQ_CODE", acq))
            command.Parameters.Add(New SqlParameter("ISS_CODE", iss))
            command.Parameters.Add(New SqlParameter("DisplayName", displayName))
            command.Parameters.Add(New SqlParameter("IS_EXTENDED", ParameterDef))
            command.Parameters.Add(New SqlParameter("FIELD_DISPLAY_NAME", fieldDisplayName))
            command.Parameters.Add(New SqlParameter("CONTENT_DESC", contentDesc))
            command.Parameters.Add(New SqlParameter("NOTE", note))
            command.Parameters.Add(New SqlParameter("CONTENT_VALUE", contentValue))
            command.Parameters.Add(New SqlParameter("TERMINAL_REC_NO", TerminalRecNo))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try


        Return strRsp
    End Function


    <WebMethod()>
    Public Function createNewEmvApplication(data As String) As String
        Dim rawresp As String = data

        Dim jss As New JavaScriptSerializer()
        Dim dict As Dictionary(Of String, String) = jss.Deserialize(Of Dictionary(Of String, String))(rawresp)
        Dim code As String
        Dim displayName As String
        Dim isExtended As String
        Dim fieldDisplayName As String
        Dim contentDesc As String
        Dim contentType As String
        Dim note As String
        Dim contentValue As String
        Dim ParameterDef As String
        Dim TerminalRecNo As String


        Dim strRsp As String
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)

        contentValue = ""
        code = dict("Code")
        displayName = dict("DisplayName")
        isExtended = dict("Parameter Type")
        fieldDisplayName = dict("Parameter Name")
        contentDesc = dict("Value")
        contentType = dict("Value Type")
        TerminalRecNo = dict("TerminalRecNo")
        note = dict("Note")

        contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
        If (contentValue Is "VALUENULL") Then
            contentValue = dict("Value")
        End If
        Try
            'Abrir Conexion
            connection.Open()
            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webCrearEmvAppTerminal"
            command.Parameters.Clear()
            Try


                contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
                If (contentValue Is "VALUENULL") Then
                    contentValue = dict("Value")
                End If



            Catch ex As Exception

            End Try

            If isExtended Is "EXTENDED" Then
                ParameterDef = "1"
            Else
                ParameterDef = "0"
            End If
            'command.Parameters.Add(New SqlParameter("Code", code))
            command.Parameters.Add(New SqlParameter("DisplayName", displayName))
            command.Parameters.Add(New SqlParameter("IS_EXTENDED", ParameterDef))
            command.Parameters.Add(New SqlParameter("FIELD_DISPLAY_NAME", fieldDisplayName))
            command.Parameters.Add(New SqlParameter("CONTENT_DESC", contentDesc))
            command.Parameters.Add(New SqlParameter("NOTE", note))
            command.Parameters.Add(New SqlParameter("CONTENT_VALUE", contentValue))
            command.Parameters.Add(New SqlParameter("TERMINAL_REC_NO", TerminalRecNo))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try


        Return strRsp
    End Function


    <WebMethod()>
    Public Function createNewEmvKey(data As String) As String
        Dim rawresp As String = data

        Dim jss As New JavaScriptSerializer()
        Dim dict As Dictionary(Of String, String) = jss.Deserialize(Of Dictionary(Of String, String))(rawresp)
        Dim code As String
        Dim displayName As String
        Dim isExtended As String
        Dim fieldDisplayName As String
        Dim contentDesc As String
        Dim contentType As String
        Dim note As String
        Dim contentValue As String
        Dim ParameterDef As String
        Dim TerminalRecNo As String


        Dim strRsp As String
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)

        contentValue = ""
        code = dict("Code")
        displayName = dict("DisplayName")
        isExtended = dict("Parameter Type")
        fieldDisplayName = dict("Parameter Name")
        contentDesc = dict("Value")
        contentType = dict("Value Type")
        TerminalRecNo = dict("TerminalRecNo")
        note = dict("Note")

        contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
        If (contentValue Is "VALUENULL") Then
            contentValue = dict("Value")
        End If
        Try
            'Abrir Conexion
            connection.Open()
            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webCrearEmvKeyTerminal"
            command.Parameters.Clear()
            Try


                contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
                If (contentValue Is "VALUENULL") Then
                    contentValue = dict("Value")
                End If



            Catch ex As Exception

            End Try

            If isExtended Is "EXTENDED" Then
                ParameterDef = "1"
            Else
                ParameterDef = "0"
            End If
            'command.Parameters.Add(New SqlParameter("Code", code))
            command.Parameters.Add(New SqlParameter("DisplayName", displayName))
            command.Parameters.Add(New SqlParameter("IS_EXTENDED", ParameterDef))
            command.Parameters.Add(New SqlParameter("FIELD_DISPLAY_NAME", fieldDisplayName))
            command.Parameters.Add(New SqlParameter("CONTENT_DESC", contentDesc))
            command.Parameters.Add(New SqlParameter("NOTE", note))
            command.Parameters.Add(New SqlParameter("CONTENT_VALUE", contentValue))
            command.Parameters.Add(New SqlParameter("TERMINAL_REC_NO", TerminalRecNo))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try


        Return strRsp
    End Function


    <WebMethod()>
    Public Function addTerminalStis(data As String) As String
        Dim rawresp As String = data

        Dim jss As New JavaScriptSerializer()
        Dim dict As Dictionary(Of String, String) = jss.Deserialize(Of Dictionary(Of String, String))(rawresp)
        Dim code As String
        Dim displayName As String
        Dim isExtended As String
        Dim fieldDisplayName As String
        Dim contentDesc As String
        Dim contentType As String
        Dim note As String
        Dim contentValue As String
        Dim ParameterDef As String
        Dim TerminalRecNo As String


        Dim tid As String
        Dim ownerName As String
        Dim merchantName As String
        Dim shopName As String
        Dim detailAddres As String
        Dim shopContact As String
        Dim shopTelNo As String
        Dim officeContact As String
        Dim officeTelNo As String
        Dim stat As String

        Dim strRsp As String
        Dim status As Integer

        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)

        contentValue = ""

        tid = dict("Tid")
        ownerName = dict("OwnerName")
        merchantName = dict("MerchantName")
        shopName = dict("ShopName")
        detailAddres = dict("DetailAddres")
        shopContact = dict("ShopContact")
        shopTelNo = dict("ShopTelNo")
        officeContact = dict("OfficeContact")
        officeTelNo = dict("OfficeTelNo")
        stat = dict("Status")

        '     displayName = dict("DisplayName")
        isExtended = dict("Parameter Type")
        fieldDisplayName = dict("Parameter Name")
        contentDesc = dict("Value")
        contentType = dict("Value Type")
        '  TerminalRecNo = dict("TerminalRecNo")
        note = dict("Note")

        contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
        If (contentValue Is "VALUENULL") Then
            contentValue = dict("Value")
        End If
        Try
            'Abrir Conexion
            connection.Open()
            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webCrearTerminal"
            command.Parameters.Clear()
            Try


                contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
                If (contentValue Is "VALUENULL") Then
                    contentValue = dict("Value")
                End If



            Catch ex As Exception

            End Try

            If isExtended Is "EXTENDED" Then
                ParameterDef = "1"
            Else
                ParameterDef = "0"
            End If
            'command.Parameters.Add(New SqlParameter("Code", code))
            command.Parameters.Add(New SqlParameter("TID", tid))
            command.Parameters.Add(New SqlParameter("OWNER_NAME", ownerName))
            command.Parameters.Add(New SqlParameter("MERCHANT_NAME", merchantName))
            command.Parameters.Add(New SqlParameter("SHOP_NAME", shopName))
            command.Parameters.Add(New SqlParameter("DETAIL_ADD", detailAddres))
            command.Parameters.Add(New SqlParameter("SHOP_CONTACT", shopContact))
            command.Parameters.Add(New SqlParameter("SHOP_TEL", shopTelNo))
            command.Parameters.Add(New SqlParameter("OFFICE_CONTACT", officeContact))
            command.Parameters.Add(New SqlParameter("OFFICE_TEL", officeTelNo))
            command.Parameters.Add(New SqlParameter("STATUS", stat))

            command.Parameters.Add(New SqlParameter("IS_EXTENDED", ParameterDef))
            command.Parameters.Add(New SqlParameter("FIELD_DISPLAY_NAME", fieldDisplayName))
            command.Parameters.Add(New SqlParameter("CONTENT_DESC", contentDesc))
            command.Parameters.Add(New SqlParameter("NOTE", note))
            command.Parameters.Add(New SqlParameter("CONTENT_VALUE", contentValue))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try


        Return strRsp
    End Function



    <WebMethod()>
    Public Function createNewEmvExtraApplication(data As String) As String
        Dim rawresp As String = data

        Dim jss As New JavaScriptSerializer()
        Dim dict As Dictionary(Of String, String) = jss.Deserialize(Of Dictionary(Of String, String))(rawresp)
        Dim code As String
        Dim displayName As String
        Dim isExtended As String
        Dim fieldDisplayName As String
        Dim contentDesc As String
        Dim contentType As String
        Dim note As String
        Dim contentValue As String
        Dim ParameterDef As String
        Dim TerminalRecNo As String


        Dim strRsp As String
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                    System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)

        contentValue = ""
        code = dict("Code")
        displayName = dict("DisplayName")

        fieldDisplayName = dict("Name")
        contentDesc = dict("Description")
        contentValue = dict("Value")
        TerminalRecNo = dict("TerminalRecNo")
        note = " "

        'contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
        'If (contentValue Is "VALUENULL") Then
        '    contentValue = dict("Value")
        'End If
        Try
            'Abrir Conexion
            connection.Open()
            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webCrearExtraAppTerminal"
            command.Parameters.Clear()
            Try


                contentValue = getContenValue(dict("Parameter Name"), dict("Value"))
                If (contentValue Is "VALUENULL") Then
                    contentValue = dict("Value")
                End If



            Catch ex As Exception

            End Try


            'command.Parameters.Add(New SqlParameter("Code", code))
            command.Parameters.Add(New SqlParameter("ID", code))
            command.Parameters.Add(New SqlParameter("DisplayName", displayName))
            command.Parameters.Add(New SqlParameter("FIELD_DISPLAY_NAME", fieldDisplayName))
            command.Parameters.Add(New SqlParameter("CONTENT_DESC", contentValue))
            command.Parameters.Add(New SqlParameter("NOTE", " "))
            command.Parameters.Add(New SqlParameter("CONTENT_VALUE", contentValue))
            command.Parameters.Add(New SqlParameter("TERMINAL_REC_NO", TerminalRecNo))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try


        Return strRsp
    End Function

    <WebMethod()>
    Public Function getConfiguracionesCodigo(hash As String) As Hash
        Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString
        Dim db As SqlConnection = New SqlConnection(conexion)
        Dim cmd As SqlCommand
        Dim sqlBuilder As StringBuilder = New StringBuilder
        Dim sha1 As String = ""
        sqlBuilder.Append("SELECT O.opc_id, O.opc_cod_boton,O.opc_proccess_code,O.opc_nombre_recaudo,O.opc_nombre, O.opc_posicion_inicial, O.opc_posicion_final, ISNULL(O.opc_prefijo,'') as prefijo FROM codigo_barras O ")
        Dim strresultado As String = ""
        Dim booleana As Boolean = False
        Dim clase As New Hash
        Dim clase2 As New Hash
        clase2.hash = "null"
        Dim recaudaciones As New List(Of Recaudaciones)
        Try
            db.Open()
        Catch ex As Exception
            db.Close()
        End Try
        Try
            cmd = New SqlCommand()
            cmd.Connection = db
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sqlBuilder.ToString
            Dim dr As SqlDataReader = cmd.ExecuteReader()
            Dim datoString As String = ""

            While dr.Read
                booleana = True
                Dim o As New Recaudaciones
                o.id = Trim(dr.GetInt16(0))
                o.CodBoton = Trim(dr.GetString(1))
                o.ProCode = Trim(dr.GetString(2))
                o.NombreTransRecaudacion = Trim(dr.GetString(3))
                o.NombreTrans = Trim(dr.GetString(4))
                o.PosInicial = Trim(dr.GetString(5))
                o.PosFinal = Trim(dr.GetString(6))
                o.Prefijo = Trim(dr.GetString(7))
                recaudaciones.Add(o)
                datoString += Trim(dr.GetInt16(0)) + Trim(dr.GetString(1)) + Trim(dr.GetString(2)) + Trim(dr.GetString(3)) + Trim(dr.GetString(4)) + Trim(dr.GetString(5)) + Trim(dr.GetString(6) + Trim(dr.GetString(7)))
            End While
            clase.Recaudaciones = recaudaciones
            Dim enc As New UTF8Encoding
            Dim data() As Byte = enc.GetBytes(Convert.ToString(datoString))
            Dim result() As Byte

            Dim sha As New SHA1CryptoServiceProvider

            result = sha.ComputeHash(data)

            Dim sb As New StringBuilder
            Dim max As Int32 = result.Length

            For i As Integer = 0 To max - 1
                If (result(i) < 16) Then
                    sb.Append("0")
                End If
                sb.Append(result(i).ToString("x"))
            Next
            sha1 = sb.ToString().ToUpper()
            clase.hash = sha1
            If booleana Then
                If hash.Equals(sha1) Then
                    Return clase2
                Else
                    Return clase
                End If
            Else
                Return clase2
            End If

        Catch ex As Exception
            db.Close()
        Finally
            cmd = Nothing
            db.Close()

        End Try
        Return clase2
    End Function

    <WebMethod()>
    Public Function getConfiguracionesTCP(hash As String) As HashTcp
        Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString
        Dim db As SqlConnection = New SqlConnection(conexion)
        Dim cmd As SqlCommand
        Dim sqlBuilder As StringBuilder = New StringBuilder
        Dim sha1 As String = ""
        sqlBuilder.Append("SELECT O.opc_id, O.opc_operador, O.opc_ip_uno, O.opc_puerto_uno, O.opc_ip_dos, O.opc_puerto_dos, O.opc_descripcion FROM configuracion_tcp O")
        Dim strresultado As String = ""
        Dim booleana As Boolean = False
        Dim clase As New HashTcp
        Dim clase2 As New HashTcp
        Dim configuraciones As New List(Of Configuraciones)
        Try
            db.Open()
        Catch ex As Exception
            db.Close()
        End Try
        Try
            cmd = New SqlCommand()
            cmd.Connection = db
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sqlBuilder.ToString
            Dim dr As SqlDataReader = cmd.ExecuteReader()
            Dim datoString As String = ""

            While dr.Read
                booleana = True
                Dim o As New Configuraciones
                o.Id = Trim(dr.GetInt16(0))
                o.Operador = Trim(dr.GetString(1))
                o.IpUno = Trim(dr.GetString(2))
                o.PuertoUno = Trim(dr.GetString(3))
                o.IpDos = Trim(dr.GetString(4))
                o.PuertoDos = Trim(dr.GetString(5))
                o.Descripcion = Trim(dr.GetString(6))
                configuraciones.Add(o)
                datoString += Trim(dr.GetInt16(0)) + Trim(dr.GetString(1)) + Trim(dr.GetString(2)) + Trim(dr.GetString(3)) + Trim(dr.GetString(4)) + Trim(dr.GetString(5)) + Trim(dr.GetString(6))
            End While
            clase.configuraciones = configuraciones
            clase.intentosIp = getintentosIp().intentos
            datoString += clase.intentosIp
            Dim data() As Byte = ASCIIEncoding.ASCII.GetBytes(Convert.ToString(datoString))
            Dim result() As Byte
            Dim sha As New SHA1CryptoServiceProvider
            result = sha.ComputeHash(data)
            Dim sb As New StringBuilder
            Dim max As Integer = result.Length

            For i As Integer = 0 To max - 1
                If (result(i) < 32) Then
                    sb.Append("0")
                End If
                sb.Append(result(i).ToString("x"))
            Next
            sha1 = sb.ToString().ToUpper()
            clase.hash = sha1
            If booleana Then
                If hash.Equals(sha1) Then
                    Return clase2
                Else
                    Return clase
                End If
            Else
                Return clase2
            End If

        Catch ex As Exception
            db.Close()
        Finally
            cmd = Nothing
            db.Close()

        End Try
        Return clase2
    End Function

    Private Function getintentosIp() As intentosIp
        Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString
        Dim db As SqlConnection = New SqlConnection(conexion)
        Dim cmd As SqlCommand
        Dim sqlBuilder As StringBuilder = New StringBuilder
        Dim intentosIpv As New intentosIp
        sqlBuilder.Append("SELECT opc_intentos from intentos_ip where opc_id = 1")
        Try
            db.Open()
        Catch ex As Exception
            db.Close()
        End Try
        Try
            cmd = New SqlCommand()
            cmd.Connection = db
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sqlBuilder.ToString
            Dim dr As SqlDataReader = cmd.ExecuteReader()

            If dr.Read Then
                intentosIpv.intentos = Trim(dr.GetString(0))
            End If
        Catch ex As Exception
            db.Close()
        Finally
            cmd = Nothing
            db.Close()

        End Try
        Return intentosIpv
    End Function


End Class

