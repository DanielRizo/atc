﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AssistantRemote.aspx.vb" Inherits="Assistant_Remote_AssistantRemote" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%>:: ASISTENTE REMOTO
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <!-- Favicon icon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon" />
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet" />
    <!-- waves.css -->
    <link rel="stylesheet" href="assets/pages/waves/css/waves.min.css" type="text/css" media="all" />
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap/css/bootstrap.min.css" />
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="assets/icon/themify-icons/themify-icons.css" />
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="assets/icon/icofont/css/icofont.css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="assets/icon/font-awesome/css/font-awesome.min.css" />
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/jquery.mCustomScrollbar.css" />
    <script type="text/javascript" src="../js/msgAlert.js"></script>


    <script src="https://ajax.aspnetcdn.com/ajax/jquery/jquery-1.8.0.js"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.22/jquery-ui.js"></script>
    <link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.10/themes/redmond/jquery-ui.css" />
    <meta http-equiv="refresh" content="" />
    <!-- Google Js Api / Chart and others -->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

    <!-- charts Related JS -->
    <script type="text/javascript" src="../js/raphael.js"></script>
    <script type="text/javascript" src="../js/analytics.js"></script>
    <script type="text/javascript" src="../js/popup.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed"
            PostBackUrl="~/Assistant_Remote/Manager.aspx">Opciones</asp:LinkButton>
    </li>
    <li>Asistente remoto</li>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">

    <asp:Panel ID="pnlMsg" runat="server" Visible="False">
        <div class="alert alert-success">
            <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
            <a href="#" class="close tips" title="Cerrar"></a>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlError" runat="server" Visible="False">
        <div class="alert alert-danger">
            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
            <a href="#" class="close tips" title="Cerrar"></a>
        </div>
    </asp:Panel>

    <div class="jumbotron text-center">
        <h1>ASISTENTE REMOTO</h1>
        <p>Seleccione el terminal ID: ↓↓</p>
    </div>
    <style>
        .form-rounded {
            border-radius: 1rem;
        }
    </style>

    <script>
        (function (Polaris) { Polaris.uniform = { options: { selectClass: "selector", radioClass: "radio", checkboxClass: "checker", fileClass: "uploader", filenameClass: "filename", fileBtnClass: "action", fileDefaultText: "No hay archivo seleccionado.", fileBtnText: "Seleccione...", checkedClass: "checked", focusClass: "focus", disabledClass: "disabled", buttonClass: "button", activeClass: "active", hoverClass: "hover", useID: true, idPrefix: "uniform", resetSelector: false, autoHide: true }, elements: [] }; if (a.browser.msie && Polaris.browser.version < 7) { Polaris.support.selectOpacity = false } else { Polaris.support.selectOpacity = true } Polaris.fn.uniform = function (k) { k = Polaris.extend(a.uniform.options, k); var d = this; if (k.resetSelector != false) { a(k.resetSelector).mouseup(function () { function l() { Polaris.uniform.update(d) } setTimeout(l, 10) }) } function j(l) { $el = a(l); $el.addClass($el.attr("type")); b(l) } function g(l) { a(l).addClass("uniform"); b(l) } function i(o) { var m = a(o); var p = a("<div>"), l = a("<span>"); p.addClass(k.buttonClass); if (k.useID && m.attr("id") != "") { p.attr("id", k.idPrefix + "-" + m.attr("id")) } var n; if (m.is("a") || m.is("button")) { n = m.text() } else { if (m.is(":submit") || m.is(":reset") || m.is("input[type=button]")) { n = m.attr("value") } } n = n == "" ? m.is(":reset") ? "Reset" : "Submit" : n; l.html(n); m.css("opacity", 0); m.wrap(p); m.wrap(l); p = m.closest("div"); l = m.closest("span"); if (m.is(":disabled")) { p.addClass(k.disabledClass) } p.bind({ "mouseenter.uniform": function () { p.addClass(k.hoverClass) }, "mouseleave.uniform": function () { p.removeClass(k.hoverClass); p.removeClass(k.activeClass) }, "mousedown.uniform touchbegin.uniform": function () { p.addClass(k.activeClass) }, "mouseup.uniform touchend.uniform": function () { p.removeClass(k.activeClass) }, "click.uniform touchend.uniform": function (r) { if (a(r.target).is("span") || a(r.target).is("div")) { if (o[0].dispatchEvent) { var q = document.createEvent("MouseEvents"); q.initEvent("click", true, true); o[0].dispatchEvent(q) } else { o[0].click() } } } }); o.bind({ "focus.uniform": function () { p.addClass(k.focusClass) }, "blur.uniform": function () { p.removeClass(k.focusClass) } }); Polaris.uniform.noSelect(p); b(o) } function e(o) { var m = a(o); var p = a("<div />"), l = a("<span />"); if (!m.css("display") == "none" && k.autoHide) { p.hide() } p.addClass(k.selectClass); if (k.useID && o.attr("id") != "") { p.attr("id", k.idPrefix + "-" + o.attr("id")) } var n = o.find(":selected:first"); if (n.length == 0) { n = o.find("option:first") } l.html(n.html()); o.css("opacity", 0); o.wrap(p); o.before(l); p = o.parent("div"); l = o.siblings("span"); o.bind({ "change.uniform": function () { l.text(o.find(":selected").html()); p.removeClass(k.activeClass) }, "focus.uniform": function () { p.addClass(k.focusClass) }, "blur.uniform": function () { p.removeClass(k.focusClass); p.removeClass(k.activeClass) }, "mousedown.uniform touchbegin.uniform": function () { p.addClass(k.activeClass) }, "mouseup.uniform touchend.uniform": function () { p.removeClass(k.activeClass) }, "click.uniform touchend.uniform": function () { p.removeClass(k.activeClass) }, "mouseenter.uniform": function () { p.addClass(k.hoverClass) }, "mouseleave.uniform": function () { p.removeClass(k.hoverClass); p.removeClass(k.activeClass) }, "keyup.uniform": function () { l.text(o.find(":selected").html()) } }); if (a(o).attr("disabled")) { p.addClass(k.disabledClass) } Polaris.uniform.noSelect(l); b(o) } function f(n) { var m = a(n); var o = a("<div />"), l = a("<span />"); if (!m.css("display") == "none" && k.autoHide) { o.hide() } o.addClass(k.checkboxClass); if (k.useID && n.attr("id") != "") { o.attr("id", k.idPrefix + "-" + n.attr("id")) } a(n).wrap(o); a(n).wrap(l); l = n.parent(); o = l.parent(); a(n).css("opacity", 0).bind({ "focus.uniform": function () { o.addClass(k.focusClass) }, "blur.uniform": function () { o.removeClass(k.focusClass) }, "click.uniform touchend.uniform": function () { if (!a(n).attr("checked")) { l.removeClass(k.checkedClass) } else { l.addClass(k.checkedClass) } }, "mousedown.uniform touchbegin.uniform": function () { o.addClass(k.activeClass) }, "mouseup.uniform touchend.uniform": function () { o.removeClass(k.activeClass) }, "mouseenter.uniform": function () { o.addClass(k.hoverClass) }, "mouseleave.uniform": function () { o.removeClass(k.hoverClass); o.removeClass(k.activeClass) } }); if (a(n).attr("checked")) { l.addClass(k.checkedClass) } if (a(n).attr("disabled")) { o.addClass(k.disabledClass) } b(n) } function c(n) { var m = a(n); var o = a("<div />"), l = a("<span />"); if (!m.css("display") == "none" && k.autoHide) { o.hide() } o.addClass(k.radioClass); if (k.useID && n.attr("id") != "") { o.attr("id", k.idPrefix + "-" + n.attr("id")) } a(n).wrap(o); a(n).wrap(l); l = n.parent(); o = l.parent(); a(n).css("opacity", 0).bind({ "focus.uniform": function () { o.addClass(k.focusClass) }, "blur.uniform": function () { o.removeClass(k.focusClass) }, "click.uniform touchend.uniform": function () { if (!a(n).attr("checked")) { l.removeClass(k.checkedClass) } else { var p = k.radioClass.split(" ")[0]; a("." + p + " span." + k.checkedClass + ":has([name='" + a(n).attr("name") + "'])").removeClass(k.checkedClass); l.addClass(k.checkedClass) } }, "mousedown.uniform touchend.uniform": function () { if (!a(n).is(":disabled")) { o.addClass(k.activeClass) } }, "mouseup.uniform touchbegin.uniform": function () { o.removeClass(k.activeClass) }, "mouseenter.uniform touchend.uniform": function () { o.addClass(k.hoverClass) }, "mouseleave.uniform": function () { o.removeClass(k.hoverClass); o.removeClass(k.activeClass) } }); if (a(n).attr("checked")) { l.addClass(k.checkedClass) } if (a(n).attr("disabled")) { o.addClass(k.disabledClass) } b(n) } function h(q) { var o = a(q); var r = a("<div />"), p = a("<span>" + k.fileDefaultText + "</span>"), m = a("<span>" + k.fileBtnText + "</span>"); if (!o.css("display") == "none" && k.autoHide) { r.hide() } r.addClass(k.fileClass); p.addClass(k.filenameClass); m.addClass(k.fileBtnClass); if (k.useID && o.attr("id") != "") { r.attr("id", k.idPrefix + "-" + o.attr("id")) } o.wrap(r); o.after(m); o.after(p); r = o.closest("div"); p = o.siblings("." + k.filenameClass); m = o.siblings("." + k.fileBtnClass); if (!o.attr("size")) { var l = r.width(); o.attr("size", l / 10) } var n = function () { var s = o.val(); if (s === "") { s = k.fileDefaultText } else { s = s.split(/[\/\\]+/); s = s[(s.length - 1)] } p.text(s) }; n(); o.css("opacity", 0).bind({ "focus.uniform": function () { r.addClass(k.focusClass) }, "blur.uniform": function () { r.removeClass(k.focusClass) }, "mousedown.uniform": function () { if (!a(q).is(":disabled")) { r.addClass(k.activeClass) } }, "mouseup.uniform": function () { r.removeClass(k.activeClass) }, "mouseenter.uniform": function () { r.addClass(k.hoverClass) }, "mouseleave.uniform": function () { r.removeClass(k.hoverClass); r.removeClass(k.activeClass) } }); if (a.browser.msie) { o.bind("click.uniform.ie7", function () { setTimeout(n, 0) }) } else { o.bind("change.uniform", n) } if (o.attr("disabled")) { r.addClass(k.disabledClass) } Polaris.uniform.noSelect(p); Polaris.uniform.noSelect(m); b(q) } Polaris.uniform.restore = function (l) { if (l == undefined) { l = a(a.uniform.elements) } a(l).each(function () { if (a(this).is(":checkbox")) { a(this).unwrap().unwrap() } else { if (a(this).is("select")) { a(this).siblings("span").remove(); a(this).unwrap() } else { if (a(this).is(":radio")) { a(this).unwrap().unwrap() } else { if (a(this).is(":file")) { a(this).siblings("span").remove(); a(this).unwrap() } else { if (a(this).is("button, :submit, :reset, a, input[type='button']")) { a(this).unwrap().unwrap() } } } } } a(this).unbind(".uniform"); a(this).css("opacity", "1"); var m = Polaris.inArray(a(l), Polaris.uniform.elements); Polaris.uniform.elements.splice(m, 1) }) }; function b(l) { l = a(l).get(); if (l.length > 1) { Polaris.each(l, function (m, n) { Polaris.uniform.elements.push(n) }) } else { Polaris.uniform.elements.push(l) } } Polaris.uniform.noSelect = function (l) { function m() { return false } a(l).each(function () { this.onselectstart = this.ondragstart = m; a(this).mousedown(m).css({ MozUserSelect: "none" }) }) }; Polaris.uniform.update = function (l) { if (l == undefined) { l = a(a.uniform.elements) } l = a(l); l.each(function () { var n = a(this); if (n.is("select")) { var m = n.siblings("span"); var p = n.parent("div"); p.removeClass(k.hoverClass + " " + k.focusClass + " " + k.activeClass); m.html(n.find(":selected").html()); if (n.is(":disabled")) { p.addClass(k.disabledClass) } else { p.removeClass(k.disabledClass) } } else { if (n.is(":checkbox")) { var m = n.closest("span"); var p = n.closest("div"); p.removeClass(k.hoverClass + " " + k.focusClass + " " + k.activeClass); m.removeClass(k.checkedClass); if (n.is(":checked")) { m.addClass(k.checkedClass) } if (n.is(":disabled")) { p.addClass(k.disabledClass) } else { p.removeClass(k.disabledClass) } } else { if (n.is(":radio")) { var m = n.closest("span"); var p = n.closest("div"); p.removeClass(k.hoverClass + " " + k.focusClass + " " + k.activeClass); m.removeClass(k.checkedClass); if (n.is(":checked")) { m.addClass(k.checkedClass) } if (n.is(":disabled")) { p.addClass(k.disabledClass) } else { p.removeClass(k.disabledClass) } } else { if (n.is(":file")) { var p = n.parent("div"); var o = n.siblings(k.filenameClass); btnTag = n.siblings(k.fileBtnClass); p.removeClass(k.hoverClass + " " + k.focusClass + " " + k.activeClass); o.text(n.val()); if (n.is(":disabled")) { p.addClass(k.disabledClass) } else { p.removeClass(k.disabledClass) } } else { if (n.is(":submit") || n.is(":reset") || n.is("button") || n.is("a") || l.is("input[type=button]")) { var p = n.closest("div"); p.removeClass(k.hoverClass + " " + k.focusClass + " " + k.activeClass); if (n.is(":disabled")) { p.addClass(k.disabledClass) } else { p.removeClass(k.disabledClass) } } } } } } }) }; return this.each(function () { if (a.support.selectOpacity) { var l = a(this); if (l.is("select")) { if (l.attr("multiple") != true) { if (l.attr("size") == undefined || l.attr("size") <= 1) { e(l) } } } else { if (l.is(":checkbox")) { f(l) } else { if (l.is(":radio")) { c(l) } else { if (l.is(":file")) { h(l) } else { if (l.is(":text, :password, input[type='email']")) { j(l) } else { if (l.is("textarea")) { g(l) } else { if (l.is("a") || l.is(":submit") || l.is(":reset") || l.is("button") || l.is("input[type=button]")) { i(l) } } } } } } } } }) } })(jQuery);
    </script>

    <script type="text/javascript">
        var j = jQuery.noConflict();

        $(function () {
            j("[id$=txtTIDIni]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Assistant_Remote/AssistantRemote.aspx/GetCustomers") %>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[0],
                                    val: item.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    $("[id$=hfCustomerId]").val(i.item.val);
                },
                minLength: 1
            });
        });
    </script>
    <style>
        .centrado {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }

        div.uploader span.filenameNew {
            height: 24px;
            /* change this line to adjust positioning of filename area */
            margin: 2px 0px 2px 2px;
            line-height: 24px;
            width: 292px !important;
        }

        div.uploader span.filenameNew {
            color: #777;
            width: 82px;
            border-right: solid 1px #bbb;
            font-size: 13px;
        }

        div.uploader.disabled span.filenameNew {
            border-color: #ddd;
            color: #aaa;
        }

        div.uploader span.filenameNew {
            padding: 0px 10px;
            float: left;
            display: block;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            cursor: default;
        }
    </style>



    <div class="toggle-message" style="z-index: 590; top: 0px; left: 0px;">
        <h3 class="title">
            <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
        <div class="st-form-line">
            <div class="st-form-line">
                <span class="st-labeltext">TID:</span>
                <asp:TextBox ID="txtTIDIni" CssClass="form-rounded" placeholder="Digite el TID.." Style="width: 250px"
                    runat="server" MaxLength="8"
                    ToolTip="Digite el TID." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
                <asp:HiddenField ID="hfCustomerId" runat="server" />
                <br />
                <asp:DropDownList ID="ddlTipoParametro" class="button-aqua dropdown-toggle" runat="server"
                    DataSourceID="dsParametro" DataTextField="DESCRIPCION"
                    DataValueField="ID" Width="200px"
                    ToolTip="Seleccione el Modelo: " TabIndex="0" AutoPostBack="false">
                </asp:DropDownList>
                <asp:SqlDataSource ID="dsParametro" runat="server"
                    ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" SelectCommand="SELECT ID,DESCRIPCION FROM ModeloTerminal where descripcion in ('NEW9220','N7210','NEW8210','T1000','A5-T1000','N8210') order by id"></asp:SqlDataSource>
                <asp:Button ID="BtnInicializacion" runat="server" Text="Filtrar"
                    CssClass="button-aqua" ToolTip="Filtrar..."  OnClientClick="return validateFilter()" />

                <asp:Button ID="btnNewTid" runat="server" Text="Adicionar configuración"
                    CssClass="button-aqua" ToolTip="Desea crear un nuevo terminal?" />
            </div>
        </div>
    </div>
    <div class="bg-1">
        <div class="container text-center">
            <h3>Configuración de funcionalidades</h3>
            <img src="https://image.flaticon.com/icons/png/512/438/438062.png" class="img-circle" alt="Bird" width="350" height="320">
        </div>
    </div>
    <br />
    <br />
    <div class="toggle-message" id="dragAndDropArea" style="z-index: 590;">
        <h6 class="title">Agregar promociones...
                <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h6>
        <div class="hide-message" id="hide-message" style="display: none;">

            <div class="st-form-line">
                <span class="st-labeltext">Archivo de promociones:</span>
                <div class="uploader" id="uniform-undefined">
                    <asp:FileUpload ID="fileApp" runat="server" CssClass="uniform" TabIndex="1" onchange="handleFiles(this.files)" />
                    <span class="filenameNew">No hay archivo seleccionado...</span><span class="action">Seleccione...</span>
                </div>
                <div class="clear"></div>
            </div>

            <div class="st-form-line">
                <div id="dragandrophandler">
                    <br />
                    <br />
                    Arrastre y Suelte el archivo aqu&iacute;...
                </div>
                <div class="clear"></div>
            </div>
            <div class="button-box">
                <asp:Button ID="btnAddApplication" runat="server" Text="Adicionar Aplicación"
                    CssClass="centrado button-aqua" TabIndex="4" OnClientClick="return validateAddApplication();" />
            </div>
        </div>
    </div>
    <div class="main-body">
        <div class="page-wrapper">
            <!-- Page body start -->
            <div class="row">
                <div class="">
                    <div class="card">
                        <div class="card-header">
                            <h5>Activar transacciones</h5>
                        </div>
                        <div class="card-block">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <div class="st-form-line">
                                            <div class="st-form-line">
                                                <span class="">Pest BCP?:
                                                </span>
                                                <br />
                                                <br />
                                                <asp:RadioButton ID="RadioBCPSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="BCP" Checked="false" Visible="false" />
                                                <br />
                                                <asp:RadioButton ID="RadioBCPNo" runat="server" Text=" No" AutoPostBack="false" GroupName="BCP" Checked="false" Visible="false" />
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="st-form-line">
                                            <div class="st-form-line">
                                                <span class="">Pest BNB?:
                                                </span>
                                                <br />
                                                <br />
                                                <asp:RadioButton ID="RadioBNBSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="BNB" Checked="false" Visible="false" />
                                                <br />
                                                <asp:RadioButton ID="RadioBNBNo" runat="server" Text=" No" AutoPostBack="false" GroupName="BNB" Checked="false" Visible="false" />
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="st-form-line">
                                            <div class="st-form-line">
                                                <span class="">Pest BISA?:
                                                </span>
                                                <br />
                                                <br />
                                                <asp:RadioButton ID="RadioBISASi" runat="server" Text=" Si" AutoPostBack="true" GroupName="BISA" Checked="false" Visible="false" />
                                                <br />
                                                <asp:RadioButton ID="RadioBISANo" runat="server" Text=" No" AutoPostBack="true" GroupName="BISA" Checked="false" Visible="false" />
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="st-form-line">
                                            <div class="st-form-line">
                                                <span class="">Anulación?:
                                                </span>
                                                <br />
                                                <br />
                                                <asp:RadioButton ID="RadioAnulaciónSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="Anulación" Checked="false" Visible="false" />
                                                <br />
                                                <asp:RadioButton ID="RadioAnulaciónNo" runat="server" Text=" No" AutoPostBack="false" GroupName="Anulación" Checked="false" Visible="false" />
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="st-form-line">
                                            <div class="st-form-line">
                                                <span class="">Deposito?:
                                                </span>
                                                <br />
                                                <br />
                                                <asp:RadioButton ID="RadioDepositoSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="Deposito" Checked="false" Visible="false" />
                                                <br />
                                                <asp:RadioButton ID="RadioDepositoNo" runat="server" Text=" No" AutoPostBack="false" GroupName="Deposito" Checked="false" Visible="false" />
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="st-form-line">
                                            <div class="st-form-line">
                                                <span class="">Ajuste Propina?:
                                                </span>
                                                <br />
                                                <br />
                                                <asp:RadioButton ID="RadioPropinaSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="Propina" Checked="false" Visible="false" />
                                                <br />
                                                <asp:RadioButton ID="RadioPropinaNo" runat="server" Text=" No" AutoPostBack="false" GroupName="Propina" Checked="false" Visible="false" />
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <div class="st-form-line">
                                            <span class="st-labeltext-inlinedos">NII Pago:</span>
                                            <asp:TextBox ID="TxtNiiPago" CssClass="form-rounded" placeholder="Nii pagos..." Style="width: 100px" Visible="false"
                                                onKeyPress="if(event.keyCode < 48 || event.keyCode > 57) return false;"
                                                runat="server" TabIndex="1" MaxLength="4"
                                                ToolTip="Solo permite numeros, longitud (4)." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                                            <div class="clear"></div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <div class="st-form-line">
                                            <div class="st-form-line">
                                                <span class="">Echo Test?:
                                                </span>
                                                <br />
                                                <br />
                                                <asp:RadioButton ID="RadioEchoSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="Echo" Checked="false" Visible="false" />
                                                <br />
                                                <asp:RadioButton ID="RadioEchoNo" runat="server" Text=" No" AutoPostBack="false" GroupName="Echo" Checked="false" Visible="false" />
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="st-form-line">
                                            <div class="st-form-line">
                                                <span class="">Cash Club?:
                                                </span>
                                                <br />
                                                <br />
                                                <asp:RadioButton ID="RadioCashSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="Cash" Checked="false" Visible="false" />
                                                <br />
                                                <asp:RadioButton ID="RadioCashNo" runat="server" Text=" No" AutoPostBack="false" GroupName="Cash" Checked="false" Visible="false" />
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="st-form-line">
                                            <div class="st-form-line">
                                                <span class="">Reservas?:
                                                </span>
                                                <br />
                                                <br />
                                                <asp:RadioButton ID="RadioReservasSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="Reservas" Checked="false" Visible="false" />
                                                <br />
                                                <asp:RadioButton ID="RadioReservasNo" runat="server" Text=" No" AutoPostBack="false" GroupName="Reservas" Checked="false" Visible="false" />
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="st-form-line">
                                            <div class="st-form-line">
                                                <span class="">Fidelización?:
                                                </span>
                                                <br />
                                                <br />
                                                <asp:RadioButton ID="RadioFidelizaciónSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="Fidelización" Checked="false" Visible="false" />
                                                <br />
                                                <asp:RadioButton ID="RadioFidelizaciónNo" runat="server" Text=" No" AutoPostBack="false" GroupName="Fidelización" Checked="false" Visible="false" />
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="st-form-line">
                                            <div class="st-form-line">
                                                <span class="">Soat?:
                                                </span>
                                                <br />
                                                <br />
                                                <asp:RadioButton ID="RadioSoatSi" runat="server" Text=" Si" AutoPostBack="True" GroupName="Soat" Checked="false" Visible="false" />
                                                <br />
                                                <asp:RadioButton ID="RadioSoatNo" runat="server" Text=" No" AutoPostBack="True" GroupName="Soat" Checked="false" Visible="false" />
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="st-form-line">
                                            <div class="st-form-line">
                                                <span class="">Promociones?:
                                                </span>
                                                <br />
                                                <br />
                                                <asp:RadioButton ID="RadioPromocionesSi" runat="server" Text=" Si" AutoPostBack="True" GroupName="Promociones2" Checked="false" Visible="false" />
                                                <br />
                                                <asp:RadioButton ID="RadioPromocionesNo" runat="server" Text=" No" AutoPostBack="True" GroupName="Promociones2" Checked="false" Visible="false" />
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <div class="st-form-line">
                                            <span class="st-labeltext-inlinedos">NII Soat:</span>
                                            <asp:TextBox ID="TextsoatNii" CssClass="form-rounded" placeholder="Nii soat..." Style="width: 100px" Visible="false"
                                                onKeyPress="if(event.keyCode < 48 || event.keyCode > 57) return false;"
                                                runat="server" TabIndex="2" MaxLength="4"
                                                ToolTip="Solo permite numeros, longitud (4)." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                                            <div class="clear"></div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="st-form-line">
                                            <span class="st-labeltext-inline">NII Promociones:</span>
                                            <asp:TextBox ID="TexPromocionesNii" CssClass="form-rounded" placeholder="Promociones..." Style="width: 100px" Visible="false"
                                                onKeyPress="if(event.keyCode < 48 || event.keyCode > 57) return false;"
                                                runat="server" TabIndex="3" MaxLength="4 "
                                                ToolTip="Solo permite numeros, longitud (4)." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                                            <div class="clear"></div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <div class="st-form-line">
                                            <div class="st-form-line">
                                                <span class="">CMB?:
                                                </span>
                                                <br />
                                                <br />
                                                <asp:RadioButton ID="RadioCMBSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="CMB" Checked="false" Visible="false" />
                                                <br />
                                                <asp:RadioButton ID="RadioCMBNo" runat="server" Text=" No" AutoPostBack="false" GroupName="CMB" Checked="false" Visible="false" />
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="st-form-line">
                                            <div class="st-form-line">
                                                <span class="">CTL?:
                                                </span>
                                                <br />
                                                <br />
                                                <asp:RadioButton ID="RadioCTLSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="CTL" Checked="false" Visible="false" />
                                                <br />
                                                <asp:RadioButton ID="RadioCTLNo" runat="server" Text=" No" AutoPostBack="false" GroupName="CTL" Checked="false" Visible="false" />
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="st-form-line">
                                            <div class="st-form-line">
                                                <span class="">Logo 1?:
                                                </span>
                                                <br />
                                                <br />
                                                <asp:RadioButton ID="RadioLogo1Si" runat="server" Text=" Si" AutoPostBack="true" GroupName="Logo1" Checked="false" Visible="false" />
                                                <br />
                                                <asp:RadioButton ID="RadioLogo1No" runat="server" Text=" No" AutoPostBack="true" GroupName="Logo1" Checked="false" Visible="false" />
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="st-form-line">
                                            <div class="st-form-line">
                                                <span class="">Logo 2?:
                                                </span>
                                                <br />
                                                <br />
                                                <asp:RadioButton ID="RadioLogo2Si" runat="server" Text=" Si" AutoPostBack="true" GroupName="Logo2" Checked="false" Visible="false" />
                                                <br />
                                                <asp:RadioButton ID="RadioLogo2No" runat="server" Text=" No" AutoPostBack="true" GroupName="Logo2" Checked="false" Visible="false" />
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="st-form-line">
                                            <div class="st-form-line">
                                                <span class="">Activar Trx Credito?:
                                                </span>
                                                <br />
                                                <br />
                                                <asp:RadioButton ID="RadiCreditotxSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="Credito" Checked="false" Visible="false" />
                                                <br />
                                                <asp:RadioButton ID="RadiCreditotxno" runat="server" Text=" No" AutoPostBack="false" GroupName="Credito" Checked="false" Visible="false" />
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="st-form-line">
                                            <span class="">Activar validación?:
                                            </span>
                                            <br />
                                            <br />
                                            <asp:RadioButton ID="RadioValidacionSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="Validación" Checked="false" Visible="false" />
                                            <br />
                                            <asp:RadioButton ID="RadioValidacionNo" runat="server" Text=" No" AutoPostBack="false" GroupName="Validación" Checked="false" Visible="false" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="st-form-line">
                                            <div class="st-form-line">
                                                <span class="st-labeltext-inline">Banca Joven:</span>
                                                <br />
                                                <br />
                                                <asp:RadioButton ID="RadioBancaJovenSi" runat="server" Text=" Si" AutoPostBack="true" GroupName="BancaJoven" Checked="false" Visible="false" />
                                                <br />
                                                <asp:RadioButton ID="RadioBancaJovenNo" runat="server" Text=" No" AutoPostBack="true" GroupName="BancaJoven" Checked="false" Visible="false" />

                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="st-form-line">
                                            <div class="st-form-line">

                                                <span class="">Activar efectivo
                                                </span>
                                                <br />
                                                <br />
                                                <asp:RadioButton ID="RadioEfectivoSi" runat="server" Text=" Si" AutoPostBack="true" GroupName="Efectivo" Checked="false" Visible="false" />
                                                <br />
                                                <asp:RadioButton ID="RadioEfectivoNo" runat="server" Text=" No" AutoPostBack="true" GroupName="Efectivo" Checked="false" Visible="false" />
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <div class="st-form-line">
                                            <span class="st-labeltext-inline">Porcentaje de descuento:</span>
                                            <asp:TextBox ID="TxtBancaJovenPorcentaje" CssClass="form-rounded" placeholder="Descuento %.." Style="width: 100px"
                                                runat="server" MaxLength="2" Visible="false"
                                                onKeyPress="if(event.keyCode < 48 || event.keyCode > 57) return false;"
                                                ToolTip="Banca joven." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                                            <div class="clear"></div>

                                        </div>
                                    </td>
                                    <td>
                                        <div class="st-form-line">
                                            <span class="st-labeltext-inline">Monto maximo:</span>
                                            <asp:TextBox ID="txtMontoMax" CssClass="form-rounded" placeholder="Monto.." Style="width: 180px"
                                                runat="server" MaxLength="8" Visible="false" onKeyPress="if(event.keyCode < 48 || event.keyCode > 57) return false;"
                                                ToolTip="Digite Un monto valido." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                                            <div class="clear"></div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="page-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Cajas</h5>
                                </div>
                                <div class="card-block">
                                </div>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <div class="st-form-line">
                                                <div class="st-form-line">
                                                    <span class="">Activar Cajas?:
                                                    </span>
                                                    <br />
                                                    <br />
                                                    <asp:RadioButton ID="RadioCajasSi" runat="server" Text=" Si" AutoPostBack="true" GroupName="Cajas" Checked="false" Visible="false" />
                                                    <br />
                                                    <asp:RadioButton ID="RadioCajasNo" runat="server" Text=" No" AutoPostBack="true" GroupName="Cajas" Checked="false" Visible="false" />
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="st-form-line">
                                                <div class="st-form-line">
                                                    <span class="">Opción:
                                                    </span>
                                                    <br />
                                                    <br />
                                                    <asp:RadioButton ID="RadioOpcionUsb" runat="server" Text=" USB" AutoPostBack="true" GroupName="Opcion" Checked="false" Visible="false" />
                                                    <br />
                                                    <asp:RadioButton ID="RadioOpcionTcp" runat="server" Text=" TCP" AutoPostBack="true" GroupName="Opcion" Checked="false" Visible="false" />
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="st-form-line">
                                                <div class="st-form-line">
                                                    <span class="">Activar serial?:
                                                    </span>
                                                    <br />
                                                    <br />
                                                    <asp:RadioButton ID="serialSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="Serial" Checked="false" Visible="false" />
                                                    <br />
                                                    <asp:RadioButton ID="serialNo" runat="server" Text=" No" AutoPostBack="false" GroupName="Serial" Checked="false" Visible="false" />
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="st-form-line">
                                                <span class="st-labeltext-inline">IP:</span>
                                                <asp:TextBox ID="TextiPCajas" CssClass="form-rounded" placeholder="Ip..." Style="width: 100px"
                                                    runat="server" MaxLength="15" Visible="false"
                                                    ToolTip="Digite Una ip valida." onkeydown="return(event.keyCode!=13);"></asp:TextBox>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="st-form-line">
                                                <span class="st-labeltext-inline">Puerto:</span>
                                                <asp:TextBox ID="txtPortCajas" CssClass="form-rounded" placeholder="Puerto..." Style="width: 100px"
                                                    runat="server" MaxLength="4" Visible="false"
                                                    ToolTip="Digite Un puerto valido." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                                                <div class="clear"></div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <div class="st-form-line">
                                                <div class="st-form-line">
                                                    <span class="">Envio BIN Cajas?
                                                    </span>
                                                    <br />
                                                    <br />
                                                    <asp:RadioButton ID="RadioBINSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="BIN" Checked="false" Visible="false" />
                                                    <br />
                                                    <asp:RadioButton ID="RadioBINo" runat="server" Text=" No" AutoPostBack="false" GroupName="BIN" Checked="false" Visible="false" />
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="st-form-line">
                                                <div class="st-form-line">
                                                    <span class="">Voucher Cajas?
                                                    </span>
                                                    <br />
                                                    <br />
                                                    <asp:RadioButton ID="RadioVaucherSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="Vaucher" Checked="false" Visible="false" />
                                                    <br />
                                                    <asp:RadioButton ID="RadioVaucherNo" runat="server" Text=" No" AutoPostBack="false" GroupName="Vaucher" Checked="false" Visible="false" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Configurar Cuotas</h5>
                                </div>
                                <div class="card-block">
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <div class="st-form-line">
                                                    <div class="st-form-line">
                                                        <span class="">Activar cuotas?:
                                                        </span>
                                                        <br />
                                                        <br />
                                                        <asp:RadioButton ID="RadioInteresesSi" runat="server" Text=" Si" AutoPostBack="true" GroupName="Intereses" Checked="false" Visible="false" />
                                                        <br />
                                                        <asp:RadioButton ID="RadioInteresesNo" runat="server" Text=" No" AutoPostBack="true" GroupName="Intereses" Checked="false" Visible="false" />
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="st-form-line">
                                                    <span class="">Editar cuotas:</span>
                                                    <asp:TextBox ID="TxtCuotas" class="form-rounded" placeholder="Cuotas.." Style="width: 100px"
                                                        runat="server" TabIndex="4" MaxLength="2" Visible="false"
                                                        onKeyPress="if(event.keyCode < 48 || event.keyCode > 57) return false;"
                                                        ToolTip="Digite solo numeros." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                                                    <div class="clear"></div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%--  --%>

            <div class="page-body">
                <div class="row">
                    <div class="col-md-6-1">
                        <div class="card">
                            <div class="card-header">
                                <h5></h5>
                            </div>
                            <div class="card-block">
                            </div>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <div class="st-form-line">
                                            <div class="st-form-line">
                                                <span class="">Habilitar Firma?:
                                                </span>
                                                <br />
                                                <br />
                                                <asp:RadioButton ID="RadioFirmaSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="Firma" Checked="false" Visible="false" />
                                                <br />
                                                <asp:RadioButton ID="RadioFirmaNo" runat="server" Text=" No" AutoPostBack="false" GroupName="Firma" Checked="false" Visible="false" />
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="st-form-line">
                                            <div class="st-form-line">
                                                <span class="">Habilitar Doc?:
                                                </span>
                                                <br />
                                                <br />
                                                <asp:RadioButton ID="RadioDocSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="Doc" Checked="false" Visible="false" />
                                                <br />
                                                <asp:RadioButton ID="RadioDocNo" runat="server" Text=" No" AutoPostBack="false" GroupName="Doc" Checked="false" Visible="false" />
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="st-form-line">
                                            <div class="st-form-line">
                                                <span class="">Habilitar Telefono?:
                                                </span>
                                                <br />
                                                <br />
                                                <asp:RadioButton ID="RadioTelefonoSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="Telefono" Checked="false" Visible="false" />
                                                <br />
                                                <asp:RadioButton ID="RadioTelefonoNo" runat="server" Text=" No" AutoPostBack="false" GroupName="Telefono" Checked="false" Visible="false" />
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="st-form-line">
                                            <div class="st-form-line">
                                                <span class="">Habilitar Mensaje Emisor?:
                                                </span>
                                                <br />
                                                <br />
                                                <asp:RadioButton ID="RadioEmisorSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="msmEmisor" Checked="false" Visible="false" />
                                                <br />
                                                <asp:RadioButton ID="RadioEmisorNo" runat="server" Text=" No" AutoPostBack="false" GroupName="msmEmisor" Checked="false" Visible="false" />
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <%--  --%>

            <div class="page-body">
                <div class="row">
                    <div class="col-md-6-1">
                        <div class="card">
                            <div class="card-header">
                                <h5>Polaris</h5>
                            </div>
                            <div class="card-block">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <div class="st-form-line">
                                                <span class="st-labeltext-inline">IP Polaris:</span>
                                                <asp:TextBox ID="IpPolaris" CssClass="form-rounded" placeholder="Ip..." Style="width: 180px"
                                                    runat="server" MaxLength="15" Visible="false"
                                                    ToolTip="Digite una ip valida." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="st-form-line">
                                                <span class="st-labeltext-inline">Puerto Polaris:</span>
                                                <asp:TextBox ID="PuertoPolaris" CssClass="form-rounded" placeholder="Puerto..." Style="width: 180px"
                                                    runat="server" MaxLength="4" Visible="false"
                                                    ToolTip="Digite Un puerto valido." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                                                <div class="clear"></div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="st-form-line">
                                                <span class="st-labeltext-inline">IP inicialización:</span>
                                                <asp:TextBox ID="IpIni" CssClass="form-rounded" placeholder="Ip inicialización..." Style="width: 180px"
                                                    runat="server" MaxLength="15" Visible="false"
                                                    ToolTip="Digite Una ip valida." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="st-form-line">
                                                <span class="st-labeltext-inline">Puerto inicialización:</span>
                                                <asp:TextBox ID="PortIni" CssClass="form-rounded" placeholder="Puerto inicialización..." Style="width: 180px"
                                                    runat="server" MaxLength="4" Visible="false"
                                                    ToolTip="Digite Un puerto valido." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                                                <div class="clear"></div>
                                            </div>
                                        </td>
                                        <td class="auto-style1">
                                            <div class="st-form-line">
                                                <span class="st-labeltext-inline">TID:</span>
                                                <asp:TextBox ID="TIDPOS" CssClass="form-rounded" placeholder="TID.." Style="width: 180px"
                                                    runat="server" MaxLength="8" Visible="false"
                                                    ToolTip="Digite Un TID valido." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                                                <div class="clear"></div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <ajaxtoolkit:ToolkitScriptManager ID="scrMgrAsp" runat="server">
    </ajaxtoolkit:ToolkitScriptManager>

    <asp:Panel ID="pnlPopupActivateOptions" runat="server" CssClass="modal-body " Height="600px" Width="1240px" BackColor="#E1E7E9" ScrollBars="Vertical">
        <asp:Label ID="lblTitulo" runat="server"
            Text="Crear terminal asistente remoto:" Font-Bold="True"
            Font-Size="Medium"></asp:Label>
        <br />
        <br />
        <br />
        <div class="main-body">
            <p>Seleccione el modelo:</p>
            <asp:DropDownList ID="ddlNewTconf" class="btn btn-primary dropdown-toggle" runat="server"
                DataSourceID="dsNewParametro" DataTextField="DESCRIPCION"
                DataValueField="ID" Width="200px"
                ToolTip="Seleccione el Modelo: " TabIndex="0" AutoPostBack="true">
            </asp:DropDownList>
            <asp:SqlDataSource ID="dsNewParametro" runat="server"
                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" SelectCommand="SELECT ID,DESCRIPCION FROM ModeloTerminal where descripcion in ('NEW9220','N7210','NEW8210','T1000','A5-T1000', 'N8210') order by id"></asp:SqlDataSource>
            <div class="page-wrapper">
                <!-- Page body start -->
                <div class="row">
                    <div class="">
                        <div class="card">
                            <div class="card-header">
                                <h5>Activar Transacciones</h5>
                            </div>
                            <div class="card-block">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <div class="st-form-line">
                                                <div class="st-form-line">
                                                    <span class="">Pest BCP?:
                                                    </span>
                                                    <br />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewBCPSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="BCP1" Checked="false" Visible="false" />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewBCPNo" runat="server" Text=" No" AutoPostBack="false" GroupName="BCP1" Checked="false" Visible="false" />
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="st-form-line">
                                                <div class="st-form-line">
                                                    <span class="">Pest BNB?:
                                                    </span>
                                                    <br />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewBNBSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="BNB1" Checked="false" Visible="false" />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewBNBNo" runat="server" Text=" No" AutoPostBack="false" GroupName="BNB1" Checked="false" Visible="false" />
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="st-form-line">
                                                <div class="st-form-line">
                                                    <span class="">Pest BISA?:
                                                    </span>
                                                    <br />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewBISASi" runat="server" Text=" Si" AutoPostBack="false" GroupName="BISA1" Checked="false" Visible="false" />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewBISANo" runat="server" Text=" No" AutoPostBack="false" GroupName="BISA1" Checked="false" Visible="false" />
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="st-form-line">
                                                <div class="st-form-line">
                                                    <span class="">Anulación?:
                                                    </span>
                                                    <br />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewAnulaciónSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="Anulación1" Checked="false" Visible="false" />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewAnulaciónNo" runat="server" Text=" No" AutoPostBack="false" GroupName="Anulación1" Checked="false" Visible="false" />
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="st-form-line">
                                                <div class="st-form-line">
                                                    <span class="">Deposito?:
                                                    </span>
                                                    <br />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewDepositoSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="Deposito1" Checked="false" Visible="false" />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewDepositoNo" runat="server" Text=" No" AutoPostBack="false" GroupName="Deposito1" Checked="false" Visible="false" />
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="st-form-line">
                                                <div class="st-form-line">
                                                    <span class="">Ajuste Propina?:
                                                    </span>
                                                    <br />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewPropinaSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="Propina1" Checked="false" Visible="false" />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewPropinaNo" runat="server" Text=" No" AutoPostBack="false" GroupName="Propina1" Checked="false" Visible="false" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <div class="st-form-line">
                                                <span class="st-labeltext-inline">NII Pago:</span>
                                                <asp:TextBox ID="TxtNewNiiPago" CssClass="form-rounded" placeholder="Nii pagos..." Style="width: 100px" Visible="false"
                                                    onKeyPress="if(event.keyCode < 48 || event.keyCode > 57) return false;"
                                                    runat="server" TabIndex="1" MaxLength="4"
                                                    ToolTip="Solo permite numeros, longitud (4)." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                                                <div class="clear"></div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <div class="st-form-line">
                                                <div class="st-form-line">
                                                    <span class="">Echo Test?:
                                                    </span>
                                                    <br />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewEchoSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="Echo1" Checked="false" Visible="false" />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewEchoNo" runat="server" Text=" No" AutoPostBack="false" GroupName="Echo1" Checked="false" Visible="false" />
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="st-form-line">
                                                <div class="st-form-line">
                                                    <span class="">Cash Club?:
                                                    </span>
                                                    <br />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewCashSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="Cash1" Checked="false" Visible="false" />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewCashNo" runat="server" Text=" No" AutoPostBack="false" GroupName="Cash1" Checked="false" Visible="false" />
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="st-form-line">
                                                <div class="st-form-line">
                                                    <span class="">Reservas?:
                                                    </span>
                                                    <br />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewReservasSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="Reservas1" Checked="false" Visible="false" />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewReservasNo" runat="server" Text=" No" AutoPostBack="false" GroupName="Reservas1" Checked="false" Visible="false" />
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="st-form-line">
                                                <div class="st-form-line">
                                                    <span class="">Fidelización?:
                                                    </span>
                                                    <br />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewFidelizaciónSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="Fidelización1" Checked="false" Visible="false" />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewFidelizaciónNo" runat="server" Text=" No" AutoPostBack="false" GroupName="Fidelización1" Checked="false" Visible="false" />
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="st-form-line">
                                                <div class="st-form-line">
                                                    <span class="">Soat?:
                                                    </span>
                                                    <br />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewSoatSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="Soat1" Checked="false" Visible="false" />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewSoatNo" runat="server" Text=" No" AutoPostBack="false" GroupName="Soat1" Checked="false" Visible="false" />
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="st-form-line">
                                                <div class="st-form-line">
                                                    <span class="">Promociones?:
                                                    </span>
                                                    <br />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewPromocionesSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="Promociones21" Checked="false" Visible="false" />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewPromocionesNo" runat="server" Text=" No" AutoPostBack="false" GroupName="Promociones21" Checked="false" Visible="false" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <div class="st-form-line">
                                                <span class="st-labeltext-inline">NII Soat:</span>
                                                <asp:TextBox ID="TextNewsoatNii" CssClass="form-rounded" placeholder="Nii soat..." Style="width: 100px" Visible="false"
                                                    onKeyPress="if(event.keyCode < 48 || event.keyCode > 57) return false;"
                                                    runat="server" TabIndex="2" MaxLength="4"
                                                    ToolTip="Solo permite numeros, longitud (4)." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                                                <div class="clear"></div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="st-form-line">
                                                <span class="st-labeltext-inline">NII Promociones:</span>
                                                <asp:TextBox ID="TexNewPromocionesNii" CssClass="form-rounded" placeholder="Promociones..." Style="width: 100px" Visible="false"
                                                    onKeyPress="if(event.keyCode < 48 || event.keyCode > 57) return false;"
                                                    runat="server" TabIndex="3" MaxLength="4 "
                                                    ToolTip="Solo permite numeros, longitud (4)." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                                                <div class="clear"></div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <div class="st-form-line">
                                                <div class="st-form-line">
                                                    <span class="">CMB?:
                                                    </span>
                                                    <br />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewCMBSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="CMB1" Checked="false" Visible="false" />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewCMBNo" runat="server" Text=" No" AutoPostBack="false" GroupName="CMB1" Checked="false" Visible="false" />
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="st-form-line">
                                                <div class="st-form-line">
                                                    <span class="">CTL?:
                                                    </span>
                                                    <br />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewCTLSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="CTL1" Checked="false" Visible="false" />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewCTLNo" runat="server" Text=" No" AutoPostBack="false" GroupName="CTL1" Checked="false" Visible="false" />
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="st-form-line">
                                                <div class="st-form-line">
                                                    <span class="">Logo 1?:
                                                    </span>
                                                    <br />
                                                    <br />
                                                    <asp:RadioButton ID="RadionewLogo1Si" runat="server" Text=" Si" AutoPostBack="false" GroupName="Logo11" Checked="false" Visible="false" />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewLogo1No" runat="server" Text=" No" AutoPostBack="false" GroupName="Logo11" Checked="false" Visible="false" />
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="st-form-line">
                                                <div class="st-form-line">
                                                    <span class="">Logo 2?:
                                                    </span>
                                                    <br />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewLogo2Si" runat="server" Text=" Si" AutoPostBack="false" GroupName="Logo21" Checked="false" Visible="false" />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewLogo2No" runat="server" Text=" No" AutoPostBack="false" GroupName="Logo21" Checked="false" Visible="false" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="st-form-line">
                                                <div class="st-form-line">
                                                    <span class="">Activar Trx Credito?:
                                                    </span>
                                                    <br />
                                                    <br />
                                                    <asp:RadioButton ID="RadiNewCreditotxSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="Credito1" Checked="false" Visible="false" />
                                                    <br />
                                                    <asp:RadioButton ID="RadiNewCreditotxno" runat="server" Text=" No" AutoPostBack="false" GroupName="Credito1" Checked="false" Visible="false" />
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="st-form-line">
                                                <span class="">Activar Validación?:
                                                </span>
                                                <br />
                                                <br />
                                                <asp:RadioButton ID="RadioNewValidacionSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="Validación1" Checked="false" Visible="false" />
                                                <br />
                                                <asp:RadioButton ID="RadioNewValidacionNo" runat="server" Text=" No" AutoPostBack="false" GroupName="Validación1" Checked="false" Visible="false" />
                                            </div>
                                        </td>
                                        <td>
                                            <div class="st-form-line">
                                                <div class="st-form-line">
                                                    <span class="st-labeltext-inline">Banca Joven:</span>
                                                    <br />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewBancaJovenSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="BancaJoven1" Checked="false" Visible="false" />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewBancaJovenNo" runat="server" Text=" No" AutoPostBack="false" GroupName="BancaJoven1" Checked="false" Visible="false" />

                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="st-form-line">
                                                <div class="st-form-line">

                                                    <span class="">Activar Efectivo
                                                    </span>
                                                    <br />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewEfectivoSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="Efectivo1" Checked="false" Visible="false" />
                                                    <br />
                                                    <asp:RadioButton ID="RadioNewEfectivoNo" runat="server" Text=" No" AutoPostBack="false" GroupName="Efectivo1" Checked="false" Visible="false" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <div class="st-form-line">
                                                <span class="st-labeltext-inline">Porcentaje de descuento:</span>
                                                <asp:TextBox ID="TxtNewBancaJovenPorcentaje" CssClass="form-rounded" placeholder="Descuento %.." Style="width: 100px"
                                                    runat="server" MaxLength="2" Visible="false"
                                                    onKeyPress="if(event.keyCode < 48 || event.keyCode > 57) return false;"
                                                    ToolTip="Banca joven." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                                                <div class="clear"></div>

                                            </div>
                                        </td>
                                        <td>
                                            <div class="st-form-line">
                                                <span class="st-labeltext-inline">Monto Maximo:</span>
                                                <asp:TextBox ID="txtNewMontoMax" CssClass="form-rounded" placeholder="Monto.." Style="width: 180px"
                                                    runat="server" MaxLength="8" Visible="false" onKeyPress="if(event.keyCode < 48 || event.keyCode > 57) return false;"
                                                    ToolTip="Digite Un monto valido." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                                                <div class="clear"></div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="page-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Configurar Cuotas</h5>
                                    </div>
                                    <div class="card-block">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <div class="st-form-line">
                                                        <div class="st-form-line">
                                                            <span class="">Activar cuotas?:
                                                            </span>
                                                            <br />
                                                            <br />
                                                            <asp:RadioButton ID="RadioNewInteresesSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="Intereses1" Checked="false" Visible="false" />
                                                            <br />
                                                            <asp:RadioButton ID="RadioNewInteresesNo" runat="server" Text=" No" AutoPostBack="false" GroupName="Intereses1" Checked="false" Visible="false" />
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="st-form-line">
                                                        <span class="">Editar cuotas:</span>
                                                        <asp:TextBox ID="TxtNewCuotas" class="form-rounded" placeholder="Cuotas.." Style="width: 100px"
                                                            runat="server" TabIndex="4" MaxLength="2" Visible="false"
                                                            onKeyPress="if(event.keyCode < 48 || event.keyCode > 57) return false;"
                                                            ToolTip="Digite solo numeros." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                                                        <div class="clear"></div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Cajas</h5>
                                    </div>
                                    <div class="card-block">
                                    </div>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <div class="st-form-line">
                                                    <div class="st-form-line">
                                                        <span class="">Activar Cajas?:
                                                        </span>
                                                        <br />
                                                        <br />
                                                        <asp:RadioButton ID="RadioNewCajasSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="Cajas1" Checked="false" Visible="false" />
                                                        <br />
                                                        <asp:RadioButton ID="RadioNewCajasNo" runat="server" Text=" No" AutoPostBack="false" GroupName="Cajas1" Checked="false" Visible="false" />
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="st-form-line">
                                                    <div class="st-form-line">
                                                        <span class="">Opción:
                                                        </span>
                                                        <br />
                                                        <br />
                                                        <asp:RadioButton ID="RadioNewOpcionUsb" runat="server" Text=" USB" AutoPostBack="true" GroupName="Opcion1" Checked="false" Visible="false" />
                                                        <br />
                                                        <asp:RadioButton ID="RadioNewOpcionTcp" runat="server" Text=" TCP" AutoPostBack="true" GroupName="Opcion1" Checked="false" Visible="false" />
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="st-form-line">
                                                    <div class="st-form-line">
                                                        <span class="">Activar serial?:
                                                        </span>
                                                        <br />
                                                        <br />
                                                        <asp:RadioButton ID="serialNewSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="Serial1" Checked="false" Visible="false" />
                                                        <br />
                                                        <asp:RadioButton ID="serialNewNo" runat="server" Text=" No" AutoPostBack="false" GroupName="Serial1" Checked="false" Visible="false" />
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="st-form-line">
                                                    <span class="st-labeltext-inline">IP:</span>
                                                    <asp:TextBox ID="TextNewiPCajas" CssClass="form-rounded" placeholder="Ip..." Style="width: 100px"
                                                        runat="server" MaxLength="15" Visible="false"
                                                        ToolTip="Digite Una ip valida." onkeydown="return(event.keyCode!=13);"></asp:TextBox>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="st-form-line">
                                                    <span class="st-labeltext-inline">Puerto:</span>
                                                    <asp:TextBox ID="txtNewPortCajas" CssClass="form-rounded" placeholder="Puerto..." Style="width: 100px"
                                                        runat="server" MaxLength="4" Visible="false"
                                                        ToolTip="Digite Un puerto valido." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                                                    <div class="clear"></div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <div class="st-form-line">
                                                    <div class="st-form-line">
                                                        <span class="">Envio BIN Cajas?
                                                        </span>
                                                        <br />
                                                        <br />
                                                        <asp:RadioButton ID="RadioNewBINSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="BIN1" Checked="false" Visible="false" />
                                                        <br />
                                                        <asp:RadioButton ID="RadioNewBINo" runat="server" Text=" No" AutoPostBack="false" GroupName="BIN1" Checked="false" Visible="false" />
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="st-form-line">
                                                    <div class="st-form-line">
                                                        <span class="">Voucher Cajas?
                                                        </span>
                                                        <br />
                                                        <br />
                                                        <asp:RadioButton ID="RadioNewVaucherSi" runat="server" Text=" Si" AutoPostBack="false" GroupName="Vaucher1" Checked="false" Visible="false" />
                                                        <br />
                                                        <asp:RadioButton ID="RadioNewVaucherNo" runat="server" Text=" No" AutoPostBack="false" GroupName="Vaucher1" Checked="false" Visible="false" />
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="page-body">
                    <div class="row">
                        <div class="col-md-6-1">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Polaris</h5>
                                </div>
                                <div class="card-block">
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <div class="st-form-line">
                                                    <span class="st-labeltext-inline">IP Polaris:</span>
                                                    <asp:TextBox ID="NewIpPolaris" CssClass="form-rounded" placeholder="Ip..." Style="width: 180px"
                                                        runat="server" MaxLength="15" Visible="false"
                                                        ToolTip="Digite una ip valida." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="st-form-line">
                                                    <span class="st-labeltext-inline">Puerto Polaris:</span>
                                                    <asp:TextBox ID="NewPuertoPolaris" CssClass="form-rounded" placeholder="Puerto..." Style="width: 180px"
                                                        runat="server" MaxLength="4" Visible="false"
                                                        ToolTip="Digite Un puerto valido." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                                                    <div class="clear"></div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="st-form-line">
                                                    <span class="st-labeltext-inline">IP inicialización:</span>
                                                    <asp:TextBox ID="NewIpIni" CssClass="form-rounded" placeholder="Ip inicialización..." Style="width: 180px"
                                                        runat="server" MaxLength="15" Visible="false"
                                                        ToolTip="Digite Una ip valida." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="st-form-line">
                                                    <span class="st-labeltext-inline">Puerto inicialización:</span>
                                                    <asp:TextBox ID="NewPortIni" CssClass="form-rounded" placeholder="Puerto inicialización..." Style="width: 180px"
                                                        runat="server" MaxLength="4" Visible="false"
                                                        ToolTip="Digite Un puerto valido." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                                                    <div class="clear"></div>
                                                </div>
                                            </td>
                                            <td class="auto-style1">
                                                <div class="st-form-line">
                                                    <span class="st-labeltext-inline">TID:</span>
                                                    <asp:TextBox ID="NewTIDPOS" CssClass="form-rounded" placeholder="TID.." Style="width: 180px"
                                                        runat="server" MaxLength="8" Visible="false"
                                                        ToolTip="Digite Un TID valido." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                                                    <div class="clear"></div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        <asp:LinkButton ID="lnkCancel" CssClass="button-red" runat="server" Font-Bold="True" OnClientClick="SetZIndexUp();"
            Font-Size="Small">Cancelar</asp:LinkButton>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<asp:Button ID="btnActivateOption" runat="server" Text="Crear" CssClass="button-aqua" />




    </asp:Panel>
    <asp:Button ID="btnPopUp" runat="server" Style="display: none" Text="Invisible Button" />


    <ajaxtoolkit:ModalPopupExtender ID="modalPopupExt" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="lnkCancel" TargetControlID="btnPopUp" PopupControlID="pnlPopupActivateOptions">
    </ajaxtoolkit:ModalPopupExtender>
    <br />
    <style>
        .bg-1 {
            background-color: #1abc9c;
            color: #ffffff;
        }

        .auto-style1 {
            width: 278px;
        }
    </style>


    <asp:Button ID="btnAsistente" runat="server" Text="Guardar cambio" alling="center"
        CssClass="btn-group-justified centrarguardar" ToolTip="Guardar..." OnClientClick="return validateAddOrEditParameters()" />
    <br />
    <br />
    <br />

    <script type="text/javascript">
        $('#btnAsistente').on('click', function (event) {
            setTimeout('document.location.reload()', 3000);
        });
    </script>

    <asp:HyperLink ID="lnkBack" runat="server"
        NavigateUrl="~/Assistant_Remote/Manager.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <script type="text/javascript">
        function validateFilter() {
            var flagField = true;
            var Modelo = document.getElementById('ctl00_MainContent_ddlTipoParametro').options[document.getElementById('ctl00_MainContent_ddlTipoParametro').selectedIndex].text;
            const ContentDiv = document.querySelector('#dragAndDropArea')




            if ($("#ctl00_MainContent_txtTIDIni").val().length <= 0) {
                $("#ctl00_MainContent_txtTIDIni").attr("class", "st-error-input");
                flagField = false;
            }
            if (!flagField) {
                $.msgAlert({
                    type: "error"
                    , title: "Mensaje del sistema"
                    , text: "Por favor complete los datos del formulario correctamente"
                    , callback: function () {
                        $.msgAlert.close();
                    }
                });
            }
            if (Modelo !== "NEW9220") {
                ContentDiv.innerHTML = "";
            }
            return flagField;
        }
    </script>
}
    <asp:Literal ID="Literal1" runat="server"></asp:Literal>
    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    <script src="../js/ValidatorParameters.js" type="text/javascript"></script>
    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>
    <script src="../js/ValidatorUploaderPromociones.js" type="text/javascript"></script>
</asp:Content>

