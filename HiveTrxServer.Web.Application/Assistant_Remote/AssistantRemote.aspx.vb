﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users
Imports System.Globalization
Imports TeleLoader.Groups
Imports TeleLoader.Terminals
Imports System.Web.Services
Imports TeleLoader.Applications

Partial Class Assistant_Remote_AssistantRemote
    Inherits TeleLoader.Web.BasePage

    Private totalTerminals As Integer
    Private totalUpdated As Integer
    Private totalPending As Integer
    Public tipo As String
    Public tipoParam As String
    Public TID As String
    Public EstadisticsObj As Group
    Dim fileDeployedObj As FileDeployedAndroid

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not IsPostBack Then
            btnAsistente.Enabled = False
        End If

        tipo = ddlTipoParametro.SelectedValue
        'Deshabilitamos el btn

        Dim logo As String
        Select Case tipo
            Case 1
                'T700
                logo = "var logo = '../img/T700.jpg'"
            Case 2
                'T800
                logo = "var logo ='../img/spectraT800.png'"
            Case 3
                'T1000
                logo = "var logo ='../img/Thumbnails_T1000-2-768x768.png'"
            Case 5
                ' A5-T1000
                logo = "var logo ='../img/Thumbnails_T1000-2-768x768.png'"
            Case 4
                'CREON
                logo = "var logo ='../img/spectraCREON.png'"
            Case 6
                'Android POS - i80 Sunyard
                logo = "var logo ='../img/i80.png'"
            Case 7
                'Android POS - i80 Sunyard
                logo = "var logo ='../img/Newpos-Miura-9220-Featured-Products-300x300.png'"
        End Select
        ltrScript.Text &= "<script language='javascript'>" & logo & "</script>"


    End Sub
    <WebMethod()>
    Public Shared Function GetCustomers(prefix As String) As String()
        Dim customers As New List(Of String)()
        Using conn As New SqlConnection()
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = "SELECT TOP 15 TID,TERMINAL_REC_NO FROM TERMINAL WHERE TID like @SearchText + '%' AND STATUS = 'Active'"
                cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        customers.Add(String.Format("{0}-{1}", sdr("TID"), sdr("TERMINAL_REC_NO")))
                    End While
                End Using
                conn.Close()
            End Using
        End Using
        Return customers.ToArray()
    End Function

    Protected Sub RadioEfectivoNo_CheckedChanged(sender As Object, e As EventArgs) Handles RadioEfectivoNo.CheckedChanged
        If RadioEfectivoNo.Checked = True Then

            txtMontoMax.Enabled = False
        End If
    End Sub
    Protected Sub RadioLogo2Si_CheckedChanged(sender As Object, e As EventArgs) Handles RadioLogo2Si.CheckedChanged
        If RadioLogo2Si.Checked = True Then
            RadioLogo1No.Checked = True
        End If
    End Sub
    Protected Sub RadioLogo2No_CheckedChanged(sender As Object, e As EventArgs) Handles RadioLogo2No.CheckedChanged
        If RadioLogo2No.Checked = True Then
            RadioLogo1Si.Checked = True
        End If
    End Sub
    Protected Sub RadioLogo1Si_CheckedChanged(sender As Object, e As EventArgs) Handles RadioLogo1Si.CheckedChanged
        If RadioLogo1Si.Checked = True Then
            RadioLogo2No.Checked = True
        End If
    End Sub
    Protected Sub RadioLogo1No_CheckedChanged(sender As Object, e As EventArgs) Handles RadioLogo1No.CheckedChanged
        If RadioLogo1No.Checked = True Then
            RadioLogo2Si.Checked = True
        End If
    End Sub
    Protected Sub RadioEfectivoSi_CheckedChanged(sender As Object, e As EventArgs) Handles RadioEfectivoSi.CheckedChanged
        If RadioEfectivoSi.Checked = True Then
            txtMontoMax.Enabled = True
        End If
    End Sub
    Protected Sub RadioSoatSi_CheckedChanged(sender As Object, e As EventArgs) Handles RadioSoatSi.CheckedChanged
        If RadioSoatSi.Checked = True Then
            TextsoatNii.Enabled = True
        End If
    End Sub
    Protected Sub RadioSoatNoCheckedChanged(sender As Object, e As EventArgs) Handles RadioSoatNo.CheckedChanged
        If RadioSoatSi.Checked = False Then
            TextsoatNii.Enabled = False
        End If
    End Sub

    Protected Sub RadioBancaJovenSi_CheckedChanged(sender As Object, e As EventArgs) Handles RadioBancaJovenSi.CheckedChanged
        If RadioBancaJovenSi.Checked = True Then
            TxtBancaJovenPorcentaje.Enabled = True
        End If
    End Sub

    Protected Sub RadioBancaJovenNo_CheckedChanged(sender As Object, e As EventArgs) Handles RadioBancaJovenNo.CheckedChanged
        If RadioBancaJovenNo.Checked = True Then
            TxtBancaJovenPorcentaje.Enabled = False
        End If
    End Sub
    '
    Protected Sub RadioFirmaSi_CheckedChanged(sender As Object, e As EventArgs) Handles RadioFirmaSi.CheckedChanged
        If RadioBancaJovenSi.Checked = True Then
            TxtBancaJovenPorcentaje.Enabled = True
        End If
    End Sub

    Protected Sub RadioFirmaNo_CheckedChanged(sender As Object, e As EventArgs) Handles RadioFirmaNo.CheckedChanged
        If RadioBancaJovenNo.Checked = True Then
            TxtBancaJovenPorcentaje.Enabled = False
        End If
    End Sub
    Protected Sub RadioDocSi_CheckedChanged(sender As Object, e As EventArgs) Handles RadioDocSi.CheckedChanged
        If RadioBancaJovenSi.Checked = True Then
            TxtBancaJovenPorcentaje.Enabled = True
        End If
    End Sub

    Protected Sub RadioDocNo_CheckedChanged(sender As Object, e As EventArgs) Handles RadioDocNo.CheckedChanged
        If RadioBancaJovenNo.Checked = True Then
            TxtBancaJovenPorcentaje.Enabled = False
        End If
    End Sub
    Protected Sub RadioTelefonoSi_CheckedChanged(sender As Object, e As EventArgs) Handles RadioTelefonoSi.CheckedChanged
        If RadioBancaJovenSi.Checked = True Then
            TxtBancaJovenPorcentaje.Enabled = True
        End If
    End Sub

    Protected Sub RadioTelefonoNo_CheckedChanged(sender As Object, e As EventArgs) Handles RadioTelefonoNo.CheckedChanged
        If RadioBancaJovenNo.Checked = True Then
            TxtBancaJovenPorcentaje.Enabled = False
        End If
    End Sub
    Protected Sub RadioEmisorSi_CheckedChanged(sender As Object, e As EventArgs) Handles RadioEmisorSi.CheckedChanged
        If RadioBancaJovenSi.Checked = True Then
            TxtBancaJovenPorcentaje.Enabled = True
        End If
    End Sub

    Protected Sub RadioEmisorNo_CheckedChanged(sender As Object, e As EventArgs) Handles RadioEmisorNo.CheckedChanged
        If RadioBancaJovenNo.Checked = True Then
            TxtBancaJovenPorcentaje.Enabled = False
        End If
    End Sub
    '
    Protected Sub RadioPromocionesSi_CheckedChanged(sender As Object, e As EventArgs) Handles RadioPromocionesSi.CheckedChanged
        If RadioPromocionesSi.Checked = True Then
            TexPromocionesNii.Enabled = True
        End If
    End Sub
    Protected Sub RadioPromocionesNo_CheckedChanged(sender As Object, e As EventArgs) Handles RadioPromocionesNo.CheckedChanged
        If RadioPromocionesNo.Checked = True Then
            TexPromocionesNii.Enabled = False
        End If
    End Sub
    Protected Sub RadioCajasNo_CheckedChanged(sender As Object, e As EventArgs) Handles RadioCajasNo.CheckedChanged
        If RadioCajasNo.Checked = True Then

            RadioOpcionUsb.Enabled = False
            RadioOpcionTcp.Enabled = False
            TextiPCajas.Enabled = False
            txtPortCajas.Enabled = False
            RadioBINSi.Enabled = False
            RadioBINo.Enabled = False
            RadioVaucherSi.Enabled = False
            RadioVaucherNo.Enabled = False

        End If
    End Sub
    Protected Sub RadioCajasSi_CheckedChanged(sender As Object, e As EventArgs) Handles RadioCajasSi.CheckedChanged
        If RadioCajasSi.Checked = True Then

            RadioOpcionUsb.Enabled = True
            RadioOpcionTcp.Enabled = True
            TextiPCajas.Enabled = True
            txtPortCajas.Enabled = True
            RadioBINSi.Enabled = True
            RadioBINo.Enabled = True
            RadioVaucherSi.Enabled = True
            RadioVaucherNo.Enabled = True

        End If
    End Sub
    Protected Sub RadioOpcionUsb_CheckedChanged(sender As Object, e As EventArgs) Handles RadioOpcionUsb.CheckedChanged
        If RadioOpcionUsb.Checked = True Then

            RadioOpcionTcp.Enabled = True
            TextiPCajas.Enabled = False
            txtPortCajas.Enabled = False
            RadioBINSi.Enabled = False
            RadioBINo.Enabled = False
            RadioVaucherSi.Enabled = False
            RadioVaucherNo.Enabled = False
        End If
    End Sub
    Protected Sub RadioOpcionTcp_CheckedChanged(sender As Object, e As EventArgs) Handles RadioOpcionTcp.CheckedChanged
        If RadioOpcionTcp.Checked = True Then

            RadioOpcionTcp.Enabled = True
            TextiPCajas.Enabled = True
            txtPortCajas.Enabled = True
            RadioBINSi.Enabled = True
            RadioBINo.Enabled = True
            RadioVaucherSi.Enabled = True
            RadioVaucherNo.Enabled = True
        End If
    End Sub

    Protected Sub RadioBISASi_CheckedChanged(sender As Object, e As EventArgs) Handles RadioBISASi.CheckedChanged
        If RadioBISASi.Checked = True Then
            TxtNiiPago.Enabled = True
        End If
    End Sub

    Protected Sub RadioBISANo_CheckedChanged(sender As Object, e As EventArgs) Handles RadioBISANo.CheckedChanged
        If RadioBISANo.Checked = True Then
            TxtNiiPago.Enabled = False
        End If
    End Sub
    Protected Sub BtnInicializacion_Click(sender As Object, e As EventArgs) Handles BtnInicializacion.Click
        TID = txtTIDIni.Text
        NewTIDPOS.Text = txtTIDIni.Text
        If DatosStis(TID, ddlTipoParametro.SelectedValue) = "No existe" Then
            btnNewTid.Enabled = True
            pnlMsg.Visible = False
            pnlError.Visible = True
            lblError.Text = "Maquina no encontrada por favor crearla."
            'Deshabilitamos el btn
            btnAsistente.Enabled = False
            'Seteamos los valores 
            RadioEfectivoSi.Enabled = False
            RadioEfectivoNo.Enabled = False
            TxtNiiPago.Enabled = False
            TextsoatNii.Enabled = False
            TexPromocionesNii.Enabled = False
            RadiCreditotxSi.Enabled = False
            RadiCreditotxno.Enabled = False
            RadioBancaJovenSi.Enabled = False
            RadioBancaJovenNo.Enabled = False
            TxtBancaJovenPorcentaje.Enabled = False
            RadioValidacionSi.Enabled = False
            RadioValidacionNo.Enabled = False
            RadioInteresesSi.Enabled = False
            RadioInteresesNo.Enabled = False
            TxtCuotas.Enabled = True
            RadioBCPSi.Enabled = False
            RadioBCPNo.Enabled = False
            RadioBNBSi.Enabled = False
            RadioBNBNo.Enabled = False
            RadioBISASi.Enabled = False
            RadioBISANo.Enabled = False
            RadioAnulaciónSi.Enabled = False
            RadioAnulaciónNo.Enabled = False
            RadioDepositoSi.Enabled = False
            RadioDepositoNo.Enabled = False
            RadioPropinaSi.Enabled = False
            RadioPropinaNo.Enabled = False
            RadioEchoSi.Enabled = False
            RadioEchoNo.Enabled = False
            RadioCashSi.Enabled = False
            RadioCashNo.Enabled = False
            RadioReservasSi.Enabled = False
            RadioReservasNo.Enabled = False
            RadioFidelizaciónSi.Enabled = False
            RadioFidelizaciónNo.Enabled = False
            RadioSoatSi.Enabled = False
            RadioSoatNo.Enabled = False
            RadioPromocionesSi.Enabled = False
            RadioPromocionesNo.Enabled = False
            RadioCMBSi.Enabled = False
            RadioCMBNo.Enabled = False
            RadioCTLSi.Enabled = False
            RadioCTLNo.Enabled = False
            RadioLogo1Si.Enabled = False
            RadioLogo1No.Enabled = False
            RadioLogo2Si.Enabled = False
            RadioLogo2No.Enabled = False
            RadioCajasSi.Enabled = False
            RadioCajasNo.Enabled = False
            RadioOpcionUsb.Enabled = False
            RadioOpcionTcp.Enabled = False
            TextiPCajas.Enabled = False
            txtPortCajas.Enabled = False
            RadioBINSi.Enabled = False
            RadioBINo.Enabled = False
            RadioVaucherSi.Enabled = False
            RadioVaucherNo.Enabled = False
            RadioPropinaSi.Enabled = False
            RadioPropinaNo.Enabled = False
            RadioSoatSi.Enabled = False
            RadioSoatNo.Enabled = False
            RadioPromocionesSi.Enabled = False
            RadioPromocionesNo.Enabled = False
            RadioCMBSi.Enabled = False
            RadioCMBNo.Enabled = False
            RadioLogo2Si.Enabled = False
            RadioLogo2No.Enabled = False
            serialSi.Enabled = False
            serialNo.Enabled = False
            serialSi.Enabled = False
            serialNo.Enabled = False
            IpPolaris.Enabled = False
            PuertoPolaris.Enabled = False
            txtMontoMax.Enabled = False
            'Adicionales
            TxtNiiPago.Enabled = False
            RadioPropinaSi.Enabled = False
            RadioPropinaNo.Enabled = False
            RadioSoatSi.Enabled = False
            RadioSoatNo.Enabled = False
            TextsoatNii.Enabled = False
            RadioPromocionesSi.Enabled = False
            RadioPromocionesNo.Enabled = False
            TexPromocionesNii.Enabled = False
            RadioFirmaSi.Enabled = False
            RadioFirmaNo.Enabled = False
            RadioDocSi.Enabled = False
            RadioDocNo.Enabled = False
            RadioTelefonoSi.Enabled = False
            RadioTelefonoNo.Enabled = False
            RadioEmisorSi.Enabled = False
            RadioEmisorNo.Enabled = False
            'Polaris
            IpPolaris.Enabled = False
            PuertoPolaris.Enabled = False
            IpIni.Enabled = False
            PortIni.Enabled = False
            TIDPOS.Enabled = False
        Else
            btnNewTid.Enabled = False
            'habilitamos el btn
            btnAsistente.Enabled = True
            'Seteamos los valores 
            RadioEfectivoSi.Enabled = True
            RadioEfectivoNo.Enabled = True
            TxtNiiPago.Enabled = True
            TextsoatNii.Enabled = True
            TexPromocionesNii.Enabled = True
            RadiCreditotxSi.Enabled = False
            RadiCreditotxno.Enabled = False
            RadioBancaJovenSi.Enabled = True
            RadioBancaJovenNo.Enabled = True
            TxtBancaJovenPorcentaje.Enabled = True
            RadioValidacionSi.Enabled = True
            RadioValidacionNo.Enabled = True
            RadioInteresesSi.Enabled = False
            RadioInteresesNo.Enabled = False
            TxtCuotas.Enabled = True
            RadioBCPSi.Enabled = True
            RadioBCPNo.Enabled = True
            RadioBNBSi.Enabled = True
            RadioBNBNo.Enabled = True
            RadioBISASi.Enabled = True
            RadioBISANo.Enabled = True
            RadioAnulaciónSi.Enabled = True
            RadioAnulaciónNo.Enabled = True
            RadioDepositoSi.Enabled = True
            RadioDepositoNo.Enabled = True
            RadioPropinaSi.Enabled = False
            RadioPropinaNo.Enabled = False
            RadioEchoSi.Enabled = True
            RadioEchoNo.Enabled = True
            RadioCashSi.Enabled = False
            RadioCashNo.Enabled = False
            RadioReservasSi.Enabled = True
            RadioReservasNo.Enabled = True
            RadioFidelizaciónSi.Enabled = False
            RadioFidelizaciónNo.Enabled = False
            RadioSoatSi.Enabled = True
            RadioSoatNo.Enabled = True
            RadioPromocionesSi.Enabled = True
            RadioPromocionesNo.Enabled = True
            RadioCMBSi.Enabled = True
            RadioCMBNo.Enabled = True
            RadioCTLSi.Enabled = True
            RadioCTLNo.Enabled = True
            RadioLogo1Si.Enabled = True
            RadioLogo1No.Enabled = True
            RadioLogo2Si.Enabled = True
            RadioLogo2No.Enabled = True
            RadioCajasSi.Enabled = True
            RadioCajasNo.Enabled = True
            RadioOpcionUsb.Enabled = True
            RadioOpcionTcp.Enabled = True
            TextiPCajas.Enabled = True
            txtPortCajas.Enabled = True
            RadioBINSi.Enabled = True
            RadioBINo.Enabled = True
            RadioVaucherSi.Enabled = True
            RadioVaucherNo.Enabled = True
            RadioPropinaSi.Enabled = False
            RadioPropinaNo.Enabled = False
            RadioSoatSi.Enabled = True
            RadioSoatNo.Enabled = True
            RadioPromocionesSi.Enabled = True
            RadioPromocionesNo.Enabled = True
            RadioCMBSi.Enabled = True
            RadioCMBNo.Enabled = True
            RadioLogo2Si.Enabled = True
            RadioLogo2No.Enabled = True
            serialSi.Enabled = True
            serialNo.Enabled = True
            serialSi.Enabled = True
            serialNo.Enabled = True
            IpPolaris.Enabled = True
            PuertoPolaris.Enabled = True
            txtMontoMax.Enabled = True
            'Adicionales
            TxtNiiPago.Enabled = True
            RadioPropinaSi.Enabled = False
            RadioPropinaNo.Enabled = False
            RadioSoatSi.Enabled = True
            RadioSoatNo.Enabled = True
            TextsoatNii.Enabled = True
            RadioPromocionesSi.Enabled = True
            RadioPromocionesNo.Enabled = True
            TexPromocionesNii.Enabled = True
            RadioFirmaSi.Enabled = True
            RadioFirmaNo.Enabled = True
            RadioDocSi.Enabled = True
            RadioDocNo.Enabled = True
            RadioTelefonoSi.Enabled = True
            RadioTelefonoNo.Enabled = True
            RadioEmisorSi.Enabled = True
            RadioEmisorNo.Enabled = True
            'Polaris
            IpPolaris.Enabled = True
            PuertoPolaris.Enabled = True
            IpIni.Enabled = True
            PortIni.Enabled = True
            TIDPOS.Enabled = True


            pnlMsg.Visible = True
            pnlError.Visible = False
            lblMsg.Text = "Datos encontrados con exito!!"

            Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
            Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString
            Dim result As String
            Using Conn As New SqlConnection(strConnString)
                Conn.Open()
                Dim ConsultaSQL = "SELECT descripcion FROM ModeloTerminal where id='" & ddlTipoParametro.SelectedValue & "'"
                result = New SqlCommand(ConsultaSQL, Conn).ExecuteScalar().ToString()
            End Using

            Dim modeloPos As String = result

            If modeloPos = "NEW9220" Then

                RadioEfectivoSi.Visible = True
                RadioEfectivoNo.Visible = True
                TxtNiiPago.Visible = True
                TextsoatNii.Visible = True
                TexPromocionesNii.Visible = True
                RadiCreditotxSi.Visible = True
                RadiCreditotxno.Visible = True
                RadiCreditotxSi.Enabled = False
                RadiCreditotxno.Enabled = False
                RadioBancaJovenSi.Visible = True
                RadioBancaJovenNo.Visible = True
                TxtBancaJovenPorcentaje.Visible = True
                RadioValidacionSi.Visible = True
                RadioValidacionNo.Visible = True
                RadioInteresesSi.Visible = True
                RadioInteresesNo.Visible = True
                RadioInteresesSi.Enabled = False
                RadioInteresesNo.Enabled = False
                TxtCuotas.Visible = True
                TxtCuotas.Enabled = True
                RadioBCPSi.Visible = True
                RadioBCPNo.Visible = True
                RadioBNBSi.Visible = True
                RadioBNBNo.Visible = True
                RadioBISASi.Visible = True
                RadioBISANo.Visible = True
                RadioAnulaciónSi.Visible = True
                RadioAnulaciónNo.Visible = True
                RadioDepositoSi.Visible = True
                RadioDepositoNo.Visible = True
                RadioPropinaSi.Visible = True
                RadioPropinaNo.Visible = True
                RadioEchoSi.Visible = True
                RadioEchoNo.Visible = True
                RadioCashSi.Visible = True
                RadioCashNo.Visible = True
                RadioReservasSi.Visible = True
                RadioReservasNo.Visible = True
                RadioFidelizaciónSi.Visible = True
                RadioFidelizaciónNo.Visible = True
                RadioFidelizaciónSi.Enabled = False
                RadioFidelizaciónNo.Enabled = False
                RadioSoatSi.Visible = True
                RadioSoatNo.Visible = True
                RadioPromocionesSi.Visible = True
                RadioPromocionesNo.Visible = True
                RadioCMBSi.Visible = True
                RadioCMBNo.Visible = True
                RadioCTLSi.Visible = True
                RadioCTLNo.Visible = True
                RadioLogo1Si.Visible = True
                RadioLogo1No.Visible = True
                RadioLogo2Si.Visible = True
                RadioLogo2No.Visible = True
                RadioCajasSi.Visible = True
                RadioCajasNo.Visible = True
                RadioOpcionUsb.Visible = True
                RadioOpcionTcp.Visible = True
                TextiPCajas.Visible = True
                txtPortCajas.Visible = True
                RadioBINSi.Visible = True
                RadioBINo.Visible = True
                RadioVaucherSi.Visible = True
                RadioVaucherNo.Visible = True
                RadioPropinaSi.Enabled = False
                RadioPropinaNo.Enabled = False
                RadioSoatSi.Enabled = True
                RadioSoatNo.Enabled = True
                RadioPromocionesSi.Enabled = True
                RadioPromocionesNo.Enabled = True
                RadioCMBSi.Enabled = True
                RadioCMBNo.Enabled = True
                RadioLogo2Si.Enabled = True
                RadioLogo2No.Enabled = True
                serialSi.Enabled = False
                serialNo.Enabled = False
                serialSi.Visible = True
                serialNo.Visible = True
                IpPolaris.Enabled = True
                IpPolaris.Visible = True
                PuertoPolaris.Enabled = True
                PuertoPolaris.Visible = True
                IpIni.Enabled = True
                IpIni.Visible = True
                PortIni.Enabled = True
                PortIni.Visible = True
                TIDPOS.Visible = True
                TIDPOS.Enabled = True
                txtMontoMax.Visible = True
                RadioFirmaSi.Visible = True
                RadioFirmaNo.Visible = True
                RadioDocSi.Visible = True
                RadioDocNo.Visible = True
                RadioTelefonoSi.Visible = True
                RadioTelefonoNo.Visible = True
                RadioEmisorSi.Visible = True
                RadioEmisorNo.Visible = True

            End If
            If modeloPos = ("T700") Or modeloPos = "T800" Or modeloPos = "T1000" Or modeloPos = "creon" Or modeloPos = "A5-T1000" Then

                RadioEfectivoSi.Visible = True
                RadioEfectivoNo.Visible = True
                TxtNiiPago.Visible = True

                TextsoatNii.Visible = True
                TexPromocionesNii.Visible = True
                'Enabled
                TextsoatNii.Enabled = False
                TexPromocionesNii.Enabled = False
                RadiCreditotxSi.Visible = True
                RadiCreditotxno.Visible = True
                RadiCreditotxSi.Enabled = False
                RadiCreditotxno.Enabled = False
                RadioBancaJovenSi.Visible = True
                RadioBancaJovenNo.Visible = True
                TxtBancaJovenPorcentaje.Visible = True
                RadioValidacionSi.Visible = True
                RadioValidacionNo.Visible = True
                RadioInteresesSi.Visible = True
                RadioInteresesNo.Visible = True
                TxtCuotas.Visible = True
                TxtCuotas.Enabled = True
                RadioBCPSi.Visible = True
                RadioBCPNo.Visible = True
                RadioBNBSi.Visible = True
                RadioBNBNo.Visible = True
                RadioBISASi.Visible = True
                RadioBISANo.Visible = True
                RadioAnulaciónSi.Visible = True
                RadioAnulaciónNo.Visible = True
                RadioDepositoSi.Visible = True
                RadioDepositoNo.Visible = True
                RadioDepositoSi.Enabled = True
                RadioDepositoNo.Enabled = True
                RadioPropinaSi.Visible = True
                RadioPropinaNo.Visible = True
                RadioEchoSi.Visible = True
                RadioEchoNo.Visible = True
                RadioCashSi.Visible = True
                RadioCashNo.Visible = True
                RadioReservasSi.Visible = True
                RadioReservasNo.Visible = True
                RadioFidelizaciónSi.Visible = True
                RadioFidelizaciónNo.Visible = True
                RadioFidelizaciónSi.Enabled = False
                RadioFidelizaciónNo.Enabled = False
                RadioSoatSi.Visible = True
                RadioSoatNo.Visible = True
                RadioPromocionesSi.Visible = True
                RadioPromocionesNo.Visible = True
                RadioCMBSi.Visible = True
                RadioCMBNo.Visible = True
                'Enabled

                RadioFidelizaciónSi.Visible = True
                RadioFidelizaciónNo.Visible = True
                RadioFidelizaciónSi.Enabled = False
                RadioFidelizaciónNo.Enabled = False
                RadioSoatSi.Enabled = False
                RadioSoatNo.Enabled = False
                RadioPromocionesSi.Enabled = False
                RadioPromocionesNo.Enabled = False
                RadioCMBSi.Enabled = False
                RadioCMBNo.Enabled = False
                RadioCTLSi.Visible = True
                RadioCTLNo.Visible = True
                RadioLogo1Si.Visible = True
                RadioLogo1No.Visible = True
                RadioLogo2Si.Visible = True
                RadioLogo2No.Visible = True
                RadioLogo2Si.Enabled = True
                RadioLogo2No.Enabled = True
                RadioCajasSi.Visible = True
                RadioCajasNo.Visible = True
                RadioOpcionUsb.Visible = True
                RadioOpcionTcp.Visible = True
                TextiPCajas.Visible = True
                txtPortCajas.Visible = True
                RadioBINSi.Visible = True
                RadioBINo.Visible = True
                RadioVaucherSi.Visible = True
                RadioVaucherNo.Visible = True
                serialSi.Enabled = True
                serialNo.Enabled = True
                serialSi.Visible = True
                serialNo.Visible = True
                IpPolaris.Enabled = True
                IpPolaris.Visible = True
                PuertoPolaris.Enabled = True
                PuertoPolaris.Visible = True
                IpIni.Enabled = True
                IpIni.Visible = True
                PortIni.Enabled = True
                PortIni.Visible = True
                TIDPOS.Visible = True
                TIDPOS.Enabled = True
                txtMontoMax.Visible = True
                RadioFirmaSi.Visible = True
                RadioFirmaNo.Visible = True
                RadioDocSi.Visible = True
                RadioDocNo.Visible = True
                RadioTelefonoSi.Visible = True
                RadioTelefonoNo.Visible = True
                RadioEmisorSi.Visible = True
                RadioEmisorNo.Visible = True

            End If

            If modeloPos = ("N8210") Or modeloPos = "N7210" Or modeloPos = "N8110" Or modeloPos = "NEW8210" Or modeloPos = "NEW7210" Then

                RadioEfectivoSi.Visible = True
                RadioEfectivoNo.Visible = True
                TxtNiiPago.Visible = True
                TextsoatNii.Visible = True
                TexPromocionesNii.Visible = True
                RadiCreditotxSi.Visible = True
                RadiCreditotxno.Visible = True
                RadiCreditotxSi.Enabled = False
                RadiCreditotxno.Enabled = False
                RadioBancaJovenSi.Visible = True
                RadioBancaJovenNo.Visible = True
                TxtBancaJovenPorcentaje.Visible = True
                RadioValidacionSi.Visible = True
                RadioValidacionNo.Visible = True
                RadioInteresesSi.Visible = True
                RadioInteresesNo.Visible = True
                TxtCuotas.Visible = True
                TxtCuotas.Enabled = True
                RadioBCPSi.Visible = True
                RadioBCPNo.Visible = True
                RadioBNBSi.Visible = True
                RadioBNBNo.Visible = True
                RadioBISASi.Visible = True
                RadioBISANo.Visible = True
                RadioAnulaciónSi.Visible = True
                RadioAnulaciónNo.Visible = True
                RadioDepositoSi.Visible = True
                RadioDepositoNo.Visible = True
                RadioPropinaSi.Visible = True
                RadioPropinaNo.Visible = True
                'enabled
                RadioPropinaSi.Enabled = False
                RadioPropinaNo.Enabled = False
                RadioEchoSi.Visible = True
                RadioEchoNo.Visible = True
                RadioCashSi.Visible = True
                RadioCashNo.Visible = True
                RadioReservasSi.Visible = True
                RadioReservasNo.Visible = True
                RadioFidelizaciónSi.Visible = True
                RadioFidelizaciónNo.Visible = True
                RadioFidelizaciónSi.Enabled = False
                RadioFidelizaciónNo.Enabled = False
                RadioSoatSi.Visible = True
                RadioSoatNo.Visible = True
                RadioPromocionesSi.Visible = True
                RadioPromocionesNo.Visible = True
                RadioCMBSi.Visible = True
                RadioCMBNo.Visible = True
                'enabled
                RadioFidelizaciónSi.Visible = True
                RadioFidelizaciónNo.Visible = True
                RadioFidelizaciónSi.Enabled = False
                RadioFidelizaciónNo.Enabled = False
                RadioSoatSi.Enabled = False
                RadioSoatNo.Enabled = False
                RadioPromocionesSi.Enabled = False
                RadioPromocionesNo.Enabled = False
                RadioCMBSi.Enabled = False
                RadioCMBNo.Enabled = False
                RadioCTLSi.Visible = True
                RadioCTLNo.Visible = True
                RadioLogo1Si.Visible = True
                RadioLogo1No.Visible = True
                RadioLogo2Si.Enabled = False
                RadioLogo2No.Enabled = False
                RadioLogo2Si.Visible = True
                RadioLogo2No.Visible = True
                RadioCajasSi.Visible = True
                RadioCajasNo.Visible = True
                RadioOpcionUsb.Visible = True
                RadioOpcionTcp.Visible = True
                TextiPCajas.Visible = True
                txtPortCajas.Visible = True
                RadioBINSi.Visible = True
                RadioBINo.Visible = True
                'enabled
                RadioBINSi.Enabled = False
                RadioBINo.Enabled = False
                RadioVaucherSi.Visible = True
                RadioVaucherNo.Visible = True
                serialSi.Enabled = False
                serialNo.Enabled = False
                serialSi.Visible = True
                serialNo.Visible = True
                IpPolaris.Enabled = True
                IpPolaris.Visible = True
                PuertoPolaris.Enabled = True
                PuertoPolaris.Visible = True
                IpIni.Enabled = True
                IpIni.Visible = True
                PortIni.Enabled = True
                PortIni.Visible = True
                TIDPOS.Visible = True
                TIDPOS.Enabled = True
                txtMontoMax.Visible = True
                RadioFirmaSi.Visible = True
                RadioFirmaNo.Visible = True
                RadioDocSi.Visible = True
                RadioDocNo.Visible = True
                RadioTelefonoSi.Visible = True
                RadioTelefonoNo.Visible = True
                RadioEmisorSi.Visible = True
                RadioEmisorNo.Visible = True
            End If
        End If
    End Sub

    Public Function DatosStisModal(ByVal Serial As String, ByVal Tipo As String) As String
        Dim connection As New SqlConnection(strConnectionString)
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim Activar_Efectivo As Boolean
        Dim retval As String
        Dim trx_credito As Boolean
        Dim banca_joven As Boolean
        Dim validacion As Boolean
        Dim intereses As Boolean
        Dim bcp As Boolean
        Dim bnb As Boolean
        Dim bisa As Boolean
        Dim anulacion As Boolean
        Dim Deposito As Boolean
        Dim propina As Boolean
        Dim echo As Boolean
        Dim cash_club As Boolean
        Dim reservas As Boolean
        Dim Fidelización As Boolean
        Dim soat As Boolean
        Dim promociones As Boolean
        Dim CMB As Boolean
        Dim ctl As Boolean
        Dim logo1 As Boolean
        Dim logo2 As Boolean
        Dim cajas As Boolean
        Dim opcionUsb As Boolean
        Dim opcionTcp As Boolean
        Dim Bin_Cajas As Boolean
        Dim Voucher_Cajas As Boolean
        Dim serialIni As Boolean
        Dim TID As String
        Dim firma As Boolean
        Dim doc As Boolean
        Dim telefono As Boolean
        Dim emisor As Boolean
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webConsutlar_datosIni"
            command.Parameters.Clear()
            command.Parameters.Add(New SqlParameter("Serial", Serial))
            command.Parameters.Add(New SqlParameter("Tipo", "NEW9220"))
            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    Activar_Efectivo = results.GetBoolean(0)
                    TxtNewNiiPago.Text = results.GetString(1)
                    TextNewsoatNii.Text = results.GetString(2)
                    TexNewPromocionesNii.Text = results.GetString(3)
                    trx_credito = results.GetBoolean(4)
                    banca_joven = results.GetBoolean(5)
                    validacion = results.GetBoolean(6)
                    intereses = results.GetBoolean(7)
                    TxtNewCuotas.Text = results.GetString(8)
                    bcp = results.GetBoolean(9)
                    bnb = results.GetBoolean(10)
                    bisa = results.GetBoolean(11)
                    anulacion = results.GetBoolean(12)
                    Deposito = results.GetBoolean(13)
                    propina = results.GetBoolean(14)
                    echo = results.GetBoolean(15)
                    cash_club = results.GetBoolean(16)
                    reservas = results.GetBoolean(17)
                    Fidelización = results.GetBoolean(18)
                    soat = results.GetBoolean(19)
                    promociones = results.GetBoolean(20)
                    CMB = results.GetBoolean(21)
                    ctl = results.GetBoolean(22)
                    logo1 = results.GetBoolean(23)
                    logo2 = results.GetBoolean(24)
                    cajas = results.GetBoolean(25)
                    opcionUsb = results.GetBoolean(26)
                    opcionTcp = results.GetBoolean(27)
                    TextNewiPCajas.Text = results.GetString(28)
                    txtNewPortCajas.Text = results.GetString(29)
                    Bin_Cajas = results.GetBoolean(30)
                    Voucher_Cajas = results.GetBoolean(31)
                    TID = results.GetString(32)
                    txtNewMontoMax.Text = results.GetString(33)
                    serialIni = results.GetBoolean(34)
                    NewIpPolaris.Text = results.GetString(35)
                    NewPuertoPolaris.Text = results.GetString(36)
                    NewIpIni.Text = results.GetString(37)
                    NewPortIni.Text = results.GetString(38)
                    NewTIDPOS.Text = txtTIDIni.Text
                    TxtNewBancaJovenPorcentaje.Text = results.GetString(40)
                    firma = results.GetBoolean(41)
                    doc = results.GetBoolean(42)
                    telefono = results.GetBoolean(43)
                    emisor = results.GetBoolean(44)

                    If firma = True Then
                        RadioFirmaSi.Checked = True
                    Else
                        RadioFirmaNo.Checked = True
                    End If
                    If doc = True Then
                        RadioDocSi.Checked = True
                    Else
                        RadioDocNo.Checked = True
                    End If
                    If telefono = True Then
                        RadioTelefonoSi.Checked = True
                    Else
                        RadioTelefonoNo.Checked = True
                    End If
                    If emisor = True Then
                        RadioEmisorSi.Checked = True
                    Else
                        RadioEmisorNo.Checked = True
                    End If

                    If Activar_Efectivo = True Then
                        RadioNewEfectivoSi.Checked = True
                        txtNewMontoMax.Enabled = True
                    Else
                        RadioNewEfectivoNo.Checked = True
                        txtNewMontoMax.Enabled = False
                    End If
                    If trx_credito = True Then
                        RadiNewCreditotxSi.Checked = True
                    Else
                        RadiNewCreditotxno.Checked = True
                    End If
                    If banca_joven = True Then
                        RadioNewBancaJovenSi.Checked = True
                    Else
                        RadioNewBancaJovenNo.Checked = True
                        TxtNewBancaJovenPorcentaje.Enabled = False
                    End If
                    If validacion = True Then
                        RadioNewValidacionSi.Checked = True
                    Else
                        RadioNewValidacionNo.Checked = True
                    End If
                    If intereses = True Then
                        RadioNewInteresesSi.Checked = True
                        TxtNewCuotas.Enabled = True
                    Else
                        RadioNewInteresesNo.Checked = True
                        TxtNewCuotas.Enabled = True
                    End If
                    If promociones = True Then
                        RadioNewPromocionesSi.Checked = True
                        TexNewPromocionesNii.Enabled = True

                    Else
                        RadioNewPromocionesNo.Checked = True
                        TexNewPromocionesNii.Enabled = True
                    End If
                    If soat = True Then
                        RadioNewSoatSi.Checked = True
                        TextNewsoatNii.Enabled = True
                    Else
                        RadioNewSoatNo.Checked = True
                        TextNewsoatNii.Enabled = True
                    End If
                    If soat = True Then
                        RadioNewSoatSi.Checked = True
                        TextNewsoatNii.Enabled = True
                    Else
                        RadioNewSoatNo.Checked = True
                        TextNewsoatNii.Enabled = True
                    End If
                    If Fidelización = True Then
                        RadioNewFidelizaciónSi.Checked = True
                    Else
                        RadioNewFidelizaciónNo.Checked = True
                    End If
                    If reservas = True Then
                        RadioNewReservasSi.Checked = True
                    Else
                        RadioNewReservasNo.Checked = True
                    End If
                    If cash_club = True Then
                        RadioNewCashSi.Checked = True
                    Else
                        RadioNewCashNo.Checked = True
                    End If
                    If echo = True Then
                        RadioNewEchoSi.Checked = True
                    Else
                        RadioNewEchoNo.Checked = True
                    End If
                    If propina = True Then
                        RadioNewPropinaSi.Checked = True
                    Else
                        RadioNewPropinaNo.Checked = True
                    End If
                    If Deposito = True Then
                        RadioNewDepositoSi.Checked = True
                    Else
                        RadioNewDepositoNo.Checked = True
                    End If
                    If anulacion = True Then
                        RadioNewAnulaciónSi.Checked = True
                    Else
                        RadioNewAnulaciónNo.Checked = True
                    End If

                    If bisa = True Then
                        RadioNewBISASi.Checked = True
                        TxtNewNiiPago.Enabled = True
                    Else
                        RadioNewBISANo.Checked = True
                        TxtNewNiiPago.Enabled = False
                    End If
                    If bnb = True Then
                        RadioNewBNBSi.Checked = True
                    Else
                        RadioNewBNBNo.Checked = True

                    End If
                    If bcp = True Then
                        RadioNewBCPSi.Checked = True
                    Else
                        RadioNewBCPNo.Checked = True

                    End If
                    If CMB = True Then
                        RadioNewCMBSi.Checked = True
                    Else
                        RadioNewCMBNo.Checked = True

                    End If

                    If ctl = True Then
                        RadioNewCTLSi.Checked = True
                    Else
                        RadioNewCTLNo.Checked = True
                    End If
                    If logo1 = True Then
                        RadionewLogo1Si.Checked = True

                    Else
                        RadioNewLogo1No.Checked = True

                    End If
                    If logo2 = True Then
                        RadioNewLogo2Si.Checked = True
                    Else
                        RadioNewLogo2No.Checked = True

                    End If

                    If cajas = True Then
                        RadioNewCajasSi.Checked = True
                        RadioNewOpcionUsb.Enabled = True
                        RadioNewOpcionTcp.Enabled = True
                        TextNewiPCajas.Enabled = True
                        txtNewPortCajas.Enabled = True
                        RadioNewBINSi.Enabled = True
                        RadioNewBINo.Enabled = True
                        RadioNewVaucherSi.Enabled = True
                        RadioNewVaucherNo.Enabled = True
                    Else
                        RadioNewCajasNo.Checked = True
                        RadioNewOpcionUsb.Enabled = False
                        RadioNewOpcionTcp.Enabled = False
                        TextNewiPCajas.Enabled = False
                        txtNewPortCajas.Enabled = False
                        RadioNewBINSi.Enabled = False
                        RadioNewBINo.Enabled = False
                        RadioNewVaucherSi.Enabled = False
                        RadioNewVaucherNo.Enabled = False

                    End If

                    If opcionTcp = True Then
                        RadioNewOpcionTcp.Checked = True

                    Else
                        RadioNewOpcionUsb.Checked = True
                        TextNewiPCajas.Enabled = False
                        txtNewPortCajas.Enabled = False
                        RadioNewBINSi.Enabled = False
                        RadioNewBINo.Enabled = False
                        RadioNewVaucherSi.Enabled = False
                        RadioNewVaucherNo.Enabled = False

                    End If
                    If opcionUsb = True Then
                        RadioNewOpcionUsb.Checked = True
                    Else
                        RadioNewOpcionTcp.Checked = True

                    End If
                    If Bin_Cajas = True Then
                        RadioNewBINSi.Checked = True
                    Else
                        RadioNewBINo.Checked = True

                    End If
                    If Voucher_Cajas = True Then
                        RadioNewVaucherSi.Checked = True
                    Else
                        RadioNewVaucherNo.Checked = True

                    End If
                    If serialIni = True Then
                        serialNewSi.Checked = True
                    Else
                        serialNewNo.Checked = True

                    End If
                End While
            Else
                retval = False
            End If

            If status = 1 Then
                retval = True
            Else
                retval = False
            End If

        Catch ex As Exception
            retval = "No existe"
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try

        Return retval

    End Function
    Public Function DatosStis(ByVal Serial As String, ByVal Tipo As String) As String
        Dim connection As New SqlConnection(strConnectionString)
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim Activar_Efectivo As Boolean
        Dim retval As String
        Dim trx_credito As Boolean
        Dim banca_joven As Boolean
        Dim validacion As Boolean
        Dim intereses As Boolean
        Dim bcp As Boolean
        Dim bnb As Boolean
        Dim bisa As Boolean
        Dim anulacion As Boolean
        Dim Deposito As Boolean
        Dim propina As Boolean
        Dim echo As Boolean
        Dim cash_club As Boolean
        Dim reservas As Boolean
        Dim Fidelización As Boolean
        Dim soat As Boolean
        Dim promociones As Boolean
        Dim CMB As Boolean
        Dim ctl As Boolean
        Dim logo1 As Boolean
        Dim logo2 As Boolean
        Dim cajas As Boolean
        Dim opcionUsb As Boolean
        Dim opcionTcp As Boolean
        Dim Bin_Cajas As Boolean
        Dim Voucher_Cajas As Boolean
        Dim serialIni As Boolean
        Dim TID As String
        Dim firma As Boolean
        Dim doc As Boolean
        Dim telefono As Boolean
        Dim emisor As Boolean

        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webConsutlar_datosIni"
            command.Parameters.Clear()
            command.Parameters.Add(New SqlParameter("Serial", Serial))
            command.Parameters.Add(New SqlParameter("Tipo", "NEW9220"))
            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    Activar_Efectivo = results.GetBoolean(0)
                    TxtNiiPago.Text = results.GetString(1)
                    TextsoatNii.Text = results.GetString(2)
                    TexPromocionesNii.Text = results.GetString(3)
                    trx_credito = results.GetBoolean(4)
                    banca_joven = results.GetBoolean(5)
                    validacion = results.GetBoolean(6)
                    intereses = results.GetBoolean(7)
                    TxtCuotas.Text = results.GetString(8)
                    bcp = results.GetBoolean(9)
                    bnb = results.GetBoolean(10)
                    bisa = results.GetBoolean(11)
                    anulacion = results.GetBoolean(12)
                    Deposito = results.GetBoolean(13)
                    propina = results.GetBoolean(14)
                    echo = results.GetBoolean(15)
                    cash_club = results.GetBoolean(16)
                    reservas = results.GetBoolean(17)
                    Fidelización = results.GetBoolean(18)
                    soat = results.GetBoolean(19)
                    promociones = results.GetBoolean(20)
                    CMB = results.GetBoolean(21)
                    ctl = results.GetBoolean(22)
                    logo1 = results.GetBoolean(23)
                    logo2 = results.GetBoolean(24)
                    cajas = results.GetBoolean(25)
                    opcionUsb = results.GetBoolean(26)
                    opcionTcp = results.GetBoolean(27)
                    TextiPCajas.Text = results.GetString(28)
                    txtPortCajas.Text = results.GetString(29)
                    Bin_Cajas = results.GetBoolean(30)
                    Voucher_Cajas = results.GetBoolean(31)
                    TID = results.GetString(32)
                    txtMontoMax.Text = results.GetString(33)
                    serialIni = results.GetBoolean(34)
                    IpPolaris.Text = results.GetString(35)
                    PuertoPolaris.Text = results.GetString(36)
                    IpIni.Text = results.GetString(37)
                    PortIni.Text = results.GetString(38)
                    TIDPOS.Text = results.GetString(39)
                    TxtBancaJovenPorcentaje.Text = results.GetString(40)
                    firma = results.GetBoolean(41)
                    doc = results.GetBoolean(42)
                    telefono = results.GetBoolean(43)
                    emisor = results.GetBoolean(44)

                    If firma = True Then
                        RadioFirmaSi.Checked = True
                    Else
                        RadioFirmaNo.Checked = True
                    End If
                    If doc = True Then
                        RadioDocSi.Checked = True
                    Else
                        RadioDocNo.Checked = True
                    End If
                    If telefono = True Then
                        RadioTelefonoSi.Checked = True
                    Else
                        RadioTelefonoNo.Checked = True
                    End If
                    If emisor = True Then
                        RadioEmisorSi.Checked = True
                    Else
                        RadioEmisorNo.Checked = True
                    End If

                    If Activar_Efectivo = True Then
                        RadioEfectivoSi.Checked = True
                        txtMontoMax.Enabled = True
                    Else
                        RadioEfectivoNo.Checked = True
                        txtMontoMax.Enabled = False
                    End If
                    If trx_credito = True Then
                        RadiCreditotxSi.Checked = True
                    Else
                        RadiCreditotxno.Checked = True
                    End If
                    If banca_joven = True Then
                        RadioBancaJovenSi.Checked = True
                    Else
                        RadioBancaJovenNo.Checked = True
                        TxtBancaJovenPorcentaje.Enabled = False
                    End If
                    If validacion = True Then
                        RadioValidacionSi.Checked = True
                    Else
                        RadioValidacionNo.Checked = True
                    End If
                    If intereses = True Then
                        RadioInteresesSi.Checked = False
                        'TxtCuotas.Enabled = True
                    Else
                        RadioInteresesNo.Checked = True
                        'TxtCuotas.Enabled = False
                    End If
                    If promociones = True Then
                        RadioPromocionesSi.Checked = True
                        TexPromocionesNii.Enabled = True

                    Else
                        RadioPromocionesNo.Checked = True
                        TexPromocionesNii.Enabled = False
                    End If
                    If soat = True Then
                        RadioSoatSi.Checked = True
                        TextsoatNii.Enabled = True
                    Else
                        RadioSoatNo.Checked = True
                        TextsoatNii.Enabled = False
                    End If
                    If Fidelización = True Then
                        RadioFidelizaciónSi.Checked = True
                    Else
                        RadioFidelizaciónNo.Checked = True
                    End If
                    If reservas = True Then
                        RadioReservasSi.Checked = True
                    Else
                        RadioReservasNo.Checked = True
                    End If
                    If cash_club = True Then
                        RadioCashSi.Checked = True
                    Else
                        RadioCashNo.Checked = True
                    End If
                    If echo = True Then
                        RadioEchoSi.Checked = True
                    Else
                        RadioEchoNo.Checked = True
                    End If
                    If propina = True Then
                        RadioPropinaSi.Checked = True
                    Else
                        RadioPropinaNo.Checked = True
                    End If
                    If Deposito = True Then
                        RadioDepositoSi.Checked = True
                    Else
                        RadioDepositoNo.Checked = True
                    End If
                    If anulacion = True Then
                        RadioAnulaciónSi.Checked = True
                    Else
                        RadioAnulaciónNo.Checked = True
                    End If

                    If bisa = True Then
                        RadioBISASi.Checked = True
                        TxtNiiPago.Enabled = True
                    Else
                        RadioBISANo.Checked = True
                        TxtNiiPago.Enabled = False
                    End If
                    If bnb = True Then
                        RadioBNBSi.Checked = True
                    Else
                        RadioBNBNo.Checked = True

                    End If
                    If bcp = True Then
                        RadioBCPSi.Checked = True
                    Else
                        RadioBCPNo.Checked = True

                    End If
                    If CMB = True Then
                        RadioCMBSi.Checked = True
                    Else
                        RadioCMBNo.Checked = True

                    End If

                    If ctl = True Then
                        RadioCTLSi.Checked = True
                    Else
                        RadioCTLNo.Checked = True
                    End If
                    If logo1 = True Then
                        RadioLogo1Si.Checked = True

                    Else
                        RadioLogo1No.Checked = True

                    End If
                    If logo2 = True Then
                        RadioLogo2Si.Checked = True
                    Else
                        RadioLogo2No.Checked = True

                    End If

                    If cajas = True Then
                        RadioCajasSi.Checked = True
                        RadioOpcionUsb.Enabled = True
                        RadioOpcionTcp.Enabled = True
                        TextiPCajas.Enabled = True
                        txtPortCajas.Enabled = True
                        RadioBINSi.Enabled = True
                        RadioBINo.Enabled = True
                        RadioVaucherSi.Enabled = True
                        RadioVaucherNo.Enabled = True
                    Else
                        RadioCajasNo.Checked = True
                        RadioOpcionUsb.Enabled = False
                        RadioOpcionTcp.Enabled = False
                        TextiPCajas.Enabled = False
                        txtPortCajas.Enabled = False
                        RadioBINSi.Enabled = False
                        RadioBINo.Enabled = False
                        RadioVaucherSi.Enabled = False
                        RadioVaucherNo.Enabled = False

                    End If

                    If opcionTcp = True Then
                        RadioOpcionTcp.Checked = True

                    Else
                        RadioOpcionUsb.Checked = True
                        TextiPCajas.Enabled = False
                        txtPortCajas.Enabled = False
                        RadioBINSi.Enabled = False
                        RadioBINo.Enabled = False
                        RadioVaucherSi.Enabled = False
                        RadioVaucherNo.Enabled = False

                    End If
                    If opcionUsb = True Then
                        RadioOpcionUsb.Checked = True
                    Else
                        RadioOpcionTcp.Checked = True

                    End If
                    If Bin_Cajas = True Then
                        RadioBINSi.Checked = True
                    Else
                        RadioBINo.Checked = True

                    End If
                    If Voucher_Cajas = True Then
                        RadioVaucherSi.Checked = True
                    Else
                        RadioVaucherNo.Checked = True

                    End If
                    If serialIni = True Then
                        serialSi.Checked = True
                    Else
                        serialNo.Checked = True

                    End If
                End While
            Else
                retval = False
            End If

            If status = 1 Then
                retval = True
            Else
                retval = False
            End If

        Catch ex As Exception
            retval = "No existe"
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try

        Return retval

    End Function
    Protected Sub ddlTipoParametro_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTipoParametro.SelectedIndexChanged
        tipoParam = ddlTipoParametro.SelectedValue
    End Sub
    Protected Sub btnAsistente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAsistente.Click

        EstadisticsObj = New Group(strConnectionString, objSessionParams.intSelectedGroup)
        EstadisticsObj.EfectivoSi = RadioEfectivoSi.Checked
        EstadisticsObj.EfectivoNo = RadioEfectivoNo.Checked
        EstadisticsObj.NII_Pago = TxtNiiPago.Text
        EstadisticsObj.NII_Soat = TextsoatNii.Text
        EstadisticsObj.NII_Promociones = TexPromocionesNii.Text
        EstadisticsObj.Activar_creditoSi = RadiCreditotxSi.Checked
        EstadisticsObj.Activar_creditoNo = RadiCreditotxno.Checked
        EstadisticsObj.Activar_Banca_JovenSi = RadioBancaJovenSi.Checked
        EstadisticsObj.Activar_Banca_JovenNo = RadioBancaJovenNo.Checked
        EstadisticsObj.Activar_Banca_Joven_Porcentaje = TxtBancaJovenPorcentaje.Text
        'validacion
        EstadisticsObj.Activar_ValidacionSi = RadioValidacionSi.Checked
        EstadisticsObj.Activar_ValidacionNo = RadioValidacionNo.Checked
        'Intereses
        EstadisticsObj.Activar_InteresesSi = RadioInteresesSi.Checked
        EstadisticsObj.Activar_InteresesNo = RadioInteresesNo.Checked
        EstadisticsObj.Cuotas = TxtCuotas.Text
        'Bcp
        EstadisticsObj.BcpSi = RadioBCPSi.Checked
        EstadisticsObj.BcpNo = RadioBCPNo.Checked
        'BNB
        EstadisticsObj.BnbSi = RadioBNBSi.Checked
        EstadisticsObj.BnbNo = RadioBNBNo.Checked
        'BISA
        EstadisticsObj.BisaSi = RadioBISASi.Checked
        EstadisticsObj.BisaNo = RadioBISANo.Checked
        'Anulacion
        EstadisticsObj.AnulacionSi = RadioAnulaciónSi.Checked
        EstadisticsObj.AnulacionNo = RadioAnulaciónNo.Checked
        'Deposito
        EstadisticsObj.DepositoSi = RadioDepositoSi.Checked
        EstadisticsObj.DepositoNo = RadioDepositoNo.Checked
        'Propina
        EstadisticsObj.PropinaSi = RadioPropinaSi.Checked
        EstadisticsObj.PropinaNo = RadioPropinaNo.Checked
        'Echo Test
        EstadisticsObj.EchoSi = RadioEchoSi.Checked
        EstadisticsObj.EchoNo = RadioEchoNo.Checked
        'Cash Club
        EstadisticsObj.CashSi = RadioCashSi.Checked
        EstadisticsObj.CashNo = RadioCashNo.Checked
        'Reservas
        EstadisticsObj.ReservasSi = RadioReservasSi.Checked
        EstadisticsObj.ReservasNo = RadioReservasNo.Checked
        'Fidelización
        EstadisticsObj.FidelizacionSi = RadioFidelizaciónSi.Checked
        EstadisticsObj.FidelizacionNo = RadioFidelizaciónNo.Checked
        'Soat
        EstadisticsObj.SoatSi = RadioSoatSi.Checked
        EstadisticsObj.SoatNo = RadioSoatNo.Checked
        'Promociones
        EstadisticsObj.PromocionesSi = RadioPromocionesSi.Checked
        EstadisticsObj.PromocionesNo = RadioPromocionesNo.Checked
        'CMB
        EstadisticsObj.CmbSi = RadioCMBSi.Checked
        EstadisticsObj.CmbNo = RadioCMBNo.Checked
        'Ctl
        EstadisticsObj.CTLSi = RadioCTLSi.Checked
        EstadisticsObj.CTLNo = RadioCTLNo.Checked
        'Logo 1
        EstadisticsObj.Logo1Si = RadioLogo1Si.Checked
        EstadisticsObj.Logo1No = RadioLogo1No.Checked
        'Logo 2
        EstadisticsObj.Logo2Si = RadioLogo2Si.Checked
        EstadisticsObj.Logo2No = RadioLogo2No.Checked
        'Cajas
        EstadisticsObj.CajasSi = RadioCajasSi.Checked
        EstadisticsObj.CajasNo = RadioCajasNo.Checked
        'Tipo Cajas
        EstadisticsObj.USBSi = RadioOpcionUsb.Checked
        EstadisticsObj.TCPSi = RadioOpcionTcp.Checked
        EstadisticsObj.IpCajas = TextiPCajas.Text
        EstadisticsObj.PuertoCajas = txtPortCajas.Text
        'Bin Cajas
        EstadisticsObj.BinSi = RadioBINSi.Checked
        EstadisticsObj.BinNo = RadioBINo.Checked
        'Voucher
        EstadisticsObj.VoucherSi = RadioVaucherSi.Checked
        EstadisticsObj.VoucherNo = RadioVaucherNo.Checked
        'activar serial
        EstadisticsObj.serialSi = serialSi.Checked
        EstadisticsObj.serialNo = serialNo.Checked
        'Ip Polaris
        EstadisticsObj.Ip_Polaris = IpPolaris.Text
        'Port Polaris
        EstadisticsObj.Port_Polaris = PuertoPolaris.Text
        'ip inicialización Polaris
        EstadisticsObj.ipIni_Polaris = IpIni.Text
        'port inicialización Polaris
        EstadisticsObj.PortIni_Polaris = PortIni.Text
        'TID inicialización Polaris
        EstadisticsObj.TidIni_Polaris = TIDPOS.Text
        'Monto max inicialización Polaris
        EstadisticsObj.MontoIni_Polaris = txtMontoMax.Text
        'firma
        EstadisticsObj.RadioFirmaSi1 = RadioFirmaSi.Checked
        EstadisticsObj.RadioFirmaNo1 = RadioFirmaNo.Checked
        'doc
        EstadisticsObj.RadioDocSi1 = RadioDocSi.Checked
        EstadisticsObj.RadioDocNo1 = RadioDocNo.Checked
        'telefono
        EstadisticsObj.RadioTelefonoSi1 = RadioTelefonoSi.Checked
        EstadisticsObj.RadioTelefonoNo1 = RadioTelefonoNo.Checked
        'emisor
        EstadisticsObj.RadioEmisorSi1 = RadioEmisorSi.Checked
        EstadisticsObj.RadioEmisorNo1 = RadioEmisorNo.Checked

        If editParametersNew(txtTIDIni.Text) Then

            pnlMsg.Visible = True
            pnlError.Visible = False
            lblMsg.Text = "Parametros editados correctamente."
        Else
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error editando parametros. Por favor valide los datos."
        End If
    End Sub
    ''' <summary>
    ''' Editar parametros
    ''' </summary>
    Public Function editParameters(ByVal TID As String) As Boolean
        Dim connection As New SqlConnection(strConnectionString)
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As String
        Dim retVal As Boolean
        Dim result As String
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString
        Using Conn As New SqlConnection(strConnString)
            Conn.Open()
            Dim ConsultaSQL = "SELECT descripcion FROM ModeloTerminal where id='" & ddlTipoParametro.SelectedValue & "'"
            result = New SqlCommand(ConsultaSQL, Conn).ExecuteScalar().ToString()
        End Using
        Dim modeloPos As String = Result

        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webEditar_parametros_pos"
            command.Parameters.Clear()
            command.Parameters.Add(New SqlParameter("Modelo", modeloPos))
            command.Parameters.Add(New SqlParameter("TID", TID))
            command.Parameters.Add(New SqlParameter("EfectivoSi", EstadisticsObj.NewstrEfectivoSi1))
            command.Parameters.Add(New SqlParameter("NII_Pago", EstadisticsObj.NewstrNii_pago1))
            command.Parameters.Add(New SqlParameter("NII_Soat", EstadisticsObj.NewstrNii_Soat1))
            command.Parameters.Add(New SqlParameter("NII_Promociones", EstadisticsObj.NewstrNii_promociones1))
            command.Parameters.Add(New SqlParameter("Activar_creditoSi", EstadisticsObj.NewstrActivar_credito1))
            command.Parameters.Add(New SqlParameter("Activar_Banca_JovenSi", EstadisticsObj.NewstrActivar_Banca_jovenSi1))
            command.Parameters.Add(New SqlParameter("Activar_Banca_Joven_Porcentaje", EstadisticsObj.NewstrActivar_Banca_joven_Porcentaje1))

            command.Parameters.Add(New SqlParameter("Activar_ValidacionSi", EstadisticsObj.NewstrActivar_ValidacionSi1))
            command.Parameters.Add(New SqlParameter("Activar_InteresesSi", EstadisticsObj.NewstrActivar_InteresesSi1))
            command.Parameters.Add(New SqlParameter("Cuotas", EstadisticsObj.NewstrCuotas1))
            command.Parameters.Add(New SqlParameter("BcpSi", EstadisticsObj.NewstrActivar_BcpSi1))
            command.Parameters.Add(New SqlParameter("BnbSi", EstadisticsObj.NewstrActivar_BnbSi1))
            command.Parameters.Add(New SqlParameter("BisaSi", EstadisticsObj.NewstrActivar_bisaSi1))
            command.Parameters.Add(New SqlParameter("AnulacionSi", EstadisticsObj.NewstrActivar_AnulacionSi1))
            command.Parameters.Add(New SqlParameter("DepositoSi", EstadisticsObj.NewstrActivar_DepositoSi1))
            command.Parameters.Add(New SqlParameter("PropinaSi", EstadisticsObj.NewstrActivar_PropinaSi1))
            command.Parameters.Add(New SqlParameter("EchoSi", EstadisticsObj.NewstrActivar_EchoSi1))
            command.Parameters.Add(New SqlParameter("CashSi", EstadisticsObj.NewstrActivar_CashSi1))
            command.Parameters.Add(New SqlParameter("ReservasSi", EstadisticsObj.NewstrActivar_ReservasSi1))
            command.Parameters.Add(New SqlParameter("FidelizacionSi", EstadisticsObj.NewstrActivar_FidelizacionSi1))
            command.Parameters.Add(New SqlParameter("SoatSi", EstadisticsObj.NewstrActivar_SoatSi1))
            command.Parameters.Add(New SqlParameter("PromocionesSi", EstadisticsObj.NewstrActivar_PromocionesSi1))
            command.Parameters.Add(New SqlParameter("CmbSi", EstadisticsObj.NewstrActivar_CmbSi1))
            command.Parameters.Add(New SqlParameter("CTLSi", EstadisticsObj.NewstrActivar_CtlSi1))
            command.Parameters.Add(New SqlParameter("Logo1Si", EstadisticsObj.NewstrActivar_Logo1Si1))
            command.Parameters.Add(New SqlParameter("Logo2Si", EstadisticsObj.NewstrActivar_Logo2Si1))
            command.Parameters.Add(New SqlParameter("CajasSi", EstadisticsObj.NewstrActivar_CajasSi1))
            command.Parameters.Add(New SqlParameter("USBSi", EstadisticsObj.NewstrActivar_UsbSi1))
            command.Parameters.Add(New SqlParameter("TCPSi", EstadisticsObj.NewstrActivar_TcpSi1))
            command.Parameters.Add(New SqlParameter("IpCajas", EstadisticsObj.NewstrIpCajas1))
            command.Parameters.Add(New SqlParameter("PuertoCajas", EstadisticsObj.NewstrPuertoCajas1))
            command.Parameters.Add(New SqlParameter("BinSi", EstadisticsObj.NewstrActivar_BinSi1))
            command.Parameters.Add(New SqlParameter("VoucherSi", EstadisticsObj.NewstrActivar_VoucherSi1))
            'Campos Nuevos
            command.Parameters.Add(New SqlParameter("SerialSi", EstadisticsObj.NewstrActivar_serialSi1))
            command.Parameters.Add(New SqlParameter("IpPolaris", EstadisticsObj.NewstrIp_Polaris1))
            command.Parameters.Add(New SqlParameter("PortPolaris", EstadisticsObj.NewstrPort_Polaris1))
            command.Parameters.Add(New SqlParameter("IpIni", EstadisticsObj.NewstrIpIni_Polaris1))
            command.Parameters.Add(New SqlParameter("PortIni", EstadisticsObj.NewstrPortIni_Polaris1))
            command.Parameters.Add(New SqlParameter("TidIni", EstadisticsObj.NewstrTidIni_Polaris1))
            command.Parameters.Add(New SqlParameter("MontoMax", EstadisticsObj.NewstrMontoIni_Polaris1))
            command.Parameters.Add(New SqlParameter("firma", EstadisticsObj.RadioFirmaSi1))
            command.Parameters.Add(New SqlParameter("doc", EstadisticsObj.RadioDocSi1))
            command.Parameters.Add(New SqlParameter("telefono", EstadisticsObj.RadioTelefonoSi1))
            command.Parameters.Add(New SqlParameter("emisor", EstadisticsObj.RadioEmisorSi1))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetString(0)
                    retVal = True
                End While
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try

        Return retVal

    End Function
    Public Function editParametersNew(ByVal TID As String) As Boolean
        Dim connection As New SqlConnection(strConnectionString)
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As String
        Dim retVal As Boolean
        Dim result As String
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString
        Using Conn As New SqlConnection(strConnString)
            Conn.Open()
            Dim ConsultaSQL = "SELECT descripcion FROM ModeloTerminal where id='" & ddlTipoParametro.SelectedValue & "'"
            result = New SqlCommand(ConsultaSQL, Conn).ExecuteScalar().ToString()
        End Using

        Dim modeloPos As String = result

        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webEditar_parametros_pos"
            command.Parameters.Clear()
            command.Parameters.Add(New SqlParameter("Modelo", modeloPos))
            command.Parameters.Add(New SqlParameter("TID", TID))
            command.Parameters.Add(New SqlParameter("EfectivoSi", EstadisticsObj.EfectivoSi))
            command.Parameters.Add(New SqlParameter("NII_Pago", EstadisticsObj.NII_Pago))
            command.Parameters.Add(New SqlParameter("NII_Soat", EstadisticsObj.NII_Soat))
            command.Parameters.Add(New SqlParameter("NII_Promociones", EstadisticsObj.NII_Promociones))
            command.Parameters.Add(New SqlParameter("Activar_creditoSi", EstadisticsObj.Activar_creditoSi))
            command.Parameters.Add(New SqlParameter("Activar_Banca_JovenSi", EstadisticsObj.Activar_Banca_JovenSi))
            command.Parameters.Add(New SqlParameter("Activar_Banca_Joven_Porcentaje", EstadisticsObj.Activar_Banca_Joven_Porcentaje))
            command.Parameters.Add(New SqlParameter("Activar_ValidacionSi", EstadisticsObj.Activar_ValidacionSi))
            command.Parameters.Add(New SqlParameter("Activar_InteresesSi", EstadisticsObj.Activar_InteresesSi))
            command.Parameters.Add(New SqlParameter("Cuotas", EstadisticsObj.Cuotas))
            command.Parameters.Add(New SqlParameter("BcpSi", EstadisticsObj.BcpSi))
            command.Parameters.Add(New SqlParameter("BnbSi", EstadisticsObj.BnbSi))
            command.Parameters.Add(New SqlParameter("BisaSi", EstadisticsObj.BisaSi))
            command.Parameters.Add(New SqlParameter("AnulacionSi", EstadisticsObj.AnulacionSi))
            command.Parameters.Add(New SqlParameter("DepositoSi", EstadisticsObj.DepositoSi))
            command.Parameters.Add(New SqlParameter("PropinaSi", EstadisticsObj.PropinaSi))
            command.Parameters.Add(New SqlParameter("EchoSi", EstadisticsObj.EchoSi))
            command.Parameters.Add(New SqlParameter("CashSi", EstadisticsObj.CashSi))
            command.Parameters.Add(New SqlParameter("ReservasSi", EstadisticsObj.ReservasSi))
            command.Parameters.Add(New SqlParameter("FidelizacionSi", EstadisticsObj.FidelizacionSi))
            command.Parameters.Add(New SqlParameter("SoatSi", EstadisticsObj.SoatSi))
            command.Parameters.Add(New SqlParameter("PromocionesSi", EstadisticsObj.PromocionesSi))
            command.Parameters.Add(New SqlParameter("CmbSi", EstadisticsObj.CmbSi))
            command.Parameters.Add(New SqlParameter("CTLSi", EstadisticsObj.CTLSi))
            command.Parameters.Add(New SqlParameter("Logo1Si", EstadisticsObj.Logo1Si))
            command.Parameters.Add(New SqlParameter("Logo2Si", EstadisticsObj.Logo2Si))
            command.Parameters.Add(New SqlParameter("CajasSi", EstadisticsObj.CajasSi))
            command.Parameters.Add(New SqlParameter("USBSi", EstadisticsObj.USBSi))
            command.Parameters.Add(New SqlParameter("TCPSi", EstadisticsObj.TCPSi))
            command.Parameters.Add(New SqlParameter("IpCajas", EstadisticsObj.IpCajas))
            command.Parameters.Add(New SqlParameter("PuertoCajas", EstadisticsObj.PuertoCajas))
            command.Parameters.Add(New SqlParameter("BinSi", EstadisticsObj.BinSi))
            command.Parameters.Add(New SqlParameter("VoucherSi", EstadisticsObj.VoucherSi))
            'Campos Nuevos
            command.Parameters.Add(New SqlParameter("SerialSi", EstadisticsObj.serialSi))
            command.Parameters.Add(New SqlParameter("IpPolaris", EstadisticsObj.Ip_Polaris))
            command.Parameters.Add(New SqlParameter("PortPolaris", EstadisticsObj.Port_Polaris))
            command.Parameters.Add(New SqlParameter("IpIni", EstadisticsObj.ipIni_Polaris))
            command.Parameters.Add(New SqlParameter("PortIni", EstadisticsObj.PortIni_Polaris))
            command.Parameters.Add(New SqlParameter("TidIni", EstadisticsObj.TidIni_Polaris))
            command.Parameters.Add(New SqlParameter("MontoMax", EstadisticsObj.MontoIni_Polaris))
            command.Parameters.Add(New SqlParameter("firma", EstadisticsObj.RadioFirmaSi1))
            command.Parameters.Add(New SqlParameter("doc", EstadisticsObj.RadioDocSi1))
            command.Parameters.Add(New SqlParameter("telefono", EstadisticsObj.RadioTelefonoSi1))
            command.Parameters.Add(New SqlParameter("emisor", EstadisticsObj.RadioEmisorSi1))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetString(0)
                    retVal = True
                End While
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try

        Return retVal

    End Function
    Protected Sub btnNewTid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNewTid.Click
        modalPopupExt.Show()
    End Sub
    Protected Sub btnActivateOption_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActivateOption.Click

        EstadisticsObj = New Group(strConnectionString, objSessionParams.intSelectedGroup)
        EstadisticsObj.NewstrEfectivoSi1 = RadioNewEfectivoSi.Checked
        EstadisticsObj.NewstrEfectivoNo1 = RadioNewEfectivoNo.Checked
        EstadisticsObj.NewstrNii_pago1 = TxtNewNiiPago.Text
        EstadisticsObj.NewstrNii_Soat1 = TextNewsoatNii.Text
        EstadisticsObj.NewstrNii_promociones1 = TexNewPromocionesNii.Text
        EstadisticsObj.NewstrActivar_credito1 = RadiNewCreditotxSi.Checked
        EstadisticsObj.NewstrActivar_creditoNo1 = RadiNewCreditotxno.Checked
        EstadisticsObj.NewstrActivar_Banca_jovenSi1 = RadioNewBancaJovenSi.Checked
        EstadisticsObj.NewstrActivar_Banca_jovenNo1 = RadioNewBancaJovenNo.Checked
        EstadisticsObj.NewstrActivar_Banca_joven_Porcentaje1 = TxtNewBancaJovenPorcentaje.Text
        'validacion
        EstadisticsObj.NewstrActivar_ValidacionSi1 = RadioValidacionSi.Checked
        EstadisticsObj.NewstrActivar_ValidacionNo1 = RadioValidacionNo.Checked
        'Intereses
        EstadisticsObj.NewstrActivar_InteresesSi1 = RadioNewInteresesSi.Checked
        EstadisticsObj.NewstrActivar_InteresesNo1 = RadioNewInteresesNo.Checked
        EstadisticsObj.NewstrCuotas1 = TxtNewCuotas.Text
        'Bcp
        EstadisticsObj.NewstrActivar_BcpSi1 = RadioNewBCPSi.Checked
        EstadisticsObj.NewstrActivar_BcpNo1 = RadioNewBCPNo.Checked
        'BNB
        EstadisticsObj.NewstrActivar_BnbSi1 = RadioNewBNBSi.Checked
        EstadisticsObj.NewstrActivar_bisaNo1 = RadioNewBNBNo.Checked
        'BISA
        EstadisticsObj.NewstrActivar_bisaSi1 = RadioNewBISASi.Checked
        EstadisticsObj.NewstrActivar_bisaNo1 = RadioNewBISANo.Checked
        'Anulacion
        EstadisticsObj.NewstrActivar_AnulacionSi1 = RadioNewAnulaciónSi.Checked
        EstadisticsObj.NewstrActivar_AnulacionNo1 = RadioNewAnulaciónNo.Checked
        'Deposito
        EstadisticsObj.NewstrActivar_DepositoSi1 = RadioNewDepositoSi.Checked
        EstadisticsObj.NewstrActivar_DepositoNo1 = RadioNewDepositoNo.Checked
        'Propina
        EstadisticsObj.NewstrActivar_PropinaSi1 = RadioNewPropinaSi.Checked
        EstadisticsObj.NewstrActivar_PropinaNo1 = RadioNewPropinaNo.Checked
        'Echo Test
        EstadisticsObj.NewstrActivar_EchoSi1 = RadioNewEchoSi.Checked
        EstadisticsObj.NewstrActivar_EchoNo1 = RadioNewEchoNo.Checked
        'Cash Club
        EstadisticsObj.NewstrActivar_CashSi1 = RadioNewCashSi.Checked
        EstadisticsObj.NewstrActivar_CashNo1 = RadioNewCashNo.Checked
        'Reservas
        EstadisticsObj.NewstrActivar_ReservasSi1 = RadioNewReservasSi.Checked
        EstadisticsObj.NewstrActivar_ReservasNo1 = RadioNewReservasNo.Checked
        'Fidelización
        EstadisticsObj.NewstrActivar_FidelizacionSi1 = RadioNewFidelizaciónSi.Checked
        EstadisticsObj.NewstrActivar_FidelizacionNo1 = RadioNewFidelizaciónNo.Checked
        'Soat
        EstadisticsObj.NewstrActivar_SoatSi1 = RadioNewSoatSi.Checked
        EstadisticsObj.NewstrActivar_SoatNo1 = RadioNewSoatNo.Checked
        'Promociones
        EstadisticsObj.NewstrActivar_PromocionesSi1 = RadioNewPromocionesSi.Checked
        EstadisticsObj.NewstrActivar_PromocionesNo1 = RadioNewPromocionesNo.Checked
        'CMB
        EstadisticsObj.NewstrActivar_CmbSi1 = RadioNewCMBSi.Checked
        EstadisticsObj.NewstrActivar_CmbNo1 = RadioNewCMBNo.Checked
        'Ctl
        EstadisticsObj.NewstrActivar_CtlSi1 = RadioNewCTLSi.Checked
        EstadisticsObj.NewstrActivar_CtlNo1 = RadioNewCTLNo.Checked
        'Logo 1
        EstadisticsObj.NewstrActivar_Logo1Si1 = RadionewLogo1Si.Checked
        EstadisticsObj.NewstrActivar_Logo1No1 = RadioNewLogo1No.Checked
        'Logo 2
        EstadisticsObj.NewstrActivar_Logo2Si1 = RadioNewLogo2Si.Checked
        EstadisticsObj.NewstrActivar_Logo2No1 = RadioNewLogo2No.Checked
        'Cajas
        EstadisticsObj.NewstrActivar_CajasSi1 = RadioNewCajasSi.Checked
        EstadisticsObj.NewstrActivar_CajasNo1 = RadioNewCajasNo.Checked
        'Tipo Cajas
        EstadisticsObj.NewstrActivar_UsbSi1 = RadioNewOpcionUsb.Checked
        EstadisticsObj.NewstrActivar_TcpSi1 = RadioNewOpcionTcp.Checked
        EstadisticsObj.NewstrIpCajas1 = TextNewiPCajas.Text
        EstadisticsObj.NewstrPuertoCajas1 = txtNewPortCajas.Text
        'Bin Cajas
        EstadisticsObj.NewstrActivar_BinSi1 = RadioNewBINSi.Checked
        EstadisticsObj.NewstrActivar_BinNo1 = RadioNewBINo.Checked
        'Voucher
        EstadisticsObj.NewstrActivar_VoucherSi1 = RadioNewVaucherSi.Checked
        EstadisticsObj.NewstrActivar_VoucherNo1 = RadioNewVaucherNo.Checked
        'activar serial
        EstadisticsObj.NewstrActivar_serialSi1 = serialNewSi.Checked
        EstadisticsObj.NewstrActivar_serialNo1 = serialNewNo.Checked
        'Ip Polaris
        EstadisticsObj.NewstrIp_Polaris1 = NewIpPolaris.Text
        'Port Polaris
        EstadisticsObj.NewstrPort_Polaris1 = NewPuertoPolaris.Text
        'ip inicialización Polaris
        EstadisticsObj.NewstrIpIni_Polaris1 = NewIpIni.Text
        'port inicialización Polaris
        EstadisticsObj.NewstrPortIni_Polaris1 = NewPortIni.Text
        'TID inicialización Polaris
        EstadisticsObj.NewstrTidIni_Polaris1 = NewTIDPOS.Text
        'Monto max inicialización Polaris
        EstadisticsObj.NewstrMontoIni_Polaris1 = txtNewMontoMax.Text

        If editParameters(txtTIDIni.Text) Then

            pnlMsg.Visible = True
            pnlError.Visible = False
            lblMsg.Text = "Parametros editados correctamente."
        Else
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error editando parametros. Por favor valide los datos."
        End If
    End Sub

    Protected Sub ddlNewTconf_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlNewTconf.SelectedIndexChanged
        Dim Modelo As String
        Dim Model9220 As String = "92205431"
        Dim ModelLegacy As String = "10005431"

        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString
        Using Conn As New SqlConnection(strConnString)
            Conn.Open()
            Dim ConsultaSQL = "SELECT descripcion FROM ModeloTerminal where id='" & ddlNewTconf.SelectedValue & "'"
            Modelo = New SqlCommand(ConsultaSQL, Conn).ExecuteScalar().ToString()
        End Using
        If Modelo = "NEW9220" Then
            DatosStisModal(Model9220, ddlNewTconf.SelectedValue)
            modalPopupExt.Show()


            RadioNewEfectivoSi.Visible = True
            RadioNewEfectivoNo.Visible = True
            TxtNewNiiPago.Visible = True
            TextNewsoatNii.Visible = True
            TexNewPromocionesNii.Visible = True
            RadiNewCreditotxSi.Visible = True
            RadiNewCreditotxno.Visible = True
            RadiNewCreditotxSi.Enabled = False
            RadiNewCreditotxno.Enabled = False
            RadioNewBancaJovenSi.Visible = True
            RadioNewBancaJovenNo.Visible = True
            TxtNewBancaJovenPorcentaje.Visible = True
            RadioNewValidacionSi.Visible = True
            RadioNewValidacionNo.Visible = True
            RadioNewInteresesSi.Visible = True
            RadioNewInteresesNo.Visible = True
            RadioNewInteresesSi.Enabled = False
            RadioNewInteresesNo.Enabled = False
            TxtNewCuotas.Visible = True
            TxtNewCuotas.Enabled = True
            RadioNewBCPSi.Visible = True
            RadioNewBCPNo.Visible = True
            RadioNewBNBSi.Visible = True
            RadioNewBNBNo.Visible = True
            RadioNewBISASi.Visible = True
            RadioNewBISANo.Visible = True
            RadioNewAnulaciónSi.Visible = True
            RadioNewAnulaciónNo.Visible = True
            RadioNewDepositoSi.Visible = True
            RadioNewDepositoNo.Visible = True
            RadioNewPropinaSi.Visible = True
            RadioNewPropinaNo.Visible = True
            RadioNewEchoSi.Visible = True
            RadioNewEchoNo.Visible = True
            RadioNewCashSi.Visible = True
            RadioNewCashNo.Visible = True
            RadioNewCashSi.Enabled = False
            RadioNewCashNo.Enabled = False
            RadioNewReservasSi.Visible = True
            RadioNewReservasNo.Visible = True
            RadioNewFidelizaciónSi.Visible = True
            RadioNewFidelizaciónNo.Visible = True
            RadioNewFidelizaciónSi.Enabled = False
            RadioNewFidelizaciónNo.Enabled = False
            RadioNewSoatSi.Visible = True
            RadioNewSoatNo.Visible = True
            RadioNewPromocionesSi.Visible = True
            RadioNewPromocionesNo.Visible = True
            RadioNewCMBSi.Visible = True
            RadioNewCMBNo.Visible = True
            RadioNewCTLSi.Visible = True
            RadioNewCTLNo.Visible = True
            RadionewLogo1Si.Visible = True
            RadioNewLogo1No.Visible = True
            RadioNewLogo2Si.Visible = True
            RadioNewLogo2No.Visible = True
            RadioNewCajasSi.Visible = True
            RadioNewCajasNo.Visible = True
            RadioNewOpcionUsb.Visible = True
            RadioNewOpcionTcp.Visible = True
            TextNewiPCajas.Visible = True
            txtNewPortCajas.Visible = True
            RadioNewBINSi.Visible = True
            RadioNewBINo.Visible = True
            RadioNewVaucherSi.Visible = True
            RadioNewVaucherNo.Visible = True
            RadioNewPropinaSi.Enabled = False
            RadioNewPropinaNo.Enabled = False
            RadioNewSoatSi.Enabled = True
            RadioNewSoatNo.Enabled = True
            RadioNewPromocionesSi.Enabled = True
            RadioNewPromocionesNo.Enabled = True
            RadioNewCMBSi.Enabled = True
            RadioNewCMBNo.Enabled = True
            RadioNewLogo2Si.Enabled = True
            RadioNewLogo2No.Enabled = True
            serialNewSi.Enabled = False
            serialNewNo.Enabled = False
            serialNewSi.Visible = True
            serialNewNo.Visible = True
            NewIpPolaris.Enabled = True
            NewIpPolaris.Visible = True
            NewPuertoPolaris.Enabled = True
            NewPuertoPolaris.Visible = True
            NewIpIni.Enabled = True
            NewIpIni.Visible = True
            NewPortIni.Enabled = True
            NewPortIni.Visible = True
            NewTIDPOS.Visible = True
            NewTIDPOS.Enabled = True
            txtMontoMax.Visible = True

        ElseIf Modelo = ("T700") Or Modelo = "T800" Or Modelo = "T1000" Or Modelo = "creon" Or Modelo = "A5-T1000" Then

            DatosStisModal(ModelLegacy, ddlNewTconf.SelectedValue)
            modalPopupExt.Show()

            RadioNewEfectivoSi.Visible = True
            RadioNewEfectivoNo.Visible = True
            TxtNewNiiPago.Visible = True
            TextNewsoatNii.Visible = True
            TexNewPromocionesNii.Visible = True
            'Enabled
            TextNewsoatNii.Enabled = False
            TexNewPromocionesNii.Enabled = False
            RadiNewCreditotxSi.Visible = True
            RadiNewCreditotxno.Visible = True
            RadiNewCreditotxSi.Enabled = False
            RadiNewCreditotxno.Enabled = False
            RadioNewBancaJovenSi.Visible = True
            RadioNewBancaJovenNo.Visible = True
            TxtNewBancaJovenPorcentaje.Visible = True
            RadioNewValidacionSi.Visible = True
            RadioNewValidacionNo.Visible = True
            RadioNewInteresesSi.Visible = True
            RadioNewInteresesNo.Visible = True
            RadioNewInteresesSi.Enabled = False
            RadioNewInteresesNo.Enabled = False
            TxtNewCuotas.Visible = True
            TxtNewCuotas.Enabled = True
            RadioNewBCPSi.Visible = True
            RadioNewBCPNo.Visible = True
            RadioNewBNBSi.Visible = True
            RadioNewBNBNo.Visible = True
            RadioNewBISASi.Visible = True
            RadioNewBISANo.Visible = True
            RadioNewAnulaciónSi.Visible = True
            RadioNewAnulaciónNo.Visible = True
            RadioNewDepositoSi.Visible = True
            RadioNewDepositoNo.Visible = True
            RadioNewDepositoSi.Enabled = True
            RadioNewDepositoNo.Enabled = True
            RadioNewPropinaSi.Visible = True
            RadioNewPropinaNo.Visible = True
            RadioNewEchoSi.Visible = True
            RadioNewEchoNo.Visible = True
            RadioNewCashSi.Visible = True
            RadioNewCashNo.Visible = True
            RadioNewCashSi.Enabled = False
            RadioNewCashNo.Enabled = False
            RadioNewReservasSi.Visible = True
            RadioNewReservasNo.Visible = True
            RadioNewFidelizaciónSi.Visible = True
            RadioNewFidelizaciónNo.Visible = True
            RadioNewFidelizaciónSi.Enabled = False
            RadioNewFidelizaciónNo.Enabled = False
            RadioNewSoatSi.Visible = True
            RadioNewSoatNo.Visible = True
            RadioNewPromocionesSi.Visible = True
            RadioNewPromocionesNo.Visible = True
            RadioNewCMBSi.Visible = True
            RadioNewCMBNo.Visible = True
            'Enabled
            RadioNewFidelizaciónSi.Visible = True
            RadioNewFidelizaciónNo.Visible = True
            RadioNewFidelizaciónSi.Enabled = False
            RadioNewFidelizaciónNo.Enabled = False
            RadioNewSoatSi.Enabled = False
            RadioNewSoatNo.Enabled = False
            RadioNewPromocionesSi.Enabled = False
            RadioNewPromocionesNo.Enabled = False
            RadioNewCMBSi.Enabled = False
            RadioNewCMBNo.Enabled = False
            RadioNewCTLSi.Visible = True
            RadioNewCTLNo.Visible = True
            RadionewLogo1Si.Visible = True
            RadioNewLogo1No.Visible = True
            RadioNewLogo2Si.Visible = True
            RadioNewLogo2No.Visible = True
            RadioNewLogo2Si.Enabled = True
            RadioNewLogo2No.Enabled = True
            RadioNewCajasSi.Visible = True
            RadioNewCajasNo.Visible = True
            RadioNewOpcionUsb.Visible = True
            RadioNewOpcionTcp.Visible = True
            TextNewiPCajas.Visible = True
            txtNewPortCajas.Visible = True
            RadioNewBINSi.Visible = True
            RadioNewBINo.Visible = True
            RadioNewVaucherSi.Visible = True
            RadioNewVaucherNo.Visible = True
            serialNewSi.Enabled = True
            serialNewNo.Enabled = True
            serialNewSi.Visible = True
            serialNewNo.Visible = True
            NewIpPolaris.Enabled = True
            NewIpPolaris.Visible = True
            NewPuertoPolaris.Enabled = True
            NewPuertoPolaris.Visible = True
            NewIpIni.Enabled = True
            NewIpIni.Visible = True
            NewPortIni.Enabled = True
            NewPortIni.Visible = True
            NewTIDPOS.Visible = True
            NewTIDPOS.Enabled = True
            txtNewMontoMax.Visible = True

        ElseIf Modelo = ("N8210") Or Modelo = "N7210" Or Modelo = "N8110" Or Modelo = "NEW8210" Or Modelo = "NEW7210" Then
            DatosStisModal(ModelLegacy, ddlNewTconf.SelectedValue)
            modalPopupExt.Show()

            RadioNewEfectivoSi.Visible = True
            RadioNewEfectivoNo.Visible = True
            TxtNewNiiPago.Visible = True
            TextNewsoatNii.Visible = True
            TexNewPromocionesNii.Visible = True
            RadiNewCreditotxSi.Visible = True
            RadiNewCreditotxno.Visible = True
            RadiNewCreditotxSi.Enabled = False
            RadiNewCreditotxno.Enabled = False
            RadioNewBancaJovenSi.Visible = True
            RadioNewBancaJovenNo.Visible = True
            TxtNewBancaJovenPorcentaje.Visible = True
            RadioNewValidacionSi.Visible = True
            RadioNewValidacionNo.Visible = True
            RadioNewInteresesSi.Visible = True
            RadioNewInteresesNo.Visible = True
            RadioNewInteresesSi.Enabled = False
            RadioNewInteresesNo.Enabled = False
            TxtNewCuotas.Visible = True
            TxtNewCuotas.Enabled = True
            RadioNewBCPSi.Visible = True
            RadioNewBCPNo.Visible = True
            RadioNewBNBSi.Visible = True
            RadioNewBNBNo.Visible = True
            RadioNewBISASi.Visible = True
            RadioNewBISANo.Visible = True
            RadioNewAnulaciónSi.Visible = True
            RadioNewAnulaciónNo.Visible = True
            RadioNewDepositoSi.Visible = True
            RadioNewDepositoNo.Visible = True
            RadioNewPropinaSi.Visible = True
            RadioNewPropinaNo.Visible = True
            'enabled
            RadioNewPropinaSi.Enabled = False
            RadioNewPropinaNo.Enabled = False
            RadioNewEchoSi.Visible = True
            RadioNewEchoNo.Visible = True
            RadioNewCashSi.Visible = True
            RadioNewCashNo.Visible = True
            RadioNewCashSi.Enabled = False
            RadioNewCashNo.Enabled = False
            RadioNewReservasSi.Visible = True
            RadioNewReservasNo.Visible = True
            RadioNewFidelizaciónSi.Visible = True
            RadioNewFidelizaciónNo.Visible = True
            RadioNewSoatSi.Visible = True
            RadioNewSoatNo.Visible = True
            RadioNewPromocionesSi.Visible = True
            RadioNewPromocionesNo.Visible = True
            RadioNewCMBSi.Visible = True
            RadioNewCMBNo.Visible = True
            'enabled
            RadioNewFidelizaciónSi.Visible = True
            RadioNewFidelizaciónNo.Visible = True
            RadioNewFidelizaciónSi.Enabled = False
            RadioNewFidelizaciónNo.Enabled = False
            RadioNewSoatSi.Enabled = False
            RadioNewSoatNo.Enabled = False
            RadioNewPromocionesSi.Enabled = False
            RadioNewPromocionesNo.Enabled = False
            RadioNewCMBSi.Enabled = False
            RadioNewCMBNo.Enabled = False
            RadioNewCTLSi.Visible = True
            RadioNewCTLNo.Visible = True
            RadionewLogo1Si.Visible = True
            RadioNewLogo1No.Visible = True
            RadioNewLogo2Si.Enabled = False
            RadioNewLogo2No.Enabled = False
            RadioNewLogo2Si.Visible = True
            RadioNewLogo2No.Visible = True
            RadioNewCajasSi.Visible = True
            RadioNewCajasNo.Visible = True
            RadioNewOpcionUsb.Visible = True
            RadioNewOpcionTcp.Visible = True
            TextNewiPCajas.Visible = True
            txtNewPortCajas.Visible = True
            RadioNewBINSi.Visible = True
            RadioNewBINo.Visible = True
            'enabled
            RadioNewBINSi.Enabled = False
            RadioNewBINo.Enabled = False
            RadioNewVaucherSi.Visible = True
            RadioNewVaucherNo.Visible = True
            serialNewSi.Enabled = False
            serialNewNo.Enabled = False
            serialNewSi.Visible = True
            serialNewNo.Visible = True
            NewIpPolaris.Enabled = True
            NewIpPolaris.Visible = True
            NewPuertoPolaris.Enabled = True
            NewPuertoPolaris.Visible = True
            NewIpIni.Enabled = True
            NewIpIni.Visible = True
            NewPortIni.Enabled = True
            NewPortIni.Visible = True
            NewTIDPOS.Visible = True
            NewTIDPOS.Enabled = True
            txtNewMontoMax.Visible = True
        End If
    End Sub

    <Obsolete>
    Protected Sub btnAddApplication_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddApplication.Click

        Dim strDirectorio As String = ""
        Dim strNombreFile As String = ""
        Dim Archivo As String = ""

        strDirectorio = ConfigurationSettings.AppSettings.Get("TempFolder") + objSessionParams.strAppFilename

        'Validar que se haya subido un archivo
        If (objSessionParams.strAppFilename.Equals("")) Then
            pnlMsg.Visible = False
            pnlError.Visible = True
            lblError.Text = "Debe seleccionar un archivo de aplicación para subir al grupo."
            'Set Toggle Visible
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
            Return
        End If

        Try

            'Capturamos el contenido del archivo desplegado
            Archivo = getAplData(strDirectorio + objSessionParams.strAppFilename)

            'Save Data
            If createDeployedFileGroupAndroid(Archivo) Then
                'New Application Group Created OK
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Archivo desplegado. Datos[ " & getFormDataLog() & " ]", "")

                pnlMsg.Visible = True
                pnlError.Visible = False
                lblMsg.Text = "Archivo de promociones desplegado correctamente."

                'Set Invisible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
            End If

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub
    'Lectura de Archivo Promociones, Almacenando su valor en un string
    <Obsolete>
    Private Function getAplData(ByVal filePath As String) As String
        Dim fileReader As System.IO.FileStream = Nothing
        Dim sLine As String = ""
        Dim AplData As New ArrayList()
        Dim bytes As Byte() = IO.File.ReadAllBytes(ConfigurationSettings.AppSettings.Get("TempFolder") + objSessionParams.strAppFilename)
        Dim hex As String() = Array.ConvertAll(bytes, Function(b) b.ToString("X2"))
        sLine = String.Join("", hex)
        Return sLine
    End Function
    ''' <summary>
    ''' Crear Archivo Desplegado en el grupo
    ''' </summary>
    Public Function createDeployedFileGroupAndroid(ByVal contenido As String) As Boolean
        Dim connection As New SqlConnection(strConnectionString)
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As String = ""
        Dim retVal As Boolean

        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webEditar_promociones"
            command.Parameters.Clear()
            command.Parameters.Add(New SqlParameter("Tid", txtTIDIni.Text))
            command.Parameters.Add(New SqlParameter("AplData", contenido))
            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetString(0)
                End While
            Else
                retVal = False
            End If

            If status = "TID NO EXISTE" Then
                retVal = False
            ElseIf status = "ERROR DE EDICION" Then
                retVal = False
            ElseIf status = "EDICION COMPLETA" Then
                retVal = True
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try

        Return retVal

    End Function
End Class
