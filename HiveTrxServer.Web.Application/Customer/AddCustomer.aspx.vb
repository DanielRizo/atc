﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users
Imports System.Net
Imports TeleLoader.Customers

Partial Class Customer_AddCustomer
    Inherits TeleLoader.Web.BasePage

    Dim customerObj As Customer
    Dim contactObj As TeleLoader.Contact

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Save")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        customerObj = New Customer(strConnectionString)

        'Establecer Datos de Cliente
        customerObj.Name = txtCustomerName.Text
        customerObj.Desc = txtDesc.Text
        customerObj.IpAddress = txtIP.Text
        customerObj.Port = txtPort.Text

        customerObj.LearningMode = customerFlagLearningMode.Value

        'Validar si es necesario el contacto
        contactObj = New TeleLoader.Contact()

        If (txtContactNumId.Text.Trim.Length > 0) Then
            contactObj.IdentificationNumber = txtContactNumId.Text.Trim
            contactObj.IdentificationType = ddlContactIdType.SelectedValue
            contactObj.Name = txtContactName.Text.Trim
            contactObj.Address = txtContactAddress.Text.Trim
            contactObj.Mobile = txtContactMobile.Text.Trim
            contactObj.Phone = txtContactPhone.Text.Trim
            contactObj.Email = txtContactEmail.Text.Trim
        Else
            contactObj.IdentificationNumber = ""
            contactObj.IdentificationType = -1
            contactObj.Name = ""
            contactObj.Address = ""
            contactObj.Mobile = ""
            contactObj.Phone = ""
            contactObj.Email = ""
        End If

        customerObj.Contact = contactObj

        'Save Data
        If customerObj.createCustomer() Then
            'New Customer Created OK
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Cliente Creado. Datos[ " & getFormDataLog() & " ]", "")

            pnlMsg.Visible = True
            pnlError.Visible = False
            lblMsg.Text = "Cliente creado correctamente."
            setSwitchLearningMode(customerObj.LearningMode)
            clearForm()
        Else
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Creando Cliente. Datos[ " & getFormDataLog() & " ]", "")
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error creando cliente. Por favor valide los datos."
        End If

    End Sub

    Private Sub clearForm()

        txtContactNumId.Enabled = True
        ddlContactIdType.Enabled = False
        txtContactName.Enabled = False
        txtContactAddress.Enabled = False
        txtContactMobile.Enabled = False
        txtContactPhone.Enabled = False
        txtContactEmail.Enabled = False

        txtCustomerName.Text = ""
        txtIP.Text = ""
        txtPort.Text = ""
        txtDesc.Text = ""
        txtContactNumId.Text = ""
        ddlContactIdType.SelectedValue = -1
        txtContactName.Text = ""
        txtContactAddress.Text = ""
        txtContactMobile.Text = ""
        txtContactPhone.Text = ""
        txtContactEmail.Text = ""

        customerFlagLearningMode.Value = 0

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then
            customerFlagLearningMode.Value = 0
            setSwitchLearningMode(0)
            txtCustomerName.Focus()
        Else
            customerFlagLearningMode.Value = 0
            setSwitchLearningMode(0)
        End If
    End Sub

    Protected Sub btnQueryContactData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQueryContactData.Click

        'Consultar Datos de contacto existentes
        If txtContactNumId.Text.Length < 8 Then
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Ingrese un número de identificación válido."
            Exit Sub
        End If

        'Buscar en BD
        getContactData()

        'Set Toggle Visible
        ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"

    End Sub

    Function changeNonStandardChars(ByVal data As String) As String
        Dim retVal As String = ""

        data = data.ToLower().Replace("á", "a")
        data = data.ToLower().Replace("é", "e")
        data = data.ToLower().Replace("í", "i")
        data = data.ToLower().Replace("ó", "o")
        data = data.ToLower().Replace("ú", "u")
        data = data.ToLower().Replace("ñ", "n")

        retVal = data

        Return retVal
    End Function

    Private Sub getContactData()
        Dim connection As New SqlConnection(ConnectionString())
        Dim command As New SqlCommand()
        Dim results As SqlDataReader

        Try
            'Abrir Conexion
            connection.Open()

            'Buscar Datos de Contacto
            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webConsultarDatosContacto"
            command.Parameters.Clear()
            command.Parameters.Add(New SqlParameter("contactNumID", txtContactNumId.Text.Trim))

            'Ejecutar SP
            results = command.ExecuteReader()

            If results.HasRows Then
                While results.Read()
                    txtContactNumId.Text = results.GetString(0)
                    ddlContactIdType.SelectedValue = results.GetInt64(1)
                    txtContactName.Text = results.GetString(2)
                    txtContactAddress.Text = results.GetString(3)
                    txtContactMobile.Text = results.GetString(4)
                    txtContactPhone.Text = results.GetString(5)
                    txtContactEmail.Text = results.GetString(6)

                    txtContactNumId.Enabled = False
                    ddlContactIdType.Enabled = False
                    txtContactName.Enabled = False
                    txtContactAddress.Enabled = False
                    txtContactMobile.Enabled = False
                    txtContactPhone.Enabled = False
                    txtContactEmail.Enabled = False

                End While
            Else
                txtContactNumId.Enabled = True
                ddlContactIdType.Enabled = True
                txtContactName.Enabled = True
                txtContactAddress.Enabled = True
                txtContactMobile.Enabled = True
                txtContactPhone.Enabled = True
                txtContactEmail.Enabled = True
            End If

            connection.Close()
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub

    Protected Sub btnDeletecontactData_Click(sender As Object, e As EventArgs) Handles btnDeletecontactData.Click

        txtContactNumId.Text = ""
        ddlContactIdType.SelectedValue = -1
        txtContactName.Text = ""
        txtContactAddress.Text = ""
        txtContactMobile.Text = ""
        txtContactPhone.Text = ""
        txtContactEmail.Text = ""

        txtContactNumId.Enabled = True
        ddlContactIdType.Enabled = False
        txtContactName.Enabled = False
        txtContactAddress.Enabled = False
        txtContactMobile.Enabled = False
        txtContactPhone.Enabled = False
        txtContactEmail.Enabled = False

        txtContactNumId.Focus()

        ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"

    End Sub

    Private Sub setSwitchLearningMode(flagLearningMode As Boolean)
        Dim htmlSwitch As String

        If flagLearningMode = True Then
            htmlSwitch = "<p class='field switch' id='switchLearningMode'><label for='radio11' id='radio21' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio12' id='radio22' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='switchLearningMode'><label for='radio11' id='radio21' class='cb-enable'><span>S&iacute;</span></label><label for='radio12' id='radio22' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrLearningMode.Text = htmlSwitch

    End Sub


End Class
