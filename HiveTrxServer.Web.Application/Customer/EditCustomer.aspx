﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="EditCustomer.aspx.vb" Inherits="Customer_EditCustomer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%>:: Agregar Usuario
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkCustomer" runat="server" CssClass="fixed"
            PostBackUrl="~/Customer/Manager.aspx">Clientes</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkListCustomers" runat="server" CssClass="fixed"
            PostBackUrl="~/Customer/ListCustomers.aspx">Listar Clientes</asp:LinkButton>
    </li>
    <li>Editar Cliente</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    Editar Cliente
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlError" runat="server" Visible="False">
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <div class="titleh">
            <h3>Datos del Cliente</h3>
        </div>
        <div class="body">

            <div class="st-form-line">
                <span class="st-labeltext">Nombre: (*)</span>
                <asp:TextBox ID="txtCustomerName" CssClass="st-forminput" Style="width: 510px"
                    runat="server" TabIndex="1" MaxLength="50"
                    ToolTip="Digite nombre del cliente." onkeydown="return (event.keyCode!=13);"></asp:TextBox> 
                <div class="clear"></div>
            </div>

            <div class="st-form-line">
                <span class="st-labeltext">IP de Descarga: (*)</span>
                <asp:TextBox ID="txtIP" CssClass="st-forminput" Style="width: 100px"
                    runat="server" TabIndex="2" MaxLength="15"
                    ToolTip="Digite IP para descarga remota." onkeydown="return (event.keyCode!=13);"></asp:TextBox> 
                <div class="clear"></div>
            </div>

            <div class="st-form-line">
                <span class="st-labeltext">Puerto de Descarga: (*)</span>
                <asp:TextBox ID="txtPort" CssClass="st-forminput" Style="width: 60px"
                    runat="server" TabIndex="3" MaxLength="5"
                    ToolTip="Digite Puerto TCP para descarga remota." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>

            <div class="st-form-line">	

                <span class="st-labeltext">Modo aprendizaje?:</span>	                

                <asp:Literal ID="ltrLearningMode" runat="server"></asp:Literal>
                
                <asp:HiddenField ID="customerFlagLearningMode" runat="server" Value="0" />

                <div class="clear"></div>

               <blockquote>El modo aprendizaje funcionara si existe un grupo INVENTARIO.</blockquote>

            </div>

            <div class="st-form-line">
                <span class="st-labeltext">Descripci&oacute;n:</span>
                <asp:TextBox ID="txtDesc" CssClass="st-forminput" Style="width: 510px"
                    runat="server" TabIndex="4" MaxLength="250" TextMode="MultiLine"
                    ToolTip="Digite descripción del cliente." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>

            <div class="st-form-line">	
                <span class="st-labeltext">Estado del Cliente:</span>	                

                <asp:Literal ID="ltrSwitch" runat="server"></asp:Literal>
                
                <asp:HiddenField ID="customerStatus" runat="server" />

                <div class="clear"></div>
            </div>

            <div class="toggle-message" style="z-index: 590; top: 0px; left: 0px;">
                <h3 class="title">Datos de Contacto (Opcional)
                    <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
                <div class="hide-message" id="hide-message" style="display: none;">

                    <div class="st-form-line">
                        <span class="st-labeltext">N&uacute;mero de Identificaci&oacute;n:</span>
                        <asp:TextBox ID="txtContactNumId" CssClass="st-forminput" Style="width: 100px"
                            runat="server" TabIndex="5" MaxLength="14"
                            ToolTip="Digite número de identificación." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnQueryContactData" runat="server" Text="Consultar Datos Existentes"
                            CssClass="button-aqua" TabIndex="6" />
                        &nbsp;&nbsp;
                        <asp:Button ID="btnDeletecontactData" runat="server" Text="Limpiar"
                            CssClass="st-button" TabIndex="7" />
                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">
                        <span class="st-labeltext">Tipo de Identificaci&oacute;n:</span>
                        <asp:DropDownList ID="ddlContactIdType" class="uniform" runat="server"
                            DataSourceID="dsContactIdType" DataTextField="descripcion"
                            DataValueField="id" Width="200px"
                            ToolTip="Seleccione el tipo de identificación." TabIndex="8" Enabled="False">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="dsContactIdType" runat="server"
                            ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" SelectCommand="SELECT -1 AS id, '       ' AS descripcion
                            UNION ALL
                            SELECT * FROM TipoIdentificacion"></asp:SqlDataSource>
                        <div class="clear"></div>
                    </div>
            
                    <div class="st-form-line">
                        <span class="st-labeltext">Nombre:</span>
                        <asp:TextBox ID="txtContactName" CssClass="st-forminput" Style="width: 510px"
                            runat="server" TabIndex="9" MaxLength="100"
                            ToolTip="Digite nombre del contacto." onkeydown="return (event.keyCode!=13);" Enabled="False"></asp:TextBox>
                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">
                        <span class="st-labeltext">Direcci&oacute;n:</span>
                        <asp:TextBox ID="txtContactAddress" CssClass="st-forminput" Style="width: 510px"
                            runat="server" TabIndex="10" MaxLength="100"
                            ToolTip="Digite dirección del contacto." onkeydown="return (event.keyCode!=13);" Enabled="False"></asp:TextBox>
                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">
                        <span class="st-labeltext">Celular:</span>
                        <asp:TextBox ID="txtContactMobile" CssClass="st-forminput" Style="width: 100px"
                            runat="server" TabIndex="11" MaxLength="20"
                            ToolTip="Digite celular del contacto." onkeydown="return (event.keyCode!=13);" Enabled="False"></asp:TextBox>
                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">
                        <span class="st-labeltext">Tel&eacute;fono Fijo:</span>
                        <asp:TextBox ID="txtContactPhone" CssClass="st-forminput" Style="width: 100px"
                            runat="server" TabIndex="12" MaxLength="20"
                            ToolTip="Digite teléfono fijo del contacto." onkeydown="return (event.keyCode!=13);" Enabled="False"></asp:TextBox>
                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">
                        <span class="st-labeltext">Correo Electr&oacute;nico:</span>
                        <asp:TextBox ID="txtContactEmail" CssClass="st-forminput" Style="width: 510px"
                            runat="server" TabIndex="13" MaxLength="100"
                            ToolTip="Digite correo electrónico del usuario." onkeydown="return (event.keyCode!=13);" Enabled="False"></asp:TextBox>
                        <div class="clear"></div>
                    </div>

                </div>
            </div>

            <div class="button-box">
                <asp:Button ID="btnEdit" runat="server" Text="Editar Cliente"
                    CssClass="button-aqua" TabIndex="14" OnClientClick="return validateEditCustomer()" />
            </div>

        </div>

    </div>

    <asp:HyperLink ID="lnkBack" runat="server" NavigateUrl="~/Customer/ListCustomers.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <!-- Validator -->
    <script src="../js/ValidatorCustomer.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

</asp:Content>

