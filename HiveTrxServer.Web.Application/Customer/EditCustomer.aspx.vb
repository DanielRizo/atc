﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users
Imports System.Net
Imports TeleLoader.Customers

Partial Class Customer_EditCustomer
    Inherits TeleLoader.Web.BasePage

    Dim customerObj As Customer
    Dim contactObj As TeleLoader.Contact

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click

        Dim msg As String = ""
        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Update")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        customerObj = New Customer(strConnectionString, objSessionParams.intSelectedCustomer)

        'Establecer Datos de Contacto
        customerObj.Name = txtCustomerName.Text
        customerObj.Desc = txtDesc.Text
        customerObj.IpAddress = txtIP.Text
        customerObj.Port = txtPort.Text
        customerObj.Status = customerStatus.Value
        customerObj.LearningMode = customerFlagLearningMode.Value

        'Validar si es necesario el contacto
        contactObj = New TeleLoader.Contact()

        If (txtContactNumId.Text.Trim.Length > 0) Then
            contactObj.IdentificationNumber = txtContactNumId.Text.Trim
            contactObj.IdentificationType = ddlContactIdType.SelectedValue
            contactObj.Name = txtContactName.Text.Trim
            contactObj.Address = txtContactAddress.Text.Trim
            contactObj.Mobile = txtContactMobile.Text.Trim
            contactObj.Phone = txtContactPhone.Text.Trim
            contactObj.Email = txtContactEmail.Text.Trim
        Else
            contactObj.IdentificationNumber = ""
            contactObj.IdentificationType = -1
            contactObj.Name = ""
            contactObj.Address = ""
            contactObj.Mobile = ""
            contactObj.Phone = ""
            contactObj.Email = ""
        End If

        customerObj.Contact = contactObj

        'Save Data
        If customerObj.editCustomer(msg) Then
            'New Customer Created OK
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Update", "Usuario Creado. Datos[ " & getFormDataLog() & " ]", "")

            pnlMsg.Visible = True
            pnlError.Visible = False
            'lblMsg.Text = "Cliente editado correctamente."
            lblMsg.Text = msg

            setSwitchStatus(customerObj.Status)
            setSwitchLearningMode(customerObj.LearningMode)

        Else
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Update", "Error Editando Usuario. Datos[ " & getFormDataLog() & " ]", "")
            pnlError.Visible = True
            pnlMsg.Visible = False
            'lblError.Text = "Error editando cliente. Por favor valide los datos."
            lblError.Text = msg
        End If

    End Sub

    Private Sub setSwitchStatus(status As Integer)
        Dim htmlSwitch As String

        If status = 1 Then
            htmlSwitch = "<p class='field switch' id='switchStatus'><label for='radio1' id='radio11' class='cb-enable selected'><span>Activo</span></label><label for='radio2' id='radio12' class='cb-disable'><span>Inactivo</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='switchStatus'><label for='radio1' id='radio11' class='cb-enable'><span>Activo</span></label><label for='radio2' id='radio12' class='cb-disable selected'><span>Inactivo</span></label></p>"
        End If
        ltrSwitch.Text = htmlSwitch

        ltrScript.Text = "<script language='javascript' type='text/javascript'> $(function() { $('#radio11').click(function() { $('#ctl00_MainContent_customerStatus').attr('value', '1'); }); $('#radio12').click(function() { $('#ctl00_MainContent_customerStatus').attr('value', '2'); }); });</script>"
    End Sub

    Private Sub setSwitchLearningMode(flagLearningMode As Boolean)
        Dim htmlSwitch As String

        If flagLearningMode = True Then
            htmlSwitch = "<p class='field switch' id='switchLearningMode'><label for='radio11' id='radio21' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio12' id='radio22' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='switchLearningMode'><label for='radio11' id='radio21' class='cb-enable'><span>S&iacute;</span></label><label for='radio12' id='radio22' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrLearningMode.Text = htmlSwitch

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            customerObj = New Customer(strConnectionString, objSessionParams.intSelectedCustomer)
            'Leer datos BD
            customerObj.getCustomerData()

            txtCustomerName.Text = customerObj.Name
            txtDesc.Text = customerObj.Desc
            txtIP.Text = customerObj.IpAddress
            txtPort.Text = customerObj.Port
            customerStatus.Value = customerObj.Status
            setSwitchStatus(customerObj.Status)

            customerFlagLearningMode.Value = customerObj.LearningMode
            setSwitchLearningMode(customerObj.LearningMode)

            If customerObj.Contact.IdentificationNumber.Length > 0 Then
                txtContactNumId.Text = customerObj.Contact.IdentificationNumber
                ddlContactIdType.SelectedValue = customerObj.Contact.IdentificationType
                txtContactName.Text = customerObj.Contact.Name
                txtContactAddress.Text = customerObj.Contact.Address
                txtContactMobile.Text = customerObj.Contact.Mobile
                txtContactPhone.Text = customerObj.Contact.Phone
                txtContactEmail.Text = customerObj.Contact.Email

                txtContactNumId.Enabled = True
                ddlContactIdType.Enabled = True
                txtContactName.Enabled = True
                txtContactAddress.Enabled = True
                txtContactMobile.Enabled = True
                txtContactPhone.Enabled = True
                txtContactEmail.Enabled = True

                'Set Toggle Visible
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"

            End If

            txtCustomerName.Focus()
        End If
    End Sub

    Protected Sub btnQueryContactData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQueryContactData.Click

        'Consultar Datos de contacto existentes
        If txtContactNumId.Text.Length < 8 Then
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Ingrese un número de identificación válido."

            'Set Toggle Visible
            ltrScript.Text &= "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"

            Exit Sub
        End If

        'Buscar en BD
        getContactData()

        'Set Toggle Visible
        ltrScript.Text &= "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"

    End Sub

    Private Sub getContactData()
        Dim connection As New SqlConnection(ConnectionString())
        Dim command As New SqlCommand()
        Dim results As SqlDataReader

        Try
            'Abrir Conexion
            connection.Open()

            'Buscar Datos de Contacto
            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webConsultarDatosContacto"
            command.Parameters.Clear()
            command.Parameters.Add(New SqlParameter("contactNumID", txtContactNumId.Text.Trim))

            'Ejecutar SP
            results = command.ExecuteReader()

            If results.HasRows Then
                While results.Read()
                    txtContactNumId.Text = results.GetString(0)
                    ddlContactIdType.SelectedValue = results.GetInt64(1)
                    txtContactName.Text = results.GetString(2)
                    txtContactAddress.Text = results.GetString(3)
                    txtContactMobile.Text = results.GetString(4)
                    txtContactPhone.Text = results.GetString(5)
                    txtContactEmail.Text = results.GetString(6)
                End While
            Else
                txtContactNumId.Enabled = True
                ddlContactIdType.Enabled = True
                txtContactName.Enabled = True
                txtContactAddress.Enabled = True
                txtContactMobile.Enabled = True
                txtContactPhone.Enabled = True
                txtContactEmail.Enabled = True
            End If

            connection.Close()
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub

    Protected Sub btnDeletecontactData_Click(sender As Object, e As EventArgs) Handles btnDeletecontactData.Click

        txtContactNumId.Text = ""
        ddlContactIdType.SelectedValue = -1
        txtContactName.Text = ""
        txtContactAddress.Text = ""
        txtContactMobile.Text = ""
        txtContactPhone.Text = ""
        txtContactEmail.Text = ""

        txtContactNumId.Enabled = True
        ddlContactIdType.Enabled = True
        txtContactName.Enabled = True
        txtContactAddress.Enabled = True
        txtContactMobile.Enabled = True
        txtContactPhone.Enabled = True
        txtContactEmail.Enabled = True
        txtContactNumId.Focus()

        'Set Toggle Visible
        ltrScript.Text &= "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"

    End Sub
End Class
