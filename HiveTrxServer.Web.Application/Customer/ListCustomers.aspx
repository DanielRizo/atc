﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ListCustomers.aspx.vb" Inherits="Customer_ListCustomers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Listar Usuarios
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkCustomer" runat="server" CssClass="fixed" 
            PostBackUrl="~/Customer/Manager.aspx">Clientes</asp:LinkButton>
    </li>
    <li>Listar Clientes</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Listar Clientes
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite ver el listado de clientes del sistema Polaris:</p>
    
    <br />
    <div class="toggle-message" style="z-index: 590; top: 0px; left: 0px;">
        <h3 class="title">Filtro de B&uacute;squeda...
            <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
        <div class="hide-message" id="hide-message" style="display: none;">
            <div class="st-form-line">
                <span class="st-labeltext"><b>Nombre: </b></span>
                <asp:TextBox ID="txtNombre" CssClass="st-forminput"
                    runat="server" TabIndex="1" MaxLength="100"
                    ToolTip="Nombre del Cliente" onkeydown="return (event.keyCode!=13);" Width="250px"></asp:TextBox>
            </div>
            <div class="st-form-line">
                <span class="st-labeltext"><b>Descripci&oacute;n: </b></span>
                <asp:TextBox ID="txtDesc" CssClass="st-forminput"
                    runat="server" TabIndex="2" MaxLength="50"
                    ToolTip="Descripción del cliente" onkeydown="return (event.keyCode!=13);" Width="250px"></asp:TextBox>
            </div>
            <div class="button-box">
                <asp:Button ID="btnConsultar" runat="server" Text="Filtrar"
                    CssClass="button-aqua" TabIndex="3" ToolTip="Filtrar Clientes..." />
            </div>
        </div>
    </div>

    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
    
        <asp:GridView ID="grdCustomers" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
            DataSourceID="dsListCustomers" ForeColor="#333333" Width="100%" 
            CssClass="mGrid" DataKeyNames="cli_id,cli_nombre" 
            EmptyDataText="No existen Clientes creados  o con el filtro aplicado.">
            <RowStyle BackColor="White" ForeColor="White" />
            <Columns>
                <asp:BoundField DataField="cli_nombre" HeaderText="Nombre del Cliente" 
                    SortExpression="cli_nombre" />
                <asp:BoundField DataField="cli_descripcion" HeaderText="Descripción" 
                    SortExpression="cli_descripcion" />
                <asp:BoundField DataField="cli_ip_descarga" HeaderText="IP Descarga" 
                    SortExpression="cli_ip_descarga" />
                <asp:BoundField DataField="cli_puerto_descarga" HeaderText="Puerto Descarga" 
                    SortExpression="cli_puerto_descarga" />
                <asp:TemplateField HeaderText="Estado" SortExpression="usu_estado">
                    <ItemTemplate>
                        <asp:CheckBox ID="editChk" runat="server" Checked='<%# Bind("cli_estado")%>' Enabled="false" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>

                <asp:TemplateField ShowHeader="False" HeaderStyle-Width="50px">
                    <ItemTemplate>
                        <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" 
                            CommandName="EditCustomer" ImageUrl="~/img/icons/16x16/edit.png" Text="Editar" 
                            ToolTip="Editar Cliente" CommandArgument='<%# grdCustomers.Rows.Count%>' style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />
                        <asp:ImageButton ID="imgLicense" runat="server" CausesValidation="False" 
                            CommandName="ManageLicense" ImageUrl="~/img/icons/16x16/license.png" Text="Administrar Licencias" 
                            ToolTip="Administrar Licencias del Cliente" CommandArgument='<%# grdCustomers.Rows.Count%>' style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />
                    </ItemTemplate>
                </asp:TemplateField>
                
            </Columns>
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
                CssClass="pgr" Font-Underline="False" />
            <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#999999" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>
    
	    <asp:SqlDataSource ID="dsListCustomers" runat="server" 
            ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" 
            SelectCommand="sp_webConsultarClientes" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:Parameter Name="customerName" Type="String" DefaultValue="-1" />
                <asp:Parameter Name="customerDesc" Type="String" DefaultValue="-1" />
            </SelectParameters>
	    </asp:SqlDataSource>

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">     
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel> 

    </div>
   
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Customer/Manager.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

