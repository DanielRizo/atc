﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users

Partial Class Customer_ListCustomers
    Inherits TeleLoader.Web.BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtNombre.Text = ""
            txtDesc.Text = ""
            txtNombre.Focus()
        End If
    End Sub

    Protected Sub grdCustomers_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdCustomers.RowCommand
        Try

            Select Case e.CommandName

                Case "EditCustomer"
                    grdCustomers.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    objSessionParams.intSelectedCustomer = grdCustomers.SelectedDataKey.Values.Item("cli_id")

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("EditCustomer.aspx", False)

                Case "ManageLicense"
                    grdCustomers.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    objSessionParams.intSelectedCustomer = grdCustomers.SelectedDataKey.Values.Item("cli_id")
                    objSessionParams.strSelectedCustomer = grdCustomers.SelectedDataKey.Values.Item("cli_nombre")

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("ManageLicenseCustomer.aspx", False)

                Case Else
                    objSessionParams.intSelectedCustomer = -1

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    grdCustomers.SelectedIndex = -1

            End Select

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub

    Protected Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click

        If txtNombre.Text <> "" Then
            dsListCustomers.SelectParameters("customerName").DefaultValue = txtNombre.Text
        Else
            dsListCustomers.SelectParameters("customerName").DefaultValue = "-1"
        End If

        If txtDesc.Text <> "" Then
            dsListCustomers.SelectParameters("customerDesc").DefaultValue = txtDesc.Text
        Else
            dsListCustomers.SelectParameters("customerDesc").DefaultValue = "-1"
        End If

        grdCustomers.DataBind()

        If txtNombre.Text = "" And txtDesc.Text = "" Then
            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
        Else
            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
        End If

    End Sub
End Class
