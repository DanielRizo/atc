﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ManageLicenseCustomer.aspx.vb" Inherits="Customer_ManageLicenseCustomer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Administrar Licencias del Cliente
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   

	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script>     
        
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkCustomer" runat="server" CssClass="fixed"
            PostBackUrl="~/Customer/Manager.aspx">Clientes</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkListCustomers" runat="server" CssClass="fixed"
            PostBackUrl="~/Customer/ListCustomers.aspx">Listar Clientes</asp:LinkButton>
    </li>
    <li>Administrar Licencias de Uso</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    Licencias del Cliente: [ <%= objSessionParams.strSelectedCustomer %> ]
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite administrar las Licencias del Cliente:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>   
            
        <asp:Panel ID="pnlError" runat="server" Visible="False">    
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>  

        <div class="toggle-message" style="z-index: 590;">
            <h3 class="title">Agregar Licencia...
                <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
            <div class="hide-message" id="hide-message" style="display: none;">
        
                <div class="st-form-line">
                    <span class="st-labeltext">Tipo de Licencia:</span>
                    <asp:DropDownList ID="ddlLicenseType" class="uniform" runat="server"
                        DataSourceID="dsLicenseType" DataTextField="descripcion"
                        DataValueField="id" Width="200px"
                        ToolTip="Seleccione el tipo de licencia." TabIndex="1" AutoPostBack="True">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="dsLicenseType" runat="server"
                        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" SelectCommand="SELECT -1 AS id, '       ' AS descripcion
                        UNION ALL
                        SELECT id, descripcion FROM TipoLicencia"></asp:SqlDataSource>
                    <div class="clear"></div>
                </div>

                <div class="st-form-line">
                    <span class="st-labeltext">License Key:</span>
                    <asp:TextBox ID="txtLicencia" CssClass="st-success-input" Style="width: 510px"
                        runat="server" TabIndex="2" MaxLength="100"
                        ToolTip="Serial de Licencia Generada." Enabled="False" onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>
               <%-- <div class="st-form-line">
                <span class="st-labeltext">Cantidad De Terminales Pstis: (*)</span>
                <asp:TextBox ID="txtTeminales" CssClass="st-forminput" Style="width: 510px"
                    runat="server" TabIndex="3" MaxLength="100"
                    ToolTip="Digite la cantidad de terminales." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
                </div>
                <div class="st-form-line">
                <span class="st-labeltext">Cantidad De Terminales MDM: (*)</span>
                <asp:TextBox ID="TxtTerminalesMdm" CssClass="st-forminput" Style="width: 510px"
                    runat="server" TabIndex="4" MaxLength="100"
                    ToolTip="Digite la cantidad de terminales." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
                </div>
                 <div class="st-form-line">
                <span class="st-labeltext">Cantidad De Terminales Descarga Remota: (*)</span>
                <asp:TextBox ID="TxtTerminalesDescargaRemota" CssClass="st-forminput" Style="width: 510px"
                    runat="server" TabIndex="5" MaxLength="100"
                    ToolTip="Digite la cantidad de terminales." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
                </div>--%>
                <div class="button-box">
                    <asp:Button ID="btnAddLicense" runat="server" Text="Adicionar Licencia" 
                        CssClass="button-aqua" TabIndex="3" OnClientClick="return validateAddLicense();" />
                </div>            
            </div>
        </div>

    </div>
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
	    <div class="titleh">
    	    <h3>Licencias Creadas para el Cliente</h3>
        </div>
        <div class="body">    
            <br />            
            <asp:GridView ID="grdLicenses" runat="server" AllowPaging="True" 
                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                DataSourceID="dsLicenses" ForeColor="#333333"
                CssClass="mGridCenter" DataKeyNames="lic_id" 
                EmptyDataText="No existen Licencias creadas para el Cliente." 
                HorizontalAlign="Center">
                <RowStyle BackColor="White" ForeColor="White" />
                <Columns>

                    <asp:BoundField DataField="lic_fecha_creacion" HeaderText="Fecha Creaci&oacute;n" 
                        SortExpression="lic_fecha_creacion" />
                    <asp:BoundField DataField="lic_fecha_expiracion" HeaderText="Fecha Expiraci&oacute;n" 
                        SortExpression="lic_fecha_expiracion" />
                    <asp:BoundField DataField="tipo_licencia" HeaderText="Tipo de Licencia" 
                        SortExpression="tipo_licencia" />
                    <asp:BoundField DataField="tipo_estado" HeaderText="Estado Licencia" 
                        SortExpression="tipo_estado" />
                    <asp:BoundField DataField="lic_key" HeaderText="License Key" 
                        SortExpression="lic_key" />
                      <%-- <asp:BoundField DataField="lic_Terminales" HeaderText="Cantidad de Terminales Pstis" ItemStyle-HorizontalAlign ="Center" 
                        SortExpression="lic_Terminales" />
                     <asp:BoundField DataField="lic_Terminales_Android" HeaderText="Cantidad de Terminales MDM" ItemStyle-HorizontalAlign ="Center" 
                        SortExpression="lic_Terminales_Android" />
                     <asp:BoundField DataField="lic_Terminales_Descarga_Remota" HeaderText="Cantidad de Terminales Descarga Remota" ItemStyle-HorizontalAlign ="Center" 
                        SortExpression="lic_Terminales_Descarga_Remota" />--%>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>                        
                            <asp:ImageButton ID="imgExpired" runat="server" CausesValidation="False" 
                                CommandName="Delete" ImageUrl="~/img/icons/16x16/expired.png" Text="Terminar Validez Licencia" 
                                ToolTip="Terminar Validez Licencia" CommandArgument='<%# grdLicenses.Rows.Count%>' style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
                    CssClass="pgr" Font-Underline="False" />
                <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid" 
                    BorderWidth="1px" />
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>            
            <br />
	        <asp:SqlDataSource ID="dsLicenses" runat="server" 
                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"                 
                SelectCommand="sp_webConsultarLicenciasCliente" SelectCommandType="StoredProcedure"
                DeleteCommand="sp_webExpirarLicenciaXCliente" DeleteCommandType="StoredProcedure" ConflictDetection="OverwriteChanges" >
                <SelectParameters>
                    <asp:Parameter Name="customerId" Type="String" />
                </SelectParameters>
                <DeleteParameters>
                    <asp:Parameter Name="licenseId" Type="String" />
                    <asp:Parameter Name="customerId" Type="String" />
                </DeleteParameters>
            </asp:SqlDataSource> 
        </div>
    </div>
    
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Customer/ListCustomers.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">
    
    <!-- Validator -->
    <script src="../js/ValidatorCustomer.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

