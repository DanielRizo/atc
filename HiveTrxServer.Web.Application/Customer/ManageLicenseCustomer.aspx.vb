﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Licenses
Imports System.Data.Common

Partial Class Customer_ManageLicenseCustomer
    Inherits TeleLoader.Web.BasePage

    Dim licenseObj As License

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            ddlLicenseType.Focus()

            dsLicenses.SelectParameters("customerId").DefaultValue = objSessionParams.intSelectedCustomer

            grdLicenses.DataBind()

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub grdLicenses_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdLicenses.RowCommand
        Try

            Select Case e.CommandName

                Case "Delete"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    grdLicenses.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    dsLicenses.DeleteParameters("customerId").DefaultValue = objSessionParams.intSelectedCustomer
                    dsLicenses.DeleteParameters("licenseId").DefaultValue = grdLicenses.SelectedDataKey.Values.Item("lic_id")

                    Dim strData As String = "Licencia ID: " & grdLicenses.SelectedDataKey.Values.Item("lic_id") & ", Cliente ID: " & objSessionParams.intSelectedCustomer

                    dsLicenses.Delete()

                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Licencia expirada para el Cliente. Datos[ " & strData & " ]", "")

                    grdLicenses.DataBind()

                    pnlError.Visible = False
                    pnlMsg.Visible = True
                    lblMsg.Text = "Licencia expirada para el Cliente"

                    grdLicenses.SelectedIndex = -1

            End Select

            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub

    Protected Sub btnAddLicense_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddLicense.Click

        Try
            licenseObj = New License(strConnectionString, objSessionParams.intSelectedCustomer)

            'Establecer Datos de la Licencia
            licenseObj.LicenseType = ddlLicenseType.SelectedValue
            licenseObj.LicenseKey = txtLicencia.Text
            'licenseObj.CntTerminales = txtTeminales.Text
            'licenseObj.CntTerminalesMdm = TxtTerminalesMdm.Text
            'licenseObj.CntTerminalesDr = TxtTerminalesDescargaRemota.Text

            'Save Data
            If licenseObj.createLicenseCustomer() Then
                'New License Created OK
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Licencia Creada para el cliente. Datos[ " & getFormDataLog() & " ]", "")

                pnlMsg.Visible = True
                pnlError.Visible = False
                lblMsg.Text = "Licencia creada correctamente."

                dsLicenses.SelectParameters("customerId").DefaultValue = objSessionParams.intSelectedCustomer

                grdLicenses.DataBind()

            Else
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error creando Licencia en el cliente. Datos[ " & getFormDataLog() & " ]", "")
                pnlError.Visible = True
                pnlMsg.Visible = False
                lblError.Text = "Error creando licencia en el cliente, porque ya posee una activa."
            End If

            clearForm()

            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub

    Private Sub clearForm()
        ddlLicenseType.SelectedIndex = -1
        txtLicencia.Text = ""
    End Sub

    Protected Sub dsLicenses_Deleting(sender As Object, e As SqlDataSourceCommandEventArgs) Handles dsLicenses.Deleting

        'Remover parametro extra, igual al datakeyname
        If e.Command.Parameters.Count > 2 Then
            Dim paramAplId As DbParameter = e.Command.Parameters("@lic_id")
            e.Command.Parameters.Remove(paramAplId)
        End If

    End Sub

    Protected Sub ddlLicenseType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLicenseType.SelectedIndexChanged

        txtLicencia.Text = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(objSessionParams.intSelectedCustomer & "|" & objSessionParams.strSelectedCustomer & "|" & Now.ToString()))

        'Set Toggle Visible
        ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
    End Sub
End Class
