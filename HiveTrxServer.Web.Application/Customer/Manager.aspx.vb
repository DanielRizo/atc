﻿
Partial Class Customer_Manager
    Inherits TeleLoader.Web.BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            DrawOptions(repOpcion)
        End If
    End Sub

    Protected Sub lnkOpcionItem_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)
        Dim path As String = e.CommandArgument.ToString.Substring(e.CommandArgument.ToString.IndexOf("/") + 1)
        Response.Redirect(path)
    End Sub

End Class
