﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Security

Partial Class ErrorPage
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        On Error Resume Next

        Dim objAccessToken As AccessToken = Session("AccessToken")

        Me.LblAnio.Text = Now.Year
        lblUserName.Text = objAccessToken.DisplayName
        lblDateTime.Text = Now().ToString

        If Not Page.IsPostBack Then

            Dim exception As System.Exception = Session("Exception")

            If Not exception Is Nothing Then

                If Not exception.InnerException Is Nothing Then
                    lblTitle2.Text = "<b>Description: </b>" & exception.Message
                    lblDescription.Text = exception.InnerException.Message
                    lnkBack.Visible = False
                Else
                    lblTitle2.Text = "<b>Description: </b>" & exception.Message
                    lblDescription.Text = ""
                    lnkBack.Visible = True
                End If

                lblAdditionalInfo.Text = "<b>Source: </b>" & exception.Source & "<br /><br />"
                lblAdditionalInfo.Text &= "<b>Stack trace: </b>" & exception.StackTrace.Replace(vbCrLf, "<BR>") & vbCrLf

                Session.Remove("Exception")

                'Llamando al método true= https; false = http;
                Dim protocol As String = ConfigurationSettings.AppSettings.Get("Protocol").Trim().ToUpper()
                protocoloSeguro(protocol = "HTTPS")
            End If

        End If
    End Sub

    'Metodo de redireccionamiento..
    Public Sub protocoloSeguro(ByVal bSeguro As Boolean)
        Dim redirectUrl As String = Nothing
        If bSeguro AndAlso (Not Request.IsSecureConnection) Then
            redirectUrl = Request.Url.ToString().Replace("http:", "https:")
        Else
            If (Not bSeguro) AndAlso Request.IsSecureConnection Then
                redirectUrl = Request.Url.ToString().Replace("https:", "http:")
            End If
        End If

        If redirectUrl IsNot Nothing Then
            Response.Redirect(redirectUrl)
        End If
    End Sub

    Protected Sub lnkLogo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLogo.Click
        'Ir a Inicio
        Session("SelectedMenu") = ""
        Session("SelectedOption") = ""

        Response.Redirect("~/Index.aspx")
    End Sub
End Class

