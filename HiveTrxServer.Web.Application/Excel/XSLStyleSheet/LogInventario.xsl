﻿<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:decimal-format name="German" decimal-separator=',' grouping-separator='.'  NaN=''/>
	<xsl:template match="/">
		<HTML>
			<HEAD>
				<STYLE>
					.stdPVTblLCell {
					color: DarkBlue;
					font-weight: bold;
					text-align: left;
					padding-left: 4px;
					padding-top: 4px;
					padding-bottom: 4px;
					width: 70%;
					font-size: 12pt;
					}
					.stdPageHdr {
					color: DarkBlue;
					font-weight: bold;
					font-style:italic;
					font-family:Verdana;
					text-align: left;
					padding-left: 4px;
					padding-top: 4px;
					padding-bottom: 4px;
					width: 70%;
					font-size: 20pt;
					}
					.gridHeader {
					background-color: #C0C0C0;
					color: DarkBlue;
					font-size: 9pt;
					font-weight: bold;
					font-family:Verdana;
					vertical-align:middle;
					text-align:center;
					border: solid thin Black;
					}
					.SearchHeader {
					color: DarkBlue;
					font-size: 9pt;
					font-weight: bold;
					font-family:Verdana;
					}
					.SearchKey {
					color: DarkBlue;
					font-size: 9pt;
					vertical-align:middle;
					text-align:right;
					font-family:Verdana;
					}
					.SearchValue {
					color: Black;
					font-size: 9pt;
					font-weight: bold;
					vertical-align:middle;
					text-align:left;
					font-family:Verdana;
					}
					.SearchResultHeader {
					color: DarkBlue;
					font-size: 9pt;
					font-weight: bold;
					font-family:Verdana;
					}
					.SearchResultItem {
					background-color: #8FC9FF;
					color: Black;
					font-size: 8pt;
					font-family:Verdana;
					border: solid thin Black;
					}
					.SearchResultAltItem {
					background-color: #CCE6FF;
					color: Black;
					font-size: 8pt;
					font-family:Verdana;
					border: solid thin Black;
					}
				</STYLE>
			</HEAD>
			<BODY>
				<TABLE>
					<TR>
						<TD class="stdPageHdr" colspan="6">
							<xsl:value-of select="NewDataSet/HeaderDetails/ReportName"/>
						</TD>
					</TR>
					<TR  class="stdPVTblLCell">
						<TD>
							Encabezado
						</TD>
					</TR>
					<TR>
						<TD> </TD>
					</TR>
					<TR>
						<TD colspan="1" class="SearchKey">Grupo:</TD>
						<TD class="SearchValue" colspan="6">
							<xsl:value-of select="NewDataSet/HeaderDetails/Group"/>
						</TD>
					</TR>
					<TR>
						<TD colspan="1" class="SearchKey">Serial Terminal:</TD>
						<TD class="SearchValue" colspan="6">
							<xsl:value-of select="NewDataSet/HeaderDetails/Terminal"/>
						</TD>
					</TR>
					<TR>
						<TD colspan="1" class="SearchKey">Período:</TD>
						<TD class="SearchValue" colspan="6">
							<xsl:value-of select="NewDataSet/HeaderDetails/Period"/>
						</TD>
					</TR>
					<TR>
						<TD></TD>
					</TR>
					<TR class="SearchResultHeader">
						<TD colspan="2">Detalle del Reporte</TD>
					</TR>
					<TR>
						<TD> </TD>
					</TR>
					<TR>
						<TD class="gridHeader">
							Fecha
						</TD>
						<TD class="gridHeader">
							Cliente
						</TD>
						<TD class="gridHeader">
							Grupo
						</TD>
						<TD class="gridHeader">
							Serial Terminal
						</TD>
						<TD class="gridHeader">
							Terminal ID
						</TD>
						<TD class="gridHeader">
							Imei Terminal
						</TD>
						<TD class="gridHeader">
							Codigo de producto
						</TD>
						<TD class="gridHeader">
							Marca
						</TD>
						<TD class="gridHeader">
							Tipo
						</TD>
						<TD class="gridHeader">
							Código de Respuesta
						</TD>
						<TD class="gridHeader">
							Mensaje de Respuesta
						</TD>
						<TD class="gridHeader">
							Estado Actualización
						</TD>
						<TD class="gridHeader">
							Versión software POS
						</TD>
						<TD class="gridHeader">
							Aplicaciones POS
						</TD>
						<TD class="gridHeader">
							Aplicaciones a Instalar
						</TD>
						<TD class="gridHeader">
							Fecha de Descarga
						</TD>
						<TD class="gridHeader">
							IP/Puerto Descarga
						</TD>
					</TR>
					<xsl:for-each select="NewDataSet/Table">
						<xsl:choose>
							<xsl:when test="position() mod 2 = 1">
								<TR>
									<TD class="SearchResultItem" style="mso-number-format:\@; text-align: center;">
										<xsl:value-of select="tra_fecha"/>
									</TD>
									<TD class="SearchResultItem">
										<xsl:value-of select="tra_cliente_pos"/>
									</TD>
									<TD class="SearchResultItem">
										<xsl:value-of select="gru_nombre"/>
									</TD>
									<TD class="SearchResultItem" style="mso-number-format:\@; text-align: center;">
										<xsl:value-of select="tra_serial_pos"/>
									</TD>
									<TD class="SearchResultItem" style="mso-number-format:\@; text-align: center;">
										<xsl:value-of select="ter_terminalId"/>
									</TD>
									<!--  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="tra_message_type"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="tra_processing_code"/>
                  </TD>
                  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="tra_stan"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="tra_direccion_ip_pos"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="tra_direccion_ip_detectada"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="tra_puerto_tcp_atencion"/>
                  </TD> -->
									<TD class="SearchResultItem" style="mso-number-format:\@; text-align: right;">
										<xsl:value-of select="tra_imei_pos"/>
									</TD>
									<TD class="SearchResultItem" style="mso-number-format:\@; text-align: right;">
										<xsl:value-of select="tra_codigo_producto"/>
									</TD>

									<!--                  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: right;">
                    <xsl:value-of select="tra_sim_pos"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="tra_nivel_gprs"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="tra_nivel_bateria"/>
                  </TD> -->
									<TD class="SearchResultItem">
										<xsl:value-of select="tra_marca_terminal"/>
									</TD>
									<TD class="SearchResultItem">
										<xsl:value-of select="tra_tipo_terminal"/>
									</TD>
									<TD class="SearchResultItem" style="mso-number-format:\@; text-align: center;">
										<xsl:value-of select="tra_codigo_respuesta"/>
									</TD>
									<TD class="SearchResultItem">
										<xsl:value-of select="tra_mensaje_respuesta"/>
									</TD>
									<TD class="SearchResultItem">
										<xsl:value-of select="tra_estado_actualizacion"/>
									</TD>
									<TD class="SearchResultItem">
										<xsl:value-of select="tra_version_software_pos"/>
									</TD>
									<TD class="SearchResultItem">
										<xsl:value-of select="tra_aplicaciones_pos"/>
									</TD>
									<TD class="SearchResultItem">
										<xsl:value-of select="tra_aplicaciones_a_instalar"/>
									</TD>
									<TD class="SearchResultItem" style="mso-number-format:\@; text-align: right;">
										<xsl:value-of select="tra_fecha_descarga"/>
									</TD>
									<TD class="SearchResultItem">
										<xsl:value-of select="tra_datos_descarga"/>
									</TD>
								</TR>
							</xsl:when>
							<xsl:otherwise>
								<TR>
									<TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: center;">
										<xsl:value-of select="tra_fecha"/>
									</TD>
									<TD class="SearchResultAltItem">
										<xsl:value-of select="tra_cliente_pos"/>
									</TD>
									<TD class="SearchResultAltItem">
										<xsl:value-of select="gru_nombre"/>
									</TD>
									<TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: center;">
										<xsl:value-of select="tra_serial_pos"/>
									</TD>
									<TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: center;">
										<xsl:value-of select="ter_terminalId"/>
									</TD>
									<!-- <TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="tra_message_type"/>
                  </TD> -->
									<!--  <TD class="SearchResultAltItem">
                    <xsl:value-of select="tra_processing_code"/>
                  </TD>
                  <TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="tra_stan"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="tra_direccion_ip_pos"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="tra_direccion_ip_detectada"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="tra_puerto_tcp_atencion"/>
                  </TD> -->
									<TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: right;">
										<xsl:value-of select="tra_imei_pos"/>
									</TD>
									<TD class="SearchResultItem" style="mso-number-format:\@; text-align: right;">
										<xsl:value-of select="tra_codigo_producto"/>
									</TD>
									<!--                   <TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: right;">
                    <xsl:value-of select="tra_sim_pos"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="tra_nivel_gprs"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="tra_nivel_bateria"/> -->
									<!--  </TD> -->
									<TD class="SearchResultAltItem">
										<xsl:value-of select="tra_marca_terminal"/>
									</TD>
									<TD class="SearchResultAltItem">
										<xsl:value-of select="tra_tipo_terminal"/>
									</TD>
									<TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: center;">
										<xsl:value-of select="tra_codigo_respuesta"/>
									</TD>
									<TD class="SearchResultAltItem">
										<xsl:value-of select="tra_mensaje_respuesta"/>
									</TD>
									<TD class="SearchResultAltItem">
										<xsl:value-of select="tra_estado_actualizacion"/>
									</TD>
									<TD class="SearchResultAltItem">
										<xsl:value-of select="tra_version_software_pos"/>
									</TD>
									<TD class="SearchResultAltItem">
										<xsl:value-of select="tra_aplicaciones_pos"/>
									</TD>
									<TD class="SearchResultAltItem">
										<xsl:value-of select="tra_aplicaciones_a_instalar"/>
									</TD>
									<TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: right;">
										<xsl:value-of select="tra_fecha_descarga"/>
									</TD>
									<TD class="SearchResultAltItem">
										<xsl:value-of select="tra_datos_descarga"/>
									</TD>
								</TR>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</TABLE>
			</BODY>
		</HTML>
	</xsl:template>
</xsl:stylesheet>