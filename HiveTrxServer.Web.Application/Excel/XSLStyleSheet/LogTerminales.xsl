﻿<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:decimal-format name="German" decimal-separator=',' grouping-separator='.'  NaN=''/>
  <xsl:template match="/">
    <HTML>
      <HEAD>
        <STYLE>
          .stdPVTblLCell {
          color: DarkBlue;
          font-weight: bold;
          text-align: left;
          padding-left: 4px;
          padding-top: 4px;
          padding-bottom: 4px;
          width: 70%;
          font-size: 12pt;
          }
          .stdPageHdr {
          color: DarkBlue;
          font-weight: bold;
          font-style:italic;
          font-family:Verdana;
          text-align: left;
          padding-left: 4px;
          padding-top: 4px;
          padding-bottom: 4px;
          width: 70%;
          font-size: 20pt;
          }
          .gridHeader {
          background-color: #C0C0C0;
          color: DarkBlue;
          font-size: 9pt;
          font-weight: bold;
          font-family:Verdana;
          vertical-align:middle;
          text-align:center;
          border: solid thin Black;
          }
          .SearchHeader {
          color: DarkBlue;
          font-size: 9pt;
          font-weight: bold;
          font-family:Verdana;
          }
          .SearchKey {
          color: DarkBlue;
          font-size: 9pt;
          vertical-align:middle;
          text-align:right;
          font-family:Verdana;
          }
          .SearchValue {
          color: Black;
          font-size: 9pt;
          font-weight: bold;
          vertical-align:middle;
          text-align:left;
          font-family:Verdana;
          }
          .SearchResultHeader {
          color: DarkBlue;
          font-size: 9pt;
          font-weight: bold;
          font-family:Verdana;
          }
          .SearchResultItem {
          background-color: #8FC9FF;
          color: Black;
          font-size: 8pt;
          font-family:Verdana;
          border: solid thin Black;
          }
          .SearchResultAltItem {
          background-color: #CCE6FF;
          color: Black;
          font-size: 8pt;
          font-family:Verdana;
          border: solid thin Black;
          }
        </STYLE>
      </HEAD>
      <BODY>
        <TABLE>
          <TR>
            <TD class="stdPageHdr" colspan="6">
              <xsl:value-of select="NewDataSet/HeaderDetails/ReportName"/>
            </TD>
          </TR>
          <TR  class="stdPVTblLCell">
            <TD>
              Encabezado
            </TD>
          </TR>
          <TR>
            <TD> </TD>
          </TR>
          <TR>
            <TD colspan="1" class="SearchKey">Grupo:</TD>
            <TD class="SearchValue" colspan="6">
              <xsl:value-of select="NewDataSet/HeaderDetails/Group"/>
            </TD>
          </TR>
          <TR>
            <TD></TD>
          </TR>
          <TR class="SearchResultHeader">
            <TD colspan="2">Detalle del Reporte</TD>
          </TR>
          <TR>
            <TD> </TD>
          </TR>
          <TR>
            <TD class="gridHeader">
              Fecha de Creación
            </TD>
            <TD class="gridHeader">
              Fecha ultima Consulta
            </TD>
            <TD class="gridHeader">
              Aplicaciones Instaladas
            </TD>
            <TD class="gridHeader">
              Aplicaciones Pendientes
            </TD>
              <TD class="gridHeader">
              Grupo
            </TD>
            <TD class="gridHeader">
              Latitud Gps
            </TD>
            <TD class="gridHeader">
              Longitud Gps
            </TD>

            <TD class="gridHeader">
              Serial
            </TD>
            <TD class="gridHeader">
              Tipo
            </TD>
            <TD class="gridHeader">
              Marca
            </TD>
            <TD class="gridHeader">
              Modelo
            </TD>
            <TD class="gridHeader">
              Interface
            </TD>
            <TD class="gridHeader">
              SIM
            </TD>
            <TD class="gridHeader">
              Teléfono SIM
            </TD>
            <TD class="gridHeader">
              IMEI
            </TD>
            <TD class="gridHeader">
              Serial Cargador
            </TD>
            <TD class="gridHeader">
              Serial Batería
            </TD>
            <TD class="gridHeader">
              Serial Lector Biométrico
            </TD>
            <TD class="gridHeader">
              Descripción Terminal
            </TD>
            <TD class="gridHeader">
              Estado Actualización
            </TD>
            <TD class="gridHeader">
              Mensaje Actualización
            </TD>
            <TD class="gridHeader">
              Estado Terminal
            </TD>
            <TD class="gridHeader">
              Prioridad Grupo?
            </TD>
            <TD class="gridHeader">
              Actualizar?
            </TD>
            <TD class="gridHeader">
              Fecha de Descarga
            </TD>
            <TD class="gridHeader">
              IP/Puerto Descarga
            </TD>
            <TD class="gridHeader">
              Validar IMEI?
            </TD>
            <TD class="gridHeader">
              Validar SIM?
            </TD>
            <TD class="gridHeader">
              Nivel GPRS
            </TD>            
            <TD class="gridHeader">
              Nivel Batería
            </TD>            	
            <TD class="gridHeader">
              Tipo Identificación
            </TD>
            <TD class="gridHeader">
              Número Identificación
            </TD>
            <TD class="gridHeader">
              Nombre Contacto
            </TD>
            <TD class="gridHeader">
              Dirección Contacto
            </TD>
            <TD class="gridHeader">
              Celular
            </TD>
            <TD class="gridHeader">
              Teléfono Fijo
            </TD>
            <TD class="gridHeader">
              Correo Electrónico
            </TD>
          </TR>
          <xsl:for-each select="NewDataSet/Table">
            <xsl:choose>
              <xsl:when test="position() mod 2 = 1">
                <TR>
                  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="ter_fecha_creacion"/>
                  </TD>
                  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="fecha_consulta"/>
                  </TD>
                  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="aplicaciones_instaladas"/>
                  </TD>
                  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="aplicaciones_pendientes"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="gru_nombre"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="latitud_gps"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="longitud_gps"/>
                  </TD>
                  
                  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="ter_serial"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="ter_tipo_terminal"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="ter_marca_terminal"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="ter_modelo_terminal"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="ter_tipo_interface_terminal"/>
                  </TD>
                  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: right;">
                    <xsl:value-of select="ter_serial_sim"/>
                  </TD>
                  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: right;">
                    <xsl:value-of select="ter_telefono_sim"/>
                  </TD>
                  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: right;">
                    <xsl:value-of select="ter_imei"/>
                  </TD>
                  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: right;">
                    <xsl:value-of select="ter_serial_cargador"/>
                  </TD>
                  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: right;">
                    <xsl:value-of select="ter_serial_bateria"/>
                  </TD>
                  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: right;">
                    <xsl:value-of select="ter_serial_lector_biometrico"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="ter_descripcion"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="ter_estado_actualizacion"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="ter_mensaje_actualizacion"/>
                  </TD>
                  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="ter_estado"/>
                  </TD>
                  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="ter_prioridad_grupo"/>
                  </TD>
                  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="ter_actualizar"/>
                  </TD>                  
                  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="ter_fecha_programacion"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="ter_datos_descarga"/>
                  </TD>
                  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="ter_validar_imei"/>
                  </TD>
                  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="ter_validar_sim"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="ter_nivel_gprs"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="ter_nivel_bateria"/> %
                  </TD>	  
                  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="tipo_identificacion_tercero"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="terc_numero_identificacion"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="terc_nombre"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="terc_direccion"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="terc_celular"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="terc_telefono_fijo"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="terc_correo_electronico"/>
                  </TD>
                </TR>
              </xsl:when>
              <xsl:otherwise>
                <TR>
                  <TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="ter_fecha_creacion"/>
                  </TD>
                  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="fecha_consulta"/>
                  </TD>
                  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="aplicaciones_instaladas"/>
                  </TD>
                  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="aplicaciones_pendientes"/>
                  </TD>
                  
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="gru_nombre"/>
                  </TD>

                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="latitud_gps"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="longitud_gps"/>
                  </TD>
                  
                  <TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="ter_serial"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="ter_tipo_terminal"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="ter_marca_terminal"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="ter_modelo_terminal"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="ter_tipo_interface_terminal"/>
                  </TD>
                  <TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: right;">
                    <xsl:value-of select="ter_serial_sim"/>
                  </TD>
                  <TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: right;">
                    <xsl:value-of select="ter_telefono_sim"/>
                  </TD>
                  <TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: right;">
                    <xsl:value-of select="ter_imei"/>
                  </TD>
                  <TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: right;">
                    <xsl:value-of select="ter_serial_cargador"/>
                  </TD>
                  <TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: right;">
                    <xsl:value-of select="ter_serial_bateria"/>
                  </TD>
                  <TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: right;">
                    <xsl:value-of select="ter_serial_lector_biometrico"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="ter_descripcion"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="ter_estado_actualizacion"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="ter_mensaje_actualizacion"/>
                  </TD>
                  <TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="ter_estado"/>
                  </TD>
                  <TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="ter_prioridad_grupo"/>
                  </TD>
                  <TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="ter_actualizar"/>
                  </TD>
                  <TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="ter_fecha_programacion"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="ter_datos_descarga"/>
                  </TD>
                  <TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="ter_validar_imei"/>
                  </TD>
                  <TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="ter_validar_sim"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="ter_nivel_gprs"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="ter_nivel_bateria"/> %
                  </TD>        
         	  				  
                  <TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="tipo_identificacion_tercero"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="terc_numero_identificacion"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="terc_nombre"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="terc_direccion"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="terc_celular"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="terc_telefono_fijo"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="terc_correo_electronico"/>
                  </TD>
                </TR>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </TABLE>
      </BODY>
    </HTML>
  </xsl:template>
</xsl:stylesheet>