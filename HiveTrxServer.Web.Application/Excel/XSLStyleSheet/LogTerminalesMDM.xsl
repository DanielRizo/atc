﻿<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:decimal-format name="German" decimal-separator=',' grouping-separator='.'  NaN=''/>
  <xsl:template match="/">
    <HTML>
      <HEAD>
        <STYLE>
          .stdPVTblLCell {
          color: DarkBlue;
          font-weight: bold;
          text-align: left;
          padding-left: 4px;
          padding-top: 4px;
          padding-bottom: 4px;
          width: 70%;
          font-size: 12pt;
          }
          .stdPageHdr {
          color: DarkBlue;
          font-weight: bold;
          font-style:italic;
          font-family:Verdana;
          text-align: left;
          padding-left: 4px;
          padding-top: 4px;
          padding-bottom: 4px;
          width: 70%;
          font-size: 20pt;
          }
          .gridHeader {
          background-color: #C0C0C0;
          color: DarkBlue;
          font-size: 9pt;
          font-weight: bold;
          font-family:Verdana;
          vertical-align:middle;
          text-align:center;
          border: solid thin Black;
          }
          .SearchHeader {
          color: DarkBlue;
          font-size: 9pt;
          font-weight: bold;
          font-family:Verdana;
          }
          .SearchKey {
          color: DarkBlue;
          font-size: 9pt;
          vertical-align:middle;
          text-align:right;
          font-family:Verdana;
          }
          .SearchValue {
          color: Black;
          font-size: 9pt;
          font-weight: bold;
          vertical-align:middle;
          text-align:left;
          font-family:Verdana;
          }
          .SearchResultHeader {
          color: DarkBlue;
          font-size: 9pt;
          font-weight: bold;
          font-family:Verdana;
          }
          .SearchResultItem {
          background-color: #8FC9FF;
          color: Black;
          font-size: 8pt;
          font-family:Verdana;
          border: solid thin Black;
          }
          .SearchResultAltItem {
          background-color: #CCE6FF;
          color: Black;
          font-size: 8pt;
          font-family:Verdana;
          border: solid thin Black;
          }
          .ItemGreen {
          background-color: #dff0d8;
          color: Black;
          font-size: 8pt;
          font-family:Verdana;
          border: solid thin Black;
          }
          .ItemRed {
          background-color: #e47a7a;
          color: White;
          font-size: 8pt;
          font-family:Verdana;
          border: solid thin Black;
          }
        </STYLE>
      </HEAD>
      <BODY>
        <TABLE>
          <TR>
            <TD>&#160;&#160;&#160;</TD>
            <TD class="stdPageHdr" colspan="6">
              <xsl:value-of select="NewDataSet/HeaderDetails/ReportName"/>
            </TD>
          </TR>
          <TR  class="stdPVTblLCell">
            <TD>&#160;&#160;&#160;</TD>
            <TD>
              Encabezado
            </TD>
          </TR>
          <TR>
            <TD>&#160;&#160;&#160;</TD>
            <TD> </TD>
          </TR>
          <TR>
            <TD>&#160;&#160;&#160;</TD>
            <TD colspan="1" class="SearchKey">Grupo:</TD>
            <TD class="SearchValue" colspan="6">
              <xsl:value-of select="NewDataSet/HeaderDetails/Group"/>
            </TD>
          </TR>
          <TR>
            <TD>&#160;&#160;&#160;</TD>
            <TD></TD>
          </TR>
          <TR class="SearchResultHeader">
            <TD>&#160;&#160;&#160;</TD>
            <TD colspan="2">Detalle del Reporte</TD>
          </TR>
          <TR>
            <TD>&#160;&#160;&#160;</TD>
            <TD> </TD>
          </TR>
          <TR>
            
      
            
            <TD class="gridHeader">
              Fecha ultima Consulta
            </TD>
            
             <TD class="gridHeader">
               Aplicaciones Instaladas
            </TD>
            
             <TD class="gridHeader">
              Aplicaciones Pendientes
            </TD>
            
             <TD class="gridHeader">
              Latitud GPS
            </TD>
            
             <TD class="gridHeader">
              Longitud GPS
            </TD>
            
            <TD class="gridHeader">
              Grupo
            </TD>
            
            <TD class="gridHeader">
              Marca
            </TD>
            
            <TD class="gridHeader">
              Tipo
            </TD>
            
            <TD class="gridHeader">
              Dispositivo ID
            </TD>
            <TD class="gridHeader">
              Estado
            </TD>
            <TD class="gridHeader">
              IP
            </TD>
      
            <TD class="gridHeader">
              Carga Componente 1
            </TD>
           
            <TD class="gridHeader">
              Carga Componente 2
            </TD>
           
            <TD class="gridHeader">
              Última Carga Llave
            </TD>
            <TD class="gridHeader">
              APK Banco
            </TD>
            <TD class="gridHeader">
              Configuración
            </TD>
            <TD class="gridHeader">
              Imágenes
            </TD>	
            <TD class="gridHeader">
              Tipo Identificación
            </TD>
            <TD class="gridHeader">
              Número Identificación
            </TD>
            <TD class="gridHeader">
              Nombre Contacto
            </TD>
            <TD class="gridHeader">
              Dirección Contacto
            </TD>
            <TD class="gridHeader">
              Celular
            </TD>
            <TD class="gridHeader">
              Teléfono Fijo
            </TD>
            <TD class="gridHeader">
              Correo Electrónico
            </TD>
          </TR>
          <xsl:for-each select="NewDataSet/Table">
            <xsl:choose>
              <xsl:when test="position() mod 2 = 1">
                <TR>
                  
                  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="fecha_consulta"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="aplicaciones_instaladas"/>
                  </TD>
                   <TD class="SearchResultItem">
                    <xsl:value-of select="aplicaciones_pendientes"/>
                  </TD>
                   <TD class="SearchResultItem">
                    <xsl:value-of select="latitud_gps"/>
                  </TD>
                   <TD class="SearchResultItem">
                    <xsl:value-of select="longitud_gps"/>
                  </TD>
                  
                  <TD class="SearchResultItem">
                    <xsl:value-of select="gru_nombre"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="ter_marca_terminal"/>
                  </TD>
                   <TD class="SearchResultItem">
                    <xsl:value-of select="ter_tipo_terminal"/>
                  </TD>
                  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="ter_serial"/>
                  </TD>
                  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="ter_estado_actualizacion"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="ter_ip_dispositivo"/>
                  </TD>
               
                  
                  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="ter_fecha_inyeccion_componente1"/>
                  </TD>
                  
                  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="ter_fecha_inyeccion_componente2"/>
                  </TD>
                  
                  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="ter_fecha_ultima_carga_llave"/>
                  </TD>
                  <xsl:choose>
                    <xsl:when test="apk_banco_color = '#dff0d8'">
                      <TD class="ItemGreen" style="mso-number-format:\@; text-align: center;">
                        <xsl:value-of select="apk_banco"/>
                      </TD>
                    </xsl:when>
                    <xsl:otherwise>
                      <TD class="ItemRed" style="mso-number-format:\@; text-align: center;">
                        <xsl:value-of select="apk_banco"/>
                      </TD>
                    </xsl:otherwise>
                  </xsl:choose>
                  <xsl:choose>
                    <xsl:when test="configuracion_color = '#dff0d8'">
                      <TD class="ItemGreen" style="mso-number-format:\@; text-align: center;">
                        <xsl:value-of select="configuracion"/>
                      </TD>
                    </xsl:when>
                    <xsl:otherwise>
                      <TD class="ItemRed" style="mso-number-format:\@; text-align: center;">
                        <xsl:value-of select="configuracion"/>
                      </TD>
                    </xsl:otherwise>
                  </xsl:choose>
                  <xsl:choose>
                    <xsl:when test="imagenes_color = '#dff0d8'">
                      <TD class="ItemGreen" style="mso-number-format:\@; text-align: center;">
                        <xsl:value-of select="imagenes"/>
                      </TD>
                    </xsl:when>
                    <xsl:otherwise>
                      <TD class="ItemRed" style="mso-number-format:\@; text-align: center;">
                        <xsl:value-of select="imagenes"/>
                      </TD>
                    </xsl:otherwise>
                  </xsl:choose>              
                  <TD class="SearchResultItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="tipo_identificacion_tercero"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="terc_numero_identificacion"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="terc_nombre"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="terc_direccion"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="terc_celular"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="terc_telefono_fijo"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="terc_correo_electronico"/>
                  </TD>
                </TR>
              </xsl:when>
              <xsl:otherwise>
                <TR>
                 
                  <TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="fecha_consulta"/>
                  </TD>
                   <TD class="SearchResultAltItem">
                    <xsl:value-of select="aplicaciones_instaladas"/>
                  </TD>
                   <TD class="SearchResultAltItem">
                    <xsl:value-of select="aplicaciones_pendientes"/>
                  </TD>
                    <TD class="SearchResultAltItem">
                    <xsl:value-of select="latitud_gps"/>
                  </TD>
                   <TD class="SearchResultAltItem">
                    <xsl:value-of select="longitud_gps"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="gru_nombre"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="ter_marca_terminal"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="ter_tipo_terminal"/>
                  </TD>
                  <TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="ter_serial"/>
                  </TD>
                  <TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="ter_estado_actualizacion"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="ter_ip_dispositivo"/>
                  </TD>
                
                  
                  <TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="ter_fecha_inyeccion_componente1"/>
                  </TD>
                  
                  <TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="ter_fecha_inyeccion_componente2"/>
                  </TD>
                  
                  <TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="ter_fecha_ultima_carga_llave"/>
                  </TD>
                  <xsl:choose>
                    <xsl:when test="apk_banco_color = '#dff0d8'">
                      <TD class="ItemGreen" style="mso-number-format:\@; text-align: center;">
                        <xsl:value-of select="apk_banco"/>
                      </TD>
                    </xsl:when>
                    <xsl:otherwise>
                      <TD class="ItemRed" style="mso-number-format:\@; text-align: center;">
                        <xsl:value-of select="apk_banco"/>
                      </TD>
                    </xsl:otherwise>
                  </xsl:choose>
                  <xsl:choose>
                    <xsl:when test="configuracion_color = '#dff0d8'">
                      <TD class="ItemGreen" style="mso-number-format:\@; text-align: center;">
                        <xsl:value-of select="configuracion"/>
                      </TD>
                    </xsl:when>
                    <xsl:otherwise>
                      <TD class="ItemRed" style="mso-number-format:\@; text-align: center;">
                        <xsl:value-of select="configuracion"/>
                      </TD>
                    </xsl:otherwise>
                  </xsl:choose>
                  <xsl:choose>
                    <xsl:when test="imagenes_color = '#dff0d8'">
                      <TD class="ItemGreen" style="mso-number-format:\@; text-align: center;">
                        <xsl:value-of select="imagenes"/>
                      </TD>
                    </xsl:when>
                    <xsl:otherwise>
                      <TD class="ItemRed" style="mso-number-format:\@; text-align: center;">
                        <xsl:value-of select="imagenes"/>
                      </TD>
                    </xsl:otherwise>
                  </xsl:choose>
                  <TD class="SearchResultAltItem" style="mso-number-format:\@; text-align: center;">
                    <xsl:value-of select="tipo_identificacion_tercero"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="terc_numero_identificacion"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="terc_nombre"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="terc_direccion"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="terc_celular"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="terc_telefono_fijo"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="terc_correo_electronico"/>
                  </TD>                                    
                </TR>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </TABLE>
      </BODY>
    </HTML>
  </xsl:template>
</xsl:stylesheet>