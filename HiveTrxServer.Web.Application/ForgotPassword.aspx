﻿<%@ Page Title="" Language="VB" MasterPageFile="~/FullPage.master" AutoEventWireup="false" CodeFile="ForgotPassword.aspx.vb" Inherits="ForgotPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title><%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Olvidé mi Clave</title>

    <!-- Toggle -->
    <script type="text/javascript" src="js/toogle.js"></script>
    
    <!-- Modal Alert -->
    <script src="js/msgAlert.js" type="text/javascript"></script>

    <!-- Google's reCaptcha -->
    <script src='https://www.google.com/recaptcha/api.js'></script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <asp:Panel ID="pnlMsg" runat="server" Visible="False">
        <div class="albox succesbox">
            <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
            <a href="#" class="close tips" title="Cerrar">Cerrar</a>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlError" runat="server" Visible="False">
        <div class="albox errorbox">
            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
            <a href="#" class="close tips" title="Cerrar">Cerrar</a>
        </div>
    </asp:Panel> 

    <div class="loginform">
        <div class="title"> <h2 style="font-size: 18px !important;">Olvidé mi clave</h2> </div>
        <div class="body" style="padding-top: 10px !important;">            
            <div class="simple-tips">
                Digite el correo electrónico asociado a su cuenta para iniciar el proceso de restauración de clave.
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
      	    <label class="log-lab">Correo electrónico:</label>
            <asp:TextBox ID="txtEmailForgotPwd" runat="server" class="login-input-email" value="" TabIndex="1" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
             <% If ConfigurationSettings.AppSettings.Get("UseRecaptcha") = "YES" Then%>
                <div class="g-recaptcha" data-callback="recaptchaCallback" data-sitekey="<%=ConfigurationSettings.AppSettings.Get("reCaptchaSiteKey")%>" style="padding-bottom: 17px;"></div>
            <% End If%>
            <asp:Button ID="btnSubmit" runat="server" Text="Reestablecer clave" class="button" TabIndex="2" onclientclick="return validateForgotPassword()" />            
            <div style="padding-top: 10px;">
                <a href="Login.aspx"><img src="img/previous.png" width="12px" height="12px" alt="Atrás"/> Iniciar sesión</a>
            </div>
        </div>
    </div>  
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">
    
    <script type="text/javascript">
        $(function () {
            $("#ctl00_MainContent_txtEmailForgotPwd").focus();
        });
    </script>    
    
    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
    <!-- Validator -->
    <script src="js/ValidatorLogin.js" type="text/javascript"></script>

    <script src="js/ValidatorUtils.js" type="text/javascript"></script>
    
</asp:Content>

