﻿Imports TeleLoader.Security
Imports System
Imports System.Net
Imports TeleLoader.Sessions
Imports TeleLoader.Users
Imports System.Data.SqlClient
Imports System.Data

Partial Class ForgotPassword
    Inherits System.Web.UI.Page
    Dim Correo As String
    Dim Password As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            pnlMsg.Visible = False
            pnlError.Visible = False
        End If
    End Sub

    Public Shared Function GetIP4Address() As String
        Dim IP4Address As String = String.Empty

        For Each IPA As IPAddress In Dns.GetHostAddresses(HttpContext.Current.Request.UserHostAddress)
            If IPA.AddressFamily.ToString() = "InterNetwork" Then
                IP4Address = IPA.ToString()
                Exit For
            End If
        Next

        If IP4Address <> String.Empty Then
            Return IP4Address
        End If

        For Each IPA As IPAddress In Dns.GetHostAddresses(Dns.GetHostName())
            If IPA.AddressFamily.ToString() = "InterNetwork" Then
                IP4Address = IPA.ToString()
                Exit For
            End If
        Next

        Return IP4Address
    End Function
    Private Function getDatosCorreo()
        Dim Json As String
        Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString
        Dim db As SqlConnection = New SqlConnection(conexion)
        Dim cmd As SqlCommand
        Dim sqlBuilder As StringBuilder = New StringBuilder
        Dim results As SqlDataReader

        Dim retVal As Boolean

        Dim strRsp As String
        strRsp = ""
        sqlBuilder.Append("SELECT CAST(DecryptByPassPhrase( 'Clave',correo_clave ) as varchar(max)) as 'password' ,CAST(DecryptByPassPhrase( 'Clave',correo_nombre ) as varchar(max)) as 'correo'  FROM dbo.correo")

        Try
            db.Open()
        Catch ex As Exception

            db.Close()
        End Try

        Try
            cmd = New SqlCommand()
            cmd.Connection = db
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sqlBuilder.ToString
            'cmd.Parameters.AddWithValue("@TRN", Data)

            Try

            Catch ex As Exception
            End Try
            'Ejecutar SP
            results = cmd.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    Password = results.GetString(0)
                    Correo = results.GetString(1)
                End While
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
        End Try
    End Function

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        ' capturo mis datos de correo
        getDatosCorreo()
        Dim IsCaptchaValid As Boolean = False

        If ConfigurationSettings.AppSettings.Get("UseRecaptcha") = "YES" Then
            Try
                Dim EncodedResponse As String = Request.Form("g-Recaptcha-Response")
                IsCaptchaValid = IIf(ReCaptcha.Validate(EncodedResponse) = "True", True, False)
            Catch ex As Exception
                Session.Remove("AccessToken")
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(function() { $.msgAlert({ type: 'error', title: 'Mensaje del sistema', text: 'Error Conectando al Servidor reCAPTCHA: [" & ex.Message.Replace("'", "").Replace("""", "") & "]' }); });</script>"
                Exit Sub
            End Try
        Else
            IsCaptchaValid = True
        End If

        If IsCaptchaValid Then
            Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

            Dim connString As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString

            Dim objUser As User = New User(connString)

            objUser.Email = txtEmailForgotPwd.Text.Trim()

            'Validate Email Typed
            If objUser.CheckUserByEmail() Then

                '1. Start Reset Password Process
                If objUser.StartResetPasswordProcess(GetIP4Address()) Then

                    If MailSender.SendEmail(objUser.Email, objUser.ActivationCode, 1, "", Correo, Password) Then  '1 = Reset Password  
                        pnlMsg.Visible = True
                        pnlError.Visible = False
                        lblMsg.Text = "Se ha enviado un mensaje a su cuenta de correo. Por favor revise su bandeja de entrada y siga las instrucciones."
                    Else
                        pnlMsg.Visible = False
                        pnlError.Visible = True
                        lblError.Text = "Estamos presentando interrupciones en nuestro servicio. Por favor intente mas tarde."
                    End If
                Else
                    pnlMsg.Visible = False
                    pnlError.Visible = True
                    lblError.Text = "Esta cuenta ya presenta un proceso de restauración de clave iniciado. Por favor verifique."
                End If

                txtEmailForgotPwd.Text = ""

            Else
                pnlMsg.Visible = False
                pnlError.Visible = True
                lblError.Text = "No existe una cuenta de correo en nuestro sistema con el valor ingresado."
            End If
        Else
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(function() { $.msgAlert({ type: 'error', title: 'Mensaje del sistema', text: 'Error de autenticación reCAPTCHA.' }); });</script>"
        End If

    End Sub
End Class
