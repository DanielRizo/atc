﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class FullPage
    Inherits System.Web.UI.MasterPage

    ' Error handler estándar, redirecciona a página de error
    Protected Sub HandleErrorRedirect(ByVal exception As Exception)

        If Session("Exception") Is Nothing Then
            Session("Exception") = exception
        End If

        Server.ClearError()

        Response.Redirect("~/ErrorPage.aspx")

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblDateTime.Text = Now().ToString
        Me.LblAnio.Text = Now.Year

        ScriptManager.RegisterOnSubmitStatement(Me, Me.GetType(), "", "ShowProgressWindow();")

        'Llamando al método true= https; false = http;
        Dim protocol As String = ConfigurationSettings.AppSettings.Get("Protocol").Trim().ToUpper()
        protocoloSeguro(protocol = "HTTPS")
    End Sub

    'Metodo de redireccionamiento..
    Public Sub protocoloSeguro(ByVal bSeguro As Boolean)
        Dim redirectUrl As String = Nothing
        If bSeguro AndAlso (Not Request.IsSecureConnection) Then
            redirectUrl = Request.Url.ToString().Replace("http:", "https:")
        Else
            If (Not bSeguro) AndAlso Request.IsSecureConnection Then
                redirectUrl = Request.Url.ToString().Replace("https:", "http:")
            End If
        End If

        If redirectUrl IsNot Nothing Then
            Response.Redirect(redirectUrl)
        End If
    End Sub

End Class

