﻿<%@ Application Language="VB" %>
<%@ Import Namespace="TeleLoader.Sessions" %>
<%@ Import Namespace="TeleLoader.Security" %>

<script runat="server">

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Código que se ejecuta al iniciarse la aplicación
        
    End Sub
    
    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Código que se ejecuta durante el cierre de aplicaciones
    End Sub
        
    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Código que se ejecuta al producirse un error no controlado
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Código que se ejecuta cuando se inicia una nueva sesión
        
        Try
           
            'Carga desde Web.config de la conexión a la base de datos
            Dim configurationSection As ConnectionStringsSection = _
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
            
            Dim connString As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString
            
            Dim IndexPageName As String = ConfigurationSettings.AppSettings.Get("IndexPageName")
            Dim ResetPasswordPageName As String = ConfigurationSettings.AppSettings.Get("ResetPasswordPageName")
            'Modificación MDM; EB: 01/Feb/2018
            Dim ActivateDevicePageName As String = ConfigurationSettings.AppSettings.Get("ActivateDevicePageName")
            Dim MonitorDataPageName As String = ConfigurationSettings.AppSettings.Get("MonitorDataPageName")
            Dim downloadedFileInfoPageName As String = ConfigurationSettings.AppSettings.Get("downloadedFileInfoPageName")
            Dim RequestKeyPageName As String = ConfigurationSettings.AppSettings.Get("RequestKeyPageName")
            Dim NotifyKeyLoadedPageName As String = ConfigurationSettings.AppSettings.Get("NotifyKeyLoadedPageName")
            
            Dim objSessionParameters As New SessionParameters()
            objSessionParameters.strSelectedMenu = "0"
            
            'Guardar en la sesión los objetos principales
            Session("ConnectionString") = connString
            Session("SessionParameters") = objSessionParameters
            
            'Modificación MDM; EB: 01/Feb/2018
            If Not Request.Path.Equals(ResetPasswordPageName) And
                Not Request.Path.Equals(ActivateDevicePageName) And
                    Not Request.Path.Equals(MonitorDataPageName) And
                    Not Request.Path.Equals(downloadedFileInfoPageName) And
                    Not Request.Path.Equals(RequestKeyPageName) And
                    Not Request.Path.Equals(NotifyKeyLoadedPageName) Then
                'Primer Ingreso
                Response.Redirect(IndexPageName)
            End If

        Catch ex As Exception
            Response.Write("Error cr&iacute;tico durante la inicializaci&oacute;n, por favor, cont&aacute;ctese con el Administrador del Sistema. " & ex.Message)
            Response.End()
            Session.Abandon()
            Exit Sub
        End Try
        
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Código que se ejecuta cuando finaliza una sesión. 
        ' Nota: El evento Session_End se desencadena sólo con el modo sessionstate
        ' se establece como InProc en el archivo Web.config. Si el modo de sesión se establece como StateServer 
        ' o SQLServer, el evento no se genera.
        
        Try
            Dim objAccessToken As AccessToken = Session("AccessToken")

            If Not objAccessToken Is Nothing Then
                objAccessToken.Validate("Session", "End")

                objAccessToken.closeSession()
                
                objAccessToken.Active = False
                
            End If
            
            'Renew ASP Cookie
            Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))

        Catch ex As Exception
            'ExceptionManager.Publish(ex)
        End Try
        
    End Sub
       
</script>