﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Applications
Imports System.Data.Common

Partial Class Groups_ListAcquirer
    Inherits TeleLoader.Web.BasePage

    Dim applicationObj As ApplicationAcquirerParameter

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            dsTerminalsParameter.SelectParameters("TABLE_KEY_CODE").DefaultValue = objSessionParams.StrAcquirerCode



            grdTerminalsParameterAcquirer.DataBind()
        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub
    Protected Sub grdTerminals_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdTerminalsParameterAcquirer.RowCommand

        Try

            Select Case e.CommandName

                Case "EditTerminalParameter"
                    grdTerminalsParameterAcquirer.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    'objSessionParams.intAcquirerCode = grdTerminalsParameterAcquirer.SelectedDataKey.Values.Item("TABLE_KEY_CODE")
                    'objSessionParams.StrFieldDisplayName = grdTerminalsParameterAcquirer.SelectedDataKey.Values.Item("FIELD_DISPLAY_NAME")
                    'objSessionParams.StrContentDesc = grdTerminalsParameterAcquirer.SelectedDataKey.Values.Item("CONTENT_DESC")

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("", False)
            End Select
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub
    'Edicion de Parametros Acquirer Stis 
    'Autor : Oscar Gutierrez
    Protected Sub grdTerminalsParameterAcquirer_Updated(sender As Object, e As GridViewUpdatedEventArgs) Handles grdTerminalsParameterAcquirer.RowUpdated

        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webEditParameterAcquirer_Stis"
            command.Parameters.Clear()


            command.Parameters.Add(New SqlParameter("TABLE_KEY_CODE", e.OldValues("TABLE_KEY_CODE")))
            command.Parameters.Add(New SqlParameter("FIELD_DISPLAY_NAME", e.OldValues("FIELD_DISPLAY_NAME")))
            command.Parameters.Add(New SqlParameter("CONTENT_DESC", e.NewValues("CONTENT_DESC")))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try


    End Sub
End Class
