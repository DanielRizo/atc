﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AddAndroidGroup.aspx.vb" Inherits="Group_AddAndroidGroup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Agregar Grupo Android
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

    <script type="text/javascript" src="../js/jquery.ui.slider.js"></script>

    <script type="text/javascript" src="../js/jquery.ui.datepicker.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>
    <li>Agregar Grupos Android</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    Agregar Grupos Android
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlError" runat="server" Visible="False">
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <div class="titleh">
            <h3>Datos del Grupo</h3>
        </div>
        <div class="body">

            <div class="st-form-line">	
                <span class="st-labeltext">Cliente:</span>	
                <asp:TextBox ID="txtCustomerName" CssClass="st-success-input" style="width:510px" 
                    runat="server" TabIndex="0" MaxLength="100" 
                    ToolTip="Nombre del Cliente." Enabled="False" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>

            <div class="st-form-line">
                <span class="st-labeltext">Nombre Grupo: (*)</span>
                <asp:TextBox ID="txtGroupName" CssClass="st-forminput" Style="width: 510px"
                    runat="server" TabIndex="1" MaxLength="100"
                    ToolTip="Digite nombre del grupo." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>

            <div class="st-form-line">
                <span class="st-labeltext">Descripci&oacute;n:</span>
                <asp:TextBox ID="txtDesc" CssClass="st-forminput" Style="width: 510px"
                    runat="server" TabIndex="2" MaxLength="250" TextMode="MultiLine"
                    ToolTip="Digite descripción del grupo." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>

            <div class="button-box">
                <asp:Button ID="btnSave" runat="server" Text="Crear Grupo"
                    CssClass="button-aqua" TabIndex="3" OnClientClick="return validateAddGroupAndroid()" />
            </div>
        </div>
    </div>

    <asp:HyperLink ID="lnkBack" runat="server" NavigateUrl="~/Group/Manager.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <!-- Validator -->
    <script src="../js/ValidatorAndroidGroup.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

</asp:Content>

