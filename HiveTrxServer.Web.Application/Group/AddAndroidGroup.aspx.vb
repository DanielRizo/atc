﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users
Imports System.Net
Imports System.Globalization
Imports TeleLoader.Groups

Partial Class Group_AddAndroidGroup
    Inherits TeleLoader.Web.BasePage

    Dim groupObj As Group

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Save")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        groupObj = New Group(strConnectionString)

        'Establecer Datos de Grupo
        groupObj.Name = txtGroupName.Text
        groupObj.Desc = txtDesc.Text
        groupObj.IpAddress = "-" 'Fixed Data
        groupObj.Port = "-" 'Fixed Data
        groupObj.allowUpdate = False  'Fixed Data
        groupObj.useDateRange = False 'Fixed Data

        groupObj.RangeDateIni = Now 'Fixed Data
        groupObj.RangeDateEnd = Now 'Fixed Data
        groupObj.RangeHourIni = Now 'Fixed Data
        groupObj.RangeHourEnd = Now 'Fixed Data

        groupObj.CustomerCode = objAccessToken.CustomerUserID
        groupObj.AndroidGroup = True    'Fixed Data
        'Aplicaciones por Defecto Grupo Android
        groupObj.ApplicationsDefault = ConfigurationSettings.AppSettings.Get("AllowedAppsDefault")
        groupObj.QueriesNumber = 3 'Fixed Data
        groupObj.Frequency = 24 'Fixed Data

        'Validate Group Name
        If groupObj.validateGroupName() Then

            'Save Data
            If groupObj.createGroupAndroid() Then
                'New Android Group Created OK
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Grupo Android Creado. Datos[ " & getFormDataLog() & " ]", "")

                pnlMsg.Visible = True
                pnlError.Visible = False
                lblMsg.Text = "Grupo Android creado correctamente."

                'Create Folder for Group Files
                Try
                    Dim groupFolderPath As String = Server.MapPath(ConfigurationSettings.AppSettings.Get("GroupsFolderPath")) & txtGroupName.Text

                    If (Not System.IO.Directory.Exists(groupFolderPath)) Then
                        System.IO.Directory.CreateDirectory(groupFolderPath)
                    End If
                Catch ex As Exception
                    HandleErrorRedirect(ex)
                End Try

                clearForm()

            Else
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Creando Grupo Android. Datos[ " & getFormDataLog() & " ]", "")
                pnlError.Visible = True
                pnlMsg.Visible = False
                lblError.Text = "Error creando grupo Android. Por favor valide los datos."
            End If

        Else
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Creando Grupo Android, nombre repetido. Datos[ " & getFormDataLog() & " ]", "")
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error creando grupo Android, nombre repetido. Por favor valide los datos."
        End If

    End Sub

    Private Sub clearForm()
        txtGroupName.Text = ""
        txtDesc.Text = ""

        txtGroupName.Focus()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then
            txtCustomerName.Text = objAccessToken.Customer

            txtGroupName.Focus()
        End If

    End Sub

End Class
