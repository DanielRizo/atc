﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AddCardRange.aspx.vb" Inherits="Groups_AddCardRange" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Adicionar CardRange STIS
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

    <link rel="stylesheet" type="text/css" href="/style/bootstrap.min.css" />

      <script type="text/javascript" src='<%= ResolveClientUrl("~/js/editTable.js") %>'></script>
   
    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.tabledit.js") %>'></script>
    
     <script type="text/javascript" src='<%= ResolveClientUrl("~/js/bootstrap.js") %>'></script>

    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/SaveTable.js") %>'></script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>

    <li>Agregar Nuevo CardRange...</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <br />
        <ul id="navst">
	<li class="current"><a href="TerminalStis.aspx">Polaris TMS</a></li>
	<li><a>Tables</a>
		<ul>
			<li><a href="TerminalAcquirer.aspx">Acquirer</a></li>
			<li><a href="TerminalIssuer.aspx">Issuer</a>	</li>
			<li><a href="TerminalCardRange.aspx">CardRange</a></li>
			<li><a href="TerminalEmvLevel2.aspx">Emv Level 2 Application</a></li>
			<li><a href="TerminalEmvKey.aspx">Emv Level 2 Key</a></li>
			<li><a href="TerminalExtraApplication.aspx">Extra Application Parameter</a></li>

		</ul>
	</li>

</ul>

    
    <div class="simplebox grid960">
        <div id="succes2"style="display: none;"  >
        <asp:Panel ID="Panel1" runat="server">
            <div class="albox succesbox">
                <asp:Label ID="lblMsg2" runat="server" Text="Datos editados correctamente."></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>
            </div> 
        
        <div id="error2"  style="display: none;">
        <asp:Panel ID="Panel2" runat="server">
            <div class="albox errorbox">
                <asp:Label ID="lblError2" runat="server" Text="Error editando datos."></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>
            </div>
    </div>


    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlError" runat="server" Visible="False">
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>
    </div>

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
        <div>
            <%--<h3>CardRange Stis...</h3>--%>
            <div class="shortcuts-icons" style="z-index: 660;">
                <br />
            </div>
        </div>
        <div >
            <h3 class="title">Agregar CardRange
                <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
            <div >
                <div class="st-form-line">
                    <div class="st-form-line">
                        <span class="st-labeltext">Code: (*)</span>
                        <input id="txtCode" type="text"/>

<%--                        <asp:TextBox ID="txtCode" CssClass="st-forminput" Style="width: 150px"
                            runat="server" TabIndex="1" MaxLength="5"
                            ToolTip="Digite El codigo del CardRange." onkeydown="return (event.keyCode!=13);"></asp:TextBox>--%>
                        <div class="clear"></div>
                    </div>
                    <div class="st-form-line">
                        <span class="st-labeltext">Display Name: (*)</span>
                        <input id="txtName" type="text"/>

                        <%--<asp:TextBox ID="TxtDisplayName" CssClass="st-forminput" Style="width: 150px"
                            runat="server" TabIndex="2" MaxLength="20"
                            ToolTip="Digite El nombre del CardRange." onkeydown="return (event.keyCode!=13);"></asp:TextBox>--%>
                        <div class="clear"></div>

                    </div>
                     <div class="st-form-line">
                        <span class="st-labeltext">Description: (*)</span>
                        <input id="txtDescription" type="text"/>

                        <%--<asp:TextBox ID="TxtDescription" CssClass="st-forminput" Style="width: 150px"
                            runat="server" TabIndex="2" MaxLength="20"
                            ToolTip="Digite la descripcion." onkeydown="return (event.keyCode!=13);"></asp:TextBox>--%>
                        <div class="clear"></div>

                    </div>
                    <div class="button-box">       
                    <%--<asp:Button ID="btnSave" runat="server" Text="Crear CardRange"
                        CssClass="button-aqua" TabIndex="0" ToolTip="Adicionar Acquirer" />--%>
                </div>
            </div>
        </div>
     </div>

          <center>
                <input type="button" class="button-aqua" id="initTerminals" value="Guardar" style="margin: 15px  3px;"/>
          </center> 

        <script type="text/javascript">
            $(document).ready(function () {
                enableEditTable("#ctl00_MainContent_grdAddCardRange", false,true, false,[[2, 'Value']], "addCardRangeTbl");
            });
        </script>


         <script type="text/javascript">

                    $('#initTerminals').on('click', function(event) {
                            event.preventDefault();
                        RecorrerTable("#ctl00_MainContent_grdAddCardRange", "addCardRangeTbl");
                       

         });     
     </script>   

       
        <div class="body">
            <div id="posStis" style="overflow: auto; width: auto; height: 660px;">
                <br />
                <asp:GridView ID="grdAddCardRange" runat="server"
                    AllowSorting="True" AutoGenerateColumns="False" CellPadding="4"
                    DataSourceID="dsAddCardRange"
                    CssClass="mGridCenter" DATAKEYNAME="IS_EXTENDED"
                    EmptyDataText="No existen Adquirientes creados con el filtro aplicado"
                    HorizontalAlign="Left" GridLines="None" ForeColor="#333333" >
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <%--<asp:CommandField ButtonType="Image" HeaderImageUrl="~/img/icons/16x16/edit.png" ShowEditButton="True" UpdateImageUrl="~/img/icons/16x16/ok.png" CancelImageUrl="~/img/icons/16x16/cancel.png" EditImageUrl="~/img/icons/16x16/edit.png" />--%>
                        <asp:BoundField DataField="IS_EXTENDED" HeaderText="Parameter Type"
                            SortExpression="IS_EXTENDED" ReadOnly="True" />
                        <asp:BoundField DataField="FIELD_DISPLAY_NAME" HeaderText="Parameter Name"
                            SortExpression="FIELD_DISPLAY_NAME" />
                        <asp:BoundField DataField="CONTENT_DESC" HeaderText="Value"
                            SortExpression="CONTENT_DESC" />
                        <asp:BoundField DataField="CONTENT_TYPE" HeaderText="Value Type"
                            SortExpression="CONTENT_TYPE" ReadOnly="True" />
                        <asp:BoundField DataField="NOTE" HeaderText="Note"
                            SortExpression="NOTE" ReadOnly="True" />
                        <%--  <asp:TemplateField ShowHeader="False" HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False"
                                CommandName="EditTerminalParameter" ImageUrl="~/img/icons/16x16/edit.png" Text="Editar"
                                ToolTip="Editar Acquirer" CommandArgument='<%# grdTerminalsParameterAcquirer.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" >
                        </ItemTemplate>

                        <HeaderStyle Width="100px"></HeaderStyle>

                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>--%>
                    </Columns>


                    <FooterStyle BackColor="#1C5E55" ForeColor="White" Font-Bold="True" />
                    <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center"
                        CssClass="pgr" />
                    <RowStyle BackColor="#E3EAEB" />
                    <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BorderColor="#666666" BorderStyle="Solid"
                        BorderWidth="1px" BackColor="#7C6F57" />
                    <SortedAscendingCellStyle BackColor="#F8FAFA" />
                    <SortedAscendingHeaderStyle BackColor="#246B61" />
                    <SortedDescendingCellStyle BackColor="#D4DFE1" />
                    <SortedDescendingHeaderStyle BackColor="#15524A" />
                </asp:GridView>
                <br />
            </div>

            <br />
            <asp:SqlDataSource ID="dsAddCardRange" runat="server"
                ConnectionString="<%$ ConnectionStrings:TeleLoaderStisConnectionString %>"
                SelectCommand="sp_webAddCardRange_Stis" SelectCommandType="StoredProcedure"
                UpdateCommand="sp_webEditDatosAcquirerNew_Stis" UpdateCommandType="StoredProcedure">
                <SelectParameters>
                    <asp:Parameter Name="IS_EXTENDED" Type="string" />

                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="FIELD_DISPLAY_NAME" Type="String" />
                    <asp:Parameter Name="CONTENT_DESC" Type="String" />

                </UpdateParameters>
            </asp:SqlDataSource>
        </div>
    </div>
    <asp:HyperLink ID="HyperLink1" runat="server"
        NavigateUrl="~/Group/TerminalIssuer.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>


<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

    <script language="javascript">
        var globalcontrol = '';

        function ConfirmAction(control) {

            globalcontrol = control;

            $.msgAlert({
                type: "warning"
                , title: "Mensaje del sistema"
                , text: "Realmente desea eliminar el adquiriente?"
                , callback: function () {
                    __doPostBack($(globalcontrol).attr('name'), '');
                }
            });

            return false;
        }
    </script>

</asp:Content>

