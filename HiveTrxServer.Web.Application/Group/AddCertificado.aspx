﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AddCertificado.aspx.vb" Inherits="Add_Certificado" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Adicionar Cerificado SSL
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkListGroups" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/ListGroups.aspx">Listar Grupos</asp:LinkButton>
    </li>
    <li>Certificados SSL</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    Certificados SSL
    <asp:HyperLink ID="HyperLink1" runat="server"
        NavigateUrl="~/Group/TerminalStis.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">

    <blockquote>
        <p>Este módulo permite ver el listado de Certificadod Pstis:</p>
        <footer class="blockquote-footer">Polaris Cloud Service (Pstis) </footer>
    </blockquote>

    <asp:Panel ID="Panel1" runat="server" Visible="False">
        <div class="alert alert-success">
            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
            <a href="#" class="close tips" title="Cerrar">x</a>
        </div>
    </asp:Panel>
    <asp:Panel ID="Panel2" runat="server" Visible="False">
        <div class="alert alert-warning">
            <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
            <a href="#" class="close tips" title="Cerrar">x</a>
        </div>
    </asp:Panel>
    <style>
        .bg-1 {
            background-color: #1abc9c;
            color: #ffffff;
        }
    </style>
    <div class="bg-1">
        <div class="container text-center">
            <h5>Lista de Certificados Pstis</h5>
            <img src="http://kapicupro.com/images/icon-certificate.svg" class="img-circle" alt="Bird" width="350" height="280">
        </div>
    </div>
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">
            <div class="alert alert-success">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">x</a>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlError" runat="server" Visible="False">
            <div class="alert alert-danger">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">x</a>
            </div>
        </asp:Panel>

        <div class="toggle-message" id="dragAndDropArea" style="z-index: 590;">
            <h5 class="title">Agregar Certificado SSL...
                <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h5>
            <div class="hide-message" id="hide-message" style="display: none;">

                <div class="st-form-line">
                    <span class="st-labeltext">Archivo de Certificado:</span>
                    <div class="uploader" id="uniform-undefined">
                        <asp:FileUpload ID="fileCer" runat="server" CssClass="uniform" TabIndex="1" onchange="handleFiles(this.files)" />
                        <span class="filename">No Hay Certificado Seleccionado...</span><span class="action">Seleccione...</span>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="st-form-line">
                    <div id="dragandrophandler">
                        <br />
                        <br />
                        Arrastre y Suelte el archivo aqu&iacute;...
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="st-form-line">
                    <span class="st-labeltext">Descripci&oacute;n:</span>
                    <asp:TextBox ID="txtDesc" CssClass="st-forminput" Style="width: 697px"
                        runat="server" TabIndex="2" MaxLength="250" TextMode="MultiLine"
                        ToolTip="Descripción de la Aplicación." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>
                <%--         CARGA DE CERTIFICADOS POR MODELO OG--%>
                <div class="button-box">
                    <asp:Button ID="btnAddCertificado" runat="server" Text="Adicionar Certificado"
                        CssClass="btn btn-info" TabIndex="4" OnClientClick="return validateAddApplication();" Enabled="false" />
                </div>
            </div>
        </div>
    </div>
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
        <div class="titleh">
            <h3>Certificados Creados en el Grupo</h3>
        </div>
        <ul id="navst">
            <li class="navbar navbar-light"><a href="TerminalStis.aspx">PSTIS</a></li>
            <li><a>Tables</a>
                <ul>
                    <li><a href="TerminalAcquirer.aspx">Acquirer</a></li>
                    <li><a href="TerminalIssuer.aspx">Issuer</a>	</li>
                    <li><a href="TerminalCardRange.aspx">CardRange</a></li>
                    <li><a href="TerminalEmvLevel2.aspx">Emv Level 2 Application</a></li>
                    <li><a href="TerminalEmvKey.aspx">Emv Level 2 Key</a></li>
                    <li><a href="TerminalExtraApplication.aspx">Extra Application Parameter</a></li>
                    <li><a href="ListarGroupPrompts.aspx">Grupos Prompts</a></li>
                    <li><a href="ListarGroupVariousPayments.aspx">Grupos Pagos Varios</a></li>
                    <li><a href="ListarGroupElectronicsPayments.aspx">Grupos Pagos Electronicos</a></li>
                    <li><a href="TerminalHostConfiguration.aspx">Host Configuration</a></li>
                    <li><a href="TerminalPrompts.aspx">Prompts</a></li>
                    <li><a href="PagosVarios.aspx">Pagos Varios</a></li>
                    <li><a href="PagosElectronicos.aspx">Pagos Electronicos</a></li>
                    <li><a href="IP.aspx">Ip</a></li>
                    <li><a href="AddCertificado.aspx">Certificados SSL</a></li>
                </ul>
            </li>
            <li><a>Terminal</a>
                <ul>
                    <li><a href="AddTerminalStis.aspx">New Terminal</a></li>
                    <li><a href="CopyTerminalStis.aspx">Copy Terminal</a></li>

                </ul>
            </li>
        </ul>
        <div class="body">
            <br />
            <asp:GridView ID="grdCertificado" runat="server" AllowPaging="True"
                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4"
                DataSourceID="dsCertificado" ForeColor="#333333"
                HeaderStyle-ForeColor="WhiteSmoke"
                HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                HeaderStyle-CssClass="table-bordered bg-info" DataKeyNames="apl_id"
                EmptyDataText="No existen Certificados creadas en el Grupo."
                HorizontalAlign="Left" Width="1060px">
                <RowStyle BackColor="White" ForeColor="White" />
                <Columns>
                    <asp:BoundField DataField="apl_id" HeaderText="ID Certificado"
                        SortExpression="apl_checksum_tms" />
                    <asp:BoundField DataField="apl_longitud" HeaderText="Tamaño (Bytes)"
                        SortExpression="apl_longitud" />
                    <asp:BoundField DataField="apl_descripcion" HeaderText="Descripci&oacute;n"
                        SortExpression="apl_descripcion" />

                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False"
                                CommandName="Delete" ImageUrl="~/img/icons/16x16/delete.png" Text="Eliminar"
                                ToolTip="Eliminar Certificado del Grupo" CommandArgument='<%# grdCertificado.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink"
                                OnClientClick="return ConfirmAction(this);" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
                    CssClass="pgr" Font-Underline="False" />
                <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid"
                    BorderWidth="1px" />
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>
            <br />
            <asp:SqlDataSource ID="dsCertificado" runat="server"
                ConnectionString="<%$ ConnectionStrings:TeleLoaderStisConnectionString %>"
                SelectCommand="sp_webConsultarCertificadosSSL" SelectCommandType="StoredProcedure"
                DeleteCommand="sp_webEliminarCertificado" DeleteCommandType="StoredProcedure" ConflictDetection="OverwriteChanges">
                <SelectParameters>
                    <asp:Parameter Name="groupId" Type="String" />
                </SelectParameters>
                <DeleteParameters>
                    <asp:Parameter Name="applicationId" Type="String" />
                    <asp:Parameter Name="groupId" Type="String" />
                </DeleteParameters>
            </asp:SqlDataSource>
        </div>
    </div>
    <footer class="container-fluid text-center">
  <p>Certificados Actualmente Creados Pstis</p>
</footer>

    <asp:HyperLink ID="lnkBack" runat="server"
        NavigateUrl="~/Group/ListGroups.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <!-- Validator Js -->
    <script src="../js/ValidatorCertificado.js" type="text/javascript"></script>
    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>
    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

</asp:Content>

