﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Applications
Imports System.Data.Common
Imports AjaxControlToolkit
Imports System.Security.Cryptography
Imports log4net


Partial Class Add_Certificado
    Inherits TeleLoader.Web.BasePage

    ' Private Shared Property Log As log4net.ILog

    Dim applicationObj As Application

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            txtDesc.Focus()

            dsCertificado.SelectParameters("groupId").DefaultValue = objSessionParams.intSelectedGroup

            grdCertificado.DataBind()

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub grdCertificado_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdCertificado.RowCommand
        Try

            Select Case e.CommandName

                Case "Delete"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    grdCertificado.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    dsCertificado.DeleteParameters("applicationId").DefaultValue = grdCertificado.SelectedDataKey.Values.Item("apl_id")
                    dsCertificado.DeleteParameters("groupId").DefaultValue = objSessionParams.intSelectedGroup

                    Dim strData As String = "Aplicación ID: " & grdCertificado.SelectedDataKey.Values.Item("apl_id") & ", grupo ID: " & objSessionParams.intSelectedGroup

                    dsCertificado.Delete()

                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Certificado eliminado Correctamente. Datos[ " & strData & " ]", "")

                    grdCertificado.DataBind()

                    pnlError.Visible = False
                    pnlMsg.Visible = True
                    lblMsg.Text = "Certificado eliminado Correctamente"

                    'Delete Physical File
                    Dim filePath = Server.MapPath(ConfigurationSettings.AppSettings.Get("GroupsFolderPath") + objSessionParams.strtSelectedGroup + "/" + grdCertificado.Rows(grdCertificado.SelectedIndex).Cells(1).Text)

                    If File.Exists(filePath) Then
                        File.Delete(filePath)
                        File.Delete(filePath)
                    End If



                    grdCertificado.SelectedIndex = -1

            End Select

            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub

    Protected Sub btnAddCertificado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddCertificado.Click

        Dim strDirectorio As String = ""
        Dim strNombreFile As String = ""

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Save")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        strDirectorio = ConfigurationSettings.AppSettings.Get("GroupsFolderPath")
        Try
            File.Copy(ConfigurationSettings.AppSettings.Get("TempFolder") + objSessionParams.strAppFilename, Server.MapPath(ConfigurationSettings.AppSettings.Get("GroupsFolderPath") + objSessionParams.strAppFilename))
        Catch ex As Exception
            pnlMsg.Visible = False
            pnlError.Visible = True
            lblError.Text = "Error subiendo Certificado, ya se encuentra en :" + "  " + ("C:\REPOSITORIO POLARIS_WEB\HiveTrxServer.Web.Application\MdmFiles" + "\" + objSessionParams.strAppFilename)
            'Set Toggle Visible 
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
            Return
        End Try

        If (objSessionParams.strAppFilename.Equals("")) Then
            pnlMsg.Visible = False
            pnlError.Visible = True
            lblError.Text = "Debe seleccionar un archivo Valido .pem"
            'Set Toggle Visible
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
            Return
        End If
        Try
            applicationObj = New Application(strConnectionString, objSessionParams.intSelectedGroup)

            'Establecer Datos de la Aplicación

            If (objSessionParams.strAppFilename.IndexOf(".pem") > -1) Then
                applicationObj.Desc = txtDesc.Text
                applicationObj.HardDrivePath = strDirectorio + objSessionParams.strAppFilename
                applicationObj.AplData = getAplData(strDirectorio + objSessionParams.strAppFilename)
            End If
            'Save Data
            'If applicationObj.createCertificadoSSL() Then
            '    'New Application Group Created OK
            '    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Certificado Creada en el grupo. Datos[ " & getFormDataLog() & " ]", "")
            '    pnlMsg.Visible = True
            '    pnlError.Visible = False
            '    lblMsg.Text = "Certificado agregada al Grupo correctamente."

            '    dsCertificado.SelectParameters("groupId").DefaultValue = objSessionParams.intSelectedGroup

            '    grdCertificado.DataBind()

            '    clearForm()

            'Else
            '    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error agregando Certificado. Certificado ya Existe. Datos[ " & getFormDataLog() & " ]", "")
            '    pnlError.Visible = True
            '    pnlMsg.Visible = False
            '    lblError.Text = "Certificado ya existe en el grupo. Nombre del Certificado repetido. Por favor elimine antes el certificado."

            'End If

            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub
    'Lectura de Archivo Tms, Almacenando su valor en un string
    Private Function getAplData(ByVal filePath As String) As String
        Dim fileReader As System.IO.StreamReader = Nothing
        Dim sLine As String = ""
        Dim AplData As New ArrayList()
        Dim objReader As New StreamReader("C:\REPOSITORIO POLARIS_WEB\HiveTrxServer.Web.Application\MdmFiles" + "\" + objSessionParams.strAppFilename)
        Dim AplData_String As String = ""
        Dim counter As String = 0
        Try
            Do
                sLine = objReader.ReadLine()
                If Not sLine Is Nothing Then
                    AplData.Add(sLine)
                    AplData_String += sLine

                End If
            Loop Until sLine Is Nothing
            objReader.Close()
        Catch ex As Exception
            AplData_String = ""
        Finally
            If Not objReader Is Nothing Then
                objReader.Close()
            End If

        End Try

        Return AplData_String
    End Function

    Private Sub clearForm()
        txtDesc.Text = ""
    End Sub

    Protected Sub dsCertificado_Deleting(sender As Object, e As SqlDataSourceCommandEventArgs) Handles dsCertificado.Deleting

        'Remover parametro extra, igual al datakeyname
        If e.Command.Parameters.Count > 2 Then
            Dim paramAplId As DbParameter = e.Command.Parameters("@apl_id")
            e.Command.Parameters.Remove(paramAplId)
        End If

    End Sub

    Private Function GetSHA1HashFromFile(ByVal filePath As String) As String
        Dim sha1 As SHA1 = SHA1.Create()
        Dim sb As StringBuilder = New StringBuilder()
        Try
            Using fs As FileStream = File.Open(filePath, FileMode.Open)
                For Each b As Byte In sha1.ComputeHash(fs)
                    sb.Append(b.ToString("x2").ToUpper())
                Next
            End Using
        Catch ex As Exception

        End Try
        Return sb.ToString()
    End Function
End Class

