﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AddGroup.aspx.vb" Inherits="Group_AddGroup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Agregar Grupo
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

    <script type="text/javascript" src="../js/jquery.ui.slider.js"></script>

    <script type="text/javascript" src="../js/jquery.ui.datepicker.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>
    <li>Agregar Grupos</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    Agregar Grupos
<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Group/Manager.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlError" runat="server" Visible="False">
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <div class="titleh">
            <h3>Datos del Grupo</h3>
        </div>
        <div class="body">

            <div class="st-form-line">	
                <span class="st-labeltext">Cliente:</span>	
                <asp:TextBox ID="txtCustomerName" CssClass="st-success-input" style="width:510px" 
                    runat="server" TabIndex="0" MaxLength="100" 
                    ToolTip="Nombre del Cliente." Enabled="False" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>

            <div class="st-form-line">
                <span class="st-labeltext">Nombre Grupo: (*)</span>
                <asp:TextBox ID="txtGroupName" CssClass="st-forminput" Style="width: 510px"
                    runat="server" TabIndex="1" MaxLength="100"
                    ToolTip="Digite nombre del grupo." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>
            <%-- <div class="st-form-line">
                            <span class="st-labeltext-inline">Nombre de Grupo Externo: (*)</span>
                            <asp:DropDownList ID="ddlCodeExterno" class="uniform" runat="server"
                                DataSourceID="dsCodeExterno" DataTextField="cod_ext_nombre"
                                DataValueField="id" Width="200px"
                                ToolTip="Seleccione el Codigo para el Grupo." TabIndex="4">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="dsCodeExterno" runat="server"
                                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" SelectCommand="SELECT -1 AS id, '       ' AS cod_ext_nombre
                                UNION ALL
                                SELECT cod_ext_id, cod_ext_nombre FROM CodigosExternos "></asp:SqlDataSource>
                            <div class="clear"></div>
                        </div>--%>

            <div class="st-form-line">
                <span class="st-labeltext">IP de Descarga: (*)</span>
                <asp:TextBox ID="txtIP" CssClass="st-forminput" Style="width: 100px"
                    runat="server" TabIndex="2" MaxLength="15"
                    ToolTip="Digite IP para descarga remota." onkeydown="return (event.keyCode!=13);"></asp:TextBox> 
                <div class="clear"></div>
            </div>

            <div class="st-form-line">
                <span class="st-labeltext">Puerto de Descarga: (*)</span>
                <asp:TextBox ID="txtPort" CssClass="st-forminput" Style="width: 60px"
                    runat="server" TabIndex="3" MaxLength="5"
                    ToolTip="Digite Puerto TCP para descarga remota." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>

            <div class="st-form-line">	

                <span class="st-labeltext">Modo bloqueante?:</span>	                

                <asp:Literal ID="ltrBlockingMode" runat="server"></asp:Literal>
                
                <asp:HiddenField ID="groupFlagBlockingMode" runat="server" Value="0" />

                <div class="clear"></div>
            </div>

            <div class="st-form-line">	

                <span class="st-labeltext">Actualizacion inmediata?:</span>	                

                <asp:Literal ID="ltrUpdateNow" runat="server"></asp:Literal>
                
                <asp:HiddenField ID="groupFlagUpdateNow" runat="server" Value="0" />

                <div class="clear"></div>
            </div>

            <div class="st-form-line">
                <span class="st-labeltext">Descripci&oacute;n:</span>
                <asp:TextBox ID="txtDesc" CssClass="st-forminput" Style="width: 510px"
                    runat="server" TabIndex="4" MaxLength="250" TextMode="MultiLine"
                    ToolTip="Digite descripción del grupo." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>

            <div class="st-form-line">	
                <span class="st-labeltext">Actualizar Terminales?:</span>	                

                <asp:Literal ID="ltrUpdate" runat="server"></asp:Literal>
                
                <asp:HiddenField ID="groupFlagUpdate" runat="server" Value="1" />

                <div class="clear"></div>
            </div>

            <div class="st-form-line">	
                <span class="st-labeltext">Usar Rango de Fechas?:</span>	                

                <asp:Literal ID="ltrDateRange" runat="server"></asp:Literal>
                
                <asp:HiddenField ID="groupFlagDateRange" runat="server" Value="0" />

                <div class="clear">
                </div>
            </div>

            <asp:Panel ID="pnlDateRange" runat="server" Visible="False">
                <div class="st-form-line" style="text-align: center;">
                    <span>Fecha Inicial: (*)&nbsp;&nbsp;</span>
                    <asp:TextBox ID="txtStartDate" CssClass="datepicker-input"
                        onkeydown="return false;" runat="server" Font-Bold="True" Width="100px"
                        ToolTip="Fecha Inicial" TabIndex="5"></asp:TextBox>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            	    <span>Fecha Final: (*)&nbsp;&nbsp;</span>
                    <asp:TextBox ID="txtEndDate" CssClass="datepicker-input"
                        onkeydown="return false;" runat="server" Font-Bold="True" Width="100px"
                        ToolTip="Fecha Inicial" TabIndex="6"></asp:TextBox>
                    <div class="clear"></div>
                </div>
            </asp:Panel>

            <div class="st-form-line">	
                <span class="st-labeltext">Rango de Horas: (*)</span>

                <asp:TextBox ID="txtRange1" CssClass="st-forminput" Style="width: 40px"
                    runat="server" TabIndex="7" MaxLength="5"
                    ToolTip="Rango Inicial de Hora" Text="00:00" Font-Bold="True" onkeydown="return false;"></asp:TextBox>
                -
                <asp:TextBox ID="txtRange2" CssClass="st-forminput" Style="width: 40px"
                    runat="server" TabIndex="8" MaxLength="5"
                    ToolTip="Rango Final de Hora" Text="23:45" Font-Bold="True" onkeydown="return false;"></asp:TextBox>

                <div id="slider-range" class="margin-top10 ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" style="z-index: 640; width: 68%; float:right;">
                    <div class="ui-slider-range ui-widget-header" style="left: 0%; width: 0%;"></div>
                    <a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 0%;"></a>
                    <a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 0%;"></a>
                </div>
                <div class="clear"></div>
            </div>

            <div class="button-box">
                <asp:Button ID="btnSave" runat="server" Text="Crear Grupo"
                    CssClass="button-aqua" TabIndex="9" OnClientClick="return validateAddOrEditGroup()" />
            </div>
        </div>
    </div>

    <asp:HyperLink ID="lnkBack" runat="server" NavigateUrl="~/Group/Manager.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <!-- Validator -->
    <script src="../js/ValidatorGroup.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

</asp:Content>

