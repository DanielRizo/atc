﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users
Imports System.Net
Imports System.Globalization
Imports TeleLoader.Groups

Partial Class Group_AddGroup
    Inherits TeleLoader.Web.BasePage

    Dim groupObj As Group

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim startDate As Date
        Dim endDate As Date
        Dim rangeHour1 As Integer
        Dim rangeHour2 As Integer

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Save")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        startDate = Date.Parse(txtStartDate.Text & " " & txtRange1.Text, New CultureInfo("es-CO"))
        endDate = Date.Parse(txtEndDate.Text & " " & txtRange2.Text, New CultureInfo("es-CO"))

        'Validate Date
        If startDate <= endDate Then
            'Validate Hour Range
            rangeHour1 = Integer.Parse(txtRange1.Text.Trim.Replace(":", ""))
            rangeHour2 = Integer.Parse(txtRange2.Text.Trim.Replace(":", ""))

            If rangeHour1 <= rangeHour2 Then

                groupObj = New Group(strConnectionString)

                'Establecer Datos de Grupo
                groupObj.Name = txtGroupName.Text
                'groupObj.CodeExtern = ddlCodeExterno.SelectedValue
                groupObj.Desc = txtDesc.Text
                groupObj.IpAddress = txtIP.Text
                groupObj.Port = txtPort.Text
                groupObj.allowUpdate = groupFlagUpdate.Value
                groupObj.useDateRange = groupFlagDateRange.Value

                groupObj.RangeDateIni = startDate
                groupObj.RangeDateEnd = endDate
                groupObj.RangeHourIni = startDate
                groupObj.RangeHourEnd = endDate

                groupObj.CustomerCode = objAccessToken.CustomerUserID

                groupObj.BlockingMode = groupFlagBlockingMode.Value

                groupObj.UpdateNow = groupFlagUpdateNow.Value
                'APLICACIONES PERMITIDAS MDM

                groupObj.ApplicationsDefault = ConfigurationSettings.AppSettings.Get("AllowedAppsDefault")

                'Save Data
                If groupObj.createGroup() Then
                    'New Group Created OK
                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Grupo Creado. Datos[ " & getFormDataLog() & " ]", "")

                    pnlMsg.Visible = True
                    pnlError.Visible = False
                    lblMsg.Text = "Grupo creado correctamente."

                    clearForm()
                Else
                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Creando Grupo. Datos[ " & getFormDataLog() & " ]", "")
                    pnlError.Visible = True
                    pnlMsg.Visible = False
                    lblError.Text = "Error creando grupo. Por favor valide los datos."
                End If
            Else
                pnlError.Visible = True
                pnlMsg.Visible = False
                lblError.Text = "La hora inicial del rango debe ser menor o igual que la hora final."
            End If
        Else
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "La fecha inicial del rango debe ser menor o igual que la fecha final."
        End If

    End Sub

    Private Sub clearForm()
        txtGroupName.Text = ""
        'ddlCodeExterno.SelectedIndex = -1
        txtIP.Text = ""
        txtPort.Text = ""
        txtDesc.Text = ""
        txtStartDate.Text = Now.ToString("dd/MM/yyyy")
        txtEndDate.Text = Now.ToString("dd/MM/yyyy")

        setSwitchUpdateData(1)
        setSwitchDateRangeData(0)
        setSwitchBlockingMode(0)
        setSwitchUpdateNow(0)

        txtRange1.Text = "00:00"
        txtRange2.Text = "23:45"

        setSliderData()

        pnlDateRange.Visible = False

        txtGroupName.Focus()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then
            txtCustomerName.Text = objAccessToken.Customer
            txtStartDate.Text = Now.ToString("dd/MM/yyyy")
            txtEndDate.Text = Now.ToString("dd/MM/yyyy")

            setSwitchUpdateData(1)
            setSwitchDateRangeData(0)
            setSwitchBlockingMode(0)
            setSwitchUpdateNow(0)

            txtGroupName.Focus()
        Else
            Dim CtrlID As String = String.Empty

            If Request.Form("__EVENTTARGET") IsNot Nothing And
               Request.Form("__EVENTTARGET") <> String.Empty Then
                CtrlID = Request.Form("__EVENTTARGET")
            End If

            If CtrlID = "groupFlagDateRangeYES" Then
                pnlDateRange.Visible = True
            End If

            If CtrlID = "groupFlagDateRangeNO" Then
                pnlDateRange.Visible = False
            End If

            setSwitchUpdateData(groupFlagUpdate.Value)
            setSwitchDateRangeData(groupFlagDateRange.Value)
            setSwitchBlockingMode(groupFlagBlockingMode.Value)
            setSwitchUpdateNow(groupFlagUpdateNow.Value)

        End If

        setSliderData()

    End Sub

    Private Sub setSwitchUpdateData(flagUpdate As Integer)
        Dim htmlSwitch As String

        If flagUpdate = 1 Then
            htmlSwitch = "<p class='field switch' id='switchUpdate'><label for='radio1' id='radio11' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio2' id='radio12' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='switchUpdate'><label for='radio1' id='radio11' class='cb-enable'><span>S&iacute;</span></label><label for='radio2' id='radio12' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrUpdate.Text = htmlSwitch

    End Sub

    Private Sub setSwitchDateRangeData(flagDateRange As Integer)
        Dim htmlSwitch As String

        If flagDateRange = 1 Then
            htmlSwitch = "<p class='field switch' id='switchDateRange'><label for='radio3' id='radio13' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio4' id='radio14' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='switchDateRange'><label for='radio3' id='radio13' class='cb-enable'><span>S&iacute;</span></label><label for='radio4' id='radio14' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrDateRange.Text = htmlSwitch

    End Sub

    Private Sub setSwitchBlockingMode(flagBlockingMode As Boolean)
        Dim htmlSwitch As String

        If flagBlockingMode = True Then
            htmlSwitch = "<p class='field switch' id='switchBlockingMode'><label for='radio11' id='radio21' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio12' id='radio22' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='switchBlockingMode'><label for='radio11' id='radio21' class='cb-enable'><span>S&iacute;</span></label><label for='radio12' id='radio22' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrBlockingMode.Text = htmlSwitch

    End Sub

    Private Sub setSwitchUpdateNow(flagUpdateNow As Boolean)
        Dim htmlSwitch As String

        If flagUpdateNow = True Then
            htmlSwitch = "<p class='field switch' id='switchUpdateNow'><label for='radio11' id='radio23' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio12' id='radio24' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='switchUpdateNow'><label for='radio11' id='radio23' class='cb-enable'><span>S&iacute;</span></label><label for='radio12' id='radio24' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrUpdateNow.Text = htmlSwitch

    End Sub

    Private Sub setSliderData()
        Dim htmlSlider As String = ""
        Dim hour1, hour2 As Integer
        Dim minute1, minute2 As Integer
        Dim val1, val2, val3, val4 As Integer
        Dim interval As Integer = 15

        hour1 = Integer.Parse(txtRange1.Text.Trim.Split(":")(0))
        minute1 = Integer.Parse(txtRange1.Text.Trim.Split(":")(1))
        hour2 = Integer.Parse(txtRange2.Text.Trim.Split(":")(0))
        minute2 = Integer.Parse(txtRange2.Text.Trim.Split(":")(1))

        val1 = (hour1 * 60) / interval
        val2 = (minute1 Mod 60) / interval

        val3 = (hour2 * 60) / interval
        val4 = (minute2 Mod 60) / interval

        htmlSlider = "<script language='javascript'>$(function() { $('#slider-range').slider({ range: true, min: 0, max: 95, values: [" & (val1 + val2) & "," & (val3 + val4) & "], slide: function (event, ui) { var interval = 15; var hour1 = (ui.values[0] * interval) / 60;	var minute1 = (ui.values[0] * interval) % 60; var hour2 = (ui.values[1] * interval) / 60; var minute2 = (ui.values[1] * interval) % 60; $('#ctl00_MainContent_txtRange1').val(zeroPad(parseInt(hour1), 2) + ':' + zeroPad(parseInt(minute1), 2)); $('#ctl00_MainContent_txtRange2').val(zeroPad(parseInt(hour2), 2) + ':' + zeroPad(parseInt(minute2), 2)); }}); });</script>"

        ltrScript.Text = htmlSlider
    End Sub

End Class
