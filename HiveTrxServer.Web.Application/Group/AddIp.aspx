﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageCSS.master" AutoEventWireup="false" CodeFile="AddIp.aspx.vb" Inherits="Groups_AddIp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Adicionar Ip
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

    <link rel="stylesheet" type="text/css" href="/style/bootstrap.min.css" />

    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/editTable.js") %>'></script>

    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.tabledit.js") %>'></script>

    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/bootstrap.js") %>'></script>

    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/SaveTable.js") %>'></script>

    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/TypeInput.js") %>'></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>

    <li>Agregar Nueva Ip...</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <blockquote>
        <p>Este módulo permite Crear nuevas IP:</p>
        <footer class="blockquote-footer">Polaris Cloud Service (Pstis) </footer>
    </blockquote>
    <style>
        .bg-1 {
            background-color: #1abc9c;
            color: #ffffff;
        }
    </style>

    <div class="bg-1">
        <div class="container text-center">
            <h3>Creación de ip's Pstis</h3>
            <img src="https://cdn0.iconfinder.com/data/icons/navigation-13/256/18-512.png" class="img-circle" alt="Bird" width="380" height="300">
        </div>
    </div>
    <div class="container-fluid bg-2 text-center">
        <h3>Creacion y edicion de ip's</h3>
        <p>Este modulo permite crear nuevas ip's, una vez sean editados se podran Agregar al Tree View de las terminales. </p>
    </div>
    <ul id="navst">
        <li class="navbar navbar-light"><a href="TerminalStis.aspx">PSTIS</a></li>
        "
            <li><a>Tables</a>
                <ul>
                    <li><a href="TerminalAcquirer.aspx">Acquirer</a></li>
                    <li><a href="TerminalIssuer.aspx">Issuer</a>	</li>
                    <li><a href="TerminalCardRange.aspx">CardRange</a></li>
                    <li><a href="TerminalEmvLevel2.aspx">Emv Level 2 Application</a></li>
                    <li><a href="TerminalEmvKey.aspx">Emv Level 2 Key</a></li>
                    <li><a href="TerminalExtraApplication.aspx">Extra Application Parameter</a></li>
                    <li><a href="ListarGroupPrompts.aspx">Grupos Prompts</a></li>
                    <li><a href="ListarGroupVariousPayments.aspx">Grupos Pagos Varios</a></li>
                    <li><a href="ListarGroupElectronicsPayments.aspx">Grupos Pagos Electronicos</a></li>
                    <li><a href="TerminalHostConfiguration.aspx">Host Configuration</a></li>
                    <li><a href="TerminalPrompts.aspx">Prompts</a></li>
                    <li><a href="PagosVarios.aspx">Pagos Varios</a></li>
                    <li><a href="PagosElectronicos.aspx">Pagos Electronicos</a></li>
                    <li><a href="IP.aspx">Ip</a></li>
                    <li><a href="AddCertificado.aspx">Certificados SSL</a></li>
                </ul>
            </li>
        <li><a>Terminal</a>
            <ul>
                <li><a href="AddTerminalStis.aspx">New Terminal</a></li>
                <li><a href="CopyTerminalStis.aspx">Copy Terminal</a></li>

            </ul>
        </li>
    </ul>

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
        <div id="succes2" style="display: none;">
            <asp:Panel ID="Panel1" runat="server">
                <div class="alert  alert-success">
                    <asp:Label ID="lblMsg2" runat="server" Text="Datos editados correctamente."></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">X</a>
                </div>
            </asp:Panel>
        </div>
        <asp:Panel ID="pnlMsg" runat="server" Visible="False">
            <div class="alert alert-warning">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">X</a>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlError" runat="server" Visible="False">
            <div class="alert alert-warning">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">X</a>
            </div>
        </asp:Panel>
    </div>

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
        <div>
            <div class="shortcuts-icons" style="z-index: 660;">
                <br />
            </div>
        </div>
        <div>
            <h3 class="title">Agregar Nueva Ip
                <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
            <div>
                <div class="st-form-line">
                    <div class="st-form-line">
                        <span class="st-labeltext">Code: (*)</span>
                        <input id="txtCode" type="text" maxlength="5" onkeypress="return numeric_only (event, this);" />

                        <div class="clear"></div>
                    </div>
                    <small id="passwordHelpInline3" class="text-muted">Must be 5 characters long.
                    </small>
                    <div class="st-form-line">
                        <span class="st-labeltext">Display Name: (*)</span>
                        <input id="txtName" type="text" maxlength="25" />
                        <div class="clear"></div>
                    </div>
                    <small id="passwordHelpInline7" class="text-muted">Must be 25 characters long.
                    </small>
                    <div class="st-form-line">
                        <span class="st-labeltext">Description: (*)</span>
                        <input id="txtDescription" type="text" maxlength="25" />
                        <div class="clear"></div>
                    </div>
                    <small id="passwordHelpInline4" class="text-muted">Must be 25 characters long.
                    </small>
                    <div class="button-box">
                    </div>
                </div>
            </div>
        </div>
        <center>
            <input type="button" class="btn btn-info" id="initTerminals" value="Crear IP" style="margin: 15px  3px;" />
        </center>
        <script type="text/javascript">

            $('#initTerminals').on('click', function (event) {
                $('#initTerminals').prop('disabled', true);
                ShowProgressWindow();
                event.preventDefault();
                RecorrerTable("#ctl00_MainContent_grdAddIp", "addIpConfigurationTbl");
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                enableEditTable("#ctl00_MainContent_grdAddIp", true, false, true, false, [[2, 'Value']], "addIpConfigurationTbl");
            });
        </script>
        <div class="body">
            <div id="posStis" style="overflow: auto; width: auto; height: 660px;">
                <br />
                <asp:GridView ID="grdAddIp" runat="server"
                    AllowSorting="True" AutoGenerateColumns="False"
                    DataSourceID="dsAddIp"
                    CssClass="table table-responsive-lg jumbotron table-light table-hover table-bordered"
                    HeaderStyle-ForeColor="WhiteSmoke"
                    HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                    HeaderStyle-CssClass="table-bordered bg-info" DATAKEYNAME="IS_EXTENDED"
                    EmptyDataText="No existen Ip's creadas con el filtro aplicado"
                    HorizontalAlign="Left">
                    <Columns>
                        <asp:BoundField DataField="IS_EXTENDED" HeaderText="Parameter Type"
                            SortExpression="IS_EXTENDED" ReadOnly="True" />
                        <asp:BoundField DataField="FIELD_DISPLAY_NAME" HeaderText="Parameter Name"
                            SortExpression="FIELD_DISPLAY_NAME" />
                        <asp:BoundField DataField="CONTENT_DESC" HeaderText="Value"
                            SortExpression="CONTENT_DESC" />
                        <asp:BoundField DataField="CONTENT_TYPE" HeaderText="Value Type"
                            SortExpression="CONTENT_TYPE" ReadOnly="True" />
                        <asp:BoundField DataField="NOTE" HeaderText="Note"
                            SortExpression="NOTE" ReadOnly="True" />
                    </Columns>
                    <PagerStyle
                        CssClass="pgr" />
                    <HeaderStyle />
                    <EditRowStyle BorderColor="#666666" BorderStyle="Solid"
                        BorderWidth="1px" />
                </asp:GridView>
                <br />
            </div>
            <br />
            <asp:SqlDataSource ID="dsAddIp" runat="server"
                ConnectionString="<%$ ConnectionStrings:TeleLoaderStisConnectionString %>"
                SelectCommand="sp_webAddIp_Stis" SelectCommandType="StoredProcedure"
                UpdateCommand="sp_webEditDatosAcquirerNew_Stis" UpdateCommandType="StoredProcedure">
                <SelectParameters>
                    <asp:Parameter Name="IS_EXTENDED" Type="string" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="FIELD_DISPLAY_NAME" Type="String" />
                    <asp:Parameter Name="CONTENT_DESC" Type="String" />
                </UpdateParameters>
            </asp:SqlDataSource>
        </div>
    </div>
    <asp:HyperLink ID="HyperLink1" runat="server"
        NavigateUrl="~/Group/IP.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>
    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    <script language="javascript">
        var globalcontrol = '';

    </script>

</asp:Content>

