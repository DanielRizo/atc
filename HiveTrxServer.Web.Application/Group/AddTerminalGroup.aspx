﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AddTerminalGroup.aspx.vb" Inherits="Group_AddTerminalGroup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Agregar Terminal al Grupo
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

    <script type="text/javascript" src="../js/jquery.ui.slider.js"></script>

    <script type="text/javascript" src="../js/jquery.ui.datepicker.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkListGroups" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/ListGroups.aspx">Listar Grupos</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkGroupTerminals" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/GroupTerminals.aspx">Terminales del Grupo</asp:LinkButton>
    </li>
    <li>Agregar Terminal</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    Agregar Terminal
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlError" runat="server" Visible="False">
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <div class="titleh">
            <h3>Datos de la Terminal</h3>
        </div>
        <div class="body">

            <div class="st-form-line">	
                <span class="st-labeltext">Grupo:</span>	
                <asp:TextBox ID="txtGroupName" CssClass="st-success-input" style="width:510px" 
                    runat="server" TabIndex="0" MaxLength="100" 
                    ToolTip="Nombre del Grupo." Enabled="False" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>

            <div class="st-form-line">
                <span class="st-labeltext">Serial: (*)</span>
                <asp:TextBox ID="txtSerial" CssClass="st-forminput" Style="width: 150px"
                    runat="server" TabIndex="1" MaxLength="20"
                    ToolTip="Digite serial de la terminal." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>

            <table width="100%">
                <tr>
                    <td>
                        <div class="st-form-line">
                            <span class="st-labeltext-inline">Tipo: (*)</span>
                            <asp:DropDownList ID="ddlTerminalType" class="uniform" runat="server"
                                DataSourceID="dsTerminalType" DataTextField="descripcion"
                                DataValueField="id" Width="200px"
                                ToolTip="Seleccione el tipo de terminal." TabIndex="2">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="dsTerminalType" runat="server"
                                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" SelectCommand="SELECT -1 AS id, '       ' AS descripcion
                                UNION ALL
                                SELECT id, descripcion FROM TipoTerminal"></asp:SqlDataSource>
                            <div class="clear"></div>
                        </div>
                    </td>
                    <td>
                        <div class="st-form-line">
                            <span class="st-labeltext-inline">Marca: (*)</span>
                            <asp:DropDownList ID="ddlTerminalBrand" class="uniform" runat="server"
                                DataSourceID="dsTerminalBrand" DataTextField="descripcion"
                                DataValueField="id" Width="200px"
                                ToolTip="Seleccione la marca de la terminal." TabIndex="3">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="dsTerminalBrand" runat="server"
                                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" SelectCommand="SELECT -1 AS id, '       ' AS descripcion
                                UNION ALL
                                SELECT id, descripcion FROM MarcaTerminal"></asp:SqlDataSource>
                            <div class="clear"></div>
                        </div>
                    </td>
                    <td>
                        <div class="st-form-line">
                            <span class="st-labeltext-inline">Modelo: (*)</span>
                            <asp:DropDownList ID="ddlTerminalModel" class="uniform" runat="server"
                                DataSourceID="dsTerminalModel" DataTextField="descripcion"
                                DataValueField="id" Width="200px"
                                ToolTip="Seleccione el modelo de la terminal." TabIndex="4">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="dsTerminalModel" runat="server"
                                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" SelectCommand="SELECT -1 AS id, '       ' AS descripcion
                                UNION ALL
                                SELECT id, descripcion FROM ModeloTerminal"></asp:SqlDataSource>
                            <div class="clear"></div>
                        </div>
                    </td>
                    <td>
                        <div class="st-form-line">
                            <span class="st-labeltext-inline">Tipo de Comunicación: (*)</span>
                            <asp:DropDownList ID="ddlTerminalInterface" class="uniform" runat="server"
                                DataSourceID="dsTerminalInterface" DataTextField="descripcion"
                                DataValueField="id" Width="200px"
                                ToolTip="Seleccione la interface de comunicación de la terminal." TabIndex="5">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="dsTerminalInterface" runat="server"
                                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" SelectCommand="SELECT -1 AS id, '       ' AS descripcion
                                UNION ALL
                                SELECT id, descripcion FROM InterfaceTerminal"></asp:SqlDataSource>
                            <div class="clear"></div>
                        </div>
                    </td>
                </tr>
            </table>

            <table width="100%">
                <tr>
                    <td>
                        <div class="st-form-line">
                            <span class="st-labeltext-inline">Serial SIM:</span>
                            <asp:TextBox ID="txtSerialSIM" CssClass="st-forminput" Style="width: 150px"
                                runat="server" TabIndex="6" MaxLength="20"
                                ToolTip="Digite serial del SIM." onkeydown="return (event.keyCode!=13);"></asp:TextBox> 
                            <div class="clear"></div>
                        </div>
                    </td>
                    <td>
                        <div class="st-form-line">
                            <span class="st-labeltext-inline">Tel&eacute;fono SIM:</span>
                            <asp:TextBox ID="txtMobileSIM" CssClass="st-forminput" Style="width: 150px"
                                runat="server" TabIndex="7" MaxLength="20"
                                ToolTip="Digite teléfono celular del SIM." onkeydown="return (event.keyCode!=13);"></asp:TextBox> 
                            <div class="clear"></div>
                        </div>
                    </td>
                    <td>
                        <div class="st-form-line">
                            <span class="st-labeltext-inline">IMEI:</span>
                            <asp:TextBox ID="txtIMEI" CssClass="st-forminput" Style="width: 150px"
                                runat="server" TabIndex="8" MaxLength="20"
                                ToolTip="Digite IMEI del Módem." onkeydown="return (event.keyCode!=13);"></asp:TextBox> 
                            <div class="clear"></div>
                        </div>
                    </td>
                </tr>
            </table>

            <table width="100%">
                <tr>
                    <td>
                        <div class="st-form-line">
                            <span class="st-labeltext-inline">Serial Cargador:</span>
                            <asp:TextBox ID="txtChargerSerial" CssClass="st-forminput" Style="width: 150px"
                                runat="server" TabIndex="9" MaxLength="20"
                                ToolTip="Digite serial del cargador." onkeydown="return (event.keyCode!=13);"></asp:TextBox> 
                            <div class="clear"></div>
                        </div>
                    </td>
                    <td>
                        <div class="st-form-line">
                            <span class="st-labeltext-inline">Serial Bater&iacute;a:</span>
                            <asp:TextBox ID="txtBatterySerial" CssClass="st-forminput" Style="width: 150px"
                                runat="server" TabIndex="10" MaxLength="20"
                                ToolTip="Digite serial del cargador." onkeydown="return (event.keyCode!=13);"></asp:TextBox> 
                            <div class="clear"></div>
                        </div>
                    </td>
                    <td>
                        <div class="st-form-line">
                            <span class="st-labeltext-inline">Serial Lector Biom&eacute;trico:</span>
                            <asp:TextBox ID="txtBiometricSerial" CssClass="st-forminput" Style="width: 150px"
                                runat="server" TabIndex="11" MaxLength="20"
                                ToolTip="Digite serial del cargador." onkeydown="return (event.keyCode!=13);"></asp:TextBox> 
                            <div class="clear"></div>
                        </div>
                    </td>
                </tr>
            </table>

            <div class="st-form-line">
                <span class="st-labeltext">Descripci&oacute;n:</span>
                <asp:TextBox ID="txtDesc" CssClass="st-forminput" Style="width: 510px"
                    runat="server" TabIndex="12" MaxLength="250" TextMode="MultiLine"
                    ToolTip="Digite descripción del grupo." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>

            <div class="st-form-line">	
                <span class="st-labeltext">Prioridad Grupo?:</span>	                

                <asp:Literal ID="ltrPriority" runat="server"></asp:Literal>
                
                <asp:HiddenField ID="terminalFlagPriority" runat="server" Value="1" />

                <div class="clear"></div>
            </div>

            <asp:Panel ID="pnlUpdateTerminal" runat="server" Visible="False">
                <div class="st-form-line">	
                    <span class="st-labeltext">Actualizar Terminal?:</span>

                    <asp:Literal ID="ltrUpdate" runat="server"></asp:Literal>
                
                    <asp:HiddenField ID="terminalFlagUpdate" runat="server" Value="0" />

                    <div class="clear">
                    </div>
                </div>
            </asp:Panel>

            <asp:Panel ID="pnlSchedulePerTerminal" runat="server" Visible="False">
                <div class="st-form-line" style="text-align: center;">
                    <span>Fecha de Programaci&oacute;n: (*)&nbsp;&nbsp;</span>
                    <asp:TextBox ID="txtScheduleDate" CssClass="datepicker-input"
                        onkeydown="return false;" runat="server" Font-Bold="True" Width="100px"
                        ToolTip="Fecha de Programación" TabIndex="13"></asp:TextBox>
                    <div class="clear"></div>
                </div>

                <div class="st-form-line">	
                    <span class="st-labeltext">Hora: (*)</span>

                    <asp:TextBox ID="txtRange1" CssClass="st-forminput" Style="width: 40px"
                        runat="server" TabIndex="14" MaxLength="5"
                        ToolTip="Hora de Programación" Text="00:00" Font-Bold="True" onkeydown="return false;"></asp:TextBox>

                    <div id="slider-range" class="margin-top10 ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" style="z-index: 640; width: 68%; float:right;">
                        <div class="ui-slider-range ui-widget-header" style="left: 0%; width: 0%;"></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="st-form-line">
                    <span class="st-labeltext">IP de Descarga: (*)</span>
                    <asp:TextBox ID="txtIP" CssClass="st-forminput" Style="width: 100px"
                        runat="server" TabIndex="15" MaxLength="15"
                        ToolTip="Digite IP para descarga remota." onkeydown="return (event.keyCode!=13);"></asp:TextBox> 
                    <div class="clear"></div>
                </div>

                <div class="st-form-line">
                    <span class="st-labeltext">Puerto de Descarga: (*)</span>
                    <asp:TextBox ID="txtPort" CssClass="st-forminput" Style="width: 60px"
                        runat="server" TabIndex="16" MaxLength="5"
                        ToolTip="Digite Puerto TCP para descarga remota." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>

                <div class="st-form-line">	

                        <span class="st-labeltext">Modo Bloqueante?:</span>

                        <asp:Literal ID="ltrBlockingMode" runat="server"></asp:Literal>
                
                        <asp:HiddenField ID="terminalFlagBlockingMode" runat="server" Value="0" />

                        <div class="clear"></div>
                </div>

                <div class="st-form-line">	

                            <span class="st-labeltext">Actualizacion inmediata?:</span>

                            <asp:Literal ID="ltrUpdateNow" runat="server"></asp:Literal>
                
                            <asp:HiddenField ID="terminalFlagUpdateNow" runat="server" Value="0" />

                        <div class="clear">
                    </div>
                </div>



            </asp:Panel>

            <div class="toggle-message" style="z-index: 590; top: 0px; left: 0px;">
                <h3 class="title">Validaciones On-Line
                    <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
                <div class="hide-message" id="hide-message" style="display: none;">

                    <div class="st-form-line">	
                        <span class="st-labeltext">Validar IMEI?:</span>	                

                        <asp:Literal ID="ltrIMEI" runat="server"></asp:Literal>
                
                        <asp:HiddenField ID="terminalIMEIFlag" runat="server" Value="0" />

                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">	
                        <span class="st-labeltext">Validar Serial SIM?:</span>

                        <asp:Literal ID="ltrSIM" runat="server"></asp:Literal>
                
                        <asp:HiddenField ID="terminalSIMFlag" runat="server" Value="0" />

                        <div class="clear">
                        </div>
                    </div>

                </div>
            </div>

            <div class="toggle-message" style="z-index: 590; top: 0px; left: 0px;">
                <h3 class="title">Datos de Contacto (Opcional)
                    <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
                <div class="hide-message" id="hide-message2" style="display: none;">

                    <div class="st-form-line">
                        <span class="st-labeltext">N&uacute;mero de Identificaci&oacute;n:</span>
                        <asp:TextBox ID="txtContactNumId" CssClass="st-forminput" Style="width: 100px"
                            runat="server" TabIndex="17" MaxLength="14"
                            ToolTip="Digite número de identificación." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnQueryContactData" runat="server" Text="Consultar Datos Existentes"
                            CssClass="button-aqua" TabIndex="18" />
                        &nbsp;&nbsp;
                        <asp:Button ID="btnDeletecontactData" runat="server" Text="Limpiar"
                            CssClass="st-button" TabIndex="19" />
                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">
                        <span class="st-labeltext">Tipo de Identificaci&oacute;n:</span>
                        <asp:DropDownList ID="ddlContactIdType" class="uniform" runat="server"
                            DataSourceID="dsContactIdType" DataTextField="descripcion"
                            DataValueField="id" Width="200px"
                            ToolTip="Seleccione el tipo de identificación." TabIndex="20" Enabled="False">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="dsContactIdType" runat="server"
                            ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" SelectCommand="SELECT -1 AS id, '       ' AS descripcion
                            UNION ALL
                            SELECT * FROM TipoIdentificacion"></asp:SqlDataSource>
                        <div class="clear"></div>
                    </div>
            
                    <div class="st-form-line">
                        <span class="st-labeltext">Nombre:</span>
                        <asp:TextBox ID="txtContactName" CssClass="st-forminput" Style="width: 510px"
                            runat="server" TabIndex="21" MaxLength="100"
                            ToolTip="Digite nombre del contacto." onkeydown="return (event.keyCode!=13);" Enabled="False"></asp:TextBox>
                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">
                        <span class="st-labeltext">Direcci&oacute;n:</span>
                        <asp:TextBox ID="txtContactAddress" CssClass="st-forminput" Style="width: 510px"
                            runat="server" TabIndex="22" MaxLength="100"
                            ToolTip="Digite dirección del contacto." onkeydown="return (event.keyCode!=13);" Enabled="False"></asp:TextBox>
                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">
                        <span class="st-labeltext">Celular:</span>
                        <asp:TextBox ID="txtContactMobile" CssClass="st-forminput" Style="width: 100px"
                            runat="server" TabIndex="23" MaxLength="20"
                            ToolTip="Digite celular del contacto." onkeydown="return (event.keyCode!=13);" Enabled="False"></asp:TextBox>
                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">
                        <span class="st-labeltext">Tel&eacute;fono Fijo:</span>
                        <asp:TextBox ID="txtContactPhone" CssClass="st-forminput" Style="width: 100px"
                            runat="server" TabIndex="24" MaxLength="20"
                            ToolTip="Digite teléfono fijo del contacto." onkeydown="return (event.keyCode!=13);" Enabled="False"></asp:TextBox>
                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">
                        <span class="st-labeltext">Correo Electr&oacute;nico:</span>
                        <asp:TextBox ID="txtContactEmail" CssClass="st-forminput" Style="width: 510px"
                            runat="server" TabIndex="25" MaxLength="100"
                            ToolTip="Digite correo electrónico del usuario." onkeydown="return (event.keyCode!=13);" Enabled="False"></asp:TextBox>
                        <div class="clear"></div>
                    </div>

                </div>
            </div>

            <div class="button-box">
                <asp:Button ID="btnSave" runat="server" Text="Crear Terminal"
                    CssClass="button-aqua" TabIndex="26" OnClientClick="return validateAddOrEditGroupTerminal()" />
            </div>
        </div>
    </div>

    <asp:HyperLink ID="lnkBack" runat="server" NavigateUrl="~/Group/GroupTerminals.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <!-- Validator -->
    <script src="../js/ValidatorGroupTerminal.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

</asp:Content>

