﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users
Imports System.Net
Imports System.Globalization
Imports TeleLoader.Terminals

Partial Class Group_AddTerminalGroup
    Inherits TeleLoader.Web.BasePage

    Dim terminalObj As Terminal
    Dim contactObj As TeleLoader.Contact

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim scheduledDate As Date

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Save")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        scheduledDate = Date.Parse(txtScheduleDate.Text & " " & txtRange1.Text, New CultureInfo("es-CO"))

        terminalObj = New Terminal(strConnectionString, objSessionParams.intSelectedGroup)

        'Establecer Datos de la Terminal
        terminalObj.TerminalSerial = txtSerial.Text.Trim()
        terminalObj.TerminalType = ddlTerminalType.SelectedValue
        terminalObj.TerminalBrand = ddlTerminalBrand.SelectedValue
        terminalObj.TerminalModel = ddlTerminalModel.SelectedValue
        terminalObj.TerminalInterface = ddlTerminalInterface.SelectedValue
        terminalObj.SerialSIM = txtSerialSIM.Text.Trim()
        terminalObj.MobileSIM = txtMobileSIM.Text.Trim()
        terminalObj.IMEI = txtIMEI.Text.Trim()
        terminalObj.ChargerSerial = txtChargerSerial.Text.Trim()
        terminalObj.BatterySerial = txtBatterySerial.Text.Trim()
        terminalObj.BiometricSerial = txtBiometricSerial.Text.Trim()
        terminalObj.Desc = txtDesc.Text.Trim()

        If terminalFlagPriority.Value = 1 Then
            'Programación por Grupo
            terminalObj.GroupPriority = True
            terminalObj.UpdateTerminal = False
            terminalObj.ScheduleUpdate = scheduledDate
            terminalObj.IPAddress = ""
            terminalObj.Port = ""

        Else
            'Programación por Terminal
            terminalObj.GroupPriority = False

            terminalObj.BlockingMode = terminalFlagBlockingMode.Value
            terminalObj.UpdateNow = terminalFlagUpdateNow.Value

            terminalObj.ScheduleUpdate = scheduledDate

            If terminalFlagUpdate.Value = 1 Then
                'Actualizar Terminal Manual
                terminalObj.UpdateTerminal = True
                terminalObj.IPAddress = txtIP.Text.Trim()
                terminalObj.Port = txtPort.Text.Trim()
            Else
                terminalObj.UpdateTerminal = False
                terminalObj.IPAddress = ""
                terminalObj.Port = ""
            End If
        End If

        'Other Validations
        terminalObj.ValidateIMEI = terminalIMEIFlag.Value
        terminalObj.ValidateSIM = terminalSIMFlag.Value

        'Validar si es necesario el contacto
        contactObj = New TeleLoader.Contact()

        If (txtContactNumId.Text.Trim.Length > 0) Then
            contactObj.IdentificationNumber = txtContactNumId.Text.Trim
            contactObj.IdentificationType = ddlContactIdType.SelectedValue
            contactObj.Name = txtContactName.Text.Trim
            contactObj.Address = txtContactAddress.Text.Trim
            contactObj.Mobile = txtContactMobile.Text.Trim
            contactObj.Phone = txtContactPhone.Text.Trim
            contactObj.Email = txtContactEmail.Text.Trim
        Else
            contactObj.IdentificationNumber = ""
            contactObj.IdentificationType = -1
            contactObj.Name = ""
            contactObj.Address = ""
            contactObj.Mobile = ""
            contactObj.Phone = ""
            contactObj.Email = ""
        End If

        terminalObj.Contact = contactObj

        'Save Data
        Dim rspCode As Int16 = 0
        Dim rspMsg As String = ""
        If terminalObj.createTerminal(rspCode, rspMsg) Then
            'New Group Created OK
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Terminal Creada. Datos[ " & getFormDataLog() & " ]", "")

            pnlMsg.Visible = True
            pnlError.Visible = False
            lblMsg.Text = rspMsg

            clearForm()
        Else
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Creando Terminal. Datos[ " & getFormDataLog() & " ]", "")
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = rspMsg
        End If

    End Sub

    Private Sub clearForm()
        txtSerial.Text = ""
        txtSerialSIM.Text = ""
        txtMobileSIM.Text = ""
        txtIMEI.Text = ""
        txtChargerSerial.Text = ""
        txtBatterySerial.Text = ""
        txtBiometricSerial.Text = ""
        txtDesc.Text = ""
        ddlTerminalType.SelectedIndex = -1
        ddlTerminalBrand.SelectedIndex = -1
        ddlTerminalModel.SelectedIndex = -1
        ddlTerminalInterface.SelectedIndex = -1

        txtIP.Text = ""
        txtPort.Text = ""
        txtScheduleDate.Text = Now.ToString("dd/MM/yyyy")

        setSwitchPriorityData(1)
        setSwitchUpdateData(0)

        txtRange1.Text = "00:00"

        setSwitchPriorityData(1)
        setSwitchUpdateData(0)

        setSwitchBlockingMode(0)
        setSwitchUpdateNow(0)

        setSwitchIMEIData(0)
        setSwitchSIMData(0)

        pnlSchedulePerTerminal.Visible = False
        pnlUpdateTerminal.Visible = False

        txtContactNumId.Enabled = True
        ddlContactIdType.Enabled = False
        txtContactName.Enabled = False
        txtContactAddress.Enabled = False
        txtContactMobile.Enabled = False
        txtContactPhone.Enabled = False
        txtContactEmail.Enabled = False

        txtContactNumId.Text = ""
        ddlContactIdType.SelectedValue = -1
        txtContactName.Text = ""
        txtContactAddress.Text = ""
        txtContactMobile.Text = ""
        txtContactPhone.Text = ""
        txtContactEmail.Text = ""

        txtSerial.Focus()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then
            txtGroupName.Text = objSessionParams.strtSelectedGroup
            txtScheduleDate.Text = Now.ToString("dd/MM/yyyy")

            setSwitchPriorityData(1)
            setSwitchUpdateData(0)

            setSwitchBlockingMode(0)
            setSwitchUpdateNow(0)

            setSwitchIMEIData(0)
            setSwitchSIMData(0)

            txtSerial.Focus()
        Else
            Dim CtrlID As String = String.Empty

            If Request.Form("__EVENTTARGET") IsNot Nothing And
               Request.Form("__EVENTTARGET") <> String.Empty Then
                CtrlID = Request.Form("__EVENTTARGET")
            End If

            If CtrlID = "terminalFlagUpdateYES" Then
                pnlSchedulePerTerminal.Visible = True
            End If

            If CtrlID = "terminalFlagUpdateNO" Then
                pnlSchedulePerTerminal.Visible = False
            End If

            If CtrlID = "terminalFlagPriorityGroupYES" Then
                pnlUpdateTerminal.Visible = False
                pnlSchedulePerTerminal.Visible = False
            End If

            If CtrlID = "terminalFlagPriorityGroupNO" Then
                pnlUpdateTerminal.Visible = True
            End If

            setSwitchPriorityData(terminalFlagPriority.Value)
            setSwitchUpdateData(terminalFlagUpdate.Value)
            setSwitchBlockingMode(terminalFlagBlockingMode.Value)
            setSwitchUpdateNow(terminalFlagUpdateNow.Value)

            setSwitchIMEIData(terminalIMEIFlag.Value)
            setSwitchSIMData(terminalSIMFlag.Value)

        End If

        setSliderData()

    End Sub

    Private Sub setSwitchPriorityData(flagPriority As Integer)
        Dim htmlSwitch As String

        If flagPriority = 1 Then
            htmlSwitch = "<p class='field switch' id='switchUpdate'><label for='radio1' id='radio11' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio2' id='radio12' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='switchUpdate'><label for='radio1' id='radio11' class='cb-enable'><span>S&iacute;</span></label><label for='radio2' id='radio12' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrPriority.Text = htmlSwitch

    End Sub

    Private Sub setSwitchUpdateData(flagUpdate As Integer)
        Dim htmlSwitch As String

        If flagUpdate = 1 Then
            htmlSwitch = "<p class='field switch' id='switchPriority'><label for='radio3' id='radio13' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio4' id='radio14' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='switchPriority'><label for='radio3' id='radio13' class='cb-enable'><span>S&iacute;</span></label><label for='radio4' id='radio14' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrUpdate.Text = htmlSwitch

    End Sub

    Private Sub setSwitchIMEIData(flagImei As Integer)
        Dim htmlSwitch As String

        If flagImei = 1 Then
            htmlSwitch = "<p class='field switch' id='switchUpdate'><label for='radio5' id='radio15' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio6' id='radio16' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='switchUpdate'><label for='radio5' id='radio15' class='cb-enable'><span>S&iacute;</span></label><label for='radio6' id='radio16' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrIMEI.Text = htmlSwitch

    End Sub

    Private Sub setSwitchSIMData(flagSim As Integer)
        Dim htmlSwitch As String

        If flagSim = 1 Then
            htmlSwitch = "<p class='field switch' id='switchUpdate'><label for='radio7' id='radio17' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio8' id='radio18' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='switchUpdate'><label for='radio7' id='radio17' class='cb-enable'><span>S&iacute;</span></label><label for='radio8' id='radio18' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrSIM.Text = htmlSwitch

    End Sub

    Private Sub setSwitchBlockingMode(flagBlockingMode As Boolean)
        Dim htmlSwitch As String

        If flagBlockingMode = True Then
            htmlSwitch = "<p class='field switch' id='switchBlockingMode'><label for='radio11' id='radio21' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio12' id='radio22' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='switchBlockingMode'><label for='radio11' id='radio21' class='cb-enable'><span>S&iacute;</span></label><label for='radio12' id='radio22' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrBlockingMode.Text = htmlSwitch

    End Sub

    Private Sub setSwitchUpdateNow(flagUpdateNow As Boolean)
        Dim htmlSwitch As String

        If flagUpdateNow = True Then
            htmlSwitch = "<p class='field switch' id='switchUpdateNow'><label for='radio11' id='radio23' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio12' id='radio24' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='switchUpdateNow'><label for='radio11' id='radio23' class='cb-enable'><span>S&iacute;</span></label><label for='radio12' id='radio24' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrUpdateNow.Text = htmlSwitch

    End Sub

    Private Sub setSliderData()
        Dim htmlSlider As String = ""
        Dim hour1 As Integer
        Dim minute1 As Integer
        Dim val1, val2 As Integer
        Dim interval As Integer = 15

        hour1 = Integer.Parse(txtRange1.Text.Trim.Split(":")(0))
        minute1 = Integer.Parse(txtRange1.Text.Trim.Split(":")(1))
        val1 = (hour1 * 60) / interval
        val2 = (minute1 Mod 60) / interval

        htmlSlider = "<script language='javascript'>$(function() { $('#slider-range').slider({ range: false, min: 0, max: 95, values: [" & (val1 + val2) & "], slide: function (event, ui) { var interval = 15; var hour1 = (ui.values[0] * interval) / 60;	var minute1 = (ui.values[0] * interval) % 60; $('#ctl00_MainContent_txtRange1').val(zeroPad(parseInt(hour1), 2) + ':' + zeroPad(parseInt(minute1), 2)); }}); });</script>"

        ltrScript.Text &= htmlSlider
    End Sub

    Protected Sub btnQueryContactData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQueryContactData.Click

        'Consultar Datos de contacto existentes
        If txtContactNumId.Text.Length < 8 Then
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Ingrese un número de identificación válido."
            'Set Toggle Visible
            ltrScript.Text &= "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message2').css('z-index', 750); $('#hide-message2').css('display', 'block'); });</script>"
            Exit Sub
        End If

        'Buscar en BD
        getContactData()

        'Set Toggle Visible
        ltrScript.Text &= "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message2').css('z-index', 750); $('#hide-message2').css('display', 'block'); });</script>"

    End Sub

    Private Sub getContactData()
        Dim connection As New SqlConnection(ConnectionString())
        Dim command As New SqlCommand()
        Dim results As SqlDataReader

        Try
            'Abrir Conexion
            connection.Open()

            'Buscar Datos de Contacto
            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webConsultarDatosContacto"
            command.Parameters.Clear()
            command.Parameters.Add(New SqlParameter("contactNumID", txtContactNumId.Text.Trim))

            'Ejecutar SP
            results = command.ExecuteReader()

            If results.HasRows Then
                While results.Read()
                    txtContactNumId.Text = results.GetString(0)
                    ddlContactIdType.SelectedValue = results.GetInt64(1)
                    txtContactName.Text = results.GetString(2)
                    txtContactAddress.Text = results.GetString(3)
                    txtContactMobile.Text = results.GetString(4)
                    txtContactPhone.Text = results.GetString(5)
                    txtContactEmail.Text = results.GetString(6)

                    txtContactNumId.Enabled = False
                    ddlContactIdType.Enabled = False
                    txtContactName.Enabled = False
                    txtContactAddress.Enabled = False
                    txtContactMobile.Enabled = False
                    txtContactPhone.Enabled = False
                    txtContactEmail.Enabled = False

                End While
            Else
                txtContactNumId.Enabled = True
                ddlContactIdType.Enabled = True
                txtContactName.Enabled = True
                txtContactAddress.Enabled = True
                txtContactMobile.Enabled = True
                txtContactPhone.Enabled = True
                txtContactEmail.Enabled = True
            End If

            connection.Close()
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub

    Protected Sub btnDeletecontactData_Click(sender As Object, e As EventArgs) Handles btnDeletecontactData.Click

        txtContactNumId.Text = ""
        ddlContactIdType.SelectedValue = -1
        txtContactName.Text = ""
        txtContactAddress.Text = ""
        txtContactMobile.Text = ""
        txtContactPhone.Text = ""
        txtContactEmail.Text = ""

        txtContactNumId.Enabled = True
        ddlContactIdType.Enabled = False
        txtContactName.Enabled = False
        txtContactAddress.Enabled = False
        txtContactMobile.Enabled = False
        txtContactPhone.Enabled = False
        txtContactEmail.Enabled = False

        txtContactNumId.Focus()

        'Set Toggle Visible
        ltrScript.Text &= "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message2').css('z-index', 750); $('#hide-message2').css('display', 'block'); });</script>"

    End Sub

End Class
