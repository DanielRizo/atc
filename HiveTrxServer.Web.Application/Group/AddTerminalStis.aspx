﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AddTerminalStis.aspx.vb" Inherits="Groups_AddTerminalStis" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Adicionar Terminal STIS
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>


    <!-- jquery base -->
    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.min.js") %>'></script>
    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery-ui-1.8.11.custom.min.js") %>'></script>

    <%--scripts edit table--%>
      <script type="text/javascript" src="../js/msgAlert.js"></script>

  <link rel="stylesheet" type="text/css" href="/style/bootstrap.min.css" />

      <script type="text/javascript" src='<%= ResolveClientUrl("~/js/editTable.js") %>'></script>
   
    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.tabledit.js") %>'></script>
    
     <script type="text/javascript" src='<%= ResolveClientUrl("~/js/bootstrap.js") %>'></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>

    <li>Agregar Nuevo Terminal...</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
          <div class="">
            <h3>Terminales</h3>
            
        </div>
   
  
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
        
        <br /><br /> <ul id="navst">
	<li class="current"><a href="TerminalStis.aspx">Polaris TMS</a></li>
	<li><a>Tables</a>
		<ul>
			<li><a href="TerminalAcquirer.aspx">Acquirer</a></li>
			<li><a href="TerminalIssuer.aspx">Issuer</a>	</li>
			<li><a href="TerminalCardRange.aspx">CardRange</a></li>
			<li><a href="TerminalEmvLevel2.aspx">Emv Level 2 Application</a></li>
			<li><a href="TerminalEmvKey.aspx">Emv Level 2 Key</a></li>
			<li><a href="TerminalExtraApplication.aspx">Extra Application Parameter</a></li>

		</ul>
	</li>

</ul>
             <asp:Panel ID="pnlMsg" runat="server" Visible="False">
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlError" runat="server" Visible="False">
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>
    </div>
        <div >
            <h3 class="">Agregar Terminales
                </h3>
            <div class="" id="" >
                <div class="st-form-line">
                    <div class="st-form-line">
                        <span class="st-labeltext">TID: (*)</span>
                        <asp:TextBox ID="txtTid" CssClass="st-forminput" Style="width: 150px"
                            runat="server" TabIndex="1" MaxLength="8"
                            ToolTip="Digite El TID." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                        <div class="clear"></div>
                    </div>
                    <div class="st-form-line">
                        <span class="st-labeltext">Owner Name: (*)</span>
                        <asp:TextBox ID="txtOwnerName" CssClass="st-forminput" Style="width: 150px"
                            runat="server" TabIndex="2" MaxLength="20"
                            ToolTip="Digite El Owner Name." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                        <div class="clear"></div>
                    </div>
                    <div class="st-form-line">
                        <span class="st-labeltext">Merchant Name: (*)</span>
                        <asp:TextBox ID="txtMerchantName" CssClass="st-forminput" Style="width: 150px"
                            runat="server" TabIndex="3" MaxLength="20"
                            ToolTip="Digite el Merchant Name." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                        <div class="clear"></div>
                    </div>

                       <div class="st-form-line">
                        <span class="st-labeltext">Shop Name: (*)</span>
                        <asp:TextBox ID="txtShopName" CssClass="st-forminput" Style="width: 150px"
                            runat="server" TabIndex="3" MaxLength="20"
                            ToolTip="Digite el Merchant Name." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                        <div class="clear"></div>
                    </div>

                       <div class="st-form-line">
                        <span class="st-labeltext">Detail Addres: (*)</span>
                        <asp:TextBox ID="txtDetailAddres" CssClass="st-forminput" Style="width: 150px"
                            runat="server" TabIndex="3" MaxLength="20"
                            ToolTip="Digite el Merchant Name." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">
                        <span class="st-labeltext">Shop Contact: (*)</span>
                        <asp:TextBox ID="txtShopContact" CssClass="st-forminput" Style="width: 150px"
                            runat="server" TabIndex="3" MaxLength="20"
                            ToolTip="Digite el Merchant Name." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                        <div class="clear"></div>
                    </div>

                      <div class="st-form-line">
                        <span class="st-labeltext">Shop Tel No: (*)</span>
                        <asp:TextBox ID="txtShopTelNo" CssClass="st-forminput" Style="width: 150px"
                            runat="server" TabIndex="3" MaxLength="20"
                            ToolTip="Digite el Merchant Name." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                        <div class="clear"></div>
                    </div>

                     <div class="st-form-line">
                        <span class="st-labeltext">Office Contact: (*)</span>
                        <asp:TextBox ID="txtOfficeContact" CssClass="st-forminput" Style="width: 150px"
                            runat="server" TabIndex="3" MaxLength="20"
                            ToolTip="Digite el Merchant Name." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">
                        <span class="st-labeltext">Office Tel No: (*)</span>
                        <asp:TextBox ID="txtOfficeTelNo" CssClass="st-forminput" Style="width: 150px"
                            runat="server" TabIndex="3" MaxLength="20"
                            ToolTip="Digite el Merchant Name." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">
            <span class="st-labeltext">Status:</span>
             <asp:DropDownList ID="ddlOpc" class="uniform" runat="server"
                                DataSourceID="dsList" DataTextField="TABLE_KEY_CODE"
                                DataValueField="TABLE_KEY_CODE" Width="200px"
                                ToolTip="Seleccione el acquirer." TabIndex="0" AutoPostBack="false">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="dsList" runat="server"
                                ConnectionString="<%$ ConnectionStrings:TeleLoaderStisConnectionString %>" SelectCommand="SELECT 'Active' AS TABLE_KEY_CODE
                                UNION ALL
                                SELECT 'Scheduled' AS TABLE_KEY_CODE
                                UNION ALL
                                SELECT 'Suspended' AS TABLE_KEY_CODE
                                UNION ALL
                                SELECT 'Cancelled' AS TABLE_KEY_CODE"></asp:SqlDataSource>
       </div>

                       
            </div>
        </div>
     </div>
        
                    <script type="text/javascript">
            $(document).ready(function () {
                enableEditTable("#ctl00_MainContent_grdAddTerminal", false,true, false,[[2, 'Value']], "addTerminalStis");
            });     </script>   
        <div class="body">
            <div id="posStis" style="overflow: auto; width: auto; height: 660px;">
                <br />
                <asp:GridView ID="grdAddTerminal" runat="server"
                    AllowSorting="True" AutoGenerateColumns="False" CellPadding="4"
                    DataSourceID="dsAddTerminal"
                    CssClass="mGridCenter" DATAKEYNAME="IS_EXTENDED"
                    EmptyDataText="No existen Terminales creados con el filtro aplicado"
                    HorizontalAlign="Left" GridLines="None" ForeColor="#333333">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <%--<asp:CommandField ButtonType="Image" HeaderImageUrl="~/img/icons/16x16/edit.png" ShowEditButton="True" UpdateImageUrl="~/img/icons/16x16/ok.png" CancelImageUrl="~/img/icons/16x16/cancel.png" EditImageUrl="~/img/icons/16x16/edit.png" />--%>
                        <asp:BoundField DataField="IS_EXTENDED" HeaderText="Parameter Type"
                            SortExpression="IS_EXTENDED" ReadOnly="True" />
                        <asp:BoundField DataField="FIELD_DISPLAY_NAME" HeaderText="Parameter Name"
                            SortExpression="FIELD_DISPLAY_NAME" />
                        <asp:BoundField DataField="CONTENT_DESC" HeaderText="Value"
                            SortExpression="CONTENT_DESC" />
                        <asp:BoundField DataField="CONTENT_TYPE" HeaderText="Value Type"
                            SortExpression="CONTENT_TYPE" ReadOnly="True" />
                        <asp:BoundField DataField="NOTE" HeaderText="Note"
                            SortExpression="NOTE" ReadOnly="True" />
                        <%--  <asp:TemplateField ShowHeader="False" HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False"
                                CommandName="EditTerminalParameter" ImageUrl="~/img/icons/16x16/edit.png" Text="Editar"
                                ToolTip="Editar Acquirer" CommandArgument='<%# grdTerminalsParameterAcquirer.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" >
                        </ItemTemplate>

                        <HeaderStyle Width="100px"></HeaderStyle>

                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>--%>
                    </Columns>


                    <FooterStyle BackColor="#1C5E55" ForeColor="White" Font-Bold="True" />
                    <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center"
                        CssClass="pgr" />
                    <RowStyle BackColor="#E3EAEB" />
                    <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BorderColor="#666666" BorderStyle="Solid"
                        BorderWidth="1px" BackColor="#7C6F57" />
                    <SortedAscendingCellStyle BackColor="#F8FAFA" />
                    <SortedAscendingHeaderStyle BackColor="#246B61" />
                    <SortedDescendingCellStyle BackColor="#D4DFE1" />
                    <SortedDescendingHeaderStyle BackColor="#15524A" />
                </asp:GridView>
                <br />
            </div>

            <br />
            <asp:SqlDataSource ID="dsAddTerminal" runat="server"
                ConnectionString="<%$ ConnectionStrings:TeleLoaderStisConnectionString %>"
                SelectCommand="sp_webAddTermianl_Stis" SelectCommandType="StoredProcedure"
                UpdateCommand="sp_webEditDatosAcquirerNew_Stis" UpdateCommandType="StoredProcedure">
                <SelectParameters>
                    <asp:Parameter Name="IS_EXTENDED" Type="string" />

                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="FIELD_DISPLAY_NAME" Type="String" />
                    <asp:Parameter Name="CONTENT_DESC" Type="String" />

                </UpdateParameters>
            </asp:SqlDataSource>
        </div>
    </div>
    <asp:HyperLink ID="HyperLink1" runat="server"
        NavigateUrl="~/Group/TerminalAcquirer.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>


<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

    <script language="javascript">
        var globalcontrol = '';

        function ConfirmAction(control) {

            globalcontrol = control;

            $.msgAlert({
                type: "warning"
                , title: "Mensaje del sistema"
                , text: "Realmente desea eliminar el terminal?"
                , callback: function () {
                    __doPostBack($(globalcontrol).attr('name'), '');
                }
            });

            return false;
        }
    </script>

</asp:Content>

