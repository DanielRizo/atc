﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AndroidGroupTerminals.aspx.vb" Inherits="Groups_AndroidGroupTerminals" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Terminales Android del Grupo
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   

	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script>     
        
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkListGroups" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/ListAndroidGroups.aspx">Listar Grupos Android</asp:LinkButton>
    </li>
    <li>Terminales Android del Grupo</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    Terminales Android del Grupo: [ <%= objSessionParams.strtSelectedGroup %> ]
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite administrar las Terminales Android del grupo:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>   
            
        <asp:Panel ID="pnlError" runat="server" Visible="False">    
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>  

        <div class="toggle-message" style="z-index: 590; top: 0px; left: 0px;">
            <h3 class="title">Filtro de B&uacute;squeda...
                <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
            <div class="hide-message" id="hide-message" style="display: none;">
                <div class="st-form-line">
                    <span class="st-labeltext"><b>Heracles ID: </b></span>
                    <asp:TextBox ID="txtfilterHeraclesID" CssClass="st-forminput"
                        runat="server" TabIndex="1" MaxLength="50"
                        ToolTip="Heracles ID Asignado" onkeydown="return (event.keyCode!=13);" Width="250px"></asp:TextBox>
                </div>
                <div class="st-form-line">
                    <span class="st-labeltext"><b>Dirección IP: </b></span>
                    <asp:TextBox ID="txtfilterIpAddress" CssClass="st-forminput"
                        runat="server" TabIndex="2" MaxLength="50"
                        ToolTip="Dirección IP del Dispositivo" onkeydown="return (event.keyCode!=13);" Width="250px"></asp:TextBox>
                </div>
                <div class="st-form-line">
                    <span class="st-labeltext"><b>Nombre: </b></span>
                    <asp:TextBox ID="txtfilterName" CssClass="st-forminput"
                        runat="server" TabIndex="2" MaxLength="50"
                        ToolTip="Nombre del Corresponsal No Bancario" onkeydown="return (event.keyCode!=13);" Width="250px"></asp:TextBox>
                </div>
                <div class="st-form-line">
                    <span class="st-labeltext"><b>Serial: </b></span>
                    <asp:TextBox ID="txtfilterSerial" CssClass="st-forminput"
                        runat="server" TabIndex="1" MaxLength="50"
                        ToolTip="Serial del Dispositivo" onkeydown="return (event.keyCode!=13);" Width="250px"></asp:TextBox>
                </div>
                <div class="button-box">
                    <asp:Button ID="btnConsultar" runat="server" Text="Filtrar"
                        CssClass="button-aqua" TabIndex="3" ToolTip="Filtrar Clientes..." />
                </div>
            </div>
        </div>

    </div>
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
	    <div class="titleh">
    	    <h3>Terminales Android Creadas en el Grupo</h3>
        </div>
        <div class="body">    
            <br />            
            <asp:GridView ID="grdTerminals" runat="server" AllowPaging="True" 
                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                DataSourceID="dsTerminals" ForeColor="#333333"
                CssClass="mGridCenter" DataKeyNames="ter_id, ter_dispositivo_id" 
                EmptyDataText="No existen Terminales Android creadas en el Grupo o con el filtro aplicado" 
                HorizontalAlign="Left" Width="1058px">
                <RowStyle BackColor="White" ForeColor="White" />
             
               <Columns>
                    <asp:TemplateField HeaderText=" " SortExpression="ter_logo" HeaderStyle-Width="20px">
                        <ItemTemplate>
                            <asp:Image ID="imgUrlIcon" runat="server" ImageUrl='<%# Bind("ter_logo") %>' ToolTip='<%# Bind("ter_fecha_actualizacion_auto") %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="ter_dispositivo_id" HeaderText="Dispositivo ID - Heracles ID"  HeaderStyle-Width="120px"
                        SortExpression="ter_dispositivo_id" />
                    <asp:BoundField DataField="ter_ip_dispositivo" HeaderText="Dirección IP" 
                        SortExpression="ter_ip_dispositivo" />
                    <asp:BoundField DataField="ter_nombre_heracles" HeaderText="Nombre" 
                        SortExpression="ter_nombre_heracles" />
                    <asp:BoundField DataField="ter_ciudad_heracles" HeaderText="Ciudad" 
                        SortExpression="ter_ciudad_heracles" />
                    <asp:BoundField DataField="ter_region_heracles" HeaderText="Región" 
                        SortExpression="ter_region_heracles" />
                    <asp:BoundField DataField="ter_agencia_heracles" HeaderText="Agencia" 
                        SortExpression="ter_agencia_heracles" />
                    <asp:BoundField DataField="apk_banco" HeaderText="APK Banco" 
                        SortExpression="apk_banco" ItemStyle-HorizontalAlign ="Center" />
                    <asp:BoundField DataField="configuracion" HeaderText="Configuración" 
                        SortExpression="configuracion" ItemStyle-HorizontalAlign ="Center" />
                    <asp:BoundField DataField="imagenes" HeaderText="Imágenes" 
                        SortExpression="imagenes" ItemStyle-HorizontalAlign ="Center" />
                    <asp:TemplateField ShowHeader="False" HeaderStyle-Width="150px">
                        <ItemTemplate>
                          
                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" 
                            CommandName="EditTerminal" ImageUrl="~/img/icons/16x16/edit.png" Text="Editar" 
                            ToolTip="Editar Terminal" CommandArgument='<%# grdTerminals.Rows.Count%>' style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />                    
                            <asp:ImageButton ID="imgShowMap" runat="server" CausesValidation="False" 
                                CommandName="ShowTerminalMap" ImageUrl="~/img/icons/16x16/map.png" Text="Ubicación" 
                                ToolTip="Mostrar Ubicación en Mapa" CommandArgument='<%# grdTerminals.Rows.Count%>' style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />

                        
                               <asp:ImageButton ID="imgXML" runat="server" CausesValidation="False" 
                            CommandName="DeployXML" ImageUrl="~/img/icons/16x16/xml.png" Text="Configuraciones" 
                            ToolTip="Deploy de Configuraciones del Terminal" CommandArgument='<%# grdTerminals.Rows.Count%>' style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />
      
                            <asp:ImageButton ID="imgKey1" runat="server" CausesValidation="False" 
                                CommandName="InjectKey1" ImageUrl="~/img/icons/16x16/key_1.png" Text="Cargar Componente 1" 
                                ToolTip="Cargar Componente 1" CommandArgument='<%# grdTerminals.Rows.Count%>' style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />

                            <asp:ImageButton ID="imgKey2" runat="server" CausesValidation="False" 
                                CommandName="InjectKey2" ImageUrl="~/img/icons/16x16/key_2.png" Text="Cargar Componente 2" 
                                ToolTip="Cargar Componente 2" CommandArgument='<%# grdTerminals.Rows.Count%>' style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />

                            <asp:Image ID="imgKey1status" runat="server" ImageUrl='<%# Bind("ter_logo_estado_componente1")%>' ToolTip='<%# Bind("ter_desc_estado_componente1") %>' style="padding: 0px 2px 2px 0px !important;" visible="false"/>
                            <asp:Image ID="imgKey2status" runat="server" ImageUrl='<%# Bind("ter_logo_estado_componente2")%>' ToolTip='<%# Bind("ter_desc_estado_componente2") %>' style="padding: 0px 2px 2px 0px !important;" visible="false"/>
                             <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False" 
                                CommandName="DeleteTerminal" ImageUrl="~/img/icons/16x16/delete.png" Text="Eliminar" 
                                ToolTip="Eliminar Terminal del Grupo" CommandArgument='<%# grdTerminals.Rows.Count%>' style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />

                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>

                </Columns>
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
                    CssClass="pgr" Font-Underline="False" />
                <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid" 
                    BorderWidth="1px" />
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>            
            <br />
	        <asp:SqlDataSource ID="dsTerminals" runat="server" 
                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"                 
                SelectCommand="sp_webConsultarTerminalesAndroidGrupo" SelectCommandType="StoredProcedure"
                DeleteCommand="sp_webEliminarTerminalXGrupo" DeleteCommandType="StoredProcedure" ConflictDetection="OverwriteChanges" >
                <SelectParameters>
                    <asp:Parameter Name="groupId" Type="String" />
                    <asp:Parameter Name="terminalHeraclesId" Type="String" DefaultValue="-1"/>
                    <asp:Parameter Name="terminalIPAddress" Type="String" DefaultValue="-1"/>
                    <asp:Parameter Name="terminalName" Type="String" DefaultValue="-1"/>
                    <asp:Parameter Name="terminalSerial" Type="String" DefaultValue="-1"/>
                </SelectParameters>
                <DeleteParameters>
                    <asp:Parameter Name="terminalId" Type="String" />
                    <asp:Parameter Name="groupId" Type="String" />
                </DeleteParameters>
            </asp:SqlDataSource> 
        </div>
    </div>
    
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Group/ListAndroidGroups.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">
    
    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
    <script language="javascript">
        var globalcontrol = '';

        function ConfirmAction(control) {

            globalcontrol = control;

            $.msgAlert({
                type: "warning"
                        , title: "Mensaje del sistema"
                        , text: "Realmente desea eliminar la terminal?"
                        , callback: function () {
                            __doPostBack($(globalcontrol).attr('name'), '');
                        }
            });

            return false;
        }
    </script>

</asp:Content>

