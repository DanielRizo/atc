﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Applications
Imports System.Data.Common
Imports System.Drawing
Imports TeleLoader.Terminals

Partial Class Groups_AndroidGroupTerminals
    Inherits TeleLoader.Web.BasePage

    Dim terminalObj As Terminal
    Dim applicationObj As Application

    'Validadores de Acceso
    Dim allowAccess1 As Boolean
    Dim allowAccess2 As Boolean
    Dim allowAccess3 As Boolean
    Dim allowAccess4 As Boolean
    Dim allowAccess5 As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        'Validar Accesos en Botones de Acción GridView
        allowAccess1 = objAccessToken.ValidateInGridView("Group/AndroidGroupTerminals.aspx", "Delete")
        allowAccess2 = objAccessToken.ValidateInGridView("Group/EditTerminalAndroidGroup.aspx", "Load")
        allowAccess3 = objAccessToken.ValidateInGridView("Group/ShowTerminalAndroidGroupLocation.aspx", "Load")
        allowAccess4 = objAccessToken.ValidateInGridView("Group/InjectComponent1TerminalAndroidGroup.aspx", "Load")
        allowAccess5 = objAccessToken.ValidateInGridView("Group/InjectComponent2TerminalAndroidGroup.aspx", "Load")

        If Not IsPostBack Then

            txtfilterHeraclesID.Focus()

            dsTerminals.SelectParameters("groupId").DefaultValue = objSessionParams.intSelectedGroup

            grdTerminals.DataBind()

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub grdTerminals_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdTerminals.RowCommand
        Try

            Select Case e.CommandName

                Case "DeployXML"

                    grdTerminals.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.intSelectedTerminal = grdTerminals.SelectedDataKey.Values.Item("ter_id")

                    terminalObj = New Terminal(strConnectionString, objSessionParams.intSelectedGroup)

                    'Leer datos BD
                    terminalObj.TerminalID = grdTerminals.SelectedDataKey.Values.Item("ter_id")
                    terminalObj.getTerminalData()

                    objSessionParams.strSelectedSerialTerminal = terminalObj.TerminalSerial
                    objSessionParams.booleanIsAddFileTerminal = True

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("TerminalAndroidConfigFiles.aspx", False)

                Case "DeleteTerminal"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    grdTerminals.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    dsTerminals.DeleteParameters("terminalId").DefaultValue = grdTerminals.SelectedDataKey.Values.Item("ter_id")
                    dsTerminals.DeleteParameters("groupId").DefaultValue = objSessionParams.intSelectedGroup

                    Dim strData As String = "Terminal ID: " & grdTerminals.SelectedDataKey.Values.Item("ter_id") & ", grupo ID: " & objSessionParams.intSelectedGroup

                    dsTerminals.Delete()

                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Terminal eliminada del Grupo Android. Datos[ " & strData & " ]", "")

                    grdTerminals.DataBind()

                    pnlError.Visible = False
                    pnlMsg.Visible = True
                    lblMsg.Text = "Terminal eliminada del Grupo Android"

                    grdTerminals.SelectedIndex = -1

                Case "EditTerminal"
                    grdTerminals.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.intSelectedTerminalID = grdTerminals.SelectedDataKey.Values.Item("ter_id")

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("EditTerminalAndroidGroup.aspx", False)

                Case "ShowTerminalMap"
                    grdTerminals.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.intSelectedTerminalID = grdTerminals.SelectedDataKey.Values.Item("ter_id")

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("ShowTerminalAndroidGroupLocation.aspx", False)

                Case "EMVConfigTerminal"
                    grdTerminals.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.intSelectedTerminalID = grdTerminals.SelectedDataKey.Values.Item("ter_id")
                    objSessionParams.strSelectedTerminalSerial = grdTerminals.SelectedDataKey.Values.Item("ter_serial")

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("EMVConfigItemsTerminal.aspx", False)

                    'Modificación Carga de Llaves; EB: 28/Feb/2018
                Case "InjectKey1"
                    grdTerminals.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.intSelectedTerminalID = grdTerminals.SelectedDataKey.Values.Item("ter_id")

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("InjectComponent1TerminalAndroidGroup.aspx", False)

                    'Modificación Carga de Llaves; EB: 28/Feb/2018
                Case "InjectKey2"
                    grdTerminals.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.intSelectedTerminalID = grdTerminals.SelectedDataKey.Values.Item("ter_id")

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("InjectComponent2TerminalAndroidGroup.aspx", False)

            End Select

            If txtfilterHeraclesID.Text = "" And txtfilterIpAddress.Text = "" And txtfilterName.Text = "" Then
                'Set Invisible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
            Else
                'Set Visible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
            End If

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub

    Protected Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click

        If txtfilterHeraclesID.Text <> "" Then
            dsTerminals.SelectParameters("terminalHeraclesId").DefaultValue = txtfilterHeraclesID.Text
        Else
            dsTerminals.SelectParameters("terminalHeraclesId").DefaultValue = "-1"
        End If

        If txtfilterIpAddress.Text <> "" Then
            dsTerminals.SelectParameters("terminalIPAddress").DefaultValue = txtfilterIpAddress.Text
        Else
            dsTerminals.SelectParameters("terminalIPAddress").DefaultValue = "-1"
        End If

        If txtfilterName.Text <> "" Then
            dsTerminals.SelectParameters("terminalName").DefaultValue = txtfilterName.Text
        Else
            dsTerminals.SelectParameters("terminalName").DefaultValue = "-1"
        End If

        'Modificación Carga de Llaves; EB: 28/Feb/2018
        If txtfilterSerial.Text <> "" Then
            dsTerminals.SelectParameters("terminalSerial").DefaultValue = txtfilterSerial.Text
        Else
            dsTerminals.SelectParameters("terminalSerial").DefaultValue = "-1"
        End If

        grdTerminals.DataBind()

        'Modificación Carga de Llaves; EB: 28/Feb/2018
        If txtfilterHeraclesID.Text = "" And txtfilterIpAddress.Text = "" And txtfilterName.Text = "" And txtfilterSerial.Text = "" Then
            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
        Else
            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
        End If

    End Sub

    Private Sub clearForm()
        txtfilterHeraclesID.Text = ""
        txtfilterIpAddress.Text = ""
        txtfilterName.Text = ""
    End Sub

    Protected Sub dsTerminals_Deleting(sender As Object, e As SqlDataSourceCommandEventArgs) Handles dsTerminals.Deleting

        'Remover parametro extra, igual al datakeyname
        If e.Command.Parameters.Count > 2 Then
            Dim paramAplId As DbParameter = e.Command.Parameters("@ter_id")
            e.Command.Parameters.Remove(paramAplId)
        End If

    End Sub

    Protected Sub grdTerminals_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdTerminals.RowDataBound

        Dim backgroundColorGreen As String = "#dff0d8"
        Dim backgroundColorRed As String = "#e47a7a"

        If e.Row.RowType = DataControlRowType.DataRow Then

            e.Row.Cells(7).Style("background") = DataBinder.Eval(e.Row.DataItem, "apk_banco_color")
            e.Row.Cells(7).ForeColor = IIf(DataBinder.Eval(e.Row.DataItem, "apk_banco_color") = backgroundColorGreen, Color.Gray, Color.White)

            e.Row.Cells(8).Style("background") = DataBinder.Eval(e.Row.DataItem, "configuracion_color")
            e.Row.Cells(8).ForeColor = IIf(DataBinder.Eval(e.Row.DataItem, "configuracion_color") = backgroundColorGreen, Color.Gray, Color.White)

            e.Row.Cells(9).Style("background") = DataBinder.Eval(e.Row.DataItem, "imagenes_color")
            e.Row.Cells(9).ForeColor = IIf(DataBinder.Eval(e.Row.DataItem, "imagenes_color") = backgroundColorGreen, Color.Gray, Color.White)

        End If

    End Sub

    Protected Sub grdTerminals_DataBound(sender As Object, e As EventArgs) Handles grdTerminals.DataBound

        For Each row As GridViewRow In grdTerminals.Rows

            CType(row.Cells(10).Controls(1), ImageButton).Visible = allowAccess1
            CType(row.Cells(10).Controls(3), ImageButton).Visible = allowAccess2
            CType(row.Cells(10).Controls(5), ImageButton).Visible = allowAccess3
            CType(row.Cells(10).Controls(7), ImageButton).Visible = allowAccess4
            CType(row.Cells(10).Controls(9), ImageButton).Visible = allowAccess5

            If allowAccess4 Or allowAccess5 Then
                CType(row.Cells(10).Controls(11), System.Web.UI.WebControls.Image).Visible = True
                CType(row.Cells(10).Controls(13), System.Web.UI.WebControls.Image).Visible = True
            End If

        Next

    End Sub

End Class
