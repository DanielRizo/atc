﻿
<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ConfiguracionTCP.aspx.vb" Inherits="Security_WebModules" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Configuracion TCP
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   

	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script>     
        
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed" 
            PostBackUrl="~/Security/Manager.aspx">Seguridad</asp:LinkButton>
    </li>
    <li>Configuración TCP</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Configuración TCP
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite configurar ip's y puerto por operador:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>   
            
        <asp:Panel ID="pnlError" runat="server" Visible="False">    
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>  

        <div class="toggle-message" style="z-index: 590;">
            <h3 class="title">Nueva Configuración...
                <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
            <div class="hide-message" id="hide-message" style="display: none;">
        
                <div class="st-form-line">	
                    <span class="st-labeltext">Nombre del Operador:</span>	
                    <asp:TextBox ID="txtModuleName" CssClass="st-forminput" style="width:510px" 
                        runat="server" TabIndex="1" MaxLength="50" 
                        ToolTip="Nombre del Operador." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div> 
                 
                
                 <div class="st-form-line">	
                    <span class="st-labeltext">Ip 1:</span>	
                    <asp:TextBox ID="IpUno" CssClass="st-forminput" style="width:510px" 
                        runat="server" TabIndex="1" MaxLength="30" 
                        ToolTip="Ip 1." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div> 

                <div class="st-form-line">	
                    <span class="st-labeltext">Puerto 1:</span>	
                    <asp:TextBox ID="PuertoUno" CssClass="st-forminput" style="width:510px" 
                        runat="server" TabIndex="1" MaxLength="30" 
                        ToolTip="Puerto 1." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div> 

                <div class="st-form-line">	
                    <span class="st-labeltext">Ip 2:</span>	
                    <asp:TextBox ID="IpDos" CssClass="st-forminput" style="width:510px" 
                        runat="server" TabIndex="1" MaxLength="30" 
                        ToolTip="Ip 2." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div> 

                <div class="st-form-line">	
                    <span class="st-labeltext">Puerto 2:</span>	
                    <asp:TextBox ID="PuertoDos" CssClass="st-forminput" style="width:510px" 
                        runat="server" TabIndex="1" MaxLength="30" 
                        ToolTip="Puerto 1." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div> 

                <div class="st-form-line">	

                    <span class="st-labeltext">Descripción:</span>	
                    <asp:TextBox ID="Descripcion" CssClass="st-forminput" style="width:510px" 
                        runat="server" TabIndex="1" MaxLength="50" 
                        ToolTip="Descripcion." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>
                
                <div class="button-box">
                    <asp:Button ID="btnAddWebModule" runat="server" Text="Adicionar Configuración TCP" 
                        CssClass="button-aqua" TabIndex="4" OnClientClick="return validateAddWebModule();" />
                    <asp:Button ID="btnFinalize" runat="server" Text="Finalizar" 
                        CssClass="st-button" TabIndex="5" Visible="false"  />
                </div> 
              
            </div>
        </div>

    </div>

     <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="panel22" runat="server" Visible="False">    
            <div class="albox succesbox">
                <asp:Label ID="lblMsg2" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>   
            
        <div class="toggle-message" style="z-index: 590;">
            <h3 class="title">Configuración de intentos...
                <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
            <div class="hide-message" id="hide-messages" style="display: none;">
        
                <div class="st-form-line">	
                    <span class="st-labeltext">Intentos:</span>	
                    <asp:TextBox ID="Intentos" CssClass="st-forminput" style="width:510px" 
                        runat="server" TabIndex="1" MaxLength="50" 
                        ToolTip="Intentos." onkeydown = "return (event.keyCode!=1);"></asp:TextBox>
                    <div class="clear"></div>
                </div> 

                
                <div class="button-box">
                    <asp:Button ID="bIntentos" runat="server" Text="Actualizar numero de intentos" 
                        CssClass="button-aqua" TabIndex="4"/>
                </div> 
              
            </div>
        </div>

    </div>
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
	    <div class="titleh">
    	    <h3>Configuraciones creadas</h3>
        </div>
        <div class="body">    
            <br />            
            <asp:GridView ID="grdWebModules" runat="server" AllowPaging="True" 
                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                DataSourceID="dsWebModules" ForeColor="#333333"
                CssClass="mGridCenter" DataKeyNames="opc_id" 
                EmptyDataText="No existen configuraciones recomendadas." 
                HorizontalAlign="Left" Width="1060px">
                <RowStyle BackColor="White" ForeColor="White" />
                <Columns>
                    <asp:TemplateField  HeaderText="Codigo" InsertVisible="False" 
                        SortExpression="opc_id">
                        <ItemTemplate>
                            <asp:Label ID="lblID1" runat="server" Text='<%# Bind("opc_id") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Nombre de Operador" SortExpression="opc_nombre">
                        <ItemTemplate>
                            <asp:Label ID="lblID2" runat="server" Text='<%# Bind("opc_operador") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Ip 1" SortExpression="opc_ip_1">
                        <ItemTemplate>
                            <asp:Label ID="lblID3" runat="server" Text='<%# Bind("opc_ip_uno") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>                    

                     <asp:TemplateField HeaderText="Puerto 1 " SortExpression="opc_puerto_1">
                        <ItemTemplate>
                            <asp:Label ID="lblID4" runat="server" Text='<%# Bind("opc_puerto_uno") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>



                    <asp:TemplateField HeaderText="Ip 2 " SortExpression="opc_ip_2">
                        <ItemTemplate>
                            <asp:Label ID="lblID5" runat="server" Text='<%# Bind("opc_ip_dos") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>

                       <asp:TemplateField HeaderText="Puerto 2 " SortExpression="opc_puerto_2">
                        <ItemTemplate>
                            <asp:Label ID="lblID6" runat="server" Text='<%# Bind("opc_puerto_Dos") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>

                      <asp:TemplateField HeaderText="Descripción " SortExpression="opc_descripcion">
                        <ItemTemplate>
                            <asp:Label ID="lblID7" runat="server" Text='<%# Bind("opc_descripcion") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>


                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>                        
                            <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False" 
                                CommandName="Delete" ImageUrl="~/img/icons/16x16/delete.png" Text="Eliminar" 
                                ToolTip="Eliminar Configuración" CommandArgument='<%# grdWebModules.Rows.Count %>' />
                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False"
                            CommandName="Edit" ImageUrl="~/img/icons/16x16/edit.png" Text="Editar"
                            ToolTip="Editar Configuración" CommandArgument='<%# grdWebModules.Rows.Count %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>

                    
                </Columns>
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
                    CssClass="pgr" Font-Underline="False" />
                <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid" 
                    BorderWidth="1px" />
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>            
            <br />
	        <asp:SqlDataSource ID="dsWebModules" runat="server" 
                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"                 
                SelectCommand="SELECT O.opc_id, O.opc_operador, O.opc_ip_uno, O.opc_puerto_uno, O.opc_ip_dos, O.opc_puerto_dos, O.opc_descripcion FROM configuracion_tcp O ORDER BY opc_id asc" 
                InsertCommand="sp_webCrearConfiguracion" InsertCommandType="StoredProcedure"
                DeleteCommand="DELETE FROM configuracion_tcp WHERE opc_id = @opc_id">
                <InsertParameters>
                    <asp:Parameter Name="optionTitle" Type="String" />
                    <asp:Parameter Name="optionIpUno" Type="String" />
                    <asp:Parameter Name="optionPuertoUno" Type="String" />
                    <asp:Parameter Name="optionIpDos" Type="String" />
                    <asp:Parameter Name="optionPuertoDos" Type="String" />
                    <asp:Parameter Name="optionDescripcion" Type="String" />
                </InsertParameters>
                <DeleteParameters>
                    <asp:Parameter Name="opc_id" Type="String" />
                </DeleteParameters>
            </asp:SqlDataSource> 
        </div>
    </div>
    
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Group/PManager.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">
    
    <!-- Validator -->
    <script src="../js/ValidatorSecurity.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    <asp:Literal ID="ltrScript2" runat="server"></asp:Literal>
    
</asp:Content>

