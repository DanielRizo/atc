﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class Security_WebModules
    Inherits TeleLoader.Web.BasePage

    Public ReadOnly IMG_PATH As String = "img/icons/sidemenu/"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then
            editarintentosv()
            txtModuleName.Focus()
        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub grdWebModules_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdWebModules.RowCommand
        Try

            Select Case e.CommandName

                Case "Edit"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Update")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try
                    grdWebModules.SelectedIndex = Convert.ToString(e.CommandArgument)
                    Dim ob As String = grdWebModules.SelectedDataKey.Values.Item("opc_id")
                    objSessionParams.codigoConfiguracion = ob

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("EditConfiguracionTcp.aspx", False)


                Case "Delete"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try
                    grdWebModules.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    Dim strData As String = "Opción ID.: " & grdWebModules.SelectedDataKey.Values.Item("opc_id") _
                                & ", Operador: " & CType(grdWebModules.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblID2"), Label).Text _
                                & ", IpUno: " & CType(grdWebModules.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblID3"), Label).Text _
                                & ", PuertoUno: " & CType(grdWebModules.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblID4"), Label).Text _
                                & ", IpDos: " & CType(grdWebModules.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblID5"), Label).Text _
                                & ", PuertoDos: " & CType(grdWebModules.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblID6"), Label).Text _
                                & ", Descripcion: " & CType(grdWebModules.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblID7"), Label).Text



                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Configuración Eliminada. Datos[ " & strData & " ]", "")

                    pnlError.Visible = False
                    pnlMsg.Visible = True
                    lblMsg.Text = "Configuración Eliminada"

                    grdWebModules.SelectedIndex = -1
            End Select

            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub

    Protected Sub btnAddWebModule_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddWebModule.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Save")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        Try
            'Set Parameters
            dsWebModules.InsertParameters.Clear()
            dsWebModules.InsertParameters.Add("optionTitle", TypeCode.String, txtModuleName.Text)
            dsWebModules.InsertParameters.Add("optionIpUno", TypeCode.String, IpUno.Text)
            dsWebModules.InsertParameters.Add("optionPuertoUno", TypeCode.String, PuertoUno.Text)
            dsWebModules.InsertParameters.Add("optionIpDos", TypeCode.String, IpDos.Text)
            dsWebModules.InsertParameters.Add("optionPuertoDos", TypeCode.String, PuertoDos.Text)
            dsWebModules.InsertParameters.Add("optionDescripcion", TypeCode.String, Descripcion.Text)

            If dsWebModules.Insert() >= 1 Then
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Confiuguración creada. Datos[ " & getFormDataLog() & " ]", "")
                pnlError.Visible = False
                pnlMsg.Visible = True
                lblMsg.Text = "Configuración creada correctamente"
                deactivateForm()
                grdWebModules.DataBind()
                dsWebModules.DataBind()
            Else
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Creando Módulo Web. Datos[ " & getFormDataLog() & " ]", "")
                pnlMsg.Visible = False
                pnlError.Visible = True
                lblError.Text = "Error creando Configuración"
            End If

            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub

    Private Sub deactivateForm()
        txtModuleName.Enabled = False
        btnAddWebModule.Enabled = False
        btnFinalize.Visible = True
        btnFinalize.Enabled = True
    End Sub

    Private Sub activateForm()
        txtModuleName.Enabled = True
        txtModuleName.Text = ""
        dsWebModules.InsertParameters.Clear()
        IpUno.Text = ""
        PuertoUno.Text = ""
        IpDos.Text = ""
        PuertoDos.Text = ""
        Descripcion.Text = ""
        btnAddWebModule.Enabled = True
        btnFinalize.Visible = False
        btnFinalize.Enabled = False
    End Sub

    Protected Sub btnFinalize_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinalize.Click
        activateForm()
        txtModuleName.Focus()

        'Set Invisible Toggle Panel
        ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"

    End Sub

    Protected Sub dsWebModules_Deleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceCommandEventArgs) Handles dsWebModules.Deleting
        If pnlError.Visible = True Then
            e.Cancel = True
        End If
    End Sub

    Private Sub editarintentosv()
        Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString
        Dim db As SqlConnection = New SqlConnection(conexion)
        Dim cmd As SqlCommand
        Dim sqlBuilder As StringBuilder = New StringBuilder
        Dim codigo = objSessionParams.codigoConfiguracion

        sqlBuilder.Append("SELECT opc_intentos from intentos_ip where opc_id = 1")

        Try
            db.Open()
        Catch ex As Exception
            db.Close()
        End Try
        Try
            cmd = New SqlCommand()
            cmd.Connection = db
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sqlBuilder.ToString

            Dim dr As SqlDataReader = cmd.ExecuteReader()

            If dr.Read Then
                Intentos.Text = Convert.ToString(Trim(dr.GetString(0)))
            End If
        Catch ex As Exception
            db.Close()


        Finally
            cmd = Nothing
            db.Close()
        End Try

    End Sub

    Protected Sub bbIntentos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bIntentos.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Update")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        Try
            Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
            Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString
            Dim db As SqlConnection = New SqlConnection(conexion)
            Dim cmd As SqlCommand
            Dim sqlBuilder As StringBuilder = New StringBuilder
            Dim intent = Intentos.Text
            sqlBuilder.Append("UPDATE intentos_ip   set opc_intentos = '" + intent + "' where opc_id = 1")

            Try
                db.Open()
            Catch ex As Exception
                db.Close()
            End Try
            Try
                cmd = New SqlCommand()
                cmd.Connection = db
                cmd.CommandType = CommandType.Text
                cmd.CommandText = sqlBuilder.ToString

                cmd.ExecuteNonQuery()


            Catch ex As Exception
                db.Close()
            Finally
                cmd = Nothing
                db.Close()
            End Try
            'Set Visible Toggle Panel

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub
End Class
