<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageStis.master" AutoEventWireup="false" CodeFile="CopyTerminalStis.aspx.vb" Inherits="Groups_EditTerminalstis" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Terminales STIS
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <%--<script type="text/javascript" src="../js/jquery.tipsy.js"></script>--%>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>
        <%--scripts menu--%>
    
 <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.min.js") %>'></script>
         <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery-ui-1.8.11.custom.min.js") %>'></script>
         <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.js") %>'></script>
    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/msgAlert.js") %>'></script>
     
         <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.contextMenu.js") %>'></script>
         <script type="text/javascript" src='<%= ResolveClientUrl("~/js/menuMethods.js") %>'></script>
   
      <%--scripts edit table--%>
  
  <link rel="stylesheet" type="text/css" href="/style/bootstrap.min.css" />

      <script type="text/javascript" src='<%= ResolveClientUrl("~/js/editTable.js") %>'></script>
   
    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.tabledit.js") %>'></script>
    
     <script type="text/javascript" src='<%= ResolveClientUrl("~/js/bootstrap.js") %>'></script>




</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>

    <li>Terminal STIS</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">

   Copiar Terminal 

     <%--   CODE : [ <%= objSessionParams.StrCode %> --%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <h4>
    Terminal Stis
    
    </h4>
    <%--<p>Este m�dulo le permite administrar las Terminales De STIS:</p>--%>
    <br />

    <!-- START SIMPLE FORM -->
    <div class="simplebox ">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text="Terminal copiada exitosamente"></asp:Label>
                <a href="" class="close tips" title="Cerrar" onclick = "ResetBtn" >Cerrar</a>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlError" runat="server" Visible="False">
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text="Hubo una falla al copiar los datos"></asp:Label>
                <a href="" class="close tips" title="Cerrar" onclick = "ResetBtn" >Cerrar</a>
            </div>
        </asp:Panel>

        <div class="toggle-message hidden" style="z-index: 590; top: 0px; left: 0px;">
            <h3 class="title">Filtro de B&uacute;squeda...
                <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
            <div class="hide-message" id="hide-message" style="display: none;">
                <div class="st-form-line">
                    <span class="st-labeltext"><b>Record No: </b></span>
                    <asp:TextBox ID="txtRecord" CssClass="st- "
                        runat="server" TabIndex="1" MaxLength="50"
                        ToolTip="Record Numero" onkeydown="return (event.keyCode!=13);" Width="250px"></asp:TextBox>
                </div>
                <div class="button-box">
                    <asp:Button ID="btnConsultar" runat="server" Text="Filtrar"
                        CssClass="button-aqua" TabIndex="3" ToolTip="Filtrar Terminales Stis..." />
                </div>
            </div>
        </div>

    </div>
    <div class="st-form">
        <span class="st-labeltext " style="width: 150px"><b>Terminal Record N�:</b></span>

        <asp:TextBox ID="txtOrigin" CssClass="st-success-input" Style="width: 510px "
            runat="server" TabIndex="1" MaxLength="100"
            ToolTip="Code." onkeydown="return (event.keyCode!=13);"
            Enabled="True"></asp:TextBox>

    </div>
    <br/>
    <div class="st-form">
        <span class="st-labeltext " style="width: 150px"><b>Terminal Dest N�:</b></span>

        <asp:TextBox ID="txtDest" CssClass="st-success-input" Style="width: 510px"
            runat="server" TabIndex="1" MaxLength="100"
            ToolTip="Code." onkeydown="return (event.keyCode!=13);"
            Enabled="True"></asp:TextBox>
    </div>
     
         <asp:Button ID="copyTerminals" runat="server" Text="Copiar"  CssClass="button-aqua" Style="margin: 10px  300px;" TabIndex="4"  />
                      
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
        <div class="titleh hidden">
            <h3>Adquiriente Terminal</h3>
            <div class="shortcuts-icons" style="z-index: 660;">
                <div class="shortcuts-icons" style="z-index: 660;">
                    <img src="../img/icons/16x16/add_options.png" width="25" height="25" alt="icon"></a>
                </div>
                <%--   <p>Tipo de Parametro "0" STANDARD</p>
                <p>Tipo de Parametro "1" EXTENDED</p>--%>
                <br />
            </div>
        </div>
        <div class="st-form-line">
            <%--     <span class="st-labeltext"><b>CODE:</b></span>

            <asp:TextBox ID="txtCode" CssClass="st-success-input" Style="width: 510px"
                runat="server" TabIndex="1" MaxLength="100"
                ToolTip="Code." onkeydown="return (event.keyCode!=13);"
                Enabled="False"></asp:TextBox>--%>
        </div>
    <ul id="navst">
	<li class="current"><a href="TerminalStis.aspx">Polaris TMS</a></li>
	<li><a>Tables</a>
		<ul>
			<li><a href="TerminalAcquirer.aspx">Acquirer</a></li>
			<li><a href="TerminalIssuer.aspx">Issuer</a>	</li>
			<li><a href="TerminalCardRange.aspx">CardRange</a></li>
			<li><a href="TerminalEmvLevel2.aspx">Emv Level 2 Application</a></li>
			<li><a href="TerminalEmvKey.aspx">Emv Level 2 Key</a></li>
			<li><a href="TerminalExtraApplication.aspx">Extra Application Parameter</a></li>
			<li><a href="ListarGroupPrompts.aspx">Grupos Prompts</a></li>
			<li><a href="ListarGroupVariousPayments.aspx">Grupos Pagos Varios</a></li>
            <li><a href="ListarGroupElectronicsPayments.aspx">Grupos Pagos Electronicos</a></li>
			<li><a href="TerminalHostConfiguration.aspx">Host Configuration</a></li>
			<li><a href="TerminalPrompts.aspx">Prompts</a></li>
			<li><a href="PagosVarios.aspx">Pagos Varios</a></li>
			<li><a href="PagosElectronicos.aspx">Pagos Electronicos</a></li>
			<li><a href="IP.aspx">Ip</a></li>
			<li><a href="AddCertificado.aspx">Certificados SSL</a></li>

		</ul>
	</li>
	<li><a>Terminal</a>
		<ul>
			<li><a href="AddTerminalStis.aspx">New Terminal</a></li>
            <li><a href="CopyTerminalStis.aspx">Copy Terminal</a></li>
			
		</ul>
	</li>
</ul>

          <script type="text/javascript">
            $(document).ready(function () {
                enableEditTable("#ctl00_MainContent_copyTerminalStis", true, false,true,false, [[2, 'Value']], "copyTerminalStis");
            });
        </script>
        <div class="form">
                            <div id="posStis" style="overflow: auto; width: auto; height: 200px;">

            <br />
            
            
        </div>

            
        </div>
    </div>

    <asp:HyperLink ID="HyperLink1" runat="server"
        NavigateUrl="~/Group/Terminalstis.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atr�s"/> Volver a la p�gina anterior</asp:HyperLink>

</asp:Content>


<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

    <script language="javascript">
        var globalcontrol = '';

        function msgProcedure() {
            if (!retVal) {
                document.getElementById("pnlMsg").style.display = "inline";
                setTimeout('document.location.reload()', 3000);
            }
            else {
                document.getElementById("pnlError").style.display = "inline";
                setTimeout('document.location.reload()',3000);
            }
        }

        function ConfirmAction(control) {

            globalcontrol = control;

            $.msgAlert({
                type: "warning"
                , title: "Mensaje del sistema"
                , text: "Realmente desea copiar la terminal?"
                , callback: function () {
                    __doPostBack($(globalcontrol).attr('name'), '');
                }
            });

            return false;
        }
    </script>

</asp:Content>

