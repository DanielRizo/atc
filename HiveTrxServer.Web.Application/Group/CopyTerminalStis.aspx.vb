Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Applications
Imports System.Data.Common


Partial Class Groups_EditTerminalstis
    Inherits TeleLoader.Web.BasePage

    Dim applicationObj As applicationSTIS2


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

    End Sub


    Protected Sub copyTerminals_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles copyTerminals.Click
        pnlError.Visible = False
        pnlMsg.Visible = False

        Dim origin As String = txtOrigin.Text
        Dim dest As String = txtDest.Text

        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Int16

        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
        System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)


        Dim contentValue As String
        contentValue = ""

        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webCopiarTerminal"
            command.Parameters.Clear()
            command.Parameters.Add(New SqlParameter("TERMINAL_ID", origin))
            command.Parameters.Add(New SqlParameter("NEW_TERMINAL_ID", dest))


            'Ejecutar SP
            results = command.ExecuteReader()
            status = 0
            If status = 0 Then
                pnlMsg.Visible = True
            End If


        Catch ex As Exception
            retVal = False
            pnlError.Visible = True
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try

    End Sub

    Protected Sub ResetBtn()
        pnlError.Visible = False
        pnlMsg.Visible = False
    End Sub





End Class
