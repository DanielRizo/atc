﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageCSS.master" AutoEventWireup="false" CodeFile="CreateGroupElectronicsPayments.aspx.vb" Inherits="Groups_CreateGroupPagosElectronicos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">

    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Crear Nuevo Grupo Pagos Electronicos
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

    <%--scripts edit table--%>

    <link rel="stylesheet" type="text/css" href="/style/bootstrap.min.css" />


    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/editTable.js") %>'></script>

    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.tabledit.js") %>'></script>

    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/bootstrap.js") %>'></script>

    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/moveElementsList.js") %>'></script>

    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/TypeInput.js") %>'></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

    <style>
        .detailPrompt {
            background-color: #f2f7fc;
            border: 1px solid black;
            padding: 2px;
            margin: 2px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="LinkButton1" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/TerminalStis.aspx">Terminales</asp:LinkButton>
    </li>

    <li>
        <asp:LinkButton ID="LinkButton2" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/ListarGroupElectronicsPayments.aspx">Listar Grupo Pagos Electronicos</asp:LinkButton>
    </li>

    <li>Crear Nuevo Grupo Pagos Electronicos.</li>
</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <blockquote>
        <p>Este módulo permite Crear nuevos rangos de Pagos Varios:</p>
        <footer class="blockquote-footer">Polaris Cloud Service (Pstis) </footer>
    </blockquote>
    <style>
        .bg-1 {
            background-color: #1abc9c;
            color: #ffffff;
        }
    </style>

    <div class="bg-1">
        <div class="container text-center">
            <h3>Creación de Acquirer Pstis</h3>
            <img src="https://image.freepik.com/vector-gratis/plantilla-recibo-pago-diseno-plano_23-2147903459.jpg" class="img-circle" alt="Bird" width="380" height="280">
        </div>
    </div>
    <div class="container-fluid bg-2 text-center">
        <h3>Creacion y edicion de grupo pagos varios</h3>
        <p>Este modulo permite crear nuevos grupos de pagos varios, una vez sean editados se podran Agregar al Tree View de las terminales. </p>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">

                    <ul id="navst">
                        <li class="navbar navbar-light"><a href="TerminalStis.aspx">PSTIS</a></li>
                        "
            <li><a>Tables</a>
                <ul>
                    <li><a href="TerminalAcquirer.aspx">Acquirer</a></li>
                    <li><a href="TerminalIssuer.aspx">Issuer</a>	</li>
                    <li><a href="TerminalCardRange.aspx">CardRange</a></li>
                    <li><a href="TerminalEmvLevel2.aspx">Emv Level 2 Application</a></li>
                    <li><a href="TerminalEmvKey.aspx">Emv Level 2 Key</a></li>
                    <li><a href="TerminalExtraApplication.aspx">Extra Application Parameter</a></li>
                    <li><a href="ListarGroupPrompts.aspx">Grupos Prompts</a></li>
                    <li><a href="ListarGroupVariousPayments.aspx">Grupos Pagos Varios</a></li>
                    <li><a href="ListarGroupElectronicsPayments.aspx">Grupos Pagos Electronicos</a></li>
                    <li><a href="TerminalHostConfiguration.aspx">Host Configuration</a></li>
                    <li><a href="TerminalPrompts.aspx">Prompts</a></li>
                    <li><a href="PagosVarios.aspx">Pagos Varios</a></li>
                    <li><a href="PagosElectronicos.aspx">Pagos Electronicos</a></li>
                    <li><a href="IP.aspx">Ip</a></li>
                    <li><a href="AddCertificado.aspx">Certificados SSL</a></li>
                </ul>
            </li>
                        <li><a>Terminal</a>
                            <ul>
                                <li><a href="AddTerminalStis.aspx">New Terminal</a></li>
                                <li><a href="CopyTerminalStis.aspx">Copy Terminal</a></li>

                            </ul>
                        </li>
                    </ul>
                </div>
            </div>

        </div>

    </div>
    <div id="alertCampos" class="alert alert-danger">
        <div>Debe llenar los campos requeridos.</div>
    </div>
    <script> $("#alertCampos").hide();</script>

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
        <div id="succes1" style="display: none;">
            <asp:Panel ID="pnlMsg" runat="server">
                <div class="alert alert-success">
                    <asp:Label ID="lblMsg" runat="server" Text="Datos almacenados correctamente."></asp:Label>
                    <a href="#" class="close tips" title="Cerrar"></a>
                </div>
            </asp:Panel>
        </div>

        <div id="error1" style="display: none;">
            <asp:Panel ID="pnlError" runat="server">
                <div class="alert alert-warning">
                    <asp:Label ID="lblError" runat="server" Text="Error almacenando datos del grupo."></asp:Label>
                    <a href="#" class="close tips" title="Cerrar"></a>
                </div>
            </asp:Panel>
        </div>
    </div>

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <div>
            <h3 class="title">Crear pagos Electronicos
                <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
        </div>
        <div class="panel panel-default" style="background-color: #fff; width: auto;">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-2">
                        <div class="" id="Code">
                            <span class="st-labeltext">Code: (*)</span>
                            <input id="txtCode" type="text" maxlength="5" onkeypress="return numeric_only (event, this);" />

                            <div class="clear"></div>
                        </div>
                        <small id="passwordHelpInline2" class="text-muted">Must be 5 characters long.
                        </small>
                        <div class="">
                            <span class="st-labeltext">Display Name: (*)</span>
                            <input id="txtName" type="text" maxlength="25" />
                            <div class="clear"></div>
                        </div>
                        <small id="passwordHelpInline54" class="text-muted">Must be 25 characters long.
                        </small>
                    </div>

                    <div class="col-sm-4" style="padding-left: 4%">
                        <select name="from" id="multiselect_left" class="form-control" size="15" multiple="multiple">
                            <optgroup label="Lista Pagos Electronicos">
                            </optgroup>
                        </select>
                        <div class="detailPrompt" id="detailPromptLeft"></div>

                    </div>
                    <script type="text/javascript">
                        $("#alertCampos").hide();

                        $(document).ready(function () {
                            $.ajax({
                                type: "POST",
                                url: "wsadd.asmx/" + "getPagosElectronicos",
                                data: '{"data":"' + "" + '"}',
                                contentType: "application/Json; charset=utf-8",
                                dataType: 'Json',
                                success: function (r) {
                                    // alert(r.stringify());
                                    var JsonRsp = JSON.parse(r.d);
                                    // alert(JsonRsp.registroPrompts[1].Codigo);
                                    for (i in JsonRsp.registroPagosElectronicos) {

                                        $('#multiselect_left').append('<option value="' + JsonRsp.registroPagosElectronicos[i].Codigo + '">' + JsonRsp.registroPagosElectronicos[i].PagosElectronicos + '</option>');
                                    }
                                },
                                error: function (r) {

                                },
                                failure: function (r) {

                                }
                            });
                        });     </script>

                    <div class="col-sm-1">
                        <button type="button" id="btn_rightAll" class="btn btn-info">
                            <img src="../img/fastforward.png" alt="rightAll" /></button><br />
                        <br />
                        <button type="button" id="btn_rightSelected" class="btn btn-info">
                            <img src="../img/right.png" alt="rightSelected" /></button><br />
                        <br />
                        <button type="button" id="btn_leftSelected" class="btn btn-info">
                            <img src="../img/left.png" alt="leftSelected" /></button><br />
                        <br />
                        <button type="button" id="btn_leftAll" class="btn btn-info">
                            <img src="../img/fastbackward.png" alt="leftAll" /></button><br />
                        <br />
                    </div>

                    <div class="col-sm-4">
                        <select name="from" id="multiselect_right" class="form-control" size="15" multiple="multiple">
                            <optgroup label="" id="Lista">
                            </optgroup>

                        </select>
                        <div class="detailPrompt" id="detailPromptRight"></div>

                    </div>

                </div>

            </div>



            <div class="button-box">

                <button id="ButGuardarGrupo" class="btn btn-info">Crear Grupo</button>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $('#ButGuardarGrupo').on('click', function (event) {
            $('#ButGuardarGrupo').prop('disabled', true);
            ShowProgressWindow();
            var Ids = "";
            var cont = 0;
            $("#txtCode").css("background", "White");
            $("#txtName").css("background", "White");
            $("#multiselect_right option").each(function () {
                Ids = Ids + "@" + $(this).val();
                cont = 1;

            });
            if (/*cont == 1 && */(!document.getElementById("txtCode").value == "") && (!document.getElementById("txtName").value == "")) {
                Ids = Ids + "@" + document.getElementById("txtCode").value + "@" + document.getElementById("txtName").value;
                event.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "wsadd.asmx/" + "addGroupPagosElectronicos",
                    data: '{"data":"' + Ids + '"}',
                    contentType: "application/Json; charset=utf-8",
                    dataType: 'Json',
                    success: function (r) {
                        document.getElementById("succes1").style.display = "inline";
                        setTimeout('document.location.reload()', 3000);
                    },
                    error: function (r) {
                        document.getElementById("error1").style.display = "inline";
                        setTimeout('document.location.reload()', 3000);

                    },
                    failure: function (r) {

                    }
                });
            }
            else {
                $("#alertCampos").fadeTo(0, 500)

                if (document.getElementById("txtName").value == "") {
                    $("#txtName").css("background", "Pink");
                }
                if (document.getElementById("txtCode").value == "") {
                    $("#txtCode").css("background", "Pink");
                }
                window.setTimeout(function () {
                    $("#alertCampos").fadeTo(500, 0).slideUp(500, function () {
                    });
                }, 3000);
                event.preventDefault();
            }

        });     </script>



    <div class="body" id="Tabla">

        <br />

    </div>
    <script src='<%= ResolveClientUrl("~/js/showDetailDataPrompts.js") %>'>
    </script>
    <asp:HyperLink ID="HyperLink1" runat="server"
        NavigateUrl="~/Group/ListarGroupElectronicsPayments.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>


<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

    <script language="javascript">
        var globalcontrol = '';

    </script>

</asp:Content>

