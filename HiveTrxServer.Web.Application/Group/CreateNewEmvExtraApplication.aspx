﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageStis.master" AutoEventWireup="false" CodeFile="CreateNewEmvExtraApplication.aspx.vb" Inherits="Groups_CreateEmvExtraApplication" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Parametros Acquierer STIS
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>
    
    
    <!-- jquery base -->
    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.min.js") %>'></script>
    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery-ui-1.8.11.custom.min.js") %>'></script>

    <%--scripts edit table--%>
  
  <link rel="stylesheet" type="text/css" href="/style/bootstrap.min.css" />

      <script type="text/javascript" src='<%= ResolveClientUrl("~/js/editTable.js") %>'></script>

    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/SaveTable.js") %>'></script>
   
    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.tabledit.js") %>'></script>
    
     <script type="text/javascript" src='<%= ResolveClientUrl("~/js/bootstrap.js") %>'></script>

              <%--scripts menu--%>
    
         <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.js") %>'></script>
    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/msgAlert.js") %>'></script>
     
         <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.contextMenu.js") %>'></script>
         <script type="text/javascript" src='<%= ResolveClientUrl("~/js/menuMethods.js") %>'></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>

    <li>Crear Extra App</li>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    Nueva Extra Application
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
         <div id="recNumber">
       <%= objSessionParams.StrTerminalRecord %>
            </div>
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlError" runat="server" Visible="False">
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>
     

    </div>

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
   

    <ul id="navst">
	<li class="current"><a href="TerminalStis.aspx">Polaris TMS</a></li>
	<li><a>Tables</a>
		<ul>
			<li><a href="TerminalAcquirer.aspx">Acquirer</a></li>
			<li><a href="TerminalIssuer.aspx">Issuer</a>	</li>
			<li><a href="TerminalCardRange.aspx">CardRange</a></li>
			<li><a href="TerminalEmvLevel2.aspx">Emv Level 2 Application</a></li>
			<li><a href="TerminalEmvKey.aspx">Emv Level 2 Key</a></li>
			<li><a href="TerminalExtraApplication.aspx">Extra Application Parameter</a></li>

		</ul>
	</li>

</ul>

       <div class="simplebox grid960">

            <div id="succes2"style="display: none;"  >
            <asp:Panel ID="Panel1" runat="server">
                <div class="albox succesbox">
                    <asp:Label ID="lblMsg2" runat="server" Text="Datos editados correctamente."></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">Cerrar</a>
                </div>
            </asp:Panel>
                </div> 
        
            <div id="error2"  style="display: none;">
            <asp:Panel ID="Panel2" runat="server">
                <div class="albox errorbox">
                    <asp:Label ID="lblError2" runat="server" Text="Error editando datos."></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">Cerrar</a>
                </div>
            </asp:Panel>
                </div>

        </div>


        <div class="st-form-line">
            <span class="st-labeltext"><b>CODE:</b></span>

            <asp:TextBox ID="txtCode" CssClass="st-success-input" Style="width: 510px"
                runat="server" TabIndex="1" MaxLength="100"
                ToolTip="Code." onkeydown="return (event.keyCode!=13);"
                Enabled="False"></asp:TextBox>
        </div>

        </div>
        <div class="st-form-line">
            <span class="st-labeltext"><b>NAME:</b></span>
             <asp:DropDownList ID="ddlOpc" class="uniform" runat="server"
                                DataSourceID="dsCardRange" DataTextField="APP_PARA_NAME"
                                DataValueField="APP_PARA_ID" Width="200px"
                                ToolTip="Seleccione App." TabIndex="0" AutoPostBack="true">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="dsCardRange" runat="server"
                                ConnectionString="<%$ ConnectionStrings:TeleLoaderStisConnectionString %>" SelectCommand="SELECT 'Seleccione Application' AS APP_PARA_NAME, ' ' AS APP_PARA_ID 
                                UNION ALL
                                SELECT APP_PARA_NAME,APP_PARA_ID FROM MASTER_PARAMETER_APP WHERE IS_HEADER_FIELD = '1'"></asp:SqlDataSource>
       </div>
                           
                            <div class="clear"></div>

     <center>
                <input type="button" class="button-aqua" id="initTerminals" value="Guardar" style="margin: 15px  3px;"/>
    </center> 
     <script type="text/javascript">

                    $('#initTerminals').on('click', function(event) {
                            event.preventDefault();
                        RecorrerTable("#ctl00_MainContent_grdTerminalsExtraApplication", "createNewEmvExtraApplication");
                    });    
     </script>

         <script type="text/javascript">
            $(document).ready(function () {
                enableEditTable("#ctl00_MainContent_grdTerminalsExtraApplication", false,true,false, [[2, 'Value']], "createNewEmvExtraApplication");
            });
        </script>      
                       
      <div class="body">
            <div id="posStis" style="overflow: auto; width: auto; height: 660px;">
            <br />
                <button id="btnAgregar" type="button" class="btn btn-block btn-info"><img src="../img/icons/16x16/plus.png" alt="icon" class="d-icon" />Agregar nuevo parametro</button>
            <asp:GridView ID="grdTerminalsExtraApplication" runat="server" AllowPaging="True"
                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4"
                DataSourceID="dsTerminalsExtraParameter"
                CssClass="mGridCenter" 
                EmptyDataText="No existen Issuer creados con el filtro aplicado"
                HorizontalAlign="Left" GridLines="None" ForeColor="#333333" >
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <%--<asp:CommandField ButtonType="Image"  HeaderImageUrl="~/img/icons/16x16/edit.png" ShowEditButton="True" UpdateImageUrl="~/img/icons/16x16/ok.png" CancelImageUrl="~/img/icons/16x16/cancel.png" editimageurl="~/img/icons/16x16/edit.png" />--%>
                    <asp:BoundField DataField="PARA_INDEX" HeaderText="Index"
                        SortExpression="PARA_INDEX" />
                    <asp:BoundField DataField="APP_PARA_NAME" HeaderText="Application Name"
                        SortExpression="APP_PARA_NAME" />
                    <asp:BoundField DataField="PARA_NAME" HeaderText="Name"
                        SortExpression="PARA_NAME" />

                    <asp:BoundField DataField="PARA_DESC" HeaderText="Description"
                        SortExpression="PARA_DESC" />
                    <asp:BoundField DataField="PARA_VALUE" HeaderText="Value"
                        SortExpression="PARA_VALUE" />
                    <asp:BoundField DataField="APP_PARA_MASK" HeaderText="Input Mask"
                        SortExpression="APP_PARA_MASK" readonly="true"/>
                 <%--   <asp:TemplateField ShowHeader="False" HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False"
                                CommandName="EditTerminalExtraApplication" ImageUrl="~/img/icons/16x16/edit.png" Text="Editar"
                                ToolTip="Editar ExtraApplication" CommandArgument='<%# grdTerminalsExtraApplication.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />
                        </ItemTemplate>

                        <HeaderStyle Width="100px"></HeaderStyle>

                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>--%>

                </Columns>

                <FooterStyle BackColor="#1C5E55" ForeColor="White" Font-Bold="True" />
                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center"
                    CssClass="pgr" />
                <RowStyle BackColor="#E3EAEB" />
                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BorderColor="#666666" BorderStyle="Solid"
                    BorderWidth="1px" BackColor="#50A2A3" />
                <SortedAscendingCellStyle BackColor="#F8FAFA" />
                <SortedAscendingHeaderStyle BackColor="#246B61" />
                <SortedDescendingCellStyle BackColor="#D4DFE1" />
                <SortedDescendingHeaderStyle BackColor="#15524A" />
            </asp:GridView>
            <br />
                </div>
            <asp:SqlDataSource ID="dsTerminalsExtraParameter" runat="server"
                ConnectionString="<%$ ConnectionStrings:TeleLoaderStisConnectionString %>"
                SelectCommand="sp_webListarExtraApplication_Stis" SelectCommandType="StoredProcedure" UpdateCommand="sp_webEditParameterApplication_Stis" UpdateCommandType="StoredProcedure">
                <SelectParameters>
                    <asp:Parameter Name="APP_PARA_NAME" Type="string" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="PARA_INDEX" Type="String" />
                    <asp:Parameter Name="APP_PARA_NAME" Type="String" />
                    <asp:Parameter Name="PARA_NAME" Type="String" />
                    <asp:Parameter Name="PARA_DESC" Type="String" />
                    <asp:Parameter Name="PARA_VALUE" Type="String" />
                 
                </UpdateParameters>
            </asp:SqlDataSource>
        </div>
    <asp:HyperLink ID="HyperLink1" runat="server"
        NavigateUrl="~/Group/TerminalAcquirer.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>


<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

    <script language="javascript">
        var globalcontrol = '';

        function ConfirmAction(control) {

            globalcontrol = control;

            $.msgAlert({
                type: "warning"
                , title: "Mensaje del sistema"
                , text: "Realmente desea eliminar el adquiriente?"
                , callback: function () {
                    __doPostBack($(globalcontrol).attr('name'), '');
                }
            });

            return false;
        }
    </script>

</asp:Content>

