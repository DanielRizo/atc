﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="EMVConfigItemsTerminal.aspx.vb" Inherits="Group_EMVConfigItemsTerminal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Items de Configuración EMV
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

    <script type="text/javascript" src="../js/jquery.ui.slider.js"></script>

    <script type="text/javascript" src="../js/jquery.ui.datepicker.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkListGroups" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/ListGroups.aspx">Listar Grupos</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkGroupTerminals" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/GroupTerminals.aspx">Terminales del Grupo</asp:LinkButton>
    </li>
    <li>Configuración EMV Terminal</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    Configuración EMV Terminal
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlError" runat="server" Visible="False">
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <div class="titleh">
            <h3>Datos de la Terminal</h3>
        </div>
        <div class="body">

            <div class="st-form-line">	
                <span class="st-labeltext">Grupo:</span>	
                <asp:TextBox ID="txtGroupName" CssClass="st-success-input" style="width:510px" 
                    runat="server" TabIndex="0" MaxLength="100" 
                    ToolTip="Nombre del Grupo." Enabled="False" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>

            <div class="st-form-line">
                <span class="st-labeltext">Serial:</span>
                <asp:TextBox ID="txtSerial" CssClass="st-success-input" Style="width: 150px"
                    runat="server" TabIndex="1" MaxLength="20" Enabled="False"
                    ToolTip="Serial de la terminal." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>

            <div style="padding-top: 6px; padding-left: 20px; padding-bottom: 145px;">

                Los siguientes módulos le permitirán configurar los parámetros EMV de la terminal:
                <br /><br />
                <asp:LinkButton ID="lnkEMVConfig" runat="server" CssClass="dashbutton" PostBackUrl="~/Group/EMVConfigTerminal.aspx" CausesValidation="false">
                    <asp:Image ID="lnkEMVConfigImg" runat="server" ImageUrl='../img/icons/dashbutton/EMVConfig.png' /> <b>EMV Config</b>
                </asp:LinkButton>

                <asp:LinkButton ID="lnkEMVKeys" runat="server" CssClass="dashbutton" PostBackUrl="~/Group/EMVKeysTerminal.aspx" CausesValidation="false">
                    <asp:Image ID="lnkEMVKeysImg" runat="server" ImageUrl='../img/icons/dashbutton/key.png' /> <b>EMV Keys</b>
                </asp:LinkButton>

            </div>

            <div class="clear"></div>

        </div>
    </div>

    <asp:HyperLink ID="lnkBack" runat="server" NavigateUrl="~/Group/GroupTerminals.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

</asp:Content>

