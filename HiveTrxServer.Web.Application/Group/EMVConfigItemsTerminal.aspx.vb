﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users
Imports System.Net
Imports System.Globalization
Imports TeleLoader.Terminals

Partial Class Group_EMVConfigItemsTerminal
    Inherits TeleLoader.Web.BasePage

    Dim terminalObj As Terminal

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            terminalObj = New Terminal(strConnectionString, objSessionParams.intSelectedGroup)

            'Leer datos BD
            terminalObj.TerminalID = objSessionParams.intSelectedTerminalID
            terminalObj.getTerminalData()

            txtSerial.Text = terminalObj.TerminalSerial

            txtGroupName.Text = objSessionParams.strtSelectedGroup

        End If

    End Sub

End Class
