﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="EMVConfigTerminal.aspx.vb" Inherits="Groups_EMVConfigTerminal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: EMV Config Terminal
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   

	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script> 
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkListGroups" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/ListGroups.aspx">Listar Grupos</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkGroupTerminals" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/GroupTerminals.aspx">Terminales del Grupo</asp:LinkButton>
    </li>
    <li>EMV Config Terminal</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    Grupo: [ <%= objSessionParams.strtSelectedGroup %> ]. Terminal ID: [ <%= objSessionParams.strSelectedTerminalSerial %> ]
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite administrar las configuraciones EMV para la Terminal:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>   
            
        <asp:Panel ID="pnlError" runat="server" Visible="False">    
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>  

        <div class="toggle-message" id="dragAndDropArea" style="z-index: 590;">
            <h3 class="title">Agregar/Editar Configuraci&oacute;n...
                <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
            <div class="hide-message" id="hide-message" style="display: none;">

                <div class="st-form-line">
                    <span class="st-labeltext">EMV Type:</span>
                    <asp:TextBox ID="txtEMVType" CssClass="st-forminput" style="width:300px" 
                        runat="server" TabIndex="1" MaxLength="2" 
                        ToolTip="EMV Type" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>

                <div class="st-form-line">
                    <span class="st-labeltext">EMV Config:</span>
                    <asp:TextBox ID="txtEMVConfig" CssClass="st-forminput" style="width:300px" 
                        runat="server" TabIndex="2" MaxLength="2" 
                        ToolTip="EMV Config" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>

                <div class="st-form-line">
                    <span class="st-labeltext">Threshold for biased RS:</span>
                    <asp:TextBox ID="txtThresholdRS" CssClass="st-forminput" style="width:300px" 
                        runat="server" TabIndex="3" MaxLength="8" 
                        ToolTip="EMV Config" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>

                <div class="st-form-line">
                    <span class="st-labeltext">Target for Random Selec.:</span>
                    <asp:TextBox ID="txtTargetRS" CssClass="st-forminput" style="width:300px" 
                        runat="server" TabIndex="4" MaxLength="2" 
                        ToolTip="Target for Random Selection" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>

                <div class="st-form-line">
                    <span class="st-labeltext">Max. Target for biased RS:</span>
                    <asp:TextBox ID="txtMaxTargetRS" CssClass="st-forminput" style="width:300px" 
                        runat="server" TabIndex="5" MaxLength="2" 
                        ToolTip="EMV Config" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>

                <div class="st-form-line">
                    <span class="st-labeltext">TAC Denial:</span>
                    <asp:TextBox ID="txtTACDenial" CssClass="st-forminput" style="width:300px" 
                        runat="server" TabIndex="6" MaxLength="10" 
                        ToolTip="TAC Denial" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>

                <div class="st-form-line">
                    <span class="st-labeltext">TAC Online:</span>
                    <asp:TextBox ID="txtTACOnline" CssClass="st-forminput" style="width:300px" 
                        runat="server" TabIndex="7" MaxLength="10" 
                        ToolTip="TAC Online" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>

                <div class="st-form-line">
                    <span class="st-labeltext">TAC Default:</span>
                    <asp:TextBox ID="txtTACDefault" CssClass="st-forminput" style="width:300px" 
                        runat="server" TabIndex="8" MaxLength="10" 
                        ToolTip="TAC Default" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>

                <div class="st-form-line">
                    <span class="st-labeltext">Application Config:</span>
                    <asp:TextBox ID="txtAppConfig" CssClass="st-forminput" style="width:697px; height: 60px;" 
                        runat="server" TabIndex="9" MaxLength="1024" TextMode="MultiLine"
                        ToolTip="Application Config" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>

                <div class="button-box">
                    <asp:Button ID="btnAddEMVConfig" runat="server" Text="Grabar" 
                        CssClass="button-aqua" TabIndex="10" OnClientClick="return validateAddOrEditEMVConfig();" />

                    <asp:Button ID="btnEditEMVConfig" runat="server" Text="Editar" Visible="false"
                        CssClass="button-aqua" TabIndex="10" OnClientClick="return validateAddOrEditEMVConfig();" />

                </div>

            </div>
        </div>

    </div>
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
	    <div class="titleh">
    	    <h3>Configuraciones EMV Creadas en la Terminal</h3>
        </div>
        <div class="body">    
            <br />            
            <asp:GridView ID="grdEMVConfigs" runat="server" AllowPaging="True" 
                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                DataSourceID="dsEMVConfigs" ForeColor="#333333"
                CssClass="mGridCenter" DataKeyNames="emv_id" 
                EmptyDataText="No existen Configuraciones EMV creadas en la Terminal." 
                HorizontalAlign="Center">
                <RowStyle BackColor="White" ForeColor="White" />
                <Columns>

                    <asp:BoundField DataField="emv_type" HeaderText="Type" 
                        SortExpression="emv_type" />
                    <asp:BoundField DataField="emv_config" HeaderText="Config" 
                        SortExpression="emv_config" />
                    <asp:BoundField DataField="emv_threshold_random_selection" HeaderText="Threshold RS" 
                        SortExpression="emv_threshold_random_selection" />
                    <asp:BoundField DataField="emv_target_random_selection" HeaderText="Target RS" 
                        SortExpression="emv_target_random_selection" />                    
                    <asp:BoundField DataField="emv_max_target_random_selection" HeaderText="Max Target RS" 
                        SortExpression="emv_max_target_random_selection" />
                    <asp:BoundField DataField="emv_tac_denial" HeaderText="TAC Denial" 
                        SortExpression="emv_tac_denial" />
                    <asp:BoundField DataField="emv_tac_online" HeaderText="TAC Online" 
                        SortExpression="emv_tac_online" />
                    <asp:BoundField DataField="emv_tac_default" HeaderText="TAC Default" 
                        SortExpression="emv_tac_default" />
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>  
                            
                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" 
                            CommandName="EditEMVConfig" ImageUrl="~/img/icons/16x16/edit.png" Text="Editar" 
                            ToolTip="Editar Configuración EMV" CommandArgument='<%# grdEMVConfigs.Rows.Count%>' style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />                            
                                                  
                            <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False" 
                                CommandName="Delete" ImageUrl="~/img/icons/16x16/delete.png" Text="Eliminar" 
                                ToolTip="Eliminar Configuración EMV de la Terminal" CommandArgument='<%# grdEMVConfigs.Rows.Count%>' style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />

                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
                    CssClass="pgr" Font-Underline="False" />
                <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid" 
                    BorderWidth="1px" />
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>            
            <br />
	        <asp:SqlDataSource ID="dsEMVConfigs" runat="server" 
                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"                 
                SelectCommand="sp_webConsultarConfigEMVTerminal" SelectCommandType="StoredProcedure"
                DeleteCommand="sp_webEliminarConfigEMVTerminal" DeleteCommandType="StoredProcedure" ConflictDetection="OverwriteChanges" >
                <SelectParameters>
                    <asp:Parameter Name="terminalId" Type="String" />
                </SelectParameters>
                <DeleteParameters>
                    <asp:Parameter Name="emvConfigId" Type="String" />
                    <asp:Parameter Name="terminalId" Type="String" />
                </DeleteParameters>
            </asp:SqlDataSource> 
        </div>
    </div>
    
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Group/EMVConfigItemsTerminal.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">

    <!-- Validator -->
    <script src="../js/ValidatorEMVConfigTerminal.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

