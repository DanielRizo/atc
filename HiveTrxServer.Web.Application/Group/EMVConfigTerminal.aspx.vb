﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Data.Common
Imports TeleLoader.EMV

Partial Class Groups_EMVConfigTerminal
    Inherits TeleLoader.Web.BasePage

    Private objEMVconfig As EMV_Config

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            txtEMVType.Focus()

            dsEMVConfigs.SelectParameters("terminalId").DefaultValue = objSessionParams.intSelectedTerminalID

            dsEMVConfigs.DataBind()

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub grdEMVConfigs_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdEMVConfigs.RowCommand
        Try

            Select Case e.CommandName

                Case "Delete"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    grdEMVConfigs.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    dsEMVConfigs.DeleteParameters("emvConfigId").DefaultValue = grdEMVConfigs.SelectedDataKey.Values.Item("emv_id")
                    dsEMVConfigs.DeleteParameters("terminalId").DefaultValue = objSessionParams.intSelectedTerminalID

                    Dim strData As String = "EMV Config ID: " & grdEMVConfigs.SelectedDataKey.Values.Item("emv_id") & ", Terminal ID: " & objSessionParams.intSelectedTerminalID

                    dsEMVConfigs.Delete()

                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Configuración eliminada de la Terminal. Datos[ " & strData & " ]", "")

                    grdEMVConfigs.DataBind()

                    pnlError.Visible = False
                    pnlMsg.Visible = True
                    lblMsg.Text = "Config EMV eliminada de la Terminal"

                    grdEMVConfigs.SelectedIndex = -1

                    clearForm()

                    'Set Invisible Toggle Panel
                    ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"

                Case "EditEMVConfig"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Update")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    grdEMVConfigs.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.intEMVConfigId = grdEMVConfigs.SelectedDataKey.Values.Item("emv_id")

                    objEMVconfig = New EMV_Config(strConnectionString, grdEMVConfigs.SelectedDataKey.Values.Item("emv_id"), -1)

                    objEMVconfig.getEMVConfig()

                    txtEMVType.Text = objEMVconfig.Type
                    txtEMVConfig.Text = objEMVconfig.Config
                    txtThresholdRS.Text = objEMVconfig.ThresholdRS
                    txtTargetRS.Text = objEMVconfig.TargetRS
                    txtMaxTargetRS.Text = objEMVconfig.MaxTargetRS
                    txtTACDenial.Text = objEMVconfig.TACDenial
                    txtTACOnline.Text = objEMVconfig.TACOnline
                    txtTACDefault.Text = objEMVconfig.TACDefault
                    txtAppConfig.Text = objEMVconfig.ApplicationConfig

                    btnAddEMVConfig.Visible = False
                    btnEditEMVConfig.Visible = True

                    'Set Visible Toggle Panel
                    ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"

            End Select

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub

    Protected Sub btnAddEMVConfig_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddEMVConfig.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Save")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try


        Try
            objEMVconfig = New EMV_Config(strConnectionString, -1, objSessionParams.intSelectedTerminalID)

            'Establecer Datos de la Configuración
            objEMVconfig.Type = txtEMVType.Text.Trim
            objEMVconfig.Config = txtEMVConfig.Text.Trim
            objEMVconfig.ThresholdRS = txtThresholdRS.Text.Trim
            objEMVconfig.TargetRS = txtTargetRS.Text.Trim
            objEMVconfig.MaxTargetRS = txtMaxTargetRS.Text.Trim
            objEMVconfig.TACDenial = txtTACDenial.Text.Trim
            objEMVconfig.TACOnline = txtTACOnline.Text.Trim
            objEMVconfig.TACDefault = txtTACDefault.Text.Trim
            objEMVconfig.ApplicationConfig = txtAppConfig.Text.Trim

            'Save Data
            If objEMVconfig.createEMVConfig() Then
                'New Config EMV Terminal Created OK
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Configuración EMV Creada en la Terminal. Datos[ " & getFormDataLog() & " ]", "")

                pnlMsg.Visible = True
                pnlError.Visible = False
                lblMsg.Text = "Configuración EMV Creada en la Terminal correctamente."

                dsEMVConfigs.SelectParameters("terminalId").DefaultValue = objSessionParams.intSelectedTerminalID

                grdEMVConfigs.DataBind()

                clearForm()

            Else
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error agregando Configuración EMV a la Terminal. Datos[ " & getFormDataLog() & " ]", "")
                pnlError.Visible = True
                pnlMsg.Visible = False
                lblError.Text = "Error agregando Configuración EMV a la Terminal. Por favor revise los datos."

            End If

            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub

    Private Sub clearForm()
        txtEMVType.Text = ""
        txtEMVConfig.Text = ""
        txtThresholdRS.Text = ""
        txtTargetRS.Text = ""
        txtMaxTargetRS.Text = ""
        txtTACDenial.Text = ""
        txtTACOnline.Text = ""
        txtTACDefault.Text = ""
        txtAppConfig.Text = ""
    End Sub

    Protected Sub dsEMVConfigs_Deleting(sender As Object, e As SqlDataSourceCommandEventArgs) Handles dsEMVConfigs.Deleting

        'Remover parametro extra, igual al datakeyname
        If e.Command.Parameters.Count > 2 Then
            Dim paramEmvId As DbParameter = e.Command.Parameters("@emv_id")
            e.Command.Parameters.Remove(paramEmvId)
        End If

    End Sub

    Protected Sub btnEditEMVConfig_Click(sender As Object, e As EventArgs) Handles btnEditEMVConfig.Click
        Try
            objEMVconfig = New EMV_Config(strConnectionString, objSessionParams.intEMVConfigId, objSessionParams.intSelectedTerminalID)

            'Establecer Datos de la Configuración
            objEMVconfig.Type = txtEMVType.Text.Trim
            objEMVconfig.Config = txtEMVConfig.Text.Trim
            objEMVconfig.ThresholdRS = txtThresholdRS.Text.Trim
            objEMVconfig.TargetRS = txtTargetRS.Text.Trim
            objEMVconfig.MaxTargetRS = txtMaxTargetRS.Text.Trim
            objEMVconfig.TACDenial = txtTACDenial.Text.Trim
            objEMVconfig.TACOnline = txtTACOnline.Text.Trim
            objEMVconfig.TACDefault = txtTACDefault.Text.Trim
            objEMVconfig.ApplicationConfig = txtAppConfig.Text.Trim

            'Edit Data
            If objEMVconfig.editEMVConfig() Then
                'New Config EMV Terminal Created OK
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Update", "Configuración EMV Actualizada en la Terminal. Datos[ " & getFormDataLog() & " ]", "")

                pnlMsg.Visible = True
                pnlError.Visible = False
                lblMsg.Text = "Configuración EMV Actualizada en la Terminal correctamente."

                dsEMVConfigs.SelectParameters("terminalId").DefaultValue = objSessionParams.intSelectedTerminalID

                grdEMVConfigs.DataBind()

                btnAddEMVConfig.Visible = True
                btnEditEMVConfig.Visible = False

                clearForm()

                grdEMVConfigs.SelectedIndex = -1

            Else
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Update", "Error actualizando Configuración EMV en la Terminal. Datos[ " & getFormDataLog() & " ]", "")
                pnlError.Visible = True
                pnlMsg.Visible = False
                lblError.Text = "Error actualizando Configuración EMV en la Terminal. Por favor revise los datos."

            End If

            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub
End Class
