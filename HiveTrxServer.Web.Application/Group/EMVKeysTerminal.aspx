﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="EMVKeysTerminal.aspx.vb" Inherits="Groups_EMVKeysTerminal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: EMV Keys Terminal
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   

	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script> 
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkListGroups" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/ListGroups.aspx">Listar Grupos</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkGroupTerminals" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/GroupTerminals.aspx">Terminales del Grupo</asp:LinkButton>
    </li>
    <li>EMV Keys Terminal</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    Grupo: [ <%= objSessionParams.strtSelectedGroup %> ]. Terminal ID: [ <%= objSessionParams.strSelectedTerminalSerial %> ]
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite administrar las llaves EMV para la Terminal:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>   
            
        <asp:Panel ID="pnlError" runat="server" Visible="False">    
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>  

        <div class="toggle-message" id="dragAndDropArea" style="z-index: 590;">
            <h3 class="title">Agregar/Editar Llaves...
                <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
            <div class="hide-message" id="hide-message" style="display: none;">

                <div class="st-form-line">
                    <span class="st-labeltext">Key Index:</span>
                    <asp:TextBox ID="txtKeyIndex" CssClass="st-forminput" style="width:300px" 
                        runat="server" TabIndex="1" MaxLength="2" 
                        ToolTip="CA Key Index" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>

                <div class="st-form-line">
                    <span class="st-labeltext">Application ID:</span>
                    <asp:TextBox ID="txtAppId" CssClass="st-forminput" style="width:300px" 
                        runat="server" TabIndex="2" MaxLength="10" 
                        ToolTip="Application ID" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>

                <div class="st-form-line">
                    <span class="st-labeltext">Key Exponent:</span>
                    <asp:TextBox ID="txtKeyExponent" CssClass="st-forminput" style="width:300px" 
                        runat="server" TabIndex="3" MaxLength="8" 
                        ToolTip="CA Key Exponent" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>

                <div class="st-form-line">
                    <span class="st-labeltext">Key Size:</span>
                    <asp:TextBox ID="txtKeySize" CssClass="st-forminput" style="width:300px" 
                        runat="server" TabIndex="4" MaxLength="8" 
                        ToolTip="CA Key Size" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>

                <div class="st-form-line">
                    <span class="st-labeltext">Key Content:</span>
                    <asp:TextBox ID="txtKeyContent" CssClass="st-forminput" style="width:697px; height: 70px;" 
                        runat="server" TabIndex="5" MaxLength="512" TextMode="MultiLine"
                        ToolTip="CA Key content" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>

                <div class="st-form-line">
                    <span class="st-labeltext">Key Expiry Date:</span>
                    <asp:TextBox ID="txtKeyExpiryDate" CssClass="st-forminput" style="width:300px" 
                        runat="server" TabIndex="6" MaxLength="4" 
                        ToolTip="CA Key Expiry Date" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>

                <div class="st-form-line">
                    <span class="st-labeltext">Key Effective Date:</span>
                    <asp:TextBox ID="txtKeyEffectiveDate" CssClass="st-forminput" style="width:300px" 
                        runat="server" TabIndex="7" MaxLength="4" 
                        ToolTip="CA Key Effective Date" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>

                <div class="st-form-line">
                    <span class="st-labeltext">Checksum:</span>
                    <asp:TextBox ID="txtChecksum" CssClass="st-forminput" style="width:300px" 
                        runat="server" TabIndex="8" MaxLength="2" 
                        ToolTip="Checksum" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>

                <div class="button-box">
                    <asp:Button ID="btnAddEMVKey" runat="server" Text="Grabar" 
                        CssClass="button-aqua" TabIndex="9" OnClientClick="return validateAddOrEditEMVKey();" />

                    <asp:Button ID="btnEditEMVKey" runat="server" Text="Editar" Visible="false"
                        CssClass="button-aqua" TabIndex="9" OnClientClick="return validateAddOrEditEMVKey();" />

                </div>

            </div>
        </div>

    </div>
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
	    <div class="titleh">
    	    <h3>Llaves EMV Creadas en la Terminal</h3>
        </div>
        <div class="body">    
            <br />            
            <asp:GridView ID="grdEMVKeys" runat="server" AllowPaging="True" 
                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                DataSourceID="dsEMVKeys" ForeColor="#333333"
                CssClass="mGridCenter" DataKeyNames="key_id" 
                EmptyDataText="No existen Llaves EMV creadas en la Terminal." 
                HorizontalAlign="Center">
                <RowStyle BackColor="White" ForeColor="White" />
                <Columns>

                    <asp:BoundField DataField="key_index" HeaderText="Index" 
                        SortExpression="key_index" />
                    <asp:BoundField DataField="key_application_id" HeaderText="App. ID" 
                        SortExpression="key_application_id" />
                    <asp:BoundField DataField="key_exponent" HeaderText="Exponent" 
                        SortExpression="key_exponent" />
                    <asp:BoundField DataField="key_size" HeaderText="Size" 
                        SortExpression="key_size" />                    
                    <asp:BoundField DataField="key_expiry_date" HeaderText="Expiry Date" 
                        SortExpression="key_expiry_date" />
                    <asp:BoundField DataField="key_effective_date" HeaderText="Effective date" 
                        SortExpression="key_effective_date" />
                    <asp:BoundField DataField="key_checksum" HeaderText="Checksum" 
                        SortExpression="key_checksum" />
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>  
                            
                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" 
                            CommandName="EditEMVKey" ImageUrl="~/img/icons/16x16/edit.png" Text="Editar" 
                            ToolTip="Editar Llave EMV" CommandArgument='<%# grdEMVKeys.Rows.Count%>' style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />                            
                                                  
                            <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False" 
                                CommandName="Delete" ImageUrl="~/img/icons/16x16/delete.png" Text="Eliminar" 
                                ToolTip="Eliminar Llave EMV de la Terminal" CommandArgument='<%# grdEMVKeys.Rows.Count%>' style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />

                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
                    CssClass="pgr" Font-Underline="False" />
                <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid" 
                    BorderWidth="1px" />
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>            
            <br />
	        <asp:SqlDataSource ID="dsEMVKeys" runat="server" 
                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"                 
                SelectCommand="sp_webConsultarKeysEMVTerminal" SelectCommandType="StoredProcedure"
                DeleteCommand="sp_webEliminarKeyEMVTerminal" DeleteCommandType="StoredProcedure" ConflictDetection="OverwriteChanges" >
                <SelectParameters>
                    <asp:Parameter Name="terminalId" Type="String" />
                </SelectParameters>
                <DeleteParameters>
                    <asp:Parameter Name="emvKeyId" Type="String" />
                    <asp:Parameter Name="terminalId" Type="String" />
                </DeleteParameters>
            </asp:SqlDataSource> 
        </div>
    </div>
    
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Group/EMVConfigItemsTerminal.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">

    <!-- Validator -->
    <script src="../js/ValidatorEMVConfigTerminal.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

