﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Data.Common
Imports TeleLoader.EMV

Partial Class Groups_EMVKeysTerminal
    Inherits TeleLoader.Web.BasePage

    Private objEMVKey As EMV_Key

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            txtKeyIndex.Focus()

            dsEMVKeys.SelectParameters("terminalId").DefaultValue = objSessionParams.intSelectedTerminalID

            dsEMVKeys.DataBind()

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub grdEMVKeys_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdEMVKeys.RowCommand
        Try

            Select Case e.CommandName

                Case "Delete"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    grdEMVKeys.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    dsEMVKeys.DeleteParameters("emvKeyId").DefaultValue = grdEMVKeys.SelectedDataKey.Values.Item("key_id")
                    dsEMVKeys.DeleteParameters("terminalId").DefaultValue = objSessionParams.intSelectedTerminalID

                    Dim strData As String = "EMV Key ID: " & grdEMVKeys.SelectedDataKey.Values.Item("key_id") & ", Terminal ID: " & objSessionParams.intSelectedTerminalID

                    dsEMVKeys.Delete()

                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Llave EMV eliminada de la Terminal. Datos[ " & strData & " ]", "")

                    dsEMVKeys.DataBind()

                    pnlError.Visible = False
                    pnlMsg.Visible = True
                    lblMsg.Text = "Llave EMV eliminada de la Terminal"

                    grdEMVKeys.SelectedIndex = -1

                    clearForm()

                    'Set Invisible Toggle Panel
                    ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"

                Case "EditEMVKey"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Update")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    grdEMVKeys.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.intEMVKeyId = grdEMVKeys.SelectedDataKey.Values.Item("key_id")

                    objEMVKey = New EMV_Key(strConnectionString, grdEMVKeys.SelectedDataKey.Values.Item("key_id"), -1)

                    objEMVKey.getEMVKey()

                    txtKeyIndex.Text = objEMVKey.Index
                    txtAppId.Text = objEMVKey.ApplicationId
                    txtKeyExponent.Text = objEMVKey.Exponent
                    txtKeySize.Text = objEMVKey.Size
                    txtKeyContent.Text = objEMVKey.Content
                    txtKeyExpiryDate.Text = objEMVKey.ExpiryDate
                    txtKeyEffectiveDate.Text = objEMVKey.EffectiveDate
                    txtChecksum.Text = objEMVKey.Checksum

                    btnAddEMVKey.Visible = False
                    btnEditEMVKey.Visible = True

                    'Set Visible Toggle Panel
                    ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"

            End Select

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub

    Protected Sub btnAddEMVKey_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddEMVKey.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Save")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try


        Try
            objEMVKey = New EMV_Key(strConnectionString, -1, objSessionParams.intSelectedTerminalID)

            'Establecer Datos de la Llave
            objEMVKey.Index = txtKeyIndex.Text.Trim
            objEMVKey.ApplicationId = txtAppId.Text.Trim
            objEMVKey.Exponent = txtKeyExponent.Text.Trim
            objEMVKey.Size = txtKeySize.Text.Trim
            objEMVKey.Content = txtKeyContent.Text.Trim
            objEMVKey.ExpiryDate = txtKeyExpiryDate.Text.Trim
            objEMVKey.EffectiveDate = txtKeyEffectiveDate.Text.Trim
            objEMVKey.Checksum = txtChecksum.Text.Trim

            'Save Data
            If objEMVKey.createEMVKey() Then
                'New Config EMV Terminal Created OK
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Llave EMV Creada en la Terminal. Datos[ " & getFormDataLog() & " ]", "")

                pnlMsg.Visible = True
                pnlError.Visible = False
                lblMsg.Text = "Llave EMV Creada en la Terminal correctamente."

                dsEMVKeys.SelectParameters("terminalId").DefaultValue = objSessionParams.intSelectedTerminalID

                grdEMVKeys.DataBind()

                clearForm()

            Else
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error agregando Llave EMV a la Terminal. Datos[ " & getFormDataLog() & " ]", "")
                pnlError.Visible = True
                pnlMsg.Visible = False
                lblError.Text = "Error agregando Llave EMV a la Terminal. Por favor revise los datos."

            End If

            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub

    Private Sub clearForm()
        txtKeyIndex.Text = ""
        txtAppId.Text = ""
        txtKeyExponent.Text = ""
        txtKeySize.Text = ""
        txtKeyContent.Text = ""
        txtKeyExpiryDate.Text = ""
        txtKeyEffectiveDate.Text = ""
        txtChecksum.Text = ""
    End Sub

    Protected Sub dsEMVKeys_Deleting(sender As Object, e As SqlDataSourceCommandEventArgs) Handles dsEMVKeys.Deleting

        'Remover parametro extra, igual al datakeyname
        If e.Command.Parameters.Count > 2 Then
            Dim paramEmvId As DbParameter = e.Command.Parameters("@key_id")
            e.Command.Parameters.Remove(paramEmvId)
        End If

    End Sub

    Protected Sub btnEditEMVKey_Click(sender As Object, e As EventArgs) Handles btnEditEMVKey.Click
        Try
            objEMVKey = New EMV_Key(strConnectionString, objSessionParams.intEMVKeyId, objSessionParams.intSelectedTerminalID)

            'Establecer Datos de la Llave
            objEMVKey.Index = txtKeyIndex.Text.Trim
            objEMVKey.ApplicationId = txtAppId.Text.Trim
            objEMVKey.Exponent = txtKeyExponent.Text.Trim
            objEMVKey.Size = txtKeySize.Text.Trim
            objEMVKey.Content = txtKeyContent.Text.Trim
            objEMVKey.ExpiryDate = txtKeyExpiryDate.Text.Trim
            objEMVKey.EffectiveDate = txtKeyEffectiveDate.Text.Trim
            objEMVKey.Checksum = txtChecksum.Text.Trim

            'Edit Data
            If objEMVKey.editEMVKey() Then
                'New Config EMV Terminal Created OK
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Update", "Llave EMV Actualizada en la Terminal. Datos[ " & getFormDataLog() & " ]", "")

                pnlMsg.Visible = True
                pnlError.Visible = False
                lblMsg.Text = "Llave EMV Actualizada en la Terminal correctamente."

                dsEMVKeys.SelectParameters("terminalId").DefaultValue = objSessionParams.intSelectedTerminalID

                grdEMVKeys.DataBind()

                btnAddEMVKey.Visible = True
                btnEditEMVKey.Visible = False

                clearForm()

                grdEMVKeys.SelectedIndex = -1

            Else
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Update", "Error actualizando Llave EMV en la Terminal. Datos[ " & getFormDataLog() & " ]", "")
                pnlError.Visible = True
                pnlMsg.Visible = False
                lblError.Text = "Error actualizando Llave EMV en la Terminal. Por favor revise los datos."

            End If

            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub
End Class
