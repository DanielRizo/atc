﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageStis.master" AutoEventWireup="false" CodeFile="EditAcquirer.aspx.vb" Inherits="Groups_EditTerminalstis" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Terminales STIS
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <%--<script type="text/javascript" src="../js/jquery.tipsy.js"></script>--%>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>
        <%--scripts menu--%>
    
 <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.min.js") %>'></script>
         <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery-ui-1.8.11.custom.min.js") %>'></script>
         <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.js") %>'></script>
    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/msgAlert.js") %>'></script>
     
         <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.contextMenu.js") %>'></script>
         <script type="text/javascript" src='<%= ResolveClientUrl("~/js/menuMethods.js") %>'></script>
   
      <%--scripts edit table--%>
  
  <link rel="stylesheet" type="text/css" href="/style/bootstrap.min.css" />

      <script type="text/javascript" src='<%= ResolveClientUrl("~/js/editTable.js") %>'></script>
   
    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.tabledit.js") %>'></script>
    
     <script type="text/javascript" src='<%= ResolveClientUrl("~/js/bootstrap.js") %>'></script>




</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>

    <li>ADQUIRIENTE STIS</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    Terminal > Acquirer
     <%--   CODE : [ <%= objSessionParams.StrCode %> --%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <h4>
    Terminal TID : [ <%= objSessionParams.StrTerminalID %> ]
    RECORD No : [ <%= objSessionParams.StrTerminalRecord %> ]
    </h4>
    <%--<p>Este módulo le permite administrar las Terminales De STIS:</p>--%>
    <br />

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlError" runat="server" Visible="False">
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>
        <div class="toggle-message hidden" style="z-index: 590; top: 0px; left: 0px;">
            <h3 class="title">Filtro de B&uacute;squeda...
                <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
            <div class="hide-message" id="hide-message" style="display: none;">
                <div class="st-form-line">
                    <span class="st-labeltext"><b>Record No: </b></span>
                    <asp:TextBox ID="txtRecord" CssClass="st- "
                        runat="server" TabIndex="1" MaxLength="50"
                        ToolTip="Record Numero" onkeydown="return (event.keyCode!=13);" Width="250px"></asp:TextBox>
                </div>
                <div class="button-box">
                    <asp:Button ID="btnConsultar" runat="server" Text="Filtrar"
                        CssClass="button-aqua" TabIndex="3" ToolTip="Filtrar Terminales Stis..." />
                </div>
            </div>
        </div>

    </div>
    <div class="st-form-line">
        <span class="st-labeltext"><b>Terminal ID:</b></span>

        <asp:TextBox ID="txtCode" CssClass="st-success-input" Style="width: 510px"
            runat="server" TabIndex="1" MaxLength="100"
            ToolTip="Code." onkeydown="return (event.keyCode!=13);"
            Enabled="False"></asp:TextBox>

    </div>
    <div class="st-form-line">
        <span class="st-labeltext"><b>Terminal No:</b></span>

        <asp:TextBox ID="TxtNameAcquirer" CssClass="st-success-input" Style="width: 510px"
            runat="server" TabIndex="1" MaxLength="100"
            ToolTip="Code." onkeydown="return (event.keyCode!=13);"
            Enabled="False"></asp:TextBox>
    </div>
   

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
        <div class="titleh hidden">
            <h3>Adquiriente Terminal</h3>
            <div class="shortcuts-icons" style="z-index: 660;">
                <div class="shortcuts-icons" style="z-index: 660;">
                    <img src="../img/icons/16x16/add_options.png" width="25" height="25" alt="icon"></a>
                </div>
                <%--   <p>Tipo de Parametro "0" STANDARD</p>
                <p>Tipo de Parametro "1" EXTENDED</p>--%>
                <br />
            </div>
        </div>
        <div class="st-form-line">
            <%--     <span class="st-labeltext"><b>CODE:</b></span>

            <asp:TextBox ID="txtCode" CssClass="st-success-input" Style="width: 510px"
                runat="server" TabIndex="1" MaxLength="100"
                ToolTip="Code." onkeydown="return (event.keyCode!=13);"
                Enabled="False"></asp:TextBox>--%>
        </div>
              <ul id="navst">
	<li class="current"><a href="TerminalStis.aspx">Polaris TMS</a></li>
	<li><a>Tables</a>
		<ul>
			<li><a href="TerminalAcquirer.aspx">Acquirer</a></li>
			<li><a href="TerminalIssuer.aspx">Issuer</a>	</li>
			<li><a href="TerminalCardRange.aspx">CardRange</a></li>
			<li><a href="TerminalEmvLevel2.aspx">Emv Level 2 Application</a></li>
			<li><a href="TerminalEmvKey.aspx">Emv Level 2 Key</a></li>
			<li><a href="TerminalExtraApplication.aspx">Extra Application Parameter</a></li>

		</ul>
	</li>
	
</ul>


          <script type="text/javascript">
            $(document).ready(function () {
                enableEditTable("#ctl00_MainContent_grdTerminalsParameter", false,true,false, [[2, 'Value']], "editAcquirer");
            });
        </script>
        <div class="body">
                            <div id="posStis" style="overflow: auto; width: auto; height: 660px;">

            <br />
            <asp:GridView ID="grdTerminalsParameter" runat="server"
                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4"
                DataSourceID="dsTerminalsParameter"
                CssClass="mGridCenter" DataKeyNames="TERMINAL_REC_NO,REFER_CODE"
                EmptyDataText="No existen Terminales creadas en el Grupo o con el filtro aplicado"
                HorizontalAlign="Center"  ForeColor="#333333" GridLines="None">
                <RowStyle BackColor="#E3EAEB" />
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <%--<asp:CommandField ButtonType="Image"  HeaderImageUrl="~/img/icons/16x16/edit.png" ShowEditButton="True" UpdateImageUrl="~/img/icons/16x16/ok.png" CancelImageUrl="~/img/icons/16x16/cancel.png" editimageurl="~/img/icons/16x16/edit.png" />--%>
                    <asp:BoundField DataField="TERMINAL_REC_NO" HeaderText="Terminal Rec No"
                        SortExpression="TERMINAL_REC_NO" />
                    <asp:BoundField DataField="FIELD_DISPLAY_NAME" HeaderText="Parameter Name"
                        SortExpression="FIELD_DISPLAY_NAME" />
                    <asp:BoundField DataField="CONTENT_DESC" HeaderText="Value"
                        SortExpression="CONTENT_DESC" />
                    <asp:BoundField DataField="IS_EXTENDED" HeaderText="Parameter Type"
                        SortExpression="IS_EXTENDED" ReadOnly="true" />
                    <asp:BoundField DataField="NOTE" HeaderText="Note"
                        SortExpression="NOTE" ReadOnly="true" />
                    <asp:BoundField DataField="REFER_CODE" HeaderText="Refer Code"
                        SortExpression="REFER_CODE" />
                    <%-- <asp:TemplateField ShowHeader="False" HeaderStyle-Width="100px">
                        <ItemTemplate>

                            <asp:ImageButton ID="ImageButton2" runat="server" CausesValidation="False"
                                CommandName="EditTerminalIssuer" ImageUrl="~/img/icons/16x16/add_options.png" Text="Editar"
                                ToolTip="Issuer Por Terminal" CommandArgument='<%# grdTerminalsParameter.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />
                        </ItemTemplate>

                        <HeaderStyle Width="100px"></HeaderStyle>

                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>--%>
                </Columns>

                <FooterStyle BackColor="#1C5E55" ForeColor="White" Font-Bold="True" />
                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center"
                    CssClass="pgr" />
                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
               <EditRowStyle BorderColor="#666666" BorderStyle="Solid"
                    BorderWidth="1px" BackColor="#50A2A3" />
                <SortedAscendingCellStyle BackColor="#F8FAFA" />
                <SortedAscendingHeaderStyle BackColor="#246B61" />
                <SortedDescendingCellStyle BackColor="#D4DFE1" />
                <SortedDescendingHeaderStyle BackColor="#15524A" />
            </asp:GridView>
            <br />
        </div>

            <asp:SqlDataSource ID="dsTerminalsParameter" runat="server"
                ConnectionString="<%$ ConnectionStrings:TeleLoaderStisConnectionString %>"
                SelectCommand="sp_webListarAcquirer_Stis" SelectCommandType="StoredProcedure"
                UpdateCommand="sp_webEditDatosAcquirer_Stis" UpdateCommandType="StoredProcedure">
                <SelectParameters>
                    <asp:Parameter Name="TERMINAL_REC_NO" Type="string" />
                    <asp:Parameter Name="REFER_CODE" Type="string" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="TERMINAL_REC_NO" Type="String" />
                    <asp:Parameter Name="FIELD_DISPLAY_NAME" Type="String" />
                    <asp:Parameter Name="CONTENT_DESC" Type="String" />
                    <asp:Parameter Name="REFER_CODE" Type="String" />
                </UpdateParameters>
            </asp:SqlDataSource>
        </div>
    </div>

    <asp:HyperLink ID="HyperLink1" runat="server"
        NavigateUrl="~/Group/EditTerminalstis.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>


<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

    <script language="javascript">
        var globalcontrol = '';

        function ConfirmAction(control) {

            globalcontrol = control;

            $.msgAlert({
                type: "warning"
                , title: "Mensaje del sistema"
                , text: "Realmente desea eliminar la terminal?"
                , callback: function () {
                    __doPostBack($(globalcontrol).attr('name'), '');
                }
            });

            return false;
        }
    </script>

</asp:Content>

