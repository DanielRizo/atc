﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="EditAndroidGroup.aspx.vb" Inherits="Group_EditAndroidGroup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Editar Grupo Android
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

    <script type="text/javascript" src="../js/jquery.ui.slider.js"></script>

    <script type="text/javascript" src="../js/jquery.ui.datepicker.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkListGroups" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/ListAndroidGroups.aspx">Listar Grupos Android</asp:LinkButton>
    </li>
    <li>Editar Grupo</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    Editar Grupo
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlError" runat="server" Visible="False">
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <div class="titleh">
            <h3>Datos del Grupo</h3>
        </div>
        <div class="body">

            <div class="st-form-line">	
                <span class="st-labeltext">Grupo:</span>	
                <asp:TextBox ID="txtGroupName" CssClass="st-success-input" style="width:510px" 
                    runat="server" TabIndex="0" MaxLength="100" 
                    ToolTip="Nombre del Grupo." Enabled="False" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>

            <div class="toggle-message" style="z-index: 590;">
                <h3 class="title">Acciones...
                    <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
                <div class="hide-message" id="hide-message" style="display: none;">
        
                    <div class="st-form-line">	
                        <span class="st-labeltext">Cambiar Clave Dispositivos: </span>
                        
                        <asp:Literal ID="ltrChangePassword" runat="server"></asp:Literal>
                
                        <asp:HiddenField ID="changePasswordFlag" runat="server" Value="0" />

                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">	
                        <span class="st-labeltext">Clave: </span>
                        <asp:TextBox ID="txtLockPassword" CssClass="st-forminput" Style="width: 510px"
                            runat="server" TabIndex="2" MaxLength="15"
                            ToolTip="Digite clave para bloqueo de pantalla." onkeydown="return (event.keyCode!=13);" disabled="disabled" placeholder="Cambiar Clave a todos los Dispositivos..."></asp:TextBox> 
                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">	
                        <span class="st-labeltext">Mensaje: </span>
                        <asp:TextBox ID="txtMessageToDisplay" CssClass="st-forminput" Style="width: 510px"
                            runat="server" TabIndex="3" MaxLength="50"
                            ToolTip="Digite mensaje a desplegar en pantalla." onkeydown="return (event.keyCode!=13);" placeholder="Enviar Mensaje a todos los Dispositivos..."></asp:TextBox> 
                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">	
                        <span class="st-labeltext">Mensaje Actual: </span>
                        <asp:TextBox ID="txtActualMessage" CssClass="st-success-input" Style="width: 510px"
                            runat="server" ToolTip="Mensaje configurado en la plataforma" MaxLength="50" Enabled="False"
                              ></asp:TextBox> 
                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">	
                        <span class="st-labeltext">Bloquear Dispositivos: </span>
                        
                        <asp:Literal ID="ltrLockTerminals" runat="server"></asp:Literal>
                
                        <asp:HiddenField ID="lockTerminalsFlag" runat="server" Value="0" />

                        <div class="clear"></div>
                    </div>

                    <div class="button-box">
                        <asp:Button ID="btnSetActions" runat="server" Text="Ejecutar" OnClientClick="return validateExecuteActions();"
                            CssClass="button-aqua" TabIndex="4" />
                    </div>            
                </div>
            </div>

            <div class="toggle-message" style="z-index: 590;">
                <h3 class="title">Frecuencia de Consultas...
                    <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
                <div class="hide-message" id="hide-message2" style="display: none;">
        
                    <div class="st-form-line">	
                        <span class="st-labeltext">Nro. de Consultas: </span>
                        <asp:TextBox ID="txtQueriesNumber" CssClass="st-forminput" Style="width: 50px"
                            runat="server" TabIndex="5" MaxLength="2"
                            ToolTip="Digite número de consultas a realizar en el dispositivo" onkeydown="return (event.keyCode!=13);"></asp:TextBox> 
                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">	
                        <span class="st-labeltext">Frecuencia en Horas: </span>
                        <asp:TextBox ID="txtFrequency" CssClass="st-forminput" Style="width: 50px"
                            runat="server" TabIndex="6" MaxLength="2"
                            ToolTip="Digite frecuencia en horas a realizar consultas en el dispositivo." onkeydown="return (event.keyCode!=13);"></asp:TextBox> 
                        <div class="clear"></div>
                    </div>

                    <div class="button-box">
                        <asp:Button ID="btnSetValues" runat="server" Text="Grabar" 
                            CssClass="button-aqua" TabIndex="7" OnClientClick="return validateSetValues();" />
                    </div>            
                </div>
            </div>

            <div class="toggle-message" style="z-index: 590;">
                <h3 class="title">Aplicaciones Permitidas...
                    <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
                <div class="hide-message" id="hide-message3" style="display: none;">
        
                    <div class="st-form-line">	
                        <span class="st-labeltext">Aplicaciones Permitidas: </span>
                        <asp:TextBox ID="txtAllowedApps" CssClass="st-forminput" Style="width: 510px; height: 100px;"
                            runat="server" TabIndex="8" MaxLength="500" TextMode="MultiLine"
                            ToolTip="Digite aplicaciones permitidas para el grupo." onkeydown="return (event.keyCode!=13);"></asp:TextBox>

                        <br />

                        <blockquote>Para efectos de mantenimiento sobre TODOS los dispositivos, marcar en Aplicaciones Permitidas la palabra "all".</blockquote>

                    </div>

                    <div class="button-box">
                        <asp:Button ID="btnSetAllowedApps" runat="server" Text="Enviar Aplicaciones" 
                            CssClass="button-aqua" TabIndex="9" OnClientClick="return validateSetAllowedApps();" />
                    </div> 


                    <div class="st-form-line">	
                        <span class="st-labeltext">Aplicacion modo Kiosko </span>
                        <asp:TextBox ID="txtKioskoApp" CssClass="st-forminput" Style="width: 510px; height: 100px;"
                            runat="server" TabIndex="8" MaxLength="500" TextMode="MultiLine"
                            ToolTip="Digite aplicacion modo kiosko para el dispositivo." onkeydown="return (event.keyCode!=13);"></asp:TextBox>

                        <br />

                        <blockquote>Se debe colocar el package de la aplicacion que va a trabajar en modo kiosko</blockquote>

                    </div>

                    <div class="button-box">
                        <asp:Button ID="btnSetKioskoApp" runat="server" Text="Enviar Aplicacion modo Kiosko" 
                            CssClass="button-aqua" TabIndex="9" OnClientClick="return validateSetAllowedApps();" />
                    </div> 

             
                </div>
            </div>

        </div>
    </div>

    <asp:HyperLink ID="lnkBack" runat="server" NavigateUrl="~/Group/ListAndroidGroups.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <!-- Validator -->
    <script src="../js/ValidatorAndroidGroup.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

</asp:Content>

