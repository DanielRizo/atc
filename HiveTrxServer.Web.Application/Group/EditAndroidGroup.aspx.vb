﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users
Imports System.Net
Imports System.Globalization
Imports TeleLoader.Groups

Partial Class Group_EditAndroidGroup
    Inherits TeleLoader.Web.BasePage

    Dim groupObj As Group

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            groupObj = New Group(strConnectionString, objSessionParams.intSelectedGroup)
            'Leer datos BD
            groupObj.getGroupDataAndroid()

            txtGroupName.Text = groupObj.Name

            txtAllowedApps.Text = groupObj.AllowedApplications
            txtQueriesNumber.Text = groupObj.QueriesNumber
            txtFrequency.Text = groupObj.Frequency

            txtKioskoApp.Text = groupObj.KioskoApplication

            txtActualMessage.Text = groupObj.ActualMessage

            'Set Toggle Visible
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); $('#hide-message2').css('z-index', 750); $('#hide-message2').css('display', 'block'); $('#hide-message3').css('z-index', 750); $('#hide-message3').css('display', 'block'); });</script>"

            setSwitchChangePassswordData(0)
            setSwitchLockTerminalsData(0)

        Else
            Dim CtrlID As String = String.Empty

            If Request.Form("__EVENTTARGET") IsNot Nothing And
               Request.Form("__EVENTTARGET") <> String.Empty Then
                CtrlID = Request.Form("__EVENTTARGET")
            End If

            setSwitchChangePassswordData(changePasswordFlag.Value)
            setSwitchLockTerminalsData(lockTerminalsFlag.Value)

        End If

    End Sub

    Private Sub setSwitchChangePassswordData(flagChangePassword As Integer)
        Dim htmlSwitch As String

        If flagChangePassword = 1 Then
            htmlSwitch = "<p class='field switch' id='changePassword'><label for='radio1' id='radio11' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio2' id='radio12' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='changePassword'><label for='radio1' id='radio11' class='cb-enable'><span>S&iacute;</span></label><label for='radio2' id='radio12' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrChangePassword.Text = htmlSwitch

    End Sub

    Private Sub setSwitchLockTerminalsData(flagLockTerminals As Integer)
        Dim htmlSwitch As String

        If flagLockTerminals = 1 Then
            htmlSwitch = "<p class='field switch' id='lockTerminals'><label for='radio3' id='radio13' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio4' id='radio14' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='lockTerminals'><label for='radio3' id='radio13' class='cb-enable'><span>S&iacute;</span></label><label for='radio4' id='radio14' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrLockTerminals.Text = htmlSwitch

    End Sub

    Protected Sub btnSetValues_Click(sender As Object, e As EventArgs) Handles btnSetValues.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Update")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        groupObj = New Group(strConnectionString, objSessionParams.intSelectedGroup)

        'Establecer Datos de Grupo
        groupObj.QueriesNumber = txtQueriesNumber.Text
        groupObj.Frequency = txtFrequency.Text

        'Save Data
        If groupObj.SetQueryParameters() Then
            'New Parameters per Group Edited OK
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Frecuencias de Consulta Actualizadas. Datos[ " & getFormDataLog() & " ]", "")

            pnlMsg.Visible = True
            pnlError.Visible = False
            lblMsg.Text = "Frecuencias de Consulta actualizadas correctamente."
        Else
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Editando Frecuencias de Consulta. Datos[ " & getFormDataLog() & " ]", "")
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error Editando Frecuencias de Consulta. Por favor valide los datos."
        End If

    End Sub

    Protected Sub btnSetAllowedApps_Click(sender As Object, e As EventArgs) Handles btnSetAllowedApps.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Update")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        groupObj = New Group(strConnectionString, objSessionParams.intSelectedGroup)

        'Establecer Datos de Grupo
        groupObj.AllowedApplications = txtAllowedApps.Text

        'Save Data
        If groupObj.SetAllowedApps() Then
            'Allowed Apps per Group Edited OK
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Aplicaciones permitidas Actualizadas. Datos[ " & getFormDataLog() & " ]", "")

            pnlMsg.Visible = True
            pnlError.Visible = False
            lblMsg.Text = "Aplicaciones permitidas actualizadas correctamente."
        Else
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Editando Aplicaciones permitidas. Datos[ " & getFormDataLog() & " ]", "")
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error Editando Aplicaciones permitidas. Por favor valide los datos."
        End If

    End Sub



    Protected Sub btnSetActions_Click(sender As Object, e As EventArgs) Handles btnSetActions.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Update")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        groupObj = New Group(strConnectionString, objSessionParams.intSelectedGroup)

        'Establecer Datos de Grupo
        groupObj.ChangePasswordTerminals = changePasswordFlag.Value
        groupObj.NewPasswordTerminals = txtLockPassword.Text
        groupObj.NewMessageTerminals = txtMessageToDisplay.Text
        groupObj.ActualMessage = groupObj.NewMessageTerminals
        groupObj.LockTerminals = lockTerminalsFlag.Value

        'Save Data
        If groupObj.ExecuteActionsOnTerminals() Then
            'Actions Executed OK
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Acciones a Ejecutar Actualizadas. Datos[ " & getFormDataLog() & " ]", "")

            pnlMsg.Visible = True
            pnlError.Visible = False
            lblMsg.Text = "Acciones Ejecutadas correctamente."

            'Clear Form
            txtLockPassword.Text = ""
            txtMessageToDisplay.Text = ""
            txtActualMessage.Text = groupObj.ActualMessage
            setSwitchChangePassswordData(0)
            setSwitchLockTerminalsData(0)

        Else
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error ejecutando acciones. Datos[ " & getFormDataLog() & " ]", "")
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error Ejecutando acciones. Por favor valide los datos."
        End If

    End Sub

    Private Sub btnSetKioskoApp_Click(sender As Object, e As EventArgs) Handles btnSetKioskoApp.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Update")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        groupObj = New Group(strConnectionString, objSessionParams.intSelectedGroup)

        'Establecer Datos de Grupo
        groupObj.KioskoApplication = txtKioskoApp.Text

        'Save Data
        If groupObj.SetKioskoApp() Then
            'Allowed Apps per Group Edited OK
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Aplicacion kiosko Actualizada. Datos[ " & getFormDataLog() & " ]", "")

            pnlMsg.Visible = True
            pnlError.Visible = False
            lblMsg.Text = "Aplicacion kiosko actualizada correctamente."
        Else
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Editando Aplicacione kiosko. Datos[ " & getFormDataLog() & " ]", "")
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error Editando Aplicacion kiosko. Por favor valide los datos."
        End If

    End Sub

End Class
