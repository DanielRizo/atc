﻿
<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="EditConfiguracionCodigo.aspx.vb" Inherits="Security_WebModules" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Edicion de configuración de parametros de codigos de barras
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   

	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script>     
        
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed" 
            PostBackUrl="~/Security/Manager.aspx">Seguridad</asp:LinkButton>
    </li>
    <li>Edicion de configuración de parametros de codigos de barras</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Edicion de configuración de parametros de codigos de barras
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite editar la configuración parametros de codigos de barras:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>   
            
        <asp:Panel ID="pnlError" runat="server" Visible="False">    
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>  

        <div class="toggle-message" style="z-index: 590;">
            <h3 class="title"> Configuración...
                <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
            <div class="hide-message" id="hide-message" style="display: none;">
                
                <div class="st-form-line">	
                    <span class="st-labeltext">Codigo de Boton:</span>	
                    <asp:TextBox ID="codBoton" CssClass="st-forminput" style="width:510px" 
                        runat="server" TabIndex="1" MaxLength="50" 
                        ToolTip="Codigo de Boton." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>   

                <div class="st-form-line">	
                    <span class="st-labeltext">Proccess Code:</span>	
                    <asp:TextBox ID="proccesCode" CssClass="st-forminput" style="width:510px" 
                        runat="server" TabIndex="1" MaxLength="50" 
                        ToolTip="Proccess Code." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>
                
                <div class="st-form-line">	
                    <span class="st-labeltext">Nombre Recaudacion:</span>	
                    <asp:TextBox ID="nombreR" CssClass="st-forminput" style="width:510px" 
                        runat="server" TabIndex="1" MaxLength="50" 
                        ToolTip="NombreRecaudacion." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div> 

                <div class="st-form-line">	
                    <span class="st-labeltext">Nombre:</span>	
                    <asp:TextBox ID="nombre" CssClass="st-forminput" style="width:510px" 
                        runat="server" TabIndex="1" MaxLength="50" 
                        ToolTip="Nombre." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div> 
                
                <div class="st-form-line">	
                    <span class="st-labeltext">Posicion Inicial:</span>	
                    <asp:TextBox ID="posicionInicial" CssClass="st-forminput" style="width:510px" 
                        runat="server" TabIndex="1" MaxLength="5" 
                        ToolTip="PosicionInicial." onkeydown = "return (event.keyCode!=13);" onkeypress="javascript:return solonumeros(event,1)"></asp:TextBox>
                    <div class="clear"></div>
                </div> 

                <div class="st-form-line">	
                    <span class="st-labeltext">Posicion Final:</span>	
                    <asp:TextBox ID="posicionFinal" CssClass="st-forminput" style="width:510px" 
                        runat="server" TabIndex="1" MaxLength="5" 
                        ToolTip="PosicionFinal." onkeydown = "return (event.keyCode!=13);" onkeypress="javascript:return solonumeros(event,2)"></asp:TextBox>
                    <div class="clear"></div>
                </div> 

                <div class="st-form-line">	
                    <span class="st-labeltext">Prefijo:</span>	
                    <asp:TextBox ID="prefijo" CssClass="st-forminput" style="width:510px" 
                        runat="server" TabIndex="1" MaxLength="50" 
                        ToolTip="Prefijo." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div> 

                <div class="button-box">
                    <asp:Button ID="btnAddWebModule" runat="server" Text="Editar Parametros de Codigo de Barras" 
                        CssClass="button-aqua" TabIndex="4" OnClientClick="return validateAddWebModule();" />
                    <asp:Button ID="btnFinalize" runat="server" Text="Finalizar" 
                        CssClass="st-button" TabIndex="5" Visible="false"  />
                </div> 
              
            </div>

            <asp:SqlDataSource ID="dsWebModules" runat="server" 
                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"                 
                 SelectCommand="SELECT O.opc_id, O.opc_operador, O.opc_ip_uno, O.opc_puerto_uno, O.opc_ip_dos, O.opc_puerto_dos, O.opc_descripcion FROM configuracion_tcp O WHERE 0.opc_id = @opc_id ORDER BY opc_id asc" 
                 InsertCommand="sp_webCrearConfiguracion" InsertCommandType="StoredProcedure"
                 DeleteCommand="DELETE FROM configuracion_tcp WHERE opc_id = @opc_id">
                <InsertParameters>
                    <asp:Parameter Name="optionId" Type="String" />
                    <asp:Parameter Name="optionTitle" Type="String" />
                    <asp:Parameter Name="optionIpUno" Type="String" />
                    <asp:Parameter Name="optionPuertoUno" Type="String" />
                    <asp:Parameter Name="optionIpDos" Type="String" />
                    <asp:Parameter Name="optionPuertoDos" Type="String" />
                    <asp:Parameter Name="optionDescripcion" Type="String" />
                </InsertParameters>
                </asp:SqlDataSource>
        </div>

    </div>



    <!-- START SIMPLE FORM -->
    
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Group/ParametrosCodigoBarras.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">
    
    <!-- Validator -->
    <script src="../js/ValidatorSecurity.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
     <script type="text/javascript">
        function solonumeros(e,dato) {
 
            var key;
            var texto;
 
            if (window.event) // IE
            {
                key = e.keyCode;
            }
            else if (e.which) // Netscape/Firefox/Opera
            {
                key = e.which;
            }

            if (key == 48) {
                if (dato == 1) {
                    texto = document.getElementById("<%=posicionInicial.ClientID%>").value;
                    if (texto=="") {
                        console.log("no puede ingresar 0 como parametro inicial");
                        return false;
                    } else {
                        return true;
                    }
                }
                if (dato == 2) {
                    texto = document.getElementById("<%=posicionFinal.ClientID%>").value;
                    if (texto=="") {
                        console.log("no puede ingresar 0 como parametro final");
                        return false;
                    } else {
                        return true;
                    }
                }
            }
 
            if (key < 49 || key > 57) {
                return false;
            }
 
            return true;
        }
 
    </script>
</asp:Content>

