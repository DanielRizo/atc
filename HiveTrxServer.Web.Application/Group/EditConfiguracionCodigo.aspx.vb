﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class Security_WebModules
    Inherits TeleLoader.Web.BasePage

    Public ReadOnly IMG_PATH As String = "img/icons/sidemenu/"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False


        If Not IsPostBack Then
            editarConfiguracionv()
            btnAddWebModule.Enabled = True
            codBoton.Enabled = False
            proccesCode.Enabled = False
            nombreR.Enabled = False
            nombre.Enabled = False
        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub
    Private Sub editarConfiguracionv()
        Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString
        Dim db As SqlConnection = New SqlConnection(conexion)
        Dim cmd As SqlCommand
        Dim sqlBuilder As StringBuilder = New StringBuilder
        Dim codigo = objSessionParams.codigoConfiguracion

        sqlBuilder.Append("SELECT O.opc_id, O.opc_cod_boton,O.opc_proccess_code,O.opc_nombre_recaudo,O.opc_nombre, O.opc_posicion_inicial, O.opc_posicion_final, O.opc_prefijo FROM codigo_barras O  where O.opc_id = '" + codigo + "'")

        Try
            db.Open()
        Catch ex As Exception
            db.Close()
        End Try
        Try
            cmd = New SqlCommand()
            cmd.Connection = db
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sqlBuilder.ToString

            Dim dr As SqlDataReader = cmd.ExecuteReader()
            dr.Read()
            codBoton.Text = Convert.ToString(Trim(dr.GetString(1)))
            proccesCode.Text = Convert.ToString(Trim(dr.GetString(2)))
            nombreR.Text = Convert.ToString(Trim(dr.GetString(3)))
            nombre.Text = Convert.ToString(Trim(dr.GetString(4)))
            posicionInicial.Text = Convert.ToString(Trim(dr.GetString(5)))
            posicionFinal.Text = Convert.ToString(Trim(dr.GetString(6)))
            prefijo.Text = Convert.ToString(Trim(dr.GetString(7)))
        Catch ex As Exception
            db.Close()
        Finally
            cmd = Nothing
            db.Close()
        End Try

    End Sub
    Protected Sub btnAddWebModule_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddWebModule.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Update")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        Try
            Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
            Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString
            Dim db As SqlConnection = New SqlConnection(conexion)
            Dim cmd As SqlCommand
            Dim sqlBuilder As StringBuilder = New StringBuilder
            Dim codigo = objSessionParams.codigoConfiguracion
            Dim posiI = posicionInicial.Text
            Dim posiF = posicionFinal.Text
            Dim prefij = prefijo.Text

            Dim res = " "

            sqlBuilder.Append("UPDATE  codigo_barras SET  opc_posicion_inicial= '" + posiI + "', opc_posicion_final= '" + posiF + "', opc_prefijo = '" + prefij + "'   where opc_id = '" + codigo + "'")

            Try
                db.Open()
            Catch ex As Exception
                db.Close()
            End Try
            Try
                cmd = New SqlCommand()
                cmd.Connection = db
                cmd.CommandType = CommandType.Text
                cmd.CommandText = sqlBuilder.ToString

                cmd.ExecuteNonQuery()

                Response.Redirect("ParametrosCodigoBarras.aspx", False)

            Catch ex As Exception
                db.Close()
            Finally
                cmd = Nothing
                db.Close()
            End Try
            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub
    Protected Sub dsWebModules_Deleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceCommandEventArgs) Handles dsWebModules.Deleting
        If pnlError.Visible = True Then
            e.Cancel = True
        End If
    End Sub
End Class
