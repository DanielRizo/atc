﻿
<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="EditConfiguracionTcp.aspx.vb" Inherits="Security_WebModules" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Edicion de configuración de TCP
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   

	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script>     
        
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed" 
            PostBackUrl="~/Security/Manager.aspx">Seguridad</asp:LinkButton>
    </li>
    <li>Edicion de configuración de TCP</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Edicion de configuración de TCP
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite editar la configuración ip's y puerto por operador:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>   
            
        <asp:Panel ID="pnlError" runat="server" Visible="False">    
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>  

        <div class="toggle-message" style="z-index: 590;">
            <h3 class="title"> Configuración...
                <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
            <div class="hide-message" id="hide-message" style="display: none;">
                
                <div class="st-form-line">	
                    <span class="st-labeltext">Nombre del Operador:</span>	
                    <asp:TextBox ID="txtModuleName" CssClass="st-forminput" style="width:510px" 
                        runat="server" TabIndex="1" MaxLength="50" 
                        ToolTip="Nombre del Operador." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>        
                
                 <div class="st-form-line">	
                    <span class="st-labeltext">Ip 1:</span>	
                    <asp:TextBox ID="IpUno" CssClass="st-forminput" style="width:510px" 
                        runat="server" TabIndex="1" MaxLength="30" 
                        ToolTip="Ip 1." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div> 

                <div class="st-form-line">	
                    <span class="st-labeltext">Puerto 1:</span>	
                    <asp:TextBox ID="PuertoUno" CssClass="st-forminput" style="width:510px" 
                        runat="server" TabIndex="1" MaxLength="30" 
                        ToolTip="Puerto 1." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div> 

                <div class="st-form-line">	
                    <span class="st-labeltext">Ip 2:</span>	
                    <asp:TextBox ID="IpDos" CssClass="st-forminput" style="width:510px" 
                        runat="server" TabIndex="1" MaxLength="30" 
                        ToolTip="Ip 2." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div> 

                <div class="st-form-line">	
                    <span class="st-labeltext">Puerto 2:</span>	
                    <asp:TextBox ID="PuertoDos" CssClass="st-forminput" style="width:510px" 
                        runat="server" TabIndex="1" MaxLength="30" 
                        ToolTip="Puerto 1." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div> 

                <div class="st-form-line">	
                    <span class="st-labeltext">Descripción:</span>	
                    <asp:TextBox ID="Descripcion" CssClass="st-forminput" style="width:510px" 
                        runat="server" TabIndex="1" MaxLength="50" 
                        ToolTip="Descripcion." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>

                
                <div class="button-box">
                    <asp:Button ID="btnAddWebModule" runat="server" Text="Editar Configuración TCP" 
                        CssClass="button-aqua" TabIndex="4"/>
                    <asp:Button ID="btnFinalize" runat="server" Text="Finalizar" 
                        CssClass="st-button" TabIndex="5" Visible="false"  />
                </div> 
              
            </div>

            <asp:SqlDataSource ID="dsWebModules" runat="server" 
                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"                 
                 SelectCommand="SELECT O.opc_id, O.opc_operador, O.opc_ip_uno, O.opc_puerto_uno, O.opc_ip_dos, O.opc_puerto_dos, O.opc_descripcion FROM configuracion_tcp O WHERE 0.opc_id = @opc_id ORDER BY opc_id asc" 
                 InsertCommand="sp_webCrearConfiguracion" InsertCommandType="StoredProcedure"
                 DeleteCommand="DELETE FROM configuracion_tcp WHERE opc_id = @opc_id">
                <InsertParameters>
                    <asp:Parameter Name="optionId" Type="String" />
                    <asp:Parameter Name="optionTitle" Type="String" />
                    <asp:Parameter Name="optionIpUno" Type="String" />
                    <asp:Parameter Name="optionPuertoUno" Type="String" />
                    <asp:Parameter Name="optionIpDos" Type="String" />
                    <asp:Parameter Name="optionPuertoDos" Type="String" />
                    <asp:Parameter Name="optionDescripcion" Type="String" />
                </InsertParameters>
                </asp:SqlDataSource>
        </div>

    </div>



    <!-- START SIMPLE FORM -->
    
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Group/ConfiguracionTCP.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">
    
    <!-- Validator -->
    <script src="../js/ValidatorSecurity.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

