﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class Security_WebModules
    Inherits TeleLoader.Web.BasePage

    Public ReadOnly IMG_PATH As String = "img/icons/sidemenu/"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False


        If Not IsPostBack Then

            txtModuleName.Focus()
            editarConfiguracionv()
            btnAddWebModule.Enabled = True
        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub
    Private Sub editarConfiguracionv()
        Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString
        Dim db As SqlConnection = New SqlConnection(conexion)
        Dim cmd As SqlCommand
        Dim sqlBuilder As StringBuilder = New StringBuilder
        Dim codigo = objSessionParams.codigoConfiguracion

        sqlBuilder.Append("SELECT O.opc_id, O.opc_operador, O.opc_ip_uno, O.opc_puerto_uno, O.opc_ip_dos, O.opc_puerto_dos, O.opc_descripcion FROM configuracion_tcp O where o.opc_id = '" + codigo + "'")

        Try
            db.Open()
        Catch ex As Exception
            db.Close()
        End Try
        Try
            cmd = New SqlCommand()
            cmd.Connection = db
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sqlBuilder.ToString

            Dim dr As SqlDataReader = cmd.ExecuteReader()
            dr.Read()


            txtModuleName.Text = Convert.ToString(Trim(dr.GetString(1)))
            IpUno.Text = Convert.ToString(Trim(dr.GetString(2)))
            PuertoUno.Text = Convert.ToString(Trim(dr.GetString(3)))
            IpDos.Text = Convert.ToString(Trim(dr.GetString(4)))
            PuertoDos.Text = Convert.ToString(Trim(dr.GetString(5)))
            Descripcion.Text = Convert.ToString(Trim(dr.GetString(6)))

        Catch ex As Exception
            db.Close()


        Finally
            cmd = Nothing
            db.Close()
        End Try

    End Sub
    Protected Sub btnAddWebModule_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddWebModule.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Update")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        Try
            Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
            Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString
            Dim db As SqlConnection = New SqlConnection(conexion)
            Dim cmd As SqlCommand
            Dim sqlBuilder As StringBuilder = New StringBuilder
            Dim codigo = objSessionParams.codigoConfiguracion
            Dim nombre = txtModuleName.Text
            Dim ip1 = IpUno.Text
            Dim puerto1 = PuertoUno.Text
            Dim ip2 = IpDos.Text
            Dim puerto2 = PuertoDos.Text
            Dim descripciond = Descripcion.Text
            Dim res = " "

            sqlBuilder.Append("UPDATE configuracion_tcp   set opc_operador = '" + nombre + "', opc_ip_uno = '" + ip1 + "', opc_puerto_uno = '" + puerto1 + "',opc_ip_dos = '" + ip2 + "', opc_puerto_dos = '" + puerto2 + "', opc_descripcion = '" + descripciond + "' where opc_id = '" + codigo + "'")

            Try
                db.Open()
            Catch ex As Exception
                db.Close()
            End Try
            Try
                cmd = New SqlCommand()
                cmd.Connection = db
                cmd.CommandType = CommandType.Text
                cmd.CommandText = sqlBuilder.ToString

                cmd.ExecuteNonQuery()

                Response.Redirect("ConfiguracionTCP.aspx", False)

            Catch ex As Exception
                db.Close()
            Finally
                cmd = Nothing
                db.Close()
            End Try
            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub


    Private Sub deactivateForm()
        txtModuleName.Enabled = False
        btnAddWebModule.Enabled = False
        btnFinalize.Visible = True
        btnFinalize.Enabled = True
    End Sub

    Private Sub activateForm()
        txtModuleName.Enabled = True
        txtModuleName.Text = ""
        btnAddWebModule.Enabled = True
        btnFinalize.Visible = False
        btnFinalize.Enabled = False
    End Sub
    Protected Sub dsWebModules_Deleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceCommandEventArgs) Handles dsWebModules.Deleting
        If pnlError.Visible = True Then
            e.Cancel = True
        End If
    End Sub
End Class
