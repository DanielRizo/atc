﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="EditGroup.aspx.vb" Inherits="Group_EditGroup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Editar Grupo
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

    <script type="text/javascript" src="../js/jquery.ui.slider.js"></script>

    <script type="text/javascript" src="../js/jquery.ui.datepicker.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkListGroups" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/ListGroups.aspx">Listar Grupos</asp:LinkButton>
    </li>
    <li>Editar Grupo</li>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    EDITAR GRUPO
     <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Group/ListGroups.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>
</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlError" runat="server" Visible="False">
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <div class="titleh">
            <h3>Datos del grupo</h3>
        </div>
        <div class="body">

            <div class="st-form-line">	
                <span class="st-labeltext">Cliente:</span>	
                <asp:TextBox ID="txtCustomerName" CssClass="st-success-input" style="width:510px" 
                    runat="server" TabIndex="0" MaxLength="100" 
                    ToolTip="Nombre del cliente." Enabled="False" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>

            <div class="st-form-line">
                <span class="st-labeltext">Nombre grupo: (*)</span>
                <asp:TextBox ID="txtGroupName" CssClass="st-forminput" Style="width: 510px"
                    runat="server" TabIndex="1" MaxLength="100"
                    ToolTip="Digite nombre del grupo." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>
            <div class="st-form-line">
                <span class="st-labeltext">IP de descarga: (*)</span>
                <asp:TextBox ID="txtIP" CssClass="st-forminput" Style="width: 100px"
                    runat="server" TabIndex="2" MaxLength="15"
                    ToolTip="Digite IP para descarga remota." onkeydown="return (event.keyCode!=13);"></asp:TextBox> 
                <div class="clear"></div>
            </div>

            <div class="st-form-line">
                <span class="st-labeltext">Puerto de descarga: (*)</span>
                <asp:TextBox ID="txtPort" CssClass="st-forminput" Style="width: 60px"
                    runat="server" TabIndex="3" MaxLength="5"
                    ToolTip="Digite Puerto TCP para descarga remota." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>

            <div class="st-form-line">	

                <span class="st-labeltext">Modo bloqueante?:</span>	                

                <asp:Literal ID="ltrBlockingMode" runat="server"></asp:Literal>
                
                <asp:HiddenField ID="groupFlagBlockingMode" runat="server" Value="0" />

                <div class="clear"></div>
            </div>

            <div class="st-form-line">	

                <span class="st-labeltext">Actualizacion inmediata?:</span>	                

                <asp:Literal ID="ltrUpdateNow" runat="server"></asp:Literal>
                
                <asp:HiddenField ID="groupFlagUpdateNow" runat="server" Value="0" />

                <div class="clear"></div>
            </div>

            <div class="st-form-line">
                <span class="st-labeltext">Descripci&oacute;n:</span>
                <asp:TextBox ID="txtDesc" CssClass="st-forminput" Style="width: 510px"
                    runat="server" TabIndex="4" MaxLength="250" TextMode="MultiLine"
                    ToolTip="Digite descripción del grupo." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>



            <div class="st-form-line">	
                <span class="st-labeltext">Actualizar terminales?:</span>	                

                <asp:Literal ID="ltrUpdate" runat="server"></asp:Literal>
                
                <asp:HiddenField ID="groupFlagUpdate" runat="server" Value="1" />

                <div class="clear"></div>
            </div>

            <div class="st-form-line">	
                <span class="st-labeltext">Usar rango de fechas?:</span>	                

                <asp:Literal ID="ltrDateRange" runat="server"></asp:Literal>
                
                <asp:HiddenField ID="groupFlagDateRange" runat="server" Value="0" />

                <div class="clear">
                </div>
            </div>

            <asp:Panel ID="pnlDateRange" runat="server" Visible="False">
                <div class="st-form-line" style="text-align: center;">
                    <span>Fecha Inicial: (*)&nbsp;&nbsp;</span>
                    <asp:TextBox ID="txtStartDate" CssClass="datepicker-input"
                        onkeydown="return false;" runat="server" Font-Bold="True" Width="100px"
                        ToolTip="Fecha Inicial" TabIndex="5"></asp:TextBox>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            	    <span>Fecha Final: (*)&nbsp;&nbsp;</span>
                    <asp:TextBox ID="txtEndDate" CssClass="datepicker-input"
                        onkeydown="return false;" runat="server" Font-Bold="True" Width="100px"
                        ToolTip="Fecha Inicial" TabIndex="6"></asp:TextBox>
                    <div class="clear"></div>
                </div>
            </asp:Panel>

            <div class="st-form-line">	
                <span class="st-labeltext">Rango de horas: (*)</span>

                <asp:TextBox ID="txtRange1" CssClass="st-forminput" Style="width: 40px"
                    runat="server" TabIndex="7" MaxLength="5"
                    ToolTip="Rango Inicial de Hora" Text="00:00" Font-Bold="True" onkeydown="return false;"></asp:TextBox>
                -
                <asp:TextBox ID="txtRange2" CssClass="st-forminput" Style="width: 40px"
                    runat="server" TabIndex="8" MaxLength="5"
                    ToolTip="Rango Final de Hora" Text="23:45" Font-Bold="True" onkeydown="return false;"></asp:TextBox>

                <div id="slider-range" class="margin-top10 ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" style="z-index: 640; width: 68%; float:right;">
                    <div class="ui-slider-range ui-widget-header" style="left: 0%; width: 0%;"></div>
                    <a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 0%;"></a>
                    <a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 0%;"></a>
                </div>
                <div class="clear"></div>
            </div>
            <div class="st-form-line">	
                <span class="st-labeltext">Inicializar terminales?:</span>	                

                <asp:Literal ID="ltrInitialize" runat="server"></asp:Literal>
                
                <asp:HiddenField ID="groupFlagInitialize" runat="server" Value="1" />

                <div class="clear"></div>
            </div>
            <div class="st-form-line">	
                <span class="st-labeltext">Descargar llaves?:</span>	                

                <asp:Literal ID="ltrkey" runat="server"></asp:Literal>
                
                <asp:HiddenField ID="GroupKeyDownload" runat="server" Value="1" />

                <div class="clear"></div>
            </div>
                <div class="button-box">
                    <asp:Button ID="btnEdit" runat="server" Text="Editar Grupo"
                        CssClass="button-aqua" TabIndex="9" OnClientClick="return validateAddOrEditGroup()" />
                </div>
        </div>
    </div>
    <div class="body">

            <%--<div class="st-form-line">	
                <span class="st-labeltext">Grupo:</span>	
                <asp:TextBox ID="TextBox1" CssClass="st-success-input" style="width:510px" 
                    runat="server" TabIndex="0" MaxLength="100" 
                    ToolTip="Nombre del Grupo." Enabled="False" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>--%>

            <div class="toggle-message" style="z-index: 590;">
                <h3 class="title">Acciones...
                    <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
                <div class="hide-message" id="hide-message" style="display: none;">
        
                    <div class="st-form-line">	
                        <span class="st-labeltext">Cambiar clave dispositivos: </span>
                        
                        <asp:Literal ID="ltrChangePassword" runat="server"></asp:Literal>
                
                        <asp:HiddenField ID="changePasswordFlag" runat="server" Value="0" />

                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">	
                        <span class="st-labeltext">Clave: </span>
                        <asp:TextBox ID="txtLockPassword" CssClass="st-forminput" Style="width: 510px"
                            runat="server" TabIndex="2" MaxLength="15"
                            ToolTip="Digite clave para bloqueo de pantalla." onkeydown="return (event.keyCode!=13);" disabled="disabled" placeholder="Cambiar Clave a todos los Dispositivos..."></asp:TextBox> 
                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">	
                        <span class="st-labeltext">Mensaje: </span>
                        <asp:TextBox ID="txtMessageToDisplay" CssClass="st-forminput" Style="width: 510px"
                            runat="server" TabIndex="3" MaxLength="50"
                            ToolTip="Digite mensaje a desplegar en pantalla." onkeydown="return (event.keyCode!=13);" placeholder="Enviar Mensaje a todos los Dispositivos..."></asp:TextBox> 
                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">	
                        <span class="st-labeltext">Mensaje actual: </span>
                        <asp:TextBox ID="txtActualMessage" CssClass="st-success-input" Style="width: 510px"
                            runat="server" ToolTip="Mensaje configurado en la plataforma" MaxLength="50" Enabled="False"
                              ></asp:TextBox> 
                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">	
                        <span class="st-labeltext">Bloquear dispositivos: </span>
                        
                        <asp:Literal ID="ltrLockTerminals" runat="server"></asp:Literal>
                
                        <asp:HiddenField ID="lockTerminalsFlag" runat="server" Value="0" />

                        <div class="clear"></div>
                    </div>

                    <div class="button-box">
                        <asp:Button ID="btnSetActions" runat="server" Text="Ejecutar" OnClientClick="return validateExecuteActions();"
                            CssClass="button-aqua" TabIndex="4" />
                    </div>            
                </div>
            </div>

            <div class="toggle-message" style="z-index: 590;">
                <h3 class="title">Frecuencia de Consultas...
                    <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
                <div class="hide-message" id="hide-message2" style="display: none;">
        
                    <div class="st-form-line">	
                        <span class="st-labeltext">Nro. de consultas: </span>
                        <asp:TextBox ID="txtQueriesNumber" CssClass="st-forminput" Style="width: 50px"
                            runat="server" TabIndex="5" MaxLength="2"
                            ToolTip="Digite número de consultas a realizar en el dispositivo" onkeydown="return (event.keyCode!=13);"></asp:TextBox> 
                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">	
                        <span class="st-labeltext">Frecuencia en horas: </span>
                        <asp:TextBox ID="txtFrequency" CssClass="st-forminput" Style="width: 50px"
                            runat="server" TabIndex="6" MaxLength="2"
                            ToolTip="Digite frecuencia en horas a realizar consultas en el dispositivo." onkeydown="return (event.keyCode!=13);"></asp:TextBox> 
                        <div class="clear"></div>
                    </div>

                    <div class="button-box">
                        <asp:Button ID="btnSetValues" runat="server" Text="Grabar" 
                            CssClass="button-aqua" TabIndex="7" OnClientClick="return validateSetValues();" />
                    </div>            
                </div>
            </div>

            <div class="toggle-message" style="z-index: 590;">
                <h3 class="title">Aplicaciones Permitidas...
                    <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
                <div class="hide-message" id="hide-message3" style="display: none;">
        
                    <div class="st-form-line">	
                        <span class="st-labeltext">Aplicaciones permitidas: </span>
                        <asp:TextBox ID="txtAllowedApps" CssClass="st-forminput" Style="width: 510px; height: 100px;"
                            runat="server" TabIndex="8" MaxLength="500" TextMode="MultiLine"
                            ToolTip="Digite aplicaciones permitidas para el grupo." onkeydown="return (event.keyCode!=13);"></asp:TextBox>

                        <br />

                        <p >Para efectos de mantenimiento sobre TODOS los dispositivos, marcar en Aplicaciones permitidas la palabra "all".*</p>

                    </div>

                    <div class="button-box">
                        <asp:Button ID="btnSetAllowedApps" runat="server" Text="Enviar aplicaciones" 
                            CssClass="centrar" TabIndex="9" OnClientClick="return validateSetAllowedApps();" />
                    </div> 


                    <div class="st-form-line">	
                        <span class="st-labeltext">Aplicacion modo kiosko </span>
                        <asp:TextBox ID="txtKioskoApp" CssClass="st-forminput" Style="width: 510px; height: 100px;"
                            runat="server" TabIndex="8" MaxLength="500" TextMode="MultiLine"
                            ToolTip="Digite aplicacion modo kiosko para el dispositivo." onkeydown="return (event.keyCode!=13);"></asp:TextBox>

                        <br />

                        <p>Se debe colocar el package de la aplicacion que va a trabajar en modo kiosko.*</p>

                    </div>

                    <div class="button-box">
                        <asp:Button ID="btnSetKioskoApp" runat="server" Text="Enviar aplicacion modo kiosko" Style="align-items:center;"
                            CssClass="centrar-uno" TabIndex="9" OnClientClick="return validateSetAllowedApps();" />
                    </div> 

             
                </div>
            </div>

        </div>

    <asp:HyperLink ID="lnkBack" runat="server" NavigateUrl="~/Group/ListGroups.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <!-- Validator -->
    <script src="../js/ValidatorGroup.js" type="text/javascript"></script>
    
    <script src="../js/ValidatorAndroidGroup.js" type="text/javascript"></script>
    
    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

</asp:Content>

