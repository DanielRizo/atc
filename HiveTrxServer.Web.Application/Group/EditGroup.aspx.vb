﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users
Imports System.Net
Imports System.Globalization
Imports TeleLoader.Groups

Partial Class Group_EditGroup
    Inherits TeleLoader.Web.BasePage

    Dim groupObj As Group
    Dim groupObjAndroid As Group


    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try

        Catch ex As Exception

        End Try
        Dim startDate As Date
        Dim endDate As Date
        Dim rangeHour1 As Integer
        Dim rangeHour2 As Integer

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Update")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        startDate = Date.Parse(txtStartDate.Text & " " & txtRange1.Text, New CultureInfo("es-CO"))
        endDate = Date.Parse(txtEndDate.Text & " " & txtRange2.Text, New CultureInfo("es-CO"))

        'Validate Date
        If startDate <= endDate Then
            'Validate Hour Range
            rangeHour1 = Integer.Parse(txtRange1.Text.Trim.Replace(":", ""))
            rangeHour2 = Integer.Parse(txtRange2.Text.Trim.Replace(":", ""))

            If rangeHour1 <= rangeHour2 Then

                groupObj = New Group(strConnectionString, objSessionParams.intSelectedGroup)

                'Establecer Datos de Grupo
                groupObj.Name = txtGroupName.Text
                groupObj.Desc = txtDesc.Text
                groupObj.IpAddress = txtIP.Text
                groupObj.Port = txtPort.Text
                groupObj.allowUpdate = groupFlagUpdate.Value
                groupObj.useDateRange = groupFlagDateRange.Value
                groupObj.RangeDateIni = startDate
                groupObj.RangeDateEnd = endDate
                groupObj.RangeHourIni = startDate
                groupObj.RangeHourEnd = endDate
                groupObj.BlockingMode = groupFlagBlockingMode.Value
                groupObj.UpdateNow = groupFlagUpdateNow.Value
                groupObj.Initialize = groupFlagInitialize.Value
                groupObj.DownloadKey = GroupKeyDownload.Value

                'Save Data
                If groupObj.editGroup() Then
                    'New Group Edited OK
                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Grupo Editado. Datos[ " & getFormDataLog() & " ]", "")

                    pnlMsg.Visible = True
                    pnlError.Visible = False
                    lblMsg.Text = "Grupo editado correctamente."
                Else
                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Editando Grupo. Datos[ " & getFormDataLog() & " ]", "")
                    pnlError.Visible = True
                    pnlMsg.Visible = False
                    lblError.Text = "Error editando grupo. Por favor valide los datos."
                End If
            Else
                pnlError.Visible = True
                pnlMsg.Visible = False
                lblError.Text = "La hora inicial del rango debe ser menor o igual que la hora final."
            End If
        Else
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "La fecha inicial del rango debe ser menor o igual que la fecha final."
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then
            txtCustomerName.Text = objAccessToken.Customer

            groupObj = New Group(strConnectionString, objSessionParams.intSelectedGroup)
            'Leer datos BD
            groupObj.getGroupData()

            txtGroupName.Text = groupObj.Name
            'ddlCodeExterno.SelectedValue = groupObj.CodeExtern
            txtDesc.Text = groupObj.Desc
            txtIP.Text = groupObj.IpAddress
            txtPort.Text = groupObj.Port
            groupFlagBlockingMode.Value = IIf(groupObj.BlockingMode, 1, 0)
            setSwitchBlockingMode(IIf(groupObj.BlockingMode, 1, 0))
            groupFlagUpdateNow.Value = IIf(groupObj.UpdateNow, 1, 0)
            setSwitchUpdateNow(IIf(groupObj.UpdateNow, 1, 0))
            groupFlagUpdate.Value = IIf(groupObj.allowUpdate, 1, 0)
            setSwitchUpdateData(IIf(groupObj.allowUpdate, 1, 0))
            groupFlagDateRange.Value = IIf(groupObj.useDateRange, 1, 0)
            setSwitchDateRangeData(IIf(groupObj.useDateRange, 1, 0))

            groupFlagInitialize.Value = IIf(groupObj.Initialize, 1, 0)
            GroupKeyDownload.Value = IIf(groupObj.DownloadKey, 1, 0)

            If groupObj.Initialize = False Then
                setSwitchInitialize(0)
            Else
                setSwitchInitialize(1)
            End If

            If groupObj.DownloadKey = False Then
                setSwitchDownloadKey(0)
            Else
                setSwitchDownloadKey(1)
            End If

            'Validar si está configurado para usar rango de fechas
            If groupObj.useDateRange Then
                pnlDateRange.Visible = True
                txtStartDate.Text = groupObj.RangeDateIni.ToString("dd/MM/yyyy")
                txtEndDate.Text = groupObj.RangeDateEnd.ToString("dd/MM/yyyy")

                txtRange1.Text = groupObj.RangeDateIni.ToString("HH:mm")
                txtRange2.Text = groupObj.RangeDateEnd.ToString("HH:mm")
            Else
                txtStartDate.Text = Now.ToString("dd/MM/yyyy")
                txtEndDate.Text = Now.ToString("dd/MM/yyyy")

                txtRange1.Text = groupObj.RangeHourIni.ToString("HH:mm")
                txtRange2.Text = groupObj.RangeHourEnd.ToString("HH:mm")
            End If

            setSliderData()

            txtGroupName.Focus()

            'ANDROID
            groupObjAndroid = New Group(strConnectionString, objSessionParams.intSelectedGroup)
            'Leer datos BD
            groupObjAndroid.getGroupDataAndroid()

            txtGroupName.Text = groupObjAndroid.Name

            txtAllowedApps.Text = groupObjAndroid.AllowedApplications
            txtQueriesNumber.Text = groupObjAndroid.QueriesNumber
            txtFrequency.Text = groupObjAndroid.Frequency

            txtKioskoApp.Text = groupObjAndroid.KioskoApplication

            txtActualMessage.Text = groupObjAndroid.ActualMessage

            'Set Toggle Visible
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); $('#hide-message2').css('z-index', 750); $('#hide-message2').css('display', 'block'); $('#hide-message3').css('z-index', 750); $('#hide-message3').css('display', 'block'); });</script>"

            setSwitchChangePassswordData(0)
            setSwitchLockTerminalsData(0)


        Else
            Dim CtrlID As String = String.Empty

            If Request.Form("__EVENTTARGET") IsNot Nothing And
               Request.Form("__EVENTTARGET") <> String.Empty Then
                CtrlID = Request.Form("__EVENTTARGET")
            End If

            If CtrlID = "groupFlagDateRangeYES" Then
                pnlDateRange.Visible = True
            End If

            If CtrlID = "groupFlagDateRangeNO" Then
                pnlDateRange.Visible = False
            End If

            setSwitchUpdateData(groupFlagUpdate.Value)
            setSwitchDateRangeData(groupFlagDateRange.Value)
            setSwitchBlockingMode(groupFlagBlockingMode.Value)
            setSwitchUpdateNow(groupFlagUpdateNow.Value)
            setSwitchInitialize(groupFlagInitialize.Value)
            setSwitchDownloadKey(GroupKeyDownload.Value)

            'ANDROID
            'Dim CtrlID As String = String.Empty

            If Request.Form("__EVENTTARGET") IsNot Nothing And
               Request.Form("__EVENTTARGET") <> String.Empty Then
                CtrlID = Request.Form("__EVENTTARGET")
            End If

            setSwitchChangePassswordData(changePasswordFlag.Value)
            setSwitchLockTerminalsData(lockTerminalsFlag.Value)
        End If

        setSliderData()

    End Sub

    Private Sub setSwitchChangePassswordData(flagChangePassword As Integer)
        Dim htmlSwitch As String

        If flagChangePassword = 1 Then
            htmlSwitch = "<p class='field switch' id='changePassword'><label for='radio1' id='radio25' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio2' id='radio26' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='changePassword'><label for='radio1' id='radio25' class='cb-enable'><span>S&iacute;</span></label><label for='radio2' id='radio26' class='cb-disable selected'><span>No</span></label></p>"
        End If



        ltrChangePassword.Text = htmlSwitch

    End Sub
    'ANDROID
    Private Sub setSwitchLockTerminalsData(flagLockTerminals As Integer)
        Dim htmlSwitch As String

        If flagLockTerminals = 1 Then
            htmlSwitch = "<p class='field switch' id='changePassword'><label for='radio1' id='radio27' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio2' id='radio28' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='lockTerminals'><label for='radio1' id='radio27' class='cb-enable'><span>S&iacute;</span></label><label for='radio2' id='radio28' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrLockTerminals.Text = htmlSwitch

    End Sub
    'ANDROID
    Private Sub setSwitchUpdateData(flagUpdate As Integer)
        Dim htmlSwitch As String

        If flagUpdate = 1 Then
            htmlSwitch = "<p class='field switch' id='switchUpdate'><label for='radio1' id='radio11' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio2' id='radio12' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='switchUpdate'><label for='radio1' id='radio11' class='cb-enable'><span>S&iacute;</span></label><label for='radio2' id='radio12' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrUpdate.Text = htmlSwitch

    End Sub

    Protected Sub btnSetValues_Click(sender As Object, e As EventArgs) Handles btnSetValues.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Update")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        groupObjAndroid = New Group(strConnectionString, objSessionParams.intSelectedGroup)

        'Establecer Datos de Grupo
        groupObjAndroid.QueriesNumber = txtQueriesNumber.Text
        groupObjAndroid.Frequency = txtFrequency.Text

        'Save Data
        If groupObjAndroid.SetQueryParameters() Then
            'New Parameters per Group Edited OK
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Frecuencias de Consulta Actualizadas. Datos[ " & getFormDataLog() & " ]", "")

            pnlMsg.Visible = True
            pnlError.Visible = False
            lblMsg.Text = "Frecuencias de Consulta actualizadas correctamente."
        Else
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Editando Frecuencias de Consulta. Datos[ " & getFormDataLog() & " ]", "")
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error Editando Frecuencias de Consulta. Por favor valide los datos."
        End If

    End Sub

    Protected Sub btnSetAllowedApps_Click(sender As Object, e As EventArgs) Handles btnSetAllowedApps.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Update")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        groupObjAndroid = New Group(strConnectionString, objSessionParams.intSelectedGroup)

        'Establecer Datos de Grupo
        groupObjAndroid.AllowedApplications = txtAllowedApps.Text

        'Save Data
        If groupObjAndroid.SetAllowedAppsAndroid() Then
            'Allowed Apps per Group Edited OK
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Aplicaciones permitidas Actualizadas. Datos[ " & getFormDataLog() & " ]", "")

            pnlMsg.Visible = True
            pnlError.Visible = False
            lblMsg.Text = "Aplicaciones permitidas actualizadas correctamente."
        Else
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Editando Aplicaciones permitidas. Datos[ " & getFormDataLog() & " ]", "")
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error Editando Aplicaciones permitidas. Por favor valide los datos."
        End If

    End Sub



    Protected Sub btnSetActions_Click(sender As Object, e As EventArgs) Handles btnSetActions.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Update")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        groupObjAndroid = New Group(strConnectionString, objSessionParams.intSelectedGroup)

        'Establecer Datos de Grupo
        groupObjAndroid.ChangePasswordTerminals = changePasswordFlag.Value
        groupObjAndroid.NewPasswordTerminals = txtLockPassword.Text
        groupObjAndroid.NewMessageTerminals = txtMessageToDisplay.Text
        groupObjAndroid.ActualMessage = groupObjAndroid.NewMessageTerminals
        groupObjAndroid.LockTerminals = lockTerminalsFlag.Value

        'Save Data
        If groupObjAndroid.ExecuteActionsOnTerminals() Then
            'Actions Executed OK
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Acciones a Ejecutar Actualizadas. Datos[ " & getFormDataLog() & " ]", "")

            pnlMsg.Visible = True
            pnlError.Visible = False
            lblMsg.Text = "Acciones Ejecutadas correctamente."

            'Clear Form
            txtLockPassword.Text = ""
            txtMessageToDisplay.Text = ""
            txtActualMessage.Text = groupObjAndroid.ActualMessage
            setSwitchChangePassswordData(0)
            setSwitchLockTerminalsData(0)

        Else
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error ejecutando acciones. Datos[ " & getFormDataLog() & " ]", "")
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error Ejecutando acciones. Por favor valide los datos."
        End If

    End Sub

    Private Sub btnSetKioskoApp_Click(sender As Object, e As EventArgs) Handles btnSetKioskoApp.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Update")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        groupObjAndroid = New Group(strConnectionString, objSessionParams.intSelectedGroup)

        'Establecer Datos de Grupo
        groupObjAndroid.KioskoApplication = txtKioskoApp.Text

        'Save Data
        If groupObjAndroid.SetKioskoAplicacion() Then
            'Allowed Apps per Group Edited OK
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Aplicacion kiosko Actualizada. Datos[ " & getFormDataLog() & " ]", "")

            pnlMsg.Visible = True
            pnlError.Visible = False
            lblMsg.Text = "Aplicacion kiosko actualizada correctamente."
        Else
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Editando Aplicacione kiosko. Datos[ " & getFormDataLog() & " ]", "")
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error Editando Aplicacion kiosko. Por favor valide los datos."
        End If

    End Sub



    Private Sub setSwitchDateRangeData(flagDateRange As Integer)
        Dim htmlSwitch As String

        If flagDateRange = 1 Then
            htmlSwitch = "<p class='field switch' id='switchDateRange'><label for='radio3' id='radio13' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio4' id='radio14' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='switchDateRange'><label for='radio3' id='radio13' class='cb-enable'><span>S&iacute;</span></label><label for='radio4' id='radio14' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrDateRange.Text = htmlSwitch

    End Sub

    Private Sub setSwitchBlockingMode(flagBlockingMode As Boolean)
        Dim htmlSwitch As String

        If flagBlockingMode = True Then
            htmlSwitch = "<p class='field switch' id='switchBlockingMode'><label for='radio11' id='radio21' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio12' id='radio22' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='switchBlockingMode'><label for='radio11' id='radio21' class='cb-enable'><span>S&iacute;</span></label><label for='radio12' id='radio22' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrBlockingMode.Text = htmlSwitch

    End Sub



    Private Sub setSwitchUpdateNow(flagUpdateNow As Boolean)
        Dim htmlSwitch As String

        If flagUpdateNow = True Then
            htmlSwitch = "<p class='field switch' id='switchUpdateNow'><label for='radio11' id='radio23' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio12' id='radio24' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='switchUpdateNow'><label for='radio11' id='radio23' class='cb-enable'><span>S&iacute;</span></label><label for='radio12' id='radio24' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrUpdateNow.Text = htmlSwitch

    End Sub
    Private Sub setSwitchInitialize(flagInitialize As Boolean)
        Dim htmlSwitch As String



        If flagInitialize = True Then
            htmlSwitch = "<p class='field switch' id='switchUpdate'><label for='radio1' id='radio29' class='cb-enable selected'><span>Activar</span></label><label for='radio2' id='radio30' class='cb-disable'><span>Desactivar</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='switchUpdate'><label for='radio1' id='radio29' class='cb-enable'><span>Activar</span></label><label for='radio2' id='radio30' class='cb-disable selected'><span>Desactivar</span></label></p>"
        End If



        ltrInitialize.Text = htmlSwitch

    End Sub
    Private Sub setSwitchDownloadKey(flagDownloadKey As Boolean)
        Dim htmlSwitch As String



        If flagDownloadKey = True Then
            htmlSwitch = "<p class='field switch' id='switchUpdate'><label for='radio52' id='radio55' class='cb-enable selected'><span>Activar</span></label><label for='radio2' id='radio31' class='cb-disable'><span>Desactivar</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='switchUpdate'><label for='radio52' id='radio55' class='cb-enable'><span>Activar</span></label><label for='radio2' id='radio31' class='cb-disable selected'><span>Desactivar</span></label></p>"
        End If



        ltrkey.Text = htmlSwitch

    End Sub
    Private Sub setSliderData()
        Dim htmlSlider As String = ""
        Dim hour1, hour2 As Integer
        Dim minute1, minute2 As Integer
        Dim val1, val2, val3, val4 As Integer
        Dim interval As Integer = 15

        hour1 = Integer.Parse(txtRange1.Text.Trim.Split(":")(0))
        minute1 = Integer.Parse(txtRange1.Text.Trim.Split(":")(1))
        hour2 = Integer.Parse(txtRange2.Text.Trim.Split(":")(0))
        minute2 = Integer.Parse(txtRange2.Text.Trim.Split(":")(1))

        val1 = (hour1 * 60) / interval
        val2 = (minute1 Mod 60) / interval

        val3 = (hour2 * 60) / interval
        val4 = (minute2 Mod 60) / interval

        htmlSlider = "<script language='javascript'>$(function() { $('#slider-range').slider({ range: true, min: 0, max: 95, values: [" & (val1 + val2) & "," & (val3 + val4) & "], slide: function (event, ui) { var interval = 15; var hour1 = (ui.values[0] * interval) / 60;	var minute1 = (ui.values[0] * interval) % 60; var hour2 = (ui.values[1] * interval) / 60; var minute2 = (ui.values[1] * interval) % 60; $('#ctl00_MainContent_txtRange1').val(zeroPad(parseInt(hour1), 2) + ':' + zeroPad(parseInt(minute1), 2)); $('#ctl00_MainContent_txtRange2').val(zeroPad(parseInt(hour2), 2) + ':' + zeroPad(parseInt(minute2), 2)); }}); });</script>"

        ltrScript.Text = htmlSlider
    End Sub

End Class

