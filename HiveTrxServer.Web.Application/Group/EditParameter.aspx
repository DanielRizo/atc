﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="EditParameter.aspx.vb" Inherits="Group_EditParameter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Editar Parametro
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="tex t/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

    <script type="text/javascript" src="../js/jquery.ui.slider.js"></script>

    <script type="text/javascript" src="../js/jquery.ui.datepicker.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkListGroups" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/ListGroups.aspx">Listar Grupos</asp:LinkButton>
    </li>
    <li>Editar Parametros</li>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    FILE NAME : [ <%= objSessionParams.StrFIeldName %> ]
     <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Group/ListGroups.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>
</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlError" runat="server" Visible="False">
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <div class="titleh">
            <h3>Datos del Terminal</h3>
        </div>
        <div class="body">

            <div class="st-form-line">
                <span class="st-labeltext">Record No:</span>
                <asp:TextBox ID="txtRecordNo" CssClass="st-success-input"
                    runat="server" TabIndex="0" MaxLength="100"
                    ToolTip="Record No." Enabled="False" onkeydown="return (event.keyCode!=13);" Width="148px"></asp:TextBox>
                <div class="clear"></div>
            </div>

            <div class="st-form-line">
                <span class="st-labeltext">Tipo de Tabla:</span>
                <asp:TextBox ID="txtTableType" CssClass="st-success-input"
                    runat="server" TabIndex="1" MaxLength="100"
                    ToolTip="Tipo de Tabla." Enabled="false" onkeydown="return (event.keyCode!=13);" Width="148px"></asp:TextBox>
                <div class="clear"></div>
            </div>

            <div class="st-form-line">
                <span class="st-labeltext">Nombre Parametro:</span>
                <asp:TextBox ID="txtFieldName" CssClass="st-success-input"
                    runat="server" TabIndex="2" MaxLength="15"
                    ToolTip="Nombre del Parametro." Enabled="false" onkeydown="return (event.keyCode!=13);" Width="266px"></asp:TextBox>
                <div class="clear"></div>
            </div>

            <div class="st-form-line">
                <span class="st-labeltext">Valor: (*)</span>
                <asp:TextBox ID="txtValue" CssClass="st-forminput"
                    runat="server" TabIndex="3" MaxLength="5"
                    ToolTip="Valor Actual del Parametro" onkeydown="return (event.keyCode!=13);" Width="574px"></asp:TextBox>
                <div class="clear"></div>
            </div>
            <div class="st-form-line">
                <span class="st-labeltext">Nota: (*)</span>
                <asp:TextBox ID="TextNote" CssClass="st-forminput"
                    runat="server" TabIndex="3" MaxLength="5"
                    ToolTip="Nota Actual del Parametro" onkeydown="return (event.keyCode!=13);" Width="574px"></asp:TextBox>
                <div class="clear"></div>
            </div>
            <div class="button-box">
                <asp:Button ID="btnEdit" runat="server" Text="Editar Parametros"
                    CssClass="button-aqua" TabIndex="9" OnClientClick="return validateAddOrEditGroup()" />
            </div>
        </div>
    </div>
    <asp:HyperLink ID="lnkBack" runat="server" NavigateUrl="~/Group/ListGroups.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <!-- Validator -->

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

</asp:Content>

