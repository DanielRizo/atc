﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users
Imports System.Net
Imports System.Globalization
Imports TeleLoader.Groups

Partial Class Group_EditParameter
    Inherits TeleLoader.Web.BasePage

    Dim ParameterObj As Paramter

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        'Validar Acceso a Función      



        Try
            objAccessToken.Validate(getCurrentPage(), "Update")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        ParameterObj = New Paramter(strConnectionString, objSessionParams.StrFIeldName)

        'Establecer Datos de Grupo
        ParameterObj.Value = txtValue.Text
        ParameterObj.Note = TextNote.Text
        'Save Data
        If ParameterObj.editParameters() Then
            'New Group Edited OK
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Parametro Editado. Datos[ " & getFormDataLog() & " ]", "")

            pnlMsg.Visible = True
            pnlError.Visible = False
            lblMsg.Text = "Parametro editado correctamente."    
        Else
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Editando Parametros. Datos[ " & getFormDataLog() & " ]", "")
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error editando Parametros. Por favor valide los datos."
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then
            txtFieldName.Text = objAccessToken.FieldName

            ParameterObj = New Paramter(strConnectionStringStis, objSessionParams.StrFIeldName)
            'Leer datos BD
            ParameterObj.getGroupData()
            txtRecordNo.Text = ParameterObj.Record
            txtTableType.Text = ParameterObj.TableType
            txtFieldName.Text = ParameterObj.NameParameter
            txtValue.Text = ParameterObj.Value
            TextNote.Text = ParameterObj.Note
        End If
    End Sub


End Class

