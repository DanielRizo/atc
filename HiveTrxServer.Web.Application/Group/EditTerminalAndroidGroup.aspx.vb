﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users
Imports System.Net
Imports System.Globalization
Imports TeleLoader.Terminals
Imports System.Drawing

Partial Class Group_EditTerminalAndroidGroup
    Inherits TeleLoader.Web.BasePage

    Dim terminalObj As Terminal
    Dim contactObj As TeleLoader.Contact

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Update")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        terminalObj = New Terminal(strConnectionString, objSessionParams.intSelectedGroup)

        'Leer datos BD
        terminalObj.TerminalID = objSessionParams.intSelectedTerminalID
        terminalObj.getTerminalData()

        ''Establecer Datos de la Terminal Android
        terminalObj.AllowedApps = txtAllowedApps.Text
        terminalObj.GroupID = ddlGroups.SelectedValue

        'Establecer la aplicacion en modo kiosko
        terminalObj.KioskoApp = txtKioskoApp.Text

        'Validar si es necesario el contacto
        contactObj = New TeleLoader.Contact()

        If (txtContactNumId.Text.Trim.Length > 0) Then
            contactObj.IdentificationNumber = txtContactNumId.Text.Trim
            contactObj.IdentificationType = ddlContactIdType.SelectedValue
            contactObj.Name = txtContactName.Text.Trim
            contactObj.Address = txtContactAddress.Text.Trim
            contactObj.Mobile = txtContactMobile.Text.Trim
            contactObj.Phone = txtContactPhone.Text.Trim
            contactObj.Email = txtContactEmail.Text.Trim
        Else
            contactObj.IdentificationNumber = ""
            contactObj.IdentificationType = -1
            contactObj.Name = ""
            contactObj.Address = ""
            contactObj.Mobile = ""
            contactObj.Phone = ""
            contactObj.Email = ""
        End If

        terminalObj.Contact = contactObj

        'Save Data
        Dim rspCode As Int16 = 0
        Dim rspMsg As String = ""
        If terminalObj.editTerminal(rspCode, rspMsg) Then
            'New Terminal Updated OK
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Update", "Terminal actualizada. Datos[ " & getFormDataLog() & " ]", "")

            pnlMsg.Visible = True
            pnlError.Visible = False
            lblMsg.Text = rspMsg

        Else
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Actualizando Terminal. Datos[ " & getFormDataLog() & " ]", "")
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = rspCode
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            'GridView Aplicaciones Descargadas
            dsAppsXTerminal.SelectParameters("terminalId").DefaultValue = objSessionParams.intSelectedTerminalID
            dsAppsXTerminal.DataBind()

            'GridView Archivos de Configuración Descargados
            dsXmlsXTerminal.SelectParameters("terminalId").DefaultValue = objSessionParams.intSelectedTerminalID
            dsXmlsXTerminal.DataBind()

            'GridView Archivos de Imagenes Descargados
            dsImgsXTerminal.SelectParameters("terminalId").DefaultValue = objSessionParams.intSelectedTerminalID
            dsImgsXTerminal.DataBind()

            terminalObj = New Terminal(strConnectionString, objSessionParams.intSelectedGroup)

            'Leer datos BD
            terminalObj.TerminalID = objSessionParams.intSelectedTerminalID
            terminalObj.getTerminalData()

            txtSerial.Text = terminalObj.TerminalSerial
            txtHeraclesId.Text = terminalObj.HeraclesID
            txtAllowedApps.Text = terminalObj.AllowedApps
            txtInstalledApps.Text = IIf(terminalObj.InstalledApps.Length > 0, terminalObj.InstalledApps.Replace(",", vbCrLf), "")

            'txtGroupName.Text = objSessionParams.strtSelectedGroup

            txtKioskoApp.Text = terminalObj.KioskoApp

            'Mostrar el mensaje actual
            txtActualMessage.Text = terminalObj.ActualMessage

            If terminalObj.Contact.IdentificationNumber.Length > 0 Then
                txtContactNumId.Text = terminalObj.Contact.IdentificationNumber
                ddlContactIdType.SelectedValue = terminalObj.Contact.IdentificationType
                txtContactName.Text = terminalObj.Contact.Name
                txtContactAddress.Text = terminalObj.Contact.Address
                txtContactMobile.Text = terminalObj.Contact.Mobile
                txtContactPhone.Text = terminalObj.Contact.Phone
                txtContactEmail.Text = terminalObj.Contact.Email

                txtContactNumId.Enabled = True
                ddlContactIdType.Enabled = True
                txtContactName.Enabled = True
                txtContactAddress.Enabled = True
                txtContactMobile.Enabled = True
                txtContactPhone.Enabled = True
                txtContactEmail.Enabled = True

            End If

            txtSerial.Focus()

            dsGroups.SelectParameters("CustomerId").DefaultValue = objAccessToken.CustomerUserID

            ddlGroups.DataBind()

            ddlGroups.SelectedValue = terminalObj.GroupID

            setSwitchChangePassswordData(0)
            setSwitchLockTerminalsData(0)
            setSwitchReloadKeyData(0)

            'Set Toggle Visible
            ltrScript.Text &= "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); $('#hide-message1').css('z-index', 750); $('#hide-message1').css('display', 'block'); $('#hide-message2').css('z-index', 750); $('#hide-message2').css('display', 'block'); $('#hide-message3').css('z-index', 750); $('#hide-message3').css('display', 'block'); $('#hide-message4').css('z-index', 750); $('#hide-message4').css('display', 'block'); $('#hide-message5').css('z-index', 750); $('#hide-message5').css('display', 'block'); });</script>"

        Else
            Dim CtrlID As String = String.Empty

            If Request.Form("__EVENTTARGET") IsNot Nothing And
               Request.Form("__EVENTTARGET") <> String.Empty Then
                CtrlID = Request.Form("__EVENTTARGET")
            End If

            setSwitchChangePassswordData(changePasswordFlag.Value)
            setSwitchLockTerminalsData(lockTerminalsFlag.Value)
            setSwitchReloadKeyData(reloadKeyFlag.Value)

        End If

    End Sub

    Private Sub setSwitchChangePassswordData(flagChangePassword As Integer)
        Dim htmlSwitch As String

        If flagChangePassword = 1 Then
            htmlSwitch = "<p class='field switch' id='changePassword'><label for='radio1' id='radio11' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio2' id='radio12' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='changePassword'><label for='radio1' id='radio11' class='cb-enable'><span>S&iacute;</span></label><label for='radio2' id='radio12' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrChangePassword.Text = htmlSwitch

    End Sub

    Private Sub setSwitchLockTerminalsData(flagLockTerminals As Integer)
        Dim htmlSwitch As String

        If flagLockTerminals = 1 Then
            htmlSwitch = "<p class='field switch' id='lockTerminals'><label for='radio3' id='radio13' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio4' id='radio14' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='lockTerminals'><label for='radio3' id='radio13' class='cb-enable'><span>S&iacute;</span></label><label for='radio4' id='radio14' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrLockTerminals.Text = htmlSwitch

    End Sub

    Private Sub setSwitchReloadKeyData(flagReloadKey As Integer)
        Dim htmlSwitch As String

        If flagReloadKey = 1 Then
            htmlSwitch = "<p class='field switch' id='reloadKey'><label for='radio5' id='radio15' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio6' id='radio16' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='reloadKey'><label for='radio5' id='radio15' class='cb-enable'><span>S&iacute;</span></label><label for='radio6' id='radio16' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrReloadKey.Text = htmlSwitch

    End Sub

    Protected Sub btnQueryContactData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQueryContactData.Click

        'Consultar Datos de contacto existentes
        If txtContactNumId.Text.Length < 8 Then
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Ingrese un número de identificación válido."
            'Set Toggle Visible
            ltrScript.Text &= "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message7').css('z-index', 750); $('#hide-message7').css('display', 'block'); });</script>"
            Exit Sub
        End If

        'Buscar en BD
        getContactData()

        'Set Toggle Visible
        ltrScript.Text &= "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message7').css('z-index', 750); $('#hide-message7').css('display', 'block'); });</script>"

    End Sub

    Private Sub getContactData()
        Dim connection As New SqlConnection(ConnectionString())
        Dim command As New SqlCommand()
        Dim results As SqlDataReader

        Try
            'Abrir Conexion
            connection.Open()

            'Buscar Datos de Contacto
            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webConsultarDatosContacto"
            command.Parameters.Clear()
            command.Parameters.Add(New SqlParameter("contactNumID", txtContactNumId.Text.Trim))

            'Ejecutar SP
            results = command.ExecuteReader()

            If results.HasRows Then
                While results.Read()
                    txtContactNumId.Text = results.GetString(0)
                    ddlContactIdType.SelectedValue = results.GetInt64(1)
                    txtContactName.Text = results.GetString(2)
                    txtContactAddress.Text = results.GetString(3)
                    txtContactMobile.Text = results.GetString(4)
                    txtContactPhone.Text = results.GetString(5)
                    txtContactEmail.Text = results.GetString(6)

                    txtContactNumId.Enabled = False
                    ddlContactIdType.Enabled = False
                    txtContactName.Enabled = False
                    txtContactAddress.Enabled = False
                    txtContactMobile.Enabled = False
                    txtContactPhone.Enabled = False
                    txtContactEmail.Enabled = False

                End While
            Else
                txtContactNumId.Enabled = True
                ddlContactIdType.Enabled = True
                txtContactName.Enabled = True
                txtContactAddress.Enabled = True
                txtContactMobile.Enabled = True
                txtContactPhone.Enabled = True
                txtContactEmail.Enabled = True
            End If

            connection.Close()
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub

    Protected Sub btnDeletecontactData_Click(sender As Object, e As EventArgs) Handles btnDeletecontactData.Click

        txtContactNumId.Text = ""
        ddlContactIdType.SelectedValue = -1
        txtContactName.Text = ""
        txtContactAddress.Text = ""
        txtContactMobile.Text = ""
        txtContactPhone.Text = ""
        txtContactEmail.Text = ""

        txtContactNumId.Enabled = True
        ddlContactIdType.Enabled = False
        txtContactName.Enabled = False
        txtContactAddress.Enabled = False
        txtContactMobile.Enabled = False
        txtContactPhone.Enabled = False
        txtContactEmail.Enabled = False

        txtContactNumId.Focus()

        'Set Toggle Visible
        ltrScript.Text &= "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message7').css('z-index', 750); $('#hide-message7').css('display', 'block'); });</script>"

    End Sub

    Protected Sub btnSetActions_Click(sender As Object, e As EventArgs) Handles btnSetActions.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Update")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        terminalObj = New Terminal(strConnectionString, objSessionParams.intSelectedGroup)

        'Establecer Datos de Grupo
        terminalObj.TerminalID = objSessionParams.intSelectedTerminalID
        terminalObj.ChangePasswordTerminal = changePasswordFlag.Value
        terminalObj.NewPasswordTerminal = txtLockPassword.Text
        terminalObj.NewMessageTerminal = txtMessageToDisplay.Text
        terminalObj.ActualMessage = terminalObj.NewMessageTerminal
        terminalObj.LockTerminal = lockTerminalsFlag.Value
        terminalObj.ReloadKey = reloadKeyFlag.Value

        'Save Data
        If terminalObj.ExecuteActionsOnTerminal() Then
            'Actions Executed OK
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Acciones a Ejecutar Actualizadas. Datos[ " & getFormDataLog() & " ]", "")

            pnlMsg.Visible = True
            pnlError.Visible = False
            lblMsg.Text = "Acciones Ejecutadas correctamente."

            'Clear Form
            txtLockPassword.Text = ""
            txtMessageToDisplay.Text = ""
            txtActualMessage.Text = terminalObj.ActualMessage
            setSwitchChangePassswordData(0)
            setSwitchLockTerminalsData(0)
            setSwitchReloadKeyData(0)

        Else
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error ejecutando acciones. Datos[ " & getFormDataLog() & " ]", "")
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error Ejecutando acciones. Por favor valide los datos."
        End If

    End Sub

    Protected Sub grdXmlsXTerminals_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdXmlsXTerminals.RowDataBound

        Dim backgroundColorGreen As String = "#dff0d8"
        Dim backgroundColorRed As String = "#e47a7a"

        If e.Row.RowType = DataControlRowType.DataRow Then

            e.Row.Style("background") = DataBinder.Eval(e.Row.DataItem, "desc_color")
            e.Row.Cells(0).ForeColor = IIf(DataBinder.Eval(e.Row.DataItem, "desc_color") = backgroundColorGreen, Color.Gray, Color.White)
            e.Row.Cells(1).ForeColor = IIf(DataBinder.Eval(e.Row.DataItem, "desc_color") = backgroundColorGreen, Color.Gray, Color.White)

        End If

    End Sub

    Protected Sub grdImgsXTerminals_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdImgsXTerminals.RowDataBound

        Dim backgroundColorGreen As String = "#dff0d8"
        Dim backgroundColorRed As String = "#e47a7a"

        If e.Row.RowType = DataControlRowType.DataRow Then

            e.Row.Style("background") = DataBinder.Eval(e.Row.DataItem, "desc_color")
            e.Row.Cells(0).ForeColor = IIf(DataBinder.Eval(e.Row.DataItem, "desc_color") = backgroundColorGreen, Color.Gray, Color.White)
            e.Row.Cells(1).ForeColor = IIf(DataBinder.Eval(e.Row.DataItem, "desc_color") = backgroundColorGreen, Color.Gray, Color.White)

        End If

    End Sub

    Protected Sub grdAppsXTerminal_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdAppsXTerminal.RowDataBound

        Dim backgroundColorGreen As String = "#dff0d8"
        Dim backgroundColorRed As String = "#e47a7a"

        If e.Row.RowType = DataControlRowType.DataRow Then

            e.Row.Style("background") = DataBinder.Eval(e.Row.DataItem, "desc_color")
            e.Row.Cells(0).ForeColor = IIf(DataBinder.Eval(e.Row.DataItem, "desc_color") = backgroundColorGreen, Color.Gray, Color.White)
            e.Row.Cells(1).ForeColor = IIf(DataBinder.Eval(e.Row.DataItem, "desc_color") = backgroundColorGreen, Color.Gray, Color.White)
            e.Row.Cells(2).ForeColor = IIf(DataBinder.Eval(e.Row.DataItem, "desc_color") = backgroundColorGreen, Color.Gray, Color.White)
            e.Row.Cells(3).ForeColor = IIf(DataBinder.Eval(e.Row.DataItem, "desc_color") = backgroundColorGreen, Color.Gray, Color.White)

        End If

    End Sub
End Class
