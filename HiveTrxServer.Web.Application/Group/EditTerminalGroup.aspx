﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="EditTerminalGroup.aspx.vb" Inherits="Group_EditTerminalGroup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Editar Terminal del Grupo
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

    <script type="text/javascript" src="../js/jquery.ui.slider.js"></script>

    <script type="text/javascript" src="../js/jquery.ui.datepicker.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkListGroups" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/ListGroups.aspx">Listar Grupos</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkGroupTerminals" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/GroupTerminals.aspx">Terminales del Grupo</asp:LinkButton>
    </li>
    <li>Editar Terminal</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    Editar Terminal 
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Group/ListGroups.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlError" runat="server" Visible="False">
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <div class="titleh">
            <h3>Datos de la Terminal</h3>
        </div>

        <div class="toggle-message" style="z-index: 590; top: 0px; left: 0px;">
            <h3 class="title">Nombre del Grupo...
                    <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
            <div class="hide-message" id="hide-message3" style="display: none;">
            </div>
            <div class="st-form-line">
                <span class="st-labeltext">Desea cambiar de Grupo?:</span>
                <asp:DropDownList ID="ddlGroups" class="uniform" runat="server"
                    DataSourceID="dsGroups" DataTextField="gru_nombre"
                    DataValueField="gru_id" ToolTip="Seleccione un Grupo." TabIndex="30">
                </asp:DropDownList>
                <asp:SqlDataSource ID="dsGroups" runat="server"
                    ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
                    SelectCommand="SELECT gru_id, gru_nombre FROM Grupo WHERE gru_cliente_id = @CustomerId AND gru_tipo_android = 0">
                    <SelectParameters>
                        <asp:Parameter Name="CustomerId" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <div class="clear"></div>
            </div>


        </div>
        <div class="body">
            <div class="st-form-line">
                <span class="st-labeltext">Serial: (*)</span>
                <asp:TextBox ID="txtSerial" CssClass="st-forminput" Style="width: 150px"
                    runat="server" TabIndex="1" MaxLength="20"
                    ToolTip="Digite serial de la terminal." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>

            <table width="100%">
                <tr>
                    <td>
                        <div class="st-form-line">
                            <span class="st-labeltext-inline">Tipo: (*)</span>
                            <asp:DropDownList ID="ddlTerminalType" class="uniform" runat="server"
                                DataSourceID="dsTerminalType" DataTextField="descripcion"
                                DataValueField="id" Width="200px"
                                ToolTip="Seleccione el tipo de terminal." TabIndex="2">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="dsTerminalType" runat="server"
                                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" SelectCommand="SELECT -1 AS id, '       ' AS descripcion
                                UNION ALL
                                SELECT id, descripcion FROM TipoTerminal"></asp:SqlDataSource>
                            <div class="clear"></div>
                        </div>
                    </td>
                    <td>
                        <div class="st-form-line">
                            <span class="st-labeltext-inline">Marca: (*)</span>
                            <asp:DropDownList ID="ddlTerminalBrand" class="uniform" runat="server"
                                DataSourceID="dsTerminalBrand" DataTextField="descripcion"
                                DataValueField="id" Width="200px"
                                ToolTip="Seleccione la marca de la terminal." TabIndex="3">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="dsTerminalBrand" runat="server"
                                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" SelectCommand="SELECT -1 AS id, '       ' AS descripcion
                                UNION ALL
                                SELECT id, descripcion FROM MarcaTerminal"></asp:SqlDataSource>
                            <div class="clear"></div>
                        </div>
                    </td>
                    <td>
                        <div class="st-form-line">
                            <span class="st-labeltext-inline">Modelo: (*)</span>
                            <asp:DropDownList ID="ddlTerminalModel" class="uniform" runat="server"
                                DataSourceID="dsTerminalModel" DataTextField="descripcion"
                                DataValueField="id" Width="200px"
                                ToolTip="Seleccione el modelo de la terminal." TabIndex="4">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="dsTerminalModel" runat="server"
                                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" SelectCommand="SELECT -1 AS id, '       ' AS descripcion
                                UNION ALL
                                SELECT id, descripcion FROM ModeloTerminal"></asp:SqlDataSource>
                            <div class="clear"></div>
                        </div>
                    </td>
                    <td>
                        <div class="st-form-line">
                            <span class="st-labeltext-inline">Tipo de Comunicación: (*)</span>
                            <asp:DropDownList ID="ddlTerminalInterface" class="uniform" runat="server"
                                DataSourceID="dsTerminalInterface" DataTextField="descripcion"
                                DataValueField="id" Width="200px"
                                ToolTip="Seleccione la interface de comunicación de la terminal." TabIndex="5">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="dsTerminalInterface" runat="server"
                                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" SelectCommand="SELECT -1 AS id, '       ' AS descripcion
                                UNION ALL
                                SELECT id, descripcion FROM InterfaceTerminal"></asp:SqlDataSource>
                            <div class="clear"></div>
                        </div>
                    </td>
                </tr>
            </table>

            <table width="100%">
                <tr>
                    <td>
                        <div class="st-form-line">
                            <span class="st-labeltext-inline">Serial SIM:</span>
                            <asp:TextBox ID="txtSerialSIM" CssClass="st-forminput" Style="width: 150px"
                                runat="server" TabIndex="6" MaxLength="20"
                                ToolTip="Digite serial del SIM." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                            <div class="clear"></div>
                        </div>
                    </td>
                    <td>
                        <div class="st-form-line">
                            <span class="st-labeltext-inline">Tel&eacute;fono SIM:</span>
                            <asp:TextBox ID="txtMobileSIM" CssClass="st-forminput" Style="width: 150px"
                                runat="server" TabIndex="7" MaxLength="20"
                                ToolTip="Digite teléfono celular del SIM." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                            <div class="clear"></div>
                        </div>
                    </td>
                    <td>
                        <div class="st-form-line">
                            <span class="st-labeltext-inline">IMEI:</span>
                            <asp:TextBox ID="txtIMEI" CssClass="st-forminput" Style="width: 150px"
                                runat="server" TabIndex="8" MaxLength="20"
                                ToolTip="Digite IMEI del Módem." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                            <div class="clear"></div>
                        </div>
                    </td>
                </tr>
            </table>

            <table width="100%">
                <tr>
                    <td>
                        <div class="st-form-line">
                            <span class="st-labeltext-inline">Serial Cargador:</span>
                            <asp:TextBox ID="txtChargerSerial" CssClass="st-forminput" Style="width: 150px"
                                runat="server" TabIndex="9" MaxLength="20"
                                ToolTip="Digite serial del cargador." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                            <div class="clear"></div>
                        </div>
                    </td>
                    <td>
                        <div class="st-form-line">
                            <span class="st-labeltext-inline">Serial Bater&iacute;a:</span>
                            <asp:TextBox ID="txtBatterySerial" CssClass="st-forminput" Style="width: 150px"
                                runat="server" TabIndex="10" MaxLength="20"
                                ToolTip="Digite serial del cargador." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                            <div class="clear"></div>
                        </div>
                    </td>
                    <td>
                        <div class="st-form-line">
                            <span class="st-labeltext-inline">Serial Lector Biom&eacute;trico:</span>
                            <asp:TextBox ID="txtBiometricSerial" CssClass="st-forminput" Style="width: 150px"
                                runat="server" TabIndex="11" MaxLength="20"
                                ToolTip="Digite serial del cargador." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                            <div class="clear"></div>
                        </div>
                    </td>
                </tr>
            </table>

            <div class="st-form-line">
                <span class="st-labeltext">Descripci&oacute;n:</span>
                <asp:TextBox ID="txtDesc" CssClass="st-forminput" Style="width: 510px"
                    runat="server" TabIndex="12" MaxLength="250" TextMode="MultiLine"
                    ToolTip="Digite descripción del grupo." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>

            <div class="st-form-line">
                <span class="st-labeltext">Prioridad Grupo?:</span>

                <asp:Literal ID="ltrPriority" runat="server"></asp:Literal>

                <asp:HiddenField ID="terminalFlagPriority" runat="server" Value="1" />

                <div class="clear"></div>
            </div>

            <asp:Panel ID="pnlUpdateTerminal" runat="server" Visible="False">
                <div class="st-form-line">
                    <span class="st-labeltext">Actualizar Terminal?:</span>

                    <asp:Literal ID="ltrUpdate" runat="server"></asp:Literal>

                    <asp:HiddenField ID="terminalFlagUpdate" runat="server" Value="0" />

                    <div class="clear">
                    </div>
                </div>
            </asp:Panel>

            <asp:Panel ID="pnlSchedulePerTerminal" runat="server" Visible="False">
                <div class="st-form-line" style="text-align: center;">
                    <span>Fecha de Programaci&oacute;n: (*)&nbsp;&nbsp;</span>
                    <asp:TextBox ID="txtScheduleDate" CssClass="datepicker-input"
                        onkeydown="return false;" runat="server" Font-Bold="True" Width="100px"
                        ToolTip="Fecha de Programación" TabIndex="13"></asp:TextBox>
                    <div class="clear"></div>
                </div>

                <div class="st-form-line">
                    <span class="st-labeltext">Hora: (*)</span>

                    <asp:TextBox ID="txtRange1" CssClass="st-forminput" Style="width: 40px"
                        runat="server" TabIndex="14" MaxLength="5"
                        ToolTip="Hora de Programación" Text="00:00" Font-Bold="True" onkeydown="return false;"></asp:TextBox>

                    <div id="slider-range" class="margin-top10 ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" style="z-index: 640; width: 68%; float: right;">
                        <div class="ui-slider-range ui-widget-header" style="left: 0%; width: 0%;"></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="st-form-line">
                    <span class="st-labeltext">IP de Descarga: (*)</span>
                    <asp:TextBox ID="txtIP" CssClass="st-forminput" Style="width: 100px"
                        runat="server" TabIndex="15" MaxLength="15"
                        ToolTip="Digite IP para descarga remota." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>

                <div class="st-form-line">
                    <span class="st-labeltext">Puerto de Descarga: (*)</span>
                    <asp:TextBox ID="txtPort" CssClass="st-forminput" Style="width: 60px"
                        runat="server" TabIndex="16" MaxLength="5"
                        ToolTip="Digite Puerto TCP para descarga remota." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>


                <div class="st-form-line">

                    <span class="st-labeltext">Modo Bloqueante?:</span>

                    <asp:Literal ID="ltrBlockingMode" runat="server"></asp:Literal>

                    <asp:HiddenField ID="terminalFlagBlockingMode" runat="server" Value="0" />

                    <div class="clear">
                    </div>
                </div>

                <div class="st-form-line">

                    <span class="st-labeltext">Actualizacion inmediata?:</span>

                    <asp:Literal ID="ltrUpdateNow" runat="server"></asp:Literal>

                    <asp:HiddenField ID="terminalFlagUpdateNow" runat="server" Value="0" />

                    <div class="clear">
                    </div>
                </div>

            </asp:Panel>

            <div class="st-form-line">
                <span class="st-labeltext">Estado Terminal:</span>

                <asp:Literal ID="ltrStatus" runat="server"></asp:Literal>

                <asp:HiddenField ID="terminalFlagStatus" runat="server" Value="0" />

                <div class="clear"></div>
            </div>
            <div class="st-form-line">
                <span class="st-labeltext">Inicializar Terminal ?:</span>

                <asp:Literal ID="ltrInitialize" runat="server"></asp:Literal>

                <asp:HiddenField ID="groupFlagInitialize" runat="server" Value="0" />

                <div class="clear"></div>
            </div>

            <div class="st-form-line">
                <span class="st-labeltext">Descargar llaves?:</span>
                <asp:Literal ID="ltrDownloadKey" runat="server"></asp:Literal>
                <asp:HiddenField ID="groupDpwnloadKey" runat="server" Value="0" />
                <div class="clear"></div>
            </div>

            <div class="toggle-message" style="z-index: 590; top: 0px; left: 0px;">
                <h3 class="title">Validaciones On-Line
                    <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
                <div class="hide-message" id="hide-message" style="display: none;">

                    <div class="st-form-line">
                        <span class="st-labeltext">Validar IMEI?:</span>

                        <asp:Literal ID="ltrIMEI" runat="server"></asp:Literal>

                        <asp:HiddenField ID="terminalIMEIFlag" runat="server" Value="0" />

                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">
                        <span class="st-labeltext">Validar Serial SIM?:</span>

                        <asp:Literal ID="ltrSIM" runat="server"></asp:Literal>

                        <asp:HiddenField ID="terminalSIMFlag" runat="server" Value="0" />

                        <div class="clear">
                        </div>
                    </div>

                </div>
            </div>


            <div class="toggle-message" style="z-index: 590; top: 0px; left: 0px;">
                <h3 class="title">Datos de Contacto (Opcional)
                    <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
                <div class="hide-message" id="hide-message2" style="display: none;">

                    <div class="st-form-line">
                        <span class="st-labeltext">N&uacute;mero de Identificaci&oacute;n:</span>
                        <asp:TextBox ID="txtContactNumId" CssClass="st-forminput" Style="width: 100px"
                            runat="server" TabIndex="17" MaxLength="14"
                            ToolTip="Digite número de identificación." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnQueryContactData" runat="server" Text="Consultar Datos Existentes"
                            CssClass="button-aqua" TabIndex="18" />
                        &nbsp;&nbsp;
                        <asp:Button ID="btnDeletecontactData" runat="server" Text="Limpiar"
                            CssClass="st-button" TabIndex="19" />
                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">
                        <span class="st-labeltext">Tipo de Identificaci&oacute;n:</span>
                        <asp:DropDownList ID="ddlContactIdType" class="uniform" runat="server"
                            DataSourceID="dsContactIdType" DataTextField="descripcion"
                            DataValueField="id" Width="200px"
                            ToolTip="Seleccione el tipo de identificación." TabIndex="20" Enabled="False">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="dsContactIdType" runat="server"
                            ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" SelectCommand="SELECT -1 AS id, '       ' AS descripcion
                            UNION ALL
                            SELECT * FROM TipoIdentificacion"></asp:SqlDataSource>
                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">
                        <span class="st-labeltext">Nombre:</span>
                        <asp:TextBox ID="txtContactName" CssClass="st-forminput" Style="width: 510px"
                            runat="server" TabIndex="21" MaxLength="100"
                            ToolTip="Digite nombre del contacto." onkeydown="return (event.keyCode!=13);" Enabled="False"></asp:TextBox>
                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">
                        <span class="st-labeltext">Direcci&oacute;n:</span>
                        <asp:TextBox ID="txtContactAddress" CssClass="st-forminput" Style="width: 510px"
                            runat="server" TabIndex="22" MaxLength="100"
                            ToolTip="Digite dirección del contacto." onkeydown="return (event.keyCode!=13);" Enabled="False"></asp:TextBox>
                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">
                        <span class="st-labeltext">Celular:</span>
                        <asp:TextBox ID="txtContactMobile" CssClass="st-forminput" Style="width: 100px"
                            runat="server" TabIndex="23" MaxLength="20"
                            ToolTip="Digite celular del contacto." onkeydown="return (event.keyCode!=13);" Enabled="False"></asp:TextBox>
                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">
                        <span class="st-labeltext">Tel&eacute;fono Fijo:</span>
                        <asp:TextBox ID="txtContactPhone" CssClass="st-forminput" Style="width: 100px"
                            runat="server" TabIndex="24" MaxLength="20"
                            ToolTip="Digite teléfono fijo del contacto." onkeydown="return (event.keyCode!=13);" Enabled="False"></asp:TextBox>
                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">
                        <span class="st-labeltext">Correo Electr&oacute;nico:</span>
                        <asp:TextBox ID="txtContactEmail" CssClass="st-forminput" Style="width: 510px"
                            runat="server" TabIndex="25" MaxLength="100"
                            ToolTip="Digite correo electrónico del usuario." onkeydown="return (event.keyCode!=13);" Enabled="False"></asp:TextBox>
                        <div class="clear"></div>
                    </div>

                </div>
            </div>

            <%--<div class="button-box">
                <asp:Button ID="btnEdit" runat="server" Text="Guardar"
                    CssClass="button-aqua" TabIndex="26" OnClientClick="return validateAddOrEditGroupTerminal()" />
            </div>--%>
        </div>
    </div>
    <div class="titleh">
        <h3>Datos de la Terminal Android</h3>
    </div>
    <div class="toggle-message" style="z-index: 590; top: 0px; left: 0px;">




        <div class="body">



            <div class="toggle-message" style="z-index: 590;">
                <h3 class="title">Acciones...
                    <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
                <div class="hide-message" id="hide-message" style="display: none;">

                    <div class="st-form-line">
                        <span class="st-labeltext">Cambiar Clave Dispositivo: </span>

                        <asp:Literal ID="ltrChangePassword" runat="server"></asp:Literal>

                        <asp:HiddenField ID="changePasswordFlag" runat="server" Value="0" />

                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">
                        <span class="st-labeltext">Clave: </span>
                        <asp:TextBox ID="txtLockPassword" CssClass="st-forminput" Style="width: 510px"
                            runat="server" TabIndex="2" MaxLength="15"
                            ToolTip="Digite clave para bloqueo de pantalla." onkeydown="return (event.keyCode!=13);" disabled="disabled" placeholder="Cambiar Clave al Dispositivo..."></asp:TextBox>
                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">
                        <span class="st-labeltext">Mensaje: </span>
                        <asp:TextBox ID="txtMessageToDisplay" CssClass="st-forminput" Style="width: 510px"
                            runat="server" TabIndex="3" MaxLength="50"
                            ToolTip="Digite mensaje a desplegar en pantalla." onkeydown="return (event.keyCode!=13);" placeholder="Enviar Mensaje al Dispositivo..."></asp:TextBox>
                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">
                        <span class="st-labeltext">Mensaje Actual: </span>
                        <asp:TextBox ID="txtActualMessage" CssClass="st-success-input" Style="width: 510px"
                            runat="server" ToolTip="Mensaje configurado en la plataforma" MaxLength="50" Enabled="False"></asp:TextBox>
                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">
                        <span class="st-labeltext">Bloquear Dispositivo: </span>

                        <asp:Literal ID="ltrLockTerminals" runat="server"></asp:Literal>

                        <asp:HiddenField ID="lockTerminalsFlag" runat="server" Value="0" />

                        <div class="clear"></div>
                    </div>

                    <div class="st-form-line">
                        <span class="st-labeltext">Recargar Llave: </span>

                        <asp:Literal ID="ltrReloadKey" runat="server"></asp:Literal>

                        <asp:HiddenField ID="reloadKeyFlag" runat="server" Value="0" />

                        <div class="clear"></div>
                    </div>

                    <div class="button-box">
                        <asp:Button ID="btnSetActions" runat="server" Text="Ejecutar" OnClientClick="return validateExecuteActions();"
                            CssClass="button-aqua" TabIndex="4" />
                    </div>
                </div>
            </div>

            <h3 class="title">Aplicaciones Descargadas...
                    <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
            <div class="hide-message" id="hide-message1" style="display: none;">

                <asp:GridView ID="grdAppsXTerminal" runat="server" AllowPaging="True"
                    AllowSorting="True" AutoGenerateColumns="False" CellPadding="4"
                    DataSourceID="dsAppsXTerminal" ForeColor="#333333"
                    CssClass="mGridCenter" DataKeyNames="desc_id"
                    EmptyDataText="No se encontraron aplicaciones descargadas por el dispositivo!"
                    HorizontalAlign="Center">
                    <RowStyle BackColor="White" ForeColor="White" />
                    <Columns>
                        <asp:BoundField DataField="desc_fecha" HeaderText="Fecha" HeaderStyle-Width="200px"
                            SortExpression="desc_fecha" />
                        <asp:BoundField DataField="desc_apk_archivo" HeaderText="Nombre de Archivo"
                            SortExpression="desc_apk_archivo" />
                        <asp:BoundField DataField="desc_apk_paquete" HeaderText="Nombre del Paquete"
                            SortExpression="desc_apk_paquete" />
                        <asp:BoundField DataField="desc_apk_version" HeaderText="Versión"
                            SortExpression="desc_apk_version" ItemStyle-HorizontalAlign="Center" />
                    </Columns>
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
                        CssClass="pgr" Font-Underline="False" />
                    <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid"
                        BorderWidth="1px" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                </asp:GridView>
                <br />
                <asp:SqlDataSource ID="dsAppsXTerminal" runat="server"
                    ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
                    SelectCommand="sp_webConsultarAppsTerminalAndroid" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:Parameter Name="terminalId" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>

            <h3 class="title">Archivos de Configuración Descargados...
                    <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
            <div class="hide-message" id="hide-message2" style="display: none;">

                <asp:GridView ID="grdXmlsXTerminals" runat="server" AllowPaging="True"
                    AllowSorting="True" AutoGenerateColumns="False" CellPadding="4"
                    DataSourceID="dsXmlsXTerminal" ForeColor="#333333"
                    CssClass="mGridCenter" DataKeyNames="desc_id"
                    EmptyDataText="No se encontraron archivos de configuración descargados por el dispositivo!"
                    HorizontalAlign="Center">
                    <RowStyle BackColor="White" ForeColor="White" />
                    <Columns>
                        <asp:BoundField DataField="desc_fecha" HeaderText="Fecha" HeaderStyle-Width="200px"
                            SortExpression="desc_fecha" />
                        <asp:BoundField DataField="desc_zip_xml" HeaderText="Nombre de Archivo"
                            SortExpression="desc_zip_xml" />
                    </Columns>
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
                        CssClass="pgr" Font-Underline="False" />
                    <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid"
                        BorderWidth="1px" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                </asp:GridView>
                <br />
                <asp:SqlDataSource ID="dsXmlsXTerminal" runat="server"
                    ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
                    SelectCommand="sp_webConsultarXMLsTerminalAndroid" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:Parameter Name="terminalId" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>

            <h3 class="title">Archivos de Imágenes Descargados...
                    <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
            <div class="hide-message" id="hide-message3" style="display: none;">

                <asp:GridView ID="grdImgsXTerminals" runat="server" AllowPaging="True"
                    AllowSorting="True" AutoGenerateColumns="False" CellPadding="4"
                    DataSourceID="dsImgsXTerminal" ForeColor="#333333"
                    CssClass="mGridCenter" DataKeyNames="desc_id"
                    EmptyDataText="No se encontraron archivos de imágenes descargados por el dispositivo!"
                    HorizontalAlign="Center">
                    <RowStyle BackColor="White" ForeColor="White" />
                    <Columns>
                        <asp:BoundField DataField="desc_fecha" HeaderText="Fecha" HeaderStyle-Width="200px"
                            SortExpression="desc_fecha" />
                        <asp:BoundField DataField="desc_zip_imgs" HeaderText="Nombre de Archivo"
                            SortExpression="desc_zip_imgs" />
                    </Columns>
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
                        CssClass="pgr" Font-Underline="False" />
                    <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid"
                        BorderWidth="1px" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                </asp:GridView>
                <br />
                <asp:SqlDataSource ID="dsImgsXTerminal" runat="server"
                    ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
                    SelectCommand="sp_webConsultarIMGsTerminalAndroid" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:Parameter Name="terminalId" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>

            <h3 class="title">Aplicaciones Permitidas...
                    <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
            <div class="hide-message" id="hide-message4" style="display: none;">

                <div class="st-form-line">
                    <span class="st-labeltext">Aplicaciones Permitidas: </span>
                    <asp:TextBox ID="txtAllowedApps" CssClass="st-forminput" Style="width: 510px; height: 100px;"
                        runat="server" TabIndex="8" MaxLength="500" TextMode="MultiLine"
                        ToolTip="Digite aplicaciones permitidas para el dispositivo." onkeydown="return (event.keyCode!=13);"></asp:TextBox>

                    <br />

                    <blockquote>Para efectos de mantenimiento sobre este dispositivo, marcar en Aplicaciones Permitidas la palabra "all".</blockquote>

                </div>
            </div>

            <h3 class="title">Aplicaciones Instaladas...
                    <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
            <div class="hide-message" id="hide-message5" style="display: none;">

                <div class="st-form-line">
                    <span class="st-labeltext">Aplicaciones Instaladas: </span>
                    <asp:TextBox ID="txtInstalledApps" CssClass="st-forminput" Style="width: 510px; height: 100px;"
                        runat="server" TabIndex="8" MaxLength="500" TextMode="MultiLine" Enabled="False"
                        ToolTip="Aplicaciones instaladas en el dispositivo." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                </div>

            </div>
        </div>

        <h3 class="title">Aplicacion modo Kiosko
                    <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
        <div class="hide-message" id="hide-message6" style="display: none;">

            <div class="st-form-line">
                <span class="st-labeltext">Aplicacion: </span>
                <asp:TextBox ID="txtKioskoApp" CssClass="st-forminput" Style="width: 510px; height: 100px;"
                    runat="server" TabIndex="8" MaxLength="500" TextMode="MultiLine"
                    ToolTip="Digite aplicacion modo kiosko para el dispositivo." onkeydown="return (event.keyCode!=13);"></asp:TextBox>

                <br />

                <blockquote>Se debe colocar el package de la aplicacion que va a trabajar en modo kiosko</blockquote>

            </div>

        </div>
        <div class="button-box" align="center">
            <asp:Button ID="btnEdit" runat="server" Text="Guardar"
                CssClass="button-aqua" TabIndex="26" OnClientClick="return validateAddOrEditGroupTerminal()" />
        </div>
    </div>

    <%--<div class="button-box">
                <asp:Button ID="Button4" runat="server" Text="Guardar"
                    CssClass="button-aqua" TabIndex="26" OnClientClick="return validateEditGroupAndroidTerminal()" />
            </div>--%>



    <asp:HyperLink ID="lnkBack" runat="server" NavigateUrl="~/Group/GroupTerminals.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <!-- Validator -->
    <script src="../js/ValidatorGroupTerminal.js" type="text/javascript"></script>
    <script src="../js/ValidatorGroupAndroidTerminal.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

</asp:Content>

