﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users
Imports System.Net
Imports System.Globalization
Imports TeleLoader.Terminals
Imports System.Drawing

Partial Class Group_EditTerminalGroup
    Inherits TeleLoader.Web.BasePage

    Dim terminalObj As Terminal
    Dim contactObj As TeleLoader.Contact

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim scheduledDate As Date

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Update")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        scheduledDate = Date.Parse(txtScheduleDate.Text & " " & txtRange1.Text, New CultureInfo("es-CO"))

        terminalObj = New Terminal(strConnectionString, objSessionParams.intSelectedGroup)

        'Establecer Datos de la Terminal
        terminalObj.TerminalID = objSessionParams.intSelectedTerminalID
        terminalObj.TerminalSerial = txtSerial.Text.Trim()
        terminalObj.TerminalType = ddlTerminalType.SelectedValue
        terminalObj.TerminalBrand = ddlTerminalBrand.SelectedValue
        terminalObj.TerminalModel = ddlTerminalModel.SelectedValue
        terminalObj.TerminalInterface = ddlTerminalInterface.SelectedValue
        terminalObj.SerialSIM = txtSerialSIM.Text.Trim()
        terminalObj.MobileSIM = txtMobileSIM.Text.Trim()
        terminalObj.IMEI = txtIMEI.Text.Trim()
        terminalObj.ChargerSerial = txtChargerSerial.Text.Trim()
        terminalObj.BatterySerial = txtBatterySerial.Text.Trim()
        terminalObj.BiometricSerial = txtBiometricSerial.Text.Trim()
        terminalObj.Desc = txtDesc.Text.Trim()
        terminalObj.GroupID = ddlGroups.SelectedValue
        ''Establecer Datos de la Terminal Android
        terminalObj.AllowedApps = txtAllowedApps.Text

        'Establecer la aplicacion en modo kiosko
        terminalObj.KioskoApp = txtKioskoApp.Text

        If terminalFlagPriority.Value = 1 Then
            'Programación por Grupo
            terminalObj.GroupPriority = True
            terminalObj.UpdateTerminal = False
            terminalObj.ScheduleUpdate = scheduledDate
            terminalObj.IPAddress = ""
            terminalObj.Port = ""

        Else
            'Programación por Terminal
            terminalObj.GroupPriority = False

            terminalObj.BlockingMode = terminalFlagBlockingMode.Value

            terminalObj.UpdateNow = terminalFlagUpdateNow.Value

            terminalObj.ScheduleUpdate = scheduledDate

            terminalObj.Initialize = groupFlagInitialize.Value
            terminalObj.DownloadKey = groupDpwnloadKey.Value

            If terminalFlagUpdate.Value = 1 Then
                'Actualizar Terminal Manual
                terminalObj.UpdateTerminal = True
                terminalObj.IPAddress = txtIP.Text.Trim()
                terminalObj.Port = txtPort.Text.Trim()
            Else
                terminalObj.UpdateTerminal = False
                terminalObj.IPAddress = ""
                terminalObj.Port = ""
            End If
        End If

        'Otras Validaciones
        terminalObj.ValidateIMEI = terminalIMEIFlag.Value
        terminalObj.ValidateSIM = terminalSIMFlag.Value

        'Estado de la Terminal
        terminalObj.TerminalStatus = IIf(terminalFlagStatus.Value = 1, 1, 2)

        'Validar si es necesario el contacto
        contactObj = New TeleLoader.Contact()

        If (txtContactNumId.Text.Trim.Length > 0) Then
            contactObj.IdentificationNumber = txtContactNumId.Text.Trim
            contactObj.IdentificationType = ddlContactIdType.SelectedValue
            contactObj.Name = txtContactName.Text.Trim
            contactObj.Address = txtContactAddress.Text.Trim
            contactObj.Mobile = txtContactMobile.Text.Trim
            contactObj.Phone = txtContactPhone.Text.Trim
            contactObj.Email = txtContactEmail.Text.Trim
        Else
            contactObj.IdentificationNumber = ""
            contactObj.IdentificationType = -1
            contactObj.Name = ""
            contactObj.Address = ""
            contactObj.Mobile = ""
            contactObj.Phone = ""
            contactObj.Email = ""
        End If

        terminalObj.Contact = contactObj

        'Save Data
        Dim rspCode As Int16 = 0
        Dim rspMsg As String = ""
        If terminalObj.editTerminal(rspCode, rspMsg) Then
            'New Group Created OK
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Update", "Terminal actualizada. Datos[ " & getFormDataLog() & " ]", "")

            pnlMsg.Visible = True
            pnlError.Visible = False
            lblMsg.Text = rspMsg

            setSwitchIMEIData(terminalObj.ValidateIMEI)
            setSwitchSIMData(terminalObj.ValidateSIM)
            setSwitchStatusData(terminalObj.TerminalStatus)
            setSwitchBlockingMode(terminalObj.BlockingMode)
            setSwitchUpdateNow(terminalObj.UpdateNow)
            setSwitchUpdateNow(terminalObj.Initialize)
        Else
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Actualizando Terminal. Datos[ " & getFormDataLog() & " ]", "")
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = rspMsg
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            terminalObj = New Terminal(strConnectionString, objSessionParams.intSelectedGroup)

            'Leer datos BD
            terminalObj.TerminalID = objSessionParams.intSelectedTerminalID
            terminalObj.getTerminalData()

            txtSerial.Text = terminalObj.TerminalSerial
            ddlTerminalType.SelectedValue = terminalObj.TerminalType
            ddlTerminalBrand.SelectedValue = terminalObj.TerminalBrand
            ddlTerminalModel.SelectedValue = terminalObj.TerminalModel
            ddlTerminalInterface.SelectedValue = terminalObj.TerminalInterface
            txtSerialSIM.Text = terminalObj.SerialSIM
            txtMobileSIM.Text = terminalObj.MobileSIM
            txtIMEI.Text = terminalObj.IMEI
            txtChargerSerial.Text = terminalObj.ChargerSerial
            txtBatterySerial.Text = terminalObj.BatterySerial
            txtBiometricSerial.Text = terminalObj.BiometricSerial
            txtDesc.Text = terminalObj.Desc

            setSwitchPriorityData(terminalObj.GroupPriority)
            terminalFlagPriority.Value = IIf(terminalObj.GroupPriority, 1, 0)


            setSwitchInitialize(terminalObj.Initialize)
            groupFlagInitialize.Value = IIf(terminalObj.Initialize, 1, 0)
            groupDpwnloadKey.Value = IIf(terminalObj.DownloadKey, 1, 0)

            If terminalObj.DownloadKey = False Then
                setSwitchDownloadKey(0)
            Else
                setSwitchDownloadKey(1)
            End If

            If terminalObj.GroupPriority = False Then
                setSwitchUpdateData(terminalObj.UpdateTerminal)
                terminalFlagUpdate.Value = IIf(terminalObj.UpdateTerminal, 1, 0)
                pnlUpdateTerminal.Visible = True

                If terminalObj.UpdateTerminal = True Then
                    pnlSchedulePerTerminal.Visible = True
                    txtScheduleDate.Text = terminalObj.ScheduleUpdate.ToString("dd/MM/yyyy")
                    txtRange1.Text = terminalObj.ScheduleUpdate.ToString("HH:mm")
                    txtIP.Text = terminalObj.IPAddress
                    txtPort.Text = terminalObj.Port
                    setSwitchBlockingMode(terminalObj.BlockingMode)
                    setSwitchUpdateNow(terminalObj.UpdateNow)
                    'terminalFlagBlockingMode.Value = IIf(terminalObj.BlockingMode, 1, 0)
                Else
                    pnlSchedulePerTerminal.Visible = False
                    txtScheduleDate.Text = Now.ToString("dd/MM/yyyy")
                    txtRange1.Text = Now.ToString("HH:mm")
                    txtIP.Text = ""
                    txtPort.Text = ""
                    setSwitchBlockingMode(0)
                    setSwitchUpdateNow(0)
                End If
            Else
                pnlUpdateTerminal.Visible = False
                setSwitchUpdateData(0)
                setSwitchBlockingMode(0)
                setSwitchUpdateNow(0)
            End If

            setSwitchIMEIData(terminalObj.ValidateIMEI)
            terminalIMEIFlag.Value = IIf(terminalObj.ValidateIMEI, 1, 0)
            setSwitchSIMData(terminalObj.ValidateSIM)
            terminalSIMFlag.Value = IIf(terminalObj.ValidateSIM, 1, 0)
            setSwitchStatusData(terminalObj.TerminalStatus)
            terminalFlagStatus.Value = IIf(terminalObj.TerminalStatus = 1, 1, 0)

            'txtGroupName.Text = objSessionParams.strtSelectedGroup

            If terminalObj.Contact.IdentificationNumber.Length > 0 Then
                txtContactNumId.Text = terminalObj.Contact.IdentificationNumber
                ddlContactIdType.SelectedValue = terminalObj.Contact.IdentificationType
                txtContactName.Text = terminalObj.Contact.Name
                txtContactAddress.Text = terminalObj.Contact.Address
                txtContactMobile.Text = terminalObj.Contact.Mobile
                txtContactPhone.Text = terminalObj.Contact.Phone
                txtContactEmail.Text = terminalObj.Contact.Email

                txtContactNumId.Enabled = True
                ddlContactIdType.Enabled = True
                txtContactName.Enabled = True
                txtContactAddress.Enabled = True
                txtContactMobile.Enabled = True
                txtContactPhone.Enabled = True
                txtContactEmail.Enabled = True

                'Set Toggle Visible
                ltrScript.Text &= "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message2').css('z-index', 750); $('#hide-message2').css('display', 'block'); });</script>"

            End If

            txtSerial.Focus()

            dsGroups.SelectParameters("CustomerId").DefaultValue = objAccessToken.CustomerUserID

            ddlGroups.DataBind()

            ddlGroups.SelectedValue = terminalObj.GroupID

        Else
            Dim CtrlID As String = String.Empty

            If Request.Form("__EVENTTARGET") IsNot Nothing And
               Request.Form("__EVENTTARGET") <> String.Empty Then
                CtrlID = Request.Form("__EVENTTARGET")
            End If

            If CtrlID = "terminalFlagUpdateYES" Then
                pnlSchedulePerTerminal.Visible = True

                txtScheduleDate.Text = Now.ToString("dd/MM/yyyy")
                txtRange1.Text = Now.ToString("HH:mm")
                txtIP.Text = ""
                txtPort.Text = ""

            End If

            If CtrlID = "terminalFlagUpdateNO" Then
                pnlSchedulePerTerminal.Visible = False
            End If

            If CtrlID = "terminalFlagPriorityGroupYES" Then
                pnlUpdateTerminal.Visible = False
                pnlSchedulePerTerminal.Visible = False
            End If

            If CtrlID = "terminalFlagPriorityGroupNO" Then
                pnlUpdateTerminal.Visible = True
            End If

            setSwitchPriorityData(terminalFlagPriority.Value)
            setSwitchUpdateData(terminalFlagUpdate.Value)

            setSwitchBlockingMode(terminalFlagBlockingMode.Value)
            setSwitchUpdateNow(terminalFlagUpdateNow.Value)

            setSwitchIMEIData(terminalIMEIFlag.Value)
            setSwitchSIMData(terminalSIMFlag.Value)
            setSwitchInitialize(groupFlagInitialize.Value)
            setSwitchDownloadKey(groupDpwnloadKey.Value)
        End If

        'ANDROID
        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            'GridView Aplicaciones Descargadas
            dsAppsXTerminal.SelectParameters("terminalId").DefaultValue = objSessionParams.intSelectedTerminalID
            dsAppsXTerminal.DataBind()

            'GridView Archivos de Configuración Descargados
            dsXmlsXTerminal.SelectParameters("terminalId").DefaultValue = objSessionParams.intSelectedTerminalID
            dsXmlsXTerminal.DataBind()

            'GridView Archivos de Imagenes Descargados
            dsImgsXTerminal.SelectParameters("terminalId").DefaultValue = objSessionParams.intSelectedTerminalID
            dsImgsXTerminal.DataBind()

            terminalObj = New Terminal(strConnectionString, objSessionParams.intSelectedGroup)

            'Leer datos BD
            terminalObj.TerminalID = objSessionParams.intSelectedTerminalID
            terminalObj.getTerminalData()

            txtSerial.Text = terminalObj.TerminalSerial
            'txtHeraclesId.Text = terminalObj.HeraclesID
            txtAllowedApps.Text = terminalObj.AllowedApps
            txtInstalledApps.Text = IIf(terminalObj.InstalledApps.Length > 0, terminalObj.InstalledApps.Replace(",", vbCrLf), "")

            'txtGroupName.Text = objSessionParams.strtSelectedGroup

            txtKioskoApp.Text = terminalObj.KioskoApp

            'Mostrar el mensaje actual
            txtActualMessage.Text = terminalObj.ActualMessage

            If terminalObj.Contact.IdentificationNumber.Length > 0 Then
                txtContactNumId.Text = terminalObj.Contact.IdentificationNumber
                ddlContactIdType.SelectedValue = terminalObj.Contact.IdentificationType
                txtContactName.Text = terminalObj.Contact.Name
                txtContactAddress.Text = terminalObj.Contact.Address
                txtContactMobile.Text = terminalObj.Contact.Mobile
                txtContactPhone.Text = terminalObj.Contact.Phone
                txtContactEmail.Text = terminalObj.Contact.Email

                txtContactNumId.Enabled = True
                ddlContactIdType.Enabled = True
                txtContactName.Enabled = True
                txtContactAddress.Enabled = True
                txtContactMobile.Enabled = True
                txtContactPhone.Enabled = True
                txtContactEmail.Enabled = True

            End If

            txtSerial.Focus()

            dsGroups.SelectParameters("CustomerId").DefaultValue = objAccessToken.CustomerUserID

            ddlGroups.DataBind()

            ddlGroups.SelectedValue = terminalObj.GroupID

            setSwitchChangePassswordData(0)
            setSwitchLockTerminalsData(0)
            setSwitchReloadKeyData(0)

            'Set Toggle Visible
            ltrScript.Text &= "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); $('#hide-message1').css('z-index', 750); $('#hide-message1').css('display', 'block'); $('#hide-message2').css('z-index', 750); $('#hide-message2').css('display', 'block'); $('#hide-message3').css('z-index', 750); $('#hide-message3').css('display', 'block'); $('#hide-message4').css('z-index', 750); $('#hide-message4').css('display', 'block'); $('#hide-message5').css('z-index', 750); $('#hide-message5').css('display', 'block'); });</script>"

        Else
            Dim CtrlID As String = String.Empty

            If Request.Form("__EVENTTARGET") IsNot Nothing And
               Request.Form("__EVENTTARGET") <> String.Empty Then
                CtrlID = Request.Form("__EVENTTARGET")
            End If

            setSwitchChangePassswordData(changePasswordFlag.Value)
            setSwitchLockTerminalsData(lockTerminalsFlag.Value)
            setSwitchReloadKeyData(reloadKeyFlag.Value)

        End If

        setSliderData()

    End Sub

    Private Sub setSwitchPriorityData(flagPriority As Boolean)
        Dim htmlSwitch As String

        If flagPriority = True Then
            htmlSwitch = "<p class='field switch' id='switchPriority'><label for='radio1' id='radio11' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio2' id='radio12' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='switchPriority'><label for='radio1' id='radio11' class='cb-enable'><span>S&iacute;</span></label><label for='radio2' id='radio12' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrPriority.Text = htmlSwitch

    End Sub

    Private Sub setSwitchUpdateData(flagUpdate As Boolean)
        Dim htmlSwitch As String

        If flagUpdate = True Then
            htmlSwitch = "<p class='field switch' id='switchUpdate'><label for='radio3' id='radio13' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio4' id='radio14' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='switchUpdate'><label for='radio3' id='radio13' class='cb-enable'><span>S&iacute;</span></label><label for='radio4' id='radio14' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrUpdate.Text = htmlSwitch

    End Sub

    Private Sub setSwitchIMEIData(flagImei As Boolean)
        Dim htmlSwitch As String

        If flagImei = True Then
            htmlSwitch = "<p class='field switch' id='switchImei'><label for='radio5' id='radio15' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio6' id='radio16' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='switchImei'><label for='radio5' id='radio15' class='cb-enable'><span>S&iacute;</span></label><label for='radio6' id='radio16' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrIMEI.Text = htmlSwitch

    End Sub

    Private Sub setSwitchSIMData(flagSim As Boolean)
        Dim htmlSwitch As String

        If flagSim = True Then
            htmlSwitch = "<p class='field switch' id='switchSim'><label for='radio7' id='radio17' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio8' id='radio18' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='switchSim'><label for='radio7' id='radio17' class='cb-enable'><span>S&iacute;</span></label><label for='radio8' id='radio18' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrSIM.Text = htmlSwitch

    End Sub

    Private Sub setSwitchStatusData(flagStatus As Integer)
        Dim htmlSwitch As String

        If flagStatus = 1 Then
            htmlSwitch = "<p class='field switch' id='switchStatus'><label for='radio9' id='radio19' class='cb-enable selected'><span>Activa</span></label><label for='radio10' id='radio20' class='cb-disable'><span>Inactiva</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='switchStatus'><label for='radio9' id='radio19' class='cb-enable'><span>Activa</span></label><label for='radio10' id='radio20' class='cb-disable selected'><span>Inactiva</span></label></p>"
        End If

        ltrStatus.Text = htmlSwitch


    End Sub


    Private Sub setSwitchBlockingMode(flagBlockingMode As Boolean)
        Dim htmlSwitch As String

        If flagBlockingMode = True Then
            htmlSwitch = "<p class='field switch' id='switchBlockingMode'><label for='radio11' id='radio21' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio12' id='radio22' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='switchBlockingMode'><label for='radio11' id='radio21' class='cb-enable'><span>S&iacute;</span></label><label for='radio12' id='radio22' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrBlockingMode.Text = htmlSwitch

    End Sub

    Private Sub setSwitchUpdateNow(flagUpdateNow As Boolean)
        Dim htmlSwitch As String

        If flagUpdateNow = True Then
            htmlSwitch = "<p class='field switch' id='switchUpdateNow'><label for='radio11' id='radio23' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio12' id='radio24' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='switchUpdateNow'><label for='radio11' id='radio23' class='cb-enable'><span>S&iacute;</span></label><label for='radio12' id='radio24' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrUpdateNow.Text = htmlSwitch

    End Sub


    Private Sub setSliderData()
        Dim htmlSlider As String = ""
        Dim hour1 As Integer
        Dim minute1 As Integer
        Dim val1, val2 As Integer
        Dim interval As Integer = 15

        hour1 = Integer.Parse(txtRange1.Text.Trim.Split(":")(0))
        minute1 = Integer.Parse(txtRange1.Text.Trim.Split(":")(1))
        val1 = (hour1 * 60) / interval
        val2 = (minute1 Mod 60) / interval

        htmlSlider = "<script language='javascript'>$(function() { $('#slider-range').slider({ range: false, min: 0, max: 95, values: [" & (val1 + val2) & "], slide: function (event, ui) { var interval = 15; var hour1 = (ui.values[0] * interval) / 60;	var minute1 = (ui.values[0] * interval) % 60; $('#ctl00_MainContent_txtRange1').val(zeroPad(parseInt(hour1), 2) + ':' + zeroPad(parseInt(minute1), 2)); }}); });</script>"

        ltrScript.Text &= htmlSlider
    End Sub

    Protected Sub btnQueryContactData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQueryContactData.Click

        'Consultar Datos de contacto existentes
        If txtContactNumId.Text.Length < 8 Then
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Ingrese un número de identificación válido."
            'Set Toggle Visible
            ltrScript.Text &= "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message2').css('z-index', 750); $('#hide-message2').css('display', 'block'); });</script>"
            Exit Sub
        End If

        'Buscar en BD
        getContactData()

        'Set Toggle Visible
        ltrScript.Text &= "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message2').css('z-index', 750); $('#hide-message2').css('display', 'block'); });</script>"

    End Sub



    Protected Sub btnDeletecontactData_Click(sender As Object, e As EventArgs) Handles btnDeletecontactData.Click

        txtContactNumId.Text = ""
        ddlContactIdType.SelectedValue = -1
        txtContactName.Text = ""
        txtContactAddress.Text = ""
        txtContactMobile.Text = ""
        txtContactPhone.Text = ""
        txtContactEmail.Text = ""

        txtContactNumId.Enabled = True
        ddlContactIdType.Enabled = False
        txtContactName.Enabled = False
        txtContactAddress.Enabled = False
        txtContactMobile.Enabled = False
        txtContactPhone.Enabled = False
        txtContactEmail.Enabled = False

        txtContactNumId.Focus()

        'Set Toggle Visible
        ltrScript.Text &= "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message2').css('z-index', 750); $('#hide-message2').css('display', 'block'); });</script>"

    End Sub

    Protected Sub terminalFlagUpdate_ValueChanged(sender As Object, e As EventArgs) Handles terminalFlagUpdate.ValueChanged

    End Sub
    Protected Sub terminalFlagPriority_ValueChanged(sender As Object, e As EventArgs) Handles terminalFlagPriority.ValueChanged

    End Sub








    Private Sub setSwitchChangePassswordData(flagChangePassword As Integer)
        Dim htmlSwitch As String

        If flagChangePassword = 1 Then
            htmlSwitch = "<p class='field switch' id='changePassword'><label for='radio1' id='radio25' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio2' id='radio26' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='changePassword'><label for='radio1' id='radio25' class='cb-enable'><span>S&iacute;</span></label><label for='radio2' id='radio26' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrChangePassword.Text = htmlSwitch

    End Sub

    Private Sub setSwitchLockTerminalsData(flagLockTerminals As Integer)
        Dim htmlSwitch As String

        If flagLockTerminals = 1 Then
            htmlSwitch = "<p class='field switch' id='lockTerminals'><label for='radio3' id='radio27' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio4' id='radio28' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='lockTerminals'><label for='radio3' id='radio27' class='cb-enable'><span>S&iacute;</span></label><label for='radio4' id='radio28' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrLockTerminals.Text = htmlSwitch

    End Sub

    Private Sub setSwitchReloadKeyData(flagReloadKey As Integer)
        Dim htmlSwitch As String

        If flagReloadKey = 1 Then
            htmlSwitch = "<p class='field switch' id='reloadKey'><label for='radio5' id='radio15' class='cb-enable selected'><span>S&iacute;</span></label><label for='radio6' id='radio16' class='cb-disable'><span>No</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='reloadKey'><label for='radio5' id='radio15' class='cb-enable'><span>S&iacute;</span></label><label for='radio6' id='radio16' class='cb-disable selected'><span>No</span></label></p>"
        End If

        ltrReloadKey.Text = htmlSwitch

    End Sub
    Private Sub setSwitchInitialize(flagInitialize As Boolean)
        Dim htmlSwitch As String



        If flagInitialize = True Then
            htmlSwitch = "<p class='field switch' id='switchUpdate'><label for='radio1' id='radio29' class='cb-enable selected'><span>Activar</span></label><label for='radio2' id='radio30' class='cb-disable'><span>Desactivar</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='switchUpdate'><label for='radio1' id='radio29' class='cb-enable'><span>Activar</span></label><label for='radio2' id='radio30' class='cb-disable selected'><span>Desactivar</span></label></p>"
        End If



        ltrInitialize.Text = htmlSwitch

    End Sub
    Private Sub setSwitchDownloadKey(flagDownloadKey As Boolean)
        Dim htmlSwitch As String



        If flagDownloadKey = True Then
            htmlSwitch = "<p class='field switch' id='switchUpdate'><label for='radio1' id='radio89' class='cb-enable selected'><span>Activar</span></label><label for='radio2' id='radio38' class='cb-disable'><span>Desactivar</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='switchUpdate'><label for='radio1' id='radio89' class='cb-enable'><span>Activar</span></label><label for='radio2' id='radio38' class='cb-disable selected'><span>Desactivar</span></label></p>"
        End If



        ltrDownloadKey.Text = htmlSwitch

    End Sub



    Private Sub getContactData()
        Dim connection As New SqlConnection(ConnectionString())
        Dim command As New SqlCommand()
        Dim results As SqlDataReader

        Try
            'Abrir Conexion
            connection.Open()

            'Buscar Datos de Contacto
            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webConsultarDatosContacto"
            command.Parameters.Clear()
            command.Parameters.Add(New SqlParameter("contactNumID", txtContactNumId.Text.Trim))

            'Ejecutar SP
            results = command.ExecuteReader()

            If results.HasRows Then
                While results.Read()
                    txtContactNumId.Text = results.GetString(0)
                    ddlContactIdType.SelectedValue = results.GetInt64(1)
                    txtContactName.Text = results.GetString(2)
                    txtContactAddress.Text = results.GetString(3)
                    txtContactMobile.Text = results.GetString(4)
                    txtContactPhone.Text = results.GetString(5)
                    txtContactEmail.Text = results.GetString(6)

                    txtContactNumId.Enabled = False
                    ddlContactIdType.Enabled = False
                    txtContactName.Enabled = False
                    txtContactAddress.Enabled = False
                    txtContactMobile.Enabled = False
                    txtContactPhone.Enabled = False
                    txtContactEmail.Enabled = False

                End While
            Else
                txtContactNumId.Enabled = True
                ddlContactIdType.Enabled = True
                txtContactName.Enabled = True
                txtContactAddress.Enabled = True
                txtContactMobile.Enabled = True
                txtContactPhone.Enabled = True
                txtContactEmail.Enabled = True
            End If

            connection.Close()
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub



    Protected Sub btnSetActions_Click(sender As Object, e As EventArgs) Handles btnSetActions.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Update")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        terminalObj = New Terminal(strConnectionString, objSessionParams.intSelectedGroup)

        'Establecer Datos de Grupo
        terminalObj.TerminalID = objSessionParams.intSelectedTerminalID
        terminalObj.ChangePasswordTerminal = changePasswordFlag.Value
        terminalObj.NewPasswordTerminal = txtLockPassword.Text
        terminalObj.NewMessageTerminal = txtMessageToDisplay.Text
        terminalObj.ActualMessage = terminalObj.NewMessageTerminal
        terminalObj.LockTerminal = lockTerminalsFlag.Value
        terminalObj.ReloadKey = reloadKeyFlag.Value

        'Save Data
        If terminalObj.ExecuteActionsOnTerminal() Then
            'Actions Executed OK
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Acciones a Ejecutar Actualizadas. Datos[ " & getFormDataLog() & " ]", "")

            pnlMsg.Visible = True
            pnlError.Visible = False
            lblMsg.Text = "Acciones Ejecutadas correctamente."

            'Clear Form
            txtLockPassword.Text = ""
            txtMessageToDisplay.Text = ""
            txtActualMessage.Text = terminalObj.ActualMessage
            setSwitchChangePassswordData(0)
            setSwitchLockTerminalsData(0)
            setSwitchReloadKeyData(0)

        Else
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error ejecutando acciones. Datos[ " & getFormDataLog() & " ]", "")
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error Ejecutando acciones. Por favor valide los datos."
        End If

    End Sub

    Protected Sub grdXmlsXTerminals_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdXmlsXTerminals.RowDataBound

        Dim backgroundColorGreen As String = "#dff0d8"
        Dim backgroundColorRed As String = "#e47a7a"

        If e.Row.RowType = DataControlRowType.DataRow Then

            e.Row.Style("background") = DataBinder.Eval(e.Row.DataItem, "desc_color")
            e.Row.Cells(0).ForeColor = IIf(DataBinder.Eval(e.Row.DataItem, "desc_color") = backgroundColorGreen, Color.Gray, Color.White)
            e.Row.Cells(1).ForeColor = IIf(DataBinder.Eval(e.Row.DataItem, "desc_color") = backgroundColorGreen, Color.Gray, Color.White)

        End If

    End Sub

    Protected Sub grdImgsXTerminals_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdImgsXTerminals.RowDataBound

        Dim backgroundColorGreen As String = "#dff0d8"
        Dim backgroundColorRed As String = "#e47a7a"

        If e.Row.RowType = DataControlRowType.DataRow Then

            e.Row.Style("background") = DataBinder.Eval(e.Row.DataItem, "desc_color")
            e.Row.Cells(0).ForeColor = IIf(DataBinder.Eval(e.Row.DataItem, "desc_color") = backgroundColorGreen, Color.Gray, Color.White)
            e.Row.Cells(1).ForeColor = IIf(DataBinder.Eval(e.Row.DataItem, "desc_color") = backgroundColorGreen, Color.Gray, Color.White)

        End If

    End Sub

    Protected Sub grdAppsXTerminal_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdAppsXTerminal.RowDataBound

        Dim backgroundColorGreen As String = "#dff0d8"
        Dim backgroundColorRed As String = "#e47a7a"

        If e.Row.RowType = DataControlRowType.DataRow Then

            e.Row.Style("background") = DataBinder.Eval(e.Row.DataItem, "desc_color")
            e.Row.Cells(0).ForeColor = IIf(DataBinder.Eval(e.Row.DataItem, "desc_color") = backgroundColorGreen, Color.Gray, Color.White)
            e.Row.Cells(1).ForeColor = IIf(DataBinder.Eval(e.Row.DataItem, "desc_color") = backgroundColorGreen, Color.Gray, Color.White)
            e.Row.Cells(2).ForeColor = IIf(DataBinder.Eval(e.Row.DataItem, "desc_color") = backgroundColorGreen, Color.Gray, Color.White)
            e.Row.Cells(3).ForeColor = IIf(DataBinder.Eval(e.Row.DataItem, "desc_color") = backgroundColorGreen, Color.Gray, Color.White)

        End If

    End Sub
End Class









