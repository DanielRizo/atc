﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Applications
Imports System.Data.Common

Partial Class Groups_EditTerminalstis
    Inherits TeleLoader.Web.BasePage

    Dim applicationObj As applicationSTIS2

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            txtRecord.Focus()
            dsTerminalsParameter.SelectParameters("TERMINAL_REC_NO").DefaultValue = objSessionParams.StrTerminalRecord

            grdTerminalsParameter.DataBind()

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub
    Protected Sub grdTerminals_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdTerminalsParameter.RowCommand
        Try

            Select Case e.CommandName

                Case "EditTerminalParameter"
                    grdTerminalsParameter.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.StrTerminalRecord = grdTerminalsParameter.SelectedDataKey.Values.Item("TERMINAL_REC_NO")
                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("EditParameter.aspx", False)
            End Select
            If txtRecord.Text = "" Then
                'Set Invisible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
            Else
                'Set Visible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
            End If
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub
    'Edicion de Parametros por Terminal Stis 
    'Autor : Oscar Gutierrez
    Protected Sub grdTerminalsParameter_Updated(sender As Object, e As GridViewUpdatedEventArgs) Handles grdTerminalsParameter.RowUpdated

        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webEditTerminalParameter_Stis"
            command.Parameters.Clear()


            command.Parameters.Add(New SqlParameter("TERMINAL_REC_NO", e.OldValues("TERMINAL_REC_NO")))
            command.Parameters.Add(New SqlParameter("FIELD_DISPLAY_NAME", e.OldValues("FIELD_DISPLAY_NAME")))
            command.Parameters.Add(New SqlParameter("CONTENT_DESC", e.NewValues("CONTENT_DESC")))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try

    End Sub
    Protected Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click
        If txtRecord.Text <> "" Then
            dsTerminalsParameter.SelectParameters("TERMINAL_REC_NO").DefaultValue = txtRecord.Text
        Else
            dsTerminalsParameter.SelectParameters("TERMINAL_REC_NO").DefaultValue = "-1"
        End If
        grdTerminalsParameter.DataBind()

        If txtRecord.Text = "" Then
            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
        Else
            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
        End If

    End Sub
    Private Sub clearForm()
        txtRecord.Text = ""
    End Sub
End Class
