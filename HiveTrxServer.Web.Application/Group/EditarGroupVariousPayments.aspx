﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageCSS.master" AutoEventWireup="false" CodeFile="EditarGroupVariousPayments.aspx.vb" Inherits="Groups_EditarGroupVariousPayments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">

    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Editar Grupo Pagos Varios
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

    <%--scripts edit table--%>

    <link rel="stylesheet" type="text/css" href="/style/bootstrap.min.css" />


    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/editTable.js") %>'></script>

    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.tabledit.js") %>'></script>

    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/bootstrap.js") %>'></script>

    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/moveElementsList.js") %>'></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

    <style>
        .detailPrompt {
            background-color: #f2f7fc;
            border: 1px solid black;
            padding: 2px;
            margin: 2px;
        }
    </style>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>

    <li>
        <asp:LinkButton ID="LinkButton1" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/TerminalStis.aspx">Terminales</asp:LinkButton>
    </li>

    <li>
        <asp:LinkButton ID="LinkButton2" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/ListarGroupVariousPayments.aspx">Listar Grupo Pagos Varios</asp:LinkButton>
    </li>

    <li>Editar Grupo Pagos Varios...</li>
</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <blockquote>
        <p>Este módulo permite ediar los Grupos (Pagos Varios):</p>
        <footer class="blockquote-footer">Polaris Cloud Service (Pstis) </footer>
    </blockquote>
    <style>
        .bg-1 {
            background-color: #1abc9c;
            color: #ffffff;
        }
    </style>

    <div class="bg-1">
        <div class="container text-center">
            <h3>Parametros por Emv Application Pstis</h3>
            <img src="https://cdn3.vectorstock.com/i/1000x1000/08/92/money-protection-flat-icon-vector-13340892.jpg" class="img-circle" alt="Bird" width="350" height="300">
            <img src="https://www.creativefabrica.com/wp-content/uploads/2019/01/Online-payment-icon-by-back1design1-1.png" class="img-circle" alt="Bird" width="350" height="300">
        </div>
    </div>

    <div class="container-fluid bg-2 text-center">
        <h3>Edicion de Grupos (Pagos Varios)</h3>
        <p>Este modulo permite editar Los grupos de pagos Varios. </p>
    </div>

    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">

                    <ul id="navst">
                        <li class="navbar navbar-light"><a href="TerminalStis.aspx">PSTIS</a></li>
                        <li><a>Tables</a>
                            <ul>
                                <li><a href="TerminalAcquirer.aspx">Acquirer</a></li>
                                <li><a href="TerminalIssuer.aspx">Issuer</a>	</li>
                                <li><a href="TerminalCardRange.aspx">CardRange</a></li>
                                <li><a href="TerminalEmvLevel2.aspx">Emv Level 2 Application</a></li>
                                <li><a href="TerminalEmvKey.aspx">Emv Level 2 Key</a></li>
                                <li><a href="TerminalExtraApplication.aspx">Extra Application Parameter</a></li>
                                <li><a href="ListarGroupPrompts.aspx">Grupos Prompts</a></li>
                                <li><a href="ListarGroupVariousPayments.aspx">Grupos Pagos Varios</a></li>
                                <li><a href="ListarGroupElectronicsPayments.aspx">Grupos Pagos Electronicos</a></li>
                                <li><a href="TerminalHostConfiguration.aspx">Host Configuration</a></li>
                                <li><a href="TerminalPrompts.aspx">Prompts</a></li>
                                <li><a href="PagosVarios.aspx">Pagos Varios</a></li>
                                <li><a href="PagosElectronicos.aspx">Pagos Electronicos</a></li>
                                <li><a href="IP.aspx">Ip</a></li>
                                <li><a href="AddCertificado.aspx">Certificados SSL</a></li>
                            </ul>
                        </li>
                        <li><a>Terminal</a>
                            <ul>
                                <li><a href="AddTerminalStis.aspx">New Terminal</a></li>
                                <li><a href="CopyTerminalStis.aspx">Copy Terminal</a></li>

                            </ul>
                        </li>
                    </ul>
                </div>
            </div>

        </div>

    </div>


    <div id="alertCampos" class="alert alert-danger">
        <div>Debe llenar los campos requeridos.</div>
    </div>
    <script> $("#alertCampos").hide();</script>

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
        <div id="succes2" style="display: none;">
            <asp:Panel ID="pnlMsg" runat="server">
                <div class="alert alert-success">
                    <asp:Label ID="lblMsg2" runat="server" Text="Datos editados correctamente."></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">x</a>
                </div>
            </asp:Panel>
        </div>

        <div id="error2" style="display: none;">
            <asp:Panel ID="pnlError" runat="server">
                <div class="alert alert-danger">
                    <asp:Label ID="lblError2" runat="server" Text="Error editando datos del grupo."></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">x</a>
                </div>
            </asp:Panel>
        </div>

    </div>

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <div>
            <h3 class="title">GRUPO PAGOS VARIOS
                <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>

        </div>
        <div class="panel panel-default" style="background-color: #fff; width: auto;">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-2">


                        <div class="" id="Code">
                            <span class="st-labeltext">Code: (*)</span>
                            <input id="txtCode" type="text" <%--disabled="disabled"--%> />

                            <div class="clear"></div>
                        </div>
                        <div class="">
                            <span class="st-labeltext">Display Name: (*)</span>
                            <input id="txtName" type="text" maxlength="49" />
                            <div class="clear"></div>
                        </div>



                    </div>

                    <div class="col-sm-4" style="padding-left: 4%">
                        <select name="from" id="multiselect_left" class="form-control" size="15" multiple="multiple">
                            <optgroup label="Lista Pagos Varios">
                            </optgroup>
                            <%--    <optgroup label="Optional Fields">
                             <option value=8>Marital Status</option>
                             <option value=9>Mother Name</option>
                             <option value=10>Spouse Name</option>
                             <option value=11>Highest Education</option>
                             </optgroup>--%>
                        </select>
                        <div class="detailPrompt" id="detailPromptLeft"></div>

                    </div>
                    <script type="text/javascript">
                        $("#alertCampos").hide();

                        $(document).ready(function () {
                            $.ajax({
                                type: "POST",
                                url: "wsadd.asmx/" + "getPagosVariosFaltantes",
                                data: '{"data":"' + "" + '"}',
                                contentType: "application/Json; charset=utf-8",
                                dataType: 'Json',
                                success: function (r) {
                                    // alert(r.stringify());
                                    var JsonRsp = JSON.parse(r.d);
                                    // alert(JsonRsp.registroPrompts[1].Codigo);
                                    for (i in JsonRsp.registroPagosVarios) {

                                        $('#multiselect_left').append('<option value="' + JsonRsp.registroPagosVarios[i].Codigo + '">' + JsonRsp.registroPagosVarios[i].PagosVarios + '</option>');
                                    }
                                },
                                error: function (r) {

                                },
                                failure: function (r) {

                                }
                            });
                        });     </script>

                    <div class="col-sm-1">
                        <button type="button" id="btn_rightAll" class="btn btn-info">
                            <img src="../img/fastforward.png" alt="rightAll" /></button><br />
                        <br />
                        <button type="button" id="btn_rightSelected" class="btn btn-info">
                            <img src="../img/right.png" alt="rightSelected" /></button><br />
                        <br />
                        <button type="button" id="btn_leftSelected" class="btn btn-info">
                            <img src="../img/left.png" alt="leftSelected" /></button><br />
                        <br />
                        <button type="button" id="btn_leftAll" class="btn btn-info">
                            <img src="../img/fastbackward.png" alt="leftAll" /></button><br />
                        <br />
                    </div>

                    <div class="col-sm-4">
                        <select name="from" id="multiselect_right" class="form-control" size="15" multiple="multiple">
                            <optgroup label="Grupo Pagos Varios" id="Lista">
                            </optgroup>

                        </select>
                        <div class="detailPrompt" id="detailPromptRight"></div>
                    </div>

                </div>

            </div>


            <script type="text/javascript">
                $(document).ready(function () {
                    $.ajax({
                        type: "POST",
                        url: "wsadd.asmx/" + "getPagosVariosExistentes",
                        data: '{"data":"' + "" + '"}',
                        contentType: "application/Json; charset=utf-8",
                        dataType: 'Json',
                        success: function (r) {
                            // alert(r.stringify());
                            var JsonRsp = JSON.parse(r.d);
                            // alert(JsonRsp.registroPrompts[1].Codigo);
                            for (i in JsonRsp.registroPagosVarios) {

                                $('#multiselect_right').append('<option value="' + JsonRsp.registroPagosVarios[i].Codigo + '">' + JsonRsp.registroPagosVarios[i].PagosVarios + '</option>');
                                document.getElementById("txtCode").value = JsonRsp.registroPagosVarios[i].CodigoGrupo.trim();
                                document.getElementById("txtName").value = JsonRsp.registroPagosVarios[i].NameGrupo.trim();
                            }
                        },
                        error: function (r) {

                        },
                        failure: function (r) {

                        }
                    });
                });     </script>


            <div class="button-box">

                <button id="ButGuardarGrupo" class="btn btn-info">Editar Grupo</button>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $('#ButGuardarGrupo').on('click', function (event) {
            var Ids = "";
            var cont = 0;
            $("#txtCode").css("background", "White");
            $("#txtName").css("background", "White");
            $("#multiselect_right option").each(function () {
                Ids = Ids + "@" + $(this).val();
                cont = 1;

            });
            if (/*cont == 1 &&*/ (!document.getElementById("txtCode").value == "") && (!document.getElementById("txtName").value == "")) {
                Ids = Ids + "@" + document.getElementById("txtCode").value + "@" + document.getElementById("txtName").value;
                event.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "wsadd.asmx/" + "EditGroupPagosVarios",
                    data: '{"data":"' + Ids + '"}',
                    contentType: "application/Json; charset=utf-8",
                    dataType: 'Json',
                    success: function (r) {
                        document.getElementById("succes2").style.display = "inline";
                        setTimeout('window.history.back()', 3000);

                    },
                    error: function (r) {
                        document.getElementById("error2").style.display = "inline";
                        setTimeout('document.location.reload()', 3000);
                    },
                    failure: function (r) {

                    }
                });
            }
            else {
                $("#alertCampos").fadeTo(0, 500)

                if (document.getElementById("txtName").value == "") {
                    $("#txtName").css("background", "Pink");
                }
                if (document.getElementById("txtCode").value == "") {
                    $("#txtCode").css("background", "Pink");
                }
                window.setTimeout(function () {
                    $("#alertCampos").fadeTo(500, 0).slideUp(500, function () {
                    });
                }, 3000);
                event.preventDefault();
            }


        });     </script>



    <div class="body">
    </div>
    <script src='<%= ResolveClientUrl("~/js/showDetailDataPrompts.js") %>'>
    </script>
    <asp:HyperLink ID="HyperLink1" runat="server"
        NavigateUrl="~/Group/ListarGroupVariousPayments.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>


<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

    <script language="javascript">
        var globalcontrol = '';

        function ConfirmAction(control) {

            globalcontrol = control;

            $.msgAlert({
                type: "warning"
                , title: "Mensaje del sistema"
                , text: "Realmente desea eliminar el adquiriente?"
                , callback: function () {
                    __doPostBack($(globalcontrol).attr('name'), '');
                }
            });

            return false;
        }
    </script>

</asp:Content>

