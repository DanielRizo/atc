﻿Imports System.IO
Imports TeleLoader.Sessions
Imports System.Net
Imports WinSCP
Imports log4net

Partial Class Group_FileUploader
    Inherits System.Web.UI.Page
    Private Function FtpUploadFile(ByVal filetoupload As String, ByVal ftpuri As String, ByVal ftpusername As String, ByVal ftppassword As String) As Boolean
        ' Create a web request that will be used to talk with the server and set the request method to upload a file by ftp.
        Dim ftpRequest As FtpWebRequest = CType(WebRequest.Create(ftpuri), FtpWebRequest)

        Try
            ftpRequest.Method = WebRequestMethods.Ftp.UploadFile

            ' Confirm the Network credentials based on the user name and password passed in.
            ftpRequest.Credentials = New NetworkCredential(ftpusername, ftppassword)

            ' Read into a Byte array the contents of the file to be uploaded 
            Dim bytes() As Byte = System.IO.File.ReadAllBytes(filetoupload)

            ' Transfer the byte array contents into the request stream, write and then close when done.
            ftpRequest.ContentLength = bytes.Length
            Using UploadStream As Stream = ftpRequest.GetRequestStream()
                UploadStream.Write(bytes, 0, bytes.Length)
                UploadStream.Close()
            End Using

            Return True

        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function sFtpUploadFile(ByVal filetoupload As String, ByVal sftpIP As String, ByVal sftpUSER As String, ByVal sftpPASS As String, sftpFINGERPRINT As String) As Boolean
        Try

            ' Setup session options
            Dim sessionOptions As New SessionOptions
            With sessionOptions
                .Protocol = Protocol.Sftp
                .HostName = sftpIP
                .UserName = sftpUSER
                .Password = sftpPASS
                .SshHostKeyFingerprint = sftpFINGERPRINT
            End With

            Using session As New Session
                ' Connect
                session.Open(sessionOptions)

                ' Upload file
                Dim transferOptions As New TransferOptions
                transferOptions.TransferMode = TransferMode.Automatic

                Dim transferResult As TransferOperationResult
                transferResult = session.PutFiles(filetoupload, "/*.*", False, transferOptions)

                ' Throw on any error
                transferResult.Check()

            End Using

            Return True

        Catch ex As Exception
            Return False
        End Try
    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then

            Dim objSessionParams As SessionParameters = Session("SessionParameters")

            'save file to temp folder
            Try

                Dim strDirectorio As String = ""
                Dim strNombreFile As String = ""
                'FTP Params
                Dim strUseFTP As String = ""
                Dim strFtpIP As String = ""
                Dim strFtpUSER As String = ""
                Dim strFtpPASSWORD As String = ""
                'sFTP Params
                Dim strUseSFTP As String = ""
                Dim strsFtpIP As String = ""
                Dim strsFtpUSER As String = ""
                Dim strsFtpPASSWORD As String = ""
                Dim strsFtpFINGERPRINT As String = ""

                '0. Validate Uploaded File
                If (Request.Files(0).FileName = "" Or Request.Files(0) Is Nothing) Then
                    Response.ClearHeaders()
                    Response.ClearContent()
                    Response.StatusCode = 500
                    Response.StatusDescription = "Internal Error"
                    Response.Write("error")
                    Return
                End If

                If (Not (Request.Files(0).FileName.ToLower().Contains(".tms") Or Request.Files(0).FileName.ToLower().Contains(".zip") Or Request.Files(0).FileName.ToLower().Contains(".gz"))) Then
                    Response.ClearHeaders()
                    Response.ClearContent()
                    Response.StatusCode = 500
                    Response.StatusDescription = "Internal Error"
                    Response.Write("error")
                    Return
                End If

                '1. Upload File / Delete it If exists
                strDirectorio = ConfigurationSettings.AppSettings.Get("TempFolder")
                strNombreFile = Request.Files(0).FileName

                If Directory.Exists(strDirectorio) = False Then ' si no existe la carpeta se crea
                    Directory.CreateDirectory(strDirectorio)
                End If

                'Delete it If exists
                If File.Exists(strDirectorio + Request.Files(0).FileName) Then
                    File.Delete(strDirectorio + Request.Files(0).FileName)
                    File.Delete(strDirectorio + Request.Files(0).FileName)
                End If


                Request.Files(0).SaveAs(strDirectorio + Request.Files(0).FileName)

                'Check For FTP Connection
                strUseFTP = ConfigurationSettings.AppSettings.Get("UseFTP")

                If strUseFTP.ToUpper = "YES" Then
                    strFtpIP = ConfigurationSettings.AppSettings.Get("FtpIP")
                    strFtpUSER = ConfigurationSettings.AppSettings.Get("FtpUSER")
                    strFtpPASSWORD = ConfigurationSettings.AppSettings.Get("FtpPASSWORD")

                    If Not FtpUploadFile(strDirectorio + Request.Files(0).FileName, strFtpIP + Request.Files(0).FileName, strFtpUSER, strFtpPASSWORD) Then
                        Throw New Exception("FTP Error")
                    End If
                End If

                'Check For sFTP Connection
                strUseSFTP = ConfigurationSettings.AppSettings.Get("UseSFTP")

                If strUseSFTP.ToUpper = "YES" Then
                    strsFtpIP = ConfigurationSettings.AppSettings.Get("sFtpIP")
                    strsFtpUSER = ConfigurationSettings.AppSettings.Get("sFtpUSER")
                    strsFtpPASSWORD = ConfigurationSettings.AppSettings.Get("sFtpPASSWORD")
                    strsFtpFINGERPRINT = ConfigurationSettings.AppSettings.Get("sFtpFINGERPRINT")

                    If Not sFtpUploadFile(strDirectorio + Request.Files(0).FileName, strsFtpIP, strsFtpUSER, strsFtpPASSWORD, strsFtpFINGERPRINT) Then
                        Throw New Exception("sFTP Error")
                    End If
                End If

                'Set FileName into Session                
                objSessionParams.strAppFilename = Request.Files(0).FileName

                Response.Write(Request.Files(0).FileName)

            Catch ex As Exception

                objSessionParams.strAppFilename = ""
                Response.Write(ex.Message)
            End Try

        End If
    End Sub
End Class
