﻿Imports System.IO
Imports TeleLoader.Sessions
Imports System.Net
Imports WinSCP
Imports System.Diagnostics

Partial Class Group_FileUploaderAndroidAPK
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then

            Dim objSessionParams As SessionParameters = Session("SessionParameters")
            Dim outputAAPT As String = ""

            'Save app file to group folder
            Try

                Dim strDirectorio As String = ""
                Dim strNombreFile As String = ""

                '0. Validate Uploaded File
                If (Request.Files(0).FileName = "" Or Request.Files(0) Is Nothing) Then
                    Response.ClearHeaders()
                    Response.ClearContent()
                    Response.StatusCode = 500
                    Response.StatusDescription = "Internal Error"
                    Response.Write("error")
                    Return
                End If

                '0. Validate Uploaded File Extension
                If (Not Request.Files(0).FileName.ToLower().Contains(".apk")) Then
                    Response.ClearHeaders()
                    Response.ClearContent()
                    Response.StatusCode = 500
                    Response.StatusDescription = "Internal Error"
                    Response.Write("error")
                    Return
                End If

                '1. Upload File / Delete it If exists
                strDirectorio = ConfigurationSettings.AppSettings.Get("TempFolder")
                strNombreFile = Request.Files(0).FileName

                If Directory.Exists(strDirectorio) = False Then ' si no existe la carpeta se crea
                    Directory.CreateDirectory(strDirectorio)
                End If

                'Delete it If exists
                If File.Exists(strDirectorio + Request.Files(0).FileName) Then
                    File.Delete(strDirectorio + Request.Files(0).FileName)
                    File.Delete(strDirectorio + Request.Files(0).FileName)
                End If

                Request.Files(0).SaveAs(strDirectorio + Request.Files(0).FileName)

                'Get PackageName and VersionName from uploaded APK
                Try

                    Dim process As New Process()
                    process.StartInfo.FileName = Server.MapPath(ConfigurationSettings.AppSettings.Get("AAPT_ExePath"))
                    process.StartInfo.Arguments = "d badging """ & strDirectorio & Request.Files(0).FileName & """"
                    process.StartInfo.UseShellExecute = False
                    process.StartInfo.RedirectStandardOutput = True
                    process.Start()

                    ' Synchronously read the standard output of the spawned process. 
                    Dim reader As StreamReader = process.StandardOutput
                    outputAAPT = reader.ReadToEnd()

                    process.WaitForExit()
                    process.Close()

                Catch ex As Exception

                End Try

                'Set FileName into Session                
                objSessionParams.strAppFilename = Request.Files(0).FileName

                Response.Write(Request.Files(0).FileName & "|" & GetPackageNameAndVersionName(outputAAPT))

            Catch ex As Exception
                objSessionParams.strAppFilename = ""
                Response.Write(ex.Message)
            End Try

        End If
    End Sub

    Private Function GetPackageNameAndVersionName(ByVal data As String) As String

        Dim retVal As String = ""

        'Extract PackageName and VersionName
        Dim lines As String() = data.Split(New String() {Environment.NewLine}, StringSplitOptions.None)
        Dim tokens As String() = lines(0).Split(" ")
        retVal = tokens(1).Substring(tokens(1).IndexOf("'") + 1, tokens(1).Length - tokens(1).IndexOf("'") - 2) + "|" + tokens(3).Substring(tokens(3).IndexOf("'") + 1, tokens(3).Length - tokens(3).IndexOf("'") - 2)

        Return retVal

    End Function

End Class
