﻿Imports System.IO
Imports TeleLoader.Sessions
Imports System.Net
Imports WinSCP

Partial Class Group_FileUploaderAndroidZIPs
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then

            Dim objSessionParams As SessionParameters = Session("SessionParameters")

            'Save app file to group folder
            Try

                Dim strDirectorio As String = ""
                Dim strNombreFile As String = ""

                '0. Validate Uploaded File
                If (Request.Files(0).FileName = "" Or Request.Files(0) Is Nothing) Then
                    objSessionParams.strAppFilename = ""
                    Response.ClearHeaders()
                    Response.ClearContent()
                    Response.StatusCode = 500
                    Response.StatusDescription = "Internal Error"
                    Response.Write("error")
                    Return
                End If

                '0. Validate Uploaded File Extension
                If (Not Request.Files(0).FileName.ToLower().Contains(".zip")) Then
                    objSessionParams.strAppFilename = ""
                    Response.ClearHeaders()
                    Response.ClearContent()
                    Response.StatusCode = 500
                    Response.StatusDescription = "Internal Error"
                    Response.Write("error")
                    Return
                End If

                '1. Upload File / Delete it If exists
                strDirectorio = ConfigurationSettings.AppSettings.Get("TempFolder")
                strNombreFile = Request.Files(0).FileName

                If Directory.Exists(strDirectorio) = False Then ' si no existe la carpeta se crea
                    Directory.CreateDirectory(strDirectorio)
                End If

                'Delete it If exists
                If File.Exists(strDirectorio + Request.Files(0).FileName) Then
                    File.Delete(strDirectorio + Request.Files(0).FileName)
                    File.Delete(strDirectorio + Request.Files(0).FileName)
                End If

                Request.Files(0).SaveAs(strDirectorio + Request.Files(0).FileName)

                'Set FileName into Session                
                objSessionParams.strAppFilename = Request.Files(0).FileName

                Response.Write(Request.Files(0).FileName)

            Catch ex As Exception
                objSessionParams.strAppFilename = ""
                Response.Write(ex.Message)
            End Try

        End If
    End Sub
End Class
