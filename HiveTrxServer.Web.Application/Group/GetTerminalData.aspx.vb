﻿Imports TeleLoader.Terminals

Partial Class Group_GetTerminalData
    Inherits TeleLoader.Web.BasePage

    Dim terminalObj As Terminal

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then

            'Get Terminal Data
            Try

                terminalObj = New Terminal(strConnectionString, objSessionParams.intSelectedGroup)

                'Leer datos BD
                terminalObj.TerminalID = Integer.Parse(Request.QueryString("terminalID"))
                terminalObj.getTerminalData()

                Dim logo As String = ""

                Select Case terminalObj.TerminalModel()
                    Case 1
                        'T700
                        logo = "../img/spectraT700.png"
                    Case 2
                        'T800
                        logo = "../img/spectraT800.png"
                    Case 3, 5
                        'T1000, A5-T1000
                        logo = "../img/spectraT1000.png"
                    Case 4
                        'CREON
                    Case 6
                        'Android POS - i80 Sunyard
                        logo = "../img/i80.png"                        
                    Case 20
                        'Modificación MDM; EB: 30/Ene/2018
                        'Android POS - 9220 NewPos
                        logo = "../img/NewPOS9220.png"
                End Select

                'Modificación MDM; EB: 30/Ene/2018
                If Not terminalObj.isAndroidGroup Then
                    'Salida HTML POS Tradicionales
                    Response.Write("<table><tr><td align=center><img src='" & logo & "'/></td><td>&nbsp;&nbsp;&nbsp;</td><td><b>Fecha Reporte: </b><br/>" & IIf(terminalObj.DatetimeAutoUpdate.Year = 2001, "-", terminalObj.DatetimeAutoUpdate) & "<br/><b>IMEI: </b><br/>" & terminalObj.IMEI & "<br/><b>SIM: </b><br/>" & terminalObj.SerialSIM & "<br/><b>Nivel Señal: </b><br/>" & terminalObj.SignalLevel & "%<br/><b>Nivel Batería: </b><br/>" & terminalObj.BatteryLevel & "%<br/><b>APN: </b><br/>" & terminalObj.APN & "<br/></td></tr></table>")
                Else
                    'Salida HTML MDM Android
                    Response.Write("<table><tr><td align=center><img src='" & logo & "'/></td><td>&nbsp;&nbsp;&nbsp;</td><td><b>Última Conexión: </b><br/>" & IIf(terminalObj.DatetimeAutoUpdate.Year = 2001, "-", terminalObj.DatetimeAutoUpdate) & "<br/><b>Heracles ID: </b><br/>" & terminalObj.HeraclesID & "<br/><b>Dirección IP: </b><br/>" & terminalObj.DeviceIP & "<br/><b>Nombre: </b><br/>" & terminalObj.HeraclesName & "<br/><b>Ciudad: </b><br/>" & terminalObj.HeraclesCity & "<br/><b>Región: </b><br/>" & terminalObj.HeraclesRegion & "<br/></td></tr></table>")
                End If


            Catch ex As Exception
                Response.Write(ex.Message)
            End Try

        End If
    End Sub
End Class
