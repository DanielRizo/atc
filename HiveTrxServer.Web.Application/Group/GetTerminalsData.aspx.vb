﻿Imports TeleLoader.Groups

Partial Class Group_GetTerminalsData
    Inherits TeleLoader.Web.BasePage

    Dim groupObj As Group

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim outputJS As String = ""

        outputJS = "[" & vbCrLf

        If Not IsPostBack Then

            groupObj = New Group(strConnectionString, objSessionParams.intSelectedGroup)

            'Leer datos BD
            groupObj.getTerminalsLocationData()

            If Not groupObj.TerminalsLocation Is Nothing Then

                'Validar Número de Puntos
                If groupObj.TerminalsLocation.Count > 0 Then
                    'Crear Javascript marker y posición para Google Map

                    'Recorrer Puntos de Marcadores
                    For Each sItem As String() In groupObj.TerminalsLocation
                        outputJS &= "[" & sItem(0) & ", '" & sItem(1) & "', " & sItem(2) & ", " & sItem(3) & ", '" & sItem(4) & "']," & vbCrLf
                    Next

                End If

            End If

        End If

        outputJS &= "]" & vbCrLf

        Response.Write(outputJS)

    End Sub
End Class
