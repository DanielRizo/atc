﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Applications
Imports System.Data.Common
Imports AjaxControlToolkit

Partial Class Groups_GroupAndroidApplications
    Inherits TeleLoader.Web.BasePage

    Dim fileDeployedObj As FileDeployedAndroid

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            dsDeployedFiles.SelectParameters("groupId").DefaultValue = objSessionParams.intSelectedGroup
            dsDeployedFiles.SelectParameters("deployTypeId").DefaultValue = 1 'Fixed Data = APK's

            grdDeployedFiles.DataBind()

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub grdDeployedFiles_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdDeployedFiles.RowCommand
        Try

            Select Case e.CommandName

                Case "Delete"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    grdDeployedFiles.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    dsDeployedFiles.DeleteParameters("deployedFileId").DefaultValue = grdDeployedFiles.SelectedDataKey.Values.Item("desp_id")
                    dsDeployedFiles.DeleteParameters("groupId").DefaultValue = objSessionParams.intSelectedGroup

                    Dim strData As String = "Archivo ID: " & grdDeployedFiles.SelectedDataKey.Values.Item("desp_id") & ", grupo ID: " & objSessionParams.intSelectedGroup

                    dsDeployedFiles.Delete()

                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Archivo eliminado del Grupo. Datos[ " & strData & " ]", "")

                    dsDeployedFiles.DataBind()

                    pnlError.Visible = False
                    pnlMsg.Visible = True
                    lblMsg.Text = "Aplicación eliminada del Grupo"

                    'Delete Physical File
                    Dim filePath = Server.MapPath(ConfigurationSettings.AppSettings.Get("GroupsFolderPath") + objSessionParams.strtSelectedGroup + "/" + grdDeployedFiles.Rows(grdDeployedFiles.SelectedIndex).Cells(1).Text)

                    If File.Exists(filePath) Then
                        File.Delete(filePath)
                        File.Delete(filePath)
                    End If

                    grdDeployedFiles.SelectedIndex = -1

            End Select

            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub

    Protected Sub btnAddApplication_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddApplication.Click

        Dim strDirectorio As String = ""
        Dim strNombreFile As String = ""

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Save")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        strDirectorio = ConfigurationSettings.AppSettings.Get("GroupsFolderPath") + objSessionParams.strtSelectedGroup

        'Validar que se haya subido un archivo
        If (objSessionParams.strAppFilename.Equals("")) Then
            pnlMsg.Visible = False
            pnlError.Visible = True
            lblError.Text = "Debe seleccionar un archivo de aplicación para subir al grupo."
            'Set Toggle Visible
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
            Return
        End If

        Try
            fileDeployedObj = New FileDeployedAndroid(strConnectionString, objSessionParams.intSelectedGroup)

            'Establecer Datos de la Aplicación
            fileDeployedObj.Filename = objSessionParams.strAppFilename
            fileDeployedObj.PackageName = txtPackageNameHidden.Value
            fileDeployedObj.Version = txtPackageVersionHidden.Value
            fileDeployedObj.DeployTypeID = 1 'Fixed Data = APK

            'Save Data
            If fileDeployedObj.createDeployedFileGroupAndroid() Then
                'New Application Group Created OK
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Aplicación desplegada en el grupo. Datos[ " & getFormDataLog() & " ]", "")

                pnlMsg.Visible = True
                pnlError.Visible = False
                lblMsg.Text = "Aplicación desplegada en el grupo correctamente."

                dsDeployedFiles.SelectParameters("groupId").DefaultValue = objSessionParams.intSelectedGroup
                dsDeployedFiles.SelectParameters("deployTypeId").DefaultValue = 1 'Fixed Data = APK's

                grdDeployedFiles.DataBind()

                clearForm()

                'Copiar Archivo Físico a la carpeta del Grupo
                Try
                    Dim path As String
                    path = Server.MapPath(ConfigurationSettings.AppSettings.Get("GroupsFolderPath") + objSessionParams.strtSelectedGroup)

                    If (Not System.IO.Directory.Exists(path)) Then
                        System.IO.Directory.CreateDirectory(path)
                    End If

                    File.Copy(ConfigurationSettings.AppSettings.Get("TempFolder") + objSessionParams.strAppFilename, Server.MapPath(ConfigurationSettings.AppSettings.Get("GroupsFolderPath") + objSessionParams.strtSelectedGroup + "/" + objSessionParams.strAppFilename))
                Catch ex As Exception
                    pnlMsg.Visible = False
                    pnlError.Visible = True
                    lblError.Text = "Error subiendo archivo, por favor reinicie el proceso."
                    'Set Toggle Visible
                    ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
                    Return
                End Try

            Else
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error desplegando aplicación en el Grupo. Aplicación ya Existe. Datos[ " & getFormDataLog() & " ]", "")
                pnlError.Visible = True
                pnlMsg.Visible = False
                lblError.Text = "Error desplegando aplicación en el Grupo. el Package " + "(" + fileDeployedObj.PackageName + ")" + " Esta repetido. Por favor elimine antes la aplicación deseada."

            End If

            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub

    Private Sub clearForm()

    End Sub

    Protected Sub dsDeployedFiles_Deleting(sender As Object, e As SqlDataSourceCommandEventArgs) Handles dsDeployedFiles.Deleting

        'Remover parametro extra, igual al datakeyname
        If e.Command.Parameters.Count > 2 Then
            Dim paramDeployId As DbParameter = e.Command.Parameters("@desp_id")
            e.Command.Parameters.Remove(paramDeployId)
        End If

    End Sub

End Class
