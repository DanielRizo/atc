﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Applications
Imports System.Data.Common
Imports AjaxControlToolkit
Imports System.IO.Compression

Partial Class Groups_GroupAndroidCtl
    Inherits TeleLoader.Web.BasePage

    Dim fileDeployedObj As FileDeployedAndroid

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            dsCtl.SelectParameters("groupId").DefaultValue = objSessionParams.intSelectedGroup
            dsCtl.SelectParameters("deployTypeId").DefaultValue = 2 'Fixed Data = ZIP de XML's

            grdCtl.DataBind()

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub grdCtl_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdCtl.RowCommand
        Try

            Select Case e.CommandName

                Case "Delete"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    grdCtl.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    dsCtl.DeleteParameters("deployedFileId").DefaultValue = grdCtl.SelectedDataKey.Values.Item("desp_id")
                    dsCtl.DeleteParameters("filename").DefaultValue = grdCtl.SelectedDataKey.Values.Item("desp_nombre_archivo")

                    Dim strData As String = "Archivo ID: " & grdCtl.SelectedDataKey.Values.Item("desp_nombre_archivo") & ", grupo ID: "

                    dsCtl.Delete()

                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Archivo eliminado del Grupo. Datos[ " & strData & " ]", "")

                    dsCtl.DataBind()

                    pnlError.Visible = False
                    pnlMsg.Visible = True
                    lblMsg.Text = "Archivo de Configuración eliminado del Grupo"

                    'Delete Physical File
                    'Dim filePath = Server.MapPath(ConfigurationSettings.AppSettings.Get("GroupsFolderPathCTL") + objSessionParams.strtSelectedGroup + "/" + grdCtl.Rows(grdCtl.SelectedIndex).Cells(1).Text)


                    'File.Delete(filePath)
                    'File.Delete(filePath)


                    grdCtl.SelectedIndex = -1

            End Select

            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub

    Protected Sub btnAddDeployFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddDeployFile.Click

        Dim strDirectorio As String = ""
        Dim strNombreFile As String = ""
        Dim boolContinue As Boolean = False


        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Save")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        strDirectorio = ConfigurationSettings.AppSettings.Get("GroupsFolderPathCTL")

        If (objSessionParams.strAppFilename.Equals("")) Then
            pnlMsg.Visible = False
            pnlError.Visible = True
            lblError.Text = "Debe seleccionar un archivo CTL de Configuración para subir al grupo."
            'Set Toggle Visible
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
            Return
        End If

        Try
            fileDeployedObj = New FileDeployedAndroid(strConnectionString, objSessionParams.intSelectedGroup)
            boolContinue = True
            ' Archivo de configuración base, validar que no exista uno repetido
            fileDeployedObj.BaseFileName = ConfigurationSettings.AppSettings.Get("ConfigsFileBaseName")

            'Continuar con el proceso
            If boolContinue = True Then
                'Establecer Datos de la Aplicación
                fileDeployedObj.Filename = objSessionParams.strAppFilename

                'Save Data

                'New Application Group Created OK
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Archivo de Configuración desplegado en el grupo. Datos[ " & getFormDataLog() & " ]", "")

                    pnlMsg.Visible = True
                    pnlError.Visible = False
                    lblMsg.Text = "Archivo de Configuración desplegado en el grupo correctamente."

                    dsCtl.SelectParameters("groupId").DefaultValue = objSessionParams.intSelectedGroup
                    dsCtl.SelectParameters("deployTypeId").DefaultValue = 2 'Fixed Data = ZIP de XML's
                    grdCtl.DataBind()

                    clearForm()

                    'si existe archvo se eliminan lso archivos desplegados en la raiz de la publicacion Web
                    Try
                        For Each fichero As String In Directory.GetFiles("C:\inetpub\wwwroot\PublicacionDatafast\MdmFilesCtl")
                            File.Delete(fichero)
                        Next

                        'Copiar Archivo Físico a la carpeta del Grupo
                        File.Copy(ConfigurationSettings.AppSettings.Get("TempFolder") + objSessionParams.strAppFilename, Server.MapPath(ConfigurationSettings.AppSettings.Get("GroupsFolderPathCTL") + objSessionParams.strAppFilename))
                    Catch ex As Exception
                        pnlMsg.Visible = False
                        pnlError.Visible = True
                        lblError.Text = "Error subiendo archivo, por favor reinicie el proceso."
                        'Set Toggle Visible
                        ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
                        Return
                    End Try

                Else
                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error desplegando Archivo de Configuración en el Grupo. Archivo ya Existe. Datos[ " & getFormDataLog() & " ]", "")
                    pnlError.Visible = True
                    pnlMsg.Visible = False
                    lblError.Text = "Error desplegando Archivo de Configuración en el Grupo. Nombre repetido. Por favor elimine antes la aplicación deseada."

                End If


            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub
    Private Sub clearForm()

    End Sub

    Protected Sub dsCtl_Deleting(sender As Object, e As SqlDataSourceCommandEventArgs) Handles dsCtl.Deleting

        'Remover parametro extra, igual al datakeyname
        If e.Command.Parameters.Count > 2 Then
            Dim paramDeployId As DbParameter = e.Command.Parameters("@desp_id")
            e.Command.Parameters.Remove(paramDeployId)
        End If

    End Sub

End Class
