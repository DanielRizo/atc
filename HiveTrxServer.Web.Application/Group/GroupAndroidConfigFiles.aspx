﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="GroupAndroidConfigFiles.aspx.vb" Inherits="Groups_GroupAndroidConfigFiles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Archivos de Configuración del Grupo Android
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   

	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script> 
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkListGroups" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/ListAndroidGroups.aspx">Listar Grupos Android</asp:LinkButton>
    </li>
    <li>Archivos de Configuración del Grupo Android</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    Archivos de Configuración del Grupo: [ <%= objSessionParams.strtSelectedGroup %> ]
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite administrar los Archivos de Configuración del grupo Android:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>   
            
        <asp:Panel ID="pnlError" runat="server" Visible="False">    
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>  

        <div class="toggle-message" id="dragAndDropArea" style="z-index: 590;">
            <h3 class="title">Agregar Archivo de Configuraci&oacute;n...
                <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
            <div class="hide-message" id="hide-message" style="display: none;">
        
                <div class="st-form-line">	
                    <span class="st-labeltext">Agregar Archivo de Config.:</span>	
                    <div class="uploader" id="uniform-undefined">
                        <asp:FileUpload ID="fileApp" runat="server" CssClass="uniform" TabIndex="1" onchange="handleFiles(this.files)" />
                        <span class="filename">No hay archivo seleccionado...</span><span class="action">Seleccione...</span>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="st-form-line">	
                        <div id="dragandrophandler"><br /><br />Arrastre y Suelte el archivo aqu&iacute;...</div>
                    <div class="clear"></div>
                </div>


                <br />

                <blockquote>Nota: Se debe subir el archivo "configuracion_base.zip" inicialmente.</blockquote>

                <div class="button-box">
                    <asp:Button ID="btnAddDeployFile" runat="server" Text="Adicionar Archivo" 
                        CssClass="button-gray" TabIndex="4" OnClientClick="return validateAddDeployFile();" Enabled="false" />
                </div>            
            </div>
        </div>

    </div>
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
	    <div class="titleh">
    	    <h3>Archivos de Configuración Creados en el Grupo Android</h3>
        </div>
        <div class="body">    
            <br />            
            <asp:GridView ID="grdDeployedFiles" runat="server" AllowPaging="True" 
                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                DataSourceID="dsDeployedFiles" ForeColor="#333333"
                CssClass="mGridCenter" DataKeyNames="desp_id" 
                EmptyDataText="No existen Archivos de Configuración creados en el Grupo Android." 
                HorizontalAlign="Center">
                <RowStyle BackColor="White" ForeColor="White" />
                <Columns>

                    <asp:BoundField DataField="desp_fecha" HeaderText="Fecha Despliegue" 
                        SortExpression="desp_fecha" />
                    <asp:BoundField DataField="desp_nombre_archivo" HeaderText="Nombre Archivo" 
                        SortExpression="desp_nombre_archivo" />
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>                        
                            <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False" 
                                CommandName="Delete" ImageUrl="~/img/icons/16x16/delete.png" Text="Eliminar" 
                                ToolTip="Eliminar Archivo de Configuración del Grupo" CommandArgument='<%# grdDeployedFiles.Rows.Count%>' style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" 
                                OnClientClick="return ConfirmAction(this);" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
                    CssClass="pgr" Font-Underline="False" />
                <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid" 
                    BorderWidth="1px" />
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>            
            <br />
	        <asp:SqlDataSource ID="dsDeployedFiles" runat="server" 
                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"                 
                SelectCommand="sp_webConsultarArchivosDesplegadosGrupoAndroid" SelectCommandType="StoredProcedure"
                DeleteCommand="sp_webEliminarArchivoDesplegadoXGrupoAndroid" DeleteCommandType="StoredProcedure" ConflictDetection="OverwriteChanges" >
                <SelectParameters>
                    <asp:Parameter Name="deployTypeId" Type="String" />
                    <asp:Parameter Name="groupId" Type="String" />
                </SelectParameters>
                <DeleteParameters>
                    <asp:Parameter Name="deployedFileId" Type="String" />
                    <asp:Parameter Name="groupId" Type="String" />
                </DeleteParameters>
            </asp:SqlDataSource> 
        </div>
    </div>
    
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Group/ListAndroidGroups.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">
    
    <!-- Validator -->
    <script src="../js/ValidatorDeployFilesAndroidGroup.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

