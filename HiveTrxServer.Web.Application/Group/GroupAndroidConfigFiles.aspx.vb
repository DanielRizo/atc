﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Applications
Imports System.Data.Common
Imports AjaxControlToolkit
Imports System.IO.Compression

Partial Class Groups_GroupAndroidConfigFiles
    Inherits TeleLoader.Web.BasePage

    Dim fileDeployedObj As FileDeployedAndroid

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            dsDeployedFiles.SelectParameters("groupId").DefaultValue = objSessionParams.intSelectedGroup
            dsDeployedFiles.SelectParameters("deployTypeId").DefaultValue = 2 'Fixed Data = ZIP de XML's

            grdDeployedFiles.DataBind()

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub grdDeployedFiles_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdDeployedFiles.RowCommand
        Try

            Select Case e.CommandName

                Case "Delete"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    grdDeployedFiles.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    dsDeployedFiles.DeleteParameters("deployedFileId").DefaultValue = grdDeployedFiles.SelectedDataKey.Values.Item("desp_id")
                    dsDeployedFiles.DeleteParameters("groupId").DefaultValue = objSessionParams.intSelectedGroup

                    Dim strData As String = "Archivo ID: " & grdDeployedFiles.SelectedDataKey.Values.Item("desp_id") & ", grupo ID: " & objSessionParams.intSelectedGroup

                    dsDeployedFiles.Delete()

                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Archivo eliminado del Grupo. Datos[ " & strData & " ]", "")

                    dsDeployedFiles.DataBind()

                    pnlError.Visible = False
                    pnlMsg.Visible = True
                    lblMsg.Text = "Archivo de Configuración eliminado del Grupo"

                    'Delete Physical File
                    Dim filePath = Server.MapPath(ConfigurationSettings.AppSettings.Get("GroupsFolderPath") + objSessionParams.strtSelectedGroup + "/" + grdDeployedFiles.Rows(grdDeployedFiles.SelectedIndex).Cells(1).Text)

                    If File.Exists(filePath) Then
                        File.Delete(filePath)
                        File.Delete(filePath)
                    End If

                    grdDeployedFiles.SelectedIndex = -1

            End Select

            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub

    Protected Sub btnAddDeployFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddDeployFile.Click

        Dim strDirectorio As String = ""
        Dim strNombreFile As String = ""
        Dim boolContinue As Boolean = False


        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Save")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        strDirectorio = ConfigurationSettings.AppSettings.Get("GroupsFolderPath") + objSessionParams.strtSelectedGroup

        If (objSessionParams.strAppFilename.Equals("")) Then
            pnlMsg.Visible = False
            pnlError.Visible = True
            lblError.Text = "Debe seleccionar un archivo de Configuración para subir al grupo."
            'Set Toggle Visible
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
            Return
        End If

        'Validar ZIP
        Try
            'Validar que el ZIP contenga al menos un archivo
            Dim zipArchive As ZipArchive = ZipFile.OpenRead(ConfigurationSettings.AppSettings.Get("TempFolder") + objSessionParams.strAppFilename)
            Dim flagContentOK As Boolean = False

            For Each appFile In zipArchive.Entries
                If appFile.Name.ToLower.Contains(".xml") Then
                    flagContentOK = True
                    Exit For
                End If
            Next

            zipArchive.Dispose()

            If Not flagContentOK Then
                pnlMsg.Visible = False
                pnlError.Visible = True
                lblError.Text = "El archivo Zip está vacío o su contenido es erróneo. Por favor validar."
                'Set Toggle Visible
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
                Return
            End If
        Catch ex As Exception
            pnlMsg.Visible = False
            pnlError.Visible = True
            lblError.Text = "El archivo Zip está dañado o su contenido es erróneo. Por favor validar."
            'Set Toggle Visible
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
            Return

        End Try

        Try
            fileDeployedObj = New FileDeployedAndroid(strConnectionString, objSessionParams.intSelectedGroup)

            'Validar Archivo Base de Configuración
            If (objSessionParams.strAppFilename = ConfigurationSettings.AppSettings.Get("ConfigsFileBaseName")) Then

                'Archivo de configuración base, validar que no exista uno repetido
                fileDeployedObj.BaseFileName = ConfigurationSettings.AppSettings.Get("ConfigsFileBaseName")

                'Validar Existencia archivo base
                If fileDeployedObj.validateBaseFileName() Then
                    pnlError.Visible = True
                    pnlMsg.Visible = False
                    lblError.Text = "El grupo debe tener un solo archivo base de configuración."

                    boolContinue = False
                Else
                    boolContinue = True
                End If

            Else


                ' Archivo de configuración base, validar que no exista uno repetido
                fileDeployedObj.BaseFileName = ConfigurationSettings.AppSettings.Get("ConfigsFileBaseName")

                'Validar Existencia archivo base
                If Not fileDeployedObj.validateBaseFileName() Then
                    pnlError.Visible = True
                    pnlMsg.Visible = False
                    lblError.Text = "El grupo debe tener un archivo base de configuración, por favor revisar."
                    boolContinue = False
                Else
                    boolContinue = True
                End If

            End If

                'Continuar con el proceso
                If boolContinue = True Then
                'Establecer Datos de la Aplicación
                fileDeployedObj.Filename = objSessionParams.strAppFilename
                fileDeployedObj.PackageName = ""
                fileDeployedObj.Version = ""
                fileDeployedObj.DeployTypeID = 2 'Fixed Data = ZIP DE XML's

                'Save Data
                If fileDeployedObj.createDeployedFileGroupAndroid() Then
                    'New Application Group Created OK
                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Archivo de Configuración desplegado en el grupo. Datos[ " & getFormDataLog() & " ]", "")

                    pnlMsg.Visible = True
                    pnlError.Visible = False
                    lblMsg.Text = "Archivo de Configuración desplegado en el grupo correctamente."

                    dsDeployedFiles.SelectParameters("groupId").DefaultValue = objSessionParams.intSelectedGroup
                    dsDeployedFiles.SelectParameters("deployTypeId").DefaultValue = 2 'Fixed Data = ZIP de XML's

                    grdDeployedFiles.DataBind()

                    clearForm()

                    'Copiar Archivo Físico a la carpeta del Grupo
                    Try
                        File.Copy(ConfigurationSettings.AppSettings.Get("TempFolder") + objSessionParams.strAppFilename, Server.MapPath(ConfigurationSettings.AppSettings.Get("GroupsFolderPath") + objSessionParams.strtSelectedGroup + "/" + objSessionParams.strAppFilename))
                    Catch ex As Exception
                        pnlMsg.Visible = False
                        pnlError.Visible = True
                        lblError.Text = "Error subiendo archivo, por favor reinicie el proceso."
                        'Set Toggle Visible
                        ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
                        Return
                    End Try

                Else
                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error desplegando Archivo de Configuración en el Grupo. Archivo ya Existe. Datos[ " & getFormDataLog() & " ]", "")
                    pnlError.Visible = True
                    pnlMsg.Visible = False
                    lblError.Text = "Error desplegando Archivo de Configuración en el Grupo. Nombre repetido. Por favor elimine antes la aplicación deseada."

                End If
            End If

            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub

    Private Sub clearForm()

    End Sub

    Protected Sub dsDeployedFiles_Deleting(sender As Object, e As SqlDataSourceCommandEventArgs) Handles dsDeployedFiles.Deleting

        'Remover parametro extra, igual al datakeyname
        If e.Command.Parameters.Count > 2 Then
            Dim paramDeployId As DbParameter = e.Command.Parameters("@desp_id")
            e.Command.Parameters.Remove(paramDeployId)
        End If

    End Sub

End Class
