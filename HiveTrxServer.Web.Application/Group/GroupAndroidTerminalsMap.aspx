﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="GroupAndroidTerminalsMap.aspx.vb" Inherits="Group_GroupAndroidTerminalsMap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Ubicación de Terminales Android X Grupo
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

    <script type="text/javascript" src="../js/jquery.ui.slider.js"></script>

    <script type="text/javascript" src="../js/jquery.ui.datepicker.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkListGroups" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/ListAndroidGroups.aspx">Listar Grupos Android</asp:LinkButton>
    </li>
    <li>Ubicación Terminales Android</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    Ubicación Terminales Android del Grupo: [ <%= objSessionParams.strtSelectedGroup %> ]
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlError" runat="server" Visible="False">
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <div class="titleh">
            <h3>Ubicación de Terminales</h3>
        </div>
        <div class="body">

            <div id="map" style="width: 100%; height: 381px">
            </div>

        </div>
    </div>

    <asp:HyperLink ID="lnkBack" runat="server" NavigateUrl="~/Group/ListAndroidGroups.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <!-- Validator -->
    <script src="../js/ValidatorTerminalsMapLocation.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <script src="<%=ConfigurationSettings.AppSettings.Get("Protocol").Trim().ToLower()%>://maps.googleapis.com/maps/api/js?key=<%=ConfigurationSettings.AppSettings.Get("LocationAPIKey")%>&callback=initMap" async defer></script>

    <script src="../js/markerclusterer.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

</asp:Content>

