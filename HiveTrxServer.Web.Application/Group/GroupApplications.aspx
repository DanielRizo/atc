﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="GroupApplications.aspx.vb" Inherits="Groups_GroupApplications" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Aplicaciones del grupo
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkListGroups" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/ListGroups.aspx">Listar Grupos</asp:LinkButton>
    </li>
    <li>Aplicaciones del Grupo</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    APLICACIONES DEL GRUPO: [ <%= objSessionParams.strtSelectedGroup %> ]
    <asp:HyperLink ID="HyperLink1" runat="server"
        NavigateUrl="~/Group/ListGroups.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <p>Este módulo le permite administrar las aplicaciones del grupo:</p>

    <br />

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlError" runat="server" Visible="False">
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <div class="toggle-message" id="dragAndDropArea" style="z-index: 590;">
            <h3 class="title">Agregar aplicaci&oacute;n...
                <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
            <div class="hide-message" id="hide-message" style="display: none;">

                <div class="st-form-line">
                    <span class="st-labeltext">Archivo de aplicaci&oacute;n:</span>
                    <div class="uploader" id="uniform-undefined">
                        <asp:FileUpload ID="fileApp" runat="server" CssClass="uniform" TabIndex="1" onchange="handleFiles(this.files)" />
                        <span class="filename">No hay archivo seleccionado...</span><span class="action">Seleccione...</span>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="st-form-line">
                    <div id="dragandrophandler">
                        <br />
                        <br />
                        Arrastre y suelte el archivo aqu&iacute;...</div>
                    <div class="clear"></div>
                </div>

                <div class="st-form-line">
                    <span class="st-labeltext">Descripci&oacute;n:</span>
                    <asp:TextBox ID="txtDesc" CssClass="st-forminput" Style="width: 697px"
                        runat="server" TabIndex="2" MaxLength="250" TextMode="MultiLine"
                        ToolTip="Descripción de la aplicación." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>
       <%--         CARGA DE APLICACIONES POR MODELO OG--%>
                <div class="st-form-line">
                    <span class="st-labeltext-inline">Modelo terminal: (*)</span>
                    <asp:DropDownList ID="ddlTerminalModel" class="uniform" runat="server"
                        DataSourceID="dsTerminalModel" DataTextField="descripcion"
                        DataValueField="id" Width="200px"
                        ToolTip="Seleccione el modelo de la terminal." TabIndex="4">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="dsTerminalModel" runat="server"
                        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" SelectCommand="SELECT -1 AS id, '       ' AS descripcion
                                UNION ALL
                                SELECT id, descripcion FROM ModeloTerminal"></asp:SqlDataSource>
                    <div class="clear"></div>
                </div>
                <div class="button-box">
                    <asp:Button ID="btnAddApplication" runat="server" Text="Adicionar aplicación"
                        CssClass="button-aqua" TabIndex="4" OnClientClick="return validateAddApplication();" Enabled="false" />
                </div>
            </div>
        </div>

    </div>
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
        <div class="titleh">
            <h3>Aplicaciones creadas en el grupo</h3>
        </div>
        <div class="body">
            <br />
            <asp:GridView ID="grdApplications" runat="server" AllowPaging="True"
                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4"
                DataSourceID="dsApplications" ForeColor="#333333"
                CssClass="mGridCenter" DataKeyNames="apl_id"
                EmptyDataText="No existen aplicaciones creadas en el grupo."
                HorizontalAlign="Left" Width="1060px">
                <RowStyle BackColor="White" ForeColor="White" />
                <Columns>
                    <asp:BoundField DataField="modelo" HeaderText="Modelo terminal"
                        SortExpression="modelo" />
                    <asp:BoundField DataField="apl_nombre_archivo_fisico" HeaderText="Nombre archivo f&iacute;sico"
                        SortExpression="apl_nombre_archivo_fisico" />
                    <asp:BoundField DataField="apl_nombre_tms" HeaderText="Nombre del TMS"
                        SortExpression="apl_nombre_tms" />
                    <asp:BoundField DataField="apl_checksum_tms" HeaderText="CheckSum del TMS y ZIP"
                        SortExpression="apl_checksum_tms" />
                    <asp:BoundField DataField="apl_longitud" HeaderText="Tamaño (Bytes)"
                        SortExpression="apl_longitud" />
                    <asp:BoundField DataField="apl_descripcion" HeaderText="Descripci&oacute;n"
                        SortExpression="apl_descripcion" />

                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False"
                                CommandName="Delete" ImageUrl="~/img/icons/16x16/delete.png" Text="Eliminar"
                                ToolTip="Eliminar Aplicación del Grupo" CommandArgument='<%# grdApplications.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink"
                                OnClientClick="return ConfirmAction(this);" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
                    CssClass="pgr" Font-Underline="False" />
                <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid"
                    BorderWidth="1px" />
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>
            <br />
            <asp:SqlDataSource ID="dsApplications" runat="server"
                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
                SelectCommand="sp_webConsultarAplicacionesGrupo" SelectCommandType="StoredProcedure"
                DeleteCommand="sp_webEliminarAplicacionXGrupo" DeleteCommandType="StoredProcedure" ConflictDetection="OverwriteChanges">
                <SelectParameters>
                    <asp:Parameter Name="groupId" Type="String" />
                </SelectParameters>
                <DeleteParameters>
                    <asp:Parameter Name="applicationId" Type="String" />
                    <asp:Parameter Name="groupId" Type="String" />
                </DeleteParameters>
            </asp:SqlDataSource>
        </div>
    </div>

    <asp:HyperLink ID="lnkBack" runat="server"
        NavigateUrl="~/Group/ListGroups.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <!-- Validator -->
    <script src="../js/ValidatorApplicationGroup.js" type="text/javascript"></script>
    <%--<script src="../js/ValidatorApplicationAndroidGroup.js" type="text/javascript"></script>--%>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

</asp:Content>

