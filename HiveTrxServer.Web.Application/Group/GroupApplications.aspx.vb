﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Applications
Imports System.Data.Common
Imports AjaxControlToolkit
Imports System.Security.Cryptography
Imports log4net


Partial Class Groups_GroupApplications
    Inherits TeleLoader.Web.BasePage

    ' Private Shared Property Log As log4net.ILog



    Dim applicationObj As Application

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            txtDesc.Focus()

            dsApplications.SelectParameters("groupId").DefaultValue = objSessionParams.intSelectedGroup

            grdApplications.DataBind()

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub grdApplications_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdApplications.RowCommand
        Try

            Select Case e.CommandName

                Case "Delete"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    grdApplications.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    dsApplications.DeleteParameters("applicationId").DefaultValue = grdApplications.SelectedDataKey.Values.Item("apl_id")
                    dsApplications.DeleteParameters("groupId").DefaultValue = objSessionParams.intSelectedGroup

                    Dim strData As String = "Aplicación ID: " & grdApplications.SelectedDataKey.Values.Item("apl_id") & ", grupo ID: " & objSessionParams.intSelectedGroup

                    dsApplications.Delete()

                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Aplicación eliminada del Grupo. Datos[ " & strData & " ]", "")

                    grdApplications.DataBind()

                    pnlError.Visible = False
                    pnlMsg.Visible = True
                    lblMsg.Text = "Aplicación eliminada del Grupo"

                    grdApplications.SelectedIndex = -1

            End Select

            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub

    Protected Sub btnAddApplication_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddApplication.Click


        Dim strDirectorio As String = ""
        Dim strNombreFile As String = ""

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Save")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        strDirectorio = ConfigurationSettings.AppSettings.Get("GroupsFolderFullPathTMP")
        Try
            File.Copy(ConfigurationSettings.AppSettings.Get("TempFolder") + objSessionParams.strAppFilename, Server.MapPath(ConfigurationSettings.AppSettings.Get("GroupsFolderPathTMP") + objSessionParams.strAppFilename))
        Catch ex As Exception
            pnlMsg.Visible = False
            pnlError.Visible = True
            lblError.Text = "Error subiendo archivo, por favor reinicie el proceso."
            'Set Toggle Visible 
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
            Return
        End Try

        If (objSessionParams.strAppFilename.Equals("")) Then
            pnlMsg.Visible = False
            pnlError.Visible = True
            lblError.Text = "Debe seleccionar un archivo de aplicación para subir al grupo."
            'Set Toggle Visible
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
            Return
        End If

        Try
            applicationObj = New Application(strConnectionString, objSessionParams.intSelectedGroup)

            'Establecer Datos de la Aplicación


            If (objSessionParams.strAppFilename.IndexOf(".tms") > -1) Then
                applicationObj.PhysicalFilename = objSessionParams.strAppFilename
                applicationObj.TMSFilename = getTMSFilename(strDirectorio + objSessionParams.strAppFilename) + New FileInfo(objSessionParams.strAppFilename).Extension
                applicationObj.TMSChecksum = getTMSChecksum(strDirectorio + objSessionParams.strAppFilename)
                applicationObj.HardDrivePath = strDirectorio + objSessionParams.strAppFilename
                applicationObj.Desc = txtDesc.Text
                applicationObj.TerminalModel = ddlTerminalModel.SelectedValue
                applicationObj.AplData = getAplData(strDirectorio + objSessionParams.strAppFilename)
            ElseIf (objSessionParams.strAppFilename.IndexOf(".zip") > -1) Then
                applicationObj.PhysicalFilename = objSessionParams.strAppFilename
                applicationObj.TMSFilename = objSessionParams.strAppFilename
                applicationObj.TMSChecksum = GetSHA1HashFromFile(strDirectorio + objSessionParams.strAppFilename)
                applicationObj.HardDrivePath = strDirectorio + objSessionParams.strAppFilename
                applicationObj.Desc = txtDesc.Text
                applicationObj.TerminalModel = ddlTerminalModel.SelectedValue
                applicationObj.AplData = getAplData(strDirectorio + objSessionParams.strAppFilename)
            ElseIf (objSessionParams.strAppFilename.IndexOf(".gz") > -1) Then
                applicationObj.PhysicalFilename = objSessionParams.strAppFilename
                applicationObj.TMSFilename = objSessionParams.strAppFilename
                applicationObj.TMSChecksum = GetSHA1HashFromFile(strDirectorio + objSessionParams.strAppFilename)
                applicationObj.HardDrivePath = strDirectorio + objSessionParams.strAppFilename
                applicationObj.Desc = txtDesc.Text
                applicationObj.TerminalModel = ddlTerminalModel.SelectedValue
                applicationObj.AplData = getAplData(strDirectorio + objSessionParams.strAppFilename)
            End If

            'Save Data
            If applicationObj.createApplicationGroup() Then
                'New Application Group Created OK
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Aplicación Creada en el grupo. Datos[ " & getFormDataLog() & " ]", "")

                pnlMsg.Visible = True
                pnlError.Visible = False
                lblMsg.Text = "Aplicación agregada al Grupo correctamente."

                dsApplications.SelectParameters("groupId").DefaultValue = objSessionParams.intSelectedGroup

                grdApplications.DataBind()

                clearForm()

            Else
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error agregando Aplicación al Grupo. Aplicación ya Existe. Datos[ " & getFormDataLog() & " ]", "")
                pnlError.Visible = True
                pnlMsg.Visible = False
                lblError.Text = "Archivo de Aplicación ya existe en el grupo. Nombre del TMS repetido. Por favor elimine antes la aplicación deseada."

            End If

            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
        My.Computer.FileSystem.DeleteFile(ConfigurationSettings.AppSettings.Get("GroupsFolderFullPathTMP") + objSessionParams.strAppFilename,
        Microsoft.VisualBasic.FileIO.UIOption.AllDialogs,
        Microsoft.VisualBasic.FileIO.RecycleOption.SendToRecycleBin)

        'If applicationObj.createDeployedFileGroupAndroid() Then
        '	'New Application Group Created OK
        '	objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Aplicación desplegada en el grupo. Datos[ " & getFormDataLog() & " ]", "")

        '	pnlMsg.Visible = True
        '	pnlError.Visible = False
        '	lblMsg.Text = "Aplicación desplegada en el grupo correctamente."

        '	dsTerminalModel.SelectParameters("groupId").DefaultValue = objSessionParams.intSelectedGroup
        '	dsTerminalModel.SelectParameters("deployTypeId").DefaultValue = 1 'Fixed Data = APK's

        '	grdApplications.DataBind()

        '	clearForm()

        '	'Copiar Archivo Físico a la carpeta del Grupo
        '	Try
        '		File.Copy(ConfigurationSettings.AppSettings.Get("TempFolder") + objSessionParams.strAppFilename, Server.MapPath(ConfigurationSettings.AppSettings.Get("GroupsFolderPath") + objSessionParams.strtSelectedGroup + "/" + objSessionParams.strAppFilename))
        '	Catch ex As Exception
        '		pnlMsg.Visible = False
        '		pnlError.Visible = True
        '		lblError.Text = "Error subiendo archivo, por favor reinicie el proceso."
        '		'Set Toggle Visible
        '		ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
        '		Return
        '	End Try

        'Else
        '	objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error desplegando aplicación en el Grupo. Aplicación ya Existe. Datos[ " & getFormDataLog() & " ]", "")
        '	pnlError.Visible = True
        '	pnlMsg.Visible = False
        '	lblError.Text = "Error desplegando aplicación en el Grupo. Nombre repetido. Por favor elimine antes la aplicación deseada."

        'End If

    End Sub



    Private Function getTMSFilename(ByVal filePath As String) As String
        Dim tmsFilename As String = ""
        Dim fileReader As System.IO.StreamReader = Nothing
        Try
            Dim counter As Integer = 0
            fileReader = My.Computer.FileSystem.OpenTextFileReader(ConfigurationSettings.AppSettings.Get("GroupsFolderFullPathTMP") + objSessionParams.strAppFilename)

            Do While fileReader.Peek() >= 0
                Dim stringReader As String = fileReader.ReadLine()

                If (counter = 3) Then
                    tmsFilename = stringReader.Substring(3, 10).Trim
                    Exit Do
                End If

                counter += 1
            Loop

        Catch ex As Exception
            tmsFilename = ""
        Finally
            If Not fileReader Is Nothing Then
                fileReader.Close()
            End If
        End Try

        Return tmsFilename
    End Function

    Private Function getTMSChecksum(ByVal filePath As String) As String
        Dim tmsChecksum As String = ""
        Dim posType As String = ""
        Dim fileReader As System.IO.StreamReader = Nothing
        Try
            Dim counter As Integer = 0
            fileReader = My.Computer.FileSystem.OpenTextFileReader(ConfigurationSettings.AppSettings.Get("GroupsFolderFullPathTMP") + objSessionParams.strAppFilename)

            Do While fileReader.Peek() >= 0
                Dim stringReader As String = fileReader.ReadLine()

                If (counter = 1) Then
                    'Obtener Tipo de Terminal
                    posType = stringReader
                ElseIf (counter = 3) Then
                    'Validar Tipo de Terminal para Leer el Checksum
                    If (posType.Trim() = "A5-T1000") Then
                        tmsChecksum = stringReader.Substring(40, 8).Trim
                    Else
                        tmsChecksum = stringReader.Substring(36, 4).Trim
                    End If

                    Exit Do
                End If

                counter += 1
            Loop

        Catch ex As Exception
            tmsChecksum = ""
        Finally
            If Not fileReader Is Nothing Then
                fileReader.Close()
            End If
        End Try

        Return tmsChecksum
    End Function

    'Lectura de Archivo Tms, Almacenando su valor en un string
    Private Function getAplData(ByVal filePath As String) As String
        Dim fileReader As System.IO.FileStream = Nothing
        Dim sLine As String = ""
        Dim AplData As New ArrayList()
        Dim bytes As Byte() = IO.File.ReadAllBytes(ConfigurationSettings.AppSettings.Get("GroupsFolderFullPathTMP") + objSessionParams.strAppFilename)
        Dim hex As String() = Array.ConvertAll(bytes, Function(b) b.ToString("X2"))
        sLine = String.Join("", hex)


        Return sLine

    End Function

    Private Sub clearForm()
        txtDesc.Text = ""
        ddlTerminalModel.SelectedIndex = -1
    End Sub

    Protected Sub dsApplications_Deleting(sender As Object, e As SqlDataSourceCommandEventArgs) Handles dsApplications.Deleting

        'Remover parametro extra, igual al datakeyname
        If e.Command.Parameters.Count > 2 Then
            Dim paramAplId As DbParameter = e.Command.Parameters("@apl_id")
            e.Command.Parameters.Remove(paramAplId)
        End If

    End Sub

    Private Function GetSHA1HashFromFile(ByVal filePath As String) As String
        Dim sha1 As SHA1 = SHA1.Create()
        Dim sb As StringBuilder = New StringBuilder()
        Try
            Using fs As FileStream = File.Open(filePath, FileMode.Open)
                For Each b As Byte In sha1.ComputeHash(fs)
                    sb.Append(b.ToString("x2").ToUpper())
                Next
            End Using
        Catch ex As Exception

        End Try
        Return sb.ToString()
    End Function

End Class