﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="GroupTerminals.aspx.vb" Inherits="Groups_GroupTerminals" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Terminales del Grupo
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkListGroups" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/ListGroups.aspx">Listar Grupos</asp:LinkButton>
    </li>
    <li>Terminales del Grupo</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    TERMINALES DE GRUPO: [ <%= objSessionParams.strtSelectedGroup %> ]
    <asp:HyperLink ID="HyperLink1" runat="server"
        NavigateUrl="~/Group/ListGroups.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <p>Este módulo le permite administrar las terminales del grupo:</p>

    <br />

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlError" runat="server" Visible="False">
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <div class="toggle-message" style="z-index: 590; top: 0px; left: 0px;">
            <h3 class="title">Filtro de b&uacute;squeda...
                <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
            <div class="hide-message" id="hide-message" style="display: none;">
                <div class="st-form-line">
                    <span class="st-labeltext"><b>Serial: </b></span>
                    <asp:TextBox ID="txtSerial" CssClass="st- "
                        runat="server" TabIndex="1" MaxLength="50"
                        ToolTip="Serial de la Terminal" onkeydown="return (event.keyCode!=13);" Width="250px"></asp:TextBox>
                </div>
                <div class="st-form-line">
                    <span class="st-labeltext"><b>IMEI: </b></span>
                    <asp:TextBox ID="txtImei" CssClass="st-forminput"
                        runat="server" TabIndex="2" MaxLength="50"
                        ToolTip="IMEI Módem de la Terminal" onkeydown="return (event.keyCode!=13);" Width="250px"></asp:TextBox>
                </div>
                <div class="button-box">
                    <asp:Button ID="btnConsultar" runat="server" Text="Filtrar"
                        CssClass="button-aqua" TabIndex="3" ToolTip="Filtrar Clientes..." />
                </div>
            </div>
        </div>

    </div>
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
        <div class="titleh">
            <h3>Terminales creadas en el grupo</h3>
            <div class="shortcuts-icons" style="z-index: 660;">
                <a class="shortcut tips" href="AddTerminalGroup.aspx" original-title="Agregar Nueva Terminal">
                    <img src="../img/icons/shortcut/plus.png" width="25" height="25" alt="icon"></a>
            </div>
        </div>
        <div class="body">
            <br />
            <asp:GridView ID="grdTerminals" runat="server" AllowPaging="True"
                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4"
                DataSourceID="dsTerminals" ForeColor="#333333"
                CssClass="mGridCenter" DataKeyNames="ter_id,ter_serial"
                EmptyDataText="No existen Terminales creadas en el grupo o con el filtro aplicado"
                HorizontalAlign="Center">
                <RowStyle BackColor="White" ForeColor="White" />
                <Columns>
                    <asp:TemplateField HeaderText=" " SortExpression="ter_logo" HeaderStyle-Width="20px">
                        <ItemTemplate>
                            <asp:Image ID="imgUrlIcon" runat="server" ImageUrl='<%# Bind("ter_logo") %>' ToolTip='<%# Bind("ter_fecha_actualizacion_auto") %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="ter_serial" HeaderText="Serial"
                        SortExpression="ter_serial" />
                    <asp:BoundField DataField="ter_modelo" HeaderText="Modelo"
                        SortExpression="ter_modelo" />
                    <asp:BoundField DataField="ter_imei" HeaderText="IMEI"
                        SortExpression="ter_imei" />
                     <asp:BoundField DataField="ter_codigo_producto" HeaderText="Codigo de producto"
                        SortExpression="ter_codigo_producto" />
                    <asp:TemplateField HeaderText="Prioridad grupo" SortExpression="ter_prioridad_grupo">
                        <ItemTemplate>
                            <asp:CheckBox ID="editChk1" runat="server" Checked='<%# Bind("ter_prioridad_grupo")%>' Enabled="false" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Actualizar manual" SortExpression="ter_actualizar">
                        <ItemTemplate>
                            <asp:CheckBox ID="editChk2" runat="server" Checked='<%# Bind("ter_actualizar")%>' Enabled="false" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="ter_fecha_programacion" HeaderText="Fecha manual descarga"
                        SortExpression="ter_fecha_programacion" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField DataField="ter_ip_descarga" HeaderText="IP descarga"
                        SortExpression="ter_ip_descarga" />
                    <asp:BoundField DataField="ter_puerto_descarga" HeaderText="Puerto descarga"
                        SortExpression="ter_puerto_descarga" />
                    <asp:BoundField DataField="ter_estado_actualizacion" HeaderText="Estado actualización"
                        SortExpression="ter_estado_actualizacion" ItemStyle-HorizontalAlign="Center" />
                    <asp:TemplateField HeaderText="Estado" SortExpression="ter_estado">
                        <ItemTemplate>
                            <asp:CheckBox ID="editChk3" runat="server" Checked='<%# Bind("ter_estado")%>' Enabled="false" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False" HeaderStyle-Width="130px">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False"
                                CommandName="EditTerminal" ImageUrl="~/img/icons/16x16/edit.png" Text="Editar"
                                ToolTip="Editar Terminal" CommandArgument='<%# grdTerminals.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />
                            <asp:ImageButton ID="imgShowMap" runat="server" CausesValidation="False"
                                CommandName="ShowTerminalMap" ImageUrl="~/img/icons/16x16/map.png" Text="Ubicación"
                                ToolTip="Mostrar Ubicación en Mapa" CommandArgument='<%# grdTerminals.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />
                            <asp:ImageButton ID="imgEMVConfig" runat="server" CausesValidation="False"
                                CommandName="EMVConfigTerminal" ImageUrl="~/img/icons/16x16/emv.png" Text="EMV Config."
                                ToolTip="Configurar Parámetros EMV" CommandArgument='<%# grdTerminals.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />
                            <asp:ImageButton ID="ImgTerminalStis" runat="server" CausesValidation="False"
                                CommandName="terminalStis" ImageUrl="~/img/icons/16x16/nuevo.png" Text=""
                                ToolTip="Parametros EMV" CommandArgument='<%# grdTerminals.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink"/>
                            <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False"
                                CommandName="DeleteTerminal" ImageUrl="~/img/icons/16x16/delete.png" Text="Eliminar"
                                ToolTip="Eliminar Terminal del Grupo" CommandArgument='<%# grdTerminals.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink"
                                OnClientClick="return ConfirmAction(this);" />           
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>

                </Columns>
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
                    CssClass="pgr" Font-Underline="False" />
                <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid"
                    BorderWidth="1px" />
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>
            <br />
            <asp:SqlDataSource ID="dsTerminals" runat="server"
                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
                SelectCommand="sp_webConsultarTerminalesGrupo" SelectCommandType="StoredProcedure"
                DeleteCommand="sp_webEliminarTerminalXGrupo" DeleteCommandType="StoredProcedure" ConflictDetection="OverwriteChanges">
                <SelectParameters>
                    <asp:Parameter Name="groupId" Type="String" />
                    <asp:Parameter Name="terminalSerial" Type="String" DefaultValue="-1" />
                    <asp:Parameter Name="terminalImei" Type="String" DefaultValue="-1" />
                </SelectParameters>
                <DeleteParameters>
                    <asp:Parameter Name="terminalId" Type="String" />
                    <asp:Parameter Name="groupId" Type="String" />
                </DeleteParameters>

            </asp:SqlDataSource>
        </div>
    </div>

    <asp:HyperLink ID="lnkBack" runat="server"
        NavigateUrl="~/Group/ListGroups.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

    <asp:GridView ID="grdLogXUser" runat="server"
        AllowPaging="True" AutoGenerateColumns="False" CellPadding="4"
        DataSourceID="dsLogXUser" ForeColor="#333333"
        CssClass="mGridCenter"
        EmptyDataText="No existen acciones registradas del usuario para el período dado."
        HorizontalAlign="Center" PageSize="25" Visible="False">
        <RowStyle BackColor="White" ForeColor="White" />
        <Columns>
            <asp:BoundField DataField="log_fecha" HeaderText="Fecha" SortExpression="log_fecha"></asp:BoundField>
            <asp:BoundField DataField="usu_login" HeaderText="Login" SortExpression="usu_login"></asp:BoundField>
            <asp:BoundField DataField="usu_nombre" HeaderText="Nombre Usuario" SortExpression="usu_nombre"></asp:BoundField>
            <asp:BoundField DataField="Objeto" HeaderText="Objeto del Sistema" SortExpression="Objeto"></asp:BoundField>
            <asp:BoundField DataField="Metodo" HeaderText="Método" SortExpression="Metodo"></asp:BoundField>
            <asp:BoundField DataField="TipoMensaje" HeaderText="Tipo de Mensaje" SortExpression="TipoMensaje"></asp:BoundField>
            <asp:BoundField DataField="log_mensaje1" HeaderText="Info. Adicional" SortExpression="log_mensaje1"></asp:BoundField>
        </Columns>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
            CssClass="pgr" Font-Underline="False" />
        <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid"
            BorderWidth="1px" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>
    <asp:SqlDataSource ID="dsLogXUser" runat="server"
        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
        SelectCommand="sp_webReporteAuditoria"
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:Parameter Name="userID" Type="Int64" />
            <asp:Parameter Name="startDate" Type="DateTime" />
            <asp:Parameter Name="endDate" Type="DateTime" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:GridView ID="grdLogXInventory" runat="server"
        AllowPaging="True" AutoGenerateColumns="False" CellPadding="4"
        DataSourceID="dsLogXInventory" ForeColor="#333333"
        CssClass="mGridCenter"
        EmptyDataText="No existe log de inventario para los filtros aplicados."
        HorizontalAlign="Center" PageSize="25" Visible="False">
        <RowStyle BackColor="White" ForeColor="White" />
        <Columns>
            <asp:BoundField DataField="tra_fecha" HeaderText="Fecha" SortExpression="tra_fecha"></asp:BoundField>
            <asp:BoundField DataField="tra_cliente_pos" HeaderText="Cliente" SortExpression="tra_cliente_pos"></asp:BoundField>
            <asp:BoundField DataField="gru_nombre" HeaderText="Grupo" SortExpression="gru_nombre"></asp:BoundField>
            <asp:BoundField DataField="tra_serial_pos" HeaderText="Serial Terminal" SortExpression="tra_serial_pos"></asp:BoundField>
            <asp:BoundField DataField="tra_marca_terminal" HeaderText="Marca" SortExpression="tra_marca_terminal"></asp:BoundField>
            <asp:BoundField DataField="tra_tipo_terminal" HeaderText="Tipo" SortExpression="tra_tipo_terminal"></asp:BoundField>
            <asp:BoundField DataField="tra_codigo_respuesta" HeaderText="C&oacute;d. Rta." SortExpression="tra_codigo_respuesta" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
            <asp:BoundField DataField="tra_mensaje_respuesta" HeaderText="Mensaje Respuesta" SortExpression="tra_mensaje_respuesta" ItemStyle-Font-Size="Smaller"></asp:BoundField>
            <asp:BoundField DataField="tra_estado_actualizacion" HeaderText="Estado" SortExpression="tra_estado_actualizacion" ItemStyle-Font-Size="Smaller"></asp:BoundField>
            <asp:BoundField DataField="tra_version_software_pos" HeaderText="Versi&oacute;n Software" SortExpression="tra_version_software_pos" ItemStyle-Font-Size="Smaller"></asp:BoundField>
            <asp:TemplateField HeaderText="Aplicaciones POS">
                <ItemTemplate>
                    <div id="posApps" style="overflow: auto; width: 130px; height: 100px;">
                        <asp:Label ID="lblPosApps" runat="server" ToolTip='Listado de aplicaciones en la Terminal' Text='<%# Bind("tra_aplicaciones_pos")%>' Font-Size="Smaller"></asp:Label>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="tra_aplicaciones_a_instalar" HeaderText="Aplicaciones a Instalar" SortExpression="tra_aplicaciones_a_instalar" ItemStyle-Font-Size="Smaller"></asp:BoundField>
            <asp:BoundField DataField="tra_fecha_descarga" HeaderText="Fecha Descarga" SortExpression="tra_fecha_descarga" ItemStyle-Font-Size="Smaller"></asp:BoundField>
            <asp:BoundField DataField="tra_datos_descarga" HeaderText="IP/Puerto Descarga" SortExpression="tra_datos_descarga"></asp:BoundField>
        </Columns>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
            CssClass="pgr" Font-Underline="False" />
        <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid"
            BorderWidth="1px" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>
    <asp:SqlDataSource ID="dsLogXInventory" runat="server"
        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
        SelectCommand="sp_webReporteInventario"
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:Parameter Name="groupID" Type="Int64" />
            <asp:Parameter Name="terminalID" Type="Int64" />
            <asp:Parameter Name="customerID" Type="Int64" />
            <asp:Parameter Name="startDate" Type="DateTime" />
            <asp:Parameter Name="endDate" Type="DateTime" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:GridView ID="grdLogXDownload" runat="server"
        AllowPaging="True" AutoGenerateColumns="False" CellPadding="4"
        DataSourceID="dsLogXDownload" ForeColor="#333333"
        CssClass="mGridCenter"
        EmptyDataText="No existe log de descarga para los filtros aplicados."
        HorizontalAlign="Center" PageSize="25" Visible="False">
        <RowStyle BackColor="White" ForeColor="White" />
        <Columns>
            <asp:BoundField DataField="tra_fecha" HeaderText="Fecha" SortExpression="tra_fecha"></asp:BoundField>
            <asp:BoundField DataField="tra_cliente_pos" HeaderText="Cliente" SortExpression="tra_cliente_pos"></asp:BoundField>
            <asp:BoundField DataField="gru_nombre" HeaderText="Grupo" SortExpression="gru_nombre"></asp:BoundField>
            <asp:BoundField DataField="tra_serial_pos" HeaderText="Serial Terminal" SortExpression="tra_serial_pos"></asp:BoundField>
            <asp:BoundField DataField="tra_marca_terminal" HeaderText="Marca" SortExpression="tra_marca_terminal"></asp:BoundField>
            <asp:BoundField DataField="tra_tipo_terminal" HeaderText="Tipo" SortExpression="tra_tipo_terminal"></asp:BoundField>
            <asp:BoundField DataField="tra_codigo_respuesta" HeaderText="C&oacute;d. Rta." SortExpression="tra_codigo_respuesta" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
            <asp:BoundField DataField="tra_mensaje_respuesta" HeaderText="Mensaje Respuesta" SortExpression="tra_mensaje_respuesta"></asp:BoundField>
            <asp:BoundField DataField="tra_estado_actualizacion" HeaderText="Estado" SortExpression="tra_estado_actualizacion"></asp:BoundField>
            <asp:BoundField DataField="apl_nombre_tms" HeaderText="Nombre Aplicaci&oacute;n" SortExpression="apl_nombre_tms"></asp:BoundField>
            <asp:BoundField DataField="tra_porcentaje_descarga" HeaderText="% de Descarga" SortExpression="tra_porcentaje_descarga"></asp:BoundField>
        </Columns>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
            CssClass="pgr" Font-Underline="False" />
        <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid"
            BorderWidth="1px" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>
    <asp:SqlDataSource ID="dsLogXDownload" runat="server"
        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
        SelectCommand="sp_webReporteDescarga"
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:Parameter Name="groupID" Type="Int64" />
            <asp:Parameter Name="terminalID" Type="Int64" />
            <asp:Parameter Name="customerID" Type="Int64" />
            <asp:Parameter Name="startDate" Type="DateTime" />
            <asp:Parameter Name="endDate" Type="DateTime" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:GridView ID="GridView1" runat="server"
        AllowPaging="True" AutoGenerateColumns="False" CellPadding="4"
        DataSourceID="dsTerminals" ForeColor="#333333"
        CssClass="mGridCenter"
        EmptyDataText="No existen terminales para los filtros aplicados."
        HorizontalAlign="Center" PageSize="25" Visible="False">
        <RowStyle BackColor="White" ForeColor="White" />
        <Columns>
            <asp:BoundField DataField="ter_fecha_creacion" HeaderText="Fecha de Creacion" SortExpression="ter_fecha_creacion"></asp:BoundField>
            <asp:BoundField DataField="fecha_consulta" HeaderText="Fecha ultima Consulta" SortExpression="fecha_consulta"></asp:BoundField>
            <asp:BoundField DataField="aplicaciones_instaladas" HeaderText="Aplicaciones Instaladas" SortExpression="aplicaciones_instaladas"></asp:BoundField>
            <asp:BoundField DataField="aplicaciones_pendientes" HeaderText="Aplicaciones Pendientes" SortExpression="aplicaciones_pendientes"></asp:BoundField>
            <asp:BoundField DataField="gru_nombre" HeaderText="Grupo" SortExpression="gru_nombre"></asp:BoundField>
            <asp:BoundField DataField="latitud_gps" HeaderText="Latitud gps" SortExpression="latitud_gps"></asp:BoundField>
            <asp:BoundField DataField="longitud_gps" HeaderText="Longitud gps" SortExpression="longitud_gps"></asp:BoundField>
            <asp:BoundField DataField="ter_serial" HeaderText="Serial Terminal" SortExpression="ter_serial"></asp:BoundField>
            <asp:BoundField DataField="ter_marca_terminal" HeaderText="Marca" SortExpression="ter_marca_terminal"></asp:BoundField>
            <asp:BoundField DataField="ter_tipo_terminal" HeaderText="Tipo" SortExpression="ter_tipo_terminal"></asp:BoundField>
            <asp:BoundField DataField="ter_serial_sim" HeaderText="Serial SIM" SortExpression="ter_serial_sim"></asp:BoundField>
            <asp:BoundField DataField="ter_estado_actualizacion" HeaderText="Estado" SortExpression="ter_estado_actualizacion"></asp:BoundField>
            <asp:BoundField DataField="ter_mensaje_actualizacion" HeaderText="Mensaje de Actualizaci&oacute;n" SortExpression="ter_mensaje_actualizacion"></asp:BoundField>
            <asp:TemplateField HeaderText="Estado" SortExpression="ter_estado">
                <ItemTemplate>
                    <asp:CheckBox ID="editChk1" runat="server" Checked='<%# Bind("ter_estado")%>' Enabled="false" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Prioridad Grupo?" SortExpression="ter_prioridad_grupo">
                <ItemTemplate>
                    <asp:CheckBox ID="editChk2" runat="server" Checked='<%# Bind("ter_prioridad_grupo")%>' Enabled="false" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Actualizar?" SortExpression="ter_actualizar">
                <ItemTemplate>
                    <asp:CheckBox ID="editChk3" runat="server" Checked='<%# Bind("ter_actualizar")%>' Enabled="false" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:BoundField DataField="ter_fecha_programacion" HeaderText="Fecha Descarga" SortExpression="ter_fecha_programacion"></asp:BoundField>
            <asp:BoundField DataField="ter_datos_descarga" HeaderText="Datos Descarga" SortExpression="ter_datos_descarga"></asp:BoundField>
        </Columns>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
            CssClass="pgr" Font-Underline="False" />
        <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid"
            BorderWidth="1px" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server"
        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
        SelectCommand="sp_webReporteTerminales"
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:Parameter Name="groupID" Type="Int64" />
            <asp:Parameter Name="terminalID" Type="Int64" />
            <asp:Parameter Name="customerID" Type="Int64" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:GridView ID="grdLogXMDM" runat="server"
        AllowPaging="True" AutoGenerateColumns="False" CellPadding="4"
        DataSourceID="dsLogXMDM" ForeColor="#333333"
        CssClass="mGridCenter"
        EmptyDataText="No existe log de MDM para los filtros aplicados."
        HorizontalAlign="Center" PageSize="25" Visible="False">
        <RowStyle BackColor="White" ForeColor="White" />
        <Columns>
            <asp:BoundField DataField="tra_fecha" HeaderText="Fecha Conexión" SortExpression="tra_fecha"></asp:BoundField>
            <asp:BoundField DataField="tra_operacion" HeaderText="Tipo Operación" SortExpression="tra_operacion"></asp:BoundField>
            <asp:BoundField DataField="tra_terminal_id_device" HeaderText="Dispositivo ID" SortExpression="tra_terminal_id_device"></asp:BoundField>
            <asp:BoundField DataField="gru_nombre" HeaderText="Grupo" SortExpression="gru_nombre"></asp:BoundField>
            <asp:BoundField DataField="tra_terminal_ip" HeaderText="Dispositivo IP" SortExpression="tra_terminal_ip"></asp:BoundField>
            <asp:TemplateField HeaderText="Apps Instaladas" ItemStyle-Width="200px">
                <ItemTemplate>
                    <div id="posApps" style="overflow: auto; width: 200px; height: 85px;">
                        <asp:Label ID="lblPosApps" runat="server" ToolTip='Listado de aplicaciones instaladas en el Dispositivo' Text='<%# Bind("tra_terminal_apks_instalados") %>' Font-Size="Smaller"></asp:Label>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="tra_apk_descargado" HeaderText="App Descargada" SortExpression="tra_apk_descargado" ItemStyle-Font-Size="Smaller"></asp:BoundField>
            <asp:BoundField DataField="tra_xml_descargado" HeaderText="Xml Descargado" SortExpression="tra_xml_descargado" ItemStyle-Font-Size="Smaller"></asp:BoundField>
            <asp:BoundField DataField="tra_img_descargado" HeaderText="Img Descargado" SortExpression="tra_img_descargado" ItemStyle-Font-Size="Smaller"></asp:BoundField>
            <asp:BoundField DataField="tra_estado_final" HeaderText="Estado Final" SortExpression="tra_estado_final"></asp:BoundField>
        </Columns>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
            CssClass="pgr" Font-Underline="False" />
        <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid"
            BorderWidth="1px" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>
    <asp:SqlDataSource ID="dsLogXMDM" runat="server"
        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
        SelectCommand="sp_webReporteMDM"
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:Parameter Name="groupID" Type="Int64" />
            <asp:Parameter Name="terminalID" Type="Int64" />
            <asp:Parameter Name="customerID" Type="Int64" />
            <asp:Parameter Name="startDate" Type="DateTime" />
            <asp:Parameter Name="endDate" Type="DateTime" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:GridView ID="grdTerminalsMDM" runat="server"
        AllowPaging="True" AutoGenerateColumns="False" CellPadding="4"
        DataSourceID="dsTerminalsMDM" ForeColor="#333333"
        CssClass="mGridCenter"
        EmptyDataText="No existen terminales Android para los filtros aplicados."
        HorizontalAlign="Center" PageSize="25" Visible="False">
        <RowStyle BackColor="White" ForeColor="White" />
        <Columns>
            <asp:TemplateField HeaderText=" " SortExpression="ter_logo">
                <ItemTemplate>
                    <asp:Image ID="imgUrlIcon" runat="server" ImageUrl='<%# Bind("ter_logo") %>' ToolTip='<%# Bind("ter_fecha_actualizacion_auto") %>' />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:BoundField DataField="ter_fecha_actualizacion_auto" HeaderText="Últ. Fecha Conexión" SortExpression="ter_fecha_actualizacion_auto"></asp:BoundField>
            <asp:BoundField DataField="gru_nombre" HeaderText="Grupo" SortExpression="gru_nombre"></asp:BoundField>
            <asp:BoundField DataField="ter_serial" HeaderText="Dispositivo ID" SortExpression="ter_serial"></asp:BoundField>
            <asp:BoundField DataField="ter_id_heracles" HeaderText="Heracles ID" SortExpression="ter_id_heracles"></asp:BoundField>
            <asp:BoundField DataField="ter_ip_dispositivo" HeaderText="IP" SortExpression="ter_ip_dispositivo"></asp:BoundField>
            <asp:BoundField DataField="ter_nombre_heracles" HeaderText="Nombre" SortExpression="ter_nombre_heracles"></asp:BoundField>
            <asp:BoundField DataField="ter_ciudad_heracles" HeaderText="Ciudad" SortExpression="ter_ciudad_heracles"></asp:BoundField>
            <asp:BoundField DataField="ter_region_heracles" HeaderText="Región" SortExpression="ter_region_heracles"></asp:BoundField>
            <asp:BoundField DataField="ter_agencia_heracles" HeaderText="Agencia" SortExpression="ter_agencia_heracles"></asp:BoundField>
            <asp:TemplateField HeaderText="Comp. 1" SortExpression="ter_logo">
                <ItemTemplate>
                    <asp:Image ID="imgUrlIconCompA" runat="server" ImageUrl='<%# Bind("ter_logo_componente1")%>' ToolTip='<%# Bind("ter_fecha_inyeccion_componente1")%>' />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Comp. 2" SortExpression="ter_logo">
                <ItemTemplate>
                    <asp:Image ID="imgUrlIconCompB" runat="server" ImageUrl='<%# Bind("ter_logo_componente2")%>' ToolTip='<%# Bind("ter_fecha_inyeccion_componente2")%>' />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Llave" SortExpression="ter_logo">
                <ItemTemplate>
                    <asp:Image ID="imgUrlIconStatusKey" runat="server" ImageUrl='<%# Bind("ter_logo_llave")%>' ToolTip='<%# Bind("ter_fecha_ultima_carga_llave") %>' />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:BoundField DataField="apk_banco" HeaderText="APK Banco" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Smaller" SortExpression="apk_banco"></asp:BoundField>
            <asp:BoundField DataField="configuracion" HeaderText="Configuración" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Smaller" SortExpression="configuracion"></asp:BoundField>
            <asp:BoundField DataField="imagenes" HeaderText="Imágenes" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Smaller" SortExpression="imagenes"></asp:BoundField>
        </Columns>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
            CssClass="pgr" Font-Underline="False" />
        <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid"
            BorderWidth="1px" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>
    <asp:SqlDataSource ID="dsTerminalsMDM" runat="server"
        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
        SelectCommand="sp_webReporteTerminalesMDM"
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:Parameter Name="groupID" Type="Int64" />
            <asp:Parameter Name="terminalID" Type="Int64" />
            <asp:Parameter Name="customerID" Type="Int64" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:GridView ID="grdLogXKeyLoading" runat="server"
        AllowPaging="True" AutoGenerateColumns="False" CellPadding="4"
        DataSourceID="dsLogXKeyLoading" ForeColor="#333333"
        CssClass="mGridCenter"
        EmptyDataText="No existe log de Transacciones de Carga de Llaves para los filtros aplicados."
        HorizontalAlign="Center" PageSize="25" Visible="False">
        <RowStyle BackColor="White" ForeColor="White" />
        <Columns>
            <asp:BoundField DataField="tra_fecha" HeaderText="Fecha Conexión" SortExpression="tra_fecha"></asp:BoundField>
            <asp:BoundField DataField="tra_operacion" HeaderText="Tipo Operación" SortExpression="tra_operacion"></asp:BoundField>
            <asp:BoundField DataField="tra_terminal_id_device" HeaderText="Dispositivo ID" SortExpression="tra_terminal_id_device"></asp:BoundField>
            <asp:BoundField DataField="gru_nombre" HeaderText="Grupo" SortExpression="gru_nombre"></asp:BoundField>
            <asp:BoundField DataField="tra_terminal_ip" HeaderText="Dispositivo IP" SortExpression="tra_terminal_ip"></asp:BoundField>
            <asp:BoundField DataField="tra_estado_final" HeaderText="Estado Final" SortExpression="tra_estado_final"></asp:BoundField>
        </Columns>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
            CssClass="pgr" Font-Underline="False" />
        <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid"
            BorderWidth="1px" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>
    <asp:SqlDataSource ID="dsLogXKeyLoading" runat="server"
        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
        SelectCommand="sp_webReporteCargaLlaves"
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:Parameter Name="groupID" Type="Int64" />
            <asp:Parameter Name="terminalID" Type="Int64" />
            <asp:Parameter Name="customerID" Type="Int64" />
            <asp:Parameter Name="startDate" Type="DateTime" />
            <asp:Parameter Name="endDate" Type="DateTime" />
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

    <script language="javascript">
        var globalcontrol = '';

        function ConfirmAction(control) {

            globalcontrol = control;

            $.msgAlert({
                type: "warning"
                , title: "Mensaje del sistema"
                , text: "Realmente desea eliminar la terminal?"
                , callback: function () {
                    __doPostBack($(globalcontrol).attr('name'), '');
                }
            });

            return false;
        }
    </script>

</asp:Content>

