﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Applications
Imports System.Data.Common

Partial Class Groups_GroupTerminals
    Inherits TeleLoader.Web.BasePage

    Dim applicationObj As Application

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            txtSerial.Focus()

            dsTerminals.SelectParameters("groupId").DefaultValue = objSessionParams.intSelectedGroup

            grdTerminals.DataBind()

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub grdTerminals_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdTerminals.RowCommand
        Try

            Select Case e.CommandName

                Case "DeleteTerminal"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    grdTerminals.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    dsTerminals.DeleteParameters("terminalId").DefaultValue = grdTerminals.SelectedDataKey.Values.Item("ter_id")
                    dsTerminals.DeleteParameters("groupId").DefaultValue = objSessionParams.intSelectedGroup

                    Dim strData As String = "Terminal ID: " & grdTerminals.SelectedDataKey.Values.Item("ter_id") & ", grupo ID: " & objSessionParams.intSelectedGroup

                    dsTerminals.Delete()

                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Terminal eliminada del Grupo. Datos[ " & strData & " ]", "")

                    grdTerminals.DataBind()

                    pnlError.Visible = False
                    pnlMsg.Visible = True
                    lblMsg.Text = "Terminal eliminada del Grupo"

                    grdTerminals.SelectedIndex = -1

                Case "EditTerminal"
                    grdTerminals.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.intSelectedTerminalID = grdTerminals.SelectedDataKey.Values.Item("ter_id")

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("EditTerminalGroup.aspx", False)

                Case "ShowTerminalMap"
                    grdTerminals.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.intSelectedTerminalID = grdTerminals.SelectedDataKey.Values.Item("ter_id")

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("ShowTerminalGroupLocation.aspx", False)

                Case "EMVConfigTerminal"
                    grdTerminals.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.intSelectedTerminalID = grdTerminals.SelectedDataKey.Values.Item("ter_id")
                    objSessionParams.strSelectedTerminalSerial = grdTerminals.SelectedDataKey.Values.Item("ter_serial")

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("EMVConfigItemsTerminal.aspx", False)

                Case "terminalStis"
                    'Validamos el Tid asociado al Tid'

                    grdTerminals.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    objSessionParams.strSelectedTerminalSerial = grdTerminals.SelectedDataKey.Values.Item("ter_serial")

                    If GetTid(objSessionParams.strSelectedTerminalSerial) Then
                        'New Group Edited OK
                        objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Load", "Tid Encontrado. Datos[ " & getFormDataLog() & " ]", "")
                        'Llenamos la variable de sesion con el resultado del Sp
                        Session("SessionParameters") = objSessionParams
                        'Tree View
                        Response.Redirect("EditTerminalstis.aspx", False)
                    Else
                        objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Load", "Tid no encontrado. Datos[ " & getFormDataLog() & " ]", "")
                        pnlError.Visible = True
                        pnlMsg.Visible = False
                        lblError.Text = "Tid no relacionado para el serial." & grdTerminals.SelectedDataKey.Values.Item("ter_serial")
                    End If

            End Select

            If txtSerial.Text = "" And txtImei.Text = "" Then
                'Set Invisible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
            Else
                'Set Visible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
            End If

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub

    Protected Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click

        If txtSerial.Text <> "" Then
            dsTerminals.SelectParameters("terminalSerial").DefaultValue = txtSerial.Text
        Else
            dsTerminals.SelectParameters("terminalSerial").DefaultValue = "-1"
        End If

        If txtImei.Text <> "" Then
            dsTerminals.SelectParameters("terminalImei").DefaultValue = txtImei.Text
        Else
            dsTerminals.SelectParameters("terminalImei").DefaultValue = "-1"
        End If

        grdTerminals.DataBind()

        If txtSerial.Text = "" And txtImei.Text = "" Then
            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
        Else
            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
        End If

    End Sub

    Private Sub clearForm()
        txtSerial.Text = ""
        txtImei.Text = ""
    End Sub

    Protected Sub dsTerminals_Deleting(sender As Object, e As SqlDataSourceCommandEventArgs) Handles dsTerminals.Deleting

        'Remover parametro extra, igual al datakeyname
        If e.Command.Parameters.Count > 2 Then
            Dim paramAplId As DbParameter = e.Command.Parameters("@ter_id")
            e.Command.Parameters.Remove(paramAplId)
        End If

    End Sub
    Public Function GetTid(ByVal Serial As String) As Boolean
        Dim connection As New SqlConnection(strConnectionString)
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As String = ""
        Dim retVal As Boolean

        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_validarTerminalId"
            command.Parameters.Clear()
            command.Parameters.Add(New SqlParameter("TermianlSerial", Serial))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetString(0)
                    objSessionParams.StrTerminalRecord = results.GetString(1)
                    objSessionParams.StrTerminalID = results.GetString(2)

                End While
            Else
                retVal = False
            End If

            If status = "TID NO ASOCIADO" Then
                retVal = False
            ElseIf status = "SERIAL NO EXISTE" Then
                retVal = False
            ElseIf status = "TID CORRECTO" Then
                retVal = True
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try
        Return retVal
    End Function
End Class
