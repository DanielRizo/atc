﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="GroupTerminalsBatch.aspx.vb" Inherits="Groups_GroupTerminals" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%>:: Crear Terminales x Lote al Grupo
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   

	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script>     
        
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>
    <li>Crear Terminales x Lote al Grupo</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    Crear Terminales x Lote
    <asp:HyperLink ID="HyperLink1" runat="server" 
        NavigateUrl="~/Group/Manager.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite agregar Terminales x Lote al Grupo. Por favor descargue <a href="../Excel/FormatoTerminalesLote.xlsx">AQU&Iacute;</a> el formato en Excel para registrar la informaci&oacute;n de Terminales.</p>
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
        <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>   
            
        <asp:Panel ID="pnlError" runat="server" Visible="False">    
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>  
    </div>

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
	    <div class="titleh">
    	    <h3>Procesar Archivo de Terminales</h3>
        </div>
        <div class="body">

            <div class="st-form-line">
                <span class="st-labeltext">Archivo Local:</span>
                <div class="uploader" id="uniform-undefined">
                    <asp:FileUpload ID="fileTerminals" runat="server" CssClass="uniform" TabIndex="1" />
                    <span class="filename">No hay archivo seleccionado...</span><span class="action">Seleccione...</span>
                </div>
                <asp:Button ID="btnPreProcessFile" runat="server" Text="1. Subir" CssClass="button-aqua" TabIndex="2" />
                <asp:Button ID="btnProcessFile" runat="server" Text="2. Procesar" CssClass="button-aqua" TabIndex="3" Visible="false" />
                <asp:Button ID="btnFinishProcess" runat="server" Text="3. Finalizar" CssClass="button-aqua" TabIndex="4" Visible="false" />
                <asp:HiddenField ID="hidFilename" runat="server" />
            </div>

            <br />  
            
            <asp:Panel ID="pnlResultFinal" runat="server" Visible="False">
	            <div class="titleh">
    	            <h3>Resultado Procesamiento de Archivo: [ <%= hidFilename.Value%>]</h3>
                </div>
                <div class="body">
                    <div class="st-form-noline">
                        <div id="stats" class="processResult">
                	        <div class="column" style="width: 295px;">
                                <b class="up"><asp:Literal runat="server" ID="ltrTerminalsOK"></asp:Literal></b> Terminales PROCESADAS
                	        </div>
                	        <div class="column" style="width: 295px;">
                                <b class="down"><asp:Literal runat="server" ID="ltrTerminalsKO"></asp:Literal></b> Terminales con ERROR
                	        </div>
                	        <div class="column last" style="width: 295px;">
                                <b class="up"><asp:Literal runat="server" ID="ltrTerminalsTotal"></asp:Literal></b> Registros Totales
                	        </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>            
                      
        </div>
    </div>
    
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Group/Manager.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">
    
    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
        $(window).on('beforeunload', function () {
            $("#ctl00_MainContent_btnProcessFile").attr('disabled', 'disabled');
        });
    </script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

