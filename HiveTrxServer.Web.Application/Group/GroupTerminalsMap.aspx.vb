﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users
Imports System.Net
Imports System.Globalization
Imports TeleLoader.Groups
Imports System.IO

Partial Class Group_GroupTerminalsMap
    Inherits TeleLoader.Web.BasePage

    Dim groupObj As Group

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            'Only to display Map, Markers will be loaded via AJAX

        End If

    End Sub
    Protected Sub btnEditKey_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditKey.Click
        Dim tmsFilename As String = ""
        Dim fileReader As System.IO.StreamReader = Nothing
        Try
                Dim counter As String = 0
                fileReader = My.Computer.FileSystem.OpenTextFileReader("C:\deploy\HiveSoft.TeleLoader.Spectra.properties")
                Do While fileReader.Peek() >= 0
                    Dim stringReader As String = fileReader.ReadLine()

                If (counter = 13) Then
                    tmsFilename = stringReader.Substring(15, 39).Trim
                    Exit Do
                End If

                counter += 1
                Loop

            Catch ex As Exception
                tmsFilename = ""
            Finally
                If Not fileReader Is Nothing Then
                    fileReader.Close()
                End If
            End Try

    End Sub


End Class
