﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Applications
Imports System.Data.Common

Partial Class GroupUpdateBatch
    Inherits TeleLoader.Web.BasePage

    Dim applicationObj As Application

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub btnPreProcessFile_Click(sender As Object, e As EventArgs) Handles btnPreProcessFile.Click
        Dim strDirectorio As String = ""
        Dim strNombreArchivo As String = ""

        '0. Validate Uploaded File
        If (fileTerminals.FileName = "" Or fileTerminals Is Nothing) Then
            pnlMsg.Visible = False
            pnlError.Visible = True
            lblError.Text = "Debe seleccionar un archivo válido."
            Return
        End If

        If (Not fileTerminals.FileName.ToLower().Contains(".xlsx")) Then
            pnlMsg.Visible = False
            pnlError.Visible = True
            lblError.Text = "Archivo no soportado, por favor seleccione un archivo válido."
            Return
        End If

        pnlMsg.Visible = False
        pnlError.Visible = False

        '1. Upload File / Delete it If exists
        Try
            strDirectorio = ConfigurationSettings.AppSettings.Get("TempFolder")
            strNombreArchivo = fileTerminals.FileName

            If Directory.Exists(strDirectorio) = False Then ' si no existe la carpeta se crea
                Directory.CreateDirectory(strDirectorio)
            End If

            'Delete it If exists
            If File.Exists(strDirectorio + fileTerminals.FileName) Then
                File.Delete(strDirectorio + fileTerminals.FileName)
                File.Delete(strDirectorio + fileTerminals.FileName)
            End If

            fileTerminals.SaveAs(strDirectorio + fileTerminals.FileName)

            fileTerminals.Enabled = False
            btnPreProcessFile.Enabled = False
            btnPreProcessFile.CssClass = "st-button"
            btnProcessFile.Visible = True

            hidFilename.Value = strNombreArchivo

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub

    Private Function insertNewTerminal(ByVal rowData() As String) As Boolean
        Dim retVal As Boolean = False
        Dim connection As New SqlConnection(strConnectionString)
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim resultSP As Integer

        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webActualizarTerminalXGrupo"
            command.Parameters.Clear()
            command.Parameters.Add(New SqlParameter("customerID", objAccessToken.CustomerUserID))
            command.Parameters.Add(New SqlParameter("groupName", rowData(0)))
            command.Parameters.Add(New SqlParameter("terminalSerial", rowData(1)))


            'Leer Resultado
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    resultSP = results.GetInt32(0)
                End While
            Else
                resultSP = 0
            End If

            retVal = True

        Catch ex As Exception
            resultSP = 0
            retVal = False
        Finally
            'Cerrar DataReader
            If Not (results Is Nothing) Then
                results.Close()
            End If

            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try

        Return retVal

    End Function

    Protected Sub btnProcessFile_Click(sender As Object, e As EventArgs) Handles btnProcessFile.Click

        Dim strDirectorio As String = ConfigurationSettings.AppSettings.Get("TempFolder")
        Dim strNombreArchivo As String = hidFilename.Value

        '2. Process File - Insert New Terminals into DB
        Dim xlWorkSheet As Excel.worksheet
        Dim rowIdx As Integer = 0
        Dim colIdx As Integer = 0
        Dim counterOk As Integer = 0
        Dim counterError As Integer = 0
        Dim MAX_ROWS As Integer = 10000
        Dim MAX_COLS As Integer = 2
        Dim flagEOF As Boolean = False

        Try
            'Read First Sheet
            xlWorkSheet = Excel.Workbook.Worksheets(strDirectorio + strNombreArchivo)(0)
            'Read Rows
            For Each rowObj In xlWorkSheet.Rows
                'Max Rows Reached
                If rowIdx > MAX_ROWS Then
                    Exit For
                ElseIf rowIdx > 0 Then
                    rowIdx += 1
                    colIdx = 0

                    Dim dataRow(MAX_COLS) As String

                    For Each cellObj In rowObj.Cells
                        If colIdx > MAX_COLS Then
                            Exit For
                        End If

                        If Not cellObj Is Nothing Then
                            Select Case colIdx
                                Case 6, 7, 8, 9, 10, 11, 14, 15, 17, 18, 19, 20, 21
                                    'Remove '.' and ','
                                    dataRow(colIdx) = cellObj.Text.Replace(".", "").Replace(",", "")
                                Case 12, 13
                                    'GPS Data
                                    dataRow(colIdx) = cellObj.Text.Replace(",", ".")
                                Case Else
                                    dataRow(colIdx) = cellObj.Text
                            End Select
                        End If
                        colIdx += 1
                    Next

                    'EOF reached
                    If dataRow(0) = "" Or dataRow(1) = "" Then
                        Exit For
                    Else
                        'Insert Terminal into DB
                        If insertNewTerminal(dataRow) Then
                            counterOk = counterOk + 1
                        Else
                            counterError = counterError + 1
                        End If
                    End If
                Else
                    rowIdx += 1
                End If
            Next

            btnPreProcessFile.Enabled = False
            btnProcessFile.Enabled = False
            btnProcessFile.CssClass = "st-button"
            btnFinishProcess.Visible = True

            'Visualizar Resultados
            pnlResultFinal.Visible = True
            ltrTerminalsOK.Text = counterOk
            ltrTerminalsKO.Text = counterError
            ltrTerminalsTotal.Text = counterOk + counterError

            'Set Visible Toggle Panel - Stats
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function(){ $('.processResult').slideToggle(); });</script>"

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub

    Protected Sub btnFinishProcess_Click(sender As Object, e As EventArgs) Handles btnFinishProcess.Click
        btnPreProcessFile.Enabled = True
        btnProcessFile.Visible = False
        btnProcessFile.Enabled = True
        btnFinishProcess.Visible = False
        btnFinishProcess.Enabled = True
        fileTerminals.Enabled = True
        pnlResultFinal.Visible = False

        btnPreProcessFile.CssClass = "button-aqua"
        btnProcessFile.CssClass = "button-aqua"
    End Sub
End Class
