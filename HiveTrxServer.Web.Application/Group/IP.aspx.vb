﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Applications
Imports System.Data.Common

Partial Class Groups_Ip
    Inherits TeleLoader.Web.BasePage

    Dim applicationObj As ApplicationIssuer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            txtIp.Focus()
            dsListIp.SelectParameters("IP_CODE").DefaultValue = objSessionParams.intCARD_CODE
            dsListIp.DataBind()
        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub grdListIp_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdListIp.RowCommand
        Try

            Select Case e.CommandName

                Case "DeleteIp"

                    Dim strConnString As String
                    Dim configurationSection As ConnectionStringsSection =
                    System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
                    strConnString = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
                    Dim connection As New SqlConnection(strConnString)
                    Dim command As New SqlCommand()
                    Dim results As SqlDataReader
                    Dim status As Integer
                    Dim rspMsg As String

                    Dim retVal As Boolean


                    rspMsg = ""
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    grdListIp.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    dsListIp.DeleteParameters("IP_CODE").DefaultValue = grdListIp.SelectedDataKey.Values.Item("IP_CODE")

                    Dim strData As String = "Terminal ID: " & grdListIp.SelectedDataKey.Values.Item("IP_CODE")


                    Try
                        'Abrir Conexion
                        connection.Open()

                        command.Connection = connection
                        command.CommandType = CommandType.StoredProcedure
                        command.CommandText = "sp_webEliminarIp"
                        command.Parameters.Clear()
                        command.Parameters.Add(New SqlParameter("IP_CODE", "" + grdListIp.SelectedDataKey.Values.Item("IP_CODE")))


                        'Ejecutar SP
                        results = command.ExecuteReader()
                        If results.HasRows Then
                            While results.Read()
                                status = results.GetInt32(0)
                                rspMsg = results.GetString(1)
                            End While
                        Else
                            retVal = False
                        End If

                        If status = 1 Then
                            grdListIp.DataBind()

                            pnlError.Visible = False
                            pnlMsg.Visible = True
                            lblMsg.Text = rspMsg
                        Else
                            grdListIp.DataBind()

                            pnlError.Visible = True
                            pnlMsg.Visible = False
                            lblError.Text = rspMsg
                        End If

                    Catch ex As Exception
                        retVal = False
                    Finally
                        'Cerrar data reader por Default
                        If Not (results Is Nothing) Then
                            results.Close()
                        End If
                        'Cerrar conexion por Default
                        If Not (connection Is Nothing) Then
                            connection.Close()
                        End If
                    End Try


                Case "EditIp"
                    grdListIp.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.StrAcquirerCode = grdListIp.SelectedDataKey.Values.Item("IP_CODE")
                    objSessionParams.StrCardRangeName = grdListIp.SelectedDataKey.Values.Item("IP_KEY_NAME")
                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("ListarParameterIp.aspx", False)
            End Select

            If txtIp.Text = "" Then
                'Set Invisible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
            Else
                'Set Visible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
            End If

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub
    'Edicion de Parametros CardRange Stis 
    'Autor : Oscar Gutierrez
    Protected Sub grdListIp_Updated(sender As Object, e As GridViewUpdatedEventArgs) Handles grdListIp.RowUpdated

        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webEditParameter_Ip_Stis"
            command.Parameters.Clear()


            command.Parameters.Add(New SqlParameter("IP_CODE", e.OldValues("IP_CODE")))
            command.Parameters.Add(New SqlParameter("IP_KEY_NAME", e.OldValues("IP_KEY_NAME")))
            command.Parameters.Add(New SqlParameter("IP_DESCRIPTION", e.NewValues("IP_DESCRIPTION")))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try
    End Sub

    Protected Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click

        If txtCode.Text <> "" Then
            dsListIp.SelectParameters("IP_CODE").DefaultValue = txtCode.Text
        Else
            dsListIp.SelectParameters("IP_CODE").DefaultValue = "-1"
        End If
        If txtIp.Text <> "" Then
            dsListIp.SelectParameters("IP_KEY_NAME").DefaultValue = txtIp.Text
        Else
            dsListIp.SelectParameters("IP_KEY_NAME").DefaultValue = "-1"
        End If
        grdListIp.DataBind()

        If txtIp.Text = "" Then
            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
        Else
            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
        End If

    End Sub

    Private Sub clearForm()
        txtIp.Text = ""

    End Sub
End Class
