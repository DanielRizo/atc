﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="InjectComponent2TerminalAndroidGroup.aspx.vb" Inherits="Group_InjectComponent2TerminalAndroidGroup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Componente 2 Terminal del Grupo Android
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
   
    <li>Componente 2 Terminal Android</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">
            <div class="alert alert-success">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">x</a>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlError" runat="server" Visible="False">
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <style>
            .bg-1 {
                background-color: #1abc9c;
                color: #ffffff;
            }
        </style>
        <div class="bg-1">
            <div class="container text-center">
                <h3>Cargar Componente 2</h3>
                <img src="https://logodownload.org/wp-content/uploads/2016/10/visa-logo-14.png" class="img-circle" alt="Bird" width="350" height="320">
            </div>
        </div>
        <div class="container-fluid bg-3 text-center">
            <h3>Datos de la terminal</h3>
            <br>
            <div class="row">
                <div class="col-sm-3">
                    <asp:Label ID="Grupo" runat="server" Text=""></asp:Label>
                    <img src="https://images.vexels.com/media/users/3/135934/isolated/preview/7527e1ec7f70e11ce678d090a5375ee4-icono-de-c--rculo-de-archivo-by-vexels.png" class="img-responsive" style="width: 100%" alt="Image">
                </div>
                <div class="col-sm-3">
                    <asp:Label ID="Serial" runat="server" Text=""></asp:Label>
                    <img src="https://us.123rf.com/450wm/axsimen/axsimen1612/axsimen161200012/69138781-sin-contacto-de-compra-de-pago-del-icono-del-vector-en-un-estilo-plano-pago-bancario-inal%C3%A1mbrico-media.jpg?ver=6" class="img-responsive" style="width: 100%" alt="Image">
                </div>
                <div class="col-sm-3">
                    <asp:Label ID="Modelo" runat="server" Text=""></asp:Label>
                    <img src="https://image.freepik.com/vector-gratis/pase-tarjeta-credito-usando-terminal-tarjeta-credito_7496-730.jpg" class="img-responsive" style="width: 100%" alt="Image">
                </div>
                <div class="col-sm-3">
                    <asp:Label ID="Comunicacion" runat="server" Text=""></asp:Label>
                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR8j3tBbwLj5L7A5-kjzkRvkM8KFmhNVc9NWg&usqp=CAU" class="img-responsive" style="width: 100%" alt="Image">
                </div>
            </div>
        </div>
        <br>
        <div class="body">
            <div class="clear"></div>

            <div class="st-form-line" id="component-segment">
                <span class="st-labeltext">Componente 2: </span>
                <asp:TextBox ID="txtComponent2_1" CssClass="hg-yellow" Style="width: 82px; font-size: 26px !important;"
                    runat="server" TabIndex="1" MaxLength="4" onkeypress="return isHexKey(event)" autocomplete="off"
                    ToolTip="" onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                &nbsp;-&nbsp;
                <asp:TextBox ID="txtComponent2_2" CssClass="hg-yellow" Style="width: 82px; font-size: 26px !important;"
                    runat="server" TabIndex="1" MaxLength="4" onkeypress="return isHexKey(event)" autocomplete="off"
                    ToolTip="" onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                &nbsp;-&nbsp;
                <asp:TextBox ID="txtComponent2_3" CssClass="hg-yellow" Style="width: 82px; font-size: 26px !important;"
                    runat="server" TabIndex="1" MaxLength="4" onkeypress="return isHexKey(event)" autocomplete="off"
                    ToolTip="" onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                &nbsp;-&nbsp;
                <asp:TextBox ID="txtComponent2_4" CssClass="hg-yellow" Style="width: 82px; font-size: 26px !important;"
                    runat="server" TabIndex="1" MaxLength="4" onkeypress="return isHexKey(event)" autocomplete="off"
                    ToolTip="" onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                &nbsp;-&nbsp;
                <asp:TextBox ID="txtComponent2_5" CssClass="hg-yellow" Style="width: 82px; font-size: 26px !important;"
                    runat="server" TabIndex="1" MaxLength="4" onkeypress="return isHexKey(event)" autocomplete="off"
                    ToolTip="" onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                &nbsp;-&nbsp;
                <asp:TextBox ID="txtComponent2_6" CssClass="hg-yellow" Style="width: 82px; font-size: 26px !important;"
                    runat="server" TabIndex="1" MaxLength="4" onkeypress="return isHexKey(event)" autocomplete="off"
                    ToolTip="" onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                &nbsp;-&nbsp;
                <asp:TextBox ID="txtComponent2_7" CssClass="hg-yellow" Style="width: 82px; font-size: 26px !important;"
                    runat="server" TabIndex="1" MaxLength="4" onkeypress="return isHexKey(event)" autocomplete="off"
                    ToolTip="" onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                &nbsp;-&nbsp;
                <asp:TextBox ID="txtComponent2_8" CssClass="hg-yellow" Style="width: 82px; font-size: 26px !important;"
                    runat="server" TabIndex="1" MaxLength="4" onkeypress="return isHexKey(event)" autocomplete="off"
                    ToolTip="" onkeydown="return (event.keyCode!=13);"></asp:TextBox>

                <div class="clear"></div>
            </div>

            <asp:Panel ID="pnlKCVCombined" runat="server" Visible="False">

                <br />
                <br />

                <div class="st-form-line" style="border-bottom: 0px solid #E5E5E5 !important;">
                    <span class="st-labeltext">Dígito de Verificación Llave Combinada: </span>
                    <asp:TextBox ID="txtKCVCombined" CssClass="hg-green" Style="width: 105px; font-size: 26px !important;"
                        runat="server" TabIndex="12" MaxLength="4"
                        ToolTip="Dígito de verificación de la Llave Combinada" onkeydown="return (event.keyCode!=13);" Enabled="False"></asp:TextBox>
                    <strong class="hg-green">* Por favor valide este dígito de verificación de los 2 componentes contra el formato impreso.</strong>
                </div>

                <br />
                <br />

            </asp:Panel>

            <div class="button-box">
                <asp:Button ID="btnStep1" runat="server" Text="Cargar Componente 2"
                    CssClass="btn btn-info" TabIndex="9" OnClientClick="return validateComponent2()" />

            </div>
        </div>
    </div>

    <asp:HyperLink ID="lnkBack" runat="server" NavigateUrl="~/Reports/Estatistics.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

    <ajaxtoolkit:ToolkitScriptManager ID="scrMgrAsp" runat="server">
    </ajaxtoolkit:ToolkitScriptManager>

    <asp:Panel ID="pnlConfirmComponent" runat="server" CssClass="modalPopup" Style="width: 350px !important; visibility: hidden !important;">
        <asp:Label ID="lblTitulo" runat="server"
            Text="Confirmar?" Font-Bold="True"
            Font-Size="Medium"></asp:Label>
        <br />
        <br />
        <h2>Dígito de Verificación:</h2>

        <asp:TextBox ID="txtKCV" CssClass="hg-blue" Style="width: 105px; font-size: 26px !important;"
            runat="server" TabIndex="10" MaxLength="4"
            ToolTip="Dígito de verificación del componente 2" onkeydown="return (event.keyCode!=13);" Enabled="False" Text="34FDBC"></asp:TextBox>

        <br />
        <br />
        <br />
        <br />
        <asp:Button ID="btnStep2" runat="server" Text="Aceptar" CssClass="btn btn-info" />
        <br />

        <asp:LinkButton ID="lnkCancel" runat="server" Font-Bold="True" OnClientClick="SetZIndexUp();"
            Font-Size="Small">Cancelar</asp:LinkButton>
    </asp:Panel>

    <asp:Button ID="btnPopUp" runat="server" Style="display: none" Text="Invisible Button" />

    <ajaxtoolkit:ModalPopupExtender ID="modalPopupExt" BehaviorID="mpe" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="lnkCancel" TargetControlID="btnPopUp" PopupControlID="pnlConfirmComponent">
    </ajaxtoolkit:ModalPopupExtender>

</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <!-- Validator -->
    <script src="../js/ValidatorGroupAndroidTerminalComponent2.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

</asp:Content>

