﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users
Imports System.Net
Imports System.Globalization
Imports TeleLoader.Terminals
Imports System.Drawing
Imports TeleLoader.Security

Partial Class Group_InjectComponent2TerminalAndroidGroup
    Inherits TeleLoader.Web.BasePage

    Dim terminalObj As Terminal

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            terminalObj = New Terminal(strConnectionString, objSessionParams.intSelectedGroup)

            'Leer datos BD
            terminalObj.TerminalID = objSessionParams.intSelectedTerminalID
            terminalObj.getTerminalData()
            terminalObj.GetKeyAndComponentsInformation()



            txtComponent2_1.Focus()


            Dim configurationSection As ConnectionStringsSection =
                       System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
            Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString
            Dim MoldePos As String = ""
            Dim ComunicacionTerminalNotPosback As String = ""

            Using Conn As New SqlConnection(strConnString)
                Conn.Open()
                Dim ConsultaSQL = "SELECT CONCAT('MODELO:  ',G.descripcion) FROM TERMINAL T INNER JOIN ModeloTerminal G ON g.id = T.ter_modelo_terminal_id where ter_serial = '" & terminalObj.TerminalSerial & "'"
                MoldePos = New SqlCommand(ConsultaSQL, Conn).ExecuteScalar().ToString()
            End Using
            Using Conn As New SqlConnection(strConnString)
                Conn.Open()
                Dim ConsultaSQL = "SELECT CONCAT('COMUNICACIÓN:  ',G.descripcion) FROM TERMINAL T INNER JOIN InterfaceTerminal G ON g.id = T.ter_interface_terminal_id where ter_serial ='" & terminalObj.TerminalSerial & "'"
                ComunicacionTerminalNotPosback = New SqlCommand(ConsultaSQL, Conn).ExecuteScalar().ToString()
            End Using

            Grupo.Text = "GRUPO: " & objSessionParams.strtSelectedGroup
            Serial.Text = "SERIAL: " & terminalObj.TerminalSerial
            Modelo.Text = MoldePos
            Comunicacion.Text = ComunicacionTerminalNotPosback
        End If

    End Sub

    Protected Sub btnStep1_Click(sender As Object, e As EventArgs) Handles btnStep1.Click

        pnlKCVCombined.Visible = False

        'Validate Component Data - Server Side
        If validateComponentfields() Then

            Try
                'Calculate KCV
                Dim objEncryptor As New Encryptor(Encryptor.hexStringToByteArray(txtComponent2_1.Text & txtComponent2_2.Text & txtComponent2_3.Text & txtComponent2_4.Text & txtComponent2_5.Text & txtComponent2_6.Text & txtComponent2_7.Text & txtComponent2_8.Text))
                Dim resultArray() = objEncryptor.calculateKCV(3)

                If resultArray.Length > 0 Then
                    txtKCV.Text = Strings.Right("00" & Hex(resultArray(0)), 2) + Strings.Right("00" & Hex(resultArray(1)), 2) + Strings.Right("00" & Hex(resultArray(2)), 2)

                    ltrScript.Text = "<script language='javascript'>$(function(){ SetZIndexDown(); $('#ctl00_MainContent_pnlConfirmComponent').hide(); setTimeout(""$('#ctl00_MainContent_pnlConfirmComponent').css('visibility', 'visible'); $find('mpe').show()"", 200); });</script>"
                    pnlConfirmComponent.Visible = True

                Else
                    pnlError.Visible = True
                    pnlMsg.Visible = False
                    lblError.Text = "Error en los campos del componente, por favor verifíquelos."
                End If
            Catch ex As Exception
                pnlError.Visible = True
                pnlMsg.Visible = False
                lblError.Text = "Error en los campos del componente, por favor verifíquelos."
            End Try

        Else
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error en los campos del componente, por favor verifíquelos."
        End If

    End Sub

    Private Function validateComponentfields() As Boolean
        Dim retVal = False

        'Validate Length And Content
        If txtComponent2_1.Text.Trim.Length = 4 And txtComponent2_2.Text.Trim.Length = 4 And txtComponent2_3.Text.Trim.Length = 4 And _
            txtComponent2_4.Text.Trim.Length = 4 And txtComponent2_5.Text.Trim.Length = 4 And txtComponent2_6.Text.Trim.Length = 4 And _
            txtComponent2_7.Text.Trim.Length = 4 And txtComponent2_8.Text.Trim.Length = 4 Then

            If Regex.IsMatch(txtComponent2_1.Text, "^[0-9A-F]+$") And Regex.IsMatch(txtComponent2_2.Text, "^[0-9A-F]+$") And Regex.IsMatch(txtComponent2_3.Text, "^[0-9A-F]+$") And _
                Regex.IsMatch(txtComponent2_4.Text, "^[0-9A-F]+$") And Regex.IsMatch(txtComponent2_5.Text, "^[0-9A-F]+$") And Regex.IsMatch(txtComponent2_6.Text, "^[0-9A-F]+$") And _
                Regex.IsMatch(txtComponent2_7.Text, "^[0-9A-F]+$") And Regex.IsMatch(txtComponent2_8.Text, "^[0-9A-F]+$") Then
                retVal = True
            End If

        End If

        Return retVal
    End Function

    Protected Sub btnStep2_Click(sender As Object, e As EventArgs) Handles btnStep2.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Save")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        'Validate Component Data - Server Side
        If validateComponentfields() Then

            Try
                'Encrypt Component 1 Using Database Key Decrypted with Hardcoded Key, and IV with zeros
                terminalObj = New Terminal(strConnectionString, objSessionParams.intSelectedGroup)
                terminalObj.GetEncriptionKey()
                Dim objDecryptorKey As New Encryptor(Encryptor._encryptionkey, {0, 0, 0, 0, 0, 0, 0, 0}, Encryptor.hexStringToByteArray(terminalObj.EncryptionKey))
                Dim decryptedKeyArray() = objDecryptorKey.doDecryption()

                'Validate Decryption Process
                If decryptedKeyArray.Length > 0 Then
                    Dim objEncryptor As New Encryptor(decryptedKeyArray, {0, 0, 0, 0, 0, 0, 0, 0}, Encryptor.hexStringToByteArray(txtComponent2_1.Text & txtComponent2_2.Text & txtComponent2_3.Text & txtComponent2_4.Text & txtComponent2_5.Text & txtComponent2_6.Text & txtComponent2_7.Text & txtComponent2_8.Text))
                    Dim encryptedKeyArray() = objEncryptor.doEncryption()

                    'Validate Encryption Process
                    If encryptedKeyArray.Length > 0 Then

                        'Save in Database
                        terminalObj.TerminalID = objSessionParams.intSelectedTerminalID
                        terminalObj.ComponentType = 2 'Second Component
                        terminalObj.Component2 = Encryptor.byteArrayToHexString(encryptedKeyArray)

                        If terminalObj.InjectComponent() Then
                            pnlError.Visible = False
                            pnlMsg.Visible = True
                            lblMsg.Text = "Componente Cargado Correctamente."

                            txtComponent2_1.Text = ""
                            txtComponent2_2.Text = ""
                            txtComponent2_3.Text = ""
                            txtComponent2_4.Text = ""
                            txtComponent2_5.Text = ""
                            txtComponent2_6.Text = ""
                            txtComponent2_7.Text = ""
                            txtComponent2_8.Text = ""
                            txtComponent2_1.Focus()

                            'Validate If Component 1 was injected
                            terminalObj.GetKeyAndComponentsInformation()

                            If terminalObj.Component1 <> "-1" Then
                                'Set Flag Components Injected to True
                                'Show KCV for Combined Key
                                Dim objDecryptorComponent1 As New Encryptor(decryptedKeyArray, {0, 0, 0, 0, 0, 0, 0, 0}, Encryptor.hexStringToByteArray(terminalObj.Component1))
                                Dim decryptedComponent1Array() = objDecryptorComponent1.doDecryption()

                                Dim objDecryptorComponent2 As New Encryptor(decryptedKeyArray, {0, 0, 0, 0, 0, 0, 0, 0}, Encryptor.hexStringToByteArray(terminalObj.Component2))
                                Dim decryptedComponent2Array() = objDecryptorComponent2.doDecryption()

                                'Get Combined Key
                                Dim combinedKeyArray() = Encryptor.calculateCombinedKey(decryptedComponent1Array, decryptedComponent2Array)

                                'Calculate KCV for Combined Key
                                Dim objCombinedEncryptor As New Encryptor(combinedKeyArray)
                                Dim resultCombinedArray() = objCombinedEncryptor.calculateKCV(3)

                                If resultCombinedArray.Length > 0 Then



                                    txtKCVCombined.Text = Strings.Right("00" & Hex(resultCombinedArray(0)), 2) + Strings.Right("00" & Hex(resultCombinedArray(1)), 2) + Strings.Right("00" & Hex(resultCombinedArray(2)), 2)
                                    pnlKCVCombined.Visible = True
                                Else
                                    pnlError.Visible = True
                                    lblError.Text = "Error Calculando KCV de la Llave Combinada."
                                End If
                            End If

                        Else
                            pnlError.Visible = True
                            pnlMsg.Visible = False
                            lblError.Text = "Error cargando componente en la Base de Datos."
                        End If
                    Else
                        pnlError.Visible = True
                        pnlMsg.Visible = False
                        lblError.Text = "Error en los campos del componente, por favor verifíquelos."
                    End If
                Else
                    pnlError.Visible = True
                    pnlMsg.Visible = False
                    lblError.Text = "Error descifrando Llave, consulte con el administrador del sistema."
                End If

            Catch ex As Exception
                pnlError.Visible = True
                pnlMsg.Visible = False
                lblError.Text = "Error en los campos del componente, por favor verifíquelos."
            End Try

        Else
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error en los campos del componente, por favor verifíquelos."
        End If

        ltrScript.Text = "<script language='javascript'>$(function(){ SetZIndexUp(); });</script>"

    End Sub
End Class
