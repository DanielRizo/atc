﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ListAndroidGroups.aspx.vb" Inherits="Group_ListAndroidGroups" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Listar Grupos Android
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed" 
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>
    <li>Listar Grupos Android</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Listar Grupos Android
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite ver el listado de grupos tipo Android del sistema Polaris:</p>
    
    <br />
    <div class="toggle-message" style="z-index: 590; top: 0px; left: 0px;">
        <h3 class="title">Filtro de B&uacute;squeda...   ↓ ↓
            <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
        <div class="hide-message" id="hide-message" style="display: none;">
            <div class="st-form-line">
                <span class="st-labeltext"><b>Nombre: </b></span>
                <asp:TextBox ID="txtNombre" CssClass="st-forminput"
                    runat="server" TabIndex="1" MaxLength="100"
                    ToolTip="Nombre del Grupo" onkeydown="return (event.keyCode!=13);" Width="250px"></asp:TextBox>
            </div>
            <div class="st-form-line">
                <span class="st-labeltext"><b>Descripci&oacute;n: </b></span>
                <asp:TextBox ID="txtDesc" CssClass="st-forminput"
                    runat="server" TabIndex="2" MaxLength="50"
                    ToolTip="Descripción del Grupo" onkeydown="return (event.keyCode!=13);" Width="250px"></asp:TextBox>

            </div>
            <div class="st-form-line">
                    <span class="st-labeltext"><b>Serial: </b></span>
                    <asp:TextBox ID="txtfilterSerial" CssClass="st-forminput"
                        runat="server" TabIndex="3" MaxLength="50"
                        ToolTip="Serial del Dispositivo" onkeydown="return (event.keyCode!=13);" Width="250px"></asp:TextBox>
                </div>
            <div class="button-box">
                <asp:Button ID="btnConsultar" runat="server" Text="Filtrar"
                    CssClass="button-aqua" TabIndex="4" ToolTip="Filtrar Clientes..." />
            </div>
        </div>
    </div>

    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
    
        <asp:GridView ID="grdGroups" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
            DataSourceID="dsListGroups" ForeColor="#333333" Width="100%" 
            CssClass="mGrid" DataKeyNames="gru_id,gru_nombre" 
            EmptyDataText="No existen Grupos tipo Android creados o con el filtro aplicado">
            <RowStyle BackColor="White" ForeColor="White" />
            <Columns>

                 <asp:BoundField DataField="gru_fecha_creacion" HeaderText="Fecha de Creacion"
                     SortExpression="gru_fecha_creacion" />

                <asp:BoundField DataField="cli_nombre" HeaderText="Nombre del Cliente"
                     SortExpression="cli_nombre" />

                <asp:BoundField DataField="gru_nombre" HeaderText="Nombre del Grupo" 
                    SortExpression="gru_nombre" />

                <asp:BoundField DataField="gru_descripcion" HeaderText="Descripción del Grupo" 
                    SortExpression="gru_descripcion" />

                 <asp:BoundField DataField="gru_mensaje_desplegar" HeaderText="Mensaje Actual" 
                    SortExpression="gru_mensaje_desplegar" />

                <asp:BoundField DataField="gru_numero_consultas" HeaderText="Frecuencia de Consultas" 
                    sortExpression="gru_numero_consultas"  ItemStyle-HorizontalAlign="Center"  />
                
                <asp:BoundField DataField="gru_default_login" HeaderText="Login"
                    sortExpression="gru_default_login" />

                <asp:BoundField DataField="gru_default_Password" HeaderText="Password"
                    sortExpression="gru_default_Password" />


                <asp:BoundField DataField="gru_total_terminales" HeaderText="Total Dispositivos" 
                    SortExpression="gru_total_terminales" ItemStyle-HorizontalAlign="Center" />

                <asp:TemplateField ShowHeader="False" HeaderStyle-Width="170px">
                    <ItemTemplate>
                        <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" 
                            CommandName="EditAndroidGroup" ImageUrl="~/img/icons/16x16/edit.png" Text="Editar" 
                            ToolTip="Editar Grupo Android" CommandArgument='<%# grdGroups.Rows.Count%>' style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />
                        <asp:Label ID="txtLabelSpace1" runat="server"></asp:Label>
                        <asp:ImageButton ID="imgShowMap" runat="server" CausesValidation="False" 
                            CommandName="ShowTerminalsMap" ImageUrl="~/img/icons/16x16/map.png" Text="Ubicación" 
                            ToolTip="Mostrar Terminales en Mapa" CommandArgument='<%# grdGroups.Rows.Count%>' style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />                        
                        <asp:Label ID="txtLabelSpace2" runat="server"></asp:Label>
                        <asp:ImageButton ID="imgApps" runat="server" CausesValidation="False" 
                            CommandName="DeployApps" ImageUrl="~/img/icons/16x16/application.png" Text="Aplicaciones" 
                            ToolTip="Deploy de Aplicaciones del Grupo" CommandArgument='<%# grdGroups.Rows.Count%>' style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />
                        <asp:ImageButton ID="imgXML" runat="server" CausesValidation="False" 
                            CommandName="DeployXML" ImageUrl="~/img/icons/16x16/xml.png" Text="Configuraciones" 
                            ToolTip="Deploy de Configuraciones del Grupo" CommandArgument='<%# grdGroups.Rows.Count%>' style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />
                        <asp:ImageButton ID="imgIMG" runat="server" CausesValidation="False" 
                            CommandName="DeployIMG" ImageUrl="~/img/icons/16x16/images.png" Text="Imagenes" 
                            ToolTip="Deploy de Imagenes del Grupo" CommandArgument='<%# grdGroups.Rows.Count%>' style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />
                        <asp:ImageButton ID="imgTerminals" runat="server" CausesValidation="False" 
                            CommandName="Terminals" ImageUrl="~/img/icons/16x16/pos.png" Text="Terminales" 
                            ToolTip="Terminales del Grupo" CommandArgument='<%# grdGroups.Rows.Count%>' style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />
                        <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False" 
                            CommandName="DeleteAndroidGroup" ImageUrl="~/img/icons/16x16/delete.png" Text="Eliminar" 
                            ToolTip="Eliminar Grupo" CommandArgument='<%# grdGroups.Rows.Count%>' style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" 
                            OnClientClick="return ConfirmAction(this);" />
                    </ItemTemplate>
                </asp:TemplateField>
                
            </Columns>
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
                CssClass="pgr" Font-Underline="False" />
            <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#999999" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>
        
        <asp:GridView ID="grdTerminals" runat="server" AllowPaging="True" 
                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                DataSourceID="dsTerminals" ForeColor="#333333"
                CssClass="mGridCenter" DataKeyNames="ter_id, ter_dispositivo_id" 
                EmptyDataText="No existen Terminales Android creadas en el Grupo o con el filtro aplicado" 
                HorizontalAlign="Center">
                <RowStyle BackColor="White" ForeColor="White" />
                <Columns>
                    <asp:TemplateField HeaderText=" " SortExpression="ter_logo" HeaderStyle-Width="20px">
                        <ItemTemplate>
                            <asp:Image ID="imgUrlIcon" runat="server" ImageUrl='<%# Bind("ter_logo") %>' ToolTip='<%# Bind("ter_fecha_actualizacion_auto") %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="ter_dispositivo_id" HeaderText="Dispositivo ID - Heracles ID"  HeaderStyle-Width="120px"
                        SortExpression="ter_dispositivo_id" />
                    <asp:BoundField DataField="ter_ip_dispositivo" HeaderText="Dirección IP" 
                        SortExpression="ter_ip_dispositivo" />
                    <asp:BoundField DataField="ter_nombre_heracles" HeaderText="Nombre" 
                        SortExpression="ter_nombre_heracles" />
                    <asp:BoundField DataField="ter_ciudad_heracles" HeaderText="Ciudad" 
                        SortExpression="ter_ciudad_heracles" />
                    <asp:BoundField DataField="ter_region_heracles" HeaderText="Región" 
                        SortExpression="ter_region_heracles" />
                    <asp:BoundField DataField="ter_agencia_heracles" HeaderText="Agencia" 
                        SortExpression="ter_agencia_heracles" />
                    <asp:BoundField DataField="apk_banco" HeaderText="APK Banco" 
                        SortExpression="apk_banco" ItemStyle-HorizontalAlign ="Center" />
                    <asp:BoundField DataField="configuracion" HeaderText="Configuración" 
                        SortExpression="configuracion" ItemStyle-HorizontalAlign ="Center" />
                    <asp:BoundField DataField="imagenes" HeaderText="Imágenes" 
                        SortExpression="imagenes" ItemStyle-HorizontalAlign ="Center" />
                    <asp:TemplateField ShowHeader="False" HeaderStyle-Width="70px">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" 
                            CommandName="EditTerminal" ImageUrl="~/img/icons/16x16/edit.png" Text="Editar" 
                            ToolTip="Editar Terminal" CommandArgument='<%# grdTerminals.Rows.Count%>' style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />
                           
                            <asp:ImageButton ID="imgShowMap" runat="server" CausesValidation="False" 
                                CommandName="ShowTerminalMap" ImageUrl="~/img/icons/16x16/map.png" Text="Ubicación" 
                                ToolTip="Mostrar Ubicación en Mapa" CommandArgument='<%# grdTerminals.Rows.Count%>' style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />

                           
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
                    CssClass="pgr" Font-Underline="False" />
                <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid" 
                    BorderWidth="1px" />
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>            
            <br />
	        <asp:SqlDataSource ID="dsTerminals" runat="server" 
                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"                 
                SelectCommand="sp_webConsultarTerminalesAndroidGrupoPos" SelectCommandType="StoredProcedure"
                DeleteCommand="sp_webEliminarTerminalXGrupo" DeleteCommandType="StoredProcedure" ConflictDetection="OverwriteChanges" >
                <SelectParameters>
                    <asp:Parameter Name="groupId" Type="String" />
                    <asp:Parameter Name="terminalHeraclesId" Type="String" DefaultValue="-1"/>
                    <asp:Parameter Name="terminalIPAddress" Type="String" DefaultValue="-1"/>
                    <asp:Parameter Name="terminalName" Type="String" DefaultValue="-1"/>
                    <asp:Parameter Name="terminalSerial" Type="String" DefaultValue="-1"/>
                </SelectParameters>
                <DeleteParameters>
                    <asp:Parameter Name="terminalId" Type="String" />
                    <asp:Parameter Name="groupId" Type="String" />
                </DeleteParameters>
            </asp:SqlDataSource> 

	    <asp:SqlDataSource ID="dsListGroups" runat="server" 
            ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" 
            SelectCommand="sp_webConsultarGruposAndroid" SelectCommandType="StoredProcedure" >
            <SelectParameters>
                <asp:parameter Name="customerID" Type="Int64" DefaultValue="-1" />
                <asp:Parameter Name="groupName" Type="String" DefaultValue="-1" />
                <asp:Parameter Name="groupDesc" Type="String" DefaultValue="-1" />
            </SelectParameters>
	    </asp:SqlDataSource>

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">     
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel> 

        <asp:Panel ID="pnlError" runat="server" Visible="False">
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

    </div>
   
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Group/Manager.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
    <script language="javascript">
        var globalcontrol = '';

        function ConfirmAction(control) {

            globalcontrol = control;

            $.msgAlert({
                type: "warning"
                        , title: "Mensaje del sistema"
                        , text: "Realmente desea eliminar el grupo?"
                        , callback: function () {
                            __doPostBack($(globalcontrol).attr('name'), '');
                        }
            });

            return false;
        }
    </script>

</asp:Content>

