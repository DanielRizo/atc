﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Groups

Partial Class Group_ListAndroidGroups
    Inherits TeleLoader.Web.BasePage

    Dim groupObj As Group

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtNombre.Text = ""
            txtDesc.Text = ""
            txtNombre.Focus()
            txtfilterSerial.Focus()


            dsTerminals.SelectParameters("groupId").DefaultValue = objSessionParams.intSelectedGroup
            dsListGroups.SelectParameters("customerID").DefaultValue = objAccessToken.CustomerUserID
            grdTerminals.DataBind()

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False



        End If
    End Sub

    Protected Sub grdGroups_DataBound(sender As Object, e As EventArgs) Handles grdGroups.DataBound
        For Each row As GridViewRow In grdGroups.Rows

            'Check for Default Group Name
            If row.Cells(0).Text = ConfigurationSettings.AppSettings.Get("DefaultGroupName") Then

                CType(row.Cells(3).Controls(1), ImageButton).Visible = False
                CType(row.Cells(3).Controls(9), ImageButton).Visible = False
                CType(row.Cells(3).Controls(11), ImageButton).Visible = False
                CType(row.Cells(3).Controls(13), ImageButton).Visible = False
                CType(row.Cells(3).Controls(17), ImageButton).Visible = False

                CType(row.Cells(3).Controls(3), Label).Width = "20"
                CType(row.Cells(3).Controls(7), Label).Width = "67"
            End If

        Next
    End Sub

    Protected Sub grdGroups_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdGroups.RowCommand
        Try

            Select Case e.CommandName

                Case "EditAndroidGroup"
                    grdGroups.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    objSessionParams.intSelectedGroup = grdGroups.SelectedDataKey.Values.Item("gru_id")
                    objSessionParams.strtSelectedGroup = grdGroups.SelectedDataKey.Values.Item("gru_nombre")

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("EditAndroidGroup.aspx", False)

                Case "ShowTerminalsMap"
                    grdGroups.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    objSessionParams.intSelectedGroup = grdGroups.SelectedDataKey.Values.Item("gru_id")
                    objSessionParams.strtSelectedGroup = grdGroups.SelectedDataKey.Values.Item("gru_nombre")

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("GroupAndroidTerminalsMap.aspx", False)

                Case "DeployApps"
                    grdGroups.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    objSessionParams.intSelectedGroup = grdGroups.SelectedDataKey.Values.Item("gru_id")
                    objSessionParams.strtSelectedGroup = grdGroups.SelectedDataKey.Values.Item("gru_nombre")

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("GroupAndroidApplications.aspx", False)

                Case "DeployXML"
                    grdGroups.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    objSessionParams.intSelectedGroup = grdGroups.SelectedDataKey.Values.Item("gru_id")
                    objSessionParams.strtSelectedGroup = grdGroups.SelectedDataKey.Values.Item("gru_nombre")
                    objSessionParams.booleanIsAddFileTerminal = False

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("GroupAndroidConfigFiles.aspx", False)

                Case "DeployIMG"
                    grdGroups.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    objSessionParams.intSelectedGroup = grdGroups.SelectedDataKey.Values.Item("gru_id")
                    objSessionParams.strtSelectedGroup = grdGroups.SelectedDataKey.Values.Item("gru_nombre")

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("GroupAndroidImagesFiles.aspx", False)

                Case "Terminals"
                    grdGroups.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    objSessionParams.intSelectedGroup = grdGroups.SelectedDataKey.Values.Item("gru_id")
                    objSessionParams.strtSelectedGroup = grdGroups.SelectedDataKey.Values.Item("gru_nombre")

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("AndroidGroupTerminals.aspx", False)

                Case "DeleteAndroidGroup"
                    grdGroups.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    Dim AndroidGroup As String = grdGroups.SelectedDataKey.Values.Item("gru_nombre")

                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    groupObj = New Group(strConnectionString, grdGroups.SelectedDataKey.Values.Item("gru_id"))

                    'Delete Group
                    Dim result As Integer = groupObj.deleteGroup()

                    Select Case result
                        Case 1
                            'Group Deleted OK
                            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Grupo Eliminado. Datos[ Grupo ID=" & grdGroups.SelectedDataKey.Values.Item("gru_id") & " ]", "")

                            pnlMsg.Visible = True
                            pnlError.Visible = False
                            lblMsg.Text = "Grupo eliminado correctamente."

                            'Delete Folder with All Group Files
                            Try
                                Dim groupFolderPath As String = Server.MapPath(ConfigurationSettings.AppSettings.Get("GroupsFolderPath")) & AndroidGroup

                                If (System.IO.Directory.Exists(groupFolderPath)) Then
                                    Dim di As New System.IO.DirectoryInfo(groupFolderPath)
                                    di.Delete(True)
                                End If
                            Catch ex As Exception

                            End Try

                        Case 2
                            'Have Terminals
                            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Error Eliminando Grupo, posee terminales asociadas. Datos[ Grupo ID=" & grdGroups.SelectedDataKey.Values.Item("gru_id") & " ]", "")
                            pnlError.Visible = True
                            pnlMsg.Visible = False
                            lblError.Text = "Error eliminando grupo, posee terminales asociadas. Por favor valide los datos."
                        Case 3
                            'Have Applications
                            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Error Eliminando Grupo, posee aplicaciones asociadas. Datos[ Grupo ID=" & grdGroups.SelectedDataKey.Values.Item("gru_id") & " ]", "")
                            pnlError.Visible = True
                            pnlMsg.Visible = False
                            lblError.Text = "Error eliminando grupo, posee aplicaciones asociadas. Por favor valide los datos."
                        Case 4
                            'Have Deployed Files
                            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Error Eliminando Grupo, posee archivos desplegados asociadas. Datos[ Grupo ID=" & grdGroups.SelectedDataKey.Values.Item("gru_id") & " ]", "")
                            pnlError.Visible = True
                            pnlMsg.Visible = False
                            lblError.Text = "Error eliminando grupo, posee archivos desplegados asociados. Por favor valide los datos."
                        Case Else
                            'Default error
                            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Error Eliminando Grupo. Datos[ Grupo ID=" & grdGroups.SelectedDataKey.Values.Item("gru_id") & " ]", "")
                            pnlError.Visible = True
                            pnlMsg.Visible = False
                            lblError.Text = "Error eliminando grupo. Por favor valide los datos."
                    End Select

                    grdGroups.DataBind()

                    grdGroups.SelectedIndex = -1

                Case Else
                    objSessionParams.intSelectedGroup = -1
                    objSessionParams.strtSelectedGroup = ""

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    grdGroups.SelectedIndex = -1

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    grdGroups.SelectedIndex = -1

            End Select

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub

    Protected Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click

        If txtNombre.Text <> "" Then
            dsListGroups.SelectParameters("groupName").DefaultValue = txtNombre.Text
        Else
            dsListGroups.SelectParameters("groupName").DefaultValue = "-1"
        End If

        If txtDesc.Text <> "" Then
            dsListGroups.SelectParameters("groupDesc").DefaultValue = txtDesc.Text
        Else
            dsListGroups.SelectParameters("groupDesc").DefaultValue = "-1"
        End If

        If txtfilterSerial.Text <> "" Then
            dsTerminals.SelectParameters("terminalSerial").DefaultValue = txtfilterSerial.Text
        Else
            dsTerminals.SelectParameters("terminalSerial").DefaultValue = "-1"
        End If

        grdGroups.DataBind()
        grdTerminals.DataBind()

        If txtNombre.Text = "" And txtDesc.Text = "" Then
            ' Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
        Else
            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
        End If
    End Sub

    Protected Sub grdTerminals_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdTerminals.RowCommand
        Select Case e.CommandName

            Case "DeleteTerminal"
                'Validar Acceso a Función
                Try
                    objAccessToken.Validate(getCurrentPage(), "Delete")
                Catch ex As Exception
                    HandleErrorRedirect(ex)
                End Try

                grdTerminals.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                dsTerminals.DeleteParameters("terminalId").DefaultValue = grdTerminals.SelectedDataKey.Values.Item("ter_id")
                dsTerminals.DeleteParameters("groupId").DefaultValue = objSessionParams.intSelectedGroup

                Dim strData As String = "Terminal ID: " & grdTerminals.SelectedDataKey.Values.Item("ter_id") & ", grupo ID: " & objSessionParams.intSelectedGroup

                dsTerminals.Delete()

                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Terminal eliminada del Grupo Android. Datos[ " & strData & " ]", "")

                grdTerminals.DataBind()

                pnlError.Visible = False
                pnlMsg.Visible = True
                lblMsg.Text = "Terminal eliminada del Grupo Android"

                grdTerminals.SelectedIndex = -1

            Case "EditTerminal"
                grdTerminals.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                objSessionParams.intSelectedTerminalID = grdTerminals.SelectedDataKey.Values.Item("ter_id")

                'Set Data into Session
                Session("SessionParameters") = objSessionParams

                Response.Redirect("EditTerminalAndroidGroup.aspx", False)

            Case "ShowTerminalMap"
                grdTerminals.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                objSessionParams.intSelectedTerminalID = grdTerminals.SelectedDataKey.Values.Item("ter_id")

                'Set Data into Session
                Session("SessionParameters") = objSessionParams

                Response.Redirect("ShowTerminalAndroidGroupLocation.aspx", False)

            Case "EMVConfigTerminal"
                grdTerminals.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                objSessionParams.intSelectedTerminalID = grdTerminals.SelectedDataKey.Values.Item("ter_id")
                objSessionParams.strSelectedTerminalSerial = grdTerminals.SelectedDataKey.Values.Item("ter_serial")

                'Set Data into Session
                Session("SessionParameters") = objSessionParams

                Response.Redirect("EMVConfigItemsTerminal.aspx", False)

                    'Modificación Carga de Llaves; EB: 28/Feb/2018
            Case "InjectKey1"
                grdTerminals.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                objSessionParams.intSelectedTerminalID = grdTerminals.SelectedDataKey.Values.Item("ter_id")

                'Set Data into Session
                Session("SessionParameters") = objSessionParams

                Response.Redirect("InjectComponent1TerminalAndroidGroup.aspx", False)

                    'Modificación Carga de Llaves; EB: 28/Feb/2018
            Case "InjectKey2"
                grdTerminals.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                objSessionParams.intSelectedTerminalID = grdTerminals.SelectedDataKey.Values.Item("ter_id")

                'Set Data into Session
                Session("SessionParameters") = objSessionParams

                Response.Redirect("InjectComponent2TerminalAndroidGroup.aspx", False)

        End Select


        If txtfilterSerial.Text = "" Then
            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
        Else
            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
        End If
    End Sub
    Protected Sub dsTerminals_Deleting(sender As Object, e As SqlDataSourceCommandEventArgs) Handles dsTerminals.Deleting

        'Remover parametro extra, igual al datakeyname
        If e.Command.Parameters.Count > 2 Then


        End If

    End Sub


End Class
