﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ListGroups.aspx.vb" Inherits="Group_ListGroups" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Listar Grupos
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>
    <li>Listar Grupos</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    LISTAR GRUPOS
      
     <asp:HyperLink ID="HyperLink2" runat="server"
         NavigateUrl="~/Group/Manager.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <p>Este módulo le permite ver el listado de grupos y terminales del sistema Polaris:</p>
    <br />

    <div class="toggle-message" style="z-index: 590; top: 0px; left: 0px; color: Highlight;">
        <h3 class="title">Filtro de b&uacute;squeda...     ↓ ↓
            <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
        <div class="hide-message" id="hide-message" style="display: none;">

            <div class="st-form-line">
                <span class="st-labeltext"><b>Nombre: </b></span>
                <asp:TextBox ID="txtNombre" CssClass="st-forminput"
                    runat="server" TabIndex="1" MaxLength="100"
                    ToolTip="Nombre del Cliente" onkeydown="return (event.keyCode!=13);" Width="250px"></asp:TextBox>
            </div>
            <div class="st-form-line">
                <span class="st-labeltext"><b>Descripcion: </b></span>
                <asp:TextBox ID="txtDesc" CssClass="st-forminput"
                    runat="server" TabIndex="2" MaxLength="50"
                    ToolTip="Descripción del cliente" onkeydown="return (event.keyCode!=13);" Width="250px"></asp:TextBox>
            </div>
            <div class="st-form-line">
                <span class="st-labeltext"><b>Serial: </b></span>
                <asp:TextBox ID="txtSerial" CssClass="st-forminput"
                    runat="server" TabIndex="3" MaxLength="50"
                    ToolTip="Serial de la Terminal" onkeydown="return (event.keyCode!=13);" Width="250px"></asp:TextBox>
            </div>
            <div class="st-form-line">
                <span class="st-labeltext"><b>IMEI: </b></span>
                <asp:TextBox ID="txtImei" CssClass="st-forminput"
                    runat="server" TabIndex="4" MaxLength="50"
                    ToolTip="IMEI Módem de la Terminal" onkeydown="return (event.keyCode!=13);" Width="250px"></asp:TextBox>
                <div class="button-box">
                    <asp:Button ID="btnConsultar" runat="server" Text="Filtrar"
                        CssClass="button-aqua" TabIndex="5" ToolTip="Filtrar Clientes..." />
                </div>
            </div>
        </div>
    </div>

    <!-- START SIMPLE FORM -->

    <div class="simplebox grid960">

        <asp:GridView ID="grdGroups" runat="server" AllowPaging="True"
            AllowSorting="True" AutoGenerateColumns="False" CellPadding="4"
            DataSourceID="dsListGroups" ForeColor="#333333" Width="100%"
            CssClass="mGrid" DataKeyNames="gru_id,gru_nombre"
            EmptyDataText="No existen Grupos creados o con el filtro aplicado">
            <RowStyle BackColor="White" ForeColor="White" />

            <Columns>
                <asp:BoundField DataField="cli_nombre" HeaderText="Nombre del cliente"
                    SortExpression="cli_nombre" />
                <asp:BoundField DataField="gru_nombre" HeaderText="Nombre del grupo"
                    SortExpression="gru_nombre" />

                <asp:BoundField DataField="gru_total_terminales" HeaderText="Total dispositivos"
                    SortExpression="gru_total_terminales" ItemStyle-HorizontalAlign="Center" />
                <asp:TemplateField HeaderText="Actualizar" SortExpression="gru_actualizar">
                    <ItemTemplate>
                        <asp:CheckBox ID="editChk" runat="server" Checked='<%# Bind("gru_actualizar")%>' Enabled="false" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Programar fecha" SortExpression="gru_programar_fecha">
                    <ItemTemplate>
                        <asp:CheckBox ID="editChk2" runat="server" Checked='<%# Bind("gru_programar_fecha")%>' Enabled="false" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:BoundField DataField="gru_fecha_programada_actualizacion_ini" HeaderText="Rango fecha/hora inicial"
                    SortExpression="gru_fecha_programada_actualizacion_ini" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="gru_fecha_programada_actualizacion_fin" HeaderText="Rango fecha/hora final"
                    SortExpression="gru_fecha_programada_actualizacion_fin" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="gru_rango_hora_actualizacion_ini" HeaderText="Rango hora inicial"
                    SortExpression="gru_rango_hora_actualizacion_ini" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="gru_rango_hora_actualizacion_fin" HeaderText="Rango hora final"
                    SortExpression="gru_rango_hora_actualizacion_fin" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="gru_ip_descarga" HeaderText="IP descarga"
                    SortExpression="gru_ip_descarga" />
                <asp:BoundField DataField="gru_puerto_descarga" HeaderText="Puerto descarga"
                    SortExpression="gru_puerto_descarga" />

                <asp:TemplateField ShowHeader="False" HeaderStyle-Width="180px">
                    <ItemTemplate>
                        <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False"
                            CommandName="EditGroup" ImageUrl="~/img/icons/16x16/edit.png" Text="Editar"
                            ToolTip="Editar Grupo" CommandArgument='<%# grdGroups.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="" />
                        <asp:ImageButton ID="imgShowMap" runat="server" CausesValidation="False"
                            CommandName="ShowTerminalsMap" ImageUrl="~/img/icons/16x16/map.png" Text="Ubicación"
                            ToolTip="Mostrar Terminales en Mapa" CommandArgument='<%# grdGroups.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="" />
                        <asp:ImageButton ID="imgApps" runat="server" CausesValidation="False"
                            CommandName="Applications" ImageUrl="~/img/icons/16x16/application.png" Text="Aplicaciones"
                            ToolTip="Aplicaciones del Grupo" CommandArgument='<%# grdGroups.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="" />
                        <asp:ImageButton ID="imgAppsAndroid" runat="server" CausesValidation="False"
                            CommandName="DeployApps" ImageUrl="~/img/icons/16x16/android_on.png" Text="Aplicaciones"
                            ToolTip="Deploy de Aplicaciones del Grupo" CommandArgument='<%# grdGroups.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="" />
                        <asp:ImageButton ID="imgTerminals" runat="server" CausesValidation="False"
                            CommandName="Terminals" ImageUrl="~/img/icons/16x16/pos.png" Text="Terminales"
                            ToolTip="Terminales del Grupo" CommandArgument='<%# grdGroups.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="" />
                        <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False"
                            CommandName="DeleteGroup" ImageUrl="~/img/icons/16x16/delete.png" Text="Eliminar"
                            ToolTip="Eliminar Grupo" CommandArgument='<%# grdGroups.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass=""
                            OnClientClick="return ConfirmAction(this);" />
                    </ItemTemplate>
                </asp:TemplateField>

            </Columns>
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="aqua" />
            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
                CssClass="pgr" Font-Underline="False" />
            <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#999999" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>

        <asp:GridView ID="grdTerminals" runat="server" AllowPaging="True"
            AllowSorting="True" AutoGenerateColumns="False" CellPadding="4"
            DataSourceID="dsTerminals" ForeColor="#333333"
            CssClass="mGridCenter" DataKeyNames="ter_id, ter_serial"
            EmptyDataText=""
            HorizontalAlign="Center">
            <RowStyle BackColor="White" ForeColor="White" />
            <Columns>
                <asp:TemplateField HeaderText=" " SortExpression="ter_logo" HeaderStyle-Width="20px">
                    <ItemTemplate>
                        <asp:Image ID="imgUrlIcon" runat="server" ImageUrl='<%# Bind("ter_logo") %>' ToolTip='<%# Bind("ter_fecha_actualizacion_auto") %>' />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:BoundField DataField="ter_serial" HeaderText="Serial"
                    SortExpression="ter_serial" />
                <asp:BoundField DataField="ter_modelo" HeaderText="Modelo"
                    SortExpression="ter_modelo" />
                <asp:BoundField DataField="ter_imei" HeaderText="IMEI"
                    SortExpression="ter_imei" />
                <asp:TemplateField HeaderText="Prioridad Grupo" SortExpression="ter_prioridad_grupo">
                    <ItemTemplate>
                        <asp:CheckBox ID="editChk1" runat="server" Checked='<%# Bind("ter_prioridad_grupo")%>' Enabled="false" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Actualizar Manual" SortExpression="ter_actualizar">
                    <ItemTemplate>
                        <asp:CheckBox ID="editChk2" runat="server" Checked='<%# Bind("ter_actualizar")%>' Enabled="false" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:BoundField DataField="ter_fecha_programacion" HeaderText="Fecha Manual Descarga"
                    SortExpression="ter_fecha_programacion" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="ter_ip_descarga" HeaderText="IP Descarga"
                    SortExpression="ter_ip_descarga" />
                <asp:BoundField DataField="ter_puerto_descarga" HeaderText="Puerto Descarga"
                    SortExpression="ter_puerto_descarga" />
                <asp:BoundField DataField="ter_estado_actualizacion" HeaderText="Estado Actualización"
                    SortExpression="ter_estado_actualizacion" ItemStyle-HorizontalAlign="Center" />
                <asp:TemplateField HeaderText="Estado" SortExpression="ter_estado">
                    <ItemTemplate>
                        <asp:CheckBox ID="editChk3" runat="server" Checked='<%# Bind("ter_estado")%>' Enabled="false" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False" HeaderStyle-Width="100px">
                    <ItemTemplate>
                        <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False"
                            CommandName="EditTerminal" ImageUrl="~/img/icons/16x16/edit.png" Text="Editar"
                            ToolTip="Editar Terminal" CommandArgument='<%# grdTerminals.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />
                        <asp:ImageButton ID="imgShowMap" runat="server" CausesValidation="False"
                            CommandName="ShowTerminalMap" ImageUrl="~/img/icons/16x16/map.png" Text="Ubicación"
                            ToolTip="Mostrar Ubicación en Mapa" CommandArgument='<%# grdTerminals.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />
                        <asp:ImageButton ID="imgEMVConfig" runat="server" CausesValidation="False"
                            CommandName="EMVConfigTerminal" ImageUrl="~/img/icons/16x16/emv.png" Text="EMV Config."
                            ToolTip="Configurar Parámetros EMV" CommandArgument='<%# grdTerminals.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />
                        <asp:ImageButton ID="ImgTerminalStis" runat="server" CausesValidation="False"
                            CommandName="terminalStis" ImageUrl="~/img/icons/16x16/nuevo.png" Text=""
                            ToolTip="Parametros EMV" CommandArgument='<%# grdTerminals.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
            </Columns>
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
                CssClass="pgr" Font-Underline="False" />
            <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid"
                BorderWidth="1px" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>
        <br />
        <asp:SqlDataSource ID="dsTerminals" runat="server"
            ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
            SelectCommand="sp_webConsultarTerminalesPos" SelectCommandType="StoredProcedure"
            DeleteCommand="sp_webEliminarTerminalXGrupo" DeleteCommandType="StoredProcedure">
            <SelectParameters>
                <asp:Parameter Name="customer" Type="String" />
                <asp:Parameter Name="groupId" Type="String" />
                <asp:Parameter Name="terminalSerial" Type="String" DefaultValue="-1" />
                <asp:Parameter Name="terminalImei" Type="String" DefaultValue="-1" />
            </SelectParameters>
            <DeleteParameters>
                <asp:Parameter Name="terminalId" Type="String" />
                <asp:Parameter Name="groupId" Type="String" />
            </DeleteParameters>
        </asp:SqlDataSource>
    </div>

    <asp:SqlDataSource ID="dsListGroups" runat="server"
        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
        SelectCommand="sp_webConsultarGrupos" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:Parameter Name="customerID" Type="Int64" DefaultValue="-1" />
            <asp:Parameter Name="groupName" Type="String" DefaultValue="-1" />
            <asp:Parameter Name="groupDesc" Type="String" DefaultValue="-1" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:Panel ID="pnlMsg" runat="server" Visible="False">
        <div class="albox succesbox">
            <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
            <a href="#" class="close tips" title="Cerrar">Cerrar</a>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlError" runat="server" Visible="False">
        <div class="albox errorbox">
            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
            <a href="#" class="close tips" title="Cerrar">Cerrar</a>
        </div>
    </asp:Panel>

</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

    <script language="javascript">
        var globalcontrol = '';

        function ConfirmAction(control) {

            globalcontrol = control;

            $.msgAlert({
                type: "warning"
                , title: "Mensaje del sistema"
                , text: "Realmente desea eliminar el grupo ?"
                , callback: function () {
                    __doPostBack($(globalcontrol).attr('name'), '');
                }
            });

            return false;
        }

    </script>

</asp:Content>

