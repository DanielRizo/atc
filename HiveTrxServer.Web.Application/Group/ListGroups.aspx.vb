﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Groups

Partial Class Group_ListGroups
    Inherits TeleLoader.Web.BasePage

    Dim groupObj As Group

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtNombre.Text = ""
            txtDesc.Text = ""
            txtNombre.Focus()
            txtSerial.Focus()

            dsTerminals.SelectParameters("groupId").DefaultValue = objSessionParams.intSelectedGroup
            dsListGroups.SelectParameters("customerID").DefaultValue = objAccessToken.CustomerUserID
            grdTerminals.DataBind()

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False

        End If
    End Sub
    Protected Sub grdGroups_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdGroups.RowCommand
        Try
            Select Case e.CommandName

                Case "EditGroup"
                    grdGroups.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    objSessionParams.intSelectedGroup = grdGroups.SelectedDataKey.Values.Item("gru_id")
                    objSessionParams.strtSelectedGroup = grdGroups.SelectedDataKey.Values.Item("gru_nombre")

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("EditGroup.aspx", False)

                Case "ShowTerminalsMap"
                    grdGroups.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    objSessionParams.intSelectedGroup = grdGroups.SelectedDataKey.Values.Item("gru_id")
                    objSessionParams.strtSelectedGroup = grdGroups.SelectedDataKey.Values.Item("gru_nombre")

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("GroupTerminalsMap.aspx", False)

                Case "Applications"
                    grdGroups.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    objSessionParams.intSelectedGroup = grdGroups.SelectedDataKey.Values.Item("gru_id")
                    objSessionParams.strtSelectedGroup = grdGroups.SelectedDataKey.Values.Item("gru_nombre")

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("GroupApplications.aspx", False)
                Case "DeployApps"
                    grdGroups.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    objSessionParams.intSelectedGroup = grdGroups.SelectedDataKey.Values.Item("gru_id")
                    objSessionParams.strtSelectedGroup = grdGroups.SelectedDataKey.Values.Item("gru_nombre")

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("GroupAndroidApplications.aspx", False)
                Case "Terminals"
                    grdGroups.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    objSessionParams.intSelectedGroup = grdGroups.SelectedDataKey.Values.Item("gru_id")
                    objSessionParams.strtSelectedGroup = grdGroups.SelectedDataKey.Values.Item("gru_nombre")

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("GroupTerminals.aspx", False)

                Case "DeleteGroup"
                    grdGroups.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    Dim AndroidGroup As String = grdGroups.SelectedDataKey.Values.Item("gru_nombre")

                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    groupObj = New Group(strConnectionString, grdGroups.SelectedDataKey.Values.Item("gru_id"))

                    'Delete Group
                    Dim result As Integer = groupObj.deleteGroup()

                    Select Case result
                        Case 1
                            'Group Deleted OK
                            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Grupo Eliminado. Datos[ Grupo ID=" & grdGroups.SelectedDataKey.Values.Item("gru_id") & " ]", "")

                            pnlMsg.Visible = True
                            pnlError.Visible = False
                            lblMsg.Text = "Grupo eliminado correctamente."

                            'Delete Folder with All Group Files
                            Try
                                Dim groupFolderPath As String = Server.MapPath(ConfigurationSettings.AppSettings.Get("GroupsFolderPath")) & AndroidGroup

                                If (System.IO.Directory.Exists(groupFolderPath)) Then
                                    Dim di As New System.IO.DirectoryInfo(groupFolderPath)
                                    di.Delete(True)
                                End If
                            Catch ex As Exception

                            End Try

                        Case 2
                            'Have Terminals
                            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Error Eliminando Grupo, posee terminales asociadas. Datos[ Grupo ID=" & grdGroups.SelectedDataKey.Values.Item("gru_id") & " ]", "")
                            pnlError.Visible = True
                            pnlMsg.Visible = False
                            lblError.Text = "Error eliminando grupo, posee terminales asociadas. Por favor valide los datos."
                        Case 3
                            'Have Applications
                            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Error Eliminando Grupo, posee aplicaciones asociadas. Datos[ Grupo ID=" & grdGroups.SelectedDataKey.Values.Item("gru_id") & " ]", "")
                            pnlError.Visible = True
                            pnlMsg.Visible = False
                            lblError.Text = "Error eliminando grupo, posee aplicaciones asociadas. Por favor valide los datos."
                        Case 4
                            'Have Deployed Files
                            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Error Eliminando Grupo, posee archivos desplegados asociadas. Datos[ Grupo ID=" & grdGroups.SelectedDataKey.Values.Item("gru_id") & " ]", "")
                            pnlError.Visible = True
                            pnlMsg.Visible = False
                            lblError.Text = "Error eliminando grupo, posee archivos desplegados asociados. Por favor valide los datos."
                        Case Else
                            'Default error
                            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Error Eliminando Grupo. Datos[ Grupo ID=" & grdGroups.SelectedDataKey.Values.Item("gru_id") & " ]", "")
                            pnlError.Visible = True
                            pnlMsg.Visible = False
                            lblError.Text = "Error eliminando grupo. Por favor valide los datos."
                    End Select

                    grdGroups.DataBind()

                    grdGroups.SelectedIndex = -1


                Case Else
                    objSessionParams.intSelectedGroup = -1
                    objSessionParams.strtSelectedGroup = ""

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    grdGroups.SelectedIndex = -1

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    grdGroups.SelectedIndex = -1


            End Select

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub

    Protected Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click

        If txtNombre.Text <> "" Then
            dsListGroups.SelectParameters("groupName").DefaultValue = txtNombre.Text

        Else
            dsListGroups.SelectParameters("groupName").DefaultValue = "-1"
        End If
        If txtSerial.Text <> "" Then
            dsTerminals.SelectParameters("terminalSerial").DefaultValue = txtSerial.Text
        Else
            dsTerminals.SelectParameters("terminalSerial").DefaultValue = "-1"
        End If
        If txtImei.Text <> "" Then
            dsTerminals.SelectParameters("terminalImei").DefaultValue = txtImei.Text
        Else
            dsTerminals.SelectParameters("terminalImei").DefaultValue = "-1"
        End If

        If txtDesc.Text <> "" Then
            dsListGroups.SelectParameters("groupDesc").DefaultValue = txtDesc.Text
        Else
            dsListGroups.SelectParameters("groupDesc").DefaultValue = "-1"
        End If
        dsTerminals.SelectParameters("customer").DefaultValue = objAccessToken.Customer
        grdGroups.DataBind()
        grdTerminals.DataBind()

        If txtSerial.Text = "" And txtImei.Text = "" Then
            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
        Else
            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
        End If

        If txtNombre.Text = "" And txtDesc.Text = "" Then
            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
        Else
            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
        End If


    End Sub
    Protected Sub grdTerminals_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdTerminals.RowCommand
        Select Case e.CommandName


            Case "EditTerminal"
                grdTerminals.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                objSessionParams.intSelectedTerminalID = grdTerminals.SelectedDataKey.Values.Item("ter_id")

                'Set Data into Session
                Session("SessionParameters") = objSessionParams

                Response.Redirect("EditTerminalGroup.aspx", False)

            Case "ShowTerminalMap"
                grdTerminals.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                objSessionParams.intSelectedTerminalID = grdTerminals.SelectedDataKey.Values.Item("ter_id")

                'Set Data into Session
                Session("SessionParameters") = objSessionParams

                Response.Redirect("ShowTerminalGroupLocation.aspx", False)

            Case "Applications"
                grdGroups.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                objSessionParams.intSelectedGroup = grdGroups.SelectedDataKey.Values.Item("gru_id")
                objSessionParams.strtSelectedGroup = grdGroups.SelectedDataKey.Values.Item("gru_nombre")

                'Set Data into Session
                Session("SessionParameters") = objSessionParams

                Response.Redirect("GroupApplications.aspx", False)

            Case "Terminals"
                grdGroups.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                objSessionParams.intSelectedGroup = grdGroups.SelectedDataKey.Values.Item("gru_id")
                objSessionParams.strtSelectedGroup = grdGroups.SelectedDataKey.Values.Item("gru_nombre")

            Case "terminalStis"
                'Validamos el Tid asociado al Tid'

                grdTerminals.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                objSessionParams.strSelectedTerminalSerial = grdTerminals.SelectedDataKey.Values.Item("ter_serial")

                If GetTid(objSessionParams.strSelectedTerminalSerial) Then
                    'New Group Edited OK
                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Load", "Tid Encontrado. Datos[ " & getFormDataLog() & " ]", "")
                    'Llenamos la variable de sesion con el resultado del Sp
                    Session("SessionParameters") = objSessionParams
                    'Tree View
                    Response.Redirect("EditTerminalstis.aspx", False)
                Else
                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Load", "Tid no encontrado. Datos[ " & getFormDataLog() & " ]", "")
                    pnlError.Visible = True
                    pnlMsg.Visible = False
                    lblError.Text = "Tid no relacionado para el serial." & grdTerminals.SelectedDataKey.Values.Item("ter_serial")
                End If




            Case "DeleteTerminal"
                'Validar Acceso a Función
                Try
                    objAccessToken.Validate(getCurrentPage(), "Delete")
                Catch ex As Exception
                    HandleErrorRedirect(ex)
                End Try

                grdTerminals.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                dsTerminals.DeleteParameters("terminalId").DefaultValue = grdTerminals.SelectedDataKey.Values.Item("ter_id")
                dsTerminals.DeleteParameters("groupId").DefaultValue = objSessionParams.intSelectedGroup

                Dim strData As String = "Terminal ID: " & grdTerminals.SelectedDataKey.Values.Item("ter_id") & ", grupo ID: " & objSessionParams.intSelectedGroup

                dsTerminals.Delete()

                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Terminal eliminada del Grupo. Datos[ " & strData & " ]", "")

                grdTerminals.DataBind()

                pnlError.Visible = False
                pnlMsg.Visible = True
                lblMsg.Text = "Terminal eliminada del Grupo"

                grdTerminals.SelectedIndex = -1


            Case "EMVConfigTerminal"
                grdTerminals.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                objSessionParams.intSelectedTerminalID = grdTerminals.SelectedDataKey.Values.Item("ter_id")
                objSessionParams.strSelectedTerminalSerial = grdTerminals.SelectedDataKey.Values.Item("ter_serial")

                'Set Data into Session
                Session("SessionParameters") = objSessionParams

                Response.Redirect("EMVConfigItemsTerminal.aspx", False)

        End Select

        If txtSerial.Text = "" And txtImei.Text = "" Then
            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
        Else
            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
        End If


    End Sub
    Public Function GetTid(ByVal Serial As String) As Boolean
        Dim connection As New SqlConnection(strConnectionString)
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As String = ""
        Dim retVal As Boolean

        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_validarTerminalId"
            command.Parameters.Clear()
            command.Parameters.Add(New SqlParameter("TermianlSerial", Serial))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetString(0)
                    objSessionParams.StrTerminalRecord = results.GetString(1)
                    objSessionParams.StrTerminalID = results.GetString(2)
                End While
            Else
                retVal = False
            End If

            If status = "TID NO ASOCIADO" Then
                retVal = False
            ElseIf status = "SERIAL NO EXISTE" Then
                retVal = False
            ElseIf status = "TID CORRECTO" Then
                retVal = True
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try
        Return retVal
    End Function
End Class
