﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ListarAcquirer.aspx.vb" Inherits="Groups_ListAcquirer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Parametros Acquierer STIS
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>

    <li>PARAMETROS POR ADQUIRIENTE</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
     Acquirer: [ <%= objSessionParams.StrAcquirerCode %> ]
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    
    <br />

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlError" runat="server" Visible="False">
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>
        <div class="toggle-message" style="z-index: 590; top: 0px; left: 0px;">
            <h3 class="title">Filtro de B&uacute;squeda...
                <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
            <div class="hide-message" id="hide-message" style="display: none;">
                <div class="st-form-line">
                    <span class="st-labeltext"><b>ReferCode No: </b></span>
                    <asp:TextBox ID="txtReferCode" CssClass="st- "
                        runat="server" TabIndex="1" MaxLength="50"
                        ToolTip="ReferCode No" onkeydown="return (event.keyCode!=13);" Width="250px"></asp:TextBox>
                </div>
                <div class="button-box">
                    <asp:Button ID="btnConsultar" runat="server" Text="Filtrar"
                        CssClass="button-aqua" TabIndex="3" ToolTip="Filtrar Paramtros Acquirer Stis..." />
                </div>
            </div>
        </div>

    </div>

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
     <%--   <div class="titleh">
            <h3>Adquiriente Terminal</h3>
            <div class="shortcuts-icons" style="z-index: 660;">
           
                <br />
            </div>
        </div>--%>
        <div class="st-form-line">
            <span class="st-labeltext"><b>CODE:</b></span>

            <asp:TextBox ID="txtCode" CssClass="st-success-input" Style="width: 510px"
                runat="server" TabIndex="1" MaxLength="100"
                ToolTip="Code." onkeydown="return (event.keyCode!=13);"
                Enabled="False"></asp:TextBox>

        </div>
        <div class="st-form-line">
            <span class="st-labeltext"><b>NAME:</b></span>

            <asp:TextBox ID="TxtNameAcquirer" CssClass="st-success-input" Style="width: 510px"
                runat="server" TabIndex="1" MaxLength="100"
                ToolTip="Code." onkeydown="return (event.keyCode!=13);"
                Enabled="False"></asp:TextBox>
       </div>
        
         <ul id="navst">
	<li class="current"><a href="TerminalStis.aspx">Polaris TMS</a></li>
	<li><a>Tables</a>
		<ul>
			<li><a href="TerminalAcquirer.aspx">Acquirer</a></li>
			<li><a href="TerminalIssuer.aspx">Issuer</a>	</li>
			<li><a href="TerminalCardRange.aspx">CardRange</a></li>
			<li><a href="TerminalEmvLevel2.aspx">Emv Level 2 Application</a></li>
			<li><a href="TerminalEmvKey.aspx">Emv Level 2 Key</a></li>
			<li><a href="TerminalExtraApplication.aspx">Extra Application Parameter</a></li>

		</ul>
	</li>
	
</ul>
          <br />
        <div class="body">
             <div id="posStis" style="overflow: auto; width: auto; height: 660px;">
            <asp:GridView ID="grdTerminalsParameterAcquirer" runat="server"
                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4"
                DataSourceID="dsTerminalsParameter"
                CssClass="mGridCenter" 
                EmptyDataText="No existen Adquirientes creados con el filtro aplicado"
                HorizontalAlign="Left" GridLines="None" ForeColor="#333333" Width="1059px"> 
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <%--<asp:CommandField ShowEditButton="True" ButtonType="Button" AccessibleHeaderText="EditParameter" HeaderImageUrl="~/img/icons/16x16/edit.png" SelectImageUrl="~/img/icons/16x16/edit.png"  />--%>
                    <asp:BoundField DataField="TABLE_KEY_CODE" HeaderText="Table Key Code" 
                        SortExpression="TABLE_KEY_CODE"/>
                    <asp:BoundField DataField="IS_EXTENDED" HeaderText="Parameter Type"
                        SortExpression="IS_EXTENDED" ReadOnly="True" />
                    <asp:BoundField DataField="FIELD_DISPLAY_NAME" HeaderText="Parameter Name"
                        SortExpression="FIELD_DISPLAY_NAME"/>
                    <asp:BoundField DataField="CONTENT_DESC" HeaderText="Value"
                        SortExpression="CONTENT_DESC" />
                    <asp:BoundField DataField="CONTENT_TYPE" HeaderText="Value Type"
                        SortExpression="CONTENT_TYPE" ReadOnly="True"/>
                    <asp:BoundField DataField="NOTE" HeaderText="Note"
                        SortExpression="NOTE" ReadOnly="True"/>
                  <%--  <asp:TemplateField ShowHeader="False" HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False"
                                CommandName="EditTerminalParameter" ImageUrl="~/img/icons/16x16/edit.png" Text="Editar"
                                ToolTip="Editar Acquirer" CommandArgument='<%# grdTerminalsParameterAcquirer.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" >
                        </ItemTemplate>

                        <HeaderStyle Width="100px"></HeaderStyle>

                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>--%>

                </Columns>

                <FooterStyle BackColor="#1C5E55" ForeColor="White" Font-Bold="True" />
                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center"
                    CssClass="pgr" />
                <RowStyle BackColor="#E3EAEB" />
                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BorderColor="#666666" BorderStyle="Solid"
                    BorderWidth="1px" BackColor="#50A2A3" />
                <SortedAscendingCellStyle BackColor="#F8FAFA" />
                <SortedAscendingHeaderStyle BackColor="#246B61" />
                <SortedDescendingCellStyle BackColor="#D4DFE1" />
                <SortedDescendingHeaderStyle BackColor="#15524A" />
            </asp:GridView>
            <br />
            <asp:SqlDataSource ID="dsTerminalsParameter" runat="server"
                ConnectionString="<%$ ConnectionStrings:TeleLoaderStisConnectionString %>"
                    SelectCommand="sp_webListarParameterAcquirer_Stis" SelectCommandType="StoredProcedure" UpdateCommand="sp_webEditParameterAcquirer_Stis" UpdateCommandType="StoredProcedure">
                <SelectParameters>
                    <asp:Parameter Name="TABLE_KEY_CODE" Type="string" />
                    <asp:Parameter Name="CONTENT_DESC" Type="string" DefaultValue="-1"/>
                   
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="TABLE_KEY_CODE" Type="String" />
                    <asp:Parameter Name="FIELD_DISPLAY_NAME" Type="String" />
                    <asp:Parameter Name="CONTENT_DESC" Type="String" />

                </UpdateParameters>
            </asp:SqlDataSource>
        </div>
            </div>
    </div>
    <asp:HyperLink ID="HyperLink1" runat="server"
        NavigateUrl="~/Group/TerminalAcquirer.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>


<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

    <script language="javascript">
        var globalcontrol = '';

        function ConfirmAction(control) {

            globalcontrol = control;

            $.msgAlert({
                type: "warning"
                , title: "Mensaje del sistema"
                , text: "Realmente desea eliminar el adquiriente?"
                , callback: function () {
                    __doPostBack($(globalcontrol).attr('name'), '');
                }
            });

            return false;
        }
    </script>

</asp:Content>

