﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ListarExtraApplicationParameter.aspx.vb" Inherits="Groups_ListExtraApplicationParameter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> ::EXTRA APPLICATION STIS 
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

        <%--scripts edit table--%>
  
  <link rel="stylesheet" type="text/css" href="/style/bootstrap.min.css" />

      <script type="text/javascript" src='<%= ResolveClientUrl("~/js/editTable.js") %>'></script>
   
    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.tabledit.js") %>'></script>
    
     <script type="text/javascript" src='<%= ResolveClientUrl("~/js/bootstrap.js") %>'></script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>

    <li>EXTRA APPLICATION STIS</li>
</asp:Content>
<%--<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    Extra Application: [ <%= objSessionParams.StrExtraParameter %> ]
</asp:Content>--%>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <br />

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlError" runat="server" Visible="False">
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>
        <div class="toggle-message" style="z-index: 590; top: 0px; left: 0px;">
            <h4 class="title">Filtro de B&uacute;squeda...
                <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h4>
            <div class="hide-message" id="hide-message" style="display: none;">
                <div class="st-form-line">
                    <span class="st-labeltext"><b>Application Name: </b></span>
                    <asp:TextBox ID="txtExtraApp" CssClass="st- "
                        runat="server" TabIndex="1" MaxLength="50"
                        ToolTip="Application Name" onkeydown="return (event.keyCode!=13);" Width="250px"></asp:TextBox>
                </div>
                <div class="button-box">
                    <asp:Button ID="btnConsultar" runat="server" Text="Filtrar"
                        CssClass="button-aqua" TabIndex="3" ToolTip="Filtrar Paramtros Acquirer Stis..." />
                </div>
            </div>
        </div>

    </div>

    <!-- START SIMPLE FORM -->
    <div >
        <div class="title">
            <h4>Extra Application Parameter</h4>
            <div class="shortcuts-icons" style="z-index: 660;">
               <%-- <p>Tipo de Parametro "0" STANDARD</p>
                <p>Tipo de Parametro "1" EXTENDED</p>--%>
                <br />
            </div>
        </div>
        <div class="st-form-line">
            <span class="st-labeltext"><b>CODE:</b></span>

            <asp:TextBox ID="txtApplicationName" CssClass="st-success-input" Style="width: 510px"
                runat="server" TabIndex="1" MaxLength="100"
                ToolTip="Code." onkeydown="return (event.keyCode!=13);"
                Enabled="False"></asp:TextBox>

        </div>
              <ul id="navst">
	<li class="current"><a href="TerminalStis.aspx">Polaris TMS</a></li>
	<li><a>Tables</a>
		<ul>
			<li><a href="TerminalAcquirer.aspx">Acquirer</a></li>
			<li><a href="TerminalIssuer.aspx">Issuer</a>	</li>
			<li><a href="TerminalCardRange.aspx">CardRange</a></li>
			<li><a href="TerminalEmvLevel2.aspx">Emv Level 2 Application</a></li>
			<li><a href="TerminalEmvKey.aspx">Emv Level 2 Key</a></li>
			<li><a href="TerminalExtraApplication.aspx">Extra Application Parameter</a></li>

		</ul>
	</li>
	
</ul>

              <script type="text/javascript">
        $(document).ready(function () {
    //enableEditTable("#ctl00_MainContent_grdTerminalsExtraApplication", true,true,false, [[2, 'Value'], [3, 'City', '{"1": "Opcion 1", "2": "Opcion 2", "3": "Opcion 3"}']], "addAcquirerTbl");

    //$("#btnAgregar").click(function () {

    //    var grd = document.getElementById('ctl00_MainContent_grdTerminalsExtraApplication');
    //    var tbod = grd.rows[0].parentNode;
    //    var newRow = grd.rows[grd.rows.length - 1].cloneNode(true);
    //    newRow.innerHTML = '<td> </td> <td>app</td> <td>name</td> <td>desc</td> <td>value</td> <td>input</td><td style="white-space: nowrap; width: 1%;"><div class="tabledit-toolbar btn-toolbar" style="text-align: left;"><div class="btn-group btn-group-sm" style="float: none;"><button type="button" class="tabledit-edit-button btn btn-sm btn-info" style="float: none;"><img src="../img/icons/16x16/edit.png" alt="icon" class="d-icon"></button><button type="button" class="tabledit-delete-button btn btn-sm btn-danger" style="float: none;"><img src="../img/icons/16x16/delete.png" alt="icon" class="d-icon"></button></div><button id="btnSave" type="button" class="tabledit-save-button btn btn-sm btn-success" style="display: none; float: none;"><img src="../img/icons/16x16/ok.png" alt="icon" class="d-icon"></button><button type="button" class="tabledit-confirm-button btn btn-sm btn-danger" style="display: none; float: none;">Confirmaaaaar</button><button type="button" class="tabledit-restore-button btn btn-sm btn-warning" style="display: none; float: none;">Restore</button></div></td>';
    //    var len = grd.rows.length;
        
    //    tbod.appendChild(newRow);
    //    tbod.appendChild();
    //    document.getElementById("ctl00_MainContent_grdTerminalsExtraApplication").insertRow(-1).innerHTML = '<td>1</td><td>2</td>';
    //    var r = grd.lastChild().getRow;

    //    return false;
    });



});
        
        </script>
        <div class="body">
            <div id="posStis" style="overflow: auto; width: auto; height: 660px;">
            <br />
                <%--<button id="btnAgregar" type="button" class="btn btn-block btn-info"><img src="../img/icons/16x16/plus.png" alt="icon" class="d-icon" />Agregar nuevo parametro</button>--%>
            <asp:GridView ID="grdTerminalsExtraApplication" runat="server" AllowPaging="True"
                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4"
                DataSourceID="dsTerminalsExtraParameter"
                CssClass="mGridCenter" 
                EmptyDataText="No existen Issuer creados con el filtro aplicado"
                HorizontalAlign="Left" GridLines="None" ForeColor="#333333" >
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <%--<asp:CommandField ButtonType="Image"  HeaderImageUrl="~/img/icons/16x16/edit.png" ShowEditButton="True" UpdateImageUrl="~/img/icons/16x16/ok.png" CancelImageUrl="~/img/icons/16x16/cancel.png" editimageurl="~/img/icons/16x16/edit.png" />--%>
                    <asp:BoundField DataField="PARA_INDEX" HeaderText="Index"
                        SortExpression="PARA_INDEX" />
                    <asp:BoundField DataField="APP_PARA_NAME" HeaderText="Application Name"
                        SortExpression="APP_PARA_NAME" />
                    <asp:BoundField DataField="PARA_NAME" HeaderText="Name"
                        SortExpression="PARA_NAME" />

                    <asp:BoundField DataField="PARA_DESC" HeaderText="Description"
                        SortExpression="PARA_DESC" />
                    <asp:BoundField DataField="PARA_VALUE" HeaderText="value (Click To Change)"
                        SortExpression="PARA_VALUE" />
                    <asp:BoundField DataField="APP_PARA_MASK" HeaderText="Input Mask"
                        SortExpression="APP_PARA_MASK" readonly="true"/>
                 <%--   <asp:TemplateField ShowHeader="False" HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False"
                                CommandName="EditTerminalExtraApplication" ImageUrl="~/img/icons/16x16/edit.png" Text="Editar"
                                ToolTip="Editar ExtraApplication" CommandArgument='<%# grdTerminalsExtraApplication.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />
                        </ItemTemplate>

                        <HeaderStyle Width="100px"></HeaderStyle>

                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>--%>

                </Columns>

                <FooterStyle BackColor="#1C5E55" ForeColor="White" Font-Bold="True" />
                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center"
                    CssClass="pgr" />
                <RowStyle BackColor="#E3EAEB" />
                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BorderColor="#666666" BorderStyle="Solid"
                    BorderWidth="1px" BackColor="#50A2A3" />
                <SortedAscendingCellStyle BackColor="#F8FAFA" />
                <SortedAscendingHeaderStyle BackColor="#246B61" />
                <SortedDescendingCellStyle BackColor="#D4DFE1" />
                <SortedDescendingHeaderStyle BackColor="#15524A" />
            </asp:GridView>
            <br />
                </div>
            <asp:SqlDataSource ID="dsTerminalsExtraParameter" runat="server"
                ConnectionString="<%$ ConnectionStrings:TeleLoaderStisConnectionString %>"
                SelectCommand="sp_webListarExtraApplication_Stis" SelectCommandType="StoredProcedure" UpdateCommand="sp_webEditParameterApplication_Stis" UpdateCommandType="StoredProcedure">
                <SelectParameters>
                    <asp:Parameter Name="APP_PARA_NAME" Type="string" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="PARA_INDEX" Type="String" />
                    <asp:Parameter Name="APP_PARA_NAME" Type="String" />
                    <asp:Parameter Name="PARA_NAME" Type="String" />
                    <asp:Parameter Name="PARA_DESC" Type="String" />
                    <asp:Parameter Name="PARA_VALUE" Type="String" />
                 
                </UpdateParameters>
            </asp:SqlDataSource>
        </div>
    </div>
    <asp:HyperLink ID="HyperLink1" runat="server"
        NavigateUrl="~/Group/TerminalExtraApplication.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>


<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

    <script language="javascript">
        var globalcontrol = '';

        function ConfirmAction(control) {

            globalcontrol = control;

            $.msgAlert({
                type: "warning"
                , title: "Mensaje del Sistema"
                , text: "Realmente desea eliminar El Extra Parameter?"
                , callback: function () {
                    __doPostBack($(globalcontrol).attr('name'), '');
                }
            });

            return false;
        }
    </script>

</asp:Content>

