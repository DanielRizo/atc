﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Applications
Imports System.Data.Common

Partial Class Groups_ListExtraApplicationParameter
    Inherits TeleLoader.Web.BasePage

    Dim applicationObj As ApplicationExtraApplication

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then
            txtApplicationName.Text = objSessionParams.StrExtraParameter
            txtExtraApp.Focus()
            dsTerminalsExtraParameter.SelectParameters("APP_PARA_NAME").DefaultValue = objSessionParams.StrExtraParameter
            grdTerminalsExtraApplication.DataBind()
        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub grdTerminals_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdTerminalsExtraApplication.RowCommand
        Try

            Select Case e.CommandName

                Case "EditTerminalParameter"
                    grdTerminalsExtraApplication.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.intTABLE_KEY_CODE = grdTerminalsExtraApplication.SelectedDataKey.Values.Item("APP_PARA_NAME")
                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("", False)
            End Select

            If txtExtraApp.Text = "" Then
                'Set Invisible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
            Else
                'Set Visible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
            End If
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub
    'Edicion de Parametros Extra Application Stis 
    'Autor : Oscar Gutierrez
    Protected Sub grdTerminalsExtraApplication_Updated(sender As Object, e As GridViewUpdatedEventArgs) Handles grdTerminalsExtraApplication.RowUpdated

        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webEditParameterApplication_Stis"
            command.Parameters.Clear()


            command.Parameters.Add(New SqlParameter("PARA_INDEX ", e.OldValues("PARA_INDEX ")))
            command.Parameters.Add(New SqlParameter("APP_PARA_NAME", e.OldValues("APP_PARA_NAME")))
            command.Parameters.Add(New SqlParameter("PARA_NAME", e.OldValues("PARA_NAME")))
            command.Parameters.Add(New SqlParameter("PARA_DESC", e.NewValues("PARA_DESC")))
            command.Parameters.Add(New SqlParameter("PARA_VALUE ", e.NewValues("PARA_VALUE ")))



            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try


    End Sub


    Protected Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click

        If txtExtraApp.Text <> "" Then
            dsTerminalsExtraParameter.SelectParameters("APP_PARA_NAME").DefaultValue = txtExtraApp.Text
        Else
            dsTerminalsExtraParameter.SelectParameters("APP_PARA_NAME").DefaultValue = "-1"
        End If
        grdTerminalsExtraApplication.DataBind()

        If txtExtraApp.Text = "" Then
            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
        Else
            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
        End If

    End Sub

    Private Sub clearForm()
        txtExtraApp.Text = ""

    End Sub

    Protected Sub grdTerminalsParameter_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdTerminalsExtraApplication.SelectedIndexChanged

    End Sub
End Class
