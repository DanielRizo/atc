﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageCSS.master" AutoEventWireup="false" CodeFile="ListarGroupVariousPayments.aspx.vb" Inherits="Groups_ListarGroupVariousPayments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">

    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Listar Grupo Pagos Varios
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

    <%--scripts edit table--%>
    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/editTable.js") %>'></script>

    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.tabledit.js") %>'></script>

    <%--<script type="text/javascript" src='<%= ResolveClientUrl("~/js/bootstrap.js") %>'></script>--%>

    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/moveElementsList.js") %>'></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>

    <li>
        <asp:LinkButton ID="LinkButton1" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/TerminalStis.aspx">Terminales</asp:LinkButton>
    </li>

    <li>Listar Grupo Pagos Varios...</li>
</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <blockquote>
        <p>Este módulo permite ver el listado de grupos (pagos varios):</p>
        <footer class="blockquote-footer">Polaris Cloud Service (Pstis) </footer>
    </blockquote>

    <asp:Panel ID="pnlMsg" runat="server" Visible="False">
        <div class="alert alert-success">
            <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
            <a href="#" class="close tips" title="Cerrar">x</a>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlError" runat="server" Visible="False">
        <div class="alert alert-danger">
            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
            <a href="#" class="close tips" title="Cerrar">x</a>
        </div>
    </asp:Panel>

    <style>
        .bg-1 {
            background-color: #1abc9c;
            color: #ffffff;
        }
    </style>
    <div class="bg-1">
        <div class="container text-center">
            <h3>Lista de Grupos (pagos varios) Pstis</h3>
            <img src="https://cdn1.vectorstock.com/i/1000x1000/94/50/concept-easy-online-payment-icon-vector-9529450.jpg" class="img-circle" alt="Bird" width="350" height="280">
        </div>
    </div>


    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
        <div class="toggle-message" style="z-index: 590; top: 0px; left: 0px;">
            <h3 class="title">Filtro de B&uacute;squeda...
                <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
            <div class="st-form-line">
                <span class="st-labeltext"><b>Codigo Grupo</b></span>
                <asp:TextBox ID="txtCode" CssClass="form-control"
                    runat="server" TabIndex="1" MaxLength="3"
                    ToolTip="Code" onkeydown="return (event.keyCode!=13);" Width="250px"></asp:TextBox>
                <small id="passwordHelpInline2" class="text-muted">Must be 3 characters long.
                </small>
            </div>
            <div class="button-box">
                <asp:Button ID="btnConsultar" runat="server" Text="Filtrar"
                    CssClass="btn btn-info" TabIndex="3" ToolTip="Filtrar Grupo Prompts..." />
            </div>
        </div>
    </div>


    <div class="simplebox grid960">

        <div class="titleh">
            <h3 style="margin: 0px;">Agregar nuevo Grupo</h3>
            <div class="shortcuts-icons " style="z-index: 660;">
                <a class="shortcut tips" href="CreateGroupVariousPayments.aspx" original-title="Agregar Nuevo Grupo">
                    <img src="../img/icons/shortcut/plus.png" width="25" height="25" alt="icon"></a>

            </div>
        </div>

        <ul id="navst">
            <li class="navbar navbar-light"><a href="TerminalStis.aspx">PSTIS</a></li>
            <li><a>Tables</a>
                <ul>
                    <li><a href="TerminalAcquirer.aspx">Acquirer</a></li>
                    <li><a href="TerminalIssuer.aspx">Issuer</a>	</li>
                    <li><a href="TerminalCardRange.aspx">CardRange</a></li>
                    <li><a href="TerminalEmvLevel2.aspx">Emv Level 2 Application</a></li>
                    <li><a href="TerminalEmvKey.aspx">Emv Level 2 Key</a></li>
                    <li><a href="TerminalExtraApplication.aspx">Extra Application Parameter</a></li>
                    <li><a href="ListarGroupPrompts.aspx">Grupos Prompts</a></li>
                    <li><a href="ListarGroupVariousPayments.aspx">Grupos Pagos Varios</a></li>
                    <li><a href="ListarGroupElectronicsPayments.aspx">Grupos Pagos Electronicos</a></li>
                    <li><a href="TerminalHostConfiguration.aspx">Host Configuration</a></li>
                    <li><a href="TerminalPrompts.aspx">Prompts</a></li>
                    <li><a href="PagosVarios.aspx">Pagos Varios</a></li>
                    <li><a href="PagosElectronicos.aspx">Pagos Electronicos</a></li>
                    <li><a href="IP.aspx">Ip</a></li>
                    <li><a href="AddCertificado.aspx">Certificados SSL</a></li>
                </ul>
            </li>
            <li><a>Terminal</a>
                <ul>
                    <li><a href="AddTerminalStis.aspx">New Terminal</a></li>
                    <li><a href="CopyTerminalStis.aspx">Copy Terminal</a></li>

                </ul>
            </li>
        </ul>
        <!-- START SIMPLE FORM -->
        <div class="simplebox grid960">

            <div>
                <br />
                <h3 class="title">Listado de Grupos (Pagos varios)
                <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>

            </div>
        </div>
        <div class="body" id="Tabla">

            <div id="posStis" style="overflow: auto; width: auto; height: 660px;">
                <asp:GridView ID="grdPrompts" runat="server"
                    AllowSorting="True" AutoGenerateColumns="False"
                    DataSourceID="dsPrompts"
                    CssClass="table table-responsive-lg jumbotron table-light table-hover table-bordered"
                    HeaderStyle-ForeColor="WhiteSmoke"
                    HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                    HeaderStyle-CssClass="table-bordered bg-info" DataKeyNames="Grupo_id"
                    EmptyDataText="No existen Adquirientes creados con el filtro aplicado"
                    HorizontalAlign="Left">
                    <Columns>
                        <asp:BoundField DataField="Grupo_id" HeaderText="Code"
                            SortExpression="Grupo_id" ReadOnly="True" />
                        <asp:BoundField DataField="Grupo_Nombre" HeaderText="Name Group"
                            SortExpression="Grupo_Nombre" />
                        <asp:TemplateField ShowHeader="False" HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False"
                                    CommandName="ListarGrupo" ImageUrl="~/img/icons/16x16/research.png" Text="Editar"
                                    ToolTip="Listar Grupo" CommandArgument='<%# grdPrompts.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />
                                <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False"
                                    CommandName="DeleteGroupPayments" ImageUrl="~/img/icons/16x16/delete.png" Text="Eliminar"
                                    ToolTip="Eliminar Grupo pagos varios" CommandArgument='<%# grdPrompts.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink"
                                    OnClientClick="return ConfirmAction(this);" />
                            </ItemTemplate>

                            <HeaderStyle Width="100px"></HeaderStyle>

                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>


                    <PagerStyle
                        CssClass="pgr" />
                    <HeaderStyle />
                    <EditRowStyle BorderColor="#666666" BorderStyle="Solid"
                        BorderWidth="1px" />
                </asp:GridView>
                <br />

            </div>

            <br />
            <asp:SqlDataSource ID="dsPrompts" runat="server"
                ConnectionString="<%$ ConnectionStrings:TeleLoaderStisConnectionString %>"
                SelectCommand="sp_webListarGruposPagosVarios" SelectCommandType="StoredProcedure"
                UpdateCommand="sp_webEditDatosAcquirerNew_Stis" UpdateCommandType="StoredProcedure"
                DeleteCommand="sp_webEliminarGrupoPagosVarios" DeleteCommandType="StoredProcedure">
                <DeleteParameters>
                    <asp:Parameter Name="Grupo_id" Type="String" />
                </DeleteParameters>
                <SelectParameters>
                    <asp:Parameter Name="Grupo_id" Type="String" />

                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Grupo_Nombre" Type="String" />

                </UpdateParameters>
            </asp:SqlDataSource>

        </div>
    </div>
       <footer class="container-fluid text-center">
  <p>Grupos Actualmente Creados Pstis</p>
</footer>
    <asp:HyperLink ID="HyperLink1" runat="server"
        NavigateUrl="~/Group/TerminalStis.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>


<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

    <script language="javascript">
        var globalcontrol = '';

        function ConfirmAction(control) {

            globalcontrol = control;

            $.msgAlert({
                type: "warning"
                , title: "Mensaje del Sistema"
                , text: "Realmente desea eliminar el grupo de pagos varios?"
                , callback: function () {
                    __doPostBack($(globalcontrol).attr('name'), '');
                }
            });

            return false;
        }
    </script>

</asp:Content>

