﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Applications
Imports System.Data.Common
Imports System.Web.Services
Imports System.Web.Script.Services

Partial Class Groups_ListarGroupVariousPayments

    Inherits TeleLoader.Web.BasePage

    Dim applicationObj As ApplicationAcquirerParameter




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsPostBack Then




        Else

            dsPrompts.SelectParameters("Grupo_id").DefaultValue = objSessionParams.StrIsExtended

            grdPrompts.DataBind()


        End If

    End Sub

    Protected Sub grdAddAcquirer_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdPrompts.RowCommand

        Try

            Select Case e.CommandName
                Case "DeleteGroupPayments"
                    Dim strConnString As String
                    Dim configurationSection As ConnectionStringsSection =
                    System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
                    strConnString = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
                    Dim connection As New SqlConnection(strConnString)
                    Dim command As New SqlCommand()
                    Dim results As SqlDataReader
                    Dim status As Integer
                    Dim rspMsg As String

                    Dim retVal As Boolean
                    rspMsg = ""
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    grdPrompts.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    dsPrompts.DeleteParameters("Grupo_id").DefaultValue = grdPrompts.SelectedDataKey.Values.Item("Grupo_id")

                    Dim strData As String = "Terminal ID: " & grdPrompts.SelectedDataKey.Values.Item("Grupo_id")
                    Try
                        'Abrir Conexion
                        connection.Open()

                        command.Connection = connection
                        command.CommandType = CommandType.StoredProcedure
                        command.CommandText = "sp_webEliminarGrupoPagosVarios"
                        command.Parameters.Clear()
                        command.Parameters.Add(New SqlParameter("Grupo_id", "" + grdPrompts.SelectedDataKey.Values.Item("Grupo_id").ToString))


                        'Ejecutar SP
                        results = command.ExecuteReader()
                        If results.HasRows Then
                            While results.Read()
                                status = results.GetInt32(0)
                                rspMsg = results.GetString(1)
                            End While
                        Else
                            retVal = False
                        End If

                        If status = 1 Then
                            grdPrompts.DataBind()

                            pnlError.Visible = False
                            pnlMsg.Visible = True
                            lblMsg.Text = rspMsg
                        Else
                            grdPrompts.DataBind()

                            pnlError.Visible = True
                            pnlMsg.Visible = False
                            lblError.Text = rspMsg
                        End If

                    Catch ex As Exception
                        retVal = False
                    Finally
                        'Cerrar data reader por Default
                        If Not (results Is Nothing) Then
                            results.Close()
                        End If
                        'Cerrar conexion por Default
                        If Not (connection Is Nothing) Then
                            connection.Close()
                        End If
                    End Try
                Case "ListarGrupo"

                    'objSessionParams.intCodeGroup = ""

                    'objSessionParams.intCodeGroup = grdPrompts.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).Text
                    'objSessionParams.NameGroup = grdPrompts.Rows(Convert.ToInt32(e.CommandArgument)).Cells(1).Text



                    'objSessionParams.NameGroup = grdPrompts.SelectedDataKey.Values.Item("Grupo_Id")

                    ''Set Data into Session
                    'Session("SessionParameters") = objSessionParams

                    Response.Redirect("EditarGroupVariousPayments.aspx", False)
            End Select
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub
    'Edicion de Parametros Acquirer Stis 
    'Autor : Oscar Gutierrez
    Protected Sub grdAddAcquirer_Update(sender As Object, e As GridViewUpdatedEventArgs) Handles grdPrompts.RowUpdated

        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webEditDatosAcquirerNew_Stis"
            command.Parameters.Clear()

            command.Parameters.Add(New SqlParameter("Grupo_Nombre", e.NewValues("Grupo_Nombre")))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try


    End Sub

    Protected Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click

        If txtCode.Text <> "" Then
            dsPrompts.SelectParameters("Grupo_id").DefaultValue = txtCode.Text
        Else
            dsPrompts.SelectParameters("Grupo_id").DefaultValue = "-1"
        End If
        grdPrompts.DataBind()

        If txtCode.Text = "" Then
            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
        Else
            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
        End If

    End Sub

End Class
