﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageCSS.master" AutoEventWireup="false" CodeFile="ListarHostConfiguration.aspx.vb" Inherits="Groups_ListHostConfiguration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> ::HOST CONFIGURATION
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>



    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/editTable.js") %>'></script>

    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/SaveTable.js") %>'></script>

    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.tabledit.js") %>'></script>

    <link rel="stylesheet" type="text/css" href="/style/bootstrap.min.css" />
    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/bootstrap.js") %>'></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>

    <li>Parametros Host Configuration</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    Host Configuration : [ <%= objSessionParams.StrAcquirerCode %> ]
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">

    <blockquote>
        <p>Este módulo permite ediar los parametros por Host Configuration:</p>
        <footer class="blockquote-footer">Polaris Cloud Service (Pstis) </footer>
    </blockquote>
    <style>
        .bg-1 {
            background-color: #1abc9c;
            color: #ffffff;
        }
    </style>

    <div class="bg-1">
        <div class="container text-center">
            <h3>Edición de Parametros Host Configuration</h3>
            <img src="https://cdn3.iconfinder.com/data/icons/cmyk-product-development/128/cmyk-03-512.png" class="img-circle" alt="Bird" width="350" height="280">
        </div>
    </div>

    <div class="container-fluid bg-2 text-center">
        <h3>Parametros Host Configuration</h3>
        <p>Este modulo permite editar los parametros de los host Configuration, una vez sean editados se podran efectuar los cambion en la Inicializacion de parametros. </p>
    </div>
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <div id="succes2" style="display: none;">
            <asp:Panel ID="Panel1" runat="server">
                <div class="alert alert-success">
                    <asp:Label ID="lblMsg2" runat="server" Text="Datos editados correctamente."></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">x</a>
                </div>
            </asp:Panel>
        </div>

        <div id="error2" style="display: none;">
            <asp:Panel ID="Panel2" runat="server">
                <div class="alert alert-danger ">
                    <asp:Label ID="lblError2" runat="server" Text="Error editando datos."></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">x</a>
                </div>
            </asp:Panel>
        </div>
    </div>

    <div class="simplebox grid960">

        <div class="w3-container">
            <h2>Host Content</h2>
            <div class="w3-card-4" style="width: 40%;">
                <header class="w3-container w3-blue">
      <h4>Datos Rango de Tarjeta</h4>
    </header>

                <div class="st-form-line">
                    <span class="st-labeltext"><b>Host Code:</b></span>

                    <asp:TextBox ID="txtCode" CssClass="st-success-input" Style="width: 400px"
                        runat="server" TabIndex="1" MaxLength="100"
                        ToolTip="Code." onkeydown="return (event.keyCode!=13);"
                        Enabled="False"></asp:TextBox>

                </div>
                <div class="st-form-line">
                    <span class="st-labeltext"><b>Host Name:</b></span>

                    <asp:TextBox ID="TxtNameHostConfiguration" CssClass="st-success-input" Style="width: 400px"
                        runat="server" TabIndex="1" MaxLength="100"
                        ToolTip="Name Host Configuration." onkeydown="return (event.keyCode!=13);"
                        Enabled="False"></asp:TextBox>

                </div>
            </div>
        </div>
    </div>
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
        <center>
            <input type="button" class="btn btn-info" id="initTerminals" value="Guardar Cambios" style="margin: 15px  3px;" />
        </center>
        <ul id="navst">
            <li class="navbar navbar-light"><a href="TerminalStis.aspx">PSTIS</a></li>
            <li><a>Tables</a>
                <ul>
                    <li><a href="TerminalAcquirer.aspx">Acquirer</a></li>
                    <li><a href="TerminalIssuer.aspx">Issuer</a>	</li>
                    <li><a href="TerminalCardRange.aspx">CardRange</a></li>
                    <li><a href="TerminalEmvLevel2.aspx">Emv Level 2 Application</a></li>
                    <li><a href="TerminalEmvKey.aspx">Emv Level 2 Key</a></li>
                    <li><a href="TerminalExtraApplication.aspx">Extra Application Parameter</a></li>
                    <li><a href="ListarGroupPrompts.aspx">Grupos Prompts</a></li>
                    <li><a href="ListarGroupVariousPayments.aspx">Grupos Pagos Varios</a></li>
                    <li><a href="ListarGroupElectronicsPayments.aspx">Grupos Pagos Electronicos</a></li>
                    <li><a href="TerminalHostConfiguration.aspx">Host Configuration</a></li>
                    <li><a href="TerminalPrompts.aspx">Prompts</a></li>
                    <li><a href="PagosVarios.aspx">Pagos Varios</a></li>
                    <li><a href="PagosElectronicos.aspx">Pagos Electronicos</a></li>
                    <li><a href="IP.aspx">Ip</a></li>
                    <li><a href="AddCertificado.aspx">Certificados SSL</a></li>
                </ul>
            </li>
            <li><a>Terminal</a>
                <ul>
                    <li><a href="AddTerminalStis.aspx">New Terminal</a></li>
                    <li><a href="CopyTerminalStis.aspx">Copy Terminal</a></li>

                </ul>
            </li>
        </ul>
        <script type="text/javascript">
            $(document).ready(function () {
                enableEditTable("#ctl00_MainContent_grdParameterHostConfiguration", true, false, true, false, [[2, 'Value']], "");
            });
        </script>
        <div class="body">
            <div id="posStis" style="overflow: auto; width: auto; height: 660px;">
                <br />
                <asp:GridView ID="grdParameterHostConfiguration" runat="server"
                    AllowSorting="True" AutoGenerateColumns="False"
                    DataSourceID="dsHostParameter"
                    CssClass="table table-responsive-lg jumbotron table-light table-hover table-bordered"
                    HeaderStyle-ForeColor="WhiteSmoke"
                    HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                    HeaderStyle-CssClass="table-bordered bg-info"
                    EmptyDataText="No existen Host Configuration creados con el filtro aplicado"
                    HorizontalAlign="Left">
                    <Columns>
                        <asp:BoundField DataField="TABLE_KEY_CODE" HeaderText="Code"
                            SortExpression="TABLE_KEY_CODE" />
                        <asp:BoundField DataField="FIELD_DISPLAY_NAME" HeaderText="Parameter Name"
                            SortExpression="FIELD_DISPLAY_NAME" />
                        <asp:BoundField DataField="CONTENT_DESC" HeaderText="Value"
                            SortExpression="CONTENT_DESC" />
                        <asp:BoundField DataField="CONTENT_TYPE" HeaderText="Value Type"
                            SortExpression="CONTENT_TYPE" ReadOnly="True" />
                        <asp:BoundField DataField="IS_EXTENDED" HeaderText="Parameter Type"
                            SortExpression="IS_EXTENDED" ReadOnly="True" />
                        <asp:BoundField DataField="NOTE" HeaderText="Note"
                            SortExpression="NOTE" ReadOnly="True" />
                    </Columns>
                    <PagerStyle
                        CssClass="pgr" />
                    <HeaderStyle />
                    <EditRowStyle BorderColor="#666666" BorderStyle="Solid"
                        BorderWidth="1px" />
                </asp:GridView>
                <br />
            </div>
            <asp:SqlDataSource ID="dsHostParameter" runat="server"
                ConnectionString="<%$ ConnectionStrings:TeleLoaderStisConnectionString %>"
                SelectCommand="sp_webListarParameter_HostConfiguration" SelectCommandType="StoredProcedure" UpdateCommand="sp_webEditParameterAcquirer_Stis" UpdateCommandType="StoredProcedure">
                <SelectParameters>
                    <asp:Parameter Name="TABLE_KEY_CODE" Type="string" />
                    <asp:Parameter Name="CONTENT_DESC" Type="string" DefaultValue="-1" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="TABLE_KEY_CODE" Type="String" />
                    <asp:Parameter Name="FIELD_DISPLAY_NAME" Type="String" />
                    <asp:Parameter Name="CONTENT_DESC" Type="String" />
                </UpdateParameters>
            </asp:SqlDataSource>

            <script type="text/javascript">

                $('#initTerminals').on('click', function (event) {
                    event.preventDefault();
                    RecorrerTable("#ctl00_MainContent_grdParameterHostConfiguration", "EditHostConfigurationTbl");


                });
            </script>
        </div>
    </div>
    <asp:HyperLink ID="HyperLink1" runat="server"
        NavigateUrl="~/Group/TerminalHostConfiguration.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>
</asp:Content>


<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

    <script language="javascript">
        var globalcontrol = '';

        function ConfirmAction(control) {

            globalcontrol = control;

            $.msgAlert({
                type: "warning"
                , title: "Mensaje del Sistema"
                , text: "Realmente desea eliminar el Host Configuration?"
                , callback: function () {
                    __doPostBack($(globalcontrol).attr('name'), '');
                }
            });

            return false;
        }
    </script>

</asp:Content>

