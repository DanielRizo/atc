﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageCSS.master" AutoEventWireup="false" CodeFile="ListarPagosElectronicos.aspx.vb" Inherits="Groups_ListPagosElectronicos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> ::PAGOS ELECTRONICOS
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>
    

    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/editTable.js") %>'></script>

    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/SaveTable.js") %>'></script>

    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.tabledit.js") %>'></script>
    
    <link rel="stylesheet" type="text/css" href="/style/bootstrap.min.css" />
    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/bootstrap.js") %>'></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>Pagos Electronicos</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    Codigo Pago Electronico [ <%= objSessionParams.StrAcquirerCode %> ]
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <blockquote>
        <p>Este módulo permite ediar los parametros por pago Electronico:</p>
        <footer class="blockquote-footer">Polaris Cloud Service (Pstis) </footer>
    </blockquote>
    <style>
        .bg-1 {
            background-color: #1abc9c;
            color: #ffffff;
        }
    </style>

    <div class="bg-1">
        <div class="container text-center">
            <h3>Parametros por pagos Electronicos Pstis</h3>
            <img src="http://s3.laprensa.com.ni-bq.s3-us-west-2.amazonaws.com/wp-content/uploads/2017/02/09060301/portada-activos090217-1.jpg" class="img-circle" alt="Bird" width="350" height="280">
            <img src="http://www.ebraintechnologies.com/image/service/ecommercedev.png" class="img-circle" alt="Bird" width="350" height="280">
        </div>
    </div>

    <div class="container-fluid bg-2 text-center">
        <h3>Parametros Pagos Electronicos</h3>
        <p>Este modulo permite editar los parametros de los pagos electronicos, una vez sean editados se podran efectuar los cambion en la Inicializacion de parametros. </p>
    </div>

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <div id="succes2" style="display: none;">
            <asp:Panel ID="Panel1" runat="server">
                <div class="alert alert-success">
                    <asp:Label ID="lblMsg2" runat="server" Text="Datos editados correctamente."></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">x</a>
                </div>
            </asp:Panel>
        </div>
        <div id="error2" style="display: none;">
            <asp:Panel ID="Panel2" runat="server">
                <div class="alert alert-warning">
                    <asp:Label ID="lblError2" runat="server" Text="Error editando datos."></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">x</a>
                </div>
            </asp:Panel>
        </div>
    </div>

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <div class="st-form-line">
            <span class="st-labeltext"><b>Codigo:</b></span>

            <asp:TextBox ID="txtCode" CssClass="st-success-input" Style="width: 210px"
                runat="server" TabIndex="1" MaxLength="100"
                ToolTip="Code." onkeydown="return (event.keyCode!=13);"
                Enabled="False"></asp:TextBox>

        </div>
        <div class="st-form-line">
            <span class="st-labeltext"><b>Nombre:</b></span>

            <asp:TextBox ID="TxtPagoElectronico" CssClass="st-success-input" Style="width: 310px"
                runat="server" TabIndex="1" MaxLength="100"
                ToolTip="Nombre Pago Electronico." onkeydown="return (event.keyCode!=13);"
                Enabled="False"></asp:TextBox>

        </div>
        <center>
            <input type="button" class="button-aqua" id="initTerminals" value="Guardar" style="margin: 15px  3px;" />
        </center>


        <script type="text/javascript">

            $('#initTerminals').on('click', function (event) {
                event.preventDefault();
                RecorrerTable("#ctl00_MainContent_grdListPagoElectronico", "EditPagoElectronicoTbl");
            });
        </script>

        <ul id="navst">
            <li class="navbar navbar-light"><a href="TerminalStis.aspx">PSTIS</a></li>
            <li><a>Tables</a>
                <ul>
                    <li><a href="TerminalAcquirer.aspx">Acquirer</a></li>
                    <li><a href="TerminalIssuer.aspx">Issuer</a>	</li>
                    <li><a href="TerminalCardRange.aspx">CardRange</a></li>
                    <li><a href="TerminalEmvLevel2.aspx">Emv Level 2 Application</a></li>
                    <li><a href="TerminalEmvKey.aspx">Emv Level 2 Key</a></li>
                    <li><a href="TerminalExtraApplication.aspx">Extra Application Parameter</a></li>
                    <li><a href="ListarGroupPrompts.aspx">Grupos Prompts</a></li>
                    <li><a href="ListarGroupVariousPayments.aspx">Grupos Pagos Varios</a></li>
                    <li><a href="ListarGroupElectronicsPayments.aspx">Grupos Pagos Electronicos</a></li>
                    <li><a href="TerminalHostConfiguration.aspx">Host Configuration</a></li>
                    <li><a href="TerminalPrompts.aspx">Prompts</a></li>
                    <li><a href="PagosVarios.aspx">Pagos Varios</a></li>
                    <li><a href="PagosElectronicos.aspx">Pagos Electronicos</a></li>
                    <li><a href="IP.aspx">Ip</a></li>
                    <li><a href="AddCertificado.aspx">Certificados SSL</a></li>
                </ul>
            </li>
            <li><a>Terminal</a>
                <ul>
                    <li><a href="AddTerminalStis.aspx">New Terminal</a></li>
                    <li><a href="CopyTerminalStis.aspx">Copy Terminal</a></li>

                </ul>
            </li>
        </ul>

        <div class="body">
            <div id="posStis" style="overflow: auto; width: auto; height: 660px;">
                <br />
                <asp:GridView ID="grdListPagoElectronico" runat="server"
                    AllowSorting="True" AutoGenerateColumns="False"
                    DataSourceID="dsListPagoElectronico"
                    CssClass="table table-responsive-lg jumbotron table-light table-hover table-bordered"
                    HeaderStyle-ForeColor="WhiteSmoke"
                    HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                    HeaderStyle-CssClass="table-bordered bg-info"
                    EmptyDataText="No existen Pagos Electronicos creados con el filtro aplicado"
                    HorizontalAlign="Left">
                    <Columns>
                        <asp:BoundField DataField="TABLE_KEY_CODE" HeaderText="Code"
                            SortExpression="TABLE_KEY_CODE" />
                        <asp:BoundField DataField="FIELD_DISPLAY_NAME" HeaderText="Parameter Name"
                            SortExpression="FIELD_DISPLAY_NAME" />
                        <asp:BoundField DataField="CONTENT_DESC" HeaderText="Value"
                            SortExpression="CONTENT_DESC" />
                        <asp:BoundField DataField="CONTENT_TYPE" HeaderText="Value Type"
                            SortExpression="CONTENT_TYPE" ReadOnly="True" />
                        <asp:BoundField DataField="IS_EXTENDED" HeaderText="Parameter Type"
                            SortExpression="IS_EXTENDED" ReadOnly="True" />
                        <asp:BoundField DataField="NOTE" HeaderText="Note"
                            SortExpression="NOTE" ReadOnly="True" />

                    </Columns>

                    <PagerStyle
                        CssClass="pgr" />
                    <HeaderStyle />
                    <EditRowStyle BorderColor="#666666" BorderStyle="Solid"
                        BorderWidth="1px" />
                </asp:GridView>
                <br />
            </div>
            <asp:SqlDataSource ID="dsListPagoElectronico" runat="server"
                ConnectionString="<%$ ConnectionStrings:TeleLoaderStisConnectionString %>"
                SelectCommand="sp_webListarParameterPagosElectronicos" SelectCommandType="StoredProcedure" UpdateCommand="sp_webEditParameterAcquirer_Stis" UpdateCommandType="StoredProcedure">
                <SelectParameters>
                    <asp:Parameter Name="TABLE_KEY_CODE" Type="string" />
                    <asp:Parameter Name="CONTENT_DESC" Type="string" DefaultValue="-1" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="TABLE_KEY_CODE" Type="String" />
                    <asp:Parameter Name="FIELD_DISPLAY_NAME" Type="String" />
                    <asp:Parameter Name="CONTENT_DESC" Type="String" />
                </UpdateParameters>
            </asp:SqlDataSource>
            <script type="text/javascript">
                $(document).ready(function () {
                    enableEditTable("#ctl00_MainContent_grdListPagoElectronico", true, false, true, false, [[2, 'Value']], "");
                });
            </script>
        </div>
    </div>
    <asp:HyperLink ID="HyperLink1" runat="server"
        NavigateUrl="~/Group/PagosElectronicos.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>


<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

    <script language="javascript">
        var globalcontrol = '';

        function ConfirmAction(control) {

            globalcontrol = control;

            $.msgAlert({
                type: "warning"
                , title: "Mensaje del Sistema"
                , text: "Realmente desea eliminar el Pago Electronico?"
                , callback: function () {
                    __doPostBack($(globalcontrol).attr('name'), '');
                }
            });

            return false;
        }
    </script>

</asp:Content>

