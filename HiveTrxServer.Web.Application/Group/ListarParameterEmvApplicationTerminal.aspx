﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ListarParameterEmvApplicationTerminal.aspx.vb" Inherits="Groups_ListEmvApplicationTerminal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> ::EMV LEVEL 2 APPLICATION
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>

    <li>EMV LEVEL 2 APPLICATION</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    EMV LEVEL 2 APPLICATION: [ <%= objSessionParams.strEmvApplication %> ]
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <p>Este módulo le permite administrar los Paramtros Por Emv level 2 Application Stis:</p>
    <br />

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlError" runat="server" Visible="False">
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>
        <div class="toggle-message" style="z-index: 590; top: 0px; left: 0px;">
            <h3 class="title">Filtro de B&uacute;squeda...
                <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
            <div class="hide-message" id="hide-message" style="display: none;">
                <div class="st-form-line">
                    <span class="st-labeltext"><b>EMV Application </b></span>
                    <asp:TextBox ID="txtEmvApplication" CssClass="st- "
                        runat="server" TabIndex="1" MaxLength="50"
                        ToolTip="Emv Application" onkeydown="return (event.keyCode!=13);" Width="250px"></asp:TextBox>
                </div>
                <div class="button-box">
                    <asp:Button ID="btnConsultar" runat="server" Text="Filtrar"
                        CssClass="button-aqua" TabIndex="3" ToolTip="Filtrar Paramtros Acquirer Stis..." />
                </div>
            </div>
        </div>

    </div>

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
        <div class="titleh">
            <h3>Emv Level 2 Application Parameter</h3>
            <div class="shortcuts-icons" style="z-index: 660;">
                <%--<p>Tipo de Parametro "0" STANDARD</p>
                <p>Tipo de Parametro "1" EXTENDED</p>--%>
                <br />
            </div>
        </div>
        <div class="st-form-line">
            <span class="st-labeltext"><b>Application:</b></span>

            <asp:TextBox ID="txtApplicationName" CssClass="st-success-input" Style="width: 510px"
                runat="server" TabIndex="1" MaxLength="100"
                ToolTip="Code." onkeydown="return (event.keyCode!=13);"
                Enabled="False"></asp:TextBox>

        </div>
        <div class="body">
            <br />
            <asp:GridView ID="grdTerminalsEmvApplication" runat="server" AllowPaging="True"
                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4"
                DataSourceID="dsTerminalsEmvApplication"
                CssClass="mGridCenter" DataKeyNames="TERMINAL_REC_NO"
                EmptyDataText="No existen Application creados con el filtro aplicado"
                HorizontalAlign="Center" GridLines="None" ForeColor="#333333">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:BoundField DataField="TERMINAL_REC_NO" HeaderText="Record No"
                        SortExpression="TERMINAL_REC_NO" />
                    <asp:BoundField DataField="IS_EXTENDED" HeaderText="Parameter Type"
                        SortExpression="IS_EXTENDED" />
                    <asp:BoundField DataField="FIELD_DISPLAY_NAME" HeaderText="Parameter Name"
                        SortExpression="FIELD_DISPLAY_NAME" />
                        <asp:BoundField DataField="CONTENT_DESC" HeaderText="Value (Click To Change)"
                        SortExpression="CONTENT_DESC" />
                    <asp:BoundField DataField="NOTE" HeaderText="Note"
                        SortExpression="NOTE" />
                     <asp:BoundField DataField="REFER_CODE" HeaderText="Codigo de Referencia"
                        SortExpression="REFER_CODE" />
                </Columns>

                <FooterStyle BackColor="#1C5E55" ForeColor="White" Font-Bold="True" />
                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center"
                    CssClass="pgr" />
                <RowStyle BackColor="#E3EAEB" />
                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BorderColor="#666666" BorderStyle="Solid"
                    BorderWidth="1px" BackColor="#50A2A3" />
                <SortedAscendingCellStyle BackColor="#F8FAFA" />
                <SortedAscendingHeaderStyle BackColor="#246B61" />
                <SortedDescendingCellStyle BackColor="#D4DFE1" />
                <SortedDescendingHeaderStyle BackColor="#15524A" />
            </asp:GridView>
            <br />
            <asp:SqlDataSource ID="dsTerminalsEmvApplication" runat="server"
                ConnectionString="<%$ ConnectionStrings:TeleLoaderStisConnectionString %>"
                SelectCommand="sp_webListarEmvLevel2Application_Stis" SelectCommandType="StoredProcedure">
                <SelectParameters>
                    <asp:Parameter Name="TABLE_KEY_CODE" Type="string" />
                </SelectParameters>
            </asp:SqlDataSource>
        </div>
    </div>
    <asp:HyperLink ID="HyperLink1" runat="server"
        NavigateUrl="~/Group/TerminalEmvLevel2.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>


<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

    <script language="javascript">
        var globalcontrol = '';

        function ConfirmAction(control) {

            globalcontrol = control;

            $.msgAlert({
                type: "warning"
                , title: "Mensaje del Sistema"
                , text: "Realmente desea eliminar la Application?"
                , callback: function () {
                    __doPostBack($(globalcontrol).attr('name'), '');
                }
            });

            return false;
        }
    </script>

</asp:Content>

