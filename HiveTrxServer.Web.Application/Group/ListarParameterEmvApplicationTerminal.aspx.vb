﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Applications
Imports System.Data.Common

Partial Class Groups_ListEmvApplicationTerminal
    Inherits TeleLoader.Web.BasePage

    Dim applicationObj As ApplicationEmvApplication

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then
            txtApplicationName.Text = objSessionParams.StrParamterEMV
            txtEmvApplication.Focus()
            dsTerminalsEmvApplication.SelectParameters("TABLE_KEY_CODE").DefaultValue = objSessionParams.StrParamterEMV
            grdTerminalsEmvApplication.DataBind()
        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub grdTerminals_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdTerminalsEmvApplication.RowCommand
        Try

            Select Case e.CommandName

                Case "EditParameterEmvLevel2"
                    grdTerminalsEmvApplication.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.strEmvApplication = grdTerminalsEmvApplication.SelectedDataKey.Values.Item("TABLE_KEY_CODE")
                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("", False)
            End Select

            If txtEmvApplication.Text = "" Then
                'Set Invisible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
            Else
                'Set Visible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
            End If
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub


    Protected Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click

        If txtEmvApplication.Text <> "" Then
            dsTerminalsEmvApplication.SelectParameters("TABLE_KEY_CODE").DefaultValue = txtEmvApplication.Text
        Else
            dsTerminalsEmvApplication.SelectParameters("TABLE_KEY_CODE").DefaultValue = "-1"
        End If
        grdTerminalsEmvApplication.DataBind()

        If txtEmvApplication.Text = "" Then
            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
        Else
            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
        End If

    End Sub

    Private Sub clearForm()
        txtEmvApplication.Text = ""

    End Sub

    Protected Sub grdTerminalsParameter_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdTerminalsEmvApplication.SelectedIndexChanged

    End Sub
End Class
