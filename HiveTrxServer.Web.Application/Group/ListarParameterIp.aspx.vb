﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Applications
Imports System.Data.Common

Partial Class Groups_ListParameterIp
    Inherits TeleLoader.Web.BasePage

    Dim applicationObj As ApplicationAcquirerParameter

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        If Not IsPostBack Then
            txtCode.Text = objSessionParams.StrAcquirerCode
            TxtIp.Text = objSessionParams.StrCardRangeName

            'txtReferCode.Focus()
            dsListIp.SelectParameters("TABLE_KEY_CODE").DefaultValue = objSessionParams.StrAcquirerCode
            grdListIp.DataBind()
        Else

        End If
    End Sub

    Protected Sub grdListIp_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdListIp.RowCommand
        Try

            Select Case e.CommandName

                Case "EditTerminalParameter"
                    grdListIp.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.intTABLE_KEY_CODE = grdListIp.SelectedDataKey.Values.Item("TABLE_KEY_CODE")
                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("", False)
            End Select

            'If txtReferCode.Text = "" Then
            '    'Set Invisible Toggle Panel
            '    ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
            'Else
            '    'Set Visible Toggle Panel
            '    ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
            'End If
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub
    'Edicion de Parametros Acquirer Stis 
    'Autor : Oscar Gutierrez
    Protected Sub grdListIp_Updated(sender As Object, e As GridViewUpdatedEventArgs) Handles grdListIp.RowUpdated

        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webEditParameterAcquirer_Stis"
            command.Parameters.Clear()


            command.Parameters.Add(New SqlParameter("TABLE_KEY_CODE", e.OldValues("TABLE_KEY_CODE")))
            command.Parameters.Add(New SqlParameter("FIELD_DISPLAY_NAME", e.OldValues("FIELD_DISPLAY_NAME")))
            command.Parameters.Add(New SqlParameter("CONTENT_DESC", e.NewValues("CONTENT_DESC")))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try


    End Sub


    'Protected Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click

    '    If txtReferCode.Text <> "" Then
    '        dsListIp.SelectParameters("TABLE_KEY_CODE").DefaultValue = txtReferCode.Text
    '    Else
    '        dsListIp.SelectParameters("TABLE_KEY_CODE").DefaultValue = "-1"
    '    End If
    '    grdListIp.DataBind()

    '    If txtReferCode.Text = "" Then
    '        'Set Invisible Toggle Panel
    '        ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
    '    Else
    '        'Set Visible Toggle Panel
    '        ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
    '    End If

    'End Sub

    Private Sub clearForm()
        'txtReferCode.Text = ""

    End Sub
End Class
