﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Applications
Imports System.Data.Common

Partial Class Groups_Pagos_Electronicos
    Inherits TeleLoader.Web.BasePage

    Dim applicationObj As ApplicationIssuer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            txtPagosElectronicos.Focus()
            dsPagosElectronicos.SelectParameters("PAGOS_CODE").DefaultValue = objSessionParams.intCARD_CODE
            dsPagosElectronicos.DataBind()
        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub grdPagosElectronicos_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdPagosElectronicos.RowCommand
        Try

            Select Case e.CommandName

                Case "DeletePagosElectronicos"
                    Dim strConnString As String
                    Dim configurationSection As ConnectionStringsSection =
                    System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
                    strConnString = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
                    Dim connection As New SqlConnection(strConnString)
                    Dim command As New SqlCommand()
                    Dim results As SqlDataReader
                    Dim status As Integer
                    Dim rspMsg As String

                    Dim retVal As Boolean
                    rspMsg = ""
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    grdPagosElectronicos.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    dsPagosElectronicos.DeleteParameters("PAGOS_CODE").DefaultValue = grdPagosElectronicos.SelectedDataKey.Values.Item("PAGOS_CODE")

                    Dim strData As String = "Terminal ID: " & grdPagosElectronicos.SelectedDataKey.Values.Item("PAGOS_CODE")

                    Try
                        'Abrir Conexion
                        connection.Open()

                        command.Connection = connection
                        command.CommandType = CommandType.StoredProcedure
                        command.CommandText = "sp_webEliminarPagos_ELECTRONICOS"
                        command.Parameters.Clear()
                        command.Parameters.Add(New SqlParameter("PAGOS_CODE", "" + grdPagosElectronicos.SelectedDataKey.Values.Item("PAGOS_CODE").ToString))


                        'Ejecutar SP
                        results = command.ExecuteReader()
                        If results.HasRows Then
                            While results.Read()
                                status = results.GetInt32(0)
                                rspMsg = results.GetString(1)
                            End While
                        Else
                            retVal = False
                        End If

                        If status = 1 Then
                            grdPagosElectronicos.DataBind()

                            pnlError.Visible = False
                            pnlMsg.Visible = True
                            lblMsg.Text = rspMsg
                        Else
                            grdPagosElectronicos.DataBind()

                            pnlError.Visible = True
                            pnlMsg.Visible = False
                            lblError.Text = rspMsg
                        End If

                    Catch ex As Exception
                        retVal = False
                    Finally
                        'Cerrar data reader por Default
                        If Not (results Is Nothing) Then
                            results.Close()
                        End If
                        'Cerrar conexion por Default
                        If Not (connection Is Nothing) Then
                            connection.Close()
                        End If
                    End Try


                Case "EditPagos"
                    grdPagosElectronicos.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.StrAcquirerCode = grdPagosElectronicos.SelectedDataKey.Values.Item("PAGOS_CODE")
                    objSessionParams.StrCardRangeName = grdPagosElectronicos.SelectedDataKey.Values.Item("PAGOS_KEY_NAME")
                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("ListarPagosElectronicos.aspx", False)
            End Select

            If txtPagosElectronicos.Text = "" Then
                'Set Invisible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
            Else
                'Set Visible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
            End If

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub
    'Edicion de Parametros CardRange Stis 
    'Autor : Oscar Gutierrez
    Protected Sub grdPagosElectronicos_Updated(sender As Object, e As GridViewUpdatedEventArgs) Handles grdPagosElectronicos.RowUpdated

        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webEditParameterPagosVarios_Stis"
            command.Parameters.Clear()


            command.Parameters.Add(New SqlParameter("PAGOS_CODE", e.OldValues("PAGOS_CODE")))
            command.Parameters.Add(New SqlParameter("PAGOS_KEY_NAME", e.OldValues("PAGOS_KEY_NAME")))
            command.Parameters.Add(New SqlParameter("PAGOS_DESCRIPTION", e.NewValues("PAGOS_DESCRIPTION")))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try
    End Sub

    Protected Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click

        If txtCode.Text <> "" Then
            dsPagosElectronicos.SelectParameters("PAGOS_CODE").DefaultValue = txtCode.Text
        Else
            dsPagosElectronicos.SelectParameters("PAGOS_CODE").DefaultValue = "-1"
        End If

        If txtPagosElectronicos.Text <> "" Then
            dsPagosElectronicos.SelectParameters("PAGOS_KEY_NAME").DefaultValue = txtPagosElectronicos.Text
        Else
            dsPagosElectronicos.SelectParameters("PAGOS_KEY_NAME").DefaultValue = "-1"
        End If
        grdPagosElectronicos.DataBind()

        If txtPagosElectronicos.Text = "" Then
            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
        Else
            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
        End If

    End Sub

    Private Sub clearForm()
        txtPagosElectronicos.Text = ""

    End Sub
End Class
