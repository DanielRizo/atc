﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Applications
Imports System.Data.Common

Partial Class Groups_Pagos_Varios
    Inherits TeleLoader.Web.BasePage

    Dim applicationObj As ApplicationIssuer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            txtPagosVarios.Focus()
            dsPagosVarios.SelectParameters("PAGOS_CODE").DefaultValue = objSessionParams.intCARD_CODE
            dsPagosVarios.DataBind()
        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub grdPagosVarios_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdPagosVarios.RowCommand
        Try

            Select Case e.CommandName

                Case "DeletePagosVarios"
                    Dim strConnString As String
                    Dim configurationSection As ConnectionStringsSection =
                    System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
                    strConnString = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
                    Dim connection As New SqlConnection(strConnString)
                    Dim command As New SqlCommand()
                    Dim results As SqlDataReader
                    Dim status As Integer
                    Dim rspMsg As String

                    Dim retVal As Boolean
                    rspMsg = ""
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    grdPagosVarios.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    dsPagosVarios.DeleteParameters("PAGOS_CODE").DefaultValue = grdPagosVarios.SelectedDataKey.Values.Item("PAGOS_CODE")

                    Dim strData As String = "Terminal ID: " & grdPagosVarios.SelectedDataKey.Values.Item("PAGOS_CODE")
                    Try
                        'Abrir Conexion
                        connection.Open()

                        command.Connection = connection
                        command.CommandType = CommandType.StoredProcedure
                        command.CommandText = "sp_webEliminarPagos_Varios"
                        command.Parameters.Clear()
                        command.Parameters.Add(New SqlParameter("PAGOS_CODE", "" + grdPagosVarios.SelectedDataKey.Values.Item("PAGOS_CODE").ToString))


                        'Ejecutar SP
                        results = command.ExecuteReader()
                        If results.HasRows Then
                            While results.Read()
                                status = results.GetInt32(0)
                                rspMsg = results.GetString(1)
                            End While
                        Else
                            retVal = False
                        End If

                        If status = 1 Then
                            grdPagosVarios.DataBind()

                            pnlError.Visible = False
                            pnlMsg.Visible = True
                            lblMsg.Text = rspMsg
                        Else
                            grdPagosVarios.DataBind()

                            pnlError.Visible = True
                            pnlMsg.Visible = False
                            lblError.Text = rspMsg
                        End If

                    Catch ex As Exception
                        retVal = False
                    Finally
                        'Cerrar data reader por Default
                        If Not (results Is Nothing) Then
                            results.Close()
                        End If
                        'Cerrar conexion por Default
                        If Not (connection Is Nothing) Then
                            connection.Close()
                        End If
                    End Try



                Case "EditPagos"
                    grdPagosVarios.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.StrAcquirerCode = grdPagosVarios.SelectedDataKey.Values.Item("PAGOS_CODE")
                    objSessionParams.StrCardRangeName = grdPagosVarios.SelectedDataKey.Values.Item("PAGOS_KEY_NAME")
                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("ListarPagosVarios.aspx", False)
            End Select

            If txtPagosVarios.Text = "" Then
                'Set Invisible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
            Else
                'Set Visible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
            End If

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub
    'Edicion de Parametros CardRange Stis 
    'Autor : Oscar Gutierrez
    Protected Sub grdPagosVarios_Updated(sender As Object, e As GridViewUpdatedEventArgs) Handles grdPagosVarios.RowUpdated

        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webEditParameterHostConfiguration_Stis"
            command.Parameters.Clear()


            command.Parameters.Add(New SqlParameter("PAGOS_CODE", e.OldValues("PAGOS_CODE")))
            command.Parameters.Add(New SqlParameter("PAGOS_KEY_NAME", e.OldValues("PAGOS_KEY_NAME")))
            command.Parameters.Add(New SqlParameter("PAGOS_DESCRIPTION", e.NewValues("PAGOS_DESCRIPTION")))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try
    End Sub

    Protected Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click

        If txtCode.Text <> "" Then
            dsPagosVarios.SelectParameters("PAGOS_CODE").DefaultValue = txtCode.Text
        Else
            dsPagosVarios.SelectParameters("PAGOS_CODE").DefaultValue = "-1"
        End If
        If txtPagosVarios.Text <> "" Then
            dsPagosVarios.SelectParameters("PAGOS_KEY_NAME").DefaultValue = txtPagosVarios.Text
        Else
            dsPagosVarios.SelectParameters("PAGOS_KEY_NAME").DefaultValue = "-1"
        End If
        grdPagosVarios.DataBind()

        If txtPagosVarios.Text = "" Then
            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
        Else
            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
        End If

    End Sub

    Private Sub clearForm()
        txtPagosVarios.Text = ""

    End Sub
End Class
