﻿
<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ParametrosCodigoBarras.aspx.vb" Inherits="Security_WebModules" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Configuracion Parametros de Codigo de Barras
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   

	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script>     
        
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed" 
            PostBackUrl="~/Security/Manager.aspx">Seguridad</asp:LinkButton>
    </li>
    <li>Configuracion Parametros de Codigo de Barras</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Configuracion Parametros de Codigo de Barras
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite configurar los parametros del codigo de barras:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>   
            
        <asp:Panel ID="pnlError" runat="server" Visible="False">    
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>  

        <div class="toggle-message" style="z-index: 590;">
            <h3 class="title">Nueva Configuración...
                <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
            <div class="hide-message" id="hide-message" style="display: none;">
                
                <div class="st-form-line">	
                    <span class="st-labeltext">Codigo de Boton:</span>	
                    <asp:TextBox ID="codBoton" CssClass="st-forminput" style="width:510px" 
                        runat="server" TabIndex="1" MaxLength="50" 
                        ToolTip="Codigo de Boton." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>   

                <div class="st-form-line">	
                    <span class="st-labeltext">Proccess Code:</span>	
                    <asp:TextBox ID="proccesCode" CssClass="st-forminput" style="width:510px" 
                        runat="server" TabIndex="1" MaxLength="50" 
                        ToolTip="Proccess Code." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>
                
                <div class="st-form-line">	
                    <span class="st-labeltext">Nombre Recaudacion:</span>	
                    <asp:TextBox ID="nombreR" CssClass="st-forminput" style="width:510px" 
                        runat="server" TabIndex="1" MaxLength="50" 
                        ToolTip="NombreRecaudacion." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div> 

                <div class="st-form-line">	
                    <span class="st-labeltext">Nombre:</span>	
                    <asp:TextBox ID="nombre" CssClass="st-forminput" style="width:510px" 
                        runat="server" TabIndex="1" MaxLength="50" 
                        ToolTip="Nombre." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div> 
                
                <div class="st-form-line" id="contenedorInicial">	
                    <span class="st-labeltext">Posicion Inicial:</span>	
                    <asp:TextBox ID="posicionInicial" CssClass="st-forminput" style="width:510px" 
                        runat="server" TabIndex="1" MaxLength="30" 
                        ToolTip="PosicionInicial." onkeydown = "return (event.keyCode!=13);" onkeypress="javascript:return solonumeros(event,1)"></asp:TextBox>
                    <div class="clear"></div>
                </div> 

                <div class="st-form-line" id="contenedorFinal">	
                    <span class="st-labeltext">Posicion Final:</span>	
                    <asp:TextBox ID="posicionFinal" CssClass="st-forminput" style="width:510px" 
                        runat="server" TabIndex="1" MaxLength="30" MaskType="Number"
                        ToolTip="PosicionFinal." onkeydown = "return (event.keyCode!=13);" onkeypress="javascript:return solonumeros(event,2)"></asp:TextBox>
                    <div class="clear"></div>
                </div> 

                <div class="st-form-line">	
                    <span class="st-labeltext">Prefijo:</span>	
                    <asp:TextBox ID="prefijo" CssClass="st-forminput" style="width:510px" 
                        runat="server" TabIndex="1" MaxLength="50" 
                        ToolTip="Prefijo." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div> 

                <div class="button-box">
                    <asp:Button ID="btnAddWebModule" runat="server" Text="Adicionar Parametros de Codigo de Barras" 
                        CssClass="button-aqua" TabIndex="4" OnClientClick="return validateAddWebModule();" />
                    <asp:Button ID="btnFinalize" runat="server" Text="Finalizar" 
                        CssClass="st-button" TabIndex="5" Visible="false"  />
                </div> 
              
            </div>
        </div>

    </div>
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
	    <div class="titleh">
    	    <h3>Configuraciones creadas</h3>
        </div>
        <div class="body">    
            <br />            
            <asp:GridView ID="grdWebModules" runat="server" AllowPaging="True" 
                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                DataSourceID="dsWebModules" ForeColor="#333333"
                CssClass="mGridCenter" DataKeyNames="opc_id" 
                EmptyDataText="No existen configuraciones recomendadas." 
                HorizontalAlign="Left" Width="1060px">
                <RowStyle BackColor="White" ForeColor="White" />
                <Columns>
                    <asp:TemplateField HeaderText="Código" InsertVisible="False" 
                        SortExpression="opc_id">
                        <ItemTemplate>
                            <asp:Label ID="lblID1" runat="server" Text='<%# Bind("opc_id") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Codigo Boton" SortExpression="opc_cod_boton">
                        <ItemTemplate>
                            <asp:Label ID="lblID2" runat="server" Text='<%# Bind("opc_cod_boton") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Proccess Code" SortExpression="opc_proccess_code">
                        <ItemTemplate>
                            <asp:Label ID="lblID3" runat="server" Text='<%# Bind("opc_proccess_code") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Nombre Recaudo" SortExpression="opc_nombre_recaudo">
                        <ItemTemplate>
                            <asp:Label ID="lblID4" runat="server" Text='<%# Bind("opc_nombre_recaudo") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Nombre" SortExpression="opc_nombre">
                        <ItemTemplate>
                            <asp:Label ID="lblID5" runat="server" Text='<%# Bind("opc_nombre") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                     <asp:TemplateField HeaderText="PosicionInicial " SortExpression="opc_posicion_inicial">
                        <ItemTemplate>
                            <asp:Label ID="lblID6" runat="server" Text='<%# Bind("opc_posicion_inicial") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>



                    <asp:TemplateField HeaderText="PosicionFinal" SortExpression="opc_posicion_final">
                        <ItemTemplate>
                            <asp:Label ID="lblID7" runat="server" Text='<%# Bind("opc_posicion_final") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Prefijo" SortExpression="opc_nombre">
                        <ItemTemplate>
                            <asp:Label ID="lblID8" runat="server" Text='<%# Bind("opc_prefijo") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>                        
                            <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False" 
                                CommandName="Delete" ImageUrl="~/img/icons/16x16/delete.png" Text="Eliminar" 
                                ToolTip="Eliminar Configuración" CommandArgument='<%# grdWebModules.Rows.Count %>' /> 
                              <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False"
                            CommandName="Edit" ImageUrl="~/img/icons/16x16/edit.png" Text="Editar"
                            ToolTip="Editar Configuración" CommandArgument='<%# grdWebModules.Rows.Count %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>

                    
                </Columns>
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
                    CssClass="pgr" Font-Underline="False" />
                <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid" 
                    BorderWidth="1px" />
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>            
            <br />
	        <asp:SqlDataSource ID="dsWebModules" runat="server" 
                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"                 
                SelectCommand="SELECT O.opc_id, O.opc_cod_boton,O.opc_proccess_code,O.opc_nombre_recaudo,O.opc_nombre, O.opc_posicion_inicial, O.opc_posicion_final, O.opc_prefijo FROM codigo_barras O ORDER BY opc_id asc" 
                InsertCommand="sp_webCrearCodigo_barras" InsertCommandType="StoredProcedure"
                DeleteCommand="DELETE FROM codigo_barras WHERE opc_id = @opc_id">
                <InsertParameters>
                    <asp:Parameter Name="optionCodigoBoton" Type="String" />
                    <asp:Parameter Name="optionProccessCode" Type="String" />
                    <asp:Parameter Name="optionNombreRecaudo" Type="String" />
                    <asp:Parameter Name="optionNombre" Type="String" />
                    <asp:Parameter Name="optionPosicionInicial" Type="String" />
                    <asp:Parameter Name="optionPosicionfinal" Type="String" />
                    <asp:Parameter Name="optionPrefijo" Type="String" />
                </InsertParameters>
                <DeleteParameters>
                    <asp:Parameter Name="opc_id" Type="String" />
                </DeleteParameters>
            </asp:SqlDataSource> 
        </div>
    </div>
    
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Group/PManager.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>



<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">
    
    <!-- Validator -->
    <script src="../js/ValidatorSecurity.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    <script type="text/javascript">
        function solonumeros(e,dato) {
 
            var key;
            var texto;
            if (window.event) // IE
            {
                key = e.keyCode;
                
            }
            else if (e.which) // Netscape/Firefox/Opera
            {
                key = e.which;
            }

            if (key == 48) {
                if (dato == 1) {
                    texto = document.getElementById("<%=posicionInicial.ClientID%>").value;
                    if (texto=="") {
                        console.log("no puede ingresar 0 como parametro inicial");
                        return false;
                    } else {
                        return true;
                    }
                }
                if (dato == 2) {
                    texto = document.getElementById("<%=posicionFinal.ClientID%>").value;
                    if (texto=="") {
                        console.log("no puede ingresar 0 como parametro final");
                        return false;
                    } else {
                        return true;
                    }
                }
            }
 
            if (key < 49 || key > 57) {
                return false;
            }
 
            return true;
        }
 
    </script>
</asp:Content>

