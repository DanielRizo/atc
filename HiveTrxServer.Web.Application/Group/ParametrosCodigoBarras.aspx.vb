﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO


Partial Class Security_WebModules
    Inherits TeleLoader.Web.BasePage

    Public ReadOnly IMG_PATH As String = "img/icons/sidemenu/"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub grdWebModules_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdWebModules.RowCommand
        Try

            Select Case e.CommandName

                Case "Edit"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Update")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try
                    grdWebModules.SelectedIndex = Convert.ToString(e.CommandArgument)
                    Dim ob As String = grdWebModules.SelectedDataKey.Values.Item("opc_id")
                    objSessionParams.codigoConfiguracion = ob

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("EditConfiguracionCodigo.aspx", False)

                Case "Delete"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try
                    grdWebModules.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    Dim strData As String = "Opción ID.: " & grdWebModules.SelectedDataKey.Values.Item("opc_id") _
                                & ", Codigo de Boton: " & CType(grdWebModules.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblID2"), Label).Text _
                                & ", Proccess Code: " & CType(grdWebModules.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblID3"), Label).Text _
                                & ", Nombre Recaudo: " & CType(grdWebModules.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblID4"), Label).Text _
                                & ", Nombre: " & CType(grdWebModules.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblID5"), Label).Text _
                                & ", PosicionInicial: " & CType(grdWebModules.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblID6"), Label).Text _
                                & ", PosicionFinal: " & CType(grdWebModules.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblID7"), Label).Text _
                                & ", Prefijo: " & CType(grdWebModules.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblID8"), Label).Text


                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Configuración Eliminada. Datos[ " & strData & " ]", "")

                    pnlError.Visible = False
                    pnlMsg.Visible = True
                    lblMsg.Text = "Configuración Eliminada"

                    grdWebModules.SelectedIndex = -1
            End Select

            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub

    Protected Sub btnAddWebModule_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddWebModule.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Save")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        Try
            'Set Parameters
            dsWebModules.InsertParameters.Clear()
            dsWebModules.InsertParameters.Add("optionCodigoBoton", TypeCode.String, codBoton.Text)
            dsWebModules.InsertParameters.Add("optionProccessCode", TypeCode.String, proccesCode.Text)
            dsWebModules.InsertParameters.Add("optionNombreRecaudo", TypeCode.String, nombreR.Text)
            dsWebModules.InsertParameters.Add("optionNombre", TypeCode.String, nombre.Text)
            dsWebModules.InsertParameters.Add("optionPosicionInicial", TypeCode.String, posicionInicial.Text)
            dsWebModules.InsertParameters.Add("optionPosicionfinal", TypeCode.String, posicionFinal.Text)
            dsWebModules.InsertParameters.Add("optionPrefijo", TypeCode.String, prefijo.Text)

            If dsWebModules.Insert() >= 1 Then
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Confiuguración creada. Datos[ " & getFormDataLog() & " ]", "")
                pnlError.Visible = False
                pnlMsg.Visible = True
                lblMsg.Text = "Configuración creada correctamente"
                grdWebModules.DataBind()
                dsWebModules.DataBind()
            Else
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Creando Parametros de Codigo de Barras . Datos[ " & getFormDataLog() & " ]", "")
                pnlMsg.Visible = False
                pnlError.Visible = True
                lblError.Text = "Error Creando Parametros de Codigo de Barras"
            End If

            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub

    Protected Sub dsWebModules_Deleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceCommandEventArgs) Handles dsWebModules.Deleting
        If pnlError.Visible = True Then
            e.Cancel = True
        End If
    End Sub
End Class
