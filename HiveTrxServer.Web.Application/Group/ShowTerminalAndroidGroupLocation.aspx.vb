﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users
Imports System.Net
Imports System.Globalization
Imports TeleLoader.Terminals

Partial Class Group_ShowTerminalAndroidGroupLocation
    Inherits TeleLoader.Web.BasePage

    Dim terminalObj As Terminal
    Dim contactObj As TeleLoader.Contact

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Modificación MDM; EB: 20/Feb/2018
        'Lat/Lng por default
        Dim lat As String = "21.983801"
        Dim lng As String = "-40.385742"

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            terminalObj = New Terminal(strConnectionString, objSessionParams.intSelectedGroup)

            'Leer datos BD
            terminalObj.TerminalID = objSessionParams.intSelectedTerminalID
            terminalObj.getTerminalData()

            txtSerial.Text = terminalObj.TerminalSerial

            txtGroupName.Text = objSessionParams.strtSelectedGroup

            'Crear Javascript marker y posición para Google Map

            Dim markerImg As String = ""

            'Establecer Tipo de Marcador (Rojo o Azúl)
            If Math.Abs(DateDiff(DateInterval.Hour, terminalObj.DatetimeAutoUpdate, DateTime.Now)) > 24 Then
                markerImg = "../img/markerRed.png"
            Else
                markerImg = "../img/markerBlue.png"
            End If

            ltrScript.Text = "<script language='javascript'>$(function() {" & _
                                "if (GBrowserIsCompatible()) {" & _
                                    "var map = new GMap2(document.getElementById('map_canvas'));"

            'Validar Lat/Long
            If terminalObj.Latitude <> "0" And terminalObj.Longitude <> "0" Then
                ltrScript.Text &= "map.setCenter(new GLatLng(" & terminalObj.Latitude & ", " & terminalObj.Longitude & "), 15);"
            Else
                ltrScript.Text &= "map.setCenter(new GLatLng(" & lat & ", " & lng & "), 2);"
            End If

            ltrScript.Text &= "map.setUIToDefault();" & _
                                    "var baseIcon = new GIcon(G_DEFAULT_ICON);" & _
                                    "baseIcon.iconSize = new GSize(24, 33);" & _
                                    "baseIcon.shadowSize = new GSize(80, 64);" & _
                                    "baseIcon.iconAnchor = new GPoint(9, 34);" & _
                                    "baseIcon.infoWindowAnchor = new GPoint(9, 2);" & _
                                    "var Icon = new GIcon(baseIcon);" & _
                                    "Icon.image = '" & markerImg & "';" & _
                                    "markerOptions = { icon: Icon };"

            'Validar Lat/Long
            If terminalObj.Latitude <> "0" And terminalObj.Longitude <> "0" Then
                ltrScript.Text &= "var marker = new GMarker(new GLatLng(" & terminalObj.Latitude & ", " & terminalObj.Longitude & "), markerOptions);"
            Else
                ltrScript.Text &= "var marker = new GMarker(new GLatLng(" & lat & ", " & lng & "), markerOptions);"
            End If

            Dim logo As String

            Select Case terminalObj.TerminalModel()
                Case 1
                    'T700
                    logo = "../img/spectraT700.png"
                Case 2
                    'T800
                    logo = "../img/spectraT800.png"
                Case 3, 5
                    'T1000, A5-T1000
                    logo = "../img/spectraT1000.png"
                Case 4
                    'CREON
                Case 6
                    'Android POS - i80 Sunyard
                    logo = "../img/i80.png"
                Case 20
                    'Modificación MDM; EB: 30/Ene/2018
                    'Android POS - 9220 NewPos
                    logo = "../img/NewPOS9220.png"
                Case 21
                    'Modificacion Mdm Polaris 
                    'Terminal SUNMI T1 MINI
                    logo = "../img/sunmiT1.png"
            End Select

            'Modificación MDM; EB: 30/Ene/2018
            If Not terminalObj.isAndroidGroup Then
                'Salida HTML POS Tradicionales
                ltrScript.Text &= "var htmlVentana = ""<table><tr><td align=center><img src='" & logo & "'/></td><td>&nbsp;&nbsp;&nbsp;</td><td><b>Fecha Reporte: </b><br/>" & IIf(terminalObj.DatetimeAutoUpdate.Year = 2001, "-", terminalObj.DatetimeAutoUpdate) & "<br/><b>IMEI: </b><br/>" & terminalObj.IMEI & "<br/><b>SIM: </b><br/>" & terminalObj.SerialSIM & "<br/><b>Nivel Señal: </b><br/>" & terminalObj.SignalLevel & "%<br/><b>Nivel Batería: </b><br/>" & terminalObj.BatteryLevel & "%<br/><b>APN: </b><br/>" & terminalObj.APN & "<br/></td></tr></table>"";" & _
                                        "GEvent.addListener(marker, 'click', function () {" & _
                                        "    marker.openInfoWindowHtml(htmlVentana);" & _
                                        "});" & _
                                        "map.addOverlay(marker);" & _
                                        "marker.openInfoWindowHtml(htmlVentana);" & _
                                    "}" & _
                                "});</script>"
            Else
                'Salida HTML MDM Android
                ltrScript.Text &= "var htmlVentana = ""<table><tr><td align=center><img src='" & logo & "'/></td><td>&nbsp;&nbsp;&nbsp;</td><td><b>Última Conexión: </b><br/>" & IIf(terminalObj.DatetimeAutoUpdate.Year = 2001, "-", terminalObj.DatetimeAutoUpdate) & "<br/><b>Heracles ID: </b><br/>" & terminalObj.HeraclesID & "<br/><b>Dirección IP: </b><br/>" & terminalObj.DeviceIP & "<br/><b>Nombre: </b><br/>" & terminalObj.HeraclesName & "<br/><b>Ciudad: </b><br/>" & terminalObj.HeraclesCity & "<br/><b>Región: </b><br/>" & terminalObj.HeraclesRegion & "<br/></td></tr></table>"";" & _
                                        "GEvent.addListener(marker, 'click', function () {" & _
                                        "    marker.openInfoWindowHtml(htmlVentana);" & _
                                        "});" & _
                                        "map.addOverlay(marker);" & _
                                        "marker.openInfoWindowHtml(htmlVentana);" & _
                                    "}" & _
                                "});</script>"
            End If

        End If

    End Sub

End Class
