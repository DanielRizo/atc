﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="TerminalAcquirer.aspx.vb" Inherits="Groups_Acquirer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: ACQUIRER STIS
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>

    <li>Adquirientes STIS</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    <%--ADQUIRIENTES STIS: [ <%= objSessionParams.StrAcquirerCode %> ]--%>
    Acquirer
    <asp:HyperLink ID="HyperLink1" runat="server"
        NavigateUrl="~/Group/TerminalStis.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlError" runat="server" Visible="False">
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <div class="toggle-message" style="z-index: 590; top: 0px; left: 0px;">
            <h3 class="title">Filtro de B&uacute;squeda...
                <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
            <div class="hide-message" id="hide-message" style="display: none;">
                <div class="st-form-line">
                    <span class="st-labeltext"><b>Acquirer Code </b></span>
                    <asp:TextBox ID="txtCode" CssClass="st- "
                        runat="server" TabIndex="1" MaxLength="50"
                        ToolTip="Code Acquirer" onkeydown="return (event.keyCode!=13);" Width="250px"></asp:TextBox>
                </div>
                <div class="button-box">
                    <asp:Button ID="btnConsultar" runat="server" Text="Filtrar"
                        CssClass="button-aqua" TabIndex="3" ToolTip="Filtrar Terminales Stis..." />
                </div>
            </div>
        </div>

    </div>
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
        <div class="titleh">
            <h3>Agregar nuevo acquirer</h3>
            <div class="shortcuts-icons" style="z-index: 660;">   
                <a class="shortcut tips" href="AddAcquirer.aspx" original-title="Agregar Nuevo Acquirer">
                    <img src="../img/icons/shortcut/plus.png" width="25" height="25" alt="icon"></a>
              
            </div>
        </div>

         <ul id="navst">
	<li class="current"><a href="TerminalStis.aspx">Polaris TMS</a></li>
	<li><a>Tables</a>
		<ul>
			<li><a href="TerminalAcquirer.aspx">Acquirer</a></li>
			<li><a href="TerminalIssuer.aspx">Issuer</a>	</li>
			<li><a href="TerminalCardRange.aspx">CardRange</a></li>
			<li><a href="TerminalEmvLevel2.aspx">Emv Level 2 Application</a></li>
			<li><a href="TerminalEmvKey.aspx">Emv Level 2 Key</a></li>
			<li><a href="TerminalExtraApplication.aspx">Extra Application Parameter</a></li>

		</ul>
	</li>
	
</ul>

        <div class="body">

            <div id="posStis" style="overflow: auto; width: auto; height: 660px;">

            <br />
            <asp:GridView ID="grdTerminalsAcquirer" runat="server"
                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4"
                DataSourceID="dsTerminalsAcquirer"
                CssClass="mGridCenter" Datakeynames="ACQUIRER_CODE,ACQUIRER_KEY_NAME"
                EmptyDataText="No existen Adquirientes con el filtro aplicado"
                HorizontalAlign="Left" ForeColor="#333333" GridLines="None" Width="1060px">
                <RowStyle BackColor="#E3EAEB" />
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <%--<asp:CommandField ButtonType="Image"  HeaderImageUrl="~/img/icons/16x16/edit.png" ShowEditButton="True" UpdateImageUrl="~/img/icons/16x16/ok.png" CancelImageUrl="~/img/icons/16x16/cancel.png" editimageurl="~/img/icons/16x16/edit.png" />--%>
                    <asp:BoundField DataField="ACQUIRER_CODE" HeaderText="Code"
                        SortExpression="ACQUIRER_CODE" />
                    <asp:BoundField DataField="ACQUIRER_KEY_NAME" HeaderText="Display Name"
                        SortExpression="ACQUIRER_KEY_NAME" />
                    <asp:BoundField DataField="ACQUIRER_NII" HeaderText="NII"
                        SortExpression="ACQUIRER_NII" />
                    <asp:BoundField DataField="CREATED_DT" HeaderText="Fecha Creacion"
                        SortExpression="CREATED_DT" ItemStyle-HorizontalAlign="Center" ReadOnly="true">
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundField>
                    <asp:TemplateField ShowHeader="False" HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False"
                                CommandName="EditAcquirer" ImageUrl="~/img/icons/16x16/add_products.png" Text="Acquirer"
                                ToolTip="ACQUIRER" CommandArgument='<%# grdTerminalsAcquirer.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />
                              <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False"
                                CommandName="DeleteAcquirer" ImageUrl="~/img/icons/16x16/delete.png" Text="Eliminar"
                                ToolTip="Eliminar Adquiriente" CommandArgument='<%# grdTerminalsAcquirer.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink"
                                OnClientClick="return ConfirmAction(this);" />
                        </ItemTemplate>
                        <HeaderStyle Width="100px"></HeaderStyle>

                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#1C5E55" ForeColor="White" Font-Bold="True" />
                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center"
                    CssClass="pgr" />
                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BorderColor="#666666" BorderStyle="Solid"
                    BorderWidth="1px" BackColor="#50A2A3" />
                <SortedAscendingCellStyle BackColor="#F8FAFA" />
                <SortedAscendingHeaderStyle BackColor="#246B61" />
                <SortedDescendingCellStyle BackColor="#D4DFE1" />
                <SortedDescendingHeaderStyle BackColor="#15524A" />
            </asp:GridView>
            <br />
                </div>
            <asp:SqlDataSource ID="dsTerminalsAcquirer" runat="server"
                ConnectionString="<%$ ConnectionStrings:TeleLoaderStisConnectionString %>"
                SelectCommand="sp_webConsultarAcquirer_Stis" SelectCommandType="StoredProcedure"
                DeleteCommand="sp_webEliminarAdquiriente" DeleteCommandType="StoredProcedure" UpdateCommand="sp_webEditAcquirerNii_Stis" UpdateCommandType="StoredProcedure">
                <SelectParameters>
                    <asp:Parameter Name="ACQUIRER_CODE" Type="string" />
                    <asp:Parameter Name="ACQUIRER_KEY_NAME" Type="string" DefaultValue="-1" />
                </SelectParameters>
                <DeleteParameters>
                    <asp:Parameter Name="ACQUIRER_CODE" Type="String" />
                   
                </DeleteParameters>
                <UpdateParameters>
                    <asp:Parameter Name="ACQUIRER_CODE" Type="String" />
                    <asp:Parameter Name="ACQUIRER_KEY_NAME" Type="String" />
                    <asp:Parameter Name="ACQUIRER_NII" Type="String" />
                </UpdateParameters>
            </asp:SqlDataSource>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

    <script language="javascript">
        var globalcontrol = '';

        function ConfirmAction(control) {

            globalcontrol = control;

            $.msgAlert({
                type: "warning"
                , title: "Mensaje del Sistema"
                , text: "Realmente desea eliminar El Adquiriente?"
                , callback: function () {
                    __doPostBack($(globalcontrol).attr('name'), '');
                }
            });

            return false;
        }
    </script>

</asp:Content>

