﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Applications
Imports System.Data.Common

Partial Class Groups_Acquirer
    Inherits TeleLoader.Web.BasePage

    Dim applicationObj As ApplicationAcquirer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            txtCode.Focus()

            dsTerminalsAcquirer.SelectParameters("ACQUIRER_CODE").DefaultValue = objSessionParams.intACQUIRER_CODE


            dsTerminalsAcquirer.DataBind()

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub grdTerminals_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdTerminalsAcquirer.RowCommand
        Try

            Select Case e.CommandName
                'Elminaciion de Acquirer
                Case "DeleteAcquirer"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)

                    End Try

                    grdTerminalsAcquirer.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    dsTerminalsAcquirer.DeleteParameters("ACQUIRER_CODE").DefaultValue = grdTerminalsAcquirer.SelectedDataKey.Values.Item("ACQUIRER_CODE")

                    Dim strData As String = "Terminal ID: " & grdTerminalsAcquirer.SelectedDataKey.Values.Item("ACQUIRER_CODE")

                    dsTerminalsAcquirer.Delete()

                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Adquiriente eliminado. Datos[ " & strData & " ]", "")

                    grdTerminalsAcquirer.DataBind()

                    pnlError.Visible = False
                    pnlMsg.Visible = True
                    lblMsg.Text = "Adquiriente eliminada del Grupo"

                    grdTerminalsAcquirer.SelectedIndex = -1


                Case "EditAcquirer"
                    grdTerminalsAcquirer.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.StrAcquirerCode = grdTerminalsAcquirer.SelectedDataKey.Values.Item("ACQUIRER_CODE")
                    objSessionParams.StrAcquirerName = grdTerminalsAcquirer.SelectedDataKey.Values.Item("ACQUIRER_KEY_NAME")
                    'objSessionParams.StrIsExtended = grdTerminalsAcquirer.SelectedDataKey.Values.Item("IS_EXTENDED")


                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("ListarAcquirer.aspx", False)
            End Select

            If txtCode.Text = "" Then
                'Set Invisible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
            Else
                'Set Visible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
            End If

        Catch ex As Exception
            lblMsg.Text = "Error Eliminando Adquiriente, se encuentra realacionado Con otra Tabla"
            HandleErrorRedirect(ex)

        End Try
    End Sub
    'Edicion de GriedView Acquierer Stis 
    'Autor : Oscar Gutierrez
    Protected Sub grdTerminalsAcquirer_Updated(sender As Object, e As GridViewUpdatedEventArgs) Handles grdTerminalsAcquirer.RowUpdated

        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webEditAcquirerNii_Stis"
            command.Parameters.Clear()


            command.Parameters.Add(New SqlParameter("ACQUIRER_CODE ", e.OldValues("ACQUIRER_CODE ")))
            command.Parameters.Add(New SqlParameter("ACQUIRER_KEY_NAME", e.OldValues("ACQUIRER_KEY_NAME")))
            command.Parameters.Add(New SqlParameter("ACQUIRER_NII", e.NewValues("ACQUIRER_NII")))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try


    End Sub

    Protected Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click

        If txtCode.Text <> "" Then
            dsTerminalsAcquirer.SelectParameters("ACQUIRER_CODE").DefaultValue = txtCode.Text
        Else
            dsTerminalsAcquirer.SelectParameters("ACQUIRER_CODE").DefaultValue = "-1"
        End If
        grdTerminalsAcquirer.DataBind()

        If txtCode.Text = "" Then
            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
        Else
            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
        End If

    End Sub

    Private Sub clearForm()
        txtCode.Text = ""

    End Sub
End Class
