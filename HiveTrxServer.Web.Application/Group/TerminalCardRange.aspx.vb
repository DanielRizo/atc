﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Applications
Imports System.Data.Common

Partial Class Groups_CardRange
    Inherits TeleLoader.Web.BasePage

    Dim applicationObj As ApplicationIssuer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            txtCodeCardRange.Focus()

            dsTerminalsAcquirer.SelectParameters("CARD_CODE").DefaultValue = objSessionParams.intCARD_CODE

            dsTerminalsAcquirer.DataBind()

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub grdTerminals_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdTerminalsCardRange.RowCommand
        Try

            Select Case e.CommandName

                Case "DeleteCardRange"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    grdTerminalsCardRange.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    dsTerminalsAcquirer.DeleteParameters("CARD_CODE").DefaultValue = grdTerminalsCardRange.SelectedDataKey.Values.Item("CARD_CODE")

                    Dim strData As String = "Terminal ID: " & grdTerminalsCardRange.SelectedDataKey.Values.Item("CARD_CODE")

                    dsTerminalsAcquirer.Delete()

                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "CardRange eliminado. Datos[ " & strData & " ]", "")

                    grdTerminalsCardRange.DataBind()

                    pnlError.Visible = False
                    pnlMsg.Visible = True
                    lblMsg.Text = "CardRange eliminada del Grupo"

                    grdTerminalsCardRange.SelectedIndex = -1


                Case "EditCardRange"
                    grdTerminalsCardRange.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.StrAcquirerCode = grdTerminalsCardRange.SelectedDataKey.Values.Item("CARD_CODE")
                    objSessionParams.StrCardRangeName = grdTerminalsCardRange.SelectedDataKey.Values.Item("CARD_KEY_NAME")


                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("ListarCardRange.aspx", False)
            End Select

            If txtCodeCardRange.Text = "" Then
                'Set Invisible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
            Else
                'Set Visible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
            End If

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub
    'Edicion de Parametros CardRange Stis 
    'Autor : Oscar Gutierrez
    Protected Sub grdTerminalsCardRange_Updated(sender As Object, e As GridViewUpdatedEventArgs) Handles grdTerminalsCardRange.RowUpdated

        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webEditParameterCardRange_Stis"
            command.Parameters.Clear()


            command.Parameters.Add(New SqlParameter("CARD_CODE", e.OldValues("CARD_CODE")))
            command.Parameters.Add(New SqlParameter("CARD_KEY_NAME", e.OldValues("CARD_KEY_NAME")))
            command.Parameters.Add(New SqlParameter("CARD_DESCRIPTION", e.NewValues("CARD_DESCRIPTION")))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try


    End Sub


    Protected Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click

        If txtCodeCardRange.Text <> "" Then
            dsTerminalsAcquirer.SelectParameters("CARD_CODE").DefaultValue = txtCodeCardRange.Text
        Else
            dsTerminalsAcquirer.SelectParameters("CARD_CODE").DefaultValue = "-1"
        End If
        grdTerminalsCardRange.DataBind()

        If txtCodeCardRange.Text = "" Then
            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
        Else
            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
        End If

    End Sub

    Private Sub clearForm()
        txtCodeCardRange.Text = ""

    End Sub
End Class
