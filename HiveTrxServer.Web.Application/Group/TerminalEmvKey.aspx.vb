﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Applications
Imports System.Data.Common

Partial Class Groups_EmvLevel2Key
    Inherits TeleLoader.Web.BasePage

    Dim applicationObj As ApplicationEmvApplication

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            txtCodeDisplayName.Focus()

            dsTerminalsKey.SelectParameters("TABLE_KEY_CODE").DefaultValue = objSessionParams.intEmvApplication

            dsTerminalsKey.DataBind()

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub grdTerminals_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdTerminalsEmvKey.RowCommand
        Try

            Select Case e.CommandName

                Case "DeleteEmvKey"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    grdTerminalsEmvKey.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    dsTerminalsKey.DeleteParameters("TABLE_KEY_CODE").DefaultValue = grdTerminalsEmvKey.SelectedDataKey.Values.Item("TABLE_KEY_CODE")

                    Dim strData As String = "Terminal ID: " & grdTerminalsEmvKey.SelectedDataKey.Values.Item("TABLE_KEY_CODE")

                    dsTerminalsKey.Delete()

                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Emv Key eliminado. Datos[ " & strData & " ]", "")

                    grdTerminalsEmvKey.DataBind()

                    pnlError.Visible = False
                    pnlMsg.Visible = True
                    lblMsg.Text = "Emv Key Eliminada de Forma Correcta"

                    grdTerminalsEmvKey.SelectedIndex = -1


                Case "EditEmvApplication"
                    grdTerminalsEmvKey.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.strEmvApplication = grdTerminalsEmvKey.SelectedDataKey.Values.Item("TABLE_KEY_CODE")
                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("ListarEmvLevel2KeyParameter.aspx", False)
            End Select

            If txtCodeDisplayName.Text = "" Then
                'Set Invisible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
            Else
                'Set Visible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
            End If

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub

    Protected Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click

        If txtCodeDisplayName.Text <> "" Then
            dsTerminalsKey.SelectParameters("TABLE_KEY_CODE").DefaultValue = txtCodeDisplayName.Text
        Else
            dsTerminalsKey.SelectParameters("TABLE_KEY_CODE").DefaultValue = "-1"
        End If
        grdTerminalsEmvKey.DataBind()

        If txtCodeDisplayName.Text = "" Then
            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
        Else
            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
        End If

    End Sub

    Private Sub clearForm()
        txtCodeDisplayName.Text = ""

    End Sub
End Class
