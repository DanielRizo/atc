﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Applications
Imports System.Data.Common

Partial Class Groups_EmvLevel2
    Inherits TeleLoader.Web.BasePage

    Dim applicationObj As ApplicationEmvApplication

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            txtCodeDisplayName.Focus()

            dsTerminalsApplication.SelectParameters("TABLE_KEY_CODE").DefaultValue = objSessionParams.intEmvApplication

            dsTerminalsApplication.DataBind()

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub grdTerminals_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdTerminalsEmvApplication.RowCommand
        Try

            Select Case e.CommandName

                Case "DeleteEmvLevel2"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    grdTerminalsEmvApplication.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    dsTerminalsApplication.DeleteParameters("TABLE_KEY_CODE").DefaultValue = grdTerminalsEmvApplication.SelectedDataKey.Values.Item("TABLE_KEY_CODE")

                    Dim strData As String = "Terminal ID: " & grdTerminalsEmvApplication.SelectedDataKey.Values.Item("TABLE_KEY_CODE")

                    dsTerminalsApplication.Delete()

                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "EMV APPLICATION eliminado. Datos[ " & strData & " ]", "")

                    grdTerminalsEmvApplication.DataBind()

                    pnlError.Visible = False
                    pnlMsg.Visible = True
                    lblMsg.Text = "Emv Application Eliminada del Grupo"

                    grdTerminalsEmvApplication.SelectedIndex = -1


                Case "EditEmvApplication"
                    grdTerminalsEmvApplication.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.StrParamterEMV = grdTerminalsEmvApplication.SelectedDataKey.Values.Item("TABLE_KEY_CODE")
                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("ListarParameterEmvApplication.aspx", False)
            End Select

            If txtCodeDisplayName.Text = "" Then
                'Set Invisible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
            Else
                'Set Visible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
            End If

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub

    Protected Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click

        If txtCodeDisplayName.Text <> "" Then
            dsTerminalsApplication.SelectParameters("TABLE_KEY_CODE").DefaultValue = txtCodeDisplayName.Text
        Else
            dsTerminalsApplication.SelectParameters("TABLE_KEY_CODE").DefaultValue = "-1"
        End If
        grdTerminalsEmvApplication.DataBind()

        If txtCodeDisplayName.Text = "" Then
            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
        Else
            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
        End If

    End Sub

    Private Sub clearForm()
        txtCodeDisplayName.Text = ""

    End Sub
End Class
