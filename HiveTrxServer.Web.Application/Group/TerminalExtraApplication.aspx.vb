﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Applications
Imports System.Data.Common

Partial Class Groups_ExtraApplication
    Inherits TeleLoader.Web.BasePage

    Dim applicationObj As ApplicationEmvApplication

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            txtCodeDisplayName.Focus()

            dsTerminalsExtraApplication.SelectParameters("APP_PARA_NAME").DefaultValue = objSessionParams.intExtraApplication

            dsTerminalsExtraApplication.DataBind()

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub grdTerminals_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdTerminalsExtraApplication.RowCommand
        Try

            Select Case e.CommandName

                Case "DeleteExtraApplication"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    grdTerminalsExtraApplication.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    dsTerminalsExtraApplication.DeleteParameters("APP_PARA_NAME").DefaultValue = grdTerminalsExtraApplication.SelectedDataKey.Values.Item("APP_PARA_NAME")

                    Dim strData As String = "Terminal ID: " & grdTerminalsExtraApplication.SelectedDataKey.Values.Item("APP_PARA_NAME")

                    dsTerminalsExtraApplication.Delete()

                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Extra Application eliminado. Datos[ " & strData & " ]", "")

                    grdTerminalsExtraApplication.DataBind()

                    pnlError.Visible = False
                    pnlMsg.Visible = True
                    lblMsg.Text = "Extra Application eliminado Con Exito"

                    grdTerminalsExtraApplication.SelectedIndex = -1


                Case "EditEmvApplication"
                    grdTerminalsExtraApplication.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.StrExtraParameter = grdTerminalsExtraApplication.SelectedDataKey.Values.Item("APP_PARA_NAME")
                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("ListarExtraApplicationParameter.aspx", False)
            End Select

            If txtCodeDisplayName.Text = "" Then
                'Set Invisible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
            Else
                'Set Visible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
            End If

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub

    Protected Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click

        If txtCodeDisplayName.Text <> "" Then
            dsTerminalsExtraApplication.SelectParameters("APP_PARA_NAME").DefaultValue = txtCodeDisplayName.Text
        Else
            dsTerminalsExtraApplication.SelectParameters("APP_PARA_NAME").DefaultValue = "-1"
        End If
        grdTerminalsExtraApplication.DataBind()

        If txtCodeDisplayName.Text = "" Then
            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
        Else
            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
        End If

    End Sub

    Private Sub clearForm()
        txtCodeDisplayName.Text = ""

    End Sub
End Class
