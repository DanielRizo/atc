﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Applications
Imports System.Data.Common

Partial Class Groups_ExtraApplicationParameters
    Inherits TeleLoader.Web.BasePage

    Dim applicationObj As ApplicationEmvApplication

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then



            dsTerminalsExtraApplication.SelectParameters("TERMINAL_REC_NO").DefaultValue = objSessionParams.StrTerminalRecord
            dsTerminalsExtraApplication.SelectParameters("REFER_CODE").DefaultValue = objSessionParams.StrExtraParameter

            dsTerminalsExtraApplication.DataBind()

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub grdTerminals_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdTerminalsExtraApplication.RowCommand
        Try

            Select Case e.CommandName

                Case "DeleteExtraApplication"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    grdTerminalsExtraApplication.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    dsTerminalsExtraApplication.DeleteParameters("TERMINAL_REC_NO").DefaultValue = grdTerminalsExtraApplication.SelectedDataKey.Values.Item("TERMINAL_REC_NO")

                    Dim strData As String = "Terminal ID: " & grdTerminalsExtraApplication.SelectedDataKey.Values.Item("TERMINAL_REC_NO")

                    dsTerminalsExtraApplication.Delete()

                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Extra Application eliminado. Datos[ " & strData & " ]", "")

                    grdTerminalsExtraApplication.DataBind()

                    pnlError.Visible = False
                    pnlMsg.Visible = True
                    lblMsg.Text = "Extra Application eliminado Con Exito"

                    grdTerminalsExtraApplication.SelectedIndex = -1


                Case "EditEmvApplication"
                    grdTerminalsExtraApplication.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.StrExtraParameter = grdTerminalsExtraApplication.SelectedDataKey.Values.Item("APP_PARA_NAME")
                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("ListarExtraApplicationParameter.aspx", False)
            End Select


        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub
    'Edicion de Parametros Extra Application Stis 
    'Autor : Oscar Gutierrez
    Protected Sub grdTerminalsExtraApplication_Updated(sender As Object, e As GridViewUpdatedEventArgs) Handles grdTerminalsExtraApplication.RowUpdated

        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webEditDatosExtraApplication_Stis"
            command.Parameters.Clear()


            command.Parameters.Add(New SqlParameter("TERMINAL_REC_NO", e.OldValues("TERMINAL_REC_NO")))
            command.Parameters.Add(New SqlParameter("FIELD_DISPLAY_NAME", e.OldValues("FIELD_DISPLAY_NAME")))
            command.Parameters.Add(New SqlParameter("REFER_CODE", e.OldValues("REFER_CODE")))
            command.Parameters.Add(New SqlParameter("CONTENT_VALUE", e.NewValues("CONTENT_VALUE")))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try


    End Sub

End Class
