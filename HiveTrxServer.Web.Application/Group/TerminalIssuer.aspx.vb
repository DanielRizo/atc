﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Applications
Imports System.Data.Common

Partial Class Groups_Issuer
    Inherits TeleLoader.Web.BasePage

    Dim applicationObj As ApplicationIssuer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            txtCodeIssuer.Focus()

            dsTerminalsAcquirer.SelectParameters("ISSUER_CODE").DefaultValue = objSessionParams.intISSUER_CODE

            dsTerminalsAcquirer.DataBind()

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub grdTerminals_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdTerminalsIssuer.RowCommand
        Try

            Select Case e.CommandName

                Case "DeleteIssuer"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    grdTerminalsIssuer.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    dsTerminalsAcquirer.DeleteParameters("ISSUER_CODE").DefaultValue = grdTerminalsIssuer.SelectedDataKey.Values.Item("ISSUER_CODE")

                    Dim strData As String = "Terminal ID: " & grdTerminalsIssuer.SelectedDataKey.Values.Item("ISSUER_CODE")

                    dsTerminalsAcquirer.Delete()

                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Issuer eliminado. Datos[ " & strData & " ]", "")

                    grdTerminalsIssuer.DataBind()

                    pnlError.Visible = False
                    pnlMsg.Visible = True
                    lblMsg.Text = "Issuer eliminada del Grupo"

                    grdTerminalsIssuer.SelectedIndex = -1


                Case "EditIssuer"
                    grdTerminalsIssuer.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.StrAcquirerCode = grdTerminalsIssuer.SelectedDataKey.Values.Item("ISSUER_CODE")
                    objSessionParams.StrIssuerName = grdTerminalsIssuer.SelectedDataKey.Values.Item("ISSUER_KEY_NAME")
                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("ListarIssuer.aspx", False)
            End Select

            If txtCodeIssuer.Text = "" Then
                'Set Invisible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
            Else
                'Set Visible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
            End If

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub

    Protected Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click

        If txtCodeIssuer.Text <> "" Then
            dsTerminalsAcquirer.SelectParameters("ISSUER_CODE").DefaultValue = txtCodeIssuer.Text
        Else
            dsTerminalsAcquirer.SelectParameters("ISSUER_CODE").DefaultValue = "-1"
        End If
        grdTerminalsIssuer.DataBind()

        If txtCodeIssuer.Text = "" Then
            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
        Else
            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
        End If

    End Sub

    Private Sub clearForm()
        txtCodeIssuer.Text = ""

    End Sub
End Class
