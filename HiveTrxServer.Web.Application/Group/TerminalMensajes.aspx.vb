﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Applications
Imports System.Data.Common

Partial Class Groups_Mensajes
    Inherits TeleLoader.Web.BasePage

    Dim applicationObj As ApplicationIssuer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            txtPrompt.Focus()
            dsTerminalMensajes.SelectParameters("MENSAJE_CODE").DefaultValue = objSessionParams.intCARD_CODE
            dsTerminalMensajes.DataBind()
        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub grdTerminalMensajes_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdTerminalMensajes.RowCommand
        Try

            Select Case e.CommandName

                Case "DeletePrompts"
                    Dim strConnString As String
                    Dim configurationSection As ConnectionStringsSection =
                    System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
                    strConnString = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
                    Dim connection As New SqlConnection(strConnString)
                    Dim command As New SqlCommand()
                    Dim results As SqlDataReader
                    Dim status As Integer
                    Dim rspMsg As String

                    Dim retVal As Boolean
                    rspMsg = ""
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    grdTerminalMensajes.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    dsTerminalMensajes.DeleteParameters("MENSAJE_CODE").DefaultValue = grdTerminalMensajes.SelectedDataKey.Values.Item("MENSAJE_CODE")

                    Dim strData As String = "Terminal ID: " & grdTerminalMensajes.SelectedDataKey.Values.Item("MENSAJE_CODE")
                    Try
                        'Abrir Conexion
                        connection.Open()

                        command.Connection = connection
                        command.CommandType = CommandType.StoredProcedure
                        command.CommandText = "sp_webEliminarDiferdios"
                        command.Parameters.Clear()
                        command.Parameters.Add(New SqlParameter("MENSAJE_CODE", "" + grdTerminalMensajes.SelectedDataKey.Values.Item("MENSAJE_CODE")))


                        'Ejecutar SP
                        results = command.ExecuteReader()
                        If results.HasRows Then
                            While results.Read()
                                status = results.GetInt32(0)
                                rspMsg = results.GetString(1)
                            End While
                        Else
                            retVal = False
                        End If

                        If status = 1 Then
                            grdTerminalMensajes.DataBind()

                            pnlError.Visible = False
                            pnlMsg.Visible = True
                            lblMsg.Text = rspMsg
                        Else
                            grdTerminalMensajes.DataBind()

                            pnlError.Visible = True
                            pnlMsg.Visible = False
                            lblError.Text = rspMsg
                        End If

                    Catch ex As Exception
                        retVal = False
                    Finally
                        'Cerrar data reader por Default
                        If Not (results Is Nothing) Then
                            results.Close()
                        End If
                        'Cerrar conexion por Default
                        If Not (connection Is Nothing) Then
                            connection.Close()
                        End If
                    End Try

                Case "EditPrompts"
                    grdTerminalMensajes.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.StrAcquirerCode = grdTerminalMensajes.SelectedDataKey.Values.Item("MENSAJE_CODE")
                    objSessionParams.StrCardRangeName = grdTerminalMensajes.SelectedDataKey.Values.Item("MENSAJE_KEY_NAME")


                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("ListarTerminalMensaje.aspx", False)
            End Select

            If txtPrompt.Text = "" Then
                'Set Invisible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
            Else
                'Set Visible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
            End If

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub
    Protected Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click

        If txtCode.Text <> "" Then
            dsTerminalMensajes.SelectParameters("MENSAJE_CODE").DefaultValue = txtCode.Text
        Else
            dsTerminalMensajes.SelectParameters("MENSAJE_CODE").DefaultValue = "-1"
        End If

        If txtPrompt.Text <> "" Then
            dsTerminalMensajes.SelectParameters("MENSAJE_KEY_NAME").DefaultValue = txtPrompt.Text
        Else
            dsTerminalMensajes.SelectParameters("MENSAJE_KEY_NAME").DefaultValue = "-1"
        End If
        grdTerminalMensajes.DataBind()

        If txtPrompt.Text = "" Then
            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
        Else
            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
        End If

    End Sub

    Private Sub clearForm()
        txtPrompt.Text = ""

    End Sub
End Class
