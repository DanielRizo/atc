﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageStis.master" AutoEventWireup="false" CodeFile="TerminalParameterEMV.aspx.vb" Inherits="Groups_ParameterEmvLevel2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: EMV LEVEL 2 APPLICATION STIS
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <%--<script type="text/javascript" src="../js/jquery.tipsy.js"></script>--%>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

       <%--scripts menu--%>
    
 <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.min.js") %>'></script>
         <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery-ui-1.8.11.custom.min.js") %>'></script>
         <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.js") %>'></script>
    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/msgAlert.js") %>'></script>
     
         <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.contextMenu.js") %>'></script>
         <script type="text/javascript" src='<%= ResolveClientUrl("~/js/menuMethods.js") %>'></script>
   

        <%--scripts edit table--%>
  
  <link rel="stylesheet" type="text/css" href="/style/bootstrap.min.css" />

      <script type="text/javascript" src='<%= ResolveClientUrl("~/js/editTable.js") %>'></script>
   
    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.tabledit.js") %>'></script>
    
     <script type="text/javascript" src='<%= ResolveClientUrl("~/js/bootstrap.js") %>'></script>



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>

    <li>EMV LEVEL 2 APPLICATION STIS</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
 
    Terminal > EMVL2 Application
    <asp:HyperLink ID="HyperLink1" runat="server"
        NavigateUrl="~/Group/EditCardRange.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
 
    <h4>
        
    TERMINAL  TID : [ <%= objSessionParams.StrTerminalID %> ]
    RECORD    No : [ <%= objSessionParams.StrTerminalRecord %> ]
    </h4>
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlError" runat="server" Visible="False">
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>
    </div>
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
        <div class="titleh hidden">
            <h3>EMV Level 2 Application  Stis</h3>
            <div class="shortcuts-icons" style="z-index: 660;">  
                <div class="shortcuts-icons" style="z-index: 660;">
                    <img src="../img/icons/sidemenu/download.png" width="25" height="25" alt="icon"></a> </div>
                </div>
           
        </div>
              <ul id="navst">
	<li class="current"><a href="TerminalStis.aspx">Polaris TMS</a></li>
	<li><a>Tables</a>
		<ul>
			<li><a href="TerminalAcquirer.aspx">Acquirer</a></li>
			<li><a href="TerminalIssuer.aspx">Issuer</a>	</li>
			<li><a href="TerminalCardRange.aspx">CardRange</a></li>
			<li><a href="TerminalEmvLevel2.aspx">Emv Level 2 Application</a></li>
			<li><a href="TerminalEmvKey.aspx">Emv Level 2 Key</a></li>
			<li><a href="TerminalExtraApplication.aspx">Extra Application Parameter</a></li>

		</ul>
	</li>
	
</ul>
            <script type="text/javascript">
            $(document).ready(function () {
                enableEditTable("#ctl00_MainContent_grdTerminalsEmvApplication", false,true, false,[[2, 'Value']], "terminalParameterEmv");
            });
        </script>
        <div class="body">
                <div id="posStis" style="overflow: auto; width: auto; height: 660px;">

            <br />
            <asp:GridView ID="grdTerminalsEmvApplication" runat="server"
                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4"
                DataSourceID="dsTerminalsApplication"
                CssClass="mGridCenter" DataKeyNames="TERMINAL_REC_NO"
                EmptyDataText="No existen EMV Application con el filtro aplicado"
                HorizontalAlign="Left"  ForeColor="#333333" GridLines="None">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <%--<asp:CommandField ButtonType="Image"  HeaderImageUrl="~/img/icons/16x16/edit.png" ShowEditButton="True" UpdateImageUrl="~/img/icons/16x16/ok.png" CancelImageUrl="~/img/icons/16x16/cancel.png" editimageurl="~/img/icons/16x16/edit.png" />--%>
                    <asp:BoundField DataField="TERMINAL_REC_NO" HeaderText="Terminal Rec No"
                        SortExpression="TERMINAL_REC_NO" />
                     <asp:BoundField DataField="FIELD_DISPLAY_NAME" HeaderText="Parameter Name"
                        SortExpression="FIELD_DISPLAY_NAME" />
                      <asp:BoundField DataField="CONTENT_DESC" HeaderText="Value"
                        SortExpression="CONTENT_DESC" />
                      <asp:BoundField DataField="IS_EXTENDED" HeaderText="Parameter Type"
                        SortExpression="IS_EXTENDED" readonly="true"/>
                      <asp:BoundField DataField="NOTE" HeaderText="Note"
                        SortExpression="NOTE" readonly="true"/>
                     <asp:BoundField DataField="REFER_CODE" HeaderText="Refer Code"
                        SortExpression="REFER_CODE" />
                 <%--   <asp:TemplateField ShowHeader="False" HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False"
                                CommandName="EditEmvApplication" ImageUrl="~/img/icons/16x16/upload.png" Text="EmvApplication"
                                ToolTip="Listar Parametros Emv Application" CommandArgument='<%# grdTerminalsEmvApplication.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />
                         <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False"
                                CommandName="DeleteEmvLevel2" ImageUrl="~/img/icons/16x16/delete.png" Text="Eliminar"
                                ToolTip="Eliminar Emv Application" CommandArgument='<%# grdTerminalsEmvApplication.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink"
                                OnClientClick="return ConfirmAction(this);" />
                        </ItemTemplate>

                        <HeaderStyle Width="100px"></HeaderStyle>

                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>--%>
                </Columns>
                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <PagerStyle ForeColor="White" HorizontalAlign="Center"
                    CssClass="pgr" BackColor="#666666" />
                <EditRowStyle BorderColor="#666666" BorderStyle="Solid"
                    BorderWidth="1px" BackColor="#50A2A3" />
                <RowStyle BackColor="#E3EAEB" />
                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F8FAFA" />
                <SortedAscendingHeaderStyle BackColor="#246B61" />
                <SortedDescendingCellStyle BackColor="#D4DFE1" />
                <SortedDescendingHeaderStyle BackColor="#15524A" />
            </asp:GridView>
            <br />
                            </div>

            <asp:SqlDataSource ID="dsTerminalsApplication" runat="server"
                ConnectionString="<%$ ConnectionStrings:TeleLoaderStisConnectionString %>"
                SelectCommand="sp_webpParameterEmvlevel2_Stis" SelectCommandType="StoredProcedure"
                UpdateCommand="sp_webEditDatosEmvApplication_Stis" UpdateCommandType="StoredProcedure">
                <SelectParameters>
                    <asp:Parameter Name="TERMINAL_REC_NO" Type="string" />
                    <asp:Parameter Name="REFER_CODE" Type="string" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="TERMINAL_REC_NO" Type="String" />
                    <asp:Parameter Name="FIELD_DISPLAY_NAME" Type="String" />
                    <asp:Parameter Name="CONTENT_DESC" Type="String" />
                    <asp:Parameter Name="REFER_CODE" Type="String" />
                </UpdateParameters>
            </asp:SqlDataSource>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

    <script language="javascript">
        var globalcontrol = '';

        function ConfirmAction(control) {

            globalcontrol = control;

            $.msgAlert({
                type: "warning"
                , title: "Mensaje del Sistema"
                , text: "Realmente desea eliminar la Emv Application?"
                , callback: function () {
                    __doPostBack($(globalcontrol).attr('name'), '');
                }
            });

            return false;
        }
    </script>

</asp:Content>

