﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Applications
Imports System.Data.Common

Partial Class Groups_ParameterEmvLevel2
    Inherits TeleLoader.Web.BasePage

    Dim applicationObj As ApplicationEmvApplication

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            dsTerminalsApplication.SelectParameters("TERMINAL_REC_NO").DefaultValue = objSessionParams.StrTerminalRecord
            dsTerminalsApplication.SelectParameters("REFER_CODE").DefaultValue = objSessionParams.StrParamterEMV

            dsTerminalsApplication.DataBind()

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub grdTerminals_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdTerminalsEmvApplication.RowCommand
        Try

            Select Case e.CommandName

                Case "DeleteEmvLevel2"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    grdTerminalsEmvApplication.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    dsTerminalsApplication.DeleteParameters("TERMINAL_REC_NO").DefaultValue = grdTerminalsEmvApplication.SelectedDataKey.Values.Item("TERMINAL_REC_NO")

                    Dim strData As String = "Terminal ID: " & grdTerminalsEmvApplication.SelectedDataKey.Values.Item("TERMINAL_REC_NO")

                    dsTerminalsApplication.Delete()

                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "EMV APPLICATION eliminado. Datos[ " & strData & " ]", "")

                    grdTerminalsEmvApplication.DataBind()

                    pnlError.Visible = False
                    pnlMsg.Visible = True
                    lblMsg.Text = "Emv Application Eliminada del Grupo"

                    grdTerminalsEmvApplication.SelectedIndex = -1


                Case "EditEmvApplication"
                    grdTerminalsEmvApplication.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.StrParamterEMV = grdTerminalsEmvApplication.SelectedDataKey.Values.Item("TERMINAL_REC_NO")
                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("ListarParameterEmvApplication.aspx", False)
            End Select

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub
    'Edicion de Parametros Emv Level 2 Application Stis 
    'Autor : Oscar Gutierrez
    Protected Sub grdTerminalsEmvApplication_Updated(sender As Object, e As GridViewUpdatedEventArgs) Handles grdTerminalsEmvApplication.RowUpdated

        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webEditDatosEmvApplication_Stis"
            command.Parameters.Clear()


            command.Parameters.Add(New SqlParameter("TERMINAL_REC_NO", e.OldValues("TERMINAL_REC_NO")))
            command.Parameters.Add(New SqlParameter("FIELD_DISPLAY_NAME", e.OldValues("FIELD_DISPLAY_NAME")))
            command.Parameters.Add(New SqlParameter("REFER_CODE", e.OldValues("REFER_CODE")))
            command.Parameters.Add(New SqlParameter("CONTENT_DESC", e.NewValues("CONTENT_DESC")))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try


    End Sub

End Class