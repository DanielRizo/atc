﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Applications
Imports System.Data.Common

Partial Class Groups_Prompts
    Inherits TeleLoader.Web.BasePage

    Dim applicationObj As ApplicationIssuer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            txtPrompt.Focus()
            dsTerminalPrompts.SelectParameters("PROMPT_CODE").DefaultValue = objSessionParams.intCARD_CODE
            dsTerminalPrompts.DataBind()
        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub grdTerminalPrompts_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdTerminalPrompts.RowCommand
        Try

            Select Case e.CommandName
                Case "DeletePrompts"
                    Dim strConnString As String
                    Dim configurationSection As ConnectionStringsSection =
                    System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
                    strConnString = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
                    Dim connection As New SqlConnection(strConnString)
                    Dim command As New SqlCommand()
                    Dim results As SqlDataReader
                    Dim status As Integer
                    Dim rspMsg As String

                    Dim retVal As Boolean
                    rspMsg = ""
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    grdTerminalPrompts.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    dsTerminalPrompts.DeleteParameters("PROMPT_CODE").DefaultValue = grdTerminalPrompts.SelectedDataKey.Values.Item("PROMPT_CODE")

                    Dim strData As String = "Terminal ID: " & grdTerminalPrompts.SelectedDataKey.Values.Item("PROMPT_CODE")
                    Try
                        'Abrir Conexion
                        connection.Open()

                        command.Connection = connection
                        command.CommandType = CommandType.StoredProcedure
                        command.CommandText = "sp_webEliminarPrompts"
                        command.Parameters.Clear()
                        command.Parameters.Add(New SqlParameter("PROMPT_CODE", "" + grdTerminalPrompts.SelectedDataKey.Values.Item("PROMPT_CODE")))


                        'Ejecutar SP
                        results = command.ExecuteReader()
                        If results.HasRows Then
                            While results.Read()
                                status = results.GetInt32(0)
                                rspMsg = results.GetString(1)
                            End While
                        Else
                            retVal = False
                        End If

                        If status = 1 Then
                            grdTerminalPrompts.DataBind()

                            pnlError.Visible = False
                            pnlMsg.Visible = True
                            lblMsg.Text = rspMsg
                        Else
                            grdTerminalPrompts.DataBind()

                            pnlError.Visible = True
                            pnlMsg.Visible = False
                            lblError.Text = rspMsg
                        End If

                    Catch ex As Exception
                        retVal = False
                    Finally
                        'Cerrar data reader por Default
                        If Not (results Is Nothing) Then
                            results.Close()
                        End If
                        'Cerrar conexion por Default
                        If Not (connection Is Nothing) Then
                            connection.Close()
                        End If
                    End Try

                Case "EditPrompts"
                    grdTerminalPrompts.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.StrAcquirerCode = grdTerminalPrompts.SelectedDataKey.Values.Item("PROMPT_CODE")
                    objSessionParams.StrCardRangeName = grdTerminalPrompts.SelectedDataKey.Values.Item("PROMPT_KEY_NAME")


                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("ListarTerminalPrompt.aspx", False)
            End Select

            If txtPrompt.Text = "" Then
                'Set Invisible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
            Else
                'Set Visible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
            End If

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub
    'Edicion de Parametros CardRange Stis 
    'Autor : Oscar Gutierrez
    Protected Sub grdTerminalPrompts_Updated(sender As Object, e As GridViewUpdatedEventArgs) Handles grdTerminalPrompts.RowUpdated

        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webEditParameterHostConfiguration_Stis"
            command.Parameters.Clear()


            command.Parameters.Add(New SqlParameter("PROMPT_CODE", e.OldValues("PROMPT_CODE")))
            command.Parameters.Add(New SqlParameter("PROMPT_KEY_NAME", e.OldValues("PROMPT_KEY_NAME")))
            command.Parameters.Add(New SqlParameter("PROMPT_DESCRIPTION", e.NewValues("PROMPT_DESCRIPTION")))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try
    End Sub

    Protected Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click

        If txtCode.Text <> "" Then
            dsTerminalPrompts.SelectParameters("PROMPT_CODE").DefaultValue = txtCode.Text
        Else
            dsTerminalPrompts.SelectParameters("PROMPT_CODE").DefaultValue = "-1"
        End If

        If txtPrompt.Text <> "" Then
            dsTerminalPrompts.SelectParameters("PROMPT_KEY_NAME").DefaultValue = txtPrompt.Text
        Else
            dsTerminalPrompts.SelectParameters("PROMPT_KEY_NAME").DefaultValue = "-1"
        End If
        grdTerminalPrompts.DataBind()

        If txtPrompt.Text = "" Then
            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
        Else
            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
        End If

    End Sub

    Private Sub clearForm()
        txtPrompt.Text = ""

    End Sub
End Class
