﻿ <%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Terminalstis.aspx.vb" Inherits="Groups_Terminalstis" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Terminales STIS
    </title>
    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>

    <li>Terminales</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    TERMINALES STIS: [ <%= objSessionParams.StrTerminalID %> ]
    <asp:HyperLink ID="HyperLink1" runat="server"
        NavigateUrl="~/Group/ListGroups.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <!-- START SIMPLE FORM -->
   <%-- <asp:TreeView ID="TreeView1" runat="server" Height="141px" ImageSet="Inbox" Width="119px">
        <HoverNodeStyle Font-Underline="True" />
        <Nodes>
            <asp:TreeNode ShowCheckBox="False" Target="New" Text="Tables" ToolTip="Tables Stis" Value="Terminal">
                <asp:TreeNode Target="New" Text="Acquirer" ToolTip="Parametros Por Terminal" Value="Acquirer/Issuer/CardRange" NavigateUrl="TerminalAcquirer.aspx">
                    <asp:TreeNode NavigateUrl="TerminalIssuer.aspx" Target="New" Text="Issuer" ToolTip="Issuer Stis" Value="Issuer">
                        <asp:TreeNode NavigateUrl="TerminalCardRange.aspx" Target="New" Text="CardRange" ToolTip="CardRange Stis" Value="CardRange"></asp:TreeNode>
                    </asp:TreeNode>
                </asp:TreeNode>
            </asp:TreeNode>
            <asp:TreeNode Text="Emv Level 2 Application" ToolTip="Emv Level 2 Application stis" Value="Emv Level 2 Application" NavigateUrl="TerminalEmvLevel2.aspx">
                <asp:TreeNode NavigateUrl="TerminalEmvKey.aspx" Target="New" Text="Emv Level 2 Key" ToolTip="Emv Level 2 Key stis" Value="Emv Level 2 Key">
                    <asp:TreeNode NavigateUrl="TerminalExtraApplication.aspx" Text="Extra Application Parameter" ToolTip="Extra Application Parameter Stis" Value="Extra Application Parameter"></asp:TreeNode>
                </asp:TreeNode>
            </asp:TreeNode>
        </Nodes>
        <NodeStyle Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" HorizontalPadding="5px" NodeSpacing="0px" VerticalPadding="0px" />
        <ParentNodeStyle Font-Bold="False" />
        <SelectedNodeStyle Font-Underline="True" HorizontalPadding="0px" VerticalPadding="0px" />
    </asp:TreeView>--%>
    <asp:XmlDataSource ID="XmlDataSource2" runat="server"></asp:XmlDataSource>
    <asp:SiteMapDataSource ID="SiteMapDataSource1" runat="server" />
    <asp:XmlDataSource ID="XmlDataSource1" runat="server"></asp:XmlDataSource>
    <br />

   
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlError" runat="server" Visible="False">
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <div class="toggle-message" style="z-index: 590; top: 0px; left: 0px;">
            <h3 class="title">Filtro de b&uacute;squeda...
                <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
            <div class="hide-message" id="hide-message" style="display: none;">
                <div class="st-form-line">
                    <span class="st-labeltext"><b>Record No: </b></span>
                    <asp:TextBox ID="txtRecord" CssClass="st- "
                        runat="server" TabIndex="1" MaxLength="50"
                        ToolTip="Record Numero" onkeydown="return (event.keyCode!=13);" Width="250px"></asp:TextBox>
                </div>
                <div class="st-form-line">
                    <span class="st-labeltext"><b>TID: </b></span>
                    <asp:TextBox ID="txtTid" CssClass="st-forminput"
                        runat="server" TabIndex="2" MaxLength="50"
                        ToolTip="TID de la Terminal" onkeydown="return (event.keyCode!=13);" Width="250px"></asp:TextBox>
                </div>
                <div class="st-form-line">
                    <span class="st-labeltext"><b>Serial: </b></span>
                    <asp:TextBox ID="txtSerial" CssClass="st- "
                        runat="server" TabIndex="1" MaxLength="50"
                        ToolTip="Numero de serie" onkeydown="return (event.keyCode!=13);" Width="250px"></asp:TextBox>
                </div>
                <div class="button-box">
                    <asp:Button ID="btnConsultar" runat="server" Text="Filtrar"
                        CssClass="button-aqua" TabIndex="3" ToolTip="Filtrar Terminales Stis..." />
                </div>
            </div>
        </div>

    </div>
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
        <div class="titleh">
            <h3>Terminales creadas</h3>
           
        </div>
       
     <ul id="navst">
	<li class="current"><a href="TerminalStis.aspx">Polaris TMS</a></li>
	<li><a>Tables</a>
		<ul>
			<li><a href="TerminalAcquirer.aspx">Acquirer</a></li>
			<li><a href="TerminalIssuer.aspx">Issuer</a>	</li>
			<li><a href="TerminalCardRange.aspx">CardRange</a></li>
			<li><a href="TerminalEmvLevel2.aspx">Emv Level 2 Application</a></li>
			<li><a href="TerminalEmvKey.aspx">Emv Level 2 Key</a></li>
			<li><a href="TerminalExtraApplication.aspx">Extra Application Parameter</a></li>

		</ul>
	</li>
	<li><a>Terminal</a>
		<ul>
			<li><a href="AddTerminalStis.aspx">New Terminal</a></li>
            <li><a href="CopyTerminalStis.aspx">Copy Terminal</a></li>
			
		</ul>
	</li>
</ul>
            
        <div class="body">
            <div id="posStis" style="overflow: auto; width: auto; height: 660px;">
            <br />
            <asp:GridView ID="grdTerminals" runat="server" AllowPaging="True"
                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4"
                DataSourceID="dsTerminals"
                CssClass="mGridCenter"  DataKeyNames ="TERMINAL_REC_NO,TID"
                EmptyDataText="No existen Terminales creadas en el Grupo o con el filtro aplicado"
                HorizontalAlign="Center" ForeColor="#333333" GridLines="None">
                <RowStyle BackColor="#E3EAEB" />
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:CommandField ButtonType="Image"  HeaderImageUrl="~/img/icons/16x16/edit.png" ShowEditButton="True" UpdateImageUrl="~/img/icons/16x16/ok.png" CancelImageUrl="~/img/icons/16x16/cancel.png" editimageurl="~/img/icons/16x16/edit.png" />
                    <asp:BoundField DataField="TERMINAL_REC_NO" HeaderText="Record No."
                        SortExpression="TERMINAL_REC_NO" />
                    <asp:BoundField DataField="TID" HeaderText="TID"
                        SortExpression="TID" />
                    <asp:BoundField DataField="POS_SERIAL_NO" HeaderText="Serial Pos"
                        SortExpression="POS_SERIAL_NO" ReadOnly ="true" />
                    <asp:BoundField DataField="OWNER_NAME" HeaderText="Fecha propietario"
                        SortExpression="OWNER_NAME" ItemStyle-HorizontalAlign="Center" ReadOnly="true">
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="MERCHANT_NAME" HeaderText="Nombre comerciante"
                        SortExpression="MERCHANT_NAME" />
                    <asp:BoundField DataField="SHOP_NAME" HeaderText="Nombre de tienda"
                        SortExpression="SHOP_NAME" />
                    <asp:BoundField DataField="MODEL" HeaderText="Modelo"
                        SortExpression="MODEL" ItemStyle-HorizontalAlign="Center" ReadOnly ="true">
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="STATUS" HeaderText="Estado"
                        SortExpression="STATUS" ItemStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="CREATED_DT" HeaderText="Fecha de creacion"
                        SortExpression="CREATED_DT" ItemStyle-HorizontalAlign="Center" ReadOnly ="true">
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundField>
                    <asp:TemplateField ShowHeader="False" HeaderStyle-Width="90px">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False"
                                CommandName="EditTerminalstis" ImageUrl="~/img/icons/16x16/add_agreements.png" Text="Parametros"
                                ToolTip="Parametros Por Terminal" CommandArgument='<%# grdTerminals.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />
                          <%--   <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False"
                                CommandName="DeleteTerminal" ImageUrl="~/img/icons/16x16/delete.png" Text="Eliminar"
                                ToolTip="Eliminar Terminal" CommandArgument='<%# grdTerminals.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink"
                                OnClientClick="return ConfirmAction(this);" />  --%>                    
                            <%--<asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False"
                                CommandName="ACQUIRER" ImageUrl="~/img/icons/16x16/add_products.png" Text="Acquirer"
                                ToolTip="Adquiriente Por Terminal" CommandArgument='<%# grdTerminals.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />--%>
                        </ItemTemplate>
                        <HeaderStyle Width="85px"></HeaderStyle>

                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>

                </Columns>
                <FooterStyle BackColor="#1C5E55" ForeColor="White" Font-Bold="True" />
                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center"
                    CssClass="pgr" />
                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BorderColor="#666666" BorderStyle="Solid"
                    BorderWidth="1px" BackColor="#50A2A3" />
                <SortedAscendingCellStyle BackColor="#F8FAFA" />
                <SortedAscendingHeaderStyle BackColor="#246B61" />
                <SortedDescendingCellStyle BackColor="#D4DFE1" />
                <SortedDescendingHeaderStyle BackColor="#15524A" />
            </asp:GridView>
            <br />
                </div>
            <asp:SqlDataSource ID="dsTerminals" runat="server"
                ConnectionString="<%$ ConnectionStrings:TeleLoaderStisConnectionString %>"
                SelectCommand="sp_webConsultarTerminales_Stis" SelectCommandType="StoredProcedure" 
                UpdateCommand="sp_webEditTerminal_Stis" UpdateCommandType="StoredProcedure">
                <SelectParameters>
                    <asp:Parameter Name="TERMINALStis" Type="string" />
                    <asp:Parameter Name="TID" Type="String" DefaultValue="-1" />
                    <asp:Parameter Name="POS_SERIAL_NO" Type="String" DefaultValue="-1" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="TERMINAL_REC_NO" Type="String" />
                    <asp:Parameter Name="TID" Type="String" />
                    <asp:Parameter Name="MERCHANT_NAME" Type="String" />
                    <asp:Parameter Name="SHOP_NAME" Type="String" />
                    <asp:Parameter Name="STATUS" Type="String" />
                </UpdateParameters>
            </asp:SqlDataSource>
        </div>
    </div>
</asp:Content>


<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

    <script language="javascript">
        var globalcontrol = '';

        function ConfirmAction(control) {

            globalcontrol = control;

            $.msgAlert({
                type: "warning"
                , title: "Mensaje del Sistema"
                , text: "Realmente desea eliminar la terminal?"
                , callback: function () {
                    __doPostBack($(globalcontrol).attr('name'), '');
                }
            });

            return false;
        }
    </script>

</asp:Content>

