﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Applications
Imports System.Data.Common

Partial Class Groups_Terminalstis
    Inherits TeleLoader.Web.BasePage

    Dim applicationObj As ApplicationSTIS

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            txtRecord.Focus()

            dsTerminals.SelectParameters("TERMINALStis").DefaultValue = objSessionParams.intTERMINAL_REC_NO

            grdTerminals.DataBind()

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub grdTerminals_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdTerminals.RowCommand
        Try

            Select Case e.CommandName

                Case "DeleteTerminal"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    grdTerminals.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    dsTerminals.DeleteParameters("TERMINALStis").DefaultValue = grdTerminals.SelectedDataKey.Values.Item("TERMINALStis")

                    Dim strData As String = "Terminal ID: " & grdTerminals.SelectedDataKey.Values.Item("TERMINALStis")

                    dsTerminals.Delete()

                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Terminal eliminado. Datos[ " & strData & " ]", "")

                    grdTerminals.DataBind()

                    pnlError.Visible = False
                    pnlMsg.Visible = True
                    lblMsg.Text = "Terminal eliminada de Stis"

                    grdTerminals.SelectedIndex = -1



                Case "EditTerminalstis"
                    grdTerminals.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.StrTerminalRecord = grdTerminals.SelectedDataKey.Values.Item("TERMINAL_REC_NO")
                    objSessionParams.StrTerminalID = grdTerminals.SelectedDataKey.Values.Item("TID")
                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("EditTerminalstis.aspx", False)

                Case "ACQUIRER"
                    grdTerminals.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.StrTerminalRecord = grdTerminals.SelectedDataKey.Values.Item("TERMINAL_REC_NO")
                    objSessionParams.StrTerminalID = grdTerminals.SelectedDataKey.Values.Item("TID")

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("EditAcquirer.aspx", False)
            End Select

            If txtRecord.Text = "" And txtTid.Text = "" And txtSerial.Text = "" Then
                'Set Invisible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
            Else
                'Set Visible Toggle Panel
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
            End If

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub
    'Edicion de Terminales Stis 
    'Autor : Oscar Gutierrez
    Protected Sub grdTerminals_Updated(sender As Object, e As GridViewUpdatedEventArgs) Handles grdTerminals.RowUpdated

        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean
        Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConnString)
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webEditTerminal_Stis"
            command.Parameters.Clear()


            command.Parameters.Add(New SqlParameter("TERMINAL_REC_NO", e.OldValues("TERMINAL_REC_NO")))
            command.Parameters.Add(New SqlParameter("TID", e.OldValues("TID")))
            command.Parameters.Add(New SqlParameter("MERCHANT_NAME", e.NewValues("MERCHANT_NAME")))
            command.Parameters.Add(New SqlParameter("SHOP_NAME", e.NewValues("SHOP_NAME")))
            command.Parameters.Add(New SqlParameter("STATUS", e.NewValues("STATUS")))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try


    End Sub

    Protected Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click

        If txtRecord.Text <> "" Then
            dsTerminals.SelectParameters("TERMINALStis").DefaultValue = txtRecord.Text
        Else
            dsTerminals.SelectParameters("TERMINALStis").DefaultValue = "-1"
        End If


        If txtTid.Text <> "" Then
            dsTerminals.SelectParameters("TID").DefaultValue = txtTid.Text
        Else
            dsTerminals.SelectParameters("TID").DefaultValue = "-1"
        End If

        If txtSerial.Text <> "" Then
            If GetTid(txtSerial.Text) Then
                'logs
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Load", "Tid Encontrado. Datos[ " & getFormDataLog() & " ]", "")
                'mostramos filtro
                dsTerminals.SelectParameters("TID").DefaultValue = objSessionParams.StrTerminalID
            Else
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Load", "Tid no encontrado. Datos[ " & getFormDataLog() & " ]", "")
                pnlError.Visible = True
                pnlMsg.Visible = False
                lblError.Text = "Tid no relacionado para el serial. " & txtSerial.Text
            End If

        Else
            'dsTerminals.SelectParameters("TID").DefaultValue = "-1"
        End If

        grdTerminals.DataBind()

        If txtRecord.Text = "" And txtTid.Text = "" And txtSerial.Text = "" Then
            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
        Else
            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
        End If

    End Sub

    Private Sub clearForm()
        txtRecord.Text = ""
        txtTid.Text = ""
        txtSerial.Text = ""
    End Sub

    'Protected Sub dsTerminals_Deleting(sender As Object, e As S qlDataSourceCommandEventArgs) Handles dsTerminals.Deleting

    '    'Remover parametro extra, igual al datakeyname
    '    If e.Command.Parameters.Count > 2 Then
    '        Dim paramAplId As DbParameter = e.Command.Parameters("TERMINALStis")
    '        e.Command.Parameters.Remove(paramAplId)
    '    End If

    'End Sub

    Public Function GetTid(ByVal Serial As String) As Boolean
        Dim connection As New SqlConnection(strConnectionString)
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As String = ""
        Dim retVal As Boolean

        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_validarTerminalId"
            command.Parameters.Clear()
            command.Parameters.Add(New SqlParameter("TermianlSerial", Serial))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetString(0)
                    objSessionParams.StrTerminalRecord = results.GetString(1)
                    objSessionParams.StrTerminalID = results.GetString(2)

                End While
            Else
                retVal = False
            End If

            If status = "TID NO ASOCIADO" Then
                retVal = False
            ElseIf status = "SERIAL NO EXISTE" Then
                retVal = False
            ElseIf status = "TID CORRECTO" Then
                retVal = True
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try
        Return retVal
    End Function

End Class
