﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Index.aspx.vb" Inherits="Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%>:: Inicio
    </title>    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    PLATAFORMA WEB ADMINISTRATIVA
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">

    <!-- START Text Box -->
    <div class="grid960">
        <div class="simplebox">
	        <div class="titleh"><h3><%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%></h3></div>
            <div class="body">

                <div style="text-align: center;">
                    <img src="img/teleloader.png" alt="Polaris" style="padding: 0px 0 0px 0; opacity: 0.85; border-radius: 25px;" />
                </div>
                <br />
    	        <div style="text-align: center;">
                    Esta plataforma le permite realizar una administraci&oacute;n efectiva y en tiempo real de todo su parque de puntos de venta dispuestos en producci&oacute;n, permiti&eacute;ndole la actualizaci&oacute;n remota de aplicaciones de una forma r&aacute;pida y eficiente.
    	        </div>
                <br />
            </div>
        </div>
    <div class="clear"></div>   
    </div>
    <!-- END Text Box -->

    <!-- START Text Box -->
    <div class="grid450-left">
        <div class="simplebox">
	        <div class="titleh"><h3>Datos de conexión:</h3></div>
            <div class="body">
                <ul class="statistics">
                    <li>IP de conexión:	<p>	<span class="blue"><asp:Label ID="lblIP" runat="server" Text=""></asp:Label></span></p></li>
                    <li>Fecha y hora de inicio de sesión:	<p>	<span class="blue"><asp:Label ID="lblLoginInitDate" runat="server" Text=""></asp:Label></span></p></li>
                </ul>
                <br />
            </div>
        </div>
    </div>
         
    <!-- END Text Box -->

    <!-- START Text Box -->
    <div class="grid450-right">
        <div class="simplebox">
	        <div class="titleh"><h3>Datos de la cuenta:</h3></div>
            <div class="body">
                <ul class="statistics">
                    <li>Nombre usuario:	<p><span class="blue"><asp:Label ID="lblUserName" runat="server" Text=""></asp:Label></span></p></li>
                    <li>Perfil:	<p><span class="blue"><asp:Label ID="lblProfile" runat="server" Text=""></asp:Label></span></p></li>
                    <li>Cliente: <p><span class="blue"><asp:Label ID="lblCustomer" runat="server" Text=""></asp:Label></span></p></li>
                    <li>Fecha y hora de expiración de clave:<p> <span class="blue"><asp:Label ID="lblExpDatetime" runat="server" Text=""></asp:Label></span></p></li>
                </ul>    	        
    	        
    	        <br />
    	        <div align="center">
    	            
                    <asp:Button ID="btnEditUser" runat="server" Text="Editar cuenta" 
                        CssClass="button-aqua" TabIndex="1" />

                    <asp:Button ID="btnChangePassword" runat="server" Text="Cambiar clave" 
                        CssClass="button-aqua" TabIndex="2" />
     	            
                </div>
                <div class="clear"></div>
                <br />
            </div>
        </div>
    </div>
    <!-- END Text Box -->


    <div style="text-align: left;">
                    <img src="img/logowposs.png" alt="Polaris" style="padding: 0px 0 0px 0; opacity: 0.85; border-radius: 25px;" />
               

           <div class="clear"></div>
              <asp:Button ID="btnContactanos" runat="server" Text="Contactanos" 
                        CssClass="button-aqua" TabIndex="3" />
             <asp:Button ID="BtnSoporte" runat="server" Text="Manual de usuario" 
                        CssClass="button-aqua" TabIndex="4" />   
     
    </div>
</asp:Content>

