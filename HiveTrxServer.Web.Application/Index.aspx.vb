﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Index
    Inherits TeleLoader.Web.BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            showSessionData()
        End If

    End Sub

    Private Sub showSessionData()

        If Not objAccessToken Is Nothing Then
            lblIP.Text = objAccessToken.IPAddress
            lblLoginInitDate.Text = objAccessToken.DateLogin.ToString()
            lblUserName.Text = objAccessToken.DisplayName
            lblProfile.Text = objAccessToken.Profile
            lblCustomer.Text = objAccessToken.Customer
            lblExpDatetime.Text = objAccessToken.DatePasswordExp
        End If

    End Sub

    Protected Sub btnEditUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditUser.Click
        Response.Redirect("Account/EditAccount.aspx", False)
    End Sub

    Protected Sub btnChangePassword_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChangePassword.Click
        Response.Redirect("Account/ChangePassword.aspx", False)
    End Sub
    Protected Sub btnContactanos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnContactanos.Click
        Response.Redirect("http://www.wposs.com/web/", False)
    End Sub
    Protected Sub btnSoporte_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSoporte.Click
        Response.Redirect("http://186.154.93.81:89/", False)
    End Sub



End Class
