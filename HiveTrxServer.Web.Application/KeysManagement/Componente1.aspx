﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Componente1.aspx.vb" Inherits="Group_KeysDevices" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%>:: Agregar Llaves
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>
    <li>Agregar Componente 1</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server" >
   AGREGAR LLAVES
   
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
      <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">
            <div class="alert alert-success">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">x</a>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlError" runat="server" Visible="False">
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>
        <style>
            .bg-1 {
                background-color: #1abc9c;
                color: #ffffff;
            }
        </style>
        <div class="bg-1">
            <div class="container text-center" style="background-color:white">
                <h3 style="color:dimgray">Cargar Componente 1</h3>
                <img src="../img/digital-key.png" style="background-color:white" class="img-circle" alt="Bird" width="350" height="320">
            </div>
        </div>
        
        <br>

        <div class="body">
            <div class="clear"></div>

            <div class="st-form-line" id="component-segment">
                <span class="st-labeltext">Componente 1: </span>
                <br>
                <asp:TextBox ID="txtComponent1_1" CssClass="hg-yellow" Style="width: 80px; font-size: 23px !important; text-align:center;"
                    runat="server" TabIndex="1" MaxLength="4" onkeypress="return isHexKey(event)" autocomplete="off" 
                    ToolTip="" onkeydown="return (event.keyCode!=13);" TextMode="singleline" Font-Size="Smaller"></asp:TextBox>
                &nbsp;-&nbsp;
                <asp:TextBox ID="txtComponent1_2" CssClass="hg-yellow" Style="width: 80px; font-size: 23px !important;text-align:center;"
                    runat="server" TabIndex="1" MaxLength="4" onkeypress="return isHexKey(event)" autocomplete="off"
                    ToolTip="" onkeydown="return (event.keyCode!=13);" TextMode="singleline"></asp:TextBox>
                &nbsp;-&nbsp;
                <asp:TextBox ID="txtComponent1_3" CssClass="hg-yellow" Style="width: 80px; font-size: 23px !important;text-align:center;"
                    runat="server" TabIndex="1" MaxLength="4" onkeypress="return isHexKey(event)" autocomplete="off"
                    ToolTip="" onkeydown="return (event.keyCode!=13);" TextMode="singleline"></asp:TextBox>
                &nbsp;-&nbsp;
                <asp:TextBox ID="txtComponent1_4" CssClass="hg-yellow" Style="width: 80px; font-size: 23px !important;text-align:center;"
                    runat="server" TabIndex="1" MaxLength="4" onkeypress="return isHexKey(event)" autocomplete="off"
                    ToolTip="" onkeydown="return (event.keyCode!=13);" TextMode="singleline"></asp:TextBox>
                &nbsp;-&nbsp;
                <asp:TextBox ID="txtComponent1_5" CssClass="hg-yellow" Style="width: 80px; font-size: 23px !important;text-align:center;"
                    runat="server" TabIndex="1" MaxLength="4" onkeypress="return isHexKey(event)" autocomplete="off"
                    ToolTip="" onkeydown="return (event.keyCode!=13);" TextMode="singleline"></asp:TextBox>
                &nbsp;-&nbsp;
                <asp:TextBox ID="txtComponent1_6" CssClass="hg-yellow" Style="width: 80px; font-size: 23px !important;text-align:center;"
                    runat="server" TabIndex="1" MaxLength="4" onkeypress="return isHexKey(event)" autocomplete="off"
                    ToolTip="" onkeydown="return (event.keyCode!=13);" TextMode="singleline"></asp:TextBox>
                &nbsp;-&nbsp;
                <asp:TextBox ID="txtComponent1_7" CssClass="hg-yellow" Style="width: 80px; font-size: 23px !important;text-align:center;"
                    runat="server" TabIndex="1" MaxLength="4" onkeypress="return isHexKey(event)" autocomplete="off"
                    ToolTip="" onkeydown="return (event.keyCode!=13);" TextMode="singleline"></asp:TextBox>
                &nbsp;-&nbsp;
                <asp:TextBox ID="txtComponent1_8" CssClass="hg-yellow" Style="width: 80px; font-size: 23px !important;text-align:center;"
                    runat="server" TabIndex="1" MaxLength="4" onkeypress="return isHexKey(event)" autocomplete="off"
                    ToolTip="" onkeydown="return (event.keyCode!=13);" TextMode="singleline"></asp:TextBox>

                    
                    <asp:ImageButton ID="btnVer" runat="server" CausesValidation="False"
                            CommandName="EditTerminal" ImageUrl="~/img/icons/16x16/verCampos.png" Text="Editar"
                            ToolTip="Ver campos" Style="padding: 3px 0px 0px 1px !important;" CssClass="imgLink" />
                    <asp:ImageButton ID="btnOcultar" runat="server" CausesValidation="False" 
                            CommandName="EditTerminal" ImageUrl="~/img/icons/16x16/restriction.png" Text="Editar"
                            ToolTip="Ver campos" Style="padding: 3px 0px 0px 1px !important;" CssClass="imgLink" />
                <br><br>
                
                <div class="clear"></div>
            </div>

            <asp:Panel ID="pnlKCVCombined" runat="server" Visible="False">

                <br />
                <br />

                <div class="st-form-line" style="border-bottom: 0px solid #E5E5E5 !important;">
                    <span class="st-labeltext">Dígito de Verificación Llave Combinada: </span>
                    <asp:TextBox ID="txtKCVCombined" CssClass="hg-green" Style="width: 125px; font-size: 26px !important;"
                        runat="server" TabIndex="12" MaxLength="4"
                        ToolTip="Dígito de verificación Llave Combinada" onkeydown="return (event.keyCode!=13);" Enabled="False"></asp:TextBox>
                    <strong class="hg-green">* Por favor valide este dígito de verificación de los 2 componentes contra el formato impreso.</strong>
                </div>
                <br />
                <br />
            </asp:Panel>

            <div class="button-box">
                <asp:Button ID="btnStep1" runat="server" Text="Cargar Componente 1"
                    CssClass="centrarguardar" TabIndex="9" OnClientClick="return validateComponent1()" />

            </div>
        </div>
    </div>

    <ajaxtoolkit:ToolkitScriptManager ID="scrMgrAsp" runat="server">
    </ajaxtoolkit:ToolkitScriptManager>

    <asp:Panel ID="pnlConfirmComponent" runat="server" CssClass="modalPopup" Style="width: 350px !important; visibility: hidden !important;">
        <asp:Label ID="lblTitulo" runat="server"
            Text="Confirmar?" Font-Bold="True"
            Font-Size="Medium"></asp:Label>
        <br />
        <br />
        <h2>Dígito de Verificación:</h2>

        <asp:TextBox ID="txtKCV" CssClass="hg-blue" Style="width: 125px; font-size: 26px !important;"
            runat="server" TabIndex="10" MaxLength="4"
            ToolTip="Dígito de verificación del componente 1" onkeydown="return (event.keyCode!=13);" Enabled="False" Text="34FDBC"></asp:TextBox>

        <br />
        <br />
        <br />
        <br />
        <asp:Button ID="btnStep2" runat="server" Text="Aceptar" CssClass="btn btn-info" />
        <br />

        <asp:LinkButton ID="lnkCancel" runat="server" Font-Bold="True" OnClientClick="SetZIndexUp();"
            Font-Size="Small">Cancelar</asp:LinkButton>
    </asp:Panel>

    <asp:Button ID="btnPopUp" runat="server" Style="display: none" Text="Invisible Button" />
    
    <ajaxtoolkit:ModalPopupExtender ID="modalPopupExt" BehaviorID="mpe" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="lnkCancel" TargetControlID="btnPopUp" PopupControlID="pnlConfirmComponent">
    </ajaxtoolkit:ModalPopupExtender>

    <asp:HyperLink ID="lnkBack" runat="server" NavigateUrl="~/KeysManagement/Manager.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>
     
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="jsOutput" Runat="Server">
     <!-- Validator -->
    <script src="../js/ValidatorGroupAndroidTerminalComponent1.js" type="text/javascript"></script>
     <%--<script src="../js/ValidatorGroup.js" type="text/javascript"></script>--%>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
</asp:Content>

