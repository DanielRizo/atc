﻿Imports System.IO
Imports System.Security.Cryptography
Imports TeleLoader.Terminals
Imports TeleLoader.Security
Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users
Imports System.Net
Imports TeleLoader.RSA
Imports System.Text
Imports System.Security.Cryptography.X509Certificates

Partial Class Group_KeysDevices
    Inherits TeleLoader.Web.BasePage
    Dim terminalObj As Terminal
    Public PrivateKey As String = ""
    Public PublicKey As String = ""
    Public BaseDecryptor As String
    Public BaseEncryptor As String
    Public ErrorMesagge As String
    Public Property DWKEYSIZE As Integer = 1024

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            terminalObj = New Terminal(strConnectionString, objSessionParams.intSelectedGroup)

            'Leer datos BD
            terminalObj.TerminalID = objSessionParams.intSelectedTerminalID
            terminalObj.GetKeyAndComponentsInformation()
            txtComponent1_1.Focus()
        End If
    End Sub

    <Obsolete>
    Protected Sub btnStep1_Click(sender As Object, e As EventArgs) Handles btnStep1.Click

        pnlKCVCombined.Visible = False



        ''Validate Component Data - Server Side
        If validateComponentfields() Then

            Try
                'Calculate KCV
                Dim resultArray() As Byte
                Dim componente As String = txtComponent1_1.Text + txtComponent1_2.Text + txtComponent1_3.Text + txtComponent1_4.Text + txtComponent1_5.Text + txtComponent1_6.Text + txtComponent1_7.Text + txtComponent1_8.Text
                Dim objEncryptor As New Encryptor(Encryptor.hexStringToByteArray(txtComponent1_1.Text & txtComponent1_2.Text & txtComponent1_3.Text & txtComponent1_4.Text & txtComponent1_5.Text & txtComponent1_6.Text & txtComponent1_7.Text & txtComponent1_8.Text))
                If componente = "00000000000000000000000000000000" Then
                    resultArray = {0, 0, 0, 0, 0, 0}
                Else
                    resultArray = objEncryptor.calculateKCV(3)
                End If
                If resultArray.Length > 0 Then
                    txtKCV.Text = Strings.Right("00" & Hex(resultArray(0)), 2) + Strings.Right("00" & Hex(resultArray(1)), 2) + Strings.Right("00" & Hex(resultArray(2)), 2)

                    ltrScript.Text = "<script language='javascript'>$(function(){ SetZIndexDown(); $('#ctl00_MainContent_pnlConfirmComponent').hide(); setTimeout(""$('#ctl00_MainContent_pnlConfirmComponent').css('visibility', 'visible'); $find('mpe').show()"", 200); });</script>"
                    pnlConfirmComponent.Visible = True

                Else
                    pnlError.Visible = True
                    pnlMsg.Visible = False
                    lblError.Text = "Error en los campos del componente, por favor verifíquelos."
                End If
            Catch ex As Exception
                pnlError.Visible = True
                pnlMsg.Visible = False
                lblError.Text = "Error en los campos del componente, por favor verifíquelos."
            End Try

        Else
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error en los campos del componente, por favor verifíquelos."
        End If

    End Sub
    Private Sub CreateNewKeys()

        Dim Keys As Keypair = Keypair.CreateNewKeys
        PrivateKey = Keys.Privatekey
        PublicKey = Keys.Publickey
    End Sub

    Private Function validateComponentfields() As Boolean
        Dim retVal = False

        'Validate Length And Content
        If txtComponent1_1.Text.Trim.Length = 4 And txtComponent1_2.Text.Trim.Length = 4 And txtComponent1_3.Text.Trim.Length = 4 And
            txtComponent1_4.Text.Trim.Length = 4 And txtComponent1_5.Text.Trim.Length = 4 And txtComponent1_6.Text.Trim.Length = 4 And
            txtComponent1_7.Text.Trim.Length = 4 And txtComponent1_8.Text.Trim.Length = 4 Then

            If Regex.IsMatch(txtComponent1_1.Text, "^[0-9A-F]+$") And Regex.IsMatch(txtComponent1_2.Text, "^[0-9A-F]+$") And Regex.IsMatch(txtComponent1_3.Text, "^[0-9A-F]+$") And
                Regex.IsMatch(txtComponent1_4.Text, "^[0-9A-F]+$") And Regex.IsMatch(txtComponent1_5.Text, "^[0-9A-F]+$") And Regex.IsMatch(txtComponent1_6.Text, "^[0-9A-F]+$") And
                Regex.IsMatch(txtComponent1_7.Text, "^[0-9A-F]+$") And Regex.IsMatch(txtComponent1_8.Text, "^[0-9A-F]+$") Then
                retVal = True
            End If

        End If

        Return retVal
    End Function


    <Obsolete>
    Protected Sub btnStep2_Click(sender As Object, e As EventArgs) Handles btnStep2.Click

        'Validate Component Data - Server Side
        If validateComponentfields() Then

            Try
                CreateNewKeys()
                'Validate Decryption Process
                Dim EncryptedMessage As RSAResult = RSA.Encrypt(txtComponent1_1.Text + txtComponent1_2.Text + txtComponent1_3.Text + txtComponent1_4.Text + txtComponent1_5.Text + txtComponent1_6.Text + txtComponent1_7.Text + txtComponent1_8.Text, PublicKey)
                Dim DecryptedMessage As RSAResult = RSA.Decrypt(EncryptedMessage.AsBytes, PrivateKey)
                BaseEncryptor = EncryptedMessage.AsBase64String
                BaseDecryptor = DecryptedMessage.AsString
                ErrorMesagge = "OK"

                'validate encryption process
                If BaseEncryptor.Length > 0 Then

                    'save in database
                    terminalObj = New Terminal(strConnectionString, objSessionParams.intSelectedGroup)
                    terminalObj.ComponentType = 1 'first component
                    terminalObj.Component1 = BaseEncryptor

                    If terminalObj.InjectComponent() Then
                        pnlError.Visible = False
                        pnlMsg.Visible = True
                        lblMsg.Text = "componente cargando correctamente."

                        txtComponent1_1.Text = ""
                        txtComponent1_2.Text = ""
                        txtComponent1_3.Text = ""
                        txtComponent1_4.Text = ""
                        txtComponent1_5.Text = ""
                        txtComponent1_6.Text = ""
                        txtComponent1_7.Text = ""
                        txtComponent1_8.Text = ""
                        txtComponent1_1.Focus()

                    Else
                        pnlError.Visible = True
                        pnlMsg.Visible = False
                        lblError.Text = "error cargando componente en la base de datos."
                    End If
                Else
                    pnlError.Visible = True
                    pnlMsg.Visible = False
                    lblError.Text = "error en los campos del componente, por favor verifíquelos."
                End If
            Catch ex As Exception
                pnlError.Visible = True
                pnlMsg.Visible = False
                lblError.Text = "Error en los campos del componente, por favor verifíquelos."
            End Try
        Else
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error en los campos del componente, por favor verifíquelos."
        End If
        ltrScript.Text = "<script language='javascript'>$(function(){ SetZIndexUp(); });</script>"
    End Sub
End Class
