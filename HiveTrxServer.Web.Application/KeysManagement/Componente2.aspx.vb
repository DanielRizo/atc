﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users
Imports System.Net
Imports System.Globalization
Imports TeleLoader.Terminals
Imports System.Drawing
Imports TeleLoader.Security
Imports System.Security.Cryptography
Imports System.IO
Imports TeleLoader.RSA
Imports System.Text
Imports System.Security.Cryptography.X509Certificates
Partial Class KeysManagement_Componente2
    Inherits TeleLoader.Web.BasePage
    Dim terminalObj As Terminal
    Public PrivateKey As String = ""
    Public PublicKey As String = ""
    Public BaseDecryptor As String
    Public BaseEncryptor As String
    Public ErrorMesagge As String
    Public Property DWKEYSIZE As Integer = 1024

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            terminalObj = New Terminal(strConnectionString, objSessionParams.intSelectedGroup)

            'Leer datos BD
            terminalObj.TerminalID = objSessionParams.intSelectedTerminalID
            terminalObj.GetKeyAndComponentsInformation()
            txtComponent2_1.Focus()

        End If

    End Sub
    <Obsolete>
    Protected Sub btnStep1_Click(sender As Object, e As EventArgs) Handles btnStep1.Click

        pnlKCVCombined.Visible = False

        If validateComponentfields() Then

            Try
                'Calculate KCV
                Dim componente As String = txtComponent2_1.Text + txtComponent2_2.Text + txtComponent2_3.Text + txtComponent2_4.Text + txtComponent2_5.Text + txtComponent2_6.Text + txtComponent2_7.Text + txtComponent2_8.Text
                Dim objEncryptor As New Encryptor(Encryptor.hexStringToByteArray(txtComponent2_1.Text + txtComponent2_2.Text + txtComponent2_3.Text + txtComponent2_4.Text + txtComponent2_5.Text + txtComponent2_6.Text + txtComponent2_7.Text + txtComponent2_8.Text))
                Dim resultArray() = objEncryptor.calculateKCV(3)

                If resultArray.Length > 0 Then
                    txtKCV.Text = Strings.Right("00" & Hex(resultArray(0)), 2) + Strings.Right("00" & Hex(resultArray(1)), 2) + Strings.Right("00" & Hex(resultArray(2)), 2)
                    ltrScript.Text = "<script language='javascript'>$(function(){ SetZIndexDown(); $('#ctl00_MainContent_pnlConfirmComponent').hide(); setTimeout(""$('#ctl00_MainContent_pnlConfirmComponent').css('visibility', 'visible'); $find('mpe').show()"", 200); });</script>"
                    pnlConfirmComponent.Visible = True
                Else
                    pnlError.Visible = True
                    pnlMsg.Visible = False
                    lblError.Text = "Error en los campos del componente, por favor verifíquelos."
                End If
            Catch ex As Exception
                pnlError.Visible = True
                pnlMsg.Visible = False
                lblError.Text = "Error en los campos del componente, por favor verifíquelos."
            End Try
        Else
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error en los campos del componente, por favor verifíquelos."
        End If

    End Sub
    Private Sub CreateNewKeys()

        Dim Keys As Keypair = Keypair.CreateNewKeys
        PrivateKey = Keys.Privatekey
        PublicKey = Keys.Publickey
    End Sub
    Private Function validateComponentfields() As Boolean
        Dim retVal = False

        'Validate Length And Content
        If txtComponent2_1.Text.Trim.Length = 4 And txtComponent2_2.Text.Trim.Length = 4 And txtComponent2_3.Text.Trim.Length = 4 And
            txtComponent2_4.Text.Trim.Length = 4 And txtComponent2_5.Text.Trim.Length = 4 And txtComponent2_6.Text.Trim.Length = 4 And
            txtComponent2_7.Text.Trim.Length = 4 And txtComponent2_8.Text.Trim.Length = 4 Then

            If Regex.IsMatch(txtComponent2_1.Text, "^[0-9A-F]+$") And Regex.IsMatch(txtComponent2_2.Text, "^[0-9A-F]+$") And Regex.IsMatch(txtComponent2_3.Text, "^[0-9A-F]+$") And
                Regex.IsMatch(txtComponent2_4.Text, "^[0-9A-F]+$") And Regex.IsMatch(txtComponent2_5.Text, "^[0-9A-F]+$") And Regex.IsMatch(txtComponent2_6.Text, "^[0-9A-F]+$") And
                Regex.IsMatch(txtComponent2_7.Text, "^[0-9A-F]+$") And Regex.IsMatch(txtComponent2_8.Text, "^[0-9A-F]+$") Then
                retVal = True
            End If

        End If

        Return retVal
    End Function

    <Obsolete>
    Protected Sub btnStep2_Click(sender As Object, e As EventArgs) Handles btnStep2.Click

        'Validate Component Data - Server Side
        If validateComponentfields() Then

            Try
                CreateNewKeys()
                Dim EncryptedMessage As RSAResult = RSA.Encrypt(txtComponent2_1.Text + txtComponent2_2.Text + txtComponent2_3.Text + txtComponent2_4.Text + txtComponent2_5.Text + txtComponent2_6.Text + txtComponent2_7.Text + txtComponent2_8.Text, PublicKey)
                Dim DecryptedMessage As RSAResult = RSA.Decrypt(EncryptedMessage.AsBytes, PrivateKey)
                BaseEncryptor = EncryptedMessage.AsBase64String
                BaseDecryptor = DecryptedMessage.AsString
                ErrorMesagge = "OK"

                'Validate Encryption Process
                If BaseEncryptor.Length > 0 Then

                    'Save in Database
                    terminalObj = New Terminal(strConnectionString, objSessionParams.intSelectedGroup)
                    terminalObj.ComponentType = 2 'Second Component
                    terminalObj.Component2 = BaseEncryptor

                    If terminalObj.InjectComponent() Then
                        pnlError.Visible = False
                        pnlMsg.Visible = True
                        lblMsg.Text = "Componente Cargado Correctamente."

                        txtComponent2_1.Text = ""
                        txtComponent2_2.Text = ""
                        txtComponent2_3.Text = ""
                        txtComponent2_4.Text = ""
                        txtComponent2_5.Text = ""
                        txtComponent2_6.Text = ""
                        txtComponent2_7.Text = ""
                        txtComponent2_8.Text = ""
                        txtComponent2_1.Focus()

                        'Validate If Component 1 was injected
                        terminalObj.GetKeyAndComponentsInformation()

                        If terminalObj.Component1 <> "-1" Then
                            'Set Flag Components Injected to True
                            'Show KCV for Combined Key

                            Dim KEY_1 = Convert.FromBase64String(terminalObj.Component1)
                            Dim KEY_2 = Convert.FromBase64String(terminalObj.Component2)

                            'Decryptor
                            Dim DecryptedKey1 As RSAResult = RSA.Decrypt(KEY_1, PrivateKey)
                            Dim DecryptedKey2 As RSAResult = RSA.Decrypt(KEY_2, PrivateKey)

                            'String
                            Dim LLave1 = DecryptedKey1.AsString
                            Dim Llave2 = DecryptedKey2.AsString
                            'BYTE
                            Dim ResultLLave1 = Encryptor.hexStringToByteArray(LLave1)
                            Dim ResultLLave2 = Encryptor.hexStringToByteArray(Llave2)

                            'Get Combined Key
                            Dim combinedKeyArray() = Encryptor.calculateCombinedKey(ResultLLave1, ResultLLave2)

                            Dim LlaveCombinada = Encryptor.byteArrayToHexString(combinedKeyArray)

                            Dim EncrypMk As RSAResult = RSA.Encrypt(LlaveCombinada, PublicKey)
                            Dim DecryptedMessage2 As RSAResult = RSA.Decrypt(EncrypMk.AsBytes, PrivateKey)
                            Dim BaseEncryptor2 = EncrypMk.AsBase64String
                            Dim BaseDecryptor2 = DecryptedMessage2.AsString

                            'Calculate KCV for Combined Key
                            Dim objCombinedEncryptor As New Encryptor(combinedKeyArray)
                            Dim resultCombinedArray() = objCombinedEncryptor.calculateKCV(3)

                            If resultCombinedArray.Length > 0 Then
                                'Components Injected in DB
                                terminalObj.SetFlagComponentsInjected(BaseEncryptor2)
                                txtKCVCombined.Text = Strings.Right("00" & Hex(resultCombinedArray(0)), 2) + Strings.Right("00" & Hex(resultCombinedArray(1)), 2) + Strings.Right("00" & Hex(resultCombinedArray(2)), 2)
                                pnlKCVCombined.Visible = True
                            Else
                                pnlError.Visible = True
                                lblError.Text = "Error Calculando KCV de la Llave Combinada."
                            End If
                        End If
                    Else
                        pnlError.Visible = True
                        pnlMsg.Visible = False
                        lblError.Text = "Error cargando componente en la Base de Datos."
                    End If
                Else
                    pnlError.Visible = True
                    pnlMsg.Visible = False
                    lblError.Text = "Error en los campos del componente, por favor verifíquelos."
                End If
            Catch ex As Exception
                pnlError.Visible = True
                pnlMsg.Visible = False
                lblError.Text = "Error en los campos del componente, por favor verifíquelos."
            End Try
        Else
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error en los campos del componente, por favor verifíquelos."
        End If
        ltrScript.Text = "<script language='javascript'>$(function(){ SetZIndexUp(); });</script>"
    End Sub

End Class
