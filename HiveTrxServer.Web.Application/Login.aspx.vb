﻿Imports TeleLoader.Security
Imports System
Imports System.Net
Imports TeleLoader.Sessions

Partial Class Login
    Inherits System.Web.UI.Page

    Private ReadOnly MAX_PASSWORD_ERRORS As Integer = 3
    Private ReadOnly YES As String = "YES"

    Public Shared Function GetIP4Address() As String
        Dim IP4Address As String = String.Empty

        For Each IPA As IPAddress In Dns.GetHostAddresses(HttpContext.Current.Request.UserHostAddress)
            If IPA.AddressFamily.ToString() = "InterNetwork" Then
                IP4Address = IPA.ToString()
                Exit For
            End If
        Next

        If IP4Address <> String.Empty Then
            Return IP4Address
        End If

        For Each IPA As IPAddress In Dns.GetHostAddresses(Dns.GetHostName())
            If IPA.AddressFamily.ToString() = "InterNetwork" Then
                IP4Address = IPA.ToString()
                Exit For
            End If
        Next

        Return IP4Address
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim strCode As String = Request.QueryString("exit")

            If strCode = "-1" Then
                lblError.Text = "Sesión Web Cerrada."
                pnlError.Visible = True
            Else
                lblError.Text = ""
                pnlError.Visible = False
            End If
        Else
            pnlError.Visible = False
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Try

            Dim IsCaptchaValid As Boolean = False

            If ConfigurationSettings.AppSettings.Get("UseRecaptcha") = "YES" Then
                Try
                    Dim EncodedResponse As String = Request.Form("g-Recaptcha-Response")
                    IsCaptchaValid = IIf(ReCaptcha.Validate(EncodedResponse) = "True", True, False)
                Catch ex As Exception
                    Session.Remove("AccessToken")
                    ltrScript.Text = "<script language='javascript' type='text/javascript'> $(function() { $.msgAlert({ type: 'error', title: 'Mensaje del sistema', text: 'Error Conectando al Servidor reCAPTCHA: [" & ex.Message.Replace("'", "").Replace("""", "") & "]' }); });</script>"
                    Exit Sub
                End Try
            Else
                IsCaptchaValid = True
            End If

            If IsCaptchaValid Then

                'Proceso Inicio de Sesión
                Dim objAccessToken As New AccessToken()
                Dim objSessionParams As New SessionParameters

                Dim strIPClient = GetIP4Address()
                objAccessToken.validateLogin(txtUser.Text.Trim(), txtPassword.Text.Trim(), strIPClient, Session.SessionID)

                If objAccessToken.LoginError = 3 Then
                    'Error de Password, Mostrar Intentos Restantes
                    Session.Remove("AccessToken")
                    ltrScript.Text = "<script language='javascript' type='text/javascript'> $(function() { $.msgAlert({ type: 'error', title: 'Mensaje del sistema', text: 'Usuario o Clave Erróneos. Por favor verifiquelos.' }); });</script>"

                    Dim maxAttemps As Integer = objAccessToken.getWrongPasswordCounter()
                    Dim extraStr As String = ""

                    If maxAttemps >= MAX_PASSWORD_ERRORS Then
                        extraStr = " Usuario Bloqueado."
                        maxAttemps = MAX_PASSWORD_ERRORS
                    End If

                    lblError.Text = "Intentos de Login " & maxAttemps & " de " & MAX_PASSWORD_ERRORS & "." & extraStr
                    pnlError.Visible = True
                Else

                    If objAccessToken.LoginError = 8 Then
                        Session.Remove("AccessToken")
                        ltrScript.Text = "<script language='javascript' type='text/javascript'> $(function() { $.msgAlert({ type: 'error', title: 'Mensaje del sistema', text: 'Licencia de Uso Expirada, contacte al Administrador del Sistema.' }); });</script>"
                    ElseIf objAccessToken.LoginError = 7 Then
                        Session.Remove("AccessToken")
                        ltrScript.Text = "<script language='javascript' type='text/javascript'> $(function() { $.msgAlert({ type: 'error', title: 'Mensaje del sistema', text: 'El usuario no cuenta con una licencia activa, contacte al Administrador del Sistema.' }); });</script>"
                    ElseIf objAccessToken.LoginError = 6 Then
                        Session.Remove("AccessToken")
                        ltrScript.Text = "<script language='javascript' type='text/javascript'> $(function() { $.msgAlert({ type: 'error', title: 'Mensaje del sistema', text: 'Usuario Bloqueado.' }); });</script>"
                    ElseIf objAccessToken.LoginError = 4 Then
                        Session.Remove("AccessToken")
                        ltrScript.Text = "<script language='javascript' type='text/javascript'> $(function() { $.msgAlert({ type: 'error', title: 'Mensaje del sistema', text: 'Este usuario presenta una sesión ya Iniciada.' }); });</script>"
                    ElseIf objAccessToken.LoginError = 99 Then
                        Session.Remove("AccessToken")
                        ltrScript.Text = "<script language='javascript' type='text/javascript'> $(function() { $.msgAlert({ type: 'error', title: 'Mensaje del sistema', text: 'Excepción Encontrada = [" & objAccessToken.ExceptionMsg.Replace("'", "").Replace("""", "") & "]' }); });</script>"
                    Else
                        If objAccessToken.Authenticated And objAccessToken.Enabled Then
                            'Marcar Session ID e Iniciar Aplicación
                            objAccessToken.SessionID = Session.SessionID
                            Session("AccessToken") = objAccessToken

                            objAccessToken.Validate("Session", "Start")

                            'Limpiar Datos Session
                            objSessionParams.strSelectedMenu = ""
                            objSessionParams.strSelectedOption = ""

                            'Sesión Activa
                            objAccessToken.Active = True

                            'Validar Fecha Expiración Clave
                            If objAccessToken.DatePasswordExp() < Now Then
                                objSessionParams.intPasswordErrorCode = 1
                                Session("SessionParameters") = objSessionParams
                                Response.Redirect("Account/ChangePassword.aspx")
                            Else
                                Session("SessionParameters") = objSessionParams
                                Response.Redirect("Index.aspx")
                            End If
                        Else
                            Session.Remove("AccessToken")
                            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(function() { $.msgAlert({ type: 'error', title: 'Mensaje del sistema', text: 'Usuario o Clave Erróneos. Por favor verifiquelos.' }); });</script>"
                        End If
                    End If
                End If

            Else
                Session.Remove("AccessToken")
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(function() { $.msgAlert({ type: 'error', title: 'Mensaje del sistema', text: 'Error de Autenticación reCAPTCHA.' }); });</script>"
            End If
        Catch ex As Exception
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(function() { $.msgAlert({ type: 'error', title: 'Mensaje del sistema', text: '" & ex.Message.Replace("'", "").Replace("""", "") & "' }); });</script>"
        End Try

    End Sub

End Class
