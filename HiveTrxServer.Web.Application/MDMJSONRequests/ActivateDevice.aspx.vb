﻿Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.IO
Imports TeleLoader.MDMRequestResponse
Imports TeleLoader.MDMTransaction
Imports System.Data.SqlClient
Imports System.Data

Partial Class MDMJSONRequests_ActivateDevice
    Inherits System.Web.UI.Page

    Private trxMDMObj As New TransactionMDM
    Private ReadOnly operationName As String = "id"
    Private ReadOnly YES As String = "YES"

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        Dim activateDeviceResponse As New ActivateDeviceResp
        Dim rawBody As String = Me.getRawBodyRequest()

        'Validar contenido del request body
        If rawBody <> "" Then
            Dim activateDeviceRequest As ActivateDeviceReq = JsonConvert.DeserializeObject(Of ActivateDeviceReq)(rawBody)
            Dim wsResponse As GetCNBData.CNBData

            'Validate Operation Name
            If activateDeviceRequest.operacion.ToLower = operationName Then

                If activateDeviceRequest.id_device <> "" And activateDeviceRequest.ip <> "" Then

                    'Retrieve information from Web Service
                    Try
                        If ConfigurationSettings.AppSettings.Get("UseRetrieveDataWS").ToUpper = YES Then
                            If CallGetCNBDataWS(activateDeviceRequest.ip, wsResponse) Then
                                'WS OK
                            End If
                        End If

                        'Validate Response WS Object
                        If (wsResponse Is Nothing) Then
                            'Set Default Values If Timeout or Connection Errors
                            wsResponse = New GetCNBData.CNBData()
                            wsResponse.id = ""
                            wsResponse.nombre = ""
                            wsResponse.ciudad = ""
                            wsResponse.region = ""
                            wsResponse.agencia = ""
                        End If

                    Catch ex As Exception
                        'Do Nothing
                    End Try

                    'Activate Terminal Or check If it is alreary Created
                    If activateOrCheckDeviceDB(activateDeviceRequest, activateDeviceResponse, wsResponse) Then
                        'Set Response OK
                        activateDeviceResponse.res = "MATCH"
                    Else
                        'Set Response Error
                        activateDeviceResponse.res = "WRONG"
                    End If

                    'Register Transaction in DB
                    trxMDMObj.operationName = activateDeviceRequest.operacion
                    trxMDMObj.login = activateDeviceRequest.login
                    trxMDMObj.pass = activateDeviceRequest.pass
                    trxMDMObj.terminalDeviceId = activateDeviceRequest.id_device
                    trxMDMObj.terminalIP = activateDeviceRequest.ip

                    Try
                        trxMDMObj.registerMDMTransaction()
                    Catch ex As Exception
                        'Do Nothing
                    End Try

                Else
                    'Set Response Error
                    activateDeviceResponse.res = "WRONG"
                End If

                'Update Transaction MDM
                trxMDMObj.FinalStatusTrx = activateDeviceResponse.res

                Try
                    trxMDMObj.updateStatusMDMTransaction()
                Catch ex As Exception
                    'Do Nothing
                End Try

            Else

                'Set Response Error
                activateDeviceResponse.res = "Operacion Inexistente"

            End If
        Else
            'Set Response Error
            activateDeviceResponse.res = "Request Mal formado"
        End If

        'Send Response Back to Client
        Response.Clear()
        Response.ContentType = "application/json; charset=utf-8"
        Response.Write(JsonConvert.SerializeObject(activateDeviceResponse))
        Response.End()

    End Sub

    Private Function CallGetCNBDataWS(ByVal deviceIP As String, ByRef wsResponse As GetCNBData.CNBData) As Boolean
        Dim clientWS As GetCNBData.GetCNBDataClient
        Dim TimeoutLoginWs As Integer = 15
        Dim retVal As Boolean = False

        Try
            clientWS = New GetCNBData.GetCNBDataClient("BasicHttpBinding_IGetCNBData")
            clientWS.InnerChannel.OperationTimeout = New TimeSpan(0, 0, TimeoutLoginWs)    'Timeout

            wsResponse = clientWS.GetCNBInfo(deviceIP)

            retVal = True

        Catch ex As Exception
            'Timeout or Connection errors
            retVal = False
        End Try

        Return retVal

    End Function

    Private Function getRawBodyRequest() As String

        Dim retBody As String = ""

        Try

            Dim memstream As MemoryStream = New MemoryStream()
            Request.InputStream.CopyTo(memstream)
            memstream.Position = 0

            Using reader As StreamReader = New StreamReader(memstream)
                retBody = reader.ReadToEnd()
            End Using

        Catch ex As Exception
            retBody = ""
        End Try

        Return retBody

    End Function

    ''' <summary>
    ''' Crear nuevo dispositivo en grupo por defecto o validar existencia del dispositivo en Base de Datos
    ''' </summary>
    Private Function activateOrCheckDeviceDB(ByVal activateDeviceRequest As ActivateDeviceReq, ByRef activateDeviceResponse As ActivateDeviceResp, ByVal wsResponse As GetCNBData.CNBData) As Boolean
        Dim configurationSection As ConnectionStringsSection = _
            System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim connection As New SqlConnection(configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString)
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean

        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webActivarTerminalAndroidMDM"
            command.Parameters.Clear()
            command.Parameters.Add(New SqlParameter("login", activateDeviceRequest.login))
            command.Parameters.Add(New SqlParameter("pass", activateDeviceRequest.pass))
            command.Parameters.Add(New SqlParameter("deviceId", activateDeviceRequest.id_device))
            command.Parameters.Add(New SqlParameter("deviceIP", activateDeviceRequest.ip))
            'Web Service Result Data
            command.Parameters.Add(New SqlParameter("heraclesID", wsResponse.id))
            command.Parameters.Add(New SqlParameter("heraclesName", wsResponse.nombre))
            command.Parameters.Add(New SqlParameter("heraclesCity", wsResponse.ciudad))
            command.Parameters.Add(New SqlParameter("heraclesRegion", wsResponse.region))
            command.Parameters.Add(New SqlParameter("heraclesAgency", wsResponse.agencia))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                    activateDeviceResponse.groupName = results.GetString(1)
                    activateDeviceResponse.idDevice = results.GetString(2)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try

        Return retVal

    End Function

End Class
