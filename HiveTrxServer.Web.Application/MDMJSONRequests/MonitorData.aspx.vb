﻿Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.IO
Imports TeleLoader.MDMRequestResponse
Imports TeleLoader.MDMTransaction
Imports System.Data.SqlClient
Imports System.Data

Partial Class MDMJSONRequests_MonitorData
    Inherits System.Web.UI.Page

    Private trxMDMObj As New TransactionMDM
    Private ReadOnly operationName As String = "datos"
    Private ReadOnly YES As String = "YES"

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        Dim monitorDataResponse As New MonitorDataResp
        Dim rawBody As String = Me.getRawBodyRequest()

        'Validar contenido del request body
        If rawBody <> "" Then
            Dim monitorDataRequest As MonitorDataReq = JsonConvert.DeserializeObject(Of MonitorDataReq)(rawBody)
            Dim wsResponse As GetCNBData.CNBData

            'Validate Operation Name
            If monitorDataRequest.operacion.ToLower = operationName Then

                'Register Transaction in DB
                trxMDMObj.operationName = monitorDataRequest.operacion
                trxMDMObj.login = monitorDataRequest.login
                trxMDMObj.pass = monitorDataRequest.pass
                trxMDMObj.terminalRam = monitorDataRequest.ram
                trxMDMObj.terminalBattery = monitorDataRequest.bateria
                trxMDMObj.terminalLatitude = monitorDataRequest.lat
                trxMDMObj.terminalLongitude = monitorDataRequest.lng
                trxMDMObj.installedAPKs = monitorDataRequest.apks
                trxMDMObj.groupName = monitorDataRequest.grupo
                trxMDMObj.terminalDeviceId = monitorDataRequest.id_device
                trxMDMObj.terminalIP = monitorDataRequest.ip

                Try
                    trxMDMObj.registerMDMTransaction()
                Catch ex As Exception
                    'Do Nothing
                End Try

                'Retrieve information from Web Service
                Try
                    If ConfigurationSettings.AppSettings.Get("UseRetrieveDataWS").ToUpper = YES Then

                        'Validate Device IP not empty
                        If monitorDataRequest.ip <> "" Then
                            If CallGetCNBDataWS(monitorDataRequest.ip, wsResponse) Then
                                'WS OK
                            End If
                        End If

                    End If

                    'Validate Response WS Object
                    If (wsResponse Is Nothing) Then
                        'Set Default Values If Timeout or Connection Errors
                        wsResponse = New GetCNBData.CNBData()
                        wsResponse.id = ""
                        wsResponse.nombre = ""
                        wsResponse.ciudad = ""
                        wsResponse.region = ""
                        wsResponse.agencia = ""
                    End If

                Catch ex As Exception
                    'Do Nothing
                End Try

                'Get DB Info x Group / Terminal
                If getDeviceGroupInfoDB(monitorDataRequest, monitorDataResponse, wsResponse) Then
                    'Set Response OK
                    monitorDataResponse.res = "MATCH"

                    'Set Response data
                    monitorDataResponse.user = monitorDataRequest.login
                    monitorDataResponse.pass = monitorDataRequest.pass

                    'Validate Base File if Exists
                    Try
                        'Configs XML
                        If monitorDataResponse.xml = ConfigurationSettings.AppSettings.Get("ConfigsFileBaseName") Then
                            Dim configBaseFilePath As String = Server.MapPath(ConfigurationSettings.AppSettings.Get("GroupsFolderPath")) & monitorDataResponse.groupName & "/" & ConfigurationSettings.AppSettings.Get("ConfigsFileBaseName")

                            If Not File.Exists(configBaseFilePath) Then
                                monitorDataResponse.xml = ""
                            End If
                        End If
                        If monitorDataResponse.xmlTerminal = ConfigurationSettings.AppSettings.Get("ConfigsFileBaseName") Then
                            Dim configBaseFilePath As String = Server.MapPath(ConfigurationSettings.AppSettings.Get("GroupsFolderPath")) & monitorDataResponse.idDevice & "/" & ConfigurationSettings.AppSettings.Get("ConfigsFileBaseName")

                            If Not File.Exists(configBaseFilePath) Then
                                monitorDataResponse.xmlTerminal = ""
                            End If
                        End If

                        'Images IMG
                        If monitorDataResponse.zip = ConfigurationSettings.AppSettings.Get("ImagesFileBaseName") Then
                            Dim imagesBaseFilePath As String = Server.MapPath(ConfigurationSettings.AppSettings.Get("GroupsFolderPath")) & monitorDataResponse.groupName & "/" & ConfigurationSettings.AppSettings.Get("ImagesFileBaseName")

                            If Not File.Exists(imagesBaseFilePath) Then
                                monitorDataResponse.zip = ""
                            End If
                        End If

                    Catch ex As Exception

                    End Try

                Else
                    'Set Response Error
                    monitorDataResponse.res = "WRONG"
                End If

                'Update Transaction MDM
                trxMDMObj.FinalStatusTrx = monitorDataResponse.res

                Try
                    trxMDMObj.updateStatusMDMTransaction()
                Catch ex As Exception
                    'Do Nothing
                End Try

            Else

                'Set Response Error
                monitorDataResponse.res = "Operacion Inexistente"

            End If
        Else
            'Set Response Error
            monitorDataResponse.res = "Request Mal formado"
        End If

        'Send Response Back to Client
        Response.Clear()
        Response.ContentType = "application/json; charset=utf-8"
        Response.Write(JsonConvert.SerializeObject(monitorDataResponse))
        Response.End()

    End Sub

    Private Function CallGetCNBDataWS(ByVal deviceIP As String, ByRef wsResponse As GetCNBData.CNBData) As Boolean
        Dim clientWS As GetCNBData.GetCNBDataClient
        Dim TimeoutLoginWs As Integer = 15
        Dim retVal As Boolean = False

        Try
            clientWS = New GetCNBData.GetCNBDataClient("BasicHttpBinding_IGetCNBData")
            clientWS.InnerChannel.OperationTimeout = New TimeSpan(0, 0, TimeoutLoginWs)    'Timeout

            wsResponse = clientWS.GetCNBInfo(deviceIP)

            retVal = True

        Catch ex As Exception
            'Timeout or Connection errors
            retVal = False
        End Try

        Return retVal

    End Function

    Private Function getRawBodyRequest() As String

        Dim retBody As String = ""

        Try

            Dim memstream As MemoryStream = New MemoryStream()
            Request.InputStream.CopyTo(memstream)
            memstream.Position = 0

            Using reader As StreamReader = New StreamReader(memstream)
                retBody = reader.ReadToEnd()
            End Using

        Catch ex As Exception
            retBody = ""
        End Try

        Return retBody

    End Function

    ''' <summary>
    ''' Obtener datos de la terminal / grupo de la Base de Datos
    ''' </summary>
    Private Function getDeviceGroupInfoDB(ByVal monitorDataRequest As MonitorDataReq, ByRef monitorDataResponse As MonitorDataResp, ByVal wsResponse As GetCNBData.CNBData) As Boolean
        Dim configurationSection As ConnectionStringsSection =
            System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim connection As New SqlConnection(configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString)
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean

        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webConsultarDatosTerminalAndroidMDM"
            command.Parameters.Clear()
            command.Parameters.Add(New SqlParameter("login", monitorDataRequest.login))
            command.Parameters.Add(New SqlParameter("pass", monitorDataRequest.pass))
            command.Parameters.Add(New SqlParameter("ram", monitorDataRequest.ram))
            command.Parameters.Add(New SqlParameter("battery", monitorDataRequest.bateria))
            command.Parameters.Add(New SqlParameter("latitude", monitorDataRequest.lat))
            command.Parameters.Add(New SqlParameter("longitude", monitorDataRequest.lng))
            command.Parameters.Add(New SqlParameter("installedApps", monitorDataRequest.apks))
            command.Parameters.Add(New SqlParameter("groupName", monitorDataRequest.grupo))
            command.Parameters.Add(New SqlParameter("deviceId", monitorDataRequest.id_device))
            command.Parameters.Add(New SqlParameter("deviceIP", monitorDataRequest.ip))
            'Web Service Result Data
            command.Parameters.Add(New SqlParameter("heraclesID", wsResponse.id))
            command.Parameters.Add(New SqlParameter("heraclesName", wsResponse.nombre))
            command.Parameters.Add(New SqlParameter("heraclesCity", wsResponse.ciudad))
            command.Parameters.Add(New SqlParameter("heraclesRegion", wsResponse.region))
            command.Parameters.Add(New SqlParameter("heraclesAgency", wsResponse.agencia))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                    monitorDataResponse.user = results.GetString(2)
                    monitorDataResponse.pass = results.GetString(3)
                    monitorDataResponse.mensaje = results.GetString(4)
                    monitorDataResponse.bloqueo = results.GetString(5)
                    monitorDataResponse.clave = results.GetString(6)
                    monitorDataResponse.idDevice = results.GetString(7)
                    monitorDataResponse.queries = results.GetString(8)
                    monitorDataResponse.frecuency = results.GetString(9)
                    monitorDataResponse.apks = results.GetString(10)
                    monitorDataResponse.xml = results.GetString(14)
                    'Modificacion Carga de aplicaciones por POS; OG: 14/AGOST/2018 
                    monitorDataResponse.xmlTerminal = results.GetString(15)
                    monitorDataResponse.zip = results.GetString(16)
                    monitorDataResponse.archivo = results.GetString(11)
                    monitorDataResponse.paquete = results.GetString(12)
                    monitorDataResponse.version = results.GetString(13)
                    monitorDataResponse.groupName = results.GetString(1)
                    'Modificación Carga de Llaves; EB: 01/Mar/2018
                    monitorDataResponse.recargar_llave = results.GetString(17)
                    'Modificación Definir aplicacion en modo kiosko; FM: 04/May/2018
                    monitorDataResponse.aplicacion_kiosko = results.GetString(18)

                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try

        Return retVal

    End Function

End Class
