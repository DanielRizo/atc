﻿Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.IO
Imports TeleLoader.MDMRequestResponse
Imports TeleLoader.MDMTransaction
Imports System.Data.SqlClient
Imports System.Data
Imports TeleLoader.Terminals
Imports TeleLoader.Security

Partial Class MDMJSONRequests_NotifyKeyLoaded
    Inherits System.Web.UI.Page

    Private trxMDMObj As New TransactionMDM
    Private ReadOnly operationName As String = "notificar_carga_llave"
    Private terminalObj As Terminal

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        Dim notifyKeyLoadedResponse As New NotifyKeyLoadedResp
        Dim rawBody As String = Me.getRawBodyRequest()
        Dim terminalId As Integer = -1
        Dim transactionId As Integer = -1

        'Validar contenido del request body
        If rawBody <> "" Then
            Dim notifyKeyLoadedRequest As NotifyKeyLoadedReq = JsonConvert.DeserializeObject(Of NotifyKeyLoadedReq)(rawBody)

            'Validate Operation Name
            If notifyKeyLoadedRequest.operacion.ToLower = operationName Then

                'Register Transaction in DB
                trxMDMObj.operationName = notifyKeyLoadedRequest.operacion
                trxMDMObj.terminalDeviceId = notifyKeyLoadedRequest.id_device
                trxMDMObj.terminalIP = notifyKeyLoadedRequest.ip

                Try
                    trxMDMObj.registerMDMTransaction()
                Catch ex As Exception
                    'Do Nothing
                End Try

                'Validate Components Data / Terminal
                If validateTerminalDataDB(notifyKeyLoadedRequest, terminalId) Then
                    'Set Response OK
                    notifyKeyLoadedResponse.res = "MATCH"

                    'Generate Combined Key And Send
                    Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

                    terminalObj = New Terminal(configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString)

                    'Set Terminal ID
                    terminalObj.TerminalID = terminalId

                    Try
                        'Get Encryption Key
                        terminalObj.GetEncriptionKey()

                        Dim objDecryptorKey As New Encryptor(Encryptor._encryptionkey, {0, 0, 0, 0, 0, 0, 0, 0}, Encryptor.hexStringToByteArray(terminalObj.EncryptionKey))
                        Dim decryptedKeyArray() = objDecryptorKey.doDecryption()

                        'Validate Decryption Process
                        If decryptedKeyArray.Length > 0 Then
                            'Get Security Info from DB
                            terminalObj.GetKeyAndComponentsInformation()

                            'Decrypt Components from DB
                            Dim objDecryptorComponent1 As New Encryptor(decryptedKeyArray, {0, 0, 0, 0, 0, 0, 0, 0}, Encryptor.hexStringToByteArray(terminalObj.Component1))
                            Dim decryptedComponent1Array() = objDecryptorComponent1.doDecryption()

                            Dim objDecryptorComponent2 As New Encryptor(decryptedKeyArray, {0, 0, 0, 0, 0, 0, 0, 0}, Encryptor.hexStringToByteArray(terminalObj.Component2))
                            Dim decryptedComponent2Array() = objDecryptorComponent2.doDecryption()

                            'Validate Decryption Process
                            If decryptedComponent1Array.Length > 0 And decryptedComponent2Array.Length > 0 Then
                                'Get Combined Key
                                Dim combinedKeyArray() = Encryptor.calculateCombinedKey(decryptedComponent1Array, decryptedComponent2Array)

                                'Calculate KCV for Combined Key
                                Dim objCombinedEncryptor As New Encryptor(combinedKeyArray)
                                Dim resultCombinedArray() = objCombinedEncryptor.calculateKCV(3)

                                If resultCombinedArray.Length > 0 Then
                                    Dim stringKCBCalculated As String = Strings.Right("00" & Hex(resultCombinedArray(0)), 2) + Strings.Right("00" & Hex(resultCombinedArray(1)), 2) + Strings.Right("00" & Hex(resultCombinedArray(2)), 2)

                                    'Validate KCV agains information send by terminal
                                    If stringKCBCalculated.Trim = notifyKeyLoadedRequest.kcv.Trim Then

                                        'Change Flag in DB - Key Loaded OK
                                        terminalObj.SetFlagKeyLoaded()

                                    Else
                                        'Set Response Error
                                        notifyKeyLoadedResponse.res = "Error Validación KCV"
                                    End If
                                Else
                                    'Set Response Error
                                    notifyKeyLoadedResponse.res = "Error Cálculo 3DES Server"
                                End If
                            Else
                                'Set Response Error
                                notifyKeyLoadedResponse.res = "Error Cálculo 3DES Server"
                            End If
                        Else
                            'Set Response Error
                            notifyKeyLoadedResponse.res = "Error Cálculo 3DES Server"
                        End If
                    Catch ex As Exception
                        'Set Response Error
                        notifyKeyLoadedResponse.res = "Error General"
                    End Try
                Else
                    'Set Response Error
                    notifyKeyLoadedResponse.res = "Componentes No Cargados"
                End If

                'Update Transaction MDM
                trxMDMObj.FinalStatusTrx = notifyKeyLoadedResponse.res

                Try
                    trxMDMObj.updateStatusMDMTransaction()
                Catch ex As Exception
                    'Do Nothing
                End Try

            Else
                'Set Response Error
                notifyKeyLoadedResponse.res = "Operacion Inexistente"
            End If
        Else
            'Set Response Error
            notifyKeyLoadedResponse.res = "Request Mal formado"
        End If

        'Send Response Back to Client
        Response.Clear()
        Response.ContentType = "application/json; charset=utf-8"
        Response.Write(JsonConvert.SerializeObject(notifyKeyLoadedResponse))
        Response.End()

    End Sub

    Private Function getRawBodyRequest() As String

        Dim retBody As String = ""

        Try

            Dim memstream As MemoryStream = New MemoryStream()
            Request.InputStream.CopyTo(memstream)
            memstream.Position = 0

            Using reader As StreamReader = New StreamReader(memstream)
                retBody = reader.ReadToEnd()
            End Using

        Catch ex As Exception
            retBody = ""
        End Try

        Return retBody

    End Function

    ''' <summary>
    ''' Valida la terminal que notifica la carga de llave
    ''' </summary>
    Private Function validateTerminalDataDB(ByVal notifyKeyLoadedRequest As NotifyKeyLoadedReq, ByRef terminalId As Integer) As Boolean
        Dim configurationSection As ConnectionStringsSection = _
            System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim connection As New SqlConnection(configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString)
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean

        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webValidarCargaLlaveTerminal"
            command.Parameters.Clear()
            command.Parameters.Add(New SqlParameter("deviceId", notifyKeyLoadedRequest.id_device))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                    terminalId = results.GetInt64(1)
                End While
            Else
                retVal = False
                terminalId = -1
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try

        Return retVal

    End Function

End Class
