﻿Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.IO
Imports TeleLoader.MDMRequestResponse
Imports TeleLoader.MDMTransaction
Imports System.Data.SqlClient
Imports System.Data
Imports TeleLoader.Terminals
Imports TeleLoader.Security

Partial Class MDMJSONRequests_RequestKey
    Inherits System.Web.UI.Page

    Private trxMDMObj As New TransactionMDM
    Private ReadOnly operationName As String = "solicitud_llave"
    Private terminalObj As Terminal

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        Dim requestKeyResponse As New RequestKeyResp
        Dim rawBody As String = Me.getRawBodyRequest()
        Dim terminalId As Integer = -1

        'Validar contenido del request body
        If rawBody <> "" Then
            Dim requestKeyRequest As RequestKeyReq = JsonConvert.DeserializeObject(Of RequestKeyReq)(rawBody)

            'Validate Operation Name
            If requestKeyRequest.operacion.ToLower = operationName Then

                'Register Transaction in DB
                trxMDMObj.operationName = requestKeyRequest.operacion
                trxMDMObj.terminalDeviceId = requestKeyRequest.id_device
                trxMDMObj.terminalIP = requestKeyRequest.ip
                trxMDMObj.key = requestKeyRequest.Key_device

                Try
                    trxMDMObj.registerMDMTransaction()
                Catch ex As Exception
                    'Do Nothing
                End Try

                'Validate Components Data / Terminal
                If validateTerminalDataDB(requestKeyRequest, terminalId) Then
                    'Set Response OK
                    requestKeyResponse.res = "MATCH"

                    'Generate Combined Key And Send
                    Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

                    terminalObj = New Terminal(configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString)

                    'Set Terminal ID
                    terminalObj.TerminalID = terminalId

                    Try
                        'Get Encryption Key

                        'terminalObj.EncryptionKey = trxMDMObj.key
                        terminalObj.GetEncriptionKey()

                        Dim objDecryptorKey As New Encryptor(Encryptor._encryptionkey, {0, 0, 0, 0, 0, 0, 0, 0}, Encryptor.hexStringToByteArray(terminalObj.EncryptionKey))
                        Dim decryptedKeyArray() = objDecryptorKey.doDecryption()

                        'Validate Decryption Process
                        If decryptedKeyArray.Length > 0 Then
                            'Get Security Info from DB
                            terminalObj.GetKeyAndComponentsInformation()

                            ' Components from DB
                            Dim objDecryptorComponent1 As New Encryptor(decryptedKeyArray, {0, 0, 0, 0, 0, 0, 0, 0}, Encryptor.hexStringToByteArray(terminalObj.Component1))
                            Dim decryptedComponent1Array() = objDecryptorComponent1.doDecryption()

                            Dim objDecryptorComponent2 As New Encryptor(decryptedKeyArray, {0, 0, 0, 0, 0, 0, 0, 0}, Encryptor.hexStringToByteArray(terminalObj.Component2))
                            Dim decryptedComponent2Array() = objDecryptorComponent2.doDecryption()

                            'Validate Decryption Process
                            If decryptedComponent1Array.Length > 0 And decryptedComponent2Array.Length > 0 Then
                                'Get Combined Key
                                Dim combinedKeyArray() = Encryptor.calculateCombinedKey(decryptedComponent1Array, decryptedComponent2Array)

                                'Encrypt Key
                                Dim objEncryptorKey As New Encryptor(decryptedKeyArray, {0, 0, 0, 0, 0, 0, 0, 0}, combinedKeyArray)
                                Dim encryptedKeyArray() = objEncryptorKey.doEncryption()

                                'Validate Encryption Process
                                If encryptedKeyArray.Length > 0 Then
                                    'Set response Key Encrypted
                                    requestKeyResponse.encrypted_key = Encryptor.byteArrayToHexString(encryptedKeyArray)
                                Else
                                    'Set Response Error
                                    requestKeyResponse.res = "Error Cálculo 3DES Server"
                                End If
                            Else
                                'Set Response Error
                                requestKeyResponse.res = "Error Cálculo 3DES Server"
                            End If
                        Else
                            'Set Response Error
                            requestKeyResponse.res = "Error Cálculo 3DES Server"
                        End If
                    Catch ex As Exception
                        'Set Response Error
                        requestKeyResponse.res = "Error General"
                    End Try
                Else
                    'Set Response Error
                    requestKeyResponse.res = "Componentes No Cargados"
                End If

                'Update Transaction MDM
                trxMDMObj.FinalStatusTrx = requestKeyResponse.res

                Try
                    trxMDMObj.updateStatusMDMTransaction()
                Catch ex As Exception
                    'Do Nothing
                End Try

            Else
                'Set Response Error
                requestKeyResponse.res = "Operacion Inexistente"
            End If
        Else
            'Set Response Error
            requestKeyResponse.res = "Request Mal formado"
        End If

        'Send Response Back to Client
        Response.Clear()
        Response.ContentType = "application/json; charset=utf-8"
        Response.Write(JsonConvert.SerializeObject(requestKeyResponse))
        Response.End()

    End Sub

    Private Function getRawBodyRequest() As String

        Dim retBody As String = ""

        Try

            Dim memstream As MemoryStream = New MemoryStream()
            Request.InputStream.CopyTo(memstream)
            memstream.Position = 0

            Using reader As StreamReader = New StreamReader(memstream)
                retBody = reader.ReadToEnd()
            End Using

        Catch ex As Exception
            retBody = ""
        End Try

        Return retBody

    End Function

    ''' <summary>
    ''' Valida la terminal que solicita la llave
    ''' </summary>
    Private Function validateTerminalDataDB(ByVal requestKeyRequest As RequestKeyReq, ByRef terminalId As Integer) As Boolean
        Dim configurationSection As ConnectionStringsSection =
            System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim connection As New SqlConnection(configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString)
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean

        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webValidarCargaLlaveTerminal"
            command.Parameters.Clear()
            command.Parameters.Add(New SqlParameter("deviceId", requestKeyRequest.id_device))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                    terminalId = results.GetInt64(1)
                End While
            Else
                retVal = False
                terminalId = -1
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try

        Return retVal

    End Function

End Class
