﻿Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.IO
Imports TeleLoader.MDMRequestResponse
Imports TeleLoader.MDMTransaction
Imports System.Data.SqlClient
Imports System.Data

Partial Class MDMJSONRequests_SetDownloadedFileInfo
    Inherits System.Web.UI.Page

    Private trxMDMObj As New TransactionMDM
    Private ReadOnly operationName As String = "descargas"

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        Dim dloadJsonResponse As New DownloadedFileInfoResp
        Dim rawBody As String = Me.getRawBodyRequest()

        'Validar contenido del request body
        If rawBody <> "" Then
            Dim dloadJsonRequest As DownloadedFileInfoReq = JsonConvert.DeserializeObject(Of DownloadedFileInfoReq)(rawBody)
            Dim groupName As String = ""

            'Validate Operation Name
            If dloadJsonRequest.operacion.ToLower = operationName Then

                'Register Transaction in DB
                trxMDMObj.operationName = dloadJsonRequest.operacion
                trxMDMObj.login = dloadJsonRequest.login
                trxMDMObj.pass = dloadJsonRequest.pass
                trxMDMObj.groupName = dloadJsonRequest.grupo
                trxMDMObj.terminalDeviceId = dloadJsonRequest.id_device
                trxMDMObj.terminalDownloadedAPK = dloadJsonRequest.apk
                trxMDMObj.terminalDownloadedXML = dloadJsonRequest.configs
                trxMDMObj.terminalDownloadedIMG = dloadJsonRequest.imagenes
                trxMDMObj.terminalIP = dloadJsonRequest.ip

                Try
                    trxMDMObj.registerMDMTransaction()
                Catch ex As Exception
                    'Do Nothing
                End Try

                'Update DB Register
                If setDownloadedFileInfoDB(dloadJsonRequest, groupName) Then
                    'Set Response OK
                    dloadJsonResponse.res = "UPDATED"
                    dloadJsonResponse.groupName = groupName

                Else
                    'Set Response Error
                    dloadJsonResponse.res = "WRONG"
                    dloadJsonResponse.groupName = ""
                End If

                'Update Transaction MDM
                trxMDMObj.FinalStatusTrx = dloadJsonResponse.res

                Try
                    trxMDMObj.updateStatusMDMTransaction()
                Catch ex As Exception
                    'Do Nothing
                End Try

            Else

                'Set Response Error
                dloadJsonResponse.res = "Operacion Inexistente"
                dloadJsonResponse.groupName = ""

            End If
        Else
            'Set Response Error
            dloadJsonResponse.res = "Request Mal formado"
            dloadJsonResponse.groupName = ""
        End If

        'Send Response Back to Client
        Response.Clear()
        Response.ContentType = "application/json; charset=utf-8"
        Response.Write(JsonConvert.SerializeObject(dloadJsonResponse))
        Response.End()

    End Sub

    Private Function getRawBodyRequest() As String

        Dim retBody As String = ""

        Try

            Dim memstream As MemoryStream = New MemoryStream()
            Request.InputStream.CopyTo(memstream)
            memstream.Position = 0

            Using reader As StreamReader = New StreamReader(memstream)
                retBody = reader.ReadToEnd()
            End Using

        Catch ex As Exception
            retBody = ""
        End Try

        Return retBody

    End Function

    ''' <summary>
    ''' Almacenar Información del Archivo Reportado
    ''' </summary>
    Private Function setDownloadedFileInfoDB(ByVal dloadJsonRequest As DownloadedFileInfoReq, ByRef groupName As String) As Boolean
        Dim configurationSection As ConnectionStringsSection = _
            System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim connection As New SqlConnection(configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString)
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As Integer
        Dim retVal As Boolean

        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webActualizarInfoDescargaTerminalAndroid"
            command.Parameters.Clear()
            command.Parameters.Add(New SqlParameter("deviceId", dloadJsonRequest.id_device))
            'Check for APK Tokens
            Dim apkTokens() As String = {"", "", ""}

            If dloadJsonRequest.apk <> "" Then
                apkTokens = dloadJsonRequest.apk.Split("#") 'Example: app-debug.apk#com.bancoguayaquil.cb#7
            End If
            command.Parameters.Add(New SqlParameter("filenameAPK", apkTokens(0)))
            command.Parameters.Add(New SqlParameter("packageNameAPK", apkTokens(1)))
            command.Parameters.Add(New SqlParameter("packageVersionAPK", apkTokens(2)))
            command.Parameters.Add(New SqlParameter("filenameXML", dloadJsonRequest.configs))
            command.Parameters.Add(New SqlParameter("filenameIMG", dloadJsonRequest.imagenes))

            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetInt32(0)
                    groupName = results.GetString(1)
                End While
            Else
                retVal = False
                groupName = ""
            End If

            If status = 1 Then
                retVal = True
            Else
                retVal = False
                groupName = ""
            End If

        Catch ex As Exception
            retVal = False
            groupName = ""
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try

        Return retVal

    End Function

End Class
