﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Script.Services
Imports System.Web.Services
Imports TeleLoader.Security
Imports TeleLoader.Sessions

Partial Class MasterPage
    Inherits System.Web.UI.MasterPage

    Private ReadOnly SEPARATOR As String = "|"
    Private ReadOnly BACKSLASH As String = "/"
    Private ReadOnly WARNING_PERIOD_FOR_CHANGE_PWD As Integer = 7


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        If Not IsPostBack Then

            Try
                Me.LblAnio.Text = Now.Year
                Dim objAccessToken As AccessToken = Session("AccessToken")
                Dim objSessionParameters As SessionParameters = Session("SessionParameters")

                If Not objAccessToken Is Nothing Then
                    If objAccessToken.Authenticated = True And objAccessToken.Enabled = True Then

                        If objSessionParameters.intPasswordErrorCode <> 1 Then
                            repMenu.DataSource = objAccessToken.GetMenu(objSessionParameters.strSelectedMenu)
                            repMenu.DataBind()

                            'Validate License Type - Free Trial
                            If objAccessToken.LicenseType = 1 Then
                                ltrLicense.Text = "<div id='stats' style='z-index: 840; display: block;'><div class='column' style='z-index: 800; width:100%; border-right: none !important;'>	<b class='down'>Free-Trial 30 Days</b>	Contacte al Administrador del Sistema para una Licencia PRO. Licencia actual válida por " & IIf(objAccessToken.LicenseExpiryDate > Now, IIf((objAccessToken.LicenseExpiryDate - Now).Days > 1, (objAccessToken.LicenseExpiryDate - Now).Days & " días", (objAccessToken.LicenseExpiryDate - Now).Days & " día"), 0) & ".</div></div>"
                            Else
                                ltrLicense.Text = ""
                            End If

                            Dim pageName = Page.ToString().Substring(4, Page.ToString().Substring(4).Length - 5)

                            'Only in Index.aspx Page
                            If pageName.ToLower = "index" Then
                                'Show Warning message to change password before WARNING_PERIOD_FOR_CHANGE_PWD Days
                                Dim PeriodInDays = DateDiff(DateInterval.Day, Now, objAccessToken.DatePasswordExp()) + 1

                                If PeriodInDays <= WARNING_PERIOD_FOR_CHANGE_PWD Then
                                    ltrLicense.Text = "<div id='stats' style='z-index: 840; display: block;'><div class='column' style='z-index: 800; width:1050px; border-right: none !important;'>	<b class='down'>Aviso de Cambio de Contraseña</b>	Su contraseña expira en " & IIf(PeriodInDays = 1, PeriodInDays & " día !", PeriodInDays & " días !") & "</div></div>"
                                Else
                                    If ltrLicense.Text.Length <= 0 Then
                                        ltrLicense.Text = ""
                                    End If
                                End If
                            End If

                        Else
                            'Estado Usuario, Sin Cambiar Clave
                            'Generar Exception
                            repMenu.DataSource = Nothing
                            repMenu.DataBind()

                            'Solo Permitir el Form. de cambio de clave
                            If getCurrentPage() <> "Account/ChangePassword.aspx" Then
                                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Load", "Access Fault Raised. You are trying to enter a page without permission.", "")
                                HandleErrorRedirect(New Exception("Access Fault Raised. You are trying to enter a page without permission."))
                                Exit Sub
                            End If
                        End If

                        'Valores Visibles
                        lblUserName.Text = objAccessToken.DisplayName
                        lblDateTime.Text = Now().ToString
                    End If
                Else
                    'Valores Visibles
                    lblUserName.Text = ""
                    lblDateTime.Text = Now().ToString
                    Server.ClearError()
                    Response.Clear()
                    Response.Redirect("Login.aspx", False)
                End If

                'Llamando al método true= https; false = http;
                Dim protocol As String = ConfigurationSettings.AppSettings.Get("Protocol").Trim().ToUpper()
                protocoloSeguro(protocol = "HTTPS")
            Catch ex As Exception
                HandleErrorRedirect(ex)
            End Try

            If Not Request.QueryString("?HelloWorld") Is Nothing Then
                Dim idcircuit As String

                idcircuit = CInt(Request.QueryString("HelloWorld"))
            End If
        End If
    End Sub

    ' Error handler estándar, redirecciona a página de error
    Protected Sub HandleErrorRedirect(ByVal exception As Exception)

        If Session("Exception") Is Nothing Then
            Session("Exception") = exception
        End If

        Server.ClearError()

        Response.Redirect("~/ErrorPage.aspx")

    End Sub

    Protected Sub lnkMenuItem_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)

        Dim objSessionParameters As SessionParameters = Session("SessionParameters")
        Dim i As Integer = 0
        Dim strAbsolutePath As String = "~/"
        Dim subFolders As Integer = 0

        'Iniciar Módulo de Menú Seleccionado
        objSessionParameters.strSelectedMenu = e.CommandArgument.ToString.Split(SEPARATOR)(1)

        Session("SessionParameters") = objSessionParameters

        Response.Redirect(strAbsolutePath & e.CommandArgument.ToString.Split(SEPARATOR)(0))

    End Sub

    Protected Sub lnkHome_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkHome.Click
        goToIndex()
    End Sub

    Protected Sub lnkLogo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLogo.Click
        goToIndex()
    End Sub

    Private Sub goToIndex()

        Dim objSessionParameters As SessionParameters = Session("SessionParameters")

        'Borrar Parámetros
        objSessionParameters.strSelectedMenu = "0"
        objSessionParameters.strSelectedOption = "0"

        'Ir a Inicio
        Session("SelectedMenu") = ""
        Session("SelectedOption") = ""

        Response.Redirect("~/Index.aspx")
    End Sub

    Private Function getCurrentPage() As String

        Dim strPageName As String = Request.AppRelativeCurrentExecutionFilePath
        strPageName = strPageName.Substring(strPageName.IndexOf("/") + 1)

        Return strPageName

    End Function

    'Metodo de redireccionamiento..
    Public Sub protocoloSeguro(ByVal bSeguro As Boolean)
        Dim redirectUrl As String = Nothing
        If bSeguro AndAlso (Not Request.IsSecureConnection) Then
            redirectUrl = Request.Url.ToString().Replace("http:", "https:")
        Else
            If (Not bSeguro) AndAlso Request.IsSecureConnection Then
                redirectUrl = Request.Url.ToString().Replace("https:", "http:")
            End If
        End If

        If redirectUrl IsNot Nothing Then
            Response.Redirect(redirectUrl)
        End If
    End Sub

End Class

