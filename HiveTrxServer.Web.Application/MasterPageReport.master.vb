﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Security
Imports TeleLoader.Sessions

Partial Class MasterPageReport
    Inherits System.Web.UI.MasterPage

    Private ReadOnly SEPARATOR As String = "|"
    Private ReadOnly BACKSLASH As String = "/"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            Try
                Me.LblAnio.Text = Now.Year
                Dim objAccessToken As AccessToken = Session("AccessToken")
                Dim objSessionParameters As SessionParameters = Session("SessionParameters")

                If Not objAccessToken Is Nothing Then
                    If objAccessToken.Authenticated = True And objAccessToken.Enabled = True Then

                        If objSessionParameters.intPasswordErrorCode <> 1 Then
                            repMenu.DataSource = objAccessToken.GetMenu(objSessionParameters.strSelectedMenu)
                            repMenu.DataBind()
                        Else
                            'Estado Usuario, Sin Cambiar Clave
                            'Generar Exception
                            repMenu.DataSource = Nothing
                            repMenu.DataBind()

                            'Solo Permitir el Form. de cambio de clave
                            If getCurrentPage() <> "Account/ChangePassword.aspx" Then
                                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Load", "Access Fault Raised. You are trying to enter a page without permission.", "")
                                HandleErrorRedirect(New Exception("Access Fault Raised. You are trying to enter a page without permission."))
                                Exit Sub
                            End If
                        End If

                        'Valores Visibles
                        lblUserName.Text = objAccessToken.DisplayName
                        lblDateTime.Text = Now().ToString
                    End If
                Else
                    'Valores Visibles
                    lblUserName.Text = ""
                    lblDateTime.Text = Now().ToString
                    Server.ClearError()
                    Response.Clear()
                    Response.Redirect("Login.aspx", False)
                End If

                'Llamando al método true= https; false = http;
                Dim protocol As String = ConfigurationSettings.AppSettings.Get("Protocol").Trim().ToUpper()
                protocoloSeguro(protocol = "HTTPS")

            Catch ex As Exception
                HandleErrorRedirect(ex)
            End Try

        End If

    End Sub

    ' Error handler estándar, redirecciona a página de error
    Protected Sub HandleErrorRedirect(ByVal exception As Exception)

        If Session("Exception") Is Nothing Then
            Session("Exception") = exception
        End If

        Server.ClearError()

        Response.Redirect("~/ErrorPage.aspx")

    End Sub

    Protected Sub lnkMenuItem_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)

        Dim objSessionParameters As SessionParameters = Session("SessionParameters")
        Dim i As Integer = 0
        Dim strReturn As String = ""
        Dim subFolders As Integer = 0

        'Iniciar Módulo de Menú Seleccionado
        objSessionParameters.strSelectedMenu = e.CommandArgument.ToString.Split(SEPARATOR)(1)

        Session("SessionParameters") = objSessionParameters

        subFolders = Request.CurrentExecutionFilePath.Split(BACKSLASH).Count - 1

        For i = 1 To subFolders - 1 '[- 1 Para Deploy WEB]  -  [- 2 Para Debug .Net]
            strReturn &= "../"
        Next

        Response.Redirect(strReturn & e.CommandArgument.ToString.Split(SEPARATOR)(0))

    End Sub

    Protected Sub lnkHome_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkHome.Click
        goToIndex()
    End Sub

    Protected Sub lnkLogo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLogo.Click
        goToIndex()
    End Sub

    Private Sub goToIndex()

        Dim objSessionParameters As SessionParameters = Session("SessionParameters")

        'Borrar Parámetros
        objSessionParameters.strSelectedMenu = "0"
        objSessionParameters.strSelectedOption = "0"

        'Ir a Inicio
        Session("SelectedMenu") = ""
        Session("SelectedOption") = ""

        Response.Redirect("~/Index.aspx")
    End Sub

    Private Function getCurrentPage() As String

        Dim strPageName As String = Request.AppRelativeCurrentExecutionFilePath
        strPageName = strPageName.Substring(strPageName.IndexOf("/") + 1)

        Return strPageName

    End Function

    'Metodo de redireccionamiento..
    Public Sub protocoloSeguro(ByVal bSeguro As Boolean)
        Dim redirectUrl As String = Nothing
        If bSeguro AndAlso (Not Request.IsSecureConnection) Then
            redirectUrl = Request.Url.ToString().Replace("http:", "https:")
        Else
            If (Not bSeguro) AndAlso Request.IsSecureConnection Then
                redirectUrl = Request.Url.ToString().Replace("https:", "http:")
            End If
        End If

        If redirectUrl IsNot Nothing Then
            Response.Redirect(redirectUrl)
        End If
    End Sub

End Class

