﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Security
Imports TeleLoader.Sessions

Partial Class MasterPageStis
    Inherits System.Web.UI.MasterPage

    Private ReadOnly SEPARATOR As String = "|"
    Private ReadOnly BACKSLASH As String = "/"
    Private ReadOnly WARNING_PERIOD_FOR_CHANGE_PWD As Integer = 7


    Private Function getArgumets() As String
        Dim eventArgument As String

        If (Request("__EVENTARGUMENT") Is Nothing) Then
            eventArgument = String.Empty
        Else
            eventArgument = Request("__EVENTARGUMENT")

        End If

        Return eventArgument

    End Function
    Private Function getAllParams() As String()
        Dim eventArgument As String
        Dim allParams() As String

        If (Request("__EVENTARGUMENT") Is Nothing) Then
            eventArgument = String.Empty
        Else
            eventArgument = Request("__EVENTARGUMENT")
            allParams = eventArgument.Split("|")
            Return allParams
        End If

        Return {}

    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsPostBack Then
            Dim eventTarget As String
            Dim allParams() As String

            eventTarget = Request("__EVENTTARGET")


            If eventTarget.Equals("refreshTree") Then
                reload()
            End If
            'addMethod methods
            If eventTarget.Equals("addMethod") Then

                'Dim valuePassed As String = eventArgument
                'addMethod(valuePassed)
            End If
            If eventTarget.Equals("addAcquirerTbl") Then

                Dim valuePassed As String = getArgumets()
                Dim valuePasseds As String = getArgumets()

                'addMethod(valuePassed)
            End If

            'Delete methods
            If eventTarget = "deleteCDR" Then
                allParams = getAllParams()
                deleteCardRage(acqAppKey:=allParams(0), iss:=allParams(1), cdr:=allParams(2))
            End If
            If eventTarget = "deleteISS" Then
                allParams = getAllParams()
                deleteIssuer(acqAppKey:=allParams(0), iss:=allParams(1))
            End If
            If eventTarget = "deleteACQ" Then
                allParams = getAllParams()
                deleteAcquirer(acqAppKey:=allParams(0))
            End If
            If eventTarget = "deleteAPP" Then
                allParams = getAllParams()
                deleteApp(acqAppKey:=allParams(0))
            End If
            If eventTarget = "deleteKEY" Then
                allParams = getAllParams()
                deleteKey(acqAppKey:=allParams(0))
            End If
            If eventTarget = "deleteEXT" Then
                allParams = getAllParams()
                deleteExt(acqAppKey:=allParams(0))
            End If
        End If
            If Not IsPostBack Then

            Try
                Me.LblAnio.Text = Now.Year
                Dim objAccessToken As AccessToken = Session("AccessToken")
                Dim objSessionParameters As SessionParameters = Session("SessionParameters")

                If Not objAccessToken Is Nothing Then
                    If objAccessToken.Authenticated = True And objAccessToken.Enabled = True Then

                        If objSessionParameters.intPasswordErrorCode <> 1 Then
                            repMenu.DataSource = objAccessToken.GetMenu(objSessionParameters.strSelectedMenu)
                            repMenu.DataBind()
                            stisTree()


                            'Validate License Type - Free Trial
                            If objAccessToken.LicenseType = 1 Then
                                ltrLicense.Text = "<div id='stats' style='z-index: 840; display: block;'><div class='column' style='z-index: 800; width:100%; border-right: none !important;'>	<b class='down'>Free-Trial 30 Days</b>	Contacte al Administrador del Sistema para una Licencia PRO. Licencia actual válida por " & IIf(objAccessToken.LicenseExpiryDate > Now, IIf((objAccessToken.LicenseExpiryDate - Now).Days > 1, (objAccessToken.LicenseExpiryDate - Now).Days & " días", (objAccessToken.LicenseExpiryDate - Now).Days & " día"), 0) & ".</div></div>"
                            Else
                                ltrLicense.Text = ""
                            End If

                            Dim pageName = Page.ToString().Substring(4, Page.ToString().Substring(4).Length - 5)

                            'Only in Index.aspx Page
                            If pageName.ToLower = "index" Then
                                'Show Warning message to change password before WARNING_PERIOD_FOR_CHANGE_PWD Days
                                Dim PeriodInDays = DateDiff(DateInterval.Day, Now, objAccessToken.DatePasswordExp()) + 1

                                If PeriodInDays <= WARNING_PERIOD_FOR_CHANGE_PWD Then
                                    ltrLicense.Text = "<div id='stats' style='z-index: 840; display: block;'><div class='column' style='z-index: 800; width:1050px; border-right: none !important;'>	<b class='down'>Aviso de Cambio de Contraseña</b>	Su contraseña expira en " & IIf(PeriodInDays = 1, PeriodInDays & " día !", PeriodInDays & " días !") & "</div></div>"
                                Else
                                    If ltrLicense.Text.Length <= 0 Then
                                        ltrLicense.Text = ""
                                    End If
                                End If
                            End If

                        Else
                            'Estado Usuario, Sin Cambiar Clave
                            'Generar Exception
                            repMenu.DataSource = Nothing
                            repMenu.DataBind()

                            'Solo Permitir el Form. de cambio de clave
                            If getCurrentPage() <> "Account/ChangePassword.aspx" Then
                                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Load", "Access Fault Raised. You are trying to enter a page without permission.", "")
                                HandleErrorRedirect(New Exception("Access Fault Raised. You are trying to enter a page without permission."))
                                Exit Sub
                            End If
                        End If

                        'Valores Visibles
                        lblUserName.Text = objAccessToken.DisplayName
                        lblDateTime.Text = Now().ToString
                    End If
                Else
                    'Valores Visibles
                    lblUserName.Text = ""
                    lblDateTime.Text = Now().ToString
                    Server.ClearError()
                    Response.Clear()
                    Response.Redirect("Login.aspx", False)
                End If

                'Llamando al método true= https; false = http;
                Dim protocol As String = ConfigurationSettings.AppSettings.Get("Protocol").Trim().ToUpper()
                protocoloSeguro(protocol = "HTTPS")
            Catch ex As Exception
                HandleErrorRedirect(ex)
            End Try
        End If
    End Sub
    'Implementacion Tree View Stis Desktop
    Private Sub stisTree()

        Dim objSessionParameters As SessionParameters = Session("SessionParameters")

        Dim packets As List(Of Packet) = New List(Of Packet)
        Dim acquirers As List(Of Acquirer)
        Dim issuers As List(Of Issuer)
        Dim cards As List(Of Card)
        Dim emvapps As List(Of EMVApp)
        Dim capks As List(Of CAPK)
        Dim exts As List(Of Ext)
        Dim packet As Packet
        Dim counter As Integer = 0
        Dim counterAcquirer As Integer = 1
        Dim counterIssuer As Integer = 1
        Dim counterCardRange As Integer = 1
        Dim counterEmvapp As Integer = 1
        Dim counterCapk As Integer = 1
        Dim counterExt As Integer = 1
        Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString
        Dim terminal As New TerminalStruc
        terminal.setId(objSessionParameters.StrTerminalID)
        terminal.setPcc("930001")
        terminal.setStan("123456")
        If (TerminalRecordNumber.Exec(conexion, terminal) And Not terminal.getRecord().Equals("")) Then
            'Console.WriteLine(terminal.To_String)
            acquirers = TreeAcquirers.Exec(conexion, terminal)
            issuers = TreeIssuers.Exec(conexion, terminal)
            cards = TreeCardRanges.Exec(conexion, terminal)
            emvapps = TreeEmvApss.Exec(conexion, terminal)
            capks = TreeCapks.Exec(conexion, terminal)
            exts = TreeExt.Exec(conexion, terminal)
            'Ordenamos en arbol
            packet = New Packet()
            packet.setSequence(counter.ToString)
            packet.setTable("TER")
            packet.setCode("TER")
            packet.setId("0")
            packets.Add(packet)
            If (acquirers IsNot Nothing) Then

                For Each acquirer As Acquirer In acquirers

                    'Console.WriteLine(acquirer.To_String)

                    packet = New Packet()
                    packet.setSequence(counter.ToString)
                    counter = counter + 1
                    packet.setTable("ACQ")
                    packet.setCode(acquirer.getAcquirer_code())
                    packet.setName(acquirer.getAcquirer_key_name())
                    packet.setId(counterAcquirer.ToString.PadLeft(2, "0"))
                    counterAcquirer = counterAcquirer + 1
                    packets.Add(packet)

                    For Each issuer As Issuer In issuers
                        If (acquirer.getAcquirer_code().Equals(issuer.getAcquirer_code())) Then

                            packet = New Packet()
                            packet.setSequence(counter.ToString)
                            counter = counter + 1
                            packet.setTable("ISS")
                            packet.setCode(issuer.getIssuer_code() + "ACQ" + acquirer.getAcquirer_code())
                            packet.setName(issuer.getIssuer_key_name())
                            packet.setId(counterIssuer.ToString.PadLeft(2, "0"))
                            packet.setAcquirerFather((counterAcquirer - 1).ToString.PadLeft(2, "0"))
                            counterIssuer = counterIssuer + 1
                            'Console.WriteLine(issuer.To_String())
                            packets.Add(packet)

                            For Each card As Card In cards

                                If ((issuer.getAcquirer_code() + issuer.getIssuer_code()).Equals(card.getAcquirer_code() + card.getIssuer_code())) Then

                                    packet = New Packet()
                                    packet.setSequence(counter.ToString)
                                    counter = counter + 1
                                    packet.setTable("CDR")
                                    packet.setCode(card.getCard_code() + "ISS" + issuer.getIssuer_code() + "ACQ" + acquirer.getAcquirer_code())
                                    packet.setName(card.getCard_key_name())
                                    packet.setId(counterCardRange.ToString.PadLeft(2, "0"))
                                    packet.setAcquirerFather((counterAcquirer - 1).ToString.PadLeft(2, "0"))
                                    packet.setIssuerFather((counterIssuer - 1).ToString.PadLeft(2, "0"))
                                    counterCardRange = counterCardRange + 1
                                    'Console.WriteLine(card.To_String())
                                    packets.Add(packet)

                                End If

                            Next


                        End If
                    Next

                    counterIssuer = 1

                Next

            End If

            Dim lastPacketParameter As Packet = packets(counter)
            lastPacketParameter.setLastParameter(True)
            packets.RemoveAt(counter)
            packets.Add(lastPacketParameter)


            For Each emvApp As EMVApp In emvapps

                packet = New Packet()
                packet.setSequence(counter.ToString)
                counter = counter + 1
                packet.setTable("APP")
                packet.setCode(emvApp.getEmvapp_code())
                packet.setId(counterEmvapp.ToString.PadLeft(2, "0"))
                counterEmvapp = counterEmvapp + 1
                'Console.WriteLine(emvApp.To_String())
                packets.Add(packet)



            Next

            If (emvapps.Count > 0) Then
                Dim lastPacketEmvApp As Packet = packets(counter)
                lastPacketEmvApp.setLastEmvApp(True)
                packets.RemoveAt(counter)
                packets.Add(lastPacketEmvApp)


            End If

            For Each capk As CAPK In capks

                packet = New Packet()
                packet.setSequence(counter.ToString)
                counter = counter + 1
                packet.setTable("KEY")
                packet.setCode(capk.getCapk_code())
                packet.setId(counterCapk.ToString.PadLeft(2, "0"))
                counterCapk = counterCapk + 1
                'Console.WriteLine(capk.To_String())
                packets.Add(packet)

            Next


            If (capks.Count > 0) Then
                Dim lastPacketCapk As Packet = packets(counter)
                lastPacketCapk.setLastCapk(True)
                packets.RemoveAt(counter)
                packets.Add(lastPacketCapk)
            End If

            For Each ext As Ext In exts

                packet = New Packet()
                packet.setSequence(counter.ToString)
                counter = counter + 1
                packet.setTable("EXT")
                packet.setCode(ext.getExt_code())
                packet.setName(ext.getExt_name())
                packet.setId(counterExt.ToString.PadLeft(2, "0"))
                counterExt = counterExt + 1
                'Console.WriteLine(ext.To_String())
                packets.Add(packet)

            Next

            If (capks.Count > 0) Then
                Dim lastPacketExt As Packet = packets(counter)
                lastPacketExt.setLastExt(True)
                packets.RemoveAt(counter)
                packets.Add(lastPacketExt)
            End If

            'Console.WriteLine()
            'Console.WriteLine("Terminal - " + terminal.getRecord + " [" + terminal.getId + "]")
            'Console.WriteLine()
            For Each readPacket As Packet In packets
                If (readPacket.getTable.Equals("TER")) Then

                    Dim child As New TreeNode() With {
                      .Text = "Terminal",
                      .Value = "0",
                      .NavigateUrl = "Group/EditTerminalstis.aspx",
                      .ImageUrl = "~\img\computerB.png"
                      }


                    TreeView1.Nodes.Add(child)

                    Dim child2 As New TreeNode() With {
                      .Text = "Acquirer/Issuer/Card Range",
                      .Value = "0",
                      .NavigateUrl = HttpContext.Current.Request.Url.ToString(),
                      .ImageUrl = "~\img\dataB.png"
                     }
                    TreeView1.Nodes(0).ChildNodes.Add(child2)
                ElseIf (readPacket.getTable.Equals("ACQ")) Then

                    Dim child As New TreeNode() With {
                        .Value = "ACQ" + readPacket.getCode(),
                        .Text = "" + readPacket.getName(),
                        .ImageUrl = "~\img\shopB.png"
                    }

                    TreeView1.Nodes(0).ChildNodes(0).ChildNodes.Add(child)

                End If
            Next
            For Each readPacket As Packet In packets


                If (readPacket.getTable.Equals("ISS")) Then

                    Dim child As New TreeNode() With {
                        .Value = "ISS" + readPacket.getCode(),
                        .Text = "" + readPacket.getName(),
                        .ImageUrl = "~\img\creditcardB.png"
                     }

                    Try
                        TreeView1.Nodes(0).ChildNodes(0).ChildNodes(Convert.ToInt32(readPacket.getAcquirerFather()) - 1).ChildNodes.Add(child)

                    Catch ex As Exception
                        Dim MEJ As String = ex.Message()
                        Console.WriteLine(MEJ)
                    End Try

                End If
            Next

            For Each readPacket As Packet In packets

                If (readPacket.getTable.Equals("CDR")) Then
                    ' Console.Write("[CARD RANGE]")
                    Dim child As New TreeNode() With {
                      .Value = "CDR" + readPacket.getCode(),
                      .Text = "" + readPacket.getName(),
                      .ImageUrl = "~\img\blogB.png"
                     }


                    Try
                        Dim acquirerFather As Integer
                        Dim issuerFather As Integer

                        acquirerFather = Convert.ToInt32(readPacket.getAcquirerFather())
                        issuerFather = Convert.ToInt32(readPacket.getIssuerFather())
                        'If acquirerFather <> 1 Then
                        '    issuerFather = issuerFather - acquirerFather
                        'End If
                        TreeView1.Nodes(0).ChildNodes(0).ChildNodes(acquirerFather - 1).ChildNodes(issuerFather - 1).ChildNodes.Add(child)

                    Catch ex As Exception
                        Dim MEJ As String = ex.Message()
                        Console.WriteLine(MEJ)
                    End Try

                End If
            Next

            Dim childEmv As New TreeNode() With {
                      .Text = "EMVL2 Application",
                      .Value = "0",
                      .NavigateUrl = HttpContext.Current.Request.Url.ToString(),
                      .ImageUrl = "~\img\networkB.png"
                       }
            TreeView1.Nodes(0).ChildNodes.Add(childEmv)
            For Each readPacket As Packet In packets
                If (readPacket.getTable.Equals("APP")) Then
                    Dim child2 As New TreeNode() With {
                      .Text = readPacket.getCode(),
                      .Value = "APP" + readPacket.getCode(),
                      .ImageUrl = "~\img\blogB.png"
                     }
                    TreeView1.Nodes(0).ChildNodes(1).ChildNodes.Add(child2)

                End If
            Next

            Dim childKey As New TreeNode() With {
                      .Text = "EMVL2 Key",
                      .Value = "0",
                      .NavigateUrl = HttpContext.Current.Request.Url.ToString(),
                      .ImageUrl = "~\img\keyB.png"
                     }
            TreeView1.Nodes(0).ChildNodes.Add(childKey)
            For Each readPacket As Packet In packets
                If (readPacket.getTable.Equals("KEY")) Then
                    Dim child2 As New TreeNode() With {
                      .Text = readPacket.getCode(),
                      .Value = "KEY" + readPacket.getCode(),
                      .ImageUrl = "~\img\blogB.png"
                     }
                    TreeView1.Nodes(0).ChildNodes(2).ChildNodes.Add(child2)
                End If
            Next
            Dim childExApp As New TreeNode() With {
                      .Text = "Extra Aplication",
                      .Value = "0",
                      .NavigateUrl = HttpContext.Current.Request.Url.ToString(),
                      .ImageUrl = "~\img\blockcB.png"
                     }
            TreeView1.Nodes(0).ChildNodes.Add(childExApp)
            For Each readPacket As Packet In packets
                If (readPacket.getTable.Equals("EXT")) Then
                    Dim child2 As New TreeNode() With {
                      .Text = readPacket.getCode(),
                      .Value = "EXT" + readPacket.getName(),
                      .ImageUrl = "~\img\blogB.png"
                     }
                    TreeView1.Nodes(0).ChildNodes(3).ChildNodes.Add(child2)
                End If
            Next
        End If
    End Sub
    ' Error handler estándar, redirecciona a página de error
    Protected Sub HandleErrorRedirect(ByVal exception As Exception)

        If Session("Exception") Is Nothing Then
            Session("Exception") = exception
        End If

        Server.ClearError()

        Response.Redirect("~/ErrorPage.aspx")

    End Sub

    Protected Sub lnkMenuItem_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)

        Dim objSessionParameters As SessionParameters = Session("SessionParameters")
        Dim i As Integer = 0
        Dim strAbsolutePath As String = "~/"
        Dim subFolders As Integer = 0

        'Iniciar Módulo de Menú Seleccionado
        objSessionParameters.strSelectedMenu = e.CommandArgument.ToString.Split(SEPARATOR)(1)

        Session("SessionParameters") = objSessionParameters

        Response.Redirect(strAbsolutePath & e.CommandArgument.ToString.Split(SEPARATOR)(0))

    End Sub

    Protected Sub lnkHome_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkHome.Click
        goToIndex()
    End Sub

    Protected Sub lnkLogo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLogo.Click
        goToIndex()
    End Sub

    Private Sub goToIndex()

        Dim objSessionParameters As SessionParameters = Session("SessionParameters")

        'Borrar Parámetros
        objSessionParameters.strSelectedMenu = "0"
        objSessionParameters.strSelectedOption = "0"

        'Ir a Inicio
        Session("SelectedMenu") = ""
        Session("SelectedOption") = ""

        Response.Redirect("~/Index.aspx")
    End Sub

    Private Function getCurrentPage() As String

        Dim strPageName As String = Request.AppRelativeCurrentExecutionFilePath
        strPageName = strPageName.Substring(strPageName.IndexOf("/") + 1)

        Return strPageName

    End Function

    'Metodo de redireccionamiento..
    Public Sub protocoloSeguro(ByVal bSeguro As Boolean)
        Dim redirectUrl As String = Nothing
        If bSeguro AndAlso (Not Request.IsSecureConnection) Then
            redirectUrl = Request.Url.ToString().Replace("http:", "https:")
        Else
            If (Not bSeguro) AndAlso Request.IsSecureConnection Then
                redirectUrl = Request.Url.ToString().Replace("https:", "http:")
            End If
        End If

        If redirectUrl IsNot Nothing Then
            Response.Redirect(redirectUrl)
        End If
    End Sub
    'Tree View Select Node
    Protected Sub TreeView1_SelectedNodeChanged(sender As Object, e As EventArgs) Handles TreeView1.SelectedNodeChanged
        Dim objSessionParameters As SessionParameters = Session("SessionParameters")

        If (TreeView1.SelectedValue.StartsWith("ACQ")) Then
            objSessionParameters.StrAcquirerCode = "ACQ" + TreeView1.SelectedValue.Substring(3)
            Response.Redirect("EditAcquirer.aspx")
        End If
        If (TreeView1.SelectedValue.StartsWith("ISS")) Then
            objSessionParameters.StrIsserCode = "ISS" + TreeView1.SelectedValue.Substring(3)
            Response.Redirect("EditIssuer.aspx")
        End If
        If (TreeView1.SelectedValue.StartsWith("CDR")) Then
            objSessionParameters.StrCardRangeCode = "CDR" + TreeView1.SelectedValue.Substring(3)
            Response.Redirect("EditCardRange.aspx")
        End If
        If (TreeView1.SelectedValue.StartsWith("APP")) Then
            objSessionParameters.StrParamterEMV = "APP" + TreeView1.SelectedNode.Text
            Response.Redirect("TerminalParameterEMV.aspx")
        End If
        If (TreeView1.SelectedValue.StartsWith("KEY")) Then
            objSessionParameters.strEmvApplication = TreeView1.SelectedNode.Text
            Response.Redirect("ListarEmvLevel2KeyParameter.aspx")
        End If
        If (TreeView1.SelectedValue.StartsWith("EXT")) Then
            objSessionParameters.StrExtraParameter = "EXT" + TreeView1.SelectedValue.Substring(3)
            Response.Redirect("TerminalExtraApplicationParameter.aspx")
        End If
    End Sub
    '.NavigateUrl = "Group/ListarCardRange.aspx"
    '.NavigateUrl = "Group/ListarIssuer.aspx"
    '.NavigateUrl = "Group/ListarAcquirer.aspx"



    'objSessionParameters.StrTerminalID = grdTerminalsParameter.SelectedDataKey.Values.Item("TID")

    Public Sub addMethod(aValue As String)
        Dim str As String
        str = aValue
        MsgBox("ADD:" + str)

    End Sub
    Public Sub deleteCardRage(acqAppKey As String, iss As String, cdr As String)
        Dim str As String
        Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString

        Dim objSessionParameters As SessionParameters = Session("SessionParameters")
        Dim terminal As New TerminalStruc


        terminal.setId(objSessionParameters.StrTerminalID)
        terminal.setPcc("930001")
        terminal.setStan("123456")
        TerminalRecordNumber.Exec(conexion, terminal)
        Delete.cardRange(conexion, terminal.getRecord(), acqAppKey, iss, cdr)


        Response.Clear()
        Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
    End Sub
    Public Sub deleteIssuer(acqAppKey As String, iss As String)
        Dim str As String
        Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString

        Dim objSessionParameters As SessionParameters = Session("SessionParameters")
        Dim terminal As New TerminalStruc


        terminal.setId(objSessionParameters.StrTerminalID)
        terminal.setPcc("930001")
        terminal.setStan("123456")
        TerminalRecordNumber.Exec(conexion, terminal)
        Delete.issuer(conexion, terminal.getRecord(), acqAppKey, iss)


        Response.Clear()
        Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
    End Sub
    Public Sub deleteAcquirer(acqAppKey As String)
        Dim str As String
        Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString

        Dim objSessionParameters As SessionParameters = Session("SessionParameters")
        Dim terminal As New TerminalStruc


        terminal.setId(objSessionParameters.StrTerminalID)
        terminal.setPcc("930001")
        terminal.setStan("123456")
        TerminalRecordNumber.Exec(conexion, terminal)
        Delete.acquirer(conexion, terminal.getRecord(), acqAppKey)


        Response.Clear()
        Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
    End Sub

    Public Sub deleteApp(acqAppKey As String)
        Dim str As String
        Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString

        Dim objSessionParameters As SessionParameters = Session("SessionParameters")
        Dim terminal As New TerminalStruc


        terminal.setId(objSessionParameters.StrTerminalID)
        terminal.setPcc("930001")
        terminal.setStan("123456")
        TerminalRecordNumber.Exec(conexion, terminal)
        Delete.application(conexion, terminal.getRecord(), acqAppKey)


        Response.Clear()
        Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
    End Sub
    Public Sub deleteKey(acqAppKey As String)
        Dim str As String
        Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString

        Dim objSessionParameters As SessionParameters = Session("SessionParameters")
        Dim terminal As New TerminalStruc


        terminal.setId(objSessionParameters.StrTerminalID)
        terminal.setPcc("930001")
        terminal.setStan("123456")
        TerminalRecordNumber.Exec(conexion, terminal)
        Delete.key(conexion, terminal.getRecord(), acqAppKey)


        Response.Clear()
        Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
    End Sub
    Public Sub deleteExt(acqAppKey As String)
        Dim str As String
        Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderStisConnectionString").ConnectionString

        Dim objSessionParameters As SessionParameters = Session("SessionParameters")
        Dim terminal As New TerminalStruc


        terminal.setId(objSessionParameters.StrTerminalID)
        terminal.setPcc("930001")
        terminal.setStan("123456")
        TerminalRecordNumber.Exec(conexion, terminal)
        Delete.extraApplication(conexion, terminal.getRecord(), acqAppKey)


        Response.Clear()
        Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
    End Sub
    Public Sub reload()
        Response.Clear()
        Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
    End Sub
End Class



