﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Security

Partial Class ReportPage
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        On Error Resume Next

        Me.LblAnio.Text = Now.Year
        Dim objAccessToken As AccessToken = Session("AccessToken")

        lblUserName.Text = objAccessToken.DisplayName
        lblDateTime.Text = Now().ToString

        If Not Page.IsPostBack Then
            'Llamando al método true= https; false = http;
            Dim protocol As String = ConfigurationSettings.AppSettings.Get("Protocol").Trim().ToUpper()
            protocoloSeguro(protocol = "HTTPS")

            'Validate License Type - Free Trial
            If objAccessToken.LicenseType = 1 Then
                ltrLicense.Text = "<div id='stats' style='z-index: 840; display: block; margin-left: 2px;'><div class='column' style='z-index: 800; width:98%; border-right: none !important;'>	<b class='down'>Free-Trial 30 Days</b>	Contacte al Administrador del Sistema para una Licencia PRO. Licencia actual válida por " & IIf(objAccessToken.LicenseExpiryDate > Now, IIf((objAccessToken.LicenseExpiryDate - Now).Days > 1, (objAccessToken.LicenseExpiryDate - Now).Days & " días", (objAccessToken.LicenseExpiryDate - Now).Days & " día"), 0) & ".</div></div>"
            Else
                ltrLicense.Text = ""
            End If

        End If
    End Sub

    'Metodo de redireccionamiento..
    Public Sub protocoloSeguro(ByVal bSeguro As Boolean)
        Dim redirectUrl As String = Nothing
        If bSeguro AndAlso (Not Request.IsSecureConnection) Then
            redirectUrl = Request.Url.ToString().Replace("http:", "https:")
        Else
            If (Not bSeguro) AndAlso Request.IsSecureConnection Then
                redirectUrl = Request.Url.ToString().Replace("https:", "http:")
            End If
        End If

        If redirectUrl IsNot Nothing Then
            Response.Redirect(redirectUrl)
        End If
    End Sub

    Protected Sub lnkLogo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLogo.Click
        'Ir a Inicio
        Session("SelectedMenu") = ""
        Session("SelectedOption") = ""

        Response.Redirect("~/Index.aspx")
    End Sub
End Class

