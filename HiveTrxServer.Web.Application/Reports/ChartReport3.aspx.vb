﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users
Imports System.Globalization

Partial Class Reports_ChartReport
    Inherits TeleLoader.Web.BasePage

    Private totalTerminals As Integer
    Private totalUpdated As Integer
    Private totalPending As Integer


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtGroupName.Text = objSessionParams.strSelectedGroupName
            getReportTerminalsData()
            txtTerminalsNumber.Text = totalTerminals

            Dim arrayDataJS = "var dataNet = [['Estado', 'Terminales'], ['Activado', " & totalUpdated & "], ['Desactivado', " & totalPending & "]];"

            arrayDataJS += " var dataNet2 = " + getReportTerminalsDataByModel() + ";"
            ltrScript.Text = "<script language='javascript'>" & arrayDataJS & "</script>"

            If totalTerminals = 0 Then
                pnlCharts.Visible = False
            Else
                pnlCharts.Visible = True
            End If

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Private Function getReportName() As String

        Dim retVal As String = ""

        Select Case objSessionParams.intReportType
            Case 54
                retVal = "Log de Auditoría"
            Case 140
                retVal = "Log de Inventario"
            Case 141
                retVal = "Log de Descarga"
            Case 142
                retVal = "Reporte de Terminales"
            Case 143
                retVal = "Estado Actualización de Terminales"
            Case 150
                retVal = "Estado De Terminales"
            Case 151
                retVal = "Terminales Modo Bloqueante"
            Case 152
                retVal = "Terminales Por Prioridad De Grupo	"

        End Select

        Return retVal

    End Function

    Public Sub getReportTerminalsData()
        Dim connection As New SqlConnection(strConnectionString)
        Dim command As New SqlCommand()
        Dim results As SqlDataReader

        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webReporteChartTerminalesPrioridad"
            command.Parameters.Clear()
            command.Parameters.Add(New SqlParameter("customerID", objAccessToken.CustomerUserID))
            command.Parameters.Add(New SqlParameter("groupID", objSessionParams.intSelectedGroupID))

            'Leer Resultado
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    totalUpdated = results.GetInt32(0)
                    totalPending = results.GetInt32(1)
                    totalTerminals = results.GetInt32(2)
                End While
            Else
                totalUpdated = 0
                totalPending = 0
                totalTerminals = 0

            End If

        Catch ex As Exception

        Finally
            'Cerrar DataReader
            If Not (results Is Nothing) Then
                results.Close()
            End If

            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try

    End Sub

    Public Function getReportTerminalsDataByModel() As String
        Dim connection As New SqlConnection(strConnectionString)
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim retVal As String
        Dim idx As Integer = 0

        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webReporteChartTerminalesXModeloPrioridad"
            command.Parameters.Clear()
            command.Parameters.Add(New SqlParameter("customerID", objAccessToken.CustomerUserID))
            command.Parameters.Add(New SqlParameter("groupID", objSessionParams.intSelectedGroupID))

            'Leer Resultado
            results = command.ExecuteReader()
            retVal = "[['Tipo De Estado','Estado'],"
            If results.HasRows Then
                While results.Read()
                    retVal += "['" + results.GetString(0) + "'," + results.GetString(1) + "],"
                End While
            End If

            retVal += "]"

        Catch ex As Exception

        Finally
            'Cerrar DataReader
            If Not (results Is Nothing) Then
                results.Close()
            End If

            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try

        Return retVal

    End Function

End Class
