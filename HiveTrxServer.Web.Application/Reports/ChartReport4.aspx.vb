﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users
Imports System.Globalization

Partial Class Reports_ChartReport
    Inherits TeleLoader.Web.BasePage

    Private totalTerminals As Integer
    Private totalUpdated As Integer
    Private totalPending As Integer


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim arrayDataJS = " var dataNet2 = " + getReportEstadisticasTerminals() + ";"
            ltrScript.Text = "<script language='javascript'>" & arrayDataJS & "</script>"

            If totalTerminals = 0 Then
                pnlCharts.Visible = False
            Else
                pnlCharts.Visible = True
            End If

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Private Function getReportName() As String

        Dim retVal As String = ""

        Select Case objSessionParams.intReportType
            Case 54
                retVal = "Log de Auditoría"
            Case 140
                retVal = "Log de Inventario"
            Case 141
                retVal = "Log de Descarga"
            Case 142
                retVal = "Reporte de Terminales"
            Case 143
                retVal = "Estado Actualización de Terminales"
            Case 150
                retVal = "Estado De Terminales"
            Case 151
                retVal = "Terminales Modo Bloqueante"
            Case 152
                retVal = "Terminales Por Prioridad De Grupo	"

        End Select

        Return retVal

    End Function


    Public Function getReportEstadisticasTerminals() As String
        Dim connection As New SqlConnection(strConnectionString)
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim retVal As String
        Dim idx As Integer = 0
        Dim startDate As String = objSessionParams.dateStartDate.Year.ToString.PadLeft(4, "0") +
          objSessionParams.dateStartDate.Month.ToString.PadLeft(2, "0") +
            objSessionParams.dateStartDate.Day.ToString.PadLeft(2, "0")
        Dim endDate As String = objSessionParams.dateEndDate.Year.ToString.PadLeft(4, "0") +
            objSessionParams.dateEndDate.Month.ToString.PadLeft(2, "0") +
            objSessionParams.dateEndDate.Day.ToString.PadLeft(2, "0")
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webReporteChartEstadisticas"
            command.Parameters.Clear()
            command.Parameters.Add(New SqlParameter("ter_id", objSessionParams.intSelectedTerminalID))
            command.Parameters.Add(New SqlParameter("tipo_id", objSessionParams.intSelectedTipoID))
            command.Parameters.Add(New SqlParameter("fechaInicial", startDate))
            command.Parameters.Add(New SqlParameter("fechaFinal", endDate))
            command.Parameters.Add(New SqlParameter("cli_id", objAccessToken.CustomerUserID))
            command.Parameters.Add(New SqlParameter("gru_id", "-1"))
            'Leer Resultado
            results = command.ExecuteReader()
            retVal = "[['fecha','valor'],"
            If results.HasRows Then
                While results.Read()
                    retVal += "['" + results.GetString(0) + "'," + results.GetString(1) + "],"
                End While
            End If

            retVal += "]"

        Catch ex As Exception

        Finally
            'Cerrar DataReader
            If Not (results Is Nothing) Then
                results.Close()
            End If

            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try
        Return retVal

    End Function

End Class
