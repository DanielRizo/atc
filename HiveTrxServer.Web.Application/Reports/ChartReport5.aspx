﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ChartReport5.aspx.vb" Inherits="Reports_ChartReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%>:: Reporte Estado Actualización
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed"
            PostBackUrl="~/Reports/Manager.aspx">Reportes</asp:LinkButton>
    </li>
    <li>Consultas Por Terminal</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    Estadisticas Terminales
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <blockquote>
        <p>Estadisticas Terminales:</p>
        <footer class="blockquote-footer">Polaris Cloud Service </footer>
    </blockquote>
    <style>
        .bg-1 {
            background-color: #1abc9c;
            color: #ffffff;
        }
    </style>

    <div class="bg-1">
        <div class="container text-center">
            <br />
            <img src="https://image.flaticon.com/icons/png/512/446/446813.png" class="img-" alt="Bird" width="350" height="310">
            <br />
            <br />
        </div>
    </div>

    <br />
    <br />
    <asp:Panel ID="pnlMsg" runat="server" Visible="False">
        <div class="albox succesbox">
            <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
            <a href="#" class="close tips" title="Cerrar">Cerrar</a>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlError" runat="server" Visible="False">
        <div class="albox errorbox">
            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
            <a href="#" class="close tips" title="Cerrar">Cerrar</a>
        </div>
    </asp:Panel>
    <asp:Panel ID="Panel1" runat="server" Visible="False">
        <div class="albox succesbox">
            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
            <a href="#" class="close tips" title="Cerrar">Cerrar</a>
        </div>
    </asp:Panel>

    <asp:Panel ID="Panel2" runat="server" Visible="False">
        <div class="albox errorbox">
            <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
            <a href="#" class="close tips" title="Cerrar">Cerrar</a>
        </div>
    </asp:Panel>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', { 'packages': ['corechart', 'bar'] });
        google.charts.setOnLoadCallback(drawStuff);

        function drawStuff() {

            var button = document.getElementById('change-chart');
            //button.remove;
            var chartDiv = document.getElementById('chart_div');

            var data = google.visualization.arrayToDataTable();

            var materialOptions = {
                width: 900,
                chart: {
                    title: 'Estadisticas Terminales',
                    subtitle: 'Estadisticas Terminales'
                },
                series: {
                    0: { axis: 'distance' },
                    1: { axis: 'brightness' }
                },
                axes: {
                    y: {
                        distance: { label: 'Estadisticas Terminales' },
                        brightness: { side: 'right', label: 'apparent magnitude' }
                    }
                }
            };
            function drawMaterialChart() {
                var materialChart = new google.charts.Bar(chartDiv);
                materialChart.draw(data, google.charts.Bar.convertOptions(materialOptions));
                button.innerText = 'Estadisticas';
                //button.onclick = drawMaterialChart;
            }


            drawMaterialChart();
        };
    </script>
    <!-- end pie chart javascript codes -->
    <asp:Panel ID="pnlCharts" runat="server">
        <!-- start chart div -->
        <br />
        <h2 style="text-align: center;"></h2>
        <div id="terminals_chart_div" style="text-align: center;"></div>
        <br />
    </asp:Panel>
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

        // Load the Visualization API and the piechart package.
        google.charts.load('current', { 'packages': ['corechart'] });

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {


            // Create the data table.

            var data = google.visualization.arrayToDataTable(dataNet2);

            document.getElementById("error_msg").innerHTML = "GRAFICA GENERADA DE FORMA CORRECTA, SE MOSTRARAN LOS ULTIMOS 40 DIAS DE ACTIVIDAD";
            // Set chart options
            var options = {
                'title': '',
                'width': 1350,
                'height': 500
            };

            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
            google.visualization.events.addListener(chart, 'error', function (googleError) {
                google.visualization.errors.removeError(googleError.id);
                document.getElementById("error_msg").innerHTML = "NO HAY DATOS SELECCIONE OTRO TERMINAL";

            });
            chart.draw(data, options);
        }
    </script>
    <div id="error_msg" style="width: 1000px; height: 70px; background-color: #1ABC9C; color: white; text-align: center; line-height: 50px; float: none; z-index: auto; position: static;">
    </div>

    <div id="chart_div" style="width: 980px; height: 500px"></div>




    <asp:HyperLink ID="lnkBack" runat="server"
        NavigateUrl="~/Reports/EntryPoint.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>


<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

</asp:Content>

