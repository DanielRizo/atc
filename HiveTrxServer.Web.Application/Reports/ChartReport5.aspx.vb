﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users
Imports System.Globalization

Partial Class Reports_ChartReport
    Inherits TeleLoader.Web.BasePage

    Private totalTerminals As Integer
    Private totalUpdated As Integer
    Private totalPending As Integer


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then


            Dim arrayDataJS = " var dataNet2 = " + getReportEstadisticasTerminals() + ";"
            ltrScript.Text = "<script language='javascript'>" & arrayDataJS & "</script>"

            If totalTerminals = 0 Then
                pnlCharts.Visible = False
            Else
                pnlCharts.Visible = True
            End If

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub
    Public Function getReportEstadisticasTerminals() As String
        Dim connection As New SqlConnection(strConnectionString)
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim retVal As String
        Dim idx As Integer = 0
        Dim startDate As String = objSessionParams.dateStartDate.Year.ToString.PadLeft(4, "0") +
          objSessionParams.dateStartDate.Month.ToString.PadLeft(2, "0") +
            objSessionParams.dateStartDate.Day.ToString.PadLeft(2, "0")
        Dim endDate As String = objSessionParams.dateEndDate.Year.ToString.PadLeft(4, "0") +
            objSessionParams.dateEndDate.Month.ToString.PadLeft(2, "0") +
            objSessionParams.dateEndDate.Day.ToString.PadLeft(2, "0")
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webReporteChartTransacciones"
            command.Parameters.Clear()
            command.Parameters.Add(New SqlParameter("tipo_id", "91"))
            command.Parameters.Add(New SqlParameter("fechaInicial", startDate))
            command.Parameters.Add(New SqlParameter("fechaFinal", endDate))
            command.Parameters.Add(New SqlParameter("TerId", objSessionParams.intSelectedTerminalID))
            'Leer Resultado
            results = command.ExecuteReader()
            retVal = "[['fecha','valor'],"
            If results.HasRows Then
                While results.Read()
                    retVal += "['" + results.GetString(0) + "'," + results.GetString(1) + "],"
                End While
            End If

            retVal += "]"

        Catch ex As Exception

        Finally
            'Cerrar DataReader
            If Not (results Is Nothing) Then
                results.Close()
            End If

            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try

        Return retVal

    End Function

End Class
