﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ChartReportCopia.aspx.vb" Inherits="Reports_ChartReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%>:: Reporte Estado Actualización
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

    <!-- Google Js Api / Chart and others -->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

    <!-- charts Related JS -->
    <script type="text/javascript" src="../js/raphael.js"></script>
    <script type="text/javascript" src="../js/analytics.js"></script>
    <script type="text/javascript" src="../js/popup.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed"
            PostBackUrl="~/Reports/Manager.aspx">Reportes</asp:LinkButton>
    </li>
    <li>Estado De Terminales Por Grupo</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    Estado De Terminales Por Grupo
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <p>Este reporte le permite visualizar el estado de terminales por grupo:</p>

    <br />

    <asp:Panel ID="pnlMsg" runat="server" Visible="False">
        <div class="albox succesbox">
            <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
            <a href="#" class="close tips" title="Cerrar">Cerrar</a>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlError" runat="server" Visible="False">
        <div class="albox errorbox">
            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
            <a href="#" class="close tips" title="Cerrar">Cerrar</a>
        </div>
    </asp:Panel>

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <div class="titleh">
            <h3>Estado De Terminales Por Grupo</h3>
        </div>
        <div class="body">

            <div class="st-form-line">
                <span class="st-labeltext"><b>Grupo:</b></span>

                <asp:TextBox ID="txtGroupName" CssClass="st-success-input" Style="width: 510px"
                    runat="server" TabIndex="1" MaxLength="100"
                    ToolTip="Nombre del Grupo" onkeydown="return (event.keyCode!=13);"
                    Enabled="False"></asp:TextBox>
            </div>

            <div class="st-form-line">
                <span class="st-labeltext"><b>Total Terminales:</b></span>

                <asp:TextBox ID="txtTerminalsNumber" CssClass="st-success-input" Style="width: 50px"
                    runat="server" TabIndex="1" MaxLength="100"
                    ToolTip="Número de Terminales" onkeydown="return (event.keyCode!=13);"
                    Enabled="False"></asp:TextBox>
            </div>

            <!-- start pie chart javascript codes -->
            <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
            <script type="text/javascript">
                google.charts.load("current", { packages: ["corechart"] });
                google.charts.setOnLoadCallback(drawChart);
                var data = google.visualization.arrayToDataTable(dataNet);

                function drawChart() {
                    var data = google.visualization.arrayToDataTable(dataNet);

                    var options = {
                        title: 'Estado De Terminales',
                        pieHole: 0.4,
                        is3D: true,
                    };

                    var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
                    chart.draw(data, options);
                }
            </script>

            <div id="donutchart" style="width: 900px; height: 500px;"></div>

            <!-- end pie chart javascript codes -->
            <asp:Panel ID="pnlCharts" runat="server">
                <!-- start chart div -->
                <br />
                <h2 style="text-align: center;"></h2>
                <div id="terminals_chart_div" style="text-align: center;"></div>
                <br />

                <!-- end chart div -->
            </asp:Panel>
        </div>
    </div>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', { 'packages': ['corechart'] });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

            var data = google.visualization.arrayToDataTable(dataNet2);


            var options = {
                title: 'Terminales Activas',
                is3D: true,

            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart'));

            chart.draw(data, options);
        }
    </script>

    <div id="piechart" style="width: 900px; height: 500px;"></div>


    <asp:HyperLink ID="lnkBack" runat="server"
        NavigateUrl="~/Reports/EntryPoint.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

</asp:Content>

