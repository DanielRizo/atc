﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="EntryPoint.aspx.vb" Inherits="Reports_EntryPoint" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%>:: Generación de Reportes
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

    <%-- Con estos dos js se autocompleta el serial  --%>
    <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery/jquery-1.8.0.js"></script>
    <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.22/jquery-ui.js"></script>

    <script type="text/javascript" src="../js/jquery.ui.datepicker.js"></script>
    <link type="text/css" rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <link type="text/css" rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
    <style type="text/css">
        .bg-1 {
            background-color: #17a2b8;
            color: #ffffff;
        }
    </style>
    <style type="text/css">
        .button {
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
        }

        .button1 {
            background-color: #4CAF50;
        }
        /* Green */
        .button2 {
            background-color: #008CBA;
        }
        /* Blue */
    </style>
    <script type="text/javascript">
        var j = jQuery.noConflict();

        $(function () {
            j("[id$=serialTerminal]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Reports/EntryPoint.aspx/GetSerials") %>',
                            data: "{ 'prefix': '" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('-')[0],
                                        val: item.split('-')[1]
                                    }
                                }))
                            },
                            error: function (response) {
                                alert(response.responseText);
                            },
                            failure: function (response) {
                                alert(response.responseText);
                            }
                        });
                    },
                    select: function (e, i) {
                        $("[id$=HiddenSerial]").val(i.item.val);
                    },
                    minLength: 1
                });
            });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed"
            PostBackUrl="~/Reports/Manager.aspx">Reportes</asp:LinkButton>
    </li>
    <li>Generación de Reportes</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <blockquote>
        <p>Este módulo le permite generar reportes del sistema:</p>
        <footer class="blockquote-footer">Polaris Cloud Service</footer>
    </blockquote>
    <br />
    <asp:Panel ID="pnlMsg" runat="server" Visible="False">
        <div class="albox succesbox">
            <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
            <a href="#" class="close tips" title="Cerrar">Cerrar</a>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlError" runat="server" Visible="False">
        <div class="albox errorbox">
            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
            <a href="#" class="close tips" title="Cerrar">Cerrar</a>
        </div>
    </asp:Panel>


    <div class="bg-1">
        <div class="container text-center">
            <h3>Reportes Polaris cloud service</h3>
            <%-- <asp:Panel ID="Panel1" runat="server" Visible="true">
                <div class="st-form-line" style="text-align: center;">
                    <span><b>Fecha Inicial:&nbsp;&nbsp;&nbsp;&nbsp;</b></span>
                    <asp:TextBox ID="txtFechaInicial" CssClass="datepicker-input"
                        onkeydown="return false;" runat="server" Font-Bold="True" Width="100px"
                        ToolTip="Fecha Inicial" TabIndex="1"></asp:TextBox>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            	    <span><b>Fecha Final:&nbsp;&nbsp;&nbsp;&nbsp;</b></span>
                    <asp:TextBox ID="txtFechaFinal" CssClass="datepicker-input"
                        onkeydown="return false;" runat="server" Font-Bold="True" Width="100px"
                        ToolTip="Fecha Inicial" TabIndex="2"></asp:TextBox>
                    <div class="clear"></div>
                </div>
            </asp:Panel>--%>
            <br />

            <%--  <asp:Button ID="btnMigrar" runat="server" Text="Migrar reporte ultimo mes?"
                CssClass="button button2" TabIndex="4" />--%>
        </div>
    </div>

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
        <div class="body">

            <asp:Panel ID="pnlDateRanges" runat="server" Visible="True">
                 <div class="st-form-line" style="text-align: center;">
                    <span><b>Fecha Inicial:&nbsp;&nbsp;&nbsp;&nbsp;</b></span>
                    <asp:TextBox ID="txtStartDate" type="Date"
                        onkeydown="return false;" runat="server" Font-Bold="True" Width="180px"
                        ToolTip="Fecha inicial" TabIndex="1"></asp:TextBox>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            	    <span><b>Fecha Final:&nbsp;&nbsp;&nbsp;&nbsp;</b></span>
                    <asp:TextBox ID="txtEndDate" type="Date"
                        onkeydown="return false;" runat="server" Font-Bold="True" Width="180px"
                        ToolTip="Fecha final" TabIndex="2"></asp:TextBox>
                    <div class="clear"></div>
                </div>
            </asp:Panel>

            <div class="st-form-line">
                <span class="st-labeltext"><b>Tipo de Reporte:</b></span>

                <asp:TextBox ID="txtReportType" CssClass="st-success-input" Style="width: 510px"
                    runat="server" TabIndex="1" MaxLength="100"
                    ToolTip="Tipo de Reporte." onkeydown="return (event.keyCode!=13);"
                    Enabled="False"></asp:TextBox>
                <div class="clear"></div>
            </div>

            <asp:Panel ID="pnlTerminals" runat="server" Visible="false">
                <div class="st-form-line">
                    <span class="st-labeltext"><b>Serial:</b></span>
                    <asp:DropDownList ID="ddlTerminals" class="btn btn-primary dropdown-toggle" runat="server"
                        DataSourceID="dsTerminals" DataTextField="ter_serial"
                        DataValueField="ter_id" Width="200px"
                        ToolTip="Seleccione la Terminal para el reporte." TabIndex="1" AutoPostBack="True">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="dsTerminals" runat="server"
                        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
                        SelectCommand="SELECT -1 AS ter_id, '- Todas las Terminales -' AS ter_serial UNION ALL SELECT ter_id, ter_serial FROM Terminal inner join grupo on Terminal.ter_grupo_id = grupo.gru_id WHERE grupo.gru_cliente_id = @customerID AND grupo.gru_tipo_android = @androidType">
                        <SelectParameters>
                            <asp:Parameter Name="customerID" Type="Int64" DefaultValue="-1" />
                            <asp:Parameter Name="androidType" Type="Boolean" DefaultValue="false" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <div class="clear"></div>
                </div>
            </asp:Panel>

            <asp:Panel ID="pnlGroupsTerminals" runat="server" Visible="false">
                <div class="st-form-line">
                    <span class="st-labeltext"><b>Grupos:</b></span>
                    <asp:DropDownList ID="ddlGroups" class="btn btn-info dropdown-toggle" runat="server"
                        DataSourceID="dsGroups" DataTextField="gru_nombre"
                        DataValueField="gru_id" Width="200px"
                        ToolTip="Seleccione el Grupo para el reporte." TabIndex="1" AutoPostBack="True">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="dsGroups" runat="server"
                        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
                        SelectCommand="SELECT -1 AS gru_id, '- Seleccione Grupo -' AS gru_nombre UNION ALL SELECT gru_id, gru_nombre FROM Grupo WHERE gru_cliente_id = @customerID ">
                        <SelectParameters>
                            <asp:Parameter Name="customerID" Type="Int64" DefaultValue="-1" />
                            <asp:Parameter Name="androidType" Type="Boolean" DefaultValue="false" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <div class="clear"></div>
                </div>
            </asp:Panel>

            <asp:Panel ID="pnlUsers" runat="server" Visible="false">
                <div class="st-form-line">
                    <span class="st-labeltext"><b>Usuarios:</b></span>
                    <asp:DropDownList ID="ddlUsers" class="uniform" runat="server"
                        DataSourceID="dsUsers" DataTextField="usu_nombre"
                        DataValueField="usu_id" Width="200px"
                        ToolTip="Seleccione el Usuario para el reporte." TabIndex="1">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="dsUsers" runat="server"
                        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
                        SelectCommand="SELECT -1 AS usu_id, '- Todos los Usuarios -' AS usu_nombre UNION ALL SELECT usu_id, usu_login + ' - ' + usu_nombre AS usu_nombre FROM Usuario "></asp:SqlDataSource>
                    <div class="clear"></div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlImei" runat="server" Visible="false">
                <div class="st-form-line">
                    <span class="st-labeltext"><b>Imei:</b></span>
                    <asp:DropDownList ID="ddlImei" class="btn btn-info dropdown-toggle" runat="server"
                        DataSourceID="dsImei" DataTextField="ter_imei"
                        DataValueField="ter_id" Width="200px"
                        ToolTip="Seleccione el Imei para el reporte." TabIndex="1" AutoPostBack="True">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="dsImei" runat="server"
                        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
                        SelectCommand="SELECT -1 AS ter_id, '- Todos los Imei -' AS ter_imei UNION ALL SELECT ter_id, ter_imei FROM Terminal inner join grupo on Terminal.ter_grupo_id = grupo.gru_id WHERE grupo.gru_cliente_id = @customerID AND grupo.gru_tipo_android = @androidType">
                        <SelectParameters>
                            <asp:Parameter Name="customerID" Type="Int64" DefaultValue="-1" />
                            <asp:Parameter Name="androidType" Type="Boolean" DefaultValue="false" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <div class="clear"></div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlTipos" runat="server" Visible="false">
                <div class="st-form-line">
                    <span class="st-labeltext"><b>Tipo De Reporte:</b></span>
                    <asp:DropDownList ID="ddlclientesestadisticasTipos" class="uniform" runat="server"
                        DataSourceID="dsclientesestadisticasTipos" DataTextField="descripcion"
                        DataValueField="tipo_id" Width="200px"
                        ToolTip="seleccione el tipo para el reporte." TabIndex="1" AutoPostBack="true">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="dsclientesestadisticasTipos" runat="server"
                        ConnectionString="<%$ connectionstrings:teleloaderconnectionstring %>"
                        SelectCommand="SELECT tipo_id, descripcion FROM ClientesEstadisticasTipos"></asp:SqlDataSource>
                    <div class="clear"></div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlTid" runat="server" Visible="false">
                <div class="st-form-line">
                    <span class="st-labeltext"><b>TID:</b></span>
                    <asp:DropDownList ID="ddlTidTrans" class="uniform" runat="server"
                        DataSourceID="dsTidTrans" DataTextField="fi_tid"
                        DataValueField="fi_tid" Width="200px"
                        ToolTip="Seleccione el TID para el reporte." TabIndex="1" AutoPostBack="True">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="dsTidTrans" runat="server"
                        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
                        SelectCommand="SELECT -1 AS  CT_ID,  '- Todos los TID -' AS fi_tid  UNION ALL SELECT CT_ID,fi_tid FROM EASYSOFT_TRANSACCIONES"></asp:SqlDataSource>
                    <div class="clear"></div>
                </div>
            </asp:Panel>

            <asp:Panel ID="pnlMerchantId" runat="server" Visible="false">
                <div class="st-form-line">
                    <span class="st-labeltext"><b>MERCHANT ID:</b></span>
                    <asp:DropDownList ID="ddlMerchantId" class="uniform" runat="server"
                        DataSourceID="dsMerchantTrans" DataTextField="fi_merchantId"
                        DataValueField="fi_merchantId" Width="200px"
                        ToolTip="Seleccione el MerchantId para el reporte." TabIndex="1" AutoPostBack="True">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="dsMerchantTrans" runat="server"
                        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
                        SelectCommand="SELECT -1 AS  CT_ID, '- Todos los MerchantId -' AS fi_merchantId UNION ALL SELECT CT_ID, fi_merchantId FROM EASYSOFT_TRANSACCIONES"></asp:SqlDataSource>
                    <div class="clear"></div>
                </div>
            </asp:Panel>

            <asp:Panel ID="pnlCodTransMod" runat="server" Visible="false">
                <div class="st-form-line">
                    <span class="st-labeltext"><b>Cod Trans Modificado:</b></span>
                    <asp:DropDownList ID="dllCodTransMod" class="uniform" runat="server"
                        DataSourceID="dsCodTransMod" DataTextField="bi_CodigoTransaccionModificado"
                        DataValueField="bi_CodigoTransaccionModificado" Width="200px"
                        ToolTip="Seleccione el MerchantId para el reporte." TabIndex="1" AutoPostBack="True">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="dsCodTransMod" runat="server"
                        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
                        SelectCommand="SELECT  -1 AS  CT_ID, '- Todos los Codigos -' AS bi_CodigoTransaccionModificado UNION ALL SELECT CT_ID, bi_CodigoTransaccionModificado FROM EASYSOFT_TRANSACCIONES"></asp:SqlDataSource>
                    <div class="clear"></div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlUsuario" runat="server" Visible="false">
                <div class="st-form-line">
                    <span class="st-labeltext"><b>Usuario:</b></span>
                    <asp:DropDownList ID="dllUsuario" class="uniform" runat="server"
                        DataSourceID="dsUsuario" DataTextField="bi_Usuario"
                        DataValueField="bi_Usuario" Width="200px"
                        ToolTip="Seleccione el MerchantId para el reporte." TabIndex="1" AutoPostBack="True">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="dsUsuario" runat="server"
                        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
                        SelectCommand="SELECT  -1 AS  CT_ID, '- Todos los Usuarios -' AS bi_Usuario UNION ALL SELECT CT_ID, bi_Usuario FROM EASYSOFT_TRANSACCIONES"></asp:SqlDataSource>
                    <div class="clear"></div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlTidHis" runat="server" Visible="false">
                <div class="st-form-line">
                    <span class="st-labeltext"><b>TID:</b></span>
                    <asp:DropDownList ID="ddlTidTransHis" class="uniform" runat="server"
                        DataSourceID="dsTidTransHis" DataTextField="fi_tid"
                        DataValueField="fi_tid" Width="200px"
                        ToolTip="Seleccione el TID para el reporte." TabIndex="1" AutoPostBack="True">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="dsTidTransHis" runat="server"
                        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
                        SelectCommand="SELECT -1 AS  CT_ID,  '- Todos los TID -' AS fi_tid  UNION ALL SELECT CT_ID,fi_tid FROM EASYSOFT_TRANSACCIONES_HISTORICO"></asp:SqlDataSource>
                    <div class="clear"></div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlMerchantIdHis" runat="server" Visible="false">
                <div class="st-form-line">
                    <span class="st-labeltext"><b>MERCHANT ID:</b></span>
                    <asp:DropDownList ID="ddlMerchantIdHis" class="uniform" runat="server"
                        DataSourceID="dsMerchantTransHis" DataTextField="fi_merchantId"
                        DataValueField="fi_merchantId" Width="200px"
                        ToolTip="Seleccione el MerchantId para el reporte." TabIndex="1" AutoPostBack="True">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="dsMerchantTransHis" runat="server"
                        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
                        SelectCommand="SELECT -1 AS  CT_ID, '- Todos los MerchantId -' AS fi_merchantId UNION ALL SELECT CT_ID, fi_merchantId FROM EASYSOFT_TRANSACCIONES_HISTORICO"></asp:SqlDataSource>
                    <div class="clear"></div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlCodTransModHis" runat="server" Visible="false">
                <div class="st-form-line">
                    <span class="st-labeltext"><b>Cod Trans Modificado:</b></span>
                    <asp:DropDownList ID="dllCodTransModHis" class="uniform" runat="server"
                        DataSourceID="dsCodTransModHis" DataTextField="bi_CodigoTransaccionModificado"
                        DataValueField="bi_CodigoTransaccionModificado" Width="200px"
                        ToolTip="Seleccione el MerchantId para el reporte." TabIndex="1" AutoPostBack="True">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="dsCodTransModHis" runat="server"
                        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
                        SelectCommand="SELECT  -1 AS  CT_ID, '- Todos los Codigos -' AS bi_CodigoTransaccionModificado UNION ALL SELECT CT_ID, bi_CodigoTransaccionModificado FROM EASYSOFT_TRANSACCIONES_HISTORICO"></asp:SqlDataSource>
                    <div class="clear"></div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlUsuarioHis" runat="server" Visible="false">
                <div class="st-form-line">
                    <span class="st-labeltext"><b>Usuario:</b></span>
                    <asp:DropDownList ID="dllUsuarioHis" class="uniform" runat="server"
                        DataSourceID="dsUsuarioHis" DataTextField="bi_Usuario"
                        DataValueField="bi_Usuario" Width="200px"
                        ToolTip="Seleccione el MerchantId para el reporte." TabIndex="1" AutoPostBack="True">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="dsUsuarioHis" runat="server"
                        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
                        SelectCommand="SELECT  -1 AS  CT_ID, '- Todos los Usuarios -' AS bi_Usuario UNION ALL SELECT CT_ID, bi_Usuario FROM EASYSOFT_TRANSACCIONES_HISTORICO"></asp:SqlDataSource>
                    <div class="clear"></div>
                </div>
            </asp:Panel>

            <asp:Panel ID="pnlSerialTerminal" runat="server" Visible="false">
                <div class="st-form-line">
                    <span class="st-labeltext"><b>Serial Terminal:</b></span>
                    <asp:TextBox ID="serialTerminal" CssClass="st-forminput" Style="width: 200px" runat="server" TabIndex="-1" MaxLength="10"
                        ToolTip="Digite el serial del equipo." onkeydown="return (event.keyCode!=13);" AutoPostBack="True" placeholder="todos los terminales"></asp:TextBox>

                    <asp:HiddenField ID="HiddenSerial" runat="server" />
                    <div class="clear"></div>
                </div>
            </asp:Panel>

            <div class="button-box">
                <asp:Button ID="btnGenerateReport" runat="server" Text="Generar Reporte"
                    CssClass="btn btn-info" TabIndex="4" />

            </div>
        </div>
    </div>
    <script type="text/javascript">
        if (document.getElementById("ctl00_MainContent_txtReportType").value === "Terminales Android") {
            document.getElementById("ctl00_MainContent_pnlTerminals").style.display = "none";
    </script>
    <script type="text/javascript">

            if (document.getElementById("ctl00_MainContent_txtReportType").value === "Terminales Por APK Descargado") {
                document.getElementById("ctl00_MainContent_pnlTerminals").style.display = "none";

            }
    </script>
    <asp:HyperLink ID="lnkBack" runat="server"
        NavigateUrl="~/Reports/Manager.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">
    <!-- Validator -->
    <script src="../js/ValidatorReport.js" type="text/javascript"></script>
    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>
    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

    <script type="text/javascript" language="javascript">
            var globalcontrol = '';

            function validateGenerateReport(control) {

                globalcontrol = control;

                $.msgAlert({
                    type: "warning"
                    , title: "Mensaje del sistema"
                    , text: "Realmente desea migrar a historico?"
                    , callback: function () {
                        __doPostBack($(globalcontrol).attr('name'), '');
                    }
                });

                return false;
            }

    </script>

</asp:Content>

