﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users
Imports System.Globalization
Imports System.Drawing
Imports System.Web.Services

Partial Class Reports_EntryPoint
    Inherits TeleLoader.Web.BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            txtStartDate.Text = Now.ToString("dd/MM/yyyy")
            txtEndDate.Text = Now.ToString("dd/MM/yyyy")
            txtReportType.Text = getReportName()
            setFormControls()

            If (objAccessToken.UserPerfilID = 15) Then
                txtStartDate.Enabled = True
            Else
                txtStartDate.Enabled = True
            End If
        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub


    Private Function getReportName() As String

        Dim retVal As String = ""

        Select Case objSessionParams.intReportType
            Case 54
                retVal = "Log de Auditoría"
            Case 140
                retVal = "Log de Inventario"
            Case 141
                retVal = "Log de Descarga"
            Case 142
                retVal = "Reporte de Terminales"
            Case 143
                retVal = "Estado Actualización de Terminales X Grupo"
                'Modificacion Estadisticas Pos; OG: 02/Jul/2018
            Case 150
                retVal = "Estado De Terminales"
            Case 151
                retVal = "Terminales Modo Bloqueante"
            Case 152
                retVal = "Terminales Por Prioridad De Grupo"
            Case 153
                retVal = "Estadistas Pos"
            Case 155
                retVal = "Consultas Por Terminal"
            Case 156
                retVal = "Descargas Por Terminal"
            Case 147
                retVal = "Log de MDM"
            Case 148
                retVal = "Terminales Android"
            Case 149
                'Modificación Carga de Llaves; EB: 02/Mar/2018
                retVal = "Log Carga de Llaves"
            Case 163
                retVal = "Reporte Transacciones"
            Case 164
                retVal = "Reporte Historico Transacciones"
            Case 167
                retVal = "Reporte Historico Transacciones"
            Case 168
                retVal = "Terminales Por APK Descargado"
        End Select

        Return retVal

    End Function

    Private Sub setFormControls()
        Select Case objSessionParams.intReportType
            Case 54
                pnlDateRanges.Visible = True
                pnlUsers.Visible = True
                pnlGroupsTerminals.Visible = False
                pnlTipos.Visible = False
                pnlImei.Visible = True

            Case 142
                pnlDateRanges.Visible = True
                pnlUsers.Visible = False

                dsGroups.SelectParameters("customerID").DefaultValue = objAccessToken.CustomerUserID
                dsTerminals.SelectParameters("customerID").DefaultValue = objAccessToken.CustomerUserID
                dsImei.SelectParameters("customerID").DefaultValue = objAccessToken.CustomerUserID

                If objSessionParams.intReportType = 147 Then
                    dsGroups.SelectParameters("androidType").DefaultValue = True
                    dsTerminals.SelectParameters("androidType").DefaultValue = True
                    dsImei.SelectParameters("androidType").DefaultValue = True
                End If
                pnlGroupsTerminals.Visible = True
                pnlTerminals.Visible = False
                pnlTipos.Visible = False
                pnlImei.Visible = False
                pnlSerialTerminal.Visible = True

            Case 140, 141, 147, 149, 167  'Modificación Carga de Llaves; EB: 02/Mar/2018
                pnlDateRanges.Visible = True
                pnlUsers.Visible = False

                dsGroups.SelectParameters("customerID").DefaultValue = objAccessToken.CustomerUserID
                dsTerminals.SelectParameters("customerID").DefaultValue = objAccessToken.CustomerUserID
                dsImei.SelectParameters("customerID").DefaultValue = objAccessToken.CustomerUserID

                'Modificación MDM; EB: 03/Feb/2018
                If objSessionParams.intReportType = 147 Then
                    dsGroups.SelectParameters("androidType").DefaultValue = True
                    dsTerminals.SelectParameters("androidType").DefaultValue = True
                    dsImei.SelectParameters("androidType").DefaultValue = True

                End If

                pnlGroupsTerminals.Visible = True
                pnlTerminals.Visible = False
                pnlTipos.Visible = False
                pnlImei.Visible = True
                pnlSerialTerminal.Visible = True
                'If serialTerminal.Text Is "" Then
                '    btnGenerateReport.Visible = False
                'End If

            Case 148
                pnlDateRanges.Visible = True
                pnlUsers.Visible = False

                dsGroups.SelectParameters("customerID").DefaultValue = objAccessToken.CustomerUserID
                dsTerminals.SelectParameters("customerID").DefaultValue = objAccessToken.CustomerUserID
                dsImei.SelectParameters("customerID").DefaultValue = objAccessToken.CustomerUserID

                'Modificación MDM; EB: 03/Feb/2018
                If objSessionParams.intReportType = 148 Then
                    dsGroups.SelectParameters("androidType").DefaultValue = True
                    dsTerminals.SelectParameters("androidType").DefaultValue = True
                    dsImei.SelectParameters("androidType").DefaultValue = True


                End If


                pnlGroupsTerminals.Visible = True
                pnlTerminals.Visible = True
                pnlTipos.Visible = False
                pnlImei.Visible = False


            Case 168
                pnlDateRanges.Visible = False
                pnlUsers.Visible = False

                dsGroups.SelectParameters("customerID").DefaultValue = objAccessToken.CustomerUserID
                dsTerminals.SelectParameters("customerID").DefaultValue = objAccessToken.CustomerUserID
                dsImei.SelectParameters("customerID").DefaultValue = objAccessToken.CustomerUserID

                'Modificación MDM; EB: 03/Feb/2018
                If objSessionParams.intReportType = 148 Then
                    dsGroups.SelectParameters("androidType").DefaultValue = True
                    dsTerminals.SelectParameters("androidType").DefaultValue = True
                    dsImei.SelectParameters("androidType").DefaultValue = True


                End If


                pnlGroupsTerminals.Visible = True
                pnlTerminals.Visible = False
                pnlTipos.Visible = False
                pnlImei.Visible = False

            Case 143, 150, 151, 152
                pnlDateRanges.Visible = True
                pnlUsers.Visible = False

                dsGroups.SelectParameters("customerID").DefaultValue = objAccessToken.CustomerUserID
                dsTerminals.SelectParameters("customerID").DefaultValue = objAccessToken.CustomerUserID
                'Modificación MDM; EB: 03/Feb/2018
                If objSessionParams.intReportType = 148 Then
                    dsGroups.SelectParameters("androidType").DefaultValue = True
                    dsTerminals.SelectParameters("androidType").DefaultValue = True
                End If

                pnlGroupsTerminals.Visible = True
                pnlTerminals.Visible = False
                pnlImei.Visible = False
            Case 153
                pnlDateRanges.Visible = False
                pnlUsers.Visible = False

                dsTerminals.SelectParameters("customerID").DefaultValue = objAccessToken.CustomerUserID
                dsImei.SelectParameters("customerID").DefaultValue = objAccessToken.CustomerUserID

                'Modificación MDM; EB: 03/Feb/2018
                If objSessionParams.intReportType = 148 Then
                    'dsGroups.SelectParameters("androidType").DefaultValue = True
                    dsTerminals.SelectParameters("androidType").DefaultValue = True
                    dsImei.SelectParameters("androidType").DefaultValue = True


                End If

                pnlGroupsTerminals.Visible = True
                pnlTerminals.Visible = True
                pnlTipos.Visible = True
                pnlImei.Visible = True
            Case 155, 156
                pnlDateRanges.Visible = False
                pnlUsers.Visible = False


                dsTerminals.SelectParameters("customerID").DefaultValue = objAccessToken.CustomerUserID
                'Modificación MDM; EB: 03/Feb/2018

                pnlGroupsTerminals.Visible = True
                pnlTerminals.Visible = True
                pnlTipos.Visible = False
                pnlImei.Visible = False

            Case 163
                pnlDateRanges.Visible = True
                pnlTid.Visible = True
                pnlMerchantId.Visible = True
                pnlCodTransMod.Visible = True
                pnlUsuario.Visible = True

            Case 164
                pnlDateRanges.Visible = True
                pnlTidHis.Visible = True
                pnlUsuarioHis.Visible = True
                pnlMerchantIdHis.Visible = True
                pnlCodTransModHis.Visible = True
        End Select
    End Sub
    'Protected Sub btnMigrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMigrar.Click

    '    objSessionParams.dateFechaInicial = Date.Parse(txtFechaInicial.Text, New CultureInfo("es-CO"))
    '    objSessionParams.dateFechaFinal = Date.Parse(txtFechaFinal.Text, New CultureInfo("es-CO"))
    '    MigrarHistorico(txtReportType.Text)
    'End Sub

    ''' <summary>
    ''' Valida la terminal que notifica la carga de llave
    ''' </summary>
    Private Function MigrarHistorico(ByVal tipo As String) As String
        Dim configurationSection As ConnectionStringsSection =
        System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim connection As New SqlConnection(configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString)
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim status As String
        Dim retVal As Boolean

        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webMigrarReporte"
            command.Parameters.Clear()
            command.Parameters.Add(New SqlParameter("tipo", tipo))
            command.Parameters.Add(New SqlParameter("startDate", objSessionParams.dateFechaInicial))
            command.Parameters.Add(New SqlParameter("endDate", objSessionParams.dateFechaFinal))


            'Ejecutar SP
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    status = results.GetString(0)
                End While
            Else
                retVal = False
            End If

            If status = 1 Then
                retVal = True
                pnlMsg.Visible = True
                pnlError.Visible = False
                lblMsg.Text = "Datos migrados exitosamente!!"
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try

        Return retVal

    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Dim startDate As Date
        Dim endDate As Date

        If txtStartDate.Text IsNot "" And txtEndDate.Text IsNot "" Then
            startDate = Date.Parse(txtStartDate.Text, New CultureInfo("es-CO"))
            endDate = Date.Parse(txtEndDate.Text, New CultureInfo("es-CO"))

            'Validate Data
            If startDate <= endDate Then
                'Calculate Difference in Days

                If DateDiff(DateInterval.Day, startDate, endDate) < 181 Then
                    'Generate Report
                    objSessionParams.dateStartDate = startDate
                    objSessionParams.dateEndDate = endDate
                    objSessionParams.strReportName = txtReportType.Text


                    If pnlUsers.Visible = True Then
                        objSessionParams.intSelectedUserID = ddlUsers.SelectedValue
                        objSessionParams.strSelectedUserName = ddlUsers.SelectedItem.Text
                    End If

                    If pnlGroupsTerminals.Visible = True Then
                        objSessionParams.intSelectedGroupID = ddlGroups.SelectedValue
                        objSessionParams.strSelectedGroupName = ddlGroups.SelectedItem.Text
                    End If

                    If pnlTerminals.Visible = True And ddlTerminals.Enabled = True Then

                        objSessionParams.intSelectedTerminalID = ddlTerminals.SelectedValue
                        objSessionParams.strSelectedTerminalSerial = ddlTerminals.SelectedItem.Text

                    End If

                    If pnlImei.Visible = True And ddlImei.Enabled = True Then
                        objSessionParams.intSelectedTerminalID = ddlImei.SelectedValue
                        objSessionParams.srtSelectTerminalImeiName = ddlImei.SelectedItem.Text

                    End If

                    If pnlTipos.Visible = True Then
                        objSessionParams.intSelectedTipoID = ddlclientesestadisticasTipos.SelectedValue
                        objSessionParams.strSelectedTipoName = ddlclientesestadisticasTipos.SelectedItem.Text
                    End If
                    If pnlTid.Visible = True Then
                        objSessionParams.intSelectTID = ddlTidTrans.SelectedValue

                    End If
                    If pnlMerchantId.Visible = True Then
                        objSessionParams.intSelectedMerchantID = ddlMerchantId.SelectedValue

                    End If
                    If pnlCodTransMod.Visible = True Then
                        objSessionParams.CodeTranMode = dllCodTransMod.SelectedValue

                    End If
                    If pnlUsuario.Visible = True Then
                        objSessionParams.UsuarioID = dllUsuario.SelectedValue

                    End If

                    If pnlSerialTerminal.Visible = True Then
                        If serialTerminal.Text = "" Then
                            objSessionParams.strSerialTerminal = "-1"
                        Else
                            objSessionParams.strSerialTerminal = serialTerminal.Text
                        End If

                    End If


                    'Reporte Transsacciones Historico

                    If pnlTidHis.Visible = True Then
                        objSessionParams.intSelectTIDHis = ddlTidTransHis.SelectedValue

                    End If

                    If pnlMerchantIdHis.Visible = True Then
                        objSessionParams.intSelectedMerchantIDHis = ddlMerchantIdHis.SelectedValue

                    End If

                    If pnlCodTransModHis.Visible = True Then
                        objSessionParams.CodeTranModeHis = dllCodTransModHis.SelectedValue

                    End If

                    If pnlUsuarioHis.Visible = True Then
                        objSessionParams.UsuarioIDHis = dllUsuarioHis.SelectedValue
                    End If

                End If

                'Set Data into Session
                Session("SessionParameters") = objSessionParams

                'Loading Window
                Select Case objSessionParams.intReportType
                    Case 54, 140, 141, 142, 147, 148, 149, 163, 164, 167, 168 'Modificación Carga de Llaves; EB: 02/Mar/2018
                        Dim url As String = "ReportViewer.aspx"
                        Response.Redirect("~/Reports/Loading.Aspx?Page=" & url, "False")
                    Case 143
                        'Redirect to Pie Chart Report
                        Response.Redirect("ChartReport.aspx", False)
                    Case 150
                        'Redirect to Pie Chart Report
                        Response.Redirect("ChartReportCopia.aspx", False)
                    Case 151
                        'Redirect to Pie Chart Report
                        Response.Redirect("ChartReport2.aspx", False)
                    Case 154
                        'Redirect to Pie Chart Report
                        Response.Redirect("ChartReport3.aspx", False)
                    Case 153
                        'Redirect to Pie Chart Report
                        Response.Redirect("ChartReport4.aspx", False)
                    Case 155
                        'Redirect To Pie Chart Report
                        Response.Redirect("ChartReport5.aspx", False)
                    Case 156
                        'Redirect Ti Pie Chart Report
                        Response.Redirect("ChartReport6.aspx", False)


                End Select
            Else
                pnlError.Visible = True
                pnlMsg.Visible = False
                lblError.Text = "El rango del reporte no debe superar los 60 días."
            End If

        Else
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Por favor diligenciar los periodos de fecha donde desea generar el reporte"
        End If



    End Sub


    Protected Sub ddlGroups_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGroups.SelectedIndexChanged
        If ddlGroups.SelectedValue <> "-1" Then
            ddlTerminals.Enabled = False
            serialTerminal.Enabled = False
            btnGenerateReport.Visible = True
        Else
            ddlTerminals.Enabled = True
            serialTerminal.Enabled = True
            'btnGenerateReport.Visible = False
        End If
        If ddlGroups.SelectedValue <> "-1" Then
            ddlImei.Enabled = False
            btnGenerateReport.Visible = True
        Else
            ddlImei.Enabled = True
        End If
    End Sub
    Protected Sub ddlTerminals_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTerminals.SelectedIndexChanged
        If ddlTerminals.SelectedValue <> "-1" Then

            ddlGroups.Enabled = False
        Else
            ddlGroups.Enabled = True

        End If
        If ddlTerminals.SelectedValue <> "-1" Then
            ddlImei.Enabled = False
        Else
            ddlImei.Enabled = True

        End If
    End Sub
    Protected Sub ddlImei_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlImei.SelectedIndexChanged
        If ddlImei.SelectedValue <> "-1" Then
            ddlTerminals.Enabled = False
        Else
            ddlTerminals.Enabled = True
        End If
        If ddlImei.SelectedValue <> "-1" Then
            ddlGroups.Enabled = False
            serialTerminal.Enabled = False
        Else
            ddlGroups.Enabled = True
            serialTerminal.Enabled = True
        End If
    End Sub

    'Reporte Transacciones ProduBanco
    Protected Sub ddlTidTrans_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTidTrans.SelectedIndexChanged
        If ddlTidTrans.SelectedValue <> "- Todos los TID -" Then
            ddlMerchantId.Enabled = False
        Else
            ddlMerchantId.Enabled = True
        End If
        If ddlTidTrans.SelectedValue <> "- Todos los TID -" Then
            dllCodTransMod.Enabled = False
        Else
            dllCodTransMod.Enabled = True
        End If
        If ddlTidTrans.SelectedValue <> "- Todos los TID -" Then
            dllUsuario.Enabled = False
        Else
            dllUsuario.Enabled = True

        End If
    End Sub
    Protected Sub ddlMerchantId_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlMerchantId.SelectedIndexChanged
        If ddlMerchantId.SelectedValue <> "- Todos los Merchantid -" Then
            ddlTidTrans.Enabled = False
        Else
            ddlTidTrans.Enabled = True
        End If
        If ddlMerchantId.SelectedValue <> "- Todos los Merchantid -" Then
            dllCodTransMod.Enabled = False
        Else
            dllCodTransMod.Enabled = True
        End If
        If ddlMerchantId.SelectedValue <> "- Todos los Merchantid -" Then
            dllUsuario.Enabled = False
        Else
            dllUsuario.Enabled = True

        End If
    End Sub
    Protected Sub dllCodTransMod_SelectedIndexChanged(sender As Object, e As EventArgs) Handles dllCodTransMod.SelectedIndexChanged
        If dllCodTransMod.SelectedValue <> "- Todos los Codigos -" Then
            ddlTidTrans.Enabled = False
        Else
            ddlTidTrans.Enabled = True
        End If
        If dllCodTransMod.SelectedValue <> "- Todos los Codigos -" Then
            ddlMerchantId.Enabled = False
        Else
            ddlMerchantId.Enabled = True
        End If
        If dllCodTransMod.SelectedValue <> "- Todos los Codigos -" Then
            dllUsuario.Enabled = False
        Else
            dllUsuario.Enabled = True

        End If
    End Sub
    Protected Sub dllUsuario_SelectedIndexChanged(sender As Object, e As EventArgs) Handles dllUsuario.SelectedIndexChanged
        If dllUsuario.SelectedValue <> "- Todos los Usuarios -" Then
            ddlTidTrans.Enabled = False
        Else
            ddlTidTrans.Enabled = True
        End If
        If dllUsuario.SelectedValue <> "- Todos los Usuarios -" Then
            ddlMerchantId.Enabled = False
        Else
            ddlMerchantId.Enabled = True
        End If
        If dllUsuario.SelectedValue <> "- Todos los Usuarios -" Then
            dllCodTransMod.Enabled = False
        Else
            dllCodTransMod.Enabled = True

        End If
    End Sub

    'Reporte Transacciones Historicas ProduBanco

    Protected Sub ddlTidTransHis_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTidTransHis.SelectedIndexChanged
        If ddlTidTransHis.SelectedValue <> "- Todos los TID -" Then
            ddlMerchantIdHis.Enabled = False
        Else
            ddlMerchantIdHis.Enabled = True
        End If
        If ddlTidTransHis.SelectedValue <> "- Todos los TID -" Then
            dllCodTransModHis.Enabled = False
        Else
            dllCodTransModHis.Enabled = True
        End If
        If ddlTidTransHis.SelectedValue <> "- Todos los TID -" Then
            dllUsuarioHis.Enabled = False
        Else
            dllUsuarioHis.Enabled = True

        End If
    End Sub

    Protected Sub ddlMerchantIdHis_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlMerchantIdHis.SelectedIndexChanged
        If ddlMerchantIdHis.SelectedValue <> "- Todos los Merchantid -" Then
            ddlTidTransHis.Enabled = False
        Else
            ddlTidTransHis.Enabled = True
        End If
        If ddlMerchantIdHis.SelectedValue <> "- Todos los Merchantid -" Then
            dllCodTransModHis.Enabled = False
        Else
            dllCodTransModHis.Enabled = True
        End If
        If ddlMerchantIdHis.SelectedValue <> "- Todos los Merchantid -" Then
            dllUsuarioHis.Enabled = False
        Else
            dllUsuarioHis.Enabled = True

        End If
    End Sub

    Protected Sub dllCodTransModHis_SelectedIndexChanged(sender As Object, e As EventArgs) Handles dllCodTransModHis.SelectedIndexChanged
        If dllCodTransModHis.SelectedValue <> "- Todos los Codigos -" Then
            ddlTidTransHis.Enabled = False
        Else
            ddlTidTransHis.Enabled = True
        End If
        If dllCodTransModHis.SelectedValue <> "- Todos los Codigos -" Then
            ddlMerchantIdHis.Enabled = False
        Else
            ddlMerchantIdHis.Enabled = True
        End If
        If dllCodTransModHis.SelectedValue <> "- Todos los Codigos -" Then
            dllUsuarioHis.Enabled = False
        Else
            dllUsuarioHis.Enabled = True

        End If
    End Sub


    Protected Sub dllUsuarioHis_SelectedIndexChanged(sender As Object, e As EventArgs) Handles dllUsuarioHis.SelectedIndexChanged
        If dllUsuarioHis.SelectedValue <> "- Todos los Usuarios -" Then
            ddlTidTransHis.Enabled = False
        Else
            ddlTidTransHis.Enabled = True
        End If
        If dllUsuarioHis.SelectedValue <> "- Todos los Usuarios -" Then
            ddlMerchantIdHis.Enabled = False
        Else
            ddlMerchantIdHis.Enabled = True
        End If
        If dllUsuarioHis.SelectedValue <> "- Todos los Usuarios -" Then
            dllCodTransModHis.Enabled = False
        Else
            dllCodTransModHis.Enabled = True

        End If
    End Sub

    Private Sub serialTerminal_TextChanged(sender As Object, e As EventArgs) Handles serialTerminal.TextChanged
        If serialTerminal.Text <> "" Then
            ddlGroups.Enabled = False
            btnGenerateReport.Visible = True
        Else
            ddlGroups.Enabled = True
            If txtReportType.Text = "Log de Descarga" Or txtReportType.Text = "Log de Inventario" Then
                'btnGenerateReport.Visible = False
            End If
        End If
    End Sub

    <WebMethod()>
    Public Shared Function GetSerials(prefix As String) As String()
        Dim customers As New List(Of String)()
        Using conn As New SqlConnection()
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("TeleLoaderConnectionString").ConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = "SELECT TOP 15 TER_SERIAL,TER_ID FROM TERMINAL WHERE TER_SERIAL like @SearchText + '%'"
                cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        customers.Add(String.Format("{0}-{1}", sdr("TER_SERIAL"), sdr("TER_ID")))
                    End While
                End Using
                conn.Close()
            End Using
        End Using
        Return customers.ToArray()
    End Function
End Class
