﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Estatistics.aspx.vb" Inherits="Reports_Estadisticas" EnableEventValidation="false" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%>:: Reporte Estado Actualización
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>
    <%-- Agregamos Morris para Graficas CSS O'G --%>
    <link rel="Stylesheet" href="../libs/morris.js-0.5.1/morris.css" />
    <%-- Agregamos Morris para Graficas J query O'G --%>
    <script type="text/javascript" src="../libs/morris.js-0.5.1/morris.min.js" charset="utf-8"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    <script type="text/javascript" src="../js/jquery-settings.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

    <style>
        body {
            background-color: cornsilk;
        }
    </style>
    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

    <meta http-equiv="refresh" content="" />
    <!-- Google Js Api / Chart and others -->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

    <!-- charts Related JS -->
    <script type="text/javascript" src="../js/raphael.js"></script>
    <script type="text/javascript" src="../js/analytics.js"></script>
    <script type="text/javascript" src="../js/popup.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed"
            PostBackUrl="~/Reports/Manager.aspx">Reportes</asp:LinkButton>
    </li>
    <li>DashBoard</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <br />
    <div class="simplebox">

        <style>
            /* Remove the navbar's default margin-bottom and rounded borders */
            .navbar {
                margin-bottom: 0;
                border-radius: 0;
            }

            /* Add a gray background color and some padding to the footer */
            footer {
                background-color: #f2f2f2;
                padding: 25px;
            }

            .carousel-inner img {
                width: 100%; /* Set width to 100% */
                margin: auto;
                min-height: 200px;
            }

            /* Hide the carousel text when the screen is less than 600 pixels wide */
            @media (max-width: 600px) {
                .carousel-caption {
                    display: none;
                }
            }
        </style>
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="https://media-exp1.licdn.com/dms/image/C4D1BAQGPOI71w_n_bQ/company-background_10000/0?e=2159024400&v=beta&t=SyUTSmlTYPH4lv5xr29XnRR1zu46NJbOHSYRjC5jM_o" alt="Image">
                    <div class="carousel-caption">
                    </div>
                </div>

                <h3><%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%></h3>
                <p>Plataforma Administrativa Polaris Cloud Service</p>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <!-- start pie chart javascript codes -->
        <script type="text/javascript">

            google.load('visualization', '1.0', { 'packages': ['corechart'] });
            google.setOnLoadCallback(drawCharts2);

            function drawCharts2() {

                var data = google.visualization.arrayToDataTable(dataNet);


                var options = {
                    'width': 955,
                    'height': 300,
                    'chartArea': { 'width': '100%', 'height': '100%' },
                    'legend': 'none',
                    'pieSliceText': 'label',
                    'colors': ['#2E9AFE', '#FA5858'],
                    'is3D': true
                };

                var options2 = {
                    'width': 955,
                    'height': 300,
                    'chartArea': { 'width': '100%', 'height': '100%' },
                    'legend': 'none',
                    'is3D': true
                };

                var chart = new google.visualization.PieChart(document.getElementById('terminals_chart_div'));
                chart.draw(data, options);
        </script>
        <!-- end pie chart javascript codes -->
        <asp:Panel ID="pnlCharts" runat="server">
            <!-- start chart div -->
            <br />
            <h2 style="text-align: center;"></h2>
            <div id="terminals_chart_div" style="text-align: center;"></div>
            <br />
            <h2 style="text-align: center;"></h2>
            <div id="terminalsModel_chart_div" style="text-align: center;"></div>
            <!-- end chart div -->
        </asp:Panel>
    </div>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
                google.charts.load("current", { packages: ["corechart"] });
                google.charts.setOnLoadCallback(drawChart);
                function drawChart() {
                    var data = google.visualization.arrayToDataTable(dataNet);

                    var options = {
                        title: '',
                        pieHole: 0.4,
                        is3D: true,
                    };

                    var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
                    chart.draw(data, options);
                }
    </script>

    <%--<div id="donutchart" style="width: 900px; height: 500px;"></div>--%>

    <div id="chartContainer3" class="container" style="height: 500px; width: 90%;">
    </div>
    <div id="chartContainer2" class="container" style="height: 500px; width: 90%;">
    </div>

    <div class="container">
        <h1></h1>
        <hr />
        <div class="row">

            <div class="col-md-6">
                <hr />
                <div id="chartContainer5" style="height: 450px; width: 600px;"></div>
            </div>
            <div class="col-md-6">
                <hr />
                <div id="chartContainer7" style="height: 450px; width: 600px;"></div>
            </div>
            <div class="col-md-6">
                <hr />
                <div id="chartContainer8" style="height: 450px; width: 1170px;"></div>
            </div>
        </div>
    </div>
    <br />
    <br />
    <br />
    <div class="jumbotron text-center">
        <h1>Geolocalización de Terminales</h1>
        <p>Seleccione el grupo: </p>
    </div>
    <br />
    <div class="toggle-message" style="z-index: 590; top: 0px; left: 0px;">
        <h3 class="title">
            <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
        <div class="st-form-line">
            <span class="st-labeltext"><b>Grupo: </b></span>
            <asp:DropDownList ID="ddlUserList" class="btn btn-primary dropdown-toggle" runat="server"
                DataSourceID="dsUserList" DataTextField="Gru_nombre"
                DataValueField="Gru_id" Width="200px"
                ToolTip="Seleccione el Grupo: " TabIndex="0" AutoPostBack="false">
            </asp:DropDownList>
            <asp:SqlDataSource ID="dsUserList" runat="server"
                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" SelectCommand="SELECT -1 AS Gru_id, '            ' AS Gru_nombre
                        UNION ALL
                        SELECT Gru_id, Gru_nombre FROM Grupo "></asp:SqlDataSource>

            <asp:Button ID="btnConsultar" runat="server" Text="Filtrar Grupos"
                CssClass="btn btn-info" ToolTip="Filtrar Grupo..." />
        </div>
    </div>
    <div id="posApps" style="overflow: auto; width: 1360px; height: 500px;">
        <asp:GridView ID="grdTerminals" runat="server"
            AllowSorting="True" AutoGenerateColumns="False"
            DataSourceID="dsTerminals" CssClass="table table-responsive-lg jumbotron table-light table-hover table-bordered" HeaderStyle-ForeColor="WhiteSmoke"
            HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
            HeaderStyle-CssClass="table-bordered bg-info" DataKeyNames="ter_id, ter_serial"
            EmptyDataText="No existen Terminales creadas en el Grupo o con el filtro aplicado"
            HorizontalAlign="Center" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
            <AlternatingRowStyle Wrap="True" />
            <Columns>
                <asp:TemplateField HeaderText=" " SortExpression="ter_logo" HeaderStyle-Width="20px">
                    <ItemTemplate>
                        <asp:Image ID="imgUrlIcon" runat="server" ImageUrl='<%# Bind("ter_logo") %>' ToolTip='<%# Bind("ter_fecha_actualizacion_auto") %>' />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:BoundField DataField="ter_serial" HeaderText="Serial"
                    SortExpression="ter_serial" />
                <asp:BoundField DataField="ter_modelo" HeaderText="Modelo"
                    SortExpression="ter_modelo" />
                <asp:BoundField DataField="ter_imei" HeaderText="IMEI"
                    SortExpression="ter_imei" />
                <asp:TemplateField HeaderText="Prioridad Grupo" SortExpression="ter_prioridad_grupo">
                    <ItemTemplate>
                        <asp:CheckBox ID="editChk1" runat="server" Checked='<%# Bind("ter_prioridad_grupo")%>' Enabled="false" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Actualizar Manual" SortExpression="ter_actualizar">
                    <ItemTemplate>
                        <asp:CheckBox ID="editChk2" runat="server" Checked='<%# Bind("ter_actualizar")%>' Enabled="false" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:BoundField DataField="ter_ip_descarga" HeaderText="IP Descarga"
                    SortExpression="ter_ip_descarga" />
                <asp:BoundField DataField="ter_puerto_descarga" HeaderText="Puerto Descarga"
                    SortExpression="ter_puerto_descarga" />
                <asp:BoundField DataField="ter_estado_actualizacion" HeaderText="Estado Actualización"
                    SortExpression="ter_estado_actualizacion" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="ter_fecha_ultima_inicializacion" HeaderText="Ultima Inicializacion"
                    SortExpression="ter_fecha_ultima_inicializacion" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="ter_flag_inicializacion" HeaderText="Autoinicializacion"
                    SortExpression="ter_flag_inicializacion" ItemStyle-HorizontalAlign="Center" />
                <asp:TemplateField HeaderText="Estado" SortExpression="ter_estado">
                    <ItemTemplate>
                        <asp:CheckBox ID="editChk3" runat="server" Checked='<%# Bind("ter_estado")%>' Enabled="false" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False" HeaderStyle-Width="50px">
                    <ItemTemplate>
                        <asp:ImageButton ID="imgShowMap" runat="server" CausesValidation="False"
                            CommandName="ShowTerminalMap" ImageUrl="~/img/icons/16x16/location.png" Text="Ubicación"
                            ToolTip="Mostrar Ubicación en Mapa" CommandArgument='<%# grdTerminals.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>

            </Columns>
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
                CssClass="pgr" Font-Underline="False" />
            <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid"
                BorderWidth="1px" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>
        <br />
        <asp:SqlDataSource ID="dsTerminals" runat="server"
            ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
            SelectCommand="sp_webConsultarTerminalesGrupo" SelectCommandType="StoredProcedure"
            DeleteCommand="sp_webEliminarTerminalXGrupo" DeleteCommandType="StoredProcedure" ConflictDetection="OverwriteChanges">
            <SelectParameters>
                <asp:Parameter Name="groupId" Type="String" />
                <asp:Parameter Name="terminalSerial" Type="String" DefaultValue="-1" />
                <asp:Parameter Name="terminalImei" Type="String" DefaultValue="-1" />
            </SelectParameters>
            <DeleteParameters>
                <asp:Parameter Name="terminalId" Type="String" />
                <asp:Parameter Name="groupId" Type="String" />
            </DeleteParameters>
        </asp:SqlDataSource>
    </div>

    <%--Administracion de grupos--%>

    <div class="jumbotron text-center">
        <h1>Administración de grupos</h1>
        <p>Seleccione el grupo: </p>
    </div>
    <br />
    <div id="posApps1" style="overflow: auto; width: 1360px; height: 500px;">

        <asp:GridView ID="GrdAdmin" runat="server"
            AllowSorting="True" AutoGenerateColumns="False"
            DataSourceID="DsAdmin"
            CssClass="table table-responsive-lg jumbotron table-light table-hover table-bordered" HeaderStyle-ForeColor="WhiteSmoke"
            HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
            HeaderStyle-CssClass="table-bordered bg-info" DataKeyNames="gru_id,gru_nombre"
            HorizontalAlign="Center" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
            <AlternatingRowStyle Wrap="True" />

            <Columns>
                <asp:BoundField DataField="cli_nombre" HeaderText="Nombre del Cliente"
                    SortExpression="cli_nombre" />
                <asp:BoundField DataField="gru_nombre" HeaderText="Nombre del Grupo"
                    SortExpression="gru_nombre" />

                <asp:BoundField DataField="gru_total_terminales" HeaderText="Total Dispositivos"
                    SortExpression="gru_total_terminales" ItemStyle-HorizontalAlign="Center" />
                <asp:TemplateField HeaderText="Actualizar" SortExpression="gru_actualizar">
                    <ItemTemplate>
                        <asp:CheckBox ID="editChk" runat="server" Checked='<%# Bind("gru_actualizar")%>' Enabled="false" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Programar Fecha" SortExpression="gru_programar_fecha">
                    <ItemTemplate>
                        <asp:CheckBox ID="editChk2" runat="server" Checked='<%# Bind("gru_programar_fecha")%>' Enabled="false" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:BoundField DataField="gru_fecha_programada_actualizacion_ini" HeaderText="Rango Fecha/Hora Inicial"
                    SortExpression="gru_fecha_programada_actualizacion_ini" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="gru_fecha_programada_actualizacion_fin" HeaderText="Rango Fecha/Hora Final"
                    SortExpression="gru_fecha_programada_actualizacion_fin" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="gru_rango_hora_actualizacion_ini" HeaderText="Rango Hora Inicial"
                    SortExpression="gru_rango_hora_actualizacion_ini" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="gru_rango_hora_actualizacion_fin" HeaderText="Rango Hora Final"
                    SortExpression="gru_rango_hora_actualizacion_fin" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="gru_ip_descarga" HeaderText="IP Descarga"
                    SortExpression="gru_ip_descarga" />
                <asp:BoundField DataField="gru_puerto_descarga" HeaderText="Puerto Descarga"
                    SortExpression="gru_puerto_descarga" />

                <asp:TemplateField ShowHeader="False" HeaderStyle-Width="45px">
                    <ItemTemplate>
                        <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False"
                            CommandName="EditGroup" ImageUrl="~/img/icons/16x16/gear.png" Text="Editar"
                            ToolTip="Editar Grupo" CommandArgument='<%# GrdAdmin.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="aqua" />
            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
                CssClass="pgr" Font-Underline="False" />
            <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#999999" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>
        <asp:SqlDataSource ID="DsAdmin" runat="server"
            ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
            SelectCommand="sp_webConsultarGrupos" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:Parameter Name="customerID" Type="Int64" DefaultValue="-1" />
                <asp:Parameter Name="groupName" Type="String" DefaultValue="-1" />
                <asp:Parameter Name="groupDesc" Type="String" DefaultValue="-1" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>

    <div class="jumbotron text-center">
        <h1>Descargas por terminal</h1>
        <p>Seleccione el terminal: </p>
    </div>
    <br />

    <div class="toggle-message" style="z-index: 590; top: 0px; left: 0px;">
        <h3 class="title">
            <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
        <div class="st-form-line">
            <span class="st-labeltext"><b>Terminales</b></span>
            <asp:DropDownList ID="ddlTerminalesPos" class="btn btn-primary dropdown-toggle" runat="server"
                DataSourceID="dsTerminalPos" DataTextField="ter_serial"
                DataValueField="ter_id" Width="200px"
                ToolTip="Seleccione el Grupo: " TabIndex="0" AutoPostBack="false">
            </asp:DropDownList>
            <asp:SqlDataSource ID="dsTerminalPos" runat="server"
                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" SelectCommand="SELECT ter_id, ter_serial FROM terminal "></asp:SqlDataSource>
            <span class="st-labeltext"><b>: </b></span>
            <asp:DropDownList ID="ddltipos" class="btn btn-primary dropdown-toggle" runat="server"
                DataSourceID="dstipos" DataTextField="descripcion"
                DataValueField="tipo_id" Width="200px"
                ToolTip="Seleccione el tipo: " TabIndex="0" AutoPostBack="false">
            </asp:DropDownList>
            <asp:SqlDataSource ID="dstipos" runat="server"
                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" SelectCommand="SELECT -1 AS tipo_id, '            ' AS descripcion
                        UNION ALL
                        SELECT tipo_id, descripcion FROM ClientesEstadisticasTipos "></asp:SqlDataSource>
            <asp:Button ID="btnChart" runat="server" Text="Filtrar"
                CssClass="btn btn-info" TabIndex="4" ToolTip="Filtrar..." />

        </div>
    </div>
    <div class="clear"></div>
    <div class="container text-center">
        <h3>Datos Terminal:</h3>
        <br>
        <div class="row">
            <div class="col-sm-3">
                <img src="https://image.freepik.com/vector-gratis/pase-tarjeta-credito-usando-terminal-tarjeta-credito_7496-730.jpg" class="img-responsive" style="width: 100%" alt="Image">
                <asp:Label ID="SerialPos" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-sm-3">
                <img src="https://us.123rf.com/450wm/axsimen/axsimen1612/axsimen161200012/69138781-sin-contacto-de-compra-de-pago-del-icono-del-vector-en-un-estilo-plano-pago-bancario-inal%C3%A1mbrico-media.jpg?ver=6" class="img-responsive" style="width: 100%" alt="Image">
                <asp:Label ID="imei" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-sm-3">
                <div class="well">
                    <asp:Label ID="SerialSim" runat="server" Text=""></asp:Label>
                </div>
                <div class="well">
                    <asp:Label ID="GrupoEstatistic" runat="server" Text=""></asp:Label>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="well">
                    <asp:Label ID="Comunicacion" runat="server" Text=""></asp:Label>
                </div>
                <div class="well">
                    <asp:Label ID="Modelo" runat="server" Text=""></asp:Label>
                </div>
            </div>
        </div>
        <hr>
    </div>
    <asp:Panel ID="PnlDescargas" runat="server" Visible="true">
        <!-- start chart div -->
        <br />
        <h2 style="text-align: center;"></h2>
        <div id="Descargas_chart_div" style="text-align: center;"></div>
        <br />
    </asp:Panel>


    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

                google.charts.load('current', { 'packages': ['corechart'] });

                google.charts.setOnLoadCallback(drawChart);
                function drawChart() {
                    var data = google.visualization.arrayToDataTable(dataNet3);

                    document.getElementById("error_msg").innerHTML = "GRAFICA GENERADA DE FORMA CORRECTA, SE MOSTRARAN LOS ULTIMOS 40 DIAS DE ACTIVIDAD"

                    var options = {
                        'title': '',
                        'width': 1350,
                        'height': 500
                    };

                    var chart = new google.visualization.LineChart(document.getElementById('Descargas_chart_div'));
                    google.visualization.events.addListener(chart, 'error', function (googleError) {
                        google.visualization.errors.removeError(googleError.id);
                        document.getElementById("error_msg").innerHTML = "NO HAY DATOS SELECCIONE OTRO TERMINAL";

                    });
                    chart.draw(data, options);
                }
    </script>
    <div id="error_msg" style="width: 1053px; height: 70px; background-color: #1ABC9C; color: white; text-align: center; line-height: 50px; float: none; z-index: auto; position: static;">
    </div>


    <div class="jumbotron text-center">
        <h1>Carga de llaves</h1>
        <p>Seleccione el Grupo: </p>
    </div>

    <div class="toggle-message" style="z-index: 590; top: 0px; left: 0px;">
        <h3 class="title">
            <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
        <div class="st-form-line">
            <span class="st-labeltext"><b>Grupo: </b></span>
            <asp:DropDownList ID="ddlkey" class="btn btn-primary dropdown-toggle" runat="server"
                DataSourceID="dskey" DataTextField="Gru_nombre"
                DataValueField="Gru_id" Width="200px"
                ToolTip="Seleccione el Grupo: " TabIndex="0" AutoPostBack="false">
            </asp:DropDownList>
            <asp:SqlDataSource ID="dskey" runat="server"
                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" SelectCommand="SELECT Gru_id, Gru_nombre FROM Grupo "></asp:SqlDataSource>

            <asp:Button ID="btnkey" runat="server" Text="Filtrar Grupos"
                CssClass="btn btn-info" ToolTip="Filtrar Grupo..." />
        </div>
    </div>
    <div id="posApps8" style="overflow: auto; width: 1360px; height: 500px;">

        <asp:GridView ID="grdLlaves" runat="server"
            AllowSorting="True" AutoGenerateColumns="False"
            DataSourceID="dsTerminalsLlaves" CssClass="table table-responsive-lg jumbotron table-light table-hover table-bordered" HeaderStyle-ForeColor="WhiteSmoke"
            HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
            HeaderStyle-CssClass="table-bordered bg-info" DataKeyNames="ter_id, ter_serial"
            EmptyDataText="No existen Terminales creadas en el Grupo o con el filtro aplicado"
            HorizontalAlign="Center" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
            <AlternatingRowStyle Wrap="True" />
            <Columns>
                <asp:TemplateField HeaderText=" " SortExpression="ter_logo" HeaderStyle-Width="20px">
                    <ItemTemplate>
                        <asp:Image ID="imgUrlIcon" runat="server" ImageUrl='<%# Bind("ter_logo") %>' ToolTip='<%# Bind("ter_fecha_actualizacion_auto") %>' />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:BoundField DataField="ter_serial" HeaderText="Serial"
                    SortExpression="ter_serial" />
                <asp:BoundField DataField="ter_modelo" HeaderText="Modelo"
                    SortExpression="ter_modelo" />
                <asp:BoundField DataField="ter_imei" HeaderText="IMEI"
                    SortExpression="ter_imei" />
                <asp:BoundField DataField="ter_codigo_producto" HeaderText="Codigo de Producto"
                    SortExpression="ter_codigo_producto" />
                <asp:TemplateField HeaderText="Prioridad Grupo" SortExpression="ter_prioridad_grupo">
                    <ItemTemplate>
                        <asp:CheckBox ID="editChk1" runat="server" Checked='<%# Bind("ter_prioridad_grupo")%>' Enabled="false" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Actualizar Manual" SortExpression="ter_actualizar">
                    <ItemTemplate>
                        <asp:CheckBox ID="editChk2" runat="server" Checked='<%# Bind("ter_actualizar")%>' Enabled="false" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:BoundField DataField="ter_fecha_programacion" HeaderText="Fecha Manual Descarga"
                    SortExpression="ter_fecha_programacion" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="ter_ip_descarga" HeaderText="IP Descarga"
                    SortExpression="ter_ip_descarga" />
                <asp:BoundField DataField="ter_puerto_descarga" HeaderText="Puerto Descarga"
                    SortExpression="ter_puerto_descarga" />
                <asp:BoundField DataField="ter_estado_actualizacion" HeaderText="Estado Actualización"
                    SortExpression="ter_estado_actualizacion" ItemStyle-HorizontalAlign="Center" />
                <asp:TemplateField HeaderText="Estado" SortExpression="ter_estado">
                    <ItemTemplate>
                        <asp:CheckBox ID="editChk3" runat="server" Checked='<%# Bind("ter_estado")%>' Enabled="false" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False" HeaderStyle-Width="80px">
                    <ItemTemplate>
                        <asp:ImageButton ID="imgKey1" runat="server" CausesValidation="False" Enabled="true" Visible="true"
                            CommandName="InjectKey1" ImageUrl="~/img/icons/16x16/key_1.png" Text="Cargar Componente 1"
                            ToolTip="Cargar Componente 1" CommandArgument='<%# grdLlaves.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />

                        <asp:ImageButton ID="imgKey2" runat="server" CausesValidation="False" Enabled="true" Visible="true"
                            CommandName="InjectKey2" ImageUrl="~/img/icons/16x16/key_2.png" Text="Cargar Componente 2"
                            ToolTip="Cargar Componente 2" CommandArgument='<%# grdLlaves.Rows.Count%>' Style="padding: 2px 2px 2px 2px !important;" CssClass="imgLink" />

                        <asp:Image ID="imgKey1status" runat="server" ImageUrl='<%# Bind("ter_logo_estado_componente1")%>' ToolTip='<%# Bind("ter_desc_estado_componente1") %>' Style="padding: 0px 2px 2px 0px !important;" Visible="true" Enabled="true" />
                        <asp:Image ID="imgKey2status" runat="server" ImageUrl='<%# Bind("ter_logo_estado_componente2")%>' ToolTip='<%# Bind("ter_desc_estado_componente2") %>' Style="padding: 0px 2px 2px 0px !important;" Visible="true" Enabled="true" />
                        <asp:Image ID="imgUrlIconStatusKey" runat="server" ImageUrl='<%# Bind("ter_logo_llave")%>' ToolTip='<%# Bind("ter_fecha_ultima_carga_llave")  %>' Style="padding: 0px 2px 2px 0px !important;" Visible="true" Enabled="true" CommandArgument='<%# grdLlaves.Rows.Count%>' />

                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>

            </Columns>
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
                CssClass="pgr" Font-Underline="False" />
            <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid"
                BorderWidth="1px" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>
        <br />
        <asp:SqlDataSource ID="dsTerminalsLlaves" runat="server"
            ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
            SelectCommand="sp_webConsultarTerminalesGrupo" SelectCommandType="StoredProcedure"
            DeleteCommand="sp_webEliminarTerminalXGrupo" DeleteCommandType="StoredProcedure" ConflictDetection="OverwriteChanges">
            <SelectParameters>
                <asp:Parameter Name="groupId" Type="String" />
                <asp:Parameter Name="terminalSerial" Type="String" DefaultValue="-1" />
                <asp:Parameter Name="terminalImei" Type="String" DefaultValue="-1" />
            </SelectParameters>
            <DeleteParameters>
                <asp:Parameter Name="terminalId" Type="String" />
                <asp:Parameter Name="groupId" Type="String" />
            </DeleteParameters>
        </asp:SqlDataSource>
    </div>

    <script type="text/javascript" src="../js/lineas.js" charset="utf-8"></script>
    <script type="text/javascript" src="../js/Graph.js" charset="utf-8"></script>

  

 

    


    
   
    
    
    
   
    
    
   
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <asp:Literal ID="Literal1" runat="server"></asp:Literal>
    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    <script src="../js/ValidatorParameters.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>
</asp:Content>

