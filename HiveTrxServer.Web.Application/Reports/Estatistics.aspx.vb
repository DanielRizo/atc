﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users
Imports System.Globalization
Imports TeleLoader.Groups
Imports TeleLoader.Terminals


Partial Class Reports_Estadisticas
    Inherits TeleLoader.Web.BasePage

    Private totalTerminals As Integer
    Private totalUpdated As Integer
    Private totalPending As Integer
    Public tipo As String
    Public tipoParam As String
    Public TID As String
    Public EstadisticsObj As Group


    Protected Sub btnChart_Click(sender As Object, e As EventArgs) Handles btnChart.Click
        objSessionParams.intSelectedGroup = Convert.ToInt64(ddltipos.SelectedValue)

        Dim serial As Integer

        tipo = ddltipos.SelectedValue
        serial = ddlTerminalesPos.SelectedValue
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim configurationSection As ConnectionStringsSection =
                            System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString

        If Not IsPostBack Then

            'llenamos la GriedView
            DsAdmin.SelectParameters("customerID").DefaultValue = objAccessToken.CustomerUserID

            Dim Grupo As Integer


            Using Conn As New SqlConnection(strConnString)
                Conn.Open()
                Dim ConsultaSQL = "SELECT gru_id from grupo where gru_nombre = 'DEFAULT_GROUP'"
                Grupo = New SqlCommand(ConsultaSQL, Conn).ExecuteScalar().ToString()
            End Using


            getReportTerminalsData()

            If ddlUserList.SelectedValue = "" Then
                objSessionParams.intSelectedGroup = Grupo
                dsTerminals.SelectParameters("groupId").DefaultValue = objSessionParams.intSelectedGroup
                grdTerminals.DataBind()

            End If

            If ddlkey.SelectedValue = "" Then
                objSessionParams.intSelectedGroup = Grupo
                dsTerminalsLlaves.SelectParameters("groupId").DefaultValue = objSessionParams.intSelectedGroup
                grdLlaves.DataBind()

            End If

            Dim arrayDataJS = "var dataNet = [['Estado', 'Terminales'], ['Actualizadas', " & totalUpdated & "], ['Pendientes', " & totalPending & "]];"

            arrayDataJS += " var dataNet2 = " + getReportTerminalsDataByModel() + ";"
            ltrScript.Text = "<script language='javascript'>" & arrayDataJS & "</script>"


            If ddlTerminalesPos.SelectedValue = "-1" Then
                objSessionParams.intSelectedTerminalID = 14250

            End If

            If ddlTerminalesPos.SelectedValue = "" Then
                objSessionParams.intSelectedTerminalID = 3
            Else
                objSessionParams.intSelectedTerminalID = ddlTerminalesPos.SelectedValue

            End If

            Dim SerialTerminal As String
            Dim imeiPos As String
            Dim SimPos As String
            Dim GrupoPos As String
            Dim ComunicionPos As String
            Dim ModeloPos As String

            Using Conn As New SqlConnection(strConnString)
                Conn.Open()
                Dim ConsultaSQL = "SELECT CONCAT('Serial:  ',ter_serial) from terminal where ter_id =" & objSessionParams.intSelectedTerminalID
                SerialTerminal = New SqlCommand(ConsultaSQL, Conn).ExecuteScalar().ToString()
            End Using
            Using Conn As New SqlConnection(strConnString)
                Conn.Open()
                Dim ConsultaSQL = "SELECT CONCAT('IMEI:  ',ter_imei) from terminal where ter_id =" & objSessionParams.intSelectedTerminalID
                imeiPos = New SqlCommand(ConsultaSQL, Conn).ExecuteScalar().ToString()
            End Using
            Using Conn As New SqlConnection(strConnString)
                Conn.Open()
                Dim ConsultaSQL = "SELECT CONCAT('SIM:  ',ter_serial_sim) from terminal where ter_id =" & objSessionParams.intSelectedTerminalID
                SimPos = New SqlCommand(ConsultaSQL, Conn).ExecuteScalar().ToString()
            End Using
            Using Conn As New SqlConnection(strConnString)
                Conn.Open()
                Dim ConsultaSQL = "SELECT CONCAT('Grupo:  ',G.gru_nombre) FROM TERMINAL T INNER JOIN GRUPO G ON G.GRU_ID = T.ter_grupo_id where ter_id =" & objSessionParams.intSelectedTerminalID
                GrupoPos = New SqlCommand(ConsultaSQL, Conn).ExecuteScalar().ToString()
            End Using
            Using Conn As New SqlConnection(strConnString)
                Conn.Open()
                Dim ConsultaSQL = "SELECT CONCAT('Comunicación:  ',G.descripcion) FROM TERMINAL T INNER JOIN InterfaceTerminal G ON g.id = T.ter_interface_terminal_id where ter_id =" & objSessionParams.intSelectedTerminalID
                ComunicionPos = New SqlCommand(ConsultaSQL, Conn).ExecuteScalar().ToString()
            End Using
            Using Conn As New SqlConnection(strConnString)
                Conn.Open()
                Dim ConsultaSQL = "SELECT CONCAT('Modelo:  ',G.descripcion) FROM TERMINAL T INNER JOIN ModeloTerminal G ON g.id = T.ter_modelo_terminal_id where ter_id =" & objSessionParams.intSelectedTerminalID
                ModeloPos = New SqlCommand(ConsultaSQL, Conn).ExecuteScalar().ToString()
            End Using

            SerialPos.Text = SerialTerminal
            imei.Text = imeiPos
            SerialSim.Text = SimPos
            GrupoEstatistic.Text = GrupoPos
            Comunicacion.Text = ComunicionPos
            Modelo.Text = ModeloPos

            Dim arrayDataJS2 = " var dataNet3 = " + getReportEstadisticasTerminals(tipo) + ";"
            Literal1.Text = "<script language='javascript'>" & arrayDataJS2 & "</script>"
            PnlDescargas.Visible = True


            If totalTerminals = 0 Then
                pnlCharts.Visible = False
            Else
                pnlCharts.Visible = True
            End If

        End If


        If ddlTerminalesPos.SelectedValue = "-1" Then
            objSessionParams.intSelectedTerminalID = 14250

        End If
        If ddlTerminalesPos.SelectedValue = "" Then
            objSessionParams.intSelectedTerminalID = 3
        Else
            objSessionParams.intSelectedTerminalID = ddlTerminalesPos.SelectedValue

        End If
        'Serial Pos 
        Dim SerialTerminalNotPosback As String
        Dim ImeiTerminalNotPosback As String
        Dim SimTerminalNotPosback As String
        Dim GrupoTerminalNotPosback As String
        Dim ComunicacionTerminalNotPosback As String
        Dim ModeloTerminalNotPosback As String

        Using Conn As New SqlConnection(strConnString)
            Conn.Open()
            Dim ConsultaSQL = "SELECT CONCAT('Serial:  ',ter_serial) from terminal where ter_id =" & objSessionParams.intSelectedTerminalID
            SerialTerminalNotPosback = New SqlCommand(ConsultaSQL, Conn).ExecuteScalar().ToString()
        End Using
        Using Conn As New SqlConnection(strConnString)
            Conn.Open()
            Dim ConsultaSQL = "SELECT CONCAT('IMEI:  ',ter_imei) from terminal where ter_id =" & objSessionParams.intSelectedTerminalID
            ImeiTerminalNotPosback = New SqlCommand(ConsultaSQL, Conn).ExecuteScalar().ToString()
        End Using
        Using Conn As New SqlConnection(strConnString)
            Conn.Open()
            Dim ConsultaSQL = "SELECT CONCAT('SIM:  ',ter_serial_sim) from terminal where ter_id =" & objSessionParams.intSelectedTerminalID
            SimTerminalNotPosback = New SqlCommand(ConsultaSQL, Conn).ExecuteScalar().ToString()
        End Using
        Using Conn As New SqlConnection(strConnString)
            Conn.Open()
            Dim ConsultaSQL = "SELECT CONCAT('Grupo:  ',G.gru_nombre) FROM TERMINAL T INNER JOIN GRUPO G ON G.GRU_ID = T.ter_grupo_id where ter_id =" & objSessionParams.intSelectedTerminalID
            GrupoTerminalNotPosback = New SqlCommand(ConsultaSQL, Conn).ExecuteScalar().ToString()
        End Using
        Using Conn As New SqlConnection(strConnString)
            Conn.Open()
            Dim ConsultaSQL = "SELECT CONCAT('Comunicación:  ',G.descripcion) FROM TERMINAL T INNER JOIN InterfaceTerminal G ON g.id = T.ter_interface_terminal_id where ter_id =" & objSessionParams.intSelectedTerminalID
            ComunicacionTerminalNotPosback = New SqlCommand(ConsultaSQL, Conn).ExecuteScalar().ToString()
        End Using
        Using Conn As New SqlConnection(strConnString)
            Conn.Open()
            Dim ConsultaSQL = "SELECT CONCAT('Modelo:  ',G.descripcion) FROM TERMINAL T INNER JOIN ModeloTerminal G ON g.id = T.ter_modelo_terminal_id where ter_id =" & objSessionParams.intSelectedTerminalID
            ModeloTerminalNotPosback = New SqlCommand(ConsultaSQL, Conn).ExecuteScalar().ToString()
        End Using



        SerialPos.Text = SerialTerminalNotPosback
        imei.Text = ImeiTerminalNotPosback
        SerialSim.Text = SimTerminalNotPosback
        GrupoEstatistic.Text = GrupoTerminalNotPosback
        Comunicacion.Text = ComunicacionTerminalNotPosback
        Modelo.Text = ModeloTerminalNotPosback

        Dim arrayDataJS3 = " var dataNet3 = " + getReportEstadisticasTerminals(ddltipos.SelectedValue) + ";"
        Literal1.Text = "<script language='javascript'>" & arrayDataJS3 & "</script>"
        PnlDescargas.Visible = True



    End Sub
    Public Function getReportEstadisticasTerminals(ByVal tipo As String) As String

        If tipo = "" Or tipo = "-1" Then
            tipo = "91"
        End If
        Dim connection As New SqlConnection(strConnectionString)
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim retVal As String
        Dim idx As Integer = 0
        Dim startDate As String = objSessionParams.dateStartDate.Year.ToString.PadLeft(4, "0") +
          objSessionParams.dateStartDate.Month.ToString.PadLeft(2, "0") +
            objSessionParams.dateStartDate.Day.ToString.PadLeft(2, "0")
        Dim endDate As String = objSessionParams.dateEndDate.Year.ToString.PadLeft(4, "0") +
            objSessionParams.dateEndDate.Month.ToString.PadLeft(2, "0") +
            objSessionParams.dateEndDate.Day.ToString.PadLeft(2, "0")
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webReporteChartTransacciones"
            command.Parameters.Clear()
            command.Parameters.Add(New SqlParameter("tipo_id", tipo))
            command.Parameters.Add(New SqlParameter("fechaInicial", startDate))
            command.Parameters.Add(New SqlParameter("fechaFinal", endDate))
            command.Parameters.Add(New SqlParameter("TerId", objSessionParams.intSelectedTerminalID))
            results = command.ExecuteReader()
            retVal = "[['fecha','valor'],"
            If results.HasRows Then
                While results.Read()
                    retVal += "['" + results.GetString(0) + "'," + results.GetString(1) + "],"
                End While
            End If

            retVal += "]"

        Catch ex As Exception

        Finally
            'Cerrar DataReader
            If Not (results Is Nothing) Then
                results.Close()
            End If

            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try

        Return retVal

    End Function

    Protected Sub grdTerminals_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdTerminals.RowCommand
        Try

            Select Case e.CommandName

                Case "ShowTerminalMap"
                    grdTerminals.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.intSelectedTerminalID = grdTerminals.SelectedDataKey.Values.Item("ter_id")

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("ShowTerminalGroupLocation.aspx", False)
            End Select

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub
    Protected Sub grdLlaves_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdLlaves.RowCommand
        Try

            Select Case e.CommandName


                Case "InjectKey1"
                    grdLlaves.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    Dim GRUPO As String = ""


                    Dim configurationSection As ConnectionStringsSection =
                            System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
                    Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString

                    Using Conn As New SqlConnection(strConnString)
                        Conn.Open()
                        Dim ConsultaSQL = "SELECT GRU_NOMBRE from GRUPO where GRU_ID =" & ddlkey.SelectedValue
                        GRUPO = New SqlCommand(ConsultaSQL, Conn).ExecuteScalar().ToString()
                    End Using

                    objSessionParams.intSelectedTerminalID = grdLlaves.SelectedDataKey.Values.Item("ter_id")
                    objSessionParams.strtSelectedGroup = GRUPO
                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("../Group/InjectComponent1TerminalAndroidGroup.aspx", False)

                    'Modificación Carga de Llaves; EB: 28/Feb/2018
                Case "InjectKey2"
                    grdLlaves.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.intSelectedTerminalID = grdLlaves.SelectedDataKey.Values.Item("ter_id")

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("../Group/InjectComponent2TerminalAndroidGroup.aspx", False)



            End Select

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub
    Protected Sub GrdAdmin_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdAdmin.RowCommand
        Try

            Select Case e.CommandName

                Case "EditGroup"
                    GrdAdmin.SelectedIndex = Convert.ToInt32(e.CommandArgument)

                    objSessionParams.intSelectedGroup = GrdAdmin.SelectedDataKey.Values.Item("gru_id")

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("MoveTerminals.aspx", False)
            End Select

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub

    Private Function getReportName() As String

        Dim retVal As String = ""

        Select Case objSessionParams.intReportType
            Case 54
                retVal = "Log de Auditoría"
            Case 140
                retVal = "Log de Inventario"
            Case 141
                retVal = "Log de Descarga"
            Case 142
                retVal = "Reporte de Terminales"
            Case 143
                retVal = "Estado Actualización de Terminales"
        End Select

        Return retVal

    End Function

    Public Sub getReportTerminalsData()
        Dim connection As New SqlConnection(strConnectionString)
        Dim command As New SqlCommand()
        Dim results As SqlDataReader

        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webReporteChartTerminales"
            command.Parameters.Clear()
            command.Parameters.Add(New SqlParameter("customerID", objAccessToken.CustomerUserID))
            command.Parameters.Add(New SqlParameter("groupID", objSessionParams.intSelectedGroupID))

            'Leer Resultado
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    totalUpdated = results.GetInt32(0)
                    totalPending = results.GetInt32(1)
                    totalTerminals = results.GetInt32(2)
                End While
            Else
                totalUpdated = 0
                totalPending = 0
                totalTerminals = 0
            End If

        Catch ex As Exception

        Finally
            'Cerrar DataReader
            If Not (results Is Nothing) Then
                results.Close()
            End If

            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try

    End Sub
    Protected Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click
        objSessionParams.intSelectedGroup = Convert.ToInt64(ddlUserList.SelectedValue)
        dsTerminals.SelectParameters("groupId").DefaultValue = objSessionParams.intSelectedGroup
        grdTerminals.DataBind()

    End Sub

    ''' <summary>
    ''' Editar Grupo
    ''' </summary>


    Protected Sub btnkey_Click(sender As Object, e As EventArgs) Handles btnkey.Click
        objSessionParams.intSelectedGroup = Convert.ToInt64(ddlkey.SelectedValue)
        dsTerminalsLlaves.SelectParameters("groupId").DefaultValue = objSessionParams.intSelectedGroup
        grdLlaves.DataBind()

    End Sub

    Public Function getReportTerminalsDataByModel() As String
        Dim connection As New SqlConnection(strConnectionString)
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim retVal As String
        Dim idx As Integer = 0

        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webReporteChartTerminalesXModelo"
            command.Parameters.Clear()
            command.Parameters.Add(New SqlParameter("customerID", objAccessToken.CustomerUserID))
            command.Parameters.Add(New SqlParameter("groupID", objSessionParams.intSelectedGroupID))

            'Leer Resultado
            results = command.ExecuteReader()
            retVal = "[['Tipo Terminal','Terminales'],"
            If results.HasRows Then
                While results.Read()
                    retVal += "['" + results.GetString(0) + "'," + results.GetString(1) + "],"
                End While
            End If

            retVal += "]"

        Catch ex As Exception

        Finally
            'Cerrar DataReader
            If Not (results Is Nothing) Then
                results.Close()
            End If

            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try

        Return retVal

    End Function
    Public Function getReportEstadisticasTerminalsConsultas() As String
        Dim connection As New SqlConnection(strConnectionString)
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim retVal As String
        Dim idx As Integer = 0
        Dim startDate As String = objSessionParams.dateStartDate.Year.ToString.PadLeft(4, "0") +
          objSessionParams.dateStartDate.Month.ToString.PadLeft(2, "0") +
            objSessionParams.dateStartDate.Day.ToString.PadLeft(2, "0")
        Dim endDate As String = objSessionParams.dateEndDate.Year.ToString.PadLeft(4, "0") +
            objSessionParams.dateEndDate.Month.ToString.PadLeft(2, "0") +
            objSessionParams.dateEndDate.Day.ToString.PadLeft(2, "0")
        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webReporteChartTransacciones"
            command.Parameters.Clear()
            command.Parameters.Add(New SqlParameter("tipo_id", "92"))
            command.Parameters.Add(New SqlParameter("fechaInicial", startDate))
            command.Parameters.Add(New SqlParameter("fechaFinal", endDate))
            command.Parameters.Add(New SqlParameter("TerId", objSessionParams.intSelectedTerminalID))
            'Leer Resultado
            results = command.ExecuteReader()
            retVal = "[['fecha','valor'],"
            If results.HasRows Then
                While results.Read()
                    retVal += "['" + results.GetString(0) + "'," + results.GetString(1) + "],"
                End While
            End If

            retVal += "]"

        Catch ex As Exception

        Finally
            'Cerrar DataReader
            If Not (results Is Nothing) Then
                results.Close()
            End If

            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try

        Return retVal

    End Function
    Protected Sub ddlUserList_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlUserList.SelectedIndexChanged
        Dim Grupo As String
        Grupo = ddlUserList.SelectedValue
    End Sub

End Class
