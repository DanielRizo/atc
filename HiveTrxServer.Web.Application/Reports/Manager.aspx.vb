﻿
Partial Class Reports_Manager
    Inherits TeleLoader.Web.BasePage

    Public ReadOnly SEPARATOR As String = "|"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            DrawOptions(repOpcion)
        End If
    End Sub

    Protected Sub lnkOpcionItem_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)
        Dim tokens() As String = e.CommandArgument.ToString().Split(SEPARATOR)
        Dim path As String = tokens(0).Substring(e.CommandArgument.ToString.IndexOf("/") + 1)

        Dim reportType As String = tokens(1)

        objSessionParams.intReportType = Integer.Parse(reportType)

        'Set Data into Session
        Session("SessionParameters") = objSessionParams

        Response.Redirect(path)
    End Sub

End Class
