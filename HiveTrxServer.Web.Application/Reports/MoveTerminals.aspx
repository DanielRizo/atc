﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="MoveTerminals.aspx.vb" Inherits="Move_Terminal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">

    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%>
        :: Editar Terminales
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

    <%--scripts edit table--%>

    <link rel="stylesheet" type="text/css" href="/style/bootstrap.min.css" />


    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/editTable.js") %>'></script>

    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.tabledit.js") %>'></script>

    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/bootstrap.js") %>'></script>

    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/moveElementsList.js") %>'></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <style>
        .detailPrompt {
            background-color: #f2f7fc;
            border: 1px solid black;
            padding: 2px;
            margin: 2px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">

    <style>
        .bg-1 {
            background-color: #1abc9c;
            color: #ffffff;
        }
    </style>
    <div class="bg-1">
        <div class="container text-center">
            <h3>Edicion de terminales</h3>
            <img src="https://cdn2.iconfinder.com/data/icons/shopping-e-commerce-3/512/wallet-512.png" class="img-circle" alt="Bird" width="250" height="250">
        </div>
    </div>
    <blockquote>
        <p>Este módulo permite mover terminales de un grupo a otro:</p>
        <footer class="blockquote-footer">Polaris Cloud Service (Pstis) </footer>
    </blockquote>

    <div class="container-fluid bg-2 text-center">
        <h3>Mover terminales de grupo </h3>
    </div>
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
        <div id="succes2" style="display: none;">
            <asp:Panel ID="pnlMsg" runat="server">
                <div class="alert  alert-success">
                    <asp:Label ID="lblMsg2" runat="server" Text="Datos editados correctamente."></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">x</a>
                </div>
            </asp:Panel>
        </div>

        <div id="error2" style="display: none;">
            <asp:Panel ID="pnlError" runat="server">
                <div class="alert alert-danger">
                    <asp:Label ID="lblError2" runat="server" Text="Error editando datos del grupo."></asp:Label>
                    <asp:Label ID="CodigoDest" runat="server" Text=""></asp:Label>
                    <asp:Label ID="Codigo" runat="server" Text=""></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">x</a>
                </div>
            </asp:Panel>
        </div>

    </div>
    <div id="alertCampos" class="alert alert-danger">
        <div>Debe llenar los campos requeridos.</div>
    </div>
    <script> $("#alertCampos").hide();</script>
    <!-- START SIMPLE FORM -->

    <div class="st-form-line">
        <span class="st-labeltext"><b>Grupo destino:</b></span>
        <asp:DropDownList ID="ddlGroup" class="btn btn-primary dropdown-toggle" runat="server"
            DataSourceID="dsGroup" DataTextField="gru_nombre"
            DataValueField="gru_id" Width="200px"
            ToolTip="Usuarios Admin." TabIndex="0" AutoPostBack="true">
        </asp:DropDownList>
        <asp:SqlDataSource ID="dsGroup" runat="server"
            ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" SelectCommand="SELECT -1 AS gru_id, '            ' AS gru_nombre
                        UNION ALL
                        SELECT gru_id, gru_nombre FROM GRUPO"></asp:SqlDataSource>
    </div>
    <br />
    <br />
    <div class="clear"></div>
    <div class="container text-center">
        <h3>Datos grupo:</h3>
        <br>
        <div class="row">
            <div class="col-sm-3">
                <img src="https://miracomohacerlo.com/wp-content/uploads/2019/06/direccion-ip-pagina-web-1024x638.jpg" class="img-responsive" style="width: 100%" alt="Image">
                <asp:Label ID="Serial" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-sm-3">
                <img src="https://us.123rf.com/450wm/axsimen/axsimen1612/axsimen161200012/69138781-sin-contacto-de-compra-de-pago-del-icono-del-vector-en-un-estilo-plano-pago-bancario-inal%C3%A1mbrico-media.jpg?ver=6" class="img-responsive" style="width: 100%" alt="Image">
                <asp:Label ID="FrecuenciaPos" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-sm-3">
                <div class="well">
                    <asp:Label ID="Kiosco" runat="server" Text=""></asp:Label>
                </div>
                <div class="well">
                    <asp:Label ID="Mensaje" runat="server" Text=""></asp:Label>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="well">
                    <asp:Label ID="UpdateNow" runat="server" Text=""></asp:Label>
                </div>
                <div class="well">
                    <asp:Label ID="BlockingMode" runat="server" Text=""></asp:Label>
                </div>
            </div>
        </div>
        <hr>
    </div>
    <div class="simplebox grid960">
        <div>
            <h3 class="title">Terminales de grupo 
                <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
        </div>
        <div class="panel panel-default" style="background-color: #fff; width: auto;">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-2">
                        <div class="">
                            <span class="st-labeltext">Grupo origen:</span>
                            <asp:TextBox ID="txtgrupo" CssClass="st-success-input" Style="width: 200px"
                                runat="server" TabIndex="0" MaxLength="100"
                                ToolTip="Nombre del Grupo." Enabled="False"></asp:TextBox>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="col-sm-4" style="padding-left: 4%">
                        <select name="from" id="multiselect_left" class="form-control" size="15" multiple="multiple">
                            <optgroup label="TERMINALES">
                            </optgroup>
                        </select>
                        <br />
                        <div class="detailPrompt" id="detailPromptLeft"></div>

                    </div>
                    <script type="text/javascript">

                        $(document).ready(function () {
                            var grupo = $("#ctl00_MainContent_Codigo").text();
                            $.ajax({
                                type: "POST",
                                url: "../Group/wsadd.asmx/" + "GetTerminalesGrupo",
                                data: '{"data":"' + grupo + '"}',
                                contentType: "application/Json; charset=utf-8",
                                dataType: 'Json',
                                success: function (r) {
                                    var JsonRsp = JSON.parse(r.d);
                                    console.log(JsonRsp)
                                    for (i in JsonRsp.registroTerminal) {

                                        $('#multiselect_left').append('<option value="' + JsonRsp.registroTerminal[i].Codigo + '">' + JsonRsp.registroTerminal[i].Terminal + '</option>');
                                    }
                                },
                                error: function (r) {

                                },
                                failure: function (r) {

                                }
                            });
                        });     </script>


                    <div class="col-sm-1">
                        <button type="button" id="btn_rightAll" class="btn btn-info">
                            <img src="../img/fastforward.png"
                                alt="rightAll" /></button><br />
                        <br />
                        <button type="button" id="btn_rightSelected" class="btn btn-info">
                            <img src="../img/right.png"
                                alt="rightSelected" /></button><br />
                        <br />
                        <button type="button" id="btn_leftSelected" class="btn btn-info">
                            <img src="../img/left.png"
                                alt="leftSelected" /></button><br />
                        <br />
                        <button type="button" id="btn_leftAll" class="btn btn-info">
                            <img src="../img/fastbackward.png"
                                alt="leftAll" /></button><br />
                        <br />
                    </div>

                    <div class="col-sm-4">
                        <select name="from" id="multiselect_right" class="form-control" size="15" multiple="multiple">
                            <optgroup label="Nuevo Grupo" id="Lista">
                            </optgroup>
                        </select>
                        <div class="Nuevo grupo" id="detailPromptRight"></div>
                    </div>
                </div>
            </div>
            <div class="button-box" id="">

                <button id="ButGuardarGrupo" class="btn btn-info">Mover terminales</button>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $('#ButGuardarGrupo').on('click', function (event) {
            var Ids = "";
            var cont = 0;
            $("#ctl00_MainContent_Codigo").css("background", "White");
            $("#multiselect_right option").each(function () {
                Ids = Ids + "@" + $(this).val();
                cont = 1;
            });

            if ((!$("#ctl00_MainContent_Codigo").text() == "")) {
                Ids = Ids + "@" + $("#ctl00_MainContent_Codigo").text() + "@" + $("#ctl00_MainContent_CodigoDest").text() 
                event.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "../Group/wsadd.asmx/" + "EditGroupPrompts",
                    data: '{"data":"' + Ids + '"}',
                    contentType: "application/Json; charset=utf-8",
                    dataType: 'Json',
                    success: function (r) {
                        document.getElementById("succes2").style.display = "inline";


                        setTimeout('window.history.back()', 3000);

                    },
                    error: function (r) {
                        document.getElementById("error2").style.display = "inline";
                        setTimeout('document.location.reload()', 3000);
                    },
                    failure: function (r) {

                    }
                });
            }
            else {

                $("#alertCampos").fadeTo(0, 500)

                if (document.getElementById("#ctl00_MainContent_Codigo").value == "") {
                    $("#ctl00_MainContent_Codigo").css("background", "Pink");
                }
                window.setTimeout(function () {
                    $("#alertCampos").fadeTo(500, 0).slideUp(500, function () {
                    });
                }, 3000);

                event.preventDefault();

            }
        });     </script>


    <div class="body">
    </div>
    <script src='<%= ResolveClientUrl("~/js/showDetailDataPrompts.js") %>'>
    </script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', { 'packages': ['corechart', 'bar'] });
        google.charts.setOnLoadCallback(drawStuff);

        function drawStuff() {

            var button = document.getElementById('change-chart');
            //button.remove;
            var chartDiv = document.getElementById('chart_div');

            var data = google.visualization.arrayToDataTable();

            var materialOptions = {
                width: 900,
                chart: {
                    title: 'Estadisticas Terminales',
                    subtitle: 'Estadisticas Terminales'
                },
                series: {
                    0: { axis: 'distance' }, // Bind series 0 to an axis named 'distance'.
                    1: { axis: 'brightness' } // Bind series 1 to an axis named 'brightness'.
                },
                axes: {
                    y: {
                        distance: { label: 'Estadisticas Terminales' }, // Left y-axis.
                        brightness: { side: 'right', label: 'apparent magnitude' } // Right y-axis.
                    }
                }
            };
            function drawMaterialChart() {
                var materialChart = new google.charts.Bar(chartDiv);
                materialChart.draw(data, google.charts.Bar.convertOptions(materialOptions));
                button.innerText = 'Estadisticas';
                //button.onclick = drawMaterialChart;
            }

            drawMaterialChart();
        };
    </script>
    <!-- end pie chart javascript codes -->
    <asp:Panel ID="pnlCharts" runat="server">
        <!-- start chart div -->
        <br />
        <h2 style="text-align: center;"></h2>
        <div id="terminals_chart_div" style="text-align: center;"></div>
        <br />
    </asp:Panel>


    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

        // Load the Visualization API and the piechart package.
        google.charts.load('current', { 'packages': ['corechart'] });

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable(dataNet2);

            document.getElementById("error_msg").innerHTML = "GRAFICA GENERADA DE FORMA CORRECTA, SE MOSTRARAN LOS ULTIMOS 40 DIAS DE ACTIVIDAD"
            // Set chart options
            var options = {
                'title': '',
                'width': 1350,
                'height': 500
            };

            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
            google.visualization.events.addListener(chart, 'error', function (googleError) {
                google.visualization.errors.removeError(googleError.id);
                // document.getElementById("error_msg").innerHTML = "Message removed = '" + googleError.message + "'";
                document.getElementById("error_msg").innerHTML = "NO HAY DATOS SELECCIONE OTRO TERMINAL";

            });
            chart.draw(data, options);
        }
    </script>


    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Reports/Estatistics.aspx"
        onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás" /> Volver
        a la página anterior</asp:HyperLink>

</asp:Content>


<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

    <script language="javascript">
        var globalcontrol = '';
    </script>

</asp:Content>
