﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports TeleLoader.Applications
Imports System.Data.Common
Imports System.Web.Services
Imports System.Web.Script.Services

Partial Class Move_Terminal

    Inherits TeleLoader.Web.BasePage

    Dim applicationObj As ApplicationAcquirerParameter
    Public ID As String
    Public Sub ddlGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGroup.SelectedIndexChanged
        Dim Gru_Name As String
        ID = ddlGroup.SelectedValue
        Gru_Name = ddlGroup.SelectedItem.Text

        Dim ip As String
        Dim frecuencia As String
        Dim KioscoGroup As String
        Dim MensajeGroup As String
        Dim UpdateGroup As String
        Dim Bloqueante As String


        Dim configurationSection As ConnectionStringsSection =
                        System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString


        Using Conn As New SqlConnection(strConnString)
            Conn.Open()
            Dim ConsultaSQL = "SELECT CONCAT('Ip-puerto:  ',gru_ip_descarga,'-',gru_puerto_descarga) FROM GRUPO where gru_id = " & ID
            ip = New SqlCommand(ConsultaSQL, Conn).ExecuteScalar().ToString()
        End Using
        Using Conn As New SqlConnection(strConnString)
            Conn.Open()
            Dim ConsultaSQL = "SELECT CONCAT('Frecuencia:  ',gru_numero_consultas,'-',gru_frecuencia_horas) FROM GRUPO where gru_id = " & ID
            frecuencia = New SqlCommand(ConsultaSQL, Conn).ExecuteScalar().ToString()
        End Using
        Using Conn As New SqlConnection(strConnString)
            Conn.Open()
            Dim ConsultaSQL = "SELECT CONCAT('Modo Kiosko:  ',gru_aplicacion_kiosko) FROM GRUPO where gru_id = " & ID
            KioscoGroup = New SqlCommand(ConsultaSQL, Conn).ExecuteScalar().ToString()
        End Using
        Using Conn As New SqlConnection(strConnString)
            Conn.Open()
            Dim ConsultaSQL = "SELECT CONCAT('Mensaje:  ',gru_mensaje_desplegar) FROM GRUPO where gru_id = " & ID
            MensajeGroup = New SqlCommand(ConsultaSQL, Conn).ExecuteScalar().ToString()
        End Using
        Using Conn As New SqlConnection(strConnString)
            Conn.Open()
            Dim ConsultaSQL = "SELECT CONCAT('Actualización Inmediata:  ', CASE WHEN gru_update_now =  1 THEN 'ACTIVO' ELSE 'DESACTIVO' END) FROM GRUPO where gru_id = " & ID
            UpdateGroup = New SqlCommand(ConsultaSQL, Conn).ExecuteScalar().ToString()
        End Using
        Using Conn As New SqlConnection(strConnString)
            Conn.Open()
            Dim ConsultaSQL = "SELECT CONCAT('Modo bloqueante:  ', CASE WHEN gru_blocking_mode =  1 THEN 'ACTIVO' ELSE 'DESACTIVO' END) FROM GRUPO where gru_id = " & ID
            Bloqueante = New SqlCommand(ConsultaSQL, Conn).ExecuteScalar().ToString()
        End Using




        Serial.Text = ip
        FrecuenciaPos.Text = frecuencia
        Kiosco.Text = KioscoGroup
        Mensaje.Text = MensajeGroup
        UpdateNow.Text = UpdateGroup
        BlockingMode.Text = Bloqueante
        CodigoDest.Text = ddlGroup.SelectedValue

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then


            ID = objSessionParams.intSelectedGroup

            Dim Grupo As String
            Dim configurationSection As ConnectionStringsSection =
                       System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
            Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString

            Using Conn As New SqlConnection(strConnString)
                Conn.Open()
                Dim ConsultaSQL = "SELECT gru_nombre from grupo where gru_id = " & ID
                Grupo = New SqlCommand(ConsultaSQL, Conn).ExecuteScalar().ToString()
            End Using
            Codigo.Text = objSessionParams.intSelectedGroup
            txtgrupo.Text = Grupo

        End If


    End Sub
End Class
