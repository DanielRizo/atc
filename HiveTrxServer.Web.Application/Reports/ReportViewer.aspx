﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ReportPage.master" AutoEventWireup="false" CodeFile="ReportViewer.aspx.vb" Inherits="Reports_ReportViewer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Visor de Reportes
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <script type="text/javascript" src="../js/toogle.js"></script>

    <%--<script type="text/javascript" src="../js/jquery.tipsy.js"></script>--%>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

    <%--scripts menu--%>

    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.min.js") %>'></script>
    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery-ui-1.8.11.custom.min.js") %>'></script>
    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.js") %>'></script>
    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/msgAlert.js") %>'></script>

    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.contextMenu.js") %>'></script>
    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/menuMethods.js") %>'></script>

    <%--scripts edit table--%>



    <link rel="stylesheet" type="text/css" href="/style/bootstrap.min.css" />

    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/editTable.js") %>'></script>

    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/jquery.tabledit.js") %>'></script>

    <script type="text/javascript" src='<%= ResolveClientUrl("~/js/bootstrap.js") %>'></script>


    <div style="text-align: center;">
        <asp:Label ID="lblReportName" runat="server" Text="Label" Font-Bold="True" Font-Size="14pt"></asp:Label>
        <br />
        <asp:Label ID="lblOptionName1" runat="server" Text="Label" Font-Bold="True" Font-Size="11pt"></asp:Label><asp:Label ID="lblOptionValue1" runat="server" Text="Label" Font-Size="11pt"></asp:Label>
        <br />
        <asp:Label ID="lblOptionName2" runat="server" Text="Label" Font-Bold="True" Font-Size="11pt"></asp:Label><asp:Label ID="lblOptionValue2" runat="server" Text="Label" Font-Size="11pt"></asp:Label>
        <br />
        <asp:Label ID="lblOptionName3" runat="server" Text="Label" Font-Bold="True" Font-Size="11pt"></asp:Label><asp:Label ID="lblOptionValue3" runat="server" Text="Label" Font-Size="11pt"></asp:Label>
        <br />
        <asp:Label ID="lblDate1" runat="server" Text="Fecha Inicial:" Font-Bold="True" Font-Size="11pt"></asp:Label>
        &nbsp;&nbsp;
            <asp:Label ID="lblStartDate" runat="server" Text="Label" Font-Size="10pt"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="lblDate2" runat="server" Text="Fecha Final:" Font-Bold="True" Font-Size="11pt"></asp:Label>
        &nbsp;&nbsp;            
            <asp:Label ID="lblEndDate" runat="server" Text="Label" Font-Size="10pt"></asp:Label>
    </div>

    <br />
    <asp:LinkButton ID="lnkExportExcel" runat="server">
        Exportar a Excel (M&aacute;s detalles...)
        <asp:Image ID="imgExcel" runat="server" ImageUrl="~/img/excel.gif" />
    </asp:LinkButton>
    <br />
    <br />


    <asp:GridView ID="grdLogXUser" runat="server"
        AllowPaging="True" AutoGenerateColumns="False" CellPadding="4"
        DataSourceID="dsLogXUser" ForeColor="#333333"
        CssClass="mGridCenter"
        EmptyDataText="No existen acciones registradas del usuario para el período dado."
        HorizontalAlign="Center" PageSize="25" Visible="False">
        <RowStyle BackColor="White" ForeColor="White" />
        <Columns>
            <asp:BoundField DataField="log_fecha" HeaderText="Fecha" SortExpression="log_fecha"></asp:BoundField>
            <asp:BoundField DataField="usu_login" HeaderText="Login" SortExpression="usu_login"></asp:BoundField>
            <asp:BoundField DataField="usu_nombre" HeaderText="Nombre Usuario" SortExpression="usu_nombre"></asp:BoundField>
            <asp:BoundField DataField="login_ip" HeaderText="Ip" SortExpression="login_ip"></asp:BoundField>
            <asp:BoundField DataField="Objeto" HeaderText="Objeto del Sistema" SortExpression="Objeto"></asp:BoundField>
            <asp:BoundField DataField="Metodo" HeaderText="Método" SortExpression="Metodo"></asp:BoundField>
            <asp:BoundField DataField="TipoMensaje" HeaderText="Tipo de Mensaje" SortExpression="TipoMensaje"></asp:BoundField>
            <asp:BoundField DataField="log_mensaje1" HeaderText="Info. Adicional" SortExpression="log_mensaje1"></asp:BoundField>
        </Columns>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
            CssClass="pgr" Font-Underline="False" />
        <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid"
            BorderWidth="1px" Width="1000px" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <SortedAscendingCellStyle BackColor="#F8FAFA" />

    </asp:GridView>
    <asp:SqlDataSource ID="dsLogXUser" runat="server"
        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
        SelectCommand="sp_webReporteAuditoria"
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:Parameter Name="userID" Type="Int64" />
            <asp:Parameter Name="startDate" Type="DateTime" />
            <asp:Parameter Name="endDate" Type="DateTime" />
        </SelectParameters>
    </asp:SqlDataSource>

    <div style="overflow: auto; width: auto; height: auto;">
        <asp:GridView ID="grdLogXInventory" runat="server"
            AllowPaging="True" AutoGenerateColumns="False" CellPadding="4"
            DataSourceID="dsLogXInventory" ForeColor="#333333"
            CssClass="mGridCenter" GridLines="Both"
            Width="1700"
            EmptyDataText="No existe log de inventario para los filtros aplicados."
            HorizontalAlign="Center" PageSize="5" Visible="False">
            <RowStyle BackColor="White" ForeColor="White" />
            <Columns>
                <asp:BoundField DataField="tra_fecha" HeaderText="Fecha" SortExpression="tra_fecha" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                <asp:BoundField DataField="tra_cliente_pos" HeaderText="Cliente" SortExpression="tra_cliente_pos" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                <asp:BoundField DataField="gru_nombre" HeaderText="Grupo" SortExpression="gru_nombre" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                <asp:BoundField DataField="tra_serial_pos" HeaderText="Serial Terminal" SortExpression="tra_serial_pos" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                <asp:BoundField DataField="ter_terminalId" HeaderText="Terminal ID" SortExpression="ter_terminalId"></asp:BoundField>
                <asp:BoundField DataField="tra_imei_pos" HeaderText="Imei Terminal" SortExpression="tra_imei_pos"></asp:BoundField>
                <asp:BoundField DataField="tra_codigo_producto" HeaderText="Codigo de Producto" SortExpression="tra_codigo_producto" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                <asp:BoundField DataField="tra_marca_terminal" HeaderText="Marca" SortExpression="tra_marca_terminal" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                <asp:BoundField DataField="tra_tipo_terminal" HeaderText="Tipo" SortExpression="tra_tipo_terminal" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                <asp:BoundField DataField="tra_codigo_respuesta" HeaderText="C&oacute;d. Rta." SortExpression="tra_codigo_respuesta" ItemStyle-HorizontalAlign="Center">
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="tra_mensaje_respuesta" HeaderText="Mensaje Respuesta" SortExpression="tra_mensaje_respuesta" ItemStyle-Font-Size="Smaller">
                    <ItemStyle Font-Size="Smaller"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="tra_estado_actualizacion" HeaderText="Estado" SortExpression="tra_estado_actualizacion" ItemStyle-Font-Size="Smaller" ItemStyle-HorizontalAlign="Center">
                    <ItemStyle Font-Size="Smaller"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="tra_version_software_pos" HeaderText="Versi&oacute;n Software" SortExpression="tra_version_software_pos" ItemStyle-Font-Size="Smaller">
                    <ItemStyle Font-Size="Smaller"></ItemStyle>
                </asp:BoundField>


                <asp:TemplateField HeaderText="Aplicaciones POS" HeaderStyle-Width="9%" HeaderStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <div id="posApps" style="overflow: auto; width: 150px; height: 125px;">
                            <asp:Label ID="lblPosApps" runat="server" ToolTip='Listado de aplicaciones en la Terminal' Text='<%# Bind("tra_aplicaciones_pos")%>' Font-Size="Smaller"></asp:Label>
                        </div>
                    </ItemTemplate>
                    <%--<ControlStyle Width="30px" /> 
                <ItemStyle Width="30px" />--%>
                </asp:TemplateField>
                <asp:BoundField DataField="tra_aplicaciones_a_instalar" HeaderText="Aplicaciones a Instalar" HeaderStyle-Width="9%" SortExpression="tra_aplicaciones_a_instalar" ItemStyle-Font-Size="Smaller">
                    <ItemStyle Font-Size="Smaller"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="tra_fecha_descarga" HeaderText="Fecha Descarga" SortExpression="tra_fecha_descarga" ItemStyle-Font-Size="Smaller" ItemStyle-HorizontalAlign="Center">
                    <ItemStyle Font-Size="Smaller"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="tra_datos_descarga" HeaderText="IP/Puerto Descarga" SortExpression="tra_datos_descarga"></asp:BoundField>
            </Columns>
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
                CssClass="pgr" Font-Underline="False" />
            <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" />
            <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid"
                BorderWidth="1px" Width="20px" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>

    </div>
    <asp:SqlDataSource ID="dsLogXInventory" runat="server"
        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
        SelectCommand="sp_webReporteInventario"
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:Parameter Name="groupID" Type="Int64" />
            <asp:Parameter Name="terminalID" Type="String" />
            <asp:Parameter Name="customerID" Type="Int64" />
            <asp:Parameter Name="startDate" Type="DateTime" />
            <asp:Parameter Name="endDate" Type="DateTime" />
        </SelectParameters>
    </asp:SqlDataSource>


    <asp:GridView ID="grdLogXDownload" runat="server"
        AllowPaging="True" AutoGenerateColumns="False" CellPadding="4"
        DataSourceID="dsLogXDownload" ForeColor="#333333"
        CssClass="mGridCenter"
        EmptyDataText="No existe log de descarga para los filtros aplicados."
        HorizontalAlign="Center" PageSize="25" Visible="False">
        <RowStyle BackColor="White" ForeColor="White" />
        <Columns>
            <asp:BoundField DataField="tra_fecha" HeaderText="Fecha" SortExpression="tra_fecha"></asp:BoundField>
            <asp:BoundField DataField="tra_cliente_pos" HeaderText="Cliente" SortExpression="tra_cliente_pos"></asp:BoundField>
            <asp:BoundField DataField="gru_nombre" HeaderText="Grupo" SortExpression="gru_nombre"></asp:BoundField>
            <asp:BoundField DataField="tra_serial_pos" HeaderText="Serial Terminal" SortExpression="tra_serial_pos"></asp:BoundField>
            <asp:BoundField DataField="tra_imei_pos" HeaderText="Imei Terminal" SortExpression="tra_imei_pos"></asp:BoundField>
            <asp:BoundField DataField="tra_marca_terminal" HeaderText="Marca" SortExpression="tra_marca_terminal"></asp:BoundField>
            <asp:BoundField DataField="tra_tipo_terminal" HeaderText="Tipo" SortExpression="tra_tipo_terminal"></asp:BoundField>
            <asp:BoundField DataField="tra_codigo_respuesta" HeaderText="C&oacute;d. Rta." SortExpression="tra_codigo_respuesta" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
            <asp:BoundField DataField="tra_mensaje_respuesta" HeaderText="Mensaje Respuesta" SortExpression="tra_mensaje_respuesta"></asp:BoundField>
            <asp:BoundField DataField="tra_estado_actualizacion" HeaderText="Estado" SortExpression="tra_estado_actualizacion"></asp:BoundField>
            <asp:BoundField DataField="apl_nombre_tms" HeaderText="Nombre Aplicaci&oacute;n" SortExpression="apl_nombre_tms"></asp:BoundField>
            <asp:BoundField DataField="tra_porcentaje_descarga" HeaderText="% de Descarga" SortExpression="tra_porcentaje_descarga"></asp:BoundField>
        </Columns>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
            CssClass="pgr" Font-Underline="False" />
        <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid"
            BorderWidth="1px" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>
    <asp:SqlDataSource ID="dsLogXDownload" runat="server"
        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
        SelectCommand="sp_webReporteDescarga"
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:Parameter Name="groupID" Type="Int64" />
            <asp:Parameter Name="terminalID" Type="String" />
            <asp:Parameter Name="customerID" Type="Int64" />
            <asp:Parameter Name="startDate" Type="DateTime" />
            <asp:Parameter Name="endDate" Type="DateTime" />
        </SelectParameters>
    </asp:SqlDataSource>

    <div style="overflow: auto; width: auto; height: auto;">
        <asp:GridView ID="grdTerminals" runat="server"
            AllowPaging="True" AutoGenerateColumns="False" CellPadding="4"
            DataSourceID="dsTerminals" ForeColor="#333333"
            CssClass="mGridCenter" GridLines="Both"
            Width="1700"
            EmptyDataText="No existen terminales para los filtros aplicados."
            HorizontalAlign="Center" PageSize="25" Visible="False">
            <RowStyle BackColor="White" ForeColor="White" />
            <Columns>
                <asp:BoundField DataField="ter_fecha_creacion" HeaderText="Fecha de Creacion" SortExpression="ter_fecha_creacion"></asp:BoundField>
                <asp:BoundField DataField="fecha_consulta" HeaderText="Fecha ultima Consulta" SortExpression="fecha_consulta"></asp:BoundField>
                <asp:BoundField DataField="aplicaciones_instaladas" HeaderText="Aplicaciones Instaladas" SortExpression="aplicaciones_instaladas" HeaderStyle-Width="8%"></asp:BoundField>
                <asp:BoundField DataField="aplicaciones_pendientes" HeaderText="Aplicaciones Pendientes" SortExpression="aplicaciones_pendientes"></asp:BoundField>
                <asp:BoundField DataField="gru_nombre" HeaderText="Grupo" SortExpression="gru_nombre"></asp:BoundField>
                <asp:BoundField DataField="latitud_gps" HeaderText="Latitud gps" SortExpression="latitud_gps"></asp:BoundField>
                <asp:BoundField DataField="longitud_gps" HeaderText="Longitud gps" SortExpression="longitud_gps"></asp:BoundField>
                <asp:BoundField DataField="ter_serial" HeaderText="Serial Terminal" SortExpression="ter_serial"></asp:BoundField>
                <asp:BoundField DataField="ter_fecha_ultima_inicializacion" HeaderText="Fecha Inicializacion" SortExpression="ter_fecha_ultima_inicializacion"></asp:BoundField>
                <asp:BoundField DataField="ter_flag_inicializacion" HeaderText="Estado Inicializacion" SortExpression="ter_flag_inicializacion"></asp:BoundField>
                <asp:BoundField DataField="ter_imei" HeaderText="Imei Terminal" SortExpression="ter_imei"></asp:BoundField>
                <asp:BoundField DataField="ter_marca_terminal" HeaderText="Marca" SortExpression="ter_marca_terminal"></asp:BoundField>
                <asp:BoundField DataField="ter_tipo_terminal" HeaderText="Tipo" SortExpression="ter_tipo_terminal"></asp:BoundField>
                <asp:BoundField DataField="ter_serial_sim" HeaderText="Serial SIM" SortExpression="ter_serial_sim"></asp:BoundField>
                <asp:BoundField DataField="ter_estado_actualizacion" HeaderText="Estado" SortExpression="ter_estado_actualizacion"></asp:BoundField>
                <asp:BoundField DataField="ter_mensaje_actualizacion" HeaderText="Mensaje de Actualizaci&oacute;n" SortExpression="ter_mensaje_actualizacion"></asp:BoundField>
                <asp:TemplateField HeaderText="Estado" SortExpression="ter_estado">
                    <ItemTemplate>
                        <asp:CheckBox ID="editChk1" runat="server" Checked='<%# Bind("ter_estado")%>' Enabled="false" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Prioridad Grupo?" SortExpression="ter_prioridad_grupo">
                    <ItemTemplate>
                        <asp:CheckBox ID="editChk2" runat="server" Checked='<%# Bind("ter_prioridad_grupo")%>' Enabled="false" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Actualizar?" SortExpression="ter_actualizar">
                    <ItemTemplate>
                        <asp:CheckBox ID="editChk3" runat="server" Checked='<%# Bind("ter_actualizar")%>' Enabled="false" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:BoundField DataField="ter_fecha_programacion" HeaderText="Fecha Descarga" SortExpression="ter_fecha_programacion"></asp:BoundField>
                <asp:BoundField DataField="ter_datos_descarga" HeaderText="Datos Descarga" SortExpression="ter_datos_descarga"></asp:BoundField>
            </Columns>
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
                CssClass="pgr" Font-Underline="False" />
            <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid"
                BorderWidth="1px" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>
    </div>
    <asp:SqlDataSource ID="dsTerminals" runat="server"
        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
        SelectCommand="sp_webReporteTerminales"
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:Parameter Name="groupID" Type="Int64" />
            <asp:Parameter Name="terminalID" Type="String" />
            <asp:Parameter Name="customerID" Type="Int64" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:GridView ID="grdLogXMDM" runat="server"
        AllowPaging="True" AutoGenerateColumns="False" CellPadding="4"
        DataSourceID="dsLogXMDM" ForeColor="#333333"
        CssClass="mGridCenter"
        EmptyDataText="No existe log de MDM para los filtros aplicados."
        HorizontalAlign="Center" PageSize="25" Visible="False">
        <RowStyle BackColor="White" ForeColor="White" />
        <Columns>
            <asp:BoundField DataField="tra_fecha" HeaderText="Fecha Conexión" SortExpression="tra_fecha"></asp:BoundField>
            <asp:BoundField DataField="tra_operacion" HeaderText="Tipo Operación" SortExpression="tra_operacion"></asp:BoundField>
            <asp:BoundField DataField="tra_terminal_id_device" HeaderText="Dispositivo ID" SortExpression="tra_terminal_id_device"></asp:BoundField>
            <asp:BoundField DataField="gru_nombre" HeaderText="Grupo" SortExpression="gru_nombre"></asp:BoundField>
            <asp:BoundField DataField="tra_terminal_ip" HeaderText="Dispositivo IP" SortExpression="tra_terminal_ip"></asp:BoundField>
            <asp:TemplateField HeaderText="Apps Instaladas" ItemStyle-Width="200px">
                <ItemTemplate>
                    <div id="posApps" style="overflow: auto; width: 200px; height: 85px;">
                        <asp:Label ID="lblPosApps" runat="server" ToolTip='Listado de aplicaciones instaladas en el Dispositivo' Text='<%# Bind("tra_terminal_apks_instalados") %>' Font-Size="Smaller"></asp:Label>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="tra_apk_descargado" HeaderText="App Descargada" SortExpression="tra_apk_descargado" ItemStyle-Font-Size="Smaller"></asp:BoundField>
            <asp:BoundField DataField="tra_xml_descargado" HeaderText="Xml Descargado" SortExpression="tra_xml_descargado" ItemStyle-Font-Size="Smaller"></asp:BoundField>
            <asp:BoundField DataField="tra_img_descargado" HeaderText="Img Descargado" SortExpression="tra_img_descargado" ItemStyle-Font-Size="Smaller"></asp:BoundField>
            <asp:BoundField DataField="tra_estado_final" HeaderText="Estado Final" SortExpression="tra_estado_final"></asp:BoundField>
        </Columns>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
            CssClass="pgr" Font-Underline="False" />
        <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid"
            BorderWidth="1px" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>
    <asp:SqlDataSource ID="dsLogXMDM" runat="server"
        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
        SelectCommand="sp_webReporteMDM"
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:Parameter Name="groupID" Type="Int64" />
            <asp:Parameter Name="terminalID" Type="Int64" />
            <asp:Parameter Name="customerID" Type="Int64" />
            <asp:Parameter Name="startDate" Type="DateTime" />
            <asp:Parameter Name="endDate" Type="DateTime" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:GridView ID="GrdApks" runat="server"
        AllowPaging="True" AutoGenerateColumns="False" CellPadding="4"
        DataSourceID="dsApks" ForeColor="#333333"
        CssClass="mGridCenter"
        EmptyDataText="No existe log de MDM para los filtros aplicados."
        HorizontalAlign="Center" PageSize="25" Visible="False">
        <RowStyle BackColor="White" ForeColor="White" />
        <Columns>
            <asp:BoundField DataField="Gru_nombre" HeaderText=" Nombtre Grupo" SortExpression="Gru_nombre"></asp:BoundField>
            <asp:BoundField DataField="ter_Serial" HeaderText="Serial Dispositivo" SortExpression="ter_Serial"></asp:BoundField>
            <asp:BoundField DataField="desc_apk_version" HeaderText="Version Apk" SortExpression="desc_apk_version"></asp:BoundField>
            <asp:BoundField DataField="desc_apk_archivo" HeaderText="Nombre APK" SortExpression="desc_apk_archivo"></asp:BoundField>
            <asp:BoundField DataField="descripcion" HeaderText="Estado Actualizacion" SortExpression="descripcion"></asp:BoundField>

        </Columns>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
            CssClass="pgr" Font-Underline="False" />
        <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid"
            BorderWidth="1px" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>
    <asp:SqlDataSource ID="dsApks" runat="server"
        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
        SelectCommand="sp_webReporteMDM_APK"
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:Parameter Name="groupID" Type="Int64" />
            <asp:Parameter Name="terminalID" Type="Int64" />
            <asp:Parameter Name="customerID" Type="Int64" />

        </SelectParameters>
    </asp:SqlDataSource>

    <asp:GridView ID="grdTerminalsMDM" runat="server"
        AllowPaging="True" AutoGenerateColumns="False" CellPadding="4"
        DataSourceID="dsTerminalsMDM" ForeColor="#333333"
        CssClass="mGridCenter"
        EmptyDataText="No existen terminales Android para los filtros aplicados."
        HorizontalAlign="Center" PageSize="25" Visible="False">
        <RowStyle BackColor="White" ForeColor="White" />
        <Columns>
            <asp:BoundField DataField="fecha_consulta" HeaderText="Fecha ultima Consulta" SortExpression="fecha_consulta"></asp:BoundField>
            <asp:TemplateField HeaderText="Aplicaciones Instaladas">
                <ItemTemplate>
                    <div id="posApps" style="overflow: auto; width: 130px; height: 100px;">
                        <asp:Label ID="lblPosApps" runat="server" ToolTip='Listado de aplicaciones en la Terminal' Text='<%# Bind("aplicaciones_instaladas")%>' Font-Size="Smaller"></asp:Label>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Aplicaciones pendientes">
                <ItemTemplate>
                    <div id="posApps" style="overflow: auto; width: 130px; height: 100px;">
                        <asp:Label ID="lblPosApps" runat="server" ToolTip='Listado de aplicaciones en la Terminal' Text='<%# Bind("aplicaciones_pendientes")%>' Font-Size="Smaller"></asp:Label>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="latitud_gps" HeaderText="Latitud GPS" SortExpression="latitud_gps"></asp:BoundField>
            <asp:BoundField DataField="longitud_gps" HeaderText="Longitud GPS" SortExpression="longitud_gps"></asp:BoundField>
            <asp:BoundField DataField="gru_nombre" HeaderText="Grupo" SortExpression="gru_nombre"></asp:BoundField>
            <asp:BoundField DataField="ter_marca_terminal" HeaderText="Marca" SortExpression="ter_marca_terminal"></asp:BoundField>
            <asp:BoundField DataField="ter_tipo_terminal" HeaderText="Tipo" SortExpression="ter_tipo_terminal"></asp:BoundField>
            <asp:BoundField DataField="ter_serial" HeaderText="Dispositivo ID" SortExpression="ter_serial"></asp:BoundField>
            <asp:BoundField DataField="ter_estado_actualizacion" HeaderText="Estado" SortExpression="ter_estado_actualizacion"></asp:BoundField>
            <asp:BoundField DataField="ter_id_heracles" HeaderText="Heracles ID" SortExpression="ter_id_heracles"></asp:BoundField>
            <asp:BoundField DataField="ter_ip_dispositivo" HeaderText="IP" SortExpression="ter_ip_dispositivo"></asp:BoundField>

            <asp:TemplateField HeaderText="Comp. 1" SortExpression="ter_logo">
                <ItemTemplate>
                    <asp:Image ID="imgUrlIconCompA" runat="server" ImageUrl='<%# Bind("ter_logo_componente1")%>' ToolTip='<%# Bind("ter_fecha_inyeccion_componente1")%>' />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Comp. 2" SortExpression="ter_logo">
                <ItemTemplate>
                    <asp:Image ID="imgUrlIconCompB" runat="server" ImageUrl='<%# Bind("ter_logo_componente2")%>' ToolTip='<%# Bind("ter_fecha_inyeccion_componente2")%>' />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Llave" SortExpression="ter_logo">
                <ItemTemplate>
                    <asp:Image ID="imgUrlIconStatusKey" runat="server" ImageUrl='<%# Bind("ter_logo_llave")%>' ToolTip='<%# Bind("ter_fecha_ultima_carga_llave") %>' />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:BoundField DataField="apk_banco" HeaderText="APK Banco" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Smaller" SortExpression="apk_banco"></asp:BoundField>
            <asp:BoundField DataField="configuracion" HeaderText="Configuración" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Smaller" SortExpression="configuracion"></asp:BoundField>
            <asp:BoundField DataField="imagenes" HeaderText="Imágenes" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Smaller" SortExpression="imagenes"></asp:BoundField>
        </Columns>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
            CssClass="pgr" Font-Underline="False" />
        <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid"
            BorderWidth="1px" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>
    <asp:SqlDataSource ID="dsTerminalsMDM" runat="server"
        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
        SelectCommand="sp_webReporteTerminalesMDM"
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:Parameter Name="groupID" Type="Int64" />
            <asp:Parameter Name="terminalID" Type="Int64" />
            <asp:Parameter Name="customerID" Type="Int64" />
        </SelectParameters>
    </asp:SqlDataSource>


    <asp:GridView ID="grdAndroid" runat="server"
        AllowPaging="True" AutoGenerateColumns="False" CellPadding="4"
        DataSourceID="dsTerminalsAndroid" ForeColor="#333333"
        CssClass="mGridCenter"
        EmptyDataText="No existen terminales Android para los filtros aplicados."
        HorizontalAlign="Center" PageSize="25" Visible="False">
        <RowStyle BackColor="White" ForeColor="White" />
        <Columns>
            <asp:BoundField DataField="fecha_consulta" HeaderText="Fecha ultima Consulta" SortExpression="fecha_consulta"></asp:BoundField>
            <asp:TemplateField HeaderText="Aplicaciones Instaladas">
                <ItemTemplate>
                    <div id="posApps" style="overflow: auto; width: 130px; height: 100px;">
                        <asp:Label ID="lblPosApps" runat="server" ToolTip='Listado de aplicaciones en la Terminal' Text='<%# Bind("aplicaciones_instaladas")%>' Font-Size="Smaller"></asp:Label>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Aplicaciones pendientes">
                <ItemTemplate>
                    <div id="posApps" style="overflow: auto; width: 130px; height: 100px;">
                        <asp:Label ID="lblPosApps" runat="server" ToolTip='Listado de aplicaciones en la Terminal' Text='<%# Bind("aplicaciones_pendientes")%>' Font-Size="Smaller"></asp:Label>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="latitud_gps" HeaderText="Latitud GPS" SortExpression="latitud_gps"></asp:BoundField>
            <asp:BoundField DataField="longitud_gps" HeaderText="Longitud GPS" SortExpression="longitud_gps"></asp:BoundField>
            <asp:BoundField DataField="gru_nombre" HeaderText="Grupo" SortExpression="gru_nombre"></asp:BoundField>
            <asp:BoundField DataField="ter_marca_terminal" HeaderText="Marca" SortExpression="ter_marca_terminal"></asp:BoundField>
            <asp:BoundField DataField="ter_tipo_terminal" HeaderText="Tipo" SortExpression="ter_tipo_terminal"></asp:BoundField>
            <asp:BoundField DataField="ter_serial" HeaderText="Dispositivo ID" SortExpression="ter_serial"></asp:BoundField>
            <asp:BoundField DataField="ter_estado_actualizacion" HeaderText="Estado" SortExpression="ter_estado_actualizacion"></asp:BoundField>
            <asp:BoundField DataField="ter_id_heracles" HeaderText="Heracles ID" SortExpression="ter_id_heracles"></asp:BoundField>
            <asp:BoundField DataField="ter_ip_dispositivo" HeaderText="IP" SortExpression="ter_ip_dispositivo"></asp:BoundField>

            <asp:TemplateField HeaderText="Comp. 1" SortExpression="ter_logo">
                <ItemTemplate>
                    <asp:Image ID="imgUrlIconCompA" runat="server" ImageUrl='<%# Bind("ter_logo_componente1")%>' ToolTip='<%# Bind("ter_fecha_inyeccion_componente1")%>' />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Comp. 2" SortExpression="ter_logo">
                <ItemTemplate>
                    <asp:Image ID="imgUrlIconCompB" runat="server" ImageUrl='<%# Bind("ter_logo_componente2")%>' ToolTip='<%# Bind("ter_fecha_inyeccion_componente2")%>' />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Llave" SortExpression="ter_logo">
                <ItemTemplate>
                    <asp:Image ID="imgUrlIconStatusKey" runat="server" ImageUrl='<%# Bind("ter_logo_llave")%>' ToolTip='<%# Bind("ter_fecha_ultima_carga_llave") %>' />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:BoundField DataField="apk_banco" HeaderText="APK Banco" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Smaller" SortExpression="apk_banco"></asp:BoundField>
            <asp:BoundField DataField="configuracion" HeaderText="Configuración" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Smaller" SortExpression="configuracion"></asp:BoundField>
            <asp:BoundField DataField="imagenes" HeaderText="Imágenes" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Smaller" SortExpression="imagenes"></asp:BoundField>
        </Columns>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
            CssClass="pgr" Font-Underline="False" />
        <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid"
            BorderWidth="1px" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>
    <asp:SqlDataSource ID="dsTerminalsAndroid" runat="server"
        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
        SelectCommand="sp_webReporteTerminalesMDM"
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:Parameter Name="groupID" Type="Int64" />
            <asp:Parameter Name="terminalID" Type="Int64" />
            <asp:Parameter Name="customerID" Type="Int64" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:GridView ID="grdLogXKeyLoading" runat="server"
        AllowPaging="True" AutoGenerateColumns="False" CellPadding="4"
        DataSourceID="dsLogXKeyLoading" ForeColor="#333333"
        CssClass="mGridCenter"
        EmptyDataText="No existe log de Transacciones de Carga de Llaves para los filtros aplicados."
        HorizontalAlign="Center" PageSize="25" Visible="False">
        <RowStyle BackColor="White" ForeColor="White" />
        <Columns>
            <asp:BoundField DataField="tra_fecha" HeaderText="Fecha Conexión" SortExpression="tra_fecha"></asp:BoundField>
            <asp:BoundField DataField="tra_operacion" HeaderText="Tipo Operación" SortExpression="tra_operacion"></asp:BoundField>
            <asp:BoundField DataField="tra_terminal_id_device" HeaderText="Dispositivo ID" SortExpression="tra_terminal_id_device"></asp:BoundField>
            <asp:BoundField DataField="gru_nombre" HeaderText="Grupo" SortExpression="gru_nombre"></asp:BoundField>
            <asp:BoundField DataField="tra_terminal_ip" HeaderText="Dispositivo IP" SortExpression="tra_terminal_ip"></asp:BoundField>
            <asp:BoundField DataField="tra_estado_final" HeaderText="Estado Final" SortExpression="tra_estado_final"></asp:BoundField>
        </Columns>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
            CssClass="pgr" Font-Underline="False" />
        <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid"
            BorderWidth="1px" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>
    <asp:SqlDataSource ID="dsLogXKeyLoading" runat="server"
        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
        SelectCommand="sp_webReporteCargaLlaves"
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:Parameter Name="groupID" Type="Int64" />
            <asp:Parameter Name="terminalID" Type="Int64" />
            <asp:Parameter Name="customerID" Type="Int64" />
            <asp:Parameter Name="startDate" Type="DateTime" />
            <asp:Parameter Name="endDate" Type="DateTime" />
        </SelectParameters>
    </asp:SqlDataSource>


    <asp:GridView ID="grdTransactions" runat="server"
        AllowPaging="False" AutoGenerateColumns="False" CellPadding="4"
        DataSourceID="sqTransactions" ForeColor="#333333"
        CssClass="mGrid"
        EmptyDataText="No existe log de Transacciones."
        HorizontalAlign="Center" Visible="False" DataKeyNames="ct_id" Width="100%">
        <RowStyle BackColor="#E3EAEB" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
            CssClass="pgr" Font-Underline="False" />
        <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <Columns>
            <asp:BoundField DataField="ct_id" HeaderText="ct_id" InsertVisible="False" ReadOnly="True" SortExpression="ct_id" />
            <asp:BoundField DataField="ct_fecha_hora" HeaderText="ct_fecha_hora" SortExpression="ct_fecha_hora" />
            <asp:BoundField DataField="ct_ip" HeaderText="ct_ip" SortExpression="ct_ip" />
            <asp:BoundField DataField="ct_puerto" HeaderText="ct_puerto" SortExpression="ct_puerto" />
            <%-- <asp:BoundField DataField="ct_incoming_data" HeaderText="ct_incoming_data" SortExpression="ct_incoming_data" />
            <asp:BoundField DataField="ct_outgoing_data" HeaderText="ct_outgoing_data" SortExpression="ct_outgoing_data" />
            <asp:BoundField DataField="fi_type" HeaderText="fi_type" SortExpression="fi_type" />
            <asp:BoundField DataField="fi_msgType" HeaderText="fi_msgType" SortExpression="fi_msgType" />
            <asp:BoundField DataField="fi_pcc" HeaderText="fi_pcc" SortExpression="fi_pcc" />
            <asp:BoundField DataField="fi_amount" HeaderText="fi_amount" SortExpression="fi_amount" />
            <asp:BoundField DataField="fi_stan_11" HeaderText="fi_stan_11" SortExpression="fi_stan_11" />
            <asp:BoundField DataField="fi_stan_62" HeaderText="fi_stan_62" SortExpression="fi_stan_62" />
            <asp:BoundField DataField="fi_expirationDate" HeaderText="fi_expirationDate" SortExpression="fi_expirationDate" />
            <asp:BoundField DataField="fi_posentrymode" HeaderText="fi_posentrymode" SortExpression="fi_posentrymode" />
            <asp:BoundField DataField="fi_track2" HeaderText="fi_track2" SortExpression="fi_track2" />
            <asp:BoundField DataField="fi_pan" HeaderText="fi_pan" SortExpression="fi_pan" />--%>
            <asp:BoundField DataField="fi_tid" HeaderText="fi_tid" SortExpression="fi_tid" />
            <asp:BoundField DataField="fi_merchantId" HeaderText="fi_merchantId" SortExpression="fi_merchantId" />
            <%-- <asp:BoundField DataField="fi_emvData" HeaderText="fi_emvData" SortExpression="fi_emvData" />
            <asp:BoundField DataField="fi_pinBlock" HeaderText="fi_pinBlock" SortExpression="fi_pinBlock" />
            <asp:BoundField DataField="fi_cvc" HeaderText="fi_cvc" SortExpression="fi_cvc" />
             <asp:BoundField DataField="fi_nombreTarjeta" HeaderText="fi_nombreTarjeta" SortExpression="fi_nombreTarjeta" />
             <asp:BoundField DataField="fi_encodedBlock1" HeaderText="fi_encodedBlock1" SortExpression="fi_encodedBlock1" />
             <asp:BoundField DataField="fi_encodedBlock2" HeaderText="fi_encodedBlock2" SortExpression="fi_encodedBlock2" />
             <asp:BoundField DataField="fi_encodedBlock3" HeaderText="fi_encodedBlock3" SortExpression="fi_encodedBlock3" />
             <asp:BoundField DataField="fi_encodedBlock4" HeaderText="fi_encodedBlock4" SortExpression="fi_encodedBlock4" />
             <asp:BoundField DataField="fi_encodedBlock5" HeaderText="fi_encodedBlock5" SortExpression="fi_encodedBlock5" />
             <asp:BoundField DataField="fi_encodedBlock6" HeaderText="fi_encodedBlock6" SortExpression="fi_encodedBlock6" />
             <asp:BoundField DataField="fi_encodedBlock7" HeaderText="fi_encodedBlock7" SortExpression="fi_encodedBlock7" />
             <asp:BoundField DataField="fi_encodedBlock8" HeaderText="fi_encodedBlock8" SortExpression="fi_encodedBlock8" />
             <asp:BoundField DataField="fi_encodedBlock9" HeaderText="fi_encodedBlock9" SortExpression="fi_encodedBlock9" />
             <asp:BoundField DataField="fi_encodedBlock10" HeaderText="fi_encodedBlock10" SortExpression="fi_encodedBlock10" />
            <asp:BoundField DataField="bi_businessData" HeaderText="bi_businessData" SortExpression="bi_businessData" />--%>
            <asp:BoundField DataField="bi_CodigoTransaccion" HeaderText="bi_CodigoTransaccion" SortExpression="bi_CodigoTransaccion" />
            <asp:BoundField DataField="bi_CodigoTransaccionModificado" HeaderText="bi_CodigoTransaccionModificado" SortExpression="bi_CodigoTransaccionModificado" />
            <asp:BoundField DataField="bi_Usuario" HeaderText="bi_Usuario" SortExpression="bi_Usuario" />
            <asp:BoundField DataField="bi_FechaHora" HeaderText="bi_FechaHora" SortExpression="bi_FechaHora" />
            <asp:BoundField DataField="bi_TipoVenta" HeaderText="bi_TipoVenta" SortExpression="bi_TipoVenta" />
            <asp:BoundField DataField="bi_IdTransaction" HeaderText="bi_IdTransaction" SortExpression="bi_IdTransaction" />
            <%-- <asp:BoundField DataField="bi_Agency" HeaderText="bi_Agency" SortExpression="bi_Agency" />
            <asp:BoundField DataField="bi_Monto" HeaderText="bi_Monto" SortExpression="bi_Monto" />
            <asp:BoundField DataField="bi_Plan" HeaderText="bi_Plan" SortExpression="bi_Plan" />
            <asp:BoundField DataField="bi_CodigoEmisor" HeaderText="bi_CodigoEmisor" SortExpression="bi_CodigoEmisor" />
            <asp:BoundField DataField="bi_TipoCredito" HeaderText="bi_TipoCredito" SortExpression="bi_TipoCredito" />
            <asp:BoundField DataField="bi_NombreTipoCredito" HeaderText="bi_NombreTipoCredito" SortExpression="bi_NombreTipoCredito" />
            <asp:BoundField DataField="bi_NombreAerolinea" HeaderText="bi_NombreAerolinea" SortExpression="bi_NombreAerolinea" />
            <asp:BoundField DataField="bi_DateTime" HeaderText="bi_DateTime" SortExpression="bi_DateTime" />
            <asp:BoundField DataField="bi_IdRecord" HeaderText="bi_IdRecord" SortExpression="bi_IdRecord" />
            <asp:BoundField DataField="bi_IdSecuencialCliente" HeaderText="bi_IdSecuencialCliente" SortExpression="bi_IdSecuencialCliente" />
            <asp:BoundField DataField="bi_Operador" HeaderText="bi_Operador" SortExpression="bi_Operador" />
            <asp:BoundField DataField="bi_CodigoEstablecimiento" HeaderText="bi_CodigoEstablecimiento" SortExpression="bi_CodigoEstablecimiento" />
            <asp:BoundField DataField="bi_IVA" HeaderText="bi_IVA" SortExpression="bi_IVA" />
            <asp:BoundField DataField="bi_Num_Voucher" HeaderText="bi_Num_Voucher" SortExpression="bi_Num_Voucher" />
            <asp:BoundField DataField="bi_Ciudad" HeaderText="bi_Ciudad" SortExpression="bi_Ciudad" />
            <asp:BoundField DataField="bi_ValorServicio" HeaderText="bi_ValorServicio" SortExpression="bi_ValorServicio" />
            <asp:BoundField DataField="bi_ValorPropina" HeaderText="bi_ValorPropina" SortExpression="bi_ValorPropina" />
            <asp:BoundField DataField="bi_ValorMontoFijo" HeaderText="bi_ValorMontoFijo" SortExpression="bi_ValorMontoFijo" />
            <asp:BoundField DataField="bi_Tarifa" HeaderText="bi_Tarifa" SortExpression="bi_Tarifa" />
            <asp:BoundField DataField="bi_Impuestos" HeaderText="bi_Impuestos" SortExpression="bi_Impuestos" />
            <asp:BoundField DataField="bi_Interes" HeaderText="bi_Interes" SortExpression="bi_Interes" />
            <asp:BoundField DataField="bi_Boletos" HeaderText="bi_Boletos" SortExpression="bi_Boletos" />
            <asp:BoundField DataField="bi_NombrePAX" HeaderText="bi_NombrePAX" SortExpression="bi_NombrePAX" />
            <asp:BoundField DataField="vtc_json_request" HeaderText="vtc_json_request" SortExpression="vtc_json_request" />
            <asp:BoundField DataField="vtc_json_response" HeaderText="vtc_json_response" SortExpression="vtc_json_response" />
            <asp:BoundField DataField="fo_rspCode" HeaderText="fo_rspCode" SortExpression="fo_rspCode" />
            <asp:BoundField DataField="fo_authorization" HeaderText="fo_authorization" SortExpression="fo_authorization" />
            <asp:BoundField DataField="fo_msgResponse" HeaderText="fo_msgResponse" SortExpression="fo_msgResponse" />
            <asp:BoundField DataField="fo_emvData" HeaderText="fo_emvData" SortExpression="fo_emvData" />
            <asp:BoundField DataField="fo_encoded" HeaderText="fo_encoded" SortExpression="fo_encoded" />
            <asp:BoundField DataField="bo_businessData" HeaderText="bo_businessData" SortExpression="bo_businessData" />
            <asp:BoundField DataField="bo_ReturnCode" HeaderText="bo_ReturnCode" SortExpression="bo_ReturnCode" />
            <asp:BoundField DataField="bo_Description" HeaderText="bo_Description" SortExpression="bo_Description" />
            <asp:BoundField DataField="bo_DateTimeReturn" HeaderText="bo_DateTimeReturn" SortExpression="bo_DateTimeReturn" />
            <asp:BoundField DataField="bo_Autorization" HeaderText="bo_Autorization" SortExpression="bo_Autorization" />
            <asp:BoundField DataField="bo_Emisor" HeaderText="bo_Emisor" SortExpression="bo_Emisor" />
            <asp:BoundField DataField="bo_NumeroTarjeta" HeaderText="bo_NumeroTarjeta" SortExpression="bo_NumeroTarjeta" />
            <asp:BoundField DataField="bo_NumeroTarjetaEditada" HeaderText="bo_NumeroTarjetaEditada" SortExpression="bo_NumeroTarjetaEditada" />
            <asp:BoundField DataField="bo_FechaExpiracion" HeaderText="bo_FechaExpiracion" SortExpression="bo_FechaExpiracion" />
            <asp:BoundField DataField="bo_Nombre" HeaderText="bo_Nombre" SortExpression="bo_Nombre" />
            <asp:BoundField DataField="bo_CodigoEmisor" HeaderText="bo_CodigoEmisor" SortExpression="bo_CodigoEmisor" />
            <asp:BoundField DataField="bo_TipoConvenio" HeaderText="bo_TipoConvenio" SortExpression="bo_TipoConvenio" />
            <asp:BoundField DataField="bo_IdRecord" HeaderText="bo_IdRecord" SortExpression="bo_IdRecord" />
            <asp:BoundField DataField="bo_Tarifa" HeaderText="bo_Tarifa" SortExpression="bo_Tarifa" />
            <asp:BoundField DataField="bo_Impuestos" HeaderText="bo_Impuestos" SortExpression="bo_Impuestos" />
            <asp:BoundField DataField="bo_Interes" HeaderText="bo_Interes" SortExpression="bo_Interes" />
            <asp:BoundField DataField="bo_Total" HeaderText="bo_Total" SortExpression="bo_Total" />
            <asp:BoundField DataField="bo_Mensaje" HeaderText="bo_Mensaje" SortExpression="bo_Mensaje" />
            <asp:BoundField DataField="bo_Comision" HeaderText="bo_Comision" SortExpression="bo_Comision" />
            <asp:BoundField DataField="bo_Boletos" HeaderText="bo_Boletos" SortExpression="bo_Boletos" />
            <asp:BoundField DataField="bo_NombrePAX" HeaderText="bo_NombrePAX" SortExpression="bo_NombrePAX" />
            <asp:BoundField DataField="db_rsp_code" HeaderText="db_rsp_code" SortExpression="db_rsp_code" />
            <asp:BoundField DataField="db_msg" HeaderText="db_msg" SortExpression="db_msg" />
            <asp:BoundField DataField="db_error_code" HeaderText="db_error_code" SortExpression="db_error_code" />
            <asp:BoundField DataField="db_error_msg" HeaderText="db_error_msg" SortExpression="db_error_msg" />--%>
        </Columns>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
            CssClass="pgr" Font-Underline="False" />
        <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid"
            BorderWidth="1px" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>

    <asp:SqlDataSource ID="sqTransactions" runat="server" ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
        SelectCommand="sp_webReporteTransactions"
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:Parameter Name="tid" Type="string" />
            <asp:Parameter Name="merchantId" Type="string" />
            <asp:Parameter Name="codTranMod" Type="string" />
            <asp:Parameter Name="usuario" Type="string" />
            <asp:Parameter Name="startDate" Type="DateTime" />
            <asp:Parameter Name="endDate" Type="DateTime" />
        </SelectParameters>
    </asp:SqlDataSource>


    <asp:GridView ID="grdTransactionsHistorico" runat="server"
        AllowPaging="False" AutoGenerateColumns="False" CellPadding="4"
        DataSourceID="sqTransactionsHistorico" ForeColor="#333333"
        CssClass="mGrid"
        EmptyDataText="No existe log de Transacciones."
        HorizontalAlign="Center" Visible="False" DataKeyNames="ct_id" Width="100%">
        <RowStyle BackColor="#E3EAEB" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
            CssClass="pgr" Font-Underline="False" />
        <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <Columns>
            <asp:BoundField DataField="ct_id" HeaderText="ct_id" InsertVisible="False" ReadOnly="True" SortExpression="ct_id" />
            <asp:BoundField DataField="ct_fecha_hora" HeaderText="ct_fecha_hora" SortExpression="ct_fecha_hora" />
            <asp:BoundField DataField="ct_ip" HeaderText="ct_ip" SortExpression="ct_ip" />
            <asp:BoundField DataField="ct_puerto" HeaderText="ct_puerto" SortExpression="ct_puerto" />
            <%--<asp:BoundField DataField="ct_incoming_data" HeaderText="ct_incoming_data" SortExpression="ct_incoming_data" />
            <asp:BoundField DataField="ct_outgoing_data" HeaderText="ct_outgoing_data" SortExpression="ct_outgoing_data" />
            <asp:BoundField DataField="fi_type" HeaderText="fi_type" SortExpression="fi_type" />
            <asp:BoundField DataField="fi_msgType" HeaderText="fi_msgType" SortExpression="fi_msgType" />
            <asp:BoundField DataField="fi_pcc" HeaderText="fi_pcc" SortExpression="fi_pcc" />
            <asp:BoundField DataField="fi_amount" HeaderText="fi_amount" SortExpression="fi_amount" />
            <asp:BoundField DataField="fi_stan_11" HeaderText="fi_stan_11" SortExpression="fi_stan_11" />
            <asp:BoundField DataField="fi_stan_62" HeaderText="fi_stan_62" SortExpression="fi_stan_62" />
            <asp:BoundField DataField="fi_expirationDate" HeaderText="fi_expirationDate" SortExpression="fi_expirationDate" />
            <asp:BoundField DataField="fi_posentrymode" HeaderText="fi_posentrymode" SortExpression="fi_posentrymode" />
            <asp:BoundField DataField="fi_track2" HeaderText="fi_track2" SortExpression="fi_track2" />--%>
            <asp:BoundField DataField="fi_pan" HeaderText="fi_pan" SortExpression="fi_pan" />
            <asp:BoundField DataField="fi_tid" HeaderText="fi_tid" SortExpression="fi_tid" />
            <asp:BoundField DataField="fi_merchantId" HeaderText="fi_merchantId" SortExpression="fi_merchantId" />
            <%--<asp:BoundField DataField="fi_emvData" HeaderText="fi_emvData" SortExpression="fi_emvData" />
            <asp:BoundField DataField="fi_pinBlock" HeaderText="fi_pinBlock" SortExpression="fi_pinBlock" />
            <asp:BoundField DataField="fi_cvc" HeaderText="fi_cvc" SortExpression="fi_cvc" />
            <asp:BoundField DataField="fi_nombreTarjeta" HeaderText="fi_nombreTarjeta" SortExpression="fi_nombreTarjeta" />
            <asp:BoundField DataField="fi_encodedBlock1" HeaderText="fi_encodedBlock1" SortExpression="fi_encodedBlock1" />
            <asp:BoundField DataField="fi_encodedBlock2" HeaderText="fi_encodedBlock2" SortExpression="fi_encodedBlock2" />
            <asp:BoundField DataField="fi_encodedBlock3" HeaderText="fi_encodedBlock3" SortExpression="fi_encodedBlock3" />
            <asp:BoundField DataField="fi_encodedBlock4" HeaderText="fi_encodedBlock4" SortExpression="fi_encodedBlock4" />
            <asp:BoundField DataField="fi_encodedBlock5" HeaderText="fi_encodedBlock5" SortExpression="fi_encodedBlock5" />
            <asp:BoundField DataField="fi_encodedBlock6" HeaderText="fi_encodedBlock6" SortExpression="fi_encodedBlock6" />
            <asp:BoundField DataField="fi_encodedBlock7" HeaderText="fi_encodedBlock7" SortExpression="fi_encodedBlock7" />
            <asp:BoundField DataField="fi_encodedBlock8" HeaderText="fi_encodedBlock8" SortExpression="fi_encodedBlock8" />
            <asp:BoundField DataField="fi_encodedBlock9" HeaderText="fi_encodedBlock9" SortExpression="fi_encodedBlock9" />
            <asp:BoundField DataField="fi_encodedBlock10" HeaderText="fi_encodedBlock10" SortExpression="fi_encodedBlock10" />
            <asp:BoundField DataField="bi_businessData" HeaderText="bi_businessData" SortExpression="bi_businessData" />--%>
            <asp:BoundField DataField="bi_CodigoTransaccion" HeaderText="bi_CodigoTransaccion" SortExpression="bi_CodigoTransaccion" />
            <asp:BoundField DataField="bi_CodigoTransaccionModificado" HeaderText="bi_CodigoTransaccionModificado" SortExpression="bi_CodigoTransaccionModificado" />
            <asp:BoundField DataField="bi_Usuario" HeaderText="bi_Usuario" SortExpression="bi_Usuario" />
            <asp:BoundField DataField="bi_FechaHora" HeaderText="bi_FechaHora" SortExpression="bi_FechaHora" />
            <asp:BoundField DataField="bi_TipoVenta" HeaderText="bi_TipoVenta" SortExpression="bi_TipoVenta" />
            <asp:BoundField DataField="bi_IdTransaction" HeaderText="bi_IdTransaction" SortExpression="bi_IdTransaction" />
            <%--<asp:BoundField DataField="bi_Agency" HeaderText="bi_Agency" SortExpression="bi_Agency" />
            <asp:BoundField DataField="bi_Monto" HeaderText="bi_Monto" SortExpression="bi_Monto" />
            <asp:BoundField DataField="bi_Plan" HeaderText="bi_Plan" SortExpression="bi_Plan" />
            <asp:BoundField DataField="bi_CodigoEmisor" HeaderText="bi_CodigoEmisor" SortExpression="bi_CodigoEmisor" />
            <asp:BoundField DataField="bi_TipoCredito" HeaderText="bi_TipoCredito" SortExpression="bi_TipoCredito" />
            <asp:BoundField DataField="bi_NombreTipoCredito" HeaderText="bi_NombreTipoCredito" SortExpression="bi_NombreTipoCredito" />
            <asp:BoundField DataField="bi_NombreAerolinea" HeaderText="bi_NombreAerolinea" SortExpression="bi_NombreAerolinea" />
            <asp:BoundField DataField="bi_DateTime" HeaderText="bi_DateTime" SortExpression="bi_DateTime" />
            <asp:BoundField DataField="bi_IdRecord" HeaderText="bi_IdRecord" SortExpression="bi_IdRecord" />
            <asp:BoundField DataField="bi_IdSecuencialCliente" HeaderText="bi_IdSecuencialCliente" SortExpression="bi_IdSecuencialCliente" />
            <asp:BoundField DataField="bi_Operador" HeaderText="bi_Operador" SortExpression="bi_Operador" />
            <asp:BoundField DataField="bi_CodigoEstablecimiento" HeaderText="bi_CodigoEstablecimiento" SortExpression="bi_CodigoEstablecimiento" />
            <asp:BoundField DataField="bi_IVA" HeaderText="bi_IVA" SortExpression="bi_IVA" />
            <asp:BoundField DataField="bi_Num_Voucher" HeaderText="bi_Num_Voucher" SortExpression="bi_Num_Voucher" />
            <asp:BoundField DataField="bi_Ciudad" HeaderText="bi_Ciudad" SortExpression="bi_Ciudad" />
            <asp:BoundField DataField="bi_ValorServicio" HeaderText="bi_ValorServicio" SortExpression="bi_ValorServicio" />
            <asp:BoundField DataField="bi_ValorPropina" HeaderText="bi_ValorPropina" SortExpression="bi_ValorPropina" />
            <asp:BoundField DataField="bi_ValorMontoFijo" HeaderText="bi_ValorMontoFijo" SortExpression="bi_ValorMontoFijo" />
            <asp:BoundField DataField="bi_Tarifa" HeaderText="bi_Tarifa" SortExpression="bi_Tarifa" />
            <asp:BoundField DataField="bi_Impuestos" HeaderText="bi_Impuestos" SortExpression="bi_Impuestos" />
            <asp:BoundField DataField="bi_Interes" HeaderText="bi_Interes" SortExpression="bi_Interes" />
            <asp:BoundField DataField="bi_Boletos" HeaderText="bi_Boletos" SortExpression="bi_Boletos" />
            <asp:BoundField DataField="bi_NombrePAX" HeaderText="bi_NombrePAX" SortExpression="bi_NombrePAX" />
            <asp:BoundField DataField="vtc_json_request" HeaderText="vtc_json_request" SortExpression="vtc_json_request" />
            <asp:BoundField DataField="vtc_json_response" HeaderText="vtc_json_response" SortExpression="vtc_json_response" />
            <asp:BoundField DataField="fo_rspCode" HeaderText="fo_rspCode" SortExpression="fo_rspCode" />
            <asp:BoundField DataField="fo_authorization" HeaderText="fo_authorization" SortExpression="fo_authorization" />
            <asp:BoundField DataField="fo_msgResponse" HeaderText="fo_msgResponse" SortExpression="fo_msgResponse" />
            <asp:BoundField DataField="fo_emvData" HeaderText="fo_emvData" SortExpression="fo_emvData" />
            <asp:BoundField DataField="fo_encoded" HeaderText="fo_encoded" SortExpression="fo_encoded" />
            <asp:BoundField DataField="bo_businessData" HeaderText="bo_businessData" SortExpression="bo_businessData" />
            <asp:BoundField DataField="bo_ReturnCode" HeaderText="bo_ReturnCode" SortExpression="bo_ReturnCode" />
            <asp:BoundField DataField="bo_Description" HeaderText="bo_Description" SortExpression="bo_Description" />
            <asp:BoundField DataField="bo_DateTimeReturn" HeaderText="bo_DateTimeReturn" SortExpression="bo_DateTimeReturn" />
            <asp:BoundField DataField="bo_Autorization" HeaderText="bo_Autorization" SortExpression="bo_Autorization" />
            <asp:BoundField DataField="bo_Emisor" HeaderText="bo_Emisor" SortExpression="bo_Emisor" />
            <asp:BoundField DataField="bo_NumeroTarjeta" HeaderText="bo_NumeroTarjeta" SortExpression="bo_NumeroTarjeta" />
            <asp:BoundField DataField="bo_NumeroTarjetaEditada" HeaderText="bo_NumeroTarjetaEditada" SortExpression="bo_NumeroTarjetaEditada" />
            <asp:BoundField DataField="bo_FechaExpiracion" HeaderText="bo_FechaExpiracion" SortExpression="bo_FechaExpiracion" />
            <asp:BoundField DataField="bo_Nombre" HeaderText="bo_Nombre" SortExpression="bo_Nombre" />
            <asp:BoundField DataField="bo_CodigoEmisor" HeaderText="bo_CodigoEmisor" SortExpression="bo_CodigoEmisor" />
            <asp:BoundField DataField="bo_TipoConvenio" HeaderText="bo_TipoConvenio" SortExpression="bo_TipoConvenio" />
            <asp:BoundField DataField="bo_IdRecord" HeaderText="bo_IdRecord" SortExpression="bo_IdRecord" />
            <asp:BoundField DataField="bo_Tarifa" HeaderText="bo_Tarifa" SortExpression="bo_Tarifa" />
            <asp:BoundField DataField="bo_Impuestos" HeaderText="bo_Impuestos" SortExpression="bo_Impuestos" />
            <asp:BoundField DataField="bo_Interes" HeaderText="bo_Interes" SortExpression="bo_Interes" />
            <asp:BoundField DataField="bo_Total" HeaderText="bo_Total" SortExpression="bo_Total" />
            <asp:BoundField DataField="bo_Mensaje" HeaderText="bo_Mensaje" SortExpression="bo_Mensaje" />
            <asp:BoundField DataField="bo_Comision" HeaderText="bo_Comision" SortExpression="bo_Comision" />
            <asp:BoundField DataField="bo_Boletos" HeaderText="bo_Boletos" SortExpression="bo_Boletos" />
            <asp:BoundField DataField="bo_NombrePAX" HeaderText="bo_NombrePAX" SortExpression="bo_NombrePAX" />
            <asp:BoundField DataField="db_rsp_code" HeaderText="db_rsp_code" SortExpression="db_rsp_code" />
            <asp:BoundField DataField="db_msg" HeaderText="db_msg" SortExpression="db_msg" />
            <asp:BoundField DataField="db_error_code" HeaderText="db_error_code" SortExpression="db_error_code" />
            <asp:BoundField DataField="db_error_msg" HeaderText="db_error_msg" SortExpression="db_error_msg" />--%>
        </Columns>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
            CssClass="pgr" Font-Underline="False" />
        <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid"
            BorderWidth="1px" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>

    <asp:SqlDataSource ID="sqTransactionsHistorico" runat="server" ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
        SelectCommand="sp_webReporteTransactionsHistorico"
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:Parameter Name="tid" Type="string" />
            <asp:Parameter Name="merchantId" Type="string" />
            <asp:Parameter Name="codTranMod" Type="string" />
            <asp:Parameter Name="usuario" Type="string" />
            <asp:Parameter Name="startDate" Type="DateTime" />
            <asp:Parameter Name="endDate" Type="DateTime" />
        </SelectParameters>


    </asp:SqlDataSource>



    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Reports/EntryPoint.aspx"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>
</asp:Content>


