﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Reports
Imports System.Xml
Imports System.IO
Imports System.Drawing


Partial Class Reports_ReportViewer
    Inherits TeleLoader.Web.BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            lblReportName.Text = objSessionParams.strReportName
            lblStartDate.Text = objSessionParams.dateStartDate
            lblEndDate.Text = objSessionParams.dateEndDate
            doReport()
        End If

    End Sub

    Private Sub doReport()

        Dim strReportData As String = ""

        Select Case objSessionParams.intReportType

            Case 54
                'Reporte Log X Usuario
                lblOptionName1.Text = "Usuario: "
                lblOptionValue1.Text = objSessionParams.strSelectedUserName
                lblOptionName2.Visible = False
                lblOptionValue2.Visible = False
                lblOptionName3.Visible = False
                lblOptionValue3.Visible = False
                dsLogXUser.SelectParameters("userID").DefaultValue = objSessionParams.intSelectedUserID
                dsLogXUser.SelectParameters("startDate").DefaultValue = objSessionParams.dateStartDate
                dsLogXUser.SelectParameters("endDate").DefaultValue = objSessionParams.dateEndDate
                grdLogXUser.Visible = True
                grdLogXInventory.Visible = False
                grdLogXDownload.Visible = False
                grdLogXMDM.Visible = False
                grdTerminals.Visible = False
                grdTerminalsMDM.Visible = False
                grdLogXKeyLoading.Visible = False   'Modificación Carga de Llaves; EB: 02/Mar/2018
                grdTransactions.Visible = False

            Case 140
                'Reporte Log X Inventario
                lblOptionName1.Text = "Grupo: "
                lblOptionValue1.Text = objSessionParams.strSelectedGroupName
                lblOptionName2.Text = "Serial Terminal: "
                lblOptionValue2.Text = objSessionParams.strSelectedTerminalName
                lblOptionName3.Visible = False
                lblOptionValue3.Visible = False
                dsLogXInventory.SelectParameters("groupID").DefaultValue = objSessionParams.intSelectedGroupID
                dsLogXInventory.SelectParameters("terminalID").DefaultValue = objSessionParams.strSerialTerminal
                dsLogXInventory.SelectParameters("customerID").DefaultValue = objAccessToken.CustomerUserID
                dsLogXInventory.SelectParameters("startDate").DefaultValue = objSessionParams.dateStartDate
                dsLogXInventory.SelectParameters("endDate").DefaultValue = objSessionParams.dateEndDate
                grdLogXInventory.Visible = True
                grdLogXUser.Visible = False
                grdLogXDownload.Visible = False
                grdLogXMDM.Visible = False
                grdTerminals.Visible = False
                grdTerminalsMDM.Visible = False
                grdLogXKeyLoading.Visible = False   'Modificación Carga de Llaves; EB: 02/Mar/2018
                grdTransactions.Visible = False

            Case 141
                'Reporte Log X Descarga
                lblOptionName1.Text = "Grupo: "
                lblOptionValue1.Text = objSessionParams.strSelectedGroupName
                lblOptionName2.Text = "Serial Terminal: "
                lblOptionValue2.Text = objSessionParams.strSelectedTerminalName
                lblOptionName3.Visible = False
                lblOptionValue3.Visible = False
                dsLogXDownload.SelectParameters("groupID").DefaultValue = objSessionParams.intSelectedGroupID
                dsLogXDownload.SelectParameters("terminalID").DefaultValue = objSessionParams.strSerialTerminal
                dsLogXDownload.SelectParameters("customerID").DefaultValue = objAccessToken.CustomerUserID
                dsLogXDownload.SelectParameters("startDate").DefaultValue = objSessionParams.dateStartDate
                dsLogXDownload.SelectParameters("endDate").DefaultValue = objSessionParams.dateEndDate
                grdLogXDownload.Visible = True
                grdLogXUser.Visible = False
                grdLogXInventory.Visible = False
                grdLogXMDM.Visible = False
                grdTerminals.Visible = False
                grdTerminalsMDM.Visible = False
                grdLogXKeyLoading.Visible = False   'Modificación Carga de Llaves; EB: 02/Mar/2018
                grdTransactions.Visible = False

            Case 142
                'Reporte Terminales
                lblOptionName1.Text = "Grupo: "
                lblOptionValue1.Text = objSessionParams.strSelectedGroupName
                lblOptionValue2.Text = objSessionParams.strSelectedTerminalName
                lblOptionName2.Visible = False
                lblOptionValue2.Visible = False
                lblOptionName3.Visible = False
                lblOptionValue3.Visible = False
                dsTerminals.SelectParameters("groupID").DefaultValue = objSessionParams.intSelectedGroupID
                dsTerminals.SelectParameters("terminalID").DefaultValue = objSessionParams.strSerialTerminal
                dsTerminals.SelectParameters("customerID").DefaultValue = objAccessToken.CustomerUserID
                grdLogXDownload.Visible = False
                grdLogXUser.Visible = False
                grdLogXInventory.Visible = False
                grdLogXMDM.Visible = False
                grdTerminalsMDM.Visible = False
                grdLogXKeyLoading.Visible = False   'Modificación Carga de Llaves; EB: 02/Mar/2018
                grdTerminals.Visible = True
                grdTransactions.Visible = False

            Case 147
                'Reporte Log MDM
                lblOptionName1.Text = "Grupo: "
                lblOptionValue1.Text = objSessionParams.strSelectedGroupName
                lblOptionName2.Text = "Serial Terminal: "
                lblOptionValue2.Text = objSessionParams.strSelectedTerminalName
                lblOptionName3.Visible = False
                lblOptionValue3.Visible = False
                dsLogXMDM.SelectParameters("groupID").DefaultValue = objSessionParams.intSelectedGroupID
                dsLogXMDM.SelectParameters("terminalID").DefaultValue = objSessionParams.intSelectedTerminalID
                dsLogXMDM.SelectParameters("customerID").DefaultValue = objAccessToken.CustomerUserID
                dsLogXMDM.SelectParameters("startDate").DefaultValue = objSessionParams.dateStartDate
                dsLogXMDM.SelectParameters("endDate").DefaultValue = objSessionParams.dateEndDate
                grdLogXDownload.Visible = False
                grdLogXUser.Visible = False
                grdLogXInventory.Visible = False
                grdTerminals.Visible = False
                grdTerminalsMDM.Visible = False
                grdLogXKeyLoading.Visible = False   'Modificación Carga de Llaves; EB: 02/Mar/2018
                grdLogXMDM.Visible = True
                grdTransactions.Visible = False

            Case 148
                'Reporte Terminales MDM
                lblOptionName1.Text = "Grupo: "
                lblOptionValue1.Text = objSessionParams.strSelectedGroupName
                lblOptionName2.Visible = False
                lblOptionValue2.Visible = False
                lblOptionName3.Visible = False
                lblOptionValue3.Visible = False
                dsTerminalsMDM.SelectParameters("groupID").DefaultValue = objSessionParams.intSelectedGroupID
                dsTerminalsMDM.SelectParameters("terminalID").DefaultValue = -1    'Default
                dsTerminalsMDM.SelectParameters("customerID").DefaultValue = objAccessToken.CustomerUserID
                grdLogXDownload.Visible = False
                grdLogXUser.Visible = False
                grdLogXInventory.Visible = False
                grdLogXMDM.Visible = False
                grdTerminals.Visible = False
                grdLogXKeyLoading.Visible = False   'Modificación Carga de Llaves; EB: 02/Mar/2018
                grdTerminalsMDM.Visible = True
                grdTransactions.Visible = False

            Case 167
                'Reporte Terminales MDM
                lblOptionName1.Text = "Grupo: "
                lblOptionValue1.Text = objSessionParams.strSelectedGroupName
                lblOptionName2.Visible = False
                lblOptionValue2.Visible = False
                lblOptionName3.Visible = False
                lblOptionValue3.Visible = False
                dsTerminalsAndroid.SelectParameters("groupID").DefaultValue = objSessionParams.intSelectedGroupID
                dsTerminalsAndroid.SelectParameters("terminalID").DefaultValue = -1    'Default
                dsTerminalsAndroid.SelectParameters("customerID").DefaultValue = objAccessToken.CustomerUserID
                grdLogXDownload.Visible = False
                grdLogXUser.Visible = False
                grdLogXInventory.Visible = False
                grdLogXMDM.Visible = False
                grdTerminals.Visible = False
                grdLogXKeyLoading.Visible = False   'Modificación Carga de Llaves; EB: 02/Mar/2018
                grdTerminalsMDM.Visible = False
                grdTransactions.Visible = False
                grdAndroid.Visible = True

            Case 168
                'Reporte Log MDM
                lblOptionName1.Text = "Grupo: "
                lblOptionValue1.Text = objSessionParams.strSelectedGroupName
                lblOptionName2.Text = ""
                lblOptionValue2.Text = ""
                lblOptionName3.Visible = False
                lblOptionValue3.Visible = False
                dsApks.SelectParameters("groupID").DefaultValue = objSessionParams.intSelectedGroupID
                dsApks.SelectParameters("terminalID").DefaultValue = objSessionParams.intSelectedTerminalID
                dsApks.SelectParameters("customerID").DefaultValue = objAccessToken.CustomerUserID
                'dsApks.SelectParameters("startDate").DefaultValue = objSessionParams.dateStartDate
                'dsApks.SelectParameters("endDate").DefaultValue = objSessionParams.dateEndDate
                grdLogXDownload.Visible = False
                grdLogXUser.Visible = False
                grdLogXInventory.Visible = False
                grdTerminals.Visible = False
                grdTerminalsMDM.Visible = False
                grdLogXKeyLoading.Visible = False   'Modificación Carga de Llaves; EB: 02/Mar/2018
                grdLogXMDM.Visible = False
                grdTransactions.Visible = False
                GrdApks.Visible = True


            Case 149        'Modificación Carga de Llaves; EB: 02/Mar/2018
                'Reporte Log Carga de Llaves
                lblOptionName1.Text = "Grupo: "
                lblOptionValue1.Text = objSessionParams.strSelectedGroupName
                lblOptionName2.Text = "Serial Terminal: "
                lblOptionValue2.Text = objSessionParams.strSelectedTerminalName
                lblOptionName3.Visible = False
                lblOptionValue3.Visible = False
                dsLogXKeyLoading.SelectParameters("groupID").DefaultValue = objSessionParams.intSelectedGroupID
                dsLogXKeyLoading.SelectParameters("terminalID").DefaultValue = objSessionParams.intSelectedTerminalID
                dsLogXKeyLoading.SelectParameters("customerID").DefaultValue = objAccessToken.CustomerUserID
                dsLogXKeyLoading.SelectParameters("startDate").DefaultValue = objSessionParams.dateStartDate
                dsLogXKeyLoading.SelectParameters("endDate").DefaultValue = objSessionParams.dateEndDate
                grdLogXDownload.Visible = False
                grdLogXUser.Visible = False
                grdLogXInventory.Visible = False
                grdTerminals.Visible = False
                grdTerminalsMDM.Visible = False
                grdLogXMDM.Visible = False
                grdLogXKeyLoading.Visible = True
                grdTransactions.Visible = False

            Case 163
                'Reporte transacciones
                lblOptionName1.Text = ""
                lblOptionValue1.Text = ""
                lblOptionName2.Text = ""
                lblOptionValue2.Text = objSessionParams.intSelectTID
                lblOptionName3.Visible = False
                lblOptionValue3.Visible = False
                sqTransactions.SelectParameters("tid").DefaultValue = objSessionParams.intSelectTID
                sqTransactions.SelectParameters("merchantId").DefaultValue = objSessionParams.intSelectedMerchantID
                sqTransactions.SelectParameters("codTranMod").DefaultValue = objSessionParams.CodeTranMode
                sqTransactions.SelectParameters("usuario").DefaultValue = objSessionParams.UsuarioID
                sqTransactions.SelectParameters("startDate").DefaultValue = objSessionParams.dateStartDate
                sqTransactions.SelectParameters("endDate").DefaultValue = objSessionParams.dateEndDate
                grdLogXDownload.Visible = False
                grdLogXUser.Visible = False
                grdLogXInventory.Visible = False
                grdTerminals.Visible = False
                grdTerminalsMDM.Visible = False
                grdLogXMDM.Visible = False
                grdLogXKeyLoading.Visible = False
                grdTransactions.Visible = True

            Case 164
                'Reporte Historico Transacciones
                lblOptionName1.Text = ""
                lblOptionValue1.Text = ""
                lblOptionName2.Text = ""
                lblOptionValue2.Text = objSessionParams.intSelectTIDHis
                lblOptionName3.Visible = False
                lblOptionValue3.Visible = False
                sqTransactionsHistorico.SelectParameters("tid").DefaultValue = objSessionParams.intSelectTIDHis
                sqTransactionsHistorico.SelectParameters("merchantId").DefaultValue = objSessionParams.intSelectedMerchantIDHis
                sqTransactionsHistorico.SelectParameters("codTranMod").DefaultValue = objSessionParams.CodeTranModeHis
                sqTransactionsHistorico.SelectParameters("usuario").DefaultValue = objSessionParams.UsuarioIDHis
                sqTransactionsHistorico.SelectParameters("startDate").DefaultValue = objSessionParams.dateStartDate
                sqTransactionsHistorico.SelectParameters("endDate").DefaultValue = objSessionParams.dateEndDate
                grdLogXDownload.Visible = False
                grdLogXUser.Visible = False
                grdLogXInventory.Visible = False
                grdTerminals.Visible = False
                grdTerminalsMDM.Visible = False
                grdLogXMDM.Visible = False
                grdLogXKeyLoading.Visible = False
                grdTransactionsHistorico.Visible = True
                grdTransactions.Visible = False

        End Select

        strReportData = "Reporte: " & objSessionParams.strReportName & ", " & lblOptionName1.Text & lblOptionValue1.Text &
                        ", " & lblOptionName2.Text & lblOptionValue2.Text & ", " & lblOptionName3.Text & lblOptionValue3.Text &
                        ", Fecha Inicial: " & objSessionParams.dateStartDate & ", Fecha Fin: " & objSessionParams.dateEndDate

        'Save Log
        objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Load", "Reporte Generado. Datos[ " & strReportData & " ]", "")

    End Sub

    Protected Sub lnkExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportExcel.Click
        Dim strExcelFile As String
        Dim XMLDoc As System.Xml.XmlDataDocument
        Dim objNewNode As System.Xml.XmlNode
        Dim objFrstNode As System.Xml.XmlNode
        Dim connection As New SqlConnection(strConnectionString)
        Dim command As New SqlCommand()
        Dim result As SqlDataReader
        Dim da As New SqlDataAdapter(command)
        Dim dataset As New Data.DataSet
        Dim objExport As New ExcelExport

        'Init Actions
        objExport.TempFolder = ConfigurationSettings.AppSettings.Get("TempReportFolder")
        objExport.XSLStyleSheetFolder = ConfigurationSettings.AppSettings.Get("XSLReportFolder")
        objExport.CleanUpTemporaryFiles()

        Try

            'Reporte Log X Usuario
            If objSessionParams.intReportType = 54 Then
                command.Connection = connection
                command.CommandType = Data.CommandType.StoredProcedure
                command.CommandText = "sp_webReporteAuditoria"
                command.Connection.Open()
                command.Parameters.Add(New SqlParameter("@userID", objSessionParams.intSelectedUserID))
                command.Parameters.Add(New SqlParameter("@startDate", objSessionParams.dateStartDate))
                command.Parameters.Add(New SqlParameter("@endDate", objSessionParams.dateEndDate))

                'Execute Command
                result = command.ExecuteReader()
                command.Connection.Close()
                da.SelectCommand = command

                'Fill dataset
                da.Fill(dataset, "Table")

                'Create the XML Data Document from the dataset.
                XMLDoc = New XmlDataDocument(dataset)

                If XMLDoc.DocumentElement Is Nothing Then
                    Exit Sub
                End If

                'Add Header Information
                objNewNode = XMLDoc.CreateElement("HeaderDetails")
                objNewNode.InnerXml = "<ReportName>" & objSessionParams.strReportName & "</ReportName>" _
                                        & "<User>" & objSessionParams.strSelectedUserName & "</User>" &
                                        "<Period>" & objSessionParams.dateStartDate.ToString("dd/MM/yyy") & " hasta: " & objSessionParams.dateEndDate.ToString("dd/MM/yyy") & "</Period>"
                XMLDoc.DataSet.EnforceConstraints = False
                objFrstNode = XMLDoc.DocumentElement.FirstChild
                XMLDoc.DocumentElement.InsertBefore(objNewNode, objFrstNode)

                'Generate Excel File
                strExcelFile = objExport.TransformXMLDocumentToExcel(XMLDoc, "LogAuditoria.xsl")

                'Send Excel File To Client
                objExport.SendExcelToClient(strExcelFile)
            End If

            'Reporte Log X Inventario
            If objSessionParams.intReportType = 140 Then
                command.Connection = connection
                command.CommandType = Data.CommandType.StoredProcedure
                command.CommandText = "sp_webReporteInventarioDetallado"
                command.Connection.Open()
                command.Parameters.Add(New SqlParameter("@groupID", -1))
                command.Parameters.Add(New SqlParameter("@terminalID ", objSessionParams.strSerialTerminal))
                command.Parameters.Add(New SqlParameter("@customerID ", objAccessToken.CustomerUserID))
                command.Parameters.Add(New SqlParameter("@startDate", objSessionParams.dateStartDate))
                command.Parameters.Add(New SqlParameter("@endDate", objSessionParams.dateEndDate))

                'Execute Command
                result = command.ExecuteReader()
                command.Connection.Close()
                da.SelectCommand = command

                'Fill dataset
                da.Fill(dataset, "Table")

                'Create the XML Data Document from the dataset.
                XMLDoc = New XmlDataDocument(dataset)

                If XMLDoc.DocumentElement Is Nothing Then
                    Exit Sub
                End If

                'Add Header Information
                objNewNode = XMLDoc.CreateElement("HeaderDetails")
                objNewNode.InnerXml = "<ReportName>" & objSessionParams.strReportName & "</ReportName>" &
                                        "<Group>" & objSessionParams.strSelectedGroupName & "</Group>" &
                                        "<Terminal>" & objSessionParams.strSelectedTerminalName & "</Terminal>" &
                                        "<Period>" & objSessionParams.dateStartDate & " hasta: " & objSessionParams.dateEndDate.ToString("dd/MM/yyy") & "</Period>"
                XMLDoc.DataSet.EnforceConstraints = False
                objFrstNode = XMLDoc.DocumentElement.FirstChild
                XMLDoc.DocumentElement.InsertBefore(objNewNode, objFrstNode)

                'Generate Excel File
                strExcelFile = objExport.TransformXMLDocumentToExcel(XMLDoc, "LogInventario.xsl")

                'Send Excel File To Client
                objExport.SendExcelToClient(strExcelFile)
            End If

            'Reporte Log X Descarga
            If objSessionParams.intReportType = 141 Then
                command.Connection = connection
                command.CommandType = Data.CommandType.StoredProcedure
                command.CommandText = "sp_webReporteDescargaDetallado"
                command.Connection.Open()
                command.Parameters.Add(New SqlParameter("@groupID", -1))
                command.Parameters.Add(New SqlParameter("@terminalID ", objSessionParams.strSerialTerminal))
                command.Parameters.Add(New SqlParameter("@customerID ", objAccessToken.CustomerUserID))
                command.Parameters.Add(New SqlParameter("@startDate", objSessionParams.dateStartDate))
                command.Parameters.Add(New SqlParameter("@endDate", objSessionParams.dateEndDate))

                'Execute Command
                result = command.ExecuteReader()
                command.Connection.Close()
                da.SelectCommand = command

                'Fill dataset
                da.Fill(dataset, "Table")

                'Create the XML Data Document from the dataset.
                XMLDoc = New XmlDataDocument(dataset)

                If XMLDoc.DocumentElement Is Nothing Then
                    Exit Sub
                End If

                'Add Header Information
                objNewNode = XMLDoc.CreateElement("HeaderDetails")
                objNewNode.InnerXml = "<ReportName>" & objSessionParams.strReportName & "</ReportName>" &
                                        "<Group>" & objSessionParams.strSelectedGroupName & "</Group>" &
                                        "<Terminal>" & objSessionParams.strSelectedTerminalName & "</Terminal>" &
                                        "<Period>" & objSessionParams.dateStartDate.ToString("dd/MM/yyy") & " hasta: " & objSessionParams.dateEndDate.ToString("dd/MM/yyy") & "</Period>"
                XMLDoc.DataSet.EnforceConstraints = False
                objFrstNode = XMLDoc.DocumentElement.FirstChild
                XMLDoc.DocumentElement.InsertBefore(objNewNode, objFrstNode)

                'Generate Excel File
                strExcelFile = objExport.TransformXMLDocumentToExcel(XMLDoc, "LogDescarga.xsl")

                'Send Excel File To Client
                objExport.SendExcelToClient(strExcelFile)
            End If

            'Reporte de Terminales
            If objSessionParams.intReportType = 142 Then
                command.Connection = connection
                command.CommandType = Data.CommandType.StoredProcedure
                command.CommandText = "sp_webReporteTerminalesDetallado"
                command.Connection.Open()
                command.Parameters.Add(New SqlParameter("@groupID", objSessionParams.intSelectedGroupID))
                command.Parameters.Add(New SqlParameter("@terminalID ", objSessionParams.strSerialTerminal))
                command.Parameters.Add(New SqlParameter("@customerID ", objAccessToken.CustomerUserID))

                'Execute Command
                result = command.ExecuteReader()
                command.Connection.Close()
                da.SelectCommand = command

                'Fill dataset
                da.Fill(dataset, "Table")

                'Create the XML Data Document from the dataset.
                XMLDoc = New XmlDataDocument(dataset)

                If XMLDoc.DocumentElement Is Nothing Then
                    Exit Sub
                End If

                'Add Header Information
                objNewNode = XMLDoc.CreateElement("HeaderDetails")
                objNewNode.InnerXml = "<ReportName>" & objSessionParams.strReportName & "</ReportName>" &
                                        "<Group>" & objSessionParams.strSelectedGroupName & "</Group>" &
                                        "<Terminal>" & objSessionParams.strSelectedTerminalName & "</Terminal>"
                XMLDoc.DataSet.EnforceConstraints = False
                objFrstNode = XMLDoc.DocumentElement.FirstChild
                XMLDoc.DocumentElement.InsertBefore(objNewNode, objFrstNode)

                'Generate Excel File
                strExcelFile = objExport.TransformXMLDocumentToExcel(XMLDoc, "LogTerminales.xsl")

                'Send Excel File To Client
                objExport.SendExcelToClient(strExcelFile)
            End If

            'Reporte Log X MDM
            If objSessionParams.intReportType = 147 Then
                command.Connection = connection
                command.CommandType = Data.CommandType.StoredProcedure
                command.CommandText = "sp_webReporteMDM"
                command.Connection.Open()
                command.Parameters.Add(New SqlParameter("@groupID", objSessionParams.intSelectedGroupID))
                command.Parameters.Add(New SqlParameter("@terminalID ", objSessionParams.intSelectedTerminalID))
                command.Parameters.Add(New SqlParameter("@customerID ", objAccessToken.CustomerUserID))
                command.Parameters.Add(New SqlParameter("@startDate", objSessionParams.dateStartDate))
                command.Parameters.Add(New SqlParameter("@endDate", objSessionParams.dateEndDate))

                'Execute Command
                result = command.ExecuteReader()
                command.Connection.Close()
                da.SelectCommand = command

                'Fill dataset
                da.Fill(dataset, "Table")

                'Create the XML Data Document from the dataset.
                XMLDoc = New XmlDataDocument(dataset)

                If XMLDoc.DocumentElement Is Nothing Then
                    Exit Sub
                End If

                'Add Header Information
                objNewNode = XMLDoc.CreateElement("HeaderDetails")
                objNewNode.InnerXml = "<ReportName>" & objSessionParams.strReportName & "</ReportName>" &
                                        "<Group>" & objSessionParams.strSelectedGroupName & "</Group>" &
                                        "<Terminal>" & objSessionParams.strSelectedTerminalName & "</Terminal>" &
                                        "<Period>" & objSessionParams.dateStartDate.ToString("dd/MM/yyy") & " hasta: " & objSessionParams.dateEndDate.ToString("dd/MM/yyy") & "</Period>"
                XMLDoc.DataSet.EnforceConstraints = False
                objFrstNode = XMLDoc.DocumentElement.FirstChild
                XMLDoc.DocumentElement.InsertBefore(objNewNode, objFrstNode)

                'Generate Excel File
                strExcelFile = objExport.TransformXMLDocumentToExcel(XMLDoc, "LogMDM.xsl")

                'Send Excel File To Client
                objExport.SendExcelToClient(strExcelFile)
            End If

            'Reporte de Terminales MDM
            If objSessionParams.intReportType = 148 Then
                command.Connection = connection
                command.CommandType = Data.CommandType.StoredProcedure
                command.CommandText = "sp_webReporteTerminalesMDMDetallado"
                command.Connection.Open()
                command.Parameters.Add(New SqlParameter("@groupID", objSessionParams.intSelectedGroupID))
                command.Parameters.Add(New SqlParameter("@terminalID", objSessionParams.intSelectedTerminalID))
                command.Parameters.Add(New SqlParameter("@customerID", objAccessToken.CustomerUserID))
                command.Parameters.Add(New SqlParameter("@currentURL", Request.Url.AbsoluteUri.Replace(Request.Url.AbsolutePath, "")))

                'Execute Command
                result = command.ExecuteReader()
                command.Connection.Close()
                da.SelectCommand = command

                'Fill dataset
                da.Fill(dataset, "Table")

                'Create the XML Data Document from the dataset.
                XMLDoc = New XmlDataDocument(dataset)

                If XMLDoc.DocumentElement Is Nothing Then
                    Exit Sub
                End If

                'Add Header Information
                objNewNode = XMLDoc.CreateElement("HeaderDetails")
                objNewNode.InnerXml = "<ReportName>" & objSessionParams.strReportName & "</ReportName>" &
                                        "<Group>" & objSessionParams.strSelectedGroupName & "</Group>" &
                                        "<Terminal>" & objSessionParams.strSelectedTerminalName & "</Terminal>"
                XMLDoc.DataSet.EnforceConstraints = False
                objFrstNode = XMLDoc.DocumentElement.FirstChild
                XMLDoc.DocumentElement.InsertBefore(objNewNode, objFrstNode)

                'Generate Excel File
                strExcelFile = objExport.TransformXMLDocumentToExcel(XMLDoc, "LogTerminalesMDM.xsl")

                'Send Excel File To Client
                objExport.SendExcelToClient(strExcelFile)
            End If
            'Reporte de Terminales MDM
            If objSessionParams.intReportType = 167 Then
                command.Connection = connection
                command.CommandType = Data.CommandType.StoredProcedure
                command.CommandText = "sp_webReporteTerminalesMDMDetallado"
                command.Connection.Open()
                command.Parameters.Add(New SqlParameter("@groupID", objSessionParams.intSelectedGroupID))
                command.Parameters.Add(New SqlParameter("@terminalID", objSessionParams.intSelectedTerminalID))
                command.Parameters.Add(New SqlParameter("@customerID", objAccessToken.CustomerUserID))
                command.Parameters.Add(New SqlParameter("@currentURL", Request.Url.AbsoluteUri.Replace(Request.Url.AbsolutePath, "")))

                'Execute Command
                result = command.ExecuteReader()
                command.Connection.Close()
                da.SelectCommand = command

                'Fill dataset
                da.Fill(dataset, "Table")

                'Create the XML Data Document from the dataset.
                XMLDoc = New XmlDataDocument(dataset)

                If XMLDoc.DocumentElement Is Nothing Then
                    Exit Sub
                End If

                'Add Header Information
                objNewNode = XMLDoc.CreateElement("HeaderDetails")
                objNewNode.InnerXml = "<ReportName>" & objSessionParams.strReportName & "</ReportName>" &
                                        "<Group>" & objSessionParams.strSelectedGroupName & "</Group>" &
                                        "<Terminal>" & objSessionParams.strSelectedTerminalName & "</Terminal>"
                XMLDoc.DataSet.EnforceConstraints = False
                objFrstNode = XMLDoc.DocumentElement.FirstChild
                XMLDoc.DocumentElement.InsertBefore(objNewNode, objFrstNode)

                'Generate Excel File
                strExcelFile = objExport.TransformXMLDocumentToExcel(XMLDoc, "LogTerminalesMDM.xsl")

                'Send Excel File To Client
                objExport.SendExcelToClient(strExcelFile)
            End If
            'Reporte de Terminales MDM
            If objSessionParams.intReportType = 168 Then
                command.Connection = connection
                command.CommandType = Data.CommandType.StoredProcedure
                command.CommandText = "sp_webReporteMDM_APK"
                command.Connection.Open()
                command.Parameters.Add(New SqlParameter("@groupID", objSessionParams.intSelectedGroupID))
                command.Parameters.Add(New SqlParameter("@terminalID ", objSessionParams.intSelectedTerminalID))
                command.Parameters.Add(New SqlParameter("@customerID ", objAccessToken.CustomerUserID))

                'Execute Command
                result = command.ExecuteReader()
                command.Connection.Close()
                da.SelectCommand = command

                'Fill dataset
                da.Fill(dataset, "Table")

                'Create the XML Data Document from the dataset.
                XMLDoc = New XmlDataDocument(dataset)

                If XMLDoc.DocumentElement Is Nothing Then
                    Exit Sub
                End If

                'Add Header Information
                objNewNode = XMLDoc.CreateElement("HeaderDetails")
                objNewNode.InnerXml = "<ReportName>" & objSessionParams.strReportName & "</ReportName>" &
                                        "<Group>" & objSessionParams.strSelectedGroupName & "</Group>" &
                                        "<Terminal>" & objSessionParams.strSelectedTerminalName & "</Terminal>"
                XMLDoc.DataSet.EnforceConstraints = False
                objFrstNode = XMLDoc.DocumentElement.FirstChild
                XMLDoc.DocumentElement.InsertBefore(objNewNode, objFrstNode)

                'Generate Excel File
                strExcelFile = objExport.TransformXMLDocumentToExcel(XMLDoc, "LogTerminalesApk.xsl")

                'Send Excel File To Client
                objExport.SendExcelToClient(strExcelFile)
            End If
            'Modificación Carga de Llaves; EB: 02/Mar/2018
            'Reporte Log Carga de Llaves
            If objSessionParams.intReportType = 149 Then
                command.Connection = connection
                command.CommandType = Data.CommandType.StoredProcedure
                command.CommandText = "sp_webReporteCargaLlaves"
                command.Connection.Open()
                command.Parameters.Add(New SqlParameter("@groupID", objSessionParams.intSelectedGroupID))
                command.Parameters.Add(New SqlParameter("@terminalID ", objSessionParams.intSelectedTerminalID))
                command.Parameters.Add(New SqlParameter("@customerID ", objAccessToken.CustomerUserID))
                command.Parameters.Add(New SqlParameter("@startDate", objSessionParams.dateStartDate))
                command.Parameters.Add(New SqlParameter("@endDate", objSessionParams.dateEndDate))

                'Execute Command
                result = command.ExecuteReader()
                command.Connection.Close()
                da.SelectCommand = command

                'Fill dataset
                da.Fill(dataset, "Table")

                'Create the XML Data Document from the dataset.
                XMLDoc = New XmlDataDocument(dataset)

                If XMLDoc.DocumentElement Is Nothing Then
                    Exit Sub
                End If

                'Add Header Information
                objNewNode = XMLDoc.CreateElement("HeaderDetails")
                objNewNode.InnerXml = "<ReportName>" & objSessionParams.strReportName & "</ReportName>" &
                                        "<Group>" & objSessionParams.strSelectedGroupName & "</Group>" &
                                        "<Terminal>" & objSessionParams.strSelectedTerminalName & "</Terminal>" &
                                        "<Period>" & objSessionParams.dateStartDate.ToString("dd/MM/yyy") & " hasta: " & objSessionParams.dateEndDate.ToString("dd/MM/yyy") & "</Period>"
                XMLDoc.DataSet.EnforceConstraints = False
                objFrstNode = XMLDoc.DocumentElement.FirstChild
                XMLDoc.DocumentElement.InsertBefore(objNewNode, objFrstNode)

                'Generate Excel File
                strExcelFile = objExport.TransformXMLDocumentToExcel(XMLDoc, "LogCargaLlaves.xsl")

                'Send Excel File To Client
                objExport.SendExcelToClient(strExcelFile)
            End If





            'Reporte Transacciones
            If objSessionParams.intReportType = 163 Then
                command.Connection = connection
                command.CommandType = Data.CommandType.StoredProcedure
                command.CommandText = "sp_webReporteTransactions"
                command.Connection.Open()

                command.Parameters.Add(New SqlParameter("@tid", objSessionParams.intSelectTID))
                command.Parameters.Add(New SqlParameter("@merchantId ", objSessionParams.intSelectedMerchantID))
                command.Parameters.Add(New SqlParameter("@codTranMod ", objSessionParams.CodeTranMode))
                command.Parameters.Add(New SqlParameter("@usuario ", objSessionParams.UsuarioID))
                command.Parameters.Add(New SqlParameter("@startDate", objSessionParams.dateStartDate))
                command.Parameters.Add(New SqlParameter("@endDate", objSessionParams.dateEndDate))

                'Execute Command
                result = command.ExecuteReader()
                command.Connection.Close()
                da.SelectCommand = command

                'Fill dataset
                da.Fill(dataset, "Table")

                'Create the XML Data Document from the dataset.
                XMLDoc = New XmlDataDocument(dataset)

                If XMLDoc.DocumentElement Is Nothing Then
                    Exit Sub
                End If

                'Add Header Information
                objNewNode = XMLDoc.CreateElement("HeaderDetails")
                objNewNode.InnerXml = "<ReportName>" & objSessionParams.strReportName & "</ReportName>" &
                                        "<Group>" & objSessionParams.strSelectedGroupName & "</Group>" &
                                        "<Terminal>" & objSessionParams.strSelectedTerminalName & "</Terminal>" &
                                        "<Period>" & objSessionParams.dateStartDate.ToString("dd/MM/yyy") & " hasta: " & objSessionParams.dateEndDate.ToString("dd/MM/yyy") & "</Period>"
                XMLDoc.DataSet.EnforceConstraints = False
                objFrstNode = XMLDoc.DocumentElement.FirstChild
                XMLDoc.DocumentElement.InsertBefore(objNewNode, objFrstNode)

                'Generate Excel File
                strExcelFile = objExport.TransformXMLDocumentToExcel(XMLDoc, "LogReporTrans.xsl")

                'Send Excel File To Client
                objExport.SendExcelToClient(strExcelFile)
            End If





            'Reporte Transacciones
            If objSessionParams.intReportType = 164 Then
                command.Connection = connection
                command.CommandType = Data.CommandType.StoredProcedure
                command.CommandText = "sp_webReporteTransactionsHistorico"
                command.Connection.Open()

                command.Parameters.Add(New SqlParameter("@tid", objSessionParams.intSelectTIDHis))
                command.Parameters.Add(New SqlParameter("@merchantId ", objSessionParams.intSelectedMerchantIDHis))
                command.Parameters.Add(New SqlParameter("@codTranMod ", objSessionParams.CodeTranModeHis))
                command.Parameters.Add(New SqlParameter("@usuario ", objSessionParams.UsuarioIDHis))
                command.Parameters.Add(New SqlParameter("@startDate", objSessionParams.dateStartDate))
                command.Parameters.Add(New SqlParameter("@endDate", objSessionParams.dateEndDate))

                'Execute Command
                result = command.ExecuteReader()
                command.Connection.Close()
                da.SelectCommand = command

                'Fill dataset
                da.Fill(dataset, "Table")

                'Create the XML Data Document from the dataset.
                XMLDoc = New XmlDataDocument(dataset)

                If XMLDoc.DocumentElement Is Nothing Then
                    Exit Sub
                End If

                'Add Header Information
                objNewNode = XMLDoc.CreateElement("HeaderDetails")
                objNewNode.InnerXml = "<ReportName>" & objSessionParams.strReportName & "</ReportName>" &
                                        "<Group>" & objSessionParams.strSelectedGroupName & "</Group>" &
                                        "<Terminal>" & objSessionParams.strSelectedTerminalName & "</Terminal>" &
                                        "<Period>" & objSessionParams.dateStartDate.ToString("dd/MM/yyy") & " hasta: " & objSessionParams.dateEndDate.ToString("dd/MM/yyy") & "</Period>"
                XMLDoc.DataSet.EnforceConstraints = False
                objFrstNode = XMLDoc.DocumentElement.FirstChild
                XMLDoc.DocumentElement.InsertBefore(objNewNode, objFrstNode)

                'Generate Excel File
                strExcelFile = objExport.TransformXMLDocumentToExcel(XMLDoc, "LogReporTransHist.xsl")

                'Send Excel File To Client
                objExport.SendExcelToClient(strExcelFile)
            End If



        Catch ex As Threading.ThreadAbortException
            'Do nothing
        Catch ex As Exception
            HandleErrorRedirect(ex)
        Finally
            If Not connection Is Nothing Then
                connection.Close()
            End If
        End Try

    End Sub

    Protected Sub grdTerminalsMDM_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdTerminalsMDM.RowDataBound
        Dim backgroundColorGreen As String = "#dff0d8"
        Dim backgroundColorRed As String = "#e47a7a"

        If e.Row.RowType = DataControlRowType.DataRow Then

            'Modificación Carga de Llaves; EB: 02/Mar/2018
            e.Row.Cells(13).Style("background") = DataBinder.Eval(e.Row.DataItem, "apk_banco_color")
            e.Row.Cells(13).ForeColor = IIf(DataBinder.Eval(e.Row.DataItem, "apk_banco_color") = backgroundColorGreen, Color.Gray, Color.White)

            'Modificación Carga de Llaves; EB: 02/Mar/2018
            e.Row.Cells(14).Style("background") = DataBinder.Eval(e.Row.DataItem, "configuracion_color")
            e.Row.Cells(14).ForeColor = IIf(DataBinder.Eval(e.Row.DataItem, "configuracion_color") = backgroundColorGreen, Color.Gray, Color.White)

            'Modificación Carga de Llaves; EB: 02/Mar/2018
            e.Row.Cells(15).Style("background") = DataBinder.Eval(e.Row.DataItem, "imagenes_color")
            e.Row.Cells(15).ForeColor = IIf(DataBinder.Eval(e.Row.DataItem, "imagenes_color") = backgroundColorGreen, Color.Gray, Color.White)

        End If
    End Sub

End Class
