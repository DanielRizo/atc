﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ShowTerminalGroupLocation.aspx.vb" Inherits="Group_ShowTerminalGroupLocation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Ubicación de la Terminal
    </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"
        integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
        crossorigin="" />
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open Sans">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/yeti/bootstrap.min.css" rel="stylesheet" integrity="sha384-bWm7zrSUE5E+21rA9qdH5frkMpXvqjQm/WJw4L5PYQLNHrywI5zs5saEjIcCdGu1" crossorigin="anonymous">
    <script type="text/javascript" src="../js/jquery.ui.datepicker.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkGroup" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/Manager.aspx">Grupos</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkListGroups" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/ListGroups.aspx">Listar Grupos</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkGroupTerminals" runat="server" CssClass="fixed"
            PostBackUrl="~/Group/GroupTerminals.aspx">Terminales del Grupo</asp:LinkButton>
    </li>
    <li>Ubicación Terminal</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">

    <div id="myMap" style="height: 600px">
        <asp:Label ID="longitud" runat="server" Text="Label"></asp:Label>
        <asp:Label ID="latitud" runat="server" Text="Label"></asp:Label>
        <asp:Label ID="MCC" runat="server" Text="Label"></asp:Label>
        <asp:Label ID="MNC" runat="server" Text="Label"></asp:Label>
        <asp:Label ID="LCA" runat="server" Text="Label"></asp:Label>
        <asp:Label ID="CID" runat="server" Text="Label"></asp:Label>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="w3-white w3-margin">
                    <div class="w3-container w3-padding w3-black">
                        <h4>Datos de la Terminal</h4>
                    </div>
                    <ul class="w3-ul w3-hoverable w3-white">
                        <li class="w3-padding-16">
                            <img src="https://previews.123rf.com/images/viktorijareut/viktorijareut1502/viktorijareut150200063/36306209-pos-m%C3%A1quina-de-la-posici%C3%B3n-terminal-de-la-tarjeta-de-cr%C3%A9dito-de-tarjetas-de-cr%C3%A9dito.jpg" alt="Image" class="w3-left w3-margin-right" style="width: 50px">
                            <span class="w3-large">Serial</span>
                            <br>
                            <asp:Label ID="Serial" runat="server" Text="Label"></asp:Label>
                        </li>
                        <li class="w3-padding-16">
                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRnzwfJ-CLNUv-XT2HeO_93cLMSew2wCL9EGIY3lZQ6g53r8LS6" alt="Image" class="w3-left w3-margin-right" style="width: 50px">
                            <span class="w3-large">Nivel Señal</span>
                            <br>
                            <br>
                            <asp:Label ID="señal" runat="server" Text="Label"></asp:Label>
                        </li>
                        <li class="w3-padding-16">
                            <img src="https://pngimage.net/wp-content/uploads/2018/06/icone-batterie-png-3.png" alt="Image" class="w3-left w3-margin-right" style="width: 50px">
                            <span class="w3-large">Nivel Bateria</span>
                            <br>
                            <br>
                            <asp:Label ID="Bateria" runat="server" Text="Label"></asp:Label>
                        </li>
                        <li class="w3-padding-16">
                            <img src="https://static.thenounproject.com/png/311054-200.png" alt="Image" class="w3-left w3-margin-right w3-sepia" style="width: 50px">
                            <span class="w3-large">imei</span>
                            <br>
                            <asp:Label ID="imei" runat="server" Text="Label"></asp:Label>
                        </li>
                        <li class="w3-padding-16">
                            <img src="https://img.pngio.com/perfect-changer-email-authentication-merchant-png-400_400.png" alt="Image" class="w3-left w3-margin-right w3-sepia" style="width: 50px">
                            <span class="w3-large">Numero Comercio 1:</span>
                            <br>
                            <asp:Label ID="Comercio" runat="server" Text="Numero Comercio 1:"></asp:Label>
                        </li>
                        <li class="w3-padding-16">
                            <img src="https://image.flaticon.com/icons/png/512/199/199549.png" alt="Image" class="w3-left w3-margin-right w3-sepia" style="width: 50px">
                            <span class="w3-large">Terminal ID 1:</span>
                            <br>
                            <asp:Label ID="TID1" runat="server" Text="Terminal ID 1:"></asp:Label>
                            <br />
                        </li>
                        <li class="w3-padding-16">
                            <img src="https://cdn3.iconfinder.com/data/icons/purchases-and-sales/512/STORE.png" alt="Image" class="w3-left w3-margin-right w3-sepia" style="width: 50px">
                            <span class="w3-large">Comercio 1:</span>
                            <br>
                            <asp:Label ID="Merchant1" runat="server" Text="Comercio 1:"></asp:Label>
                            <br />
                        </li>
                        <li class="w3-padding-16">
                            <img src="https://cdn.imgbin.com/14/14/14/imgbin-computer-icons-mobile-phones-telephone-call-email-email-2jtSMCiNyZY8CfXTS4ajDW5x0.jpg" alt="Image" class="w3-left w3-margin-right w3-sepia" style="width: 50px">
                            <span class="w3-large">Tel Comercio 1:</span>
                            <br>
                            <asp:Label ID="TelComercio1" runat="server" Text="Tel Comercio 1:"></asp:Label>
                            <br />
                        </li>
                        <li class="w3-padding-16">
                            <img src="https://www.expertsure.com/uk/merchant-accounts/wp-content/uploads/2017/11/small-retail-unit.png" alt="Image" class="w3-left w3-margin-right w3-sepia" style="width: 50px">
                            <span class="w3-large">Numero Comercio 2:</span>
                            <br>
                            <asp:Label ID="MerchantName2" runat="server" Text="Numero Comercio 2:"></asp:Label>
                            <br />
                        </li>
                        <li class="w3-padding-16">
                            <img src="https://st2.depositphotos.com/1007566/12458/v/950/depositphotos_124582494-stock-illustration-dataphone-credit-card-money-icon.jpg" alt="Image" class="w3-left w3-margin-right w3-sepia" style="width: 50px">
                            <span class="w3-large">TID 2:</span>
                            <br>
                            <asp:Label ID="TID2" runat="server" Text="TID 2:"></asp:Label>
                            <br />
                        </li>
                        <li class="w3-padding-16">
                            <img src="https://productimages.nimbledeals.com/images/ncCorp/platform/merchant/img-platform-merchant-consumers.jpg" alt="Image" class="w3-left w3-margin-right w3-sepia" style="width: 50px">
                            <span class="w3-large">Nombre Comercio 2:</span>
                            <br>
                            <asp:Label ID="MerchantNombre2" runat="server" Text="Comercio 2:"></asp:Label>
                            <br />
                        </li>
                        <li class="w3-padding-16">
                            <img src="https://cdn.icon-icons.com/icons2/56/PNG/512/metrophone_metr_11263.png" alt="Image" class="w3-left w3-margin-right w3-sepia" style="width: 50px">
                            <span class="w3-large">Tel Comercio 2:</span>
                            <br>
                            <asp:Label ID="TelMerchant2" runat="server" Text="Tel Comercio 2:"></asp:Label>
                            <br />
                        </li>
                    </ul>
                </div>
                <hr>
            </div>
            <script type="text/javascript">

                $(document).ready(function () {
                    $("#logo").attr("src", logo);
                });
            </script>
            <div class="col-md-4 mx-auto p-4">
                <img width="300" alt="Logo"
                    src="" id="logo" />

            </div>
        </div>
    </div>

    <!-- START SIMPLE FORM -->
    <asp:HyperLink ID="lnkBack" runat="server" NavigateUrl="~/Group/GroupTerminals.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <!-- Validator -->

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"
        integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og=="
        crossorigin=""></script>
    <script type="text/javascript" src="../js/map.js"></script>
</asp:Content>

