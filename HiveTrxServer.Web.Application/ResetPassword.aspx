﻿<%@ Page Title="" Language="VB" MasterPageFile="~/FullPage.master" AutoEventWireup="false" CodeFile="ResetPassword.aspx.vb" Inherits="ResetPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title><%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%>:: Restaurar Clave</title>

    <!-- Toggle -->
    <script type="text/javascript" src="js/toogle.js"></script>
    
    <!-- Modal Alert -->
    <script src="js/msgAlert.js" type="text/javascript"></script>

    <!-- MD5 -->
    <script src="js/md5-min.js" type="text/javascript"></script>

    <!-- SHA-1 -->
    <script src="js/sha1-min.js" type="text/javascript"></script>

    <!-- Google's reCaptcha -->
    <script src='https://www.google.com/recaptcha/api.js'></script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <asp:Panel ID="pnlMsg" runat="server" Visible="False">
        <div class="albox succesbox">
            <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
            <a href="#" class="close tips" title="Cerrar">Cerrar</a>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlError" runat="server" Visible="False">
        <div class="albox errorbox">
            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
            <a href="#" class="close tips" title="Cerrar">Cerrar</a>
        </div>
    </asp:Panel> 

    <div class="loginform">
        <div class="title"> <h2 style="font-size: 18px !important;">Restaurar Clave</h2> </div>
        <div class="body" style="padding-top: 10px !important;">            
            <div class="simple-tips">
                Para terminar su proceso de restauración de clave, complete el siguiente formulario.
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
      	    <label class="log-lab">Nueva Clave:</label>
            <asp:TextBox ID="txtNewPassword" runat="server" class="login-input-pass" style="padding-left: 20px !important;" value="" TabIndex="1" onkeydown = "return (event.keyCode!=13);" TextMode="Password"></asp:TextBox>
            <asp:Image ID="imgPassword" runat="server" ImageUrl="~/img/icons/16x16/cancel.png" />
      	    <label class="log-lab">Repita Nueva Clave:</label>
            <asp:TextBox ID="txtNewPassword2" runat="server" class="login-input-pass" style="padding-left: 20px !important;" value="" TabIndex="2" onkeydown = "return (event.keyCode!=13);" TextMode="Password"></asp:TextBox>
            <asp:Image ID="imgPassword2" runat="server" ImageUrl="~/img/icons/16x16/cancel.png" />
             <% If ConfigurationSettings.AppSettings.Get("UseRecaptcha") = "YES" Then%>
                <div class="g-recaptcha" data-callback="recaptchaCallback" data-sitekey="<%=ConfigurationSettings.AppSettings.Get("reCaptchaSiteKey")%>" style="padding-bottom: 17px;"></div>
            <% End If%>
           
            <asp:HiddenField ID="hidNewActivationCode" runat="server" />
            <asp:Button ID="btnSubmit" runat="server" Text="Cambiar Clave" class="button" TabIndex="3" onclientclick="return validateResetPassword()" />
            <div style="padding-top: 10px;">
                <a href="Login.aspx"><img src="img/previous.png" width="12px" height="12px" alt="Atrás"/> Iniciar Sesión</a>
            </div>
        </div>
    </div>  
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">
    
    <script type="text/javascript">
        $(function () {
            $("#ctl00_MainContent_txtNewPassword").focus();
        });
    </script>    
    
    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
    <!-- Validator -->
    <script src="js/ValidatorLogin.js" type="text/javascript"></script>    
    
</asp:Content>

