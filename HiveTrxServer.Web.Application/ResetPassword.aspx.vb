﻿Imports TeleLoader.Security
Imports System
Imports System.Net
Imports TeleLoader.Sessions
Imports TeleLoader.Users

Partial Class ResetPassword
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            Try

                pnlMsg.Visible = False
                pnlError.Visible = False

                Dim activationCode As String = Request.QueryString(0)

                Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

                Dim connString As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString

                Dim objUser As User = New User(connString)

                objUser.ActivationCode = activationCode

                'Continue Reset Password Process
                If objUser.ContinueResetPasswordProcess() Then
                    hidNewActivationCode.Value = objUser.ActivationCode

                    pnlMsg.Visible = True
                    pnlError.Visible = False
                    lblMsg.Text = "Código de Restauración procesado correctamente. Recuerde que la clave debe contener: <br /><br /><li>8 Caracteres Mínimo.</li><li>Números y Letras</li><li>Al menos 1 caracter especial (*, /, -, +, etc...)</li>"

                Else
                    hidNewActivationCode.Value = "-1"

                    pnlMsg.Visible = False
                    pnlError.Visible = True
                    lblError.Text = "El Código de Restauración ya Expiró. Por favor verifique."

                End If

            Catch ex As Exception
                pnlMsg.Visible = False
                pnlError.Visible = True
                lblError.Text = "Estamos presentando interrupciones en nuestro servicio. Por favor intente mas tarde."
            End Try

        End If
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click

        Dim IsCaptchaValid As Boolean = False
        Dim respVal As Integer

        If ConfigurationSettings.AppSettings.Get("UseRecaptcha") = "YES" Then
            Try
                Dim EncodedResponse As String = Request.Form("g-Recaptcha-Response")
                IsCaptchaValid = IIf(ReCaptcha.Validate(EncodedResponse) = "True", True, False)
            Catch ex As Exception
                Session.Remove("AccessToken")
                ltrScript.Text = "<script language='javascript' type='text/javascript'> $(function() { $.msgAlert({ type: 'error', title: 'Mensaje del sistema', text: 'Error Conectando al Servidor reCAPTCHA: [" & ex.Message.Replace("'", "").Replace("""", "") & "]' }); });</script>"
                Exit Sub
            End Try
        Else
            IsCaptchaValid = True
        End If

        If IsCaptchaValid Then

            Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")

            Dim connString As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString

            Dim objUser As User = New User(connString)

            objUser.ActivationCode = hidNewActivationCode.Value
            objUser.Password = txtNewPassword.Text.Trim()


            'Set new Password & Finish Process
            If objUser.FinishResetPasswordProcess() Then

                txtNewPassword.Text = ""
                txtNewPassword2.Text = ""

                pnlMsg.Visible = True
                pnlError.Visible = False
                lblMsg.Text = "Clave Reestablecida con éxito. Por favor inicie sesión con su nueva clave."

            Else
                pnlMsg.Visible = False
                pnlError.Visible = True
                lblError.Text = "Error en Cambio de Clave. Por favor verifique, contraseña invalida o ha sido usada anteriormente"
            End If

        Else
            Session.Remove("AccessToken")
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(function() { $.msgAlert({ type: 'error', title: 'Mensaje del sistema', text: 'Error de Autenticación reCAPTCHA.' }); });</script>"
        End If
    End Sub

End Class
