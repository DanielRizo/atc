﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AddFunctionsXProfile.aspx.vb" Inherits="Security_AddFunctionsXProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Agregar Funciones X Perfil
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   

	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script>     
        
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed" 
            PostBackUrl="~/Security/Manager.aspx">Seguridad</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkProfiles" runat="server" CssClass="fixed" 
            PostBackUrl="~/Security/Profiles.aspx">Perfiles</asp:LinkButton>
    </li>
    <li>Agregar Funciones X Perfil</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Agregar Funciones X Perfil
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite agregar funciones x perfil al sistema Polaris:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>   
            
        <asp:Panel ID="pnlError" runat="server" Visible="False">    
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>  

	    <div class="titleh">
    	    <h3>Datos del Perfil</h3>
        </div>
        <div class="body">
        
            <div class="st-form-line">	
                <span class="st-labeltext">Nombre:</span>	
                <asp:TextBox ID="txtProfileName" CssClass="st-forminput" style="width:510px" 
                    runat="server" TabIndex="1" MaxLength="100" 
                    ToolTip="Nombre del Perfil." Enabled="False" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                <asp:TextBox ID="txtProfileID" runat="server" Visible="False"></asp:TextBox>                    
                &nbsp;&nbsp;&nbsp;&nbsp;
                <div class="clear"></div>
            </div>        
        
            <div class="st-form-line">	
                <span class="st-labeltext">Funciones del Sistema:</span>	                
                <asp:DropDownList ID="ddlFunctions" runat="server" 
                    DataSourceID="dsFunctions" DataTextField="descripcion" 
                    DataValueField="func_id" ToolTip="Seleccione una función." TabIndex="6">
                </asp:DropDownList>
                <asp:Button ID="btnHidRefresh" runat="server" Text="" Visible="false"  />
                <asp:SqlDataSource ID="dsFunctions" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" 
                    SelectCommand="SELECT F.func_id, O.nombre + ' :: ' + M.nombre + '    ' AS descripcion FROM Funcion F INNER JOIN Objeto O ON (O.id = F.func_objeto_id) INNER JOIN Metodo M ON (M.id = F.func_metodo_id) WHERE func_id NOT IN (SELECT func_id FROM PerfilXFuncion WHERE perf_id = @perf_id) AND LOWER(O.nombre) NOT LIKE '%customer%' ORDER BY 2">                    
                    <SelectParameters>
                        <asp:ControlParameter ControlID="txtProfileID" Name="perf_id" PropertyName="Text" Type="String" />
                    </SelectParameters>                                        
                </asp:SqlDataSource>                
                <div class="clear"></div>
            </div>            
            
            <div class="button-box">
                <asp:Button ID="btnAddFunction" runat="server" Text="Adicionar Función" 
                    CssClass="button-aqua" TabIndex="7" />
            </div>            
        </div>
    </div>
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
	    <div class="titleh">
    	    <h3>Funciones adicionadas al Perfil</h3>
        </div>
        <div class="body">    
            <br />            
            <asp:GridView ID="grdFunctionsXProfile" runat="server" AllowPaging="True" 
                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                DataSourceID="dsFunctionsXProfile" ForeColor="#333333"
                CssClass="mGridCenter" DataKeyNames="func_id" 
                EmptyDataText="No existen Funciones adicionadas a este Perfil." 
                HorizontalAlign="Left"
                OnRowDeleted="btnHidRefresh_Click" Width="1060px">
                <RowStyle BackColor="White" ForeColor="White" />
                <Columns>
                    <asp:TemplateField HeaderText="Código" InsertVisible="False" 
                        SortExpression="func_id">
                        <ItemTemplate>
                            <asp:Label ID="lblID1" runat="server" Text='<%# Bind("func_id") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Descripción" SortExpression="func_descripcion">
                        <ItemTemplate>
                            <asp:Label ID="lblID2" runat="server" Text='<%# Bind("func_descripcion") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Objeto" SortExpression="Objeto">
                        <ItemTemplate>
                            <asp:Label ID="lblID3" runat="server" Text='<%# Bind("Objeto") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>                    
                    <asp:TemplateField HeaderText="Metodo" SortExpression="Metodo">
                        <ItemTemplate>
                            <asp:Label ID="lblID4" runat="server" Text='<%# Bind("Metodo") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>                        
                            <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False" 
                                CommandName="Delete" ImageUrl="~/img/icons/16x16/delete.png" Text="Eliminar" 
                                ToolTip="Eliminar Función del Perfil" CommandArgument='<%# grdFunctionsXProfile.Rows.Count %>' />                        
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                </Columns>
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
                    CssClass="pgr" Font-Underline="False" />
                <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid" 
                    BorderWidth="1px" />
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>            
            <br />
	        <asp:SqlDataSource ID="dsFunctionsXProfile" runat="server" 
                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" 
                SelectCommand="SELECT F.func_id, F.func_descripcion, O.nombre AS Objeto, M.nombre AS Metodo 
                    FROM Funcion F INNER JOIN Objeto O ON (O.id = F.func_objeto_id) 
                    INNER JOIN Metodo M ON (M.id = F.func_metodo_id)
                    INNER JOIN PerfilXFuncion PXF ON (PXF.func_id = F.func_id)
                    INNER JOIN Perfil P ON (P.perf_id = PXF.perf_id)
                    WHERE PXF.perf_id = @perf_id
                    ORDER BY 1" 
                DeleteCommand="DELETE FROM PerfilXFuncion WHERE (perf_id = @perf_id) AND (func_id = @func_id)" 
                InsertCommand="INSERT INTO PerfilXFuncion(perf_id, func_id) VALUES (@perf_id, @func_id)">
                <SelectParameters>
                    <asp:ControlParameter ControlID="txtProfileID" Name="perf_id" 
                        PropertyName="Text" />
                </SelectParameters>
                <DeleteParameters>
                    <asp:ControlParameter ControlID="txtProfileID" Name="perf_id" 
                        PropertyName="Text" />
                    <asp:Parameter Name="func_id" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:ControlParameter ControlID="txtProfileID" Name="perf_id" 
                        PropertyName="Text" />                    
                    <asp:ControlParameter ControlID="ddlFunctions" Name="func_id" 
                        PropertyName="SelectedValue" />
                </InsertParameters>
            </asp:SqlDataSource> 
        </div>
    </div>
    
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Security/Profiles.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">
    
    <!-- Validator -->
    <script src="../js/ValidatorSecurity.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

