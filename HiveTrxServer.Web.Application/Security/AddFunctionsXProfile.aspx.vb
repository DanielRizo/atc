﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users

Partial Class Security_AddFunctionsXProfile
    Inherits TeleLoader.Web.BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            txtProfileID.Text = objSessionParams.intSelectedProfile
            txtProfileName.Text = objSessionParams.strSelectedProfile

            ddlFunctions.Focus()
        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub btnAddFunction_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddFunction.Click

        If ddlFunctions.SelectedIndex <> -1 Then

            'Validar Acceso a Función
            Try
                objAccessToken.Validate(getCurrentPage(), "Save")
            Catch ex As Exception
                HandleErrorRedirect(ex)
            End Try

            If dsFunctionsXProfile.Insert() >= 1 Then
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Función Adicionada al Perfil. Datos[ " & getFormDataLog() & " ]", "")
                pnlError.Visible = False
                pnlMsg.Visible = True
                lblMsg.Text = "Función adicionada correctamente al perfil"

                ddlFunctions.DataBind()
                grdFunctionsXProfile.DataBind()

            Else
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Adicionando Función al Perfil. Datos[ " & getFormDataLog() & " ]", "")
                pnlMsg.Visible = False
                pnlError.Visible = True
                lblError.Text = "Error adicionando función al perfil"
            End If

        Else
            pnlMsg.Visible = False
            pnlError.Visible = True
            lblError.Text = "No hay funciones para adicionar"
        End If


    End Sub

    Protected Sub grdFunctionsXProfile_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdFunctionsXProfile.RowCommand
        Try

            Select Case e.CommandName

                Case "Delete"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    grdFunctionsXProfile.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    Dim strData As String = "Función ID.: " & grdFunctionsXProfile.SelectedDataKey.Values.Item("func_id") & ", Perfil ID.:" & txtProfileID.Text

                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Función Eliminada del Perfil. Datos[ " & strData & " ]", "")

                    pnlError.Visible = False
                    pnlMsg.Visible = True
                    lblMsg.Text = "Función eliminada del Perfil"

                    grdFunctionsXProfile.SelectedIndex = -1

            End Select

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub

    Protected Sub btnHidRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHidRefresh.Click
        ddlFunctions.DataBind()
    End Sub
End Class
