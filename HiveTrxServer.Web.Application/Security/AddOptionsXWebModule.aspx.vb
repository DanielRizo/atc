﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class Security_AddOptionsXWebModule
    Inherits TeleLoader.Web.BasePage

    Public ReadOnly IMG_PATH As String = "img/icons/dashbutton/"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            txtWebModuleName.Text = objSessionParams.strSelectedModule
            txtWebModuleID.Text = objSessionParams.intSelectedModule

            txtOptionName.Focus()

            fillDropDownListImages()

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Private Sub fillDropDownListImages()
        Dim DataDirectory As String = "~/" & IMG_PATH

        Try
            Dim files() As FileInfo = New DirectoryInfo(Server.MapPath(DataDirectory)).GetFiles

            ddlUrlImage.DataSource = files
            ddlUrlImage.DataTextField = "Name"
            ddlUrlImage.DataValueField = "Name"
            ddlUrlImage.DataBind()
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub

    Protected Sub grdWebOptions_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdWebOptions.RowCommand
        Try

            Select Case e.CommandName

                Case "Delete"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    grdWebOptions.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    Dim strData As String = "Opción ID.: " & grdWebOptions.SelectedDataKey.Values.Item("opc_id") _
                                & ", Título: " & CType(grdWebOptions.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblID2"), Label).Text _
                                & ", URL: " & CType(grdWebOptions.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblID3"), Label).Text _
                                & ", Imagen URL: " & CType(grdWebOptions.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("imgUrlIcon"), Image).ImageUrl _
                                & ", Orden: " & CType(grdWebOptions.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblID5"), Label).Text

                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Módulo Web Eliminado. Datos[ " & strData & " ]", "")

                    pnlError.Visible = False
                    pnlMsg.Visible = True
                    lblMsg.Text = "Módulo Web eliminado"
                    grdWebOptions.SelectedIndex = -1
            End Select

            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub

    Protected Sub btnAddOption_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddOption.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Save")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        Try
            'Set Parameters
            dsWebOptions.InsertParameters.Clear()
            dsWebOptions.InsertParameters.Add("optionTitle", TypeCode.String, txtOptionName.Text)
            dsWebOptions.InsertParameters.Add("optionURL", TypeCode.String, ddlURLModules.SelectedItem.Text)
            dsWebOptions.InsertParameters.Add("optionIMGURL", TypeCode.String, "../" + IMG_PATH + ddlUrlImage.SelectedValue)
            dsWebOptions.InsertParameters.Add("optionType", TypeCode.Int32, "2") 'Fixed Data, Option Type
            dsWebOptions.InsertParameters.Add("optionParentID", TypeCode.String, txtWebModuleID.Text)

            If dsWebOptions.Insert() >= 1 Then
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Opción Web Creada. Datos[ " & getFormDataLog() & " ]", "")
                pnlError.Visible = False
                pnlMsg.Visible = True
                lblMsg.Text = "Opción Web creada correctamente"
                deactivateForm()
                grdWebOptions.DataBind()
                dsWebOptions.DataBind()
            Else
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Creando Opción Web. Datos[ " & getFormDataLog() & " ]", "")
                pnlMsg.Visible = False
                pnlError.Visible = True
                lblError.Text = "Error creando Opción Web"
            End If

            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub

    Private Sub deactivateForm()
        ddlUrlImage.Enabled = False
        txtOptionName.Enabled = False
        ddlURLModules.Enabled = False
        btnAddOption.Enabled = False
        btnFinalize.Visible = True
        btnFinalize.Enabled = True
    End Sub

    Private Sub activateForm()
        ddlUrlImage.Enabled = True
        txtOptionName.Enabled = True
        txtOptionName.Text = ""
        ddlURLModules.Enabled = True
        btnAddOption.Enabled = True
        btnFinalize.Visible = False
        btnFinalize.Enabled = False
    End Sub

    Protected Sub btnFinalize_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinalize.Click
        activateForm()
        txtOptionName.Focus()

        'Set Invisible Toggle Panel
        ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"

    End Sub

    Protected Sub ddlUrlImage_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUrlImage.DataBound
        imgIcon.ImageUrl = "../" & IMG_PATH & ddlUrlImage.SelectedValue
    End Sub

    Protected Sub ddlUrlImage_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUrlImage.SelectedIndexChanged
        imgIcon.ImageUrl = "../" & IMG_PATH & ddlUrlImage.SelectedValue

        'Set Visible Toggle Panel
        ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"

    End Sub

End Class
