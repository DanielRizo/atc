﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AddUser.aspx.vb" Inherits="Security_AddUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Agregar Usuario
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>

    <script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

    <script src="../js/md5-min.js" type="text/javascript"></script>

    <script src="../js/sha1-min.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed"
            PostBackUrl="~/Security/Manager.aspx">Seguridad</asp:LinkButton>
    </li>
    <li>Agregar Usuario</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    Agregar Usuarios
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <p>Este módulo le permite agregar usuarios al sistema Polaris:</p>

    <br />

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlError" runat="server" Visible="False">
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <div class="titleh">
            <h3>Datos del Usuario</h3>
        </div>
        <div class="body">

            <div class="st-form-line">
                <span class="st-labeltext">Nombre Completo:</span>
                <asp:TextBox ID="txtUserName" CssClass="st-forminput" Style="width: 510px"
                    runat="server" TabIndex="1" MaxLength="100"
                    ToolTip="Digite nombres y apellidos del usuario." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnValidateUserName" runat="server" Text="Validar"
                    CssClass="button-aqua" TabIndex="2" />
                <div class="clear"></div>
            </div>

            <div class="st-form-line">
                <span class="st-labeltext">Login:</span>
                <asp:TextBox ID="txtLogin" CssClass="st-forminput" Style="width: 510px"
                    runat="server" Enabled="false" ToolTip="Login para entrada al sistema."
                    TabIndex="3" onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="chkLocalPassword" CssClass="uniform" runat="server" Text="Asignar Clave Localmente?" AutoPostBack="True" TabIndex="4"
                    ToolTip="Permitir asignar la clave del usuario en este mismo formulario?" Checked="False" />
                <div class="clear"></div>
            </div>

            <asp:Panel ID="pnlLocalPassword" runat="server" Visible="False">
                <div class="st-form-line">
                    <span class="st-labeltext">Clave de Acceso:</span>
                    <asp:TextBox ID="txtPassword" CssClass="st-forminput" Style="width: 510px"
                        runat="server" TabIndex="4" MaxLength="20" TextMode="Password"
                        ToolTip="Digite la clave de acceso para el usuario." onkeydown="return (event.keyCode!=13);" onpaste="javascript:return false;"></asp:TextBox>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Image ID="imgPassword" runat="server" ImageUrl="~/img/icons/16x16/cancel.png" />
                    <div class="clear"></div>
                </div>
            </asp:Panel>

            <div class="st-form-line">
                <span class="st-labeltext">Correo Electrónico:</span>
                <asp:TextBox ID="txtEmail" CssClass="st-forminput" Style="width: 510px"
                    runat="server" TabIndex="5" MaxLength="100"
                    ToolTip="Digite correo electrónico del usuario." onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>

            <div class="st-form-line">
                <span class="st-labeltext">Perfil de Usuario:</span>
                <asp:DropDownList ID="ddlUserProfile" class="uniform" runat="server"
                    DataSourceID="dsUserProfile" DataTextField="perf_nombre"
                    DataValueField="perf_id" Width="200px"
                    ToolTip="Seleccione el perfil del usuario." TabIndex="6" AutoPostBack="True">
                </asp:DropDownList>
                <asp:SqlDataSource ID="dsUserProfile" runat="server"
                    ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" SelectCommand="SELECT -1 AS perf_id, '       ' AS perf_nombre
                    UNION ALL
                    SELECT * FROM Perfil WHERE perf_id NOT IN (7, 18)"></asp:SqlDataSource>
                <div class="clear"></div>
            </div>

            <div class="st-form-line">
                <span class="st-labeltext">Cliente:</span>
                <asp:DropDownList ID="ddlCustomer" class="uniform" runat="server"
                    DataSourceID="dsCustomer" DataTextField="cli_nombre"
                    DataValueField="cli_id" Width="200px"
                    ToolTip="Seleccione el cliente a asociar con este usuario." TabIndex="7" Enabled="False">
                </asp:DropDownList>
                <asp:SqlDataSource ID="dsCustomer" runat="server"
                    ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" SelectCommand="SELECT -1 AS cli_id, '       ' AS cli_nombre
                    UNION ALL
                    SELECT cli_id, cli_nombre FROM CLIENTE"></asp:SqlDataSource>
                <div class="clear"></div>
            </div>

            <div class="button-box">
                <asp:Button ID="btnSave" runat="server" Text="Crear Usuario"
                    CssClass="button-aqua" TabIndex="7" OnClientClick="return validateAddUser()" />
            </div>
        </div>
    </div>

    <asp:HyperLink ID="lnkBack" runat="server" NavigateUrl="~/Security/Manager.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <!-- Validator -->
    <script src="../js/ValidatorSecurity.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
        $(function () {
            validatePasswordIcon();
        });
    </script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

</asp:Content>

