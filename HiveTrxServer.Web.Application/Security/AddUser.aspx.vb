﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users
Imports System.Net
Imports TeleLoader.Security

Partial Class Security_AddUser
    Inherits TeleLoader.Web.BasePage

    Private ReadOnly SEPARATOR As String = " "
    Private ReadOnly SEPARATOR_LOGIN As String = "."

    Public userObj As User
    Dim Correo As String
    Dim Password As String

    Public Shared Function GetIP4Address() As String
        Dim IP4Address As String = String.Empty

        For Each IPA As IPAddress In Dns.GetHostAddresses(HttpContext.Current.Request.UserHostAddress)
            If IPA.AddressFamily.ToString() = "InterNetwork" Then
                IP4Address = IPA.ToString()
                Exit For
            End If
        Next

        If IP4Address <> String.Empty Then
            Return IP4Address
        End If

        For Each IPA As IPAddress In Dns.GetHostAddresses(Dns.GetHostName())
            If IPA.AddressFamily.ToString() = "InterNetwork" Then
                IP4Address = IPA.ToString()
                Exit For
            End If
        Next

        Return IP4Address
    End Function
    Private Function getDatosCorreo()
        Dim Json As String
        Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString
        Dim db As SqlConnection = New SqlConnection(conexion)
        Dim cmd As SqlCommand
        Dim sqlBuilder As StringBuilder = New StringBuilder
        Dim results As SqlDataReader
        Dim retVal As Boolean

        Dim strRsp As String
        strRsp = ""
        sqlBuilder.Append("SELECT CAST(DecryptByPassPhrase( 'Clave',correo_clave ) as varchar(max)) as 'password' ,CAST(DecryptByPassPhrase( 'Clave',correo_nombre ) as varchar(max)) as 'correo'  FROM dbo.correo")

        Try
            db.Open()
        Catch ex As Exception

            db.Close()
        End Try

        Try
            cmd = New SqlCommand()
            cmd.Connection = db
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sqlBuilder.ToString
            'cmd.Parameters.AddWithValue("@TRN", Data)

            Try

            Catch ex As Exception
            End Try
            'Ejecutar SP
            results = cmd.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    Password = results.GetString(0)
                    Correo = results.GetString(1)
                End While
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
        End Try
    End Function
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        getDatosCorreo()

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Save")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        Dim passwordType As Integer
        userObj = New User(strConnectionString, objAccessToken.UserID)

        'Set New Data
        userObj.DisplayName = txtUserName.Text
        userObj.Email = txtEmail.Text
        userObj.LoginName = txtLogin.Text
        userObj.Profile = ddlUserProfile.SelectedValue
        userObj.Customer = ddlCustomer.SelectedValue

        If chkLocalPassword.Checked = True Then
            'Set Local Password
            userObj.Password = txtPassword.Text
            passwordType = 1
        Else
            'Send Instructions Via Mail
            userObj.Password = ""
            passwordType = 2
        End If

        'Save Data
        If userObj.createUser() Then
            'New User Created OK
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Usuario Creado. Datos[ " & getFormDataLog() & " ]", "")

            If passwordType = 2 Then
                '1. Start Reset Password Process
                If userObj.StartResetPasswordProcess(GetIP4Address()) Then
                    If MailSender.SendEmail(userObj.Email, userObj.ActivationCode, 2, userObj.LoginName, Correo, Password) Then  '2 = Create New Password
                        pnlMsg.Visible = True
                        pnlError.Visible = False
                        lblMsg.Text = "Usuario Creado correctamente. Se ha enviado un mensaje a la cuenta de correo con instrucciones de creación de clave."
                    Else
                        pnlMsg.Visible = False
                        pnlError.Visible = True
                        lblError.Text = "Usuario Creado correctamente. No se pudo enviar el mensaje de correo. Por favor revisar."
                    End If
                End If
            Else
                pnlMsg.Visible = True
                pnlError.Visible = False
                ddlCustomer.Enabled = False
                lblMsg.Text = "Usuario creado correctamente."
            End If

            clearForm()
        Else
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Creando Usuario. Datos[ " & getFormDataLog() & " ]", "")
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error creando usuario. Nombre de usuario o Correo Electrónico ya existen."
        End If

    End Sub

    Private Sub clearForm()
        txtUserName.Text = ""
        txtEmail.Text = ""
        txtLogin.Text = ""
        txtPassword.Text = ""
        ddlUserProfile.SelectedValue = -1

        chkLocalPassword.Checked = False
        pnlLocalPassword.Visible = False

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        SetCssClasses()

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then
            txtUserName.Focus()
        Else
            If chkLocalPassword.Checked = True Then
                pnlLocalPassword.Visible = True
                pnlMsg.Visible = True
                pnlError.Visible = False
                txtPassword.Attributes.Add("value", txtPassword.Text)
            Else
                pnlMsg.Visible = False
                pnlError.Visible = False
                pnlLocalPassword.Visible = False
                txtPassword.Text = ""
            End If
        End If
    End Sub

    Protected Sub btnValidateUserName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnValidateUserName.Click

        'Validar Campo Nombre de Usuario
        If txtUserName.Text.Length < 10 Or txtUserName.Text.Trim().Split(SEPARATOR).Length <= 1 Then
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Ingrese el campo Nombre Completo correctamente. Mínimo un(1) Nombre, un(1) Apellidoy una longitud de 10 Caracteres"
            Exit Sub
        End If

        'Set Login
        txtLogin.Text = getUserLogin()

        If txtLogin.Text.Trim().Length <= 0 Then
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Login Ya registrado, Digite otro nombre."
        Else
            pnlError.Visible = False
            txtPassword.Focus()
        End If

    End Sub

    Function changeNonStandardChars(ByVal data As String) As String
        Dim retVal As String = ""

        data = data.ToLower().Replace("á", "a")
        data = data.ToLower().Replace("é", "e")
        data = data.ToLower().Replace("í", "i")
        data = data.ToLower().Replace("ó", "o")
        data = data.ToLower().Replace("ú", "u")
        data = data.ToLower().Replace("ñ", "n")

        retVal = data

        Return retVal
    End Function

    Function getUserLogin() As String
        Dim connection As New SqlConnection(ConnectionString())
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim retVal As String = ""
        Dim status As Integer = 0
        Dim i As Integer = 0
        Dim tmpUserName() As String = changeNonStandardChars(txtUserName.Text).Trim().Split(SEPARATOR)
        Dim posibleUserName As String = ""

        Try
            'Abrir Conexion
            connection.Open()

            For i = 1 To tmpUserName(0).Length
                If tmpUserName.Length = 2 Then
                    posibleUserName = tmpUserName(0).Substring(0, i) & SEPARATOR_LOGIN & tmpUserName(1)
                ElseIf tmpUserName.Length = 3 Then
                    posibleUserName = tmpUserName(0).Substring(0, i) & tmpUserName(1).Substring(0, 1) & SEPARATOR_LOGIN & tmpUserName(2)
                ElseIf tmpUserName.Length = 4 Then
                    posibleUserName = tmpUserName(0).Substring(0, i) & tmpUserName(1).Substring(0, 1) & tmpUserName(2).Substring(0, 1) & SEPARATOR_LOGIN & tmpUserName(3)
                End If

                'Verify Login vs. DB

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webValidarLoginName"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("userID", posibleUserName))

                'Ejecutar SP
                results = command.ExecuteReader()

                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = ""
                End If

                If status = 0 Then
                    'Login Disponible
                    retVal = posibleUserName
                    Exit For
                Else
                    retVal = ""
                End If

                results.Close()
                results = Nothing

            Next i

            connection.Close()
        Catch ex As Exception
            retVal = ""
            HandleErrorRedirect(ex)
        End Try

        Return retVal

    End Function

    Private Sub SetCssClasses()
        chkLocalPassword.InputAttributes("class") = "uniform"
    End Sub

    Protected Sub chkLocalPassword_CheckedChanged(sender As Object, e As EventArgs) Handles chkLocalPassword.CheckedChanged
        If chkLocalPassword.Checked = True Then
            pnlLocalPassword.Visible = True
            txtPassword.Text = ""
            txtPassword.Focus()

            pnlMsg.Visible = True
            pnlError.Visible = False
            lblMsg.Text = "Recuerde que la clave debe contener: <br /><br /><li>8 Caracteres Mínimo.</li><li>Números y Letras</li><li>Al menos 1 caracter especial (*, /, -, +, etc...)</li>"

        Else

            pnlMsg.Visible = False
            pnlError.Visible = False

            pnlLocalPassword.Visible = False
            txtPassword.Text = ""
        End If
    End Sub

    Protected Sub ddlUserProfile_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlUserProfile.SelectedIndexChanged
        If ddlUserProfile.SelectedValue = 7 Or ddlUserProfile.SelectedValue = 18 Then    '7=Admin. Licencias; 18=Seguridad
            ddlCustomer.SelectedIndex = -1
            ddlCustomer.Enabled = False
        Else
            ddlCustomer.Enabled = True
        End If
    End Sub
End Class
