﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AddWebModulesXProfile.aspx.vb" Inherits="Security_AddWebModulesXProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Agregar Módulos Web X Perfil
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   

	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script>     
        
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed" 
            PostBackUrl="~/Security/Manager.aspx">Seguridad</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkProfiles" runat="server" CssClass="fixed" 
            PostBackUrl="~/Security/Profiles.aspx">Perfiles</asp:LinkButton>
    </li>
    <li>Agregar Módulos Web X Perfil</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Agregar Módulos Web X Perfil
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite agregar módulos web x perfil al sistema Polaris:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
	    <div class="titleh">
    	    <h3>Datos del Perfil</h3>
        </div>
        <div class="body">
        
            <div class="st-form-line">	
                <span class="st-labeltext">Nombre:</span>	
                <asp:TextBox ID="txtProfileName" CssClass="st-forminput" style="width:510px" 
                    runat="server" TabIndex="1" MaxLength="100" 
                    ToolTip="Nombre del Perfil." Enabled="False" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                <asp:TextBox ID="txtProfileID" runat="server" Visible="False"></asp:TextBox>
                <asp:TextBox ID="txtWebModuleID" runat="server" Visible="False"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <div class="clear"></div>
            </div>        
        
            <div class="st-form-line">	
                <span class="st-labeltext">Módulos Web Disponibles:</span>	                
                <asp:DropDownList ID="ddlWebModules" class="uniform" runat="server" 
                    DataSourceID="dsWebModules" DataTextField="opc_titulo" 
                    DataValueField="opc_id" ToolTip="Seleccione un Módulo Web." TabIndex="6">
                </asp:DropDownList>
                <asp:Button ID="btnHidRefresh" runat="server" Text="" Visible="false"  />
                <asp:SqlDataSource ID="dsWebModules" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" 
                    SelectCommand="SELECT O.opc_id, O.opc_titulo FROM Opcion O WHERE O.opc_tipo_opcion_id = 1 AND O.opc_id NOT IN ( SELECT O2.opc_id FROM Opcion O2 INNER JOIN PerfilXOpcion PXO2 ON (O2.opc_id = PXO2.opc_id AND O2.opc_tipo_opcion_id = 1) INNER JOIN Perfil P2 ON (P2.perf_id = PXO2.perf_id AND P2.perf_id = @perf_id) ) AND LOWER(O.opc_titulo) <> LOWER('clientes')">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="txtProfileID" Name="perf_id" PropertyName="Text" Type="String" />
                    </SelectParameters>                                        
                </asp:SqlDataSource>                
                <div class="clear"></div>
            </div>            
            
            <div class="button-box">
                <asp:Button ID="btnAddWebModule" runat="server" Text="Adicionar Módulo Web" 
                    CssClass="button-aqua" TabIndex="7" />
            </div>
            
            <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
                <div class="albox succesbox">
                    <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">Cerrar</a>
                </div>
            </asp:Panel>   
            
            <asp:Panel ID="pnlError" runat="server" Visible="False">    
                <div class="albox errorbox">
                    <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">Cerrar</a>
                </div>
            </asp:Panel>                       
            
        </div>
    </div>
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
	    <div class="titleh">
    	    <h3>Módulos Web adicionados al Perfil</h3>
        </div>
        <div class="body">    
            <br />            
            <asp:GridView ID="grdWebModulesXProfile" runat="server" AllowPaging="True" 
                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                DataSourceID="dsWebModulesXProfile" ForeColor="#333333"
                CssClass="mGridCenter" DataKeyNames="opc_id" 
                EmptyDataText="No existen Módulos Web adicionados a este Perfil." 
                HorizontalAlign="Left"
                OnRowDeleted="btnHidRefresh_Click" Width="1060px">
                <RowStyle BackColor="White" ForeColor="White" />
                <Columns>
                    <asp:TemplateField HeaderText="Código" InsertVisible="False" 
                        SortExpression="opc_id">
                        <ItemTemplate>
                            <asp:Label ID="lblID1" runat="server" Text='<%# Bind("opc_id") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Título" SortExpression="opc_titulo">
                        <ItemTemplate>
                            <asp:Label ID="opc_titulo" runat="server" Text='<%# Bind("opc_titulo") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Opciones Activas" SortExpression="OpcionesActivas">
                        <ItemTemplate>
                            <asp:Label ID="opc_activas" runat="server" Text='<%# Bind("OpcionesActivas") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>                        
                            <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False" 
                                CommandName="Delete" ImageUrl="~/img/icons/16x16/delete.png" Text="Eliminar" 
                                ToolTip="Eliminar Módulo Web del Perfil" CommandArgument='<%# grdWebModulesXProfile.Rows.Count %>' />                        
                            <asp:ImageButton ID="imgActivateOptions" runat="server" CausesValidation="False" 
                                CommandName="ActivateOptions" ImageUrl="~/img/icons/16x16/activate.png" Text="Activar/Desactivar Opciones" 
                                ToolTip="Activar/Desactivar Opciones del Módulo Web" CommandArgument='<%# grdWebModulesXProfile.Rows.Count %>' />
                        </ItemTemplate>
                    </asp:TemplateField>                    
                </Columns>
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
                    CssClass="pgr" Font-Underline="False" />
                <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid" 
                    BorderWidth="1px" />
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>            
            
	        <asp:SqlDataSource ID="dsWebModulesXProfile" runat="server" 
                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" 
                SelectCommand="SELECT Op.opc_id, Op.opc_titulo, (SELECT COUNT(*) FROM Opcion Op2 INNER JOIN PerfilXOpcion PXO ON (PXO.opc_id=Op2.opc_id) INNER JOIN Perfil P ON (PXO.perf_id=P.perf_id) WHERE Op2.opc_opcion_padre_id = Op.opc_id AND P.perf_id = @perf_id) AS OpcionesActivas
                    FROM Opcion Op 
                    INNER JOIN PerfilXOpcion PXO ON (PXO.opc_id = Op.opc_id)
                    INNER JOIN Perfil P ON (P.perf_id = PXO.perf_id)
                    WHERE PXO.perf_id = @perf_id AND Op.opc_tipo_opcion_id = 1
                    ORDER BY 1" 
                DeleteCommand="sp_webEliminarModuloWebXPerfil" DeleteCommandType="StoredProcedure"
                InsertCommand="sp_webAgregarModuloWebXPerfil" InsertCommandType="StoredProcedure">
                <SelectParameters>
                    <asp:ControlParameter ControlID="txtProfileID" Name="perf_id" 
                        PropertyName="Text" />
                </SelectParameters>
                <DeleteParameters>
                    <asp:ControlParameter ControlID="txtProfileID" Name="profileID" 
                        PropertyName="Text" />
                    <asp:Parameter Name="opc_id" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:ControlParameter ControlID="txtProfileID" Name="profileID" 
                        PropertyName="Text" />                    
                    <asp:ControlParameter ControlID="ddlWebModules" Name="webModuleID" 
                        PropertyName="SelectedValue" />
                </InsertParameters>
            </asp:SqlDataSource> 
        </div>
    </div>
    
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Security/Profiles.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>    
    
    <ajaxtoolkit:ToolkitScriptManager ID="scrMgrAsp" runat="server">
    </ajaxtoolkit:ToolkitScriptManager>
    
    <asp:Panel ID="pnlPopupActivateOptions" runat="server" CssClass="modalPopup">
        <asp:Label ID="lblTitulo" runat="server" 
            Text="Activar/Desactivar Opciones:" Font-Bold="True" 
            Font-Size="Medium"></asp:Label>
        <br />
        <br />
        <b>Perfil:</b>&nbsp;&nbsp;<asp:Label ID="lblProfileName" runat="server" Text="Label"></asp:Label>
        <br />
        <b>ID Módulo Web:</b>&nbsp;&nbsp;<asp:Label ID="lblWebModuleID" runat="server" Text="Label"></asp:Label>
        <br />
        <b>Nombre Módulo Web:</b>&nbsp;&nbsp;<asp:Label ID="lblWebModuleName" runat="server" Text="Label"></asp:Label>
        <br />

        <br /> 
        
        <asp:GridView ID="grdOptionsXWebModules" runat="server"
            AutoGenerateColumns="False" CellPadding="4" 
            DataSourceID="dsOptionsXModule" ForeColor="#333333" Width="300px" 
            CssClass="mGridCenter" 
            EmptyDataText="No existen Opciones para este Módulo Web." 
            HorizontalAlign="Center">
            <RowStyle BackColor="White" ForeColor="White" />
            <Columns>
                <asp:TemplateField HeaderText="Código" InsertVisible="False" 
                    SortExpression="opc_id">
                    <ItemTemplate>
                        <asp:Label ID="lblID1" runat="server" Text='<%# Bind("opc_id") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Título Opción" SortExpression="opc_titulo">
                    <ItemTemplate>
                        <asp:Label ID="lblID2" runat="server" Text='<%# Bind("opc_titulo") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Activar?" SortExpression="Activo">
                    <ItemTemplate>
                        <asp:CheckBox ID="chkActive" runat="server" Checked='<%# Bind("Activo") %>' 
                            Enabled="true" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>
                
        <asp:SqlDataSource ID="dsOptionsXModule" runat="server" 
            ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"             
            SelectCommand="SELECT opc_id, opc_titulo, CAST((CASE WHEN T.ACTIVO IS NULL THEN 0 ELSE 1 END) AS BIT) AS Activo FROM (SELECT DISTINCT O.opc_id, O.opc_titulo, (SELECT opc_id FROM PerfilXOpcion AS PXO2 WHERE (opc_id = O.opc_id) AND (perf_id = @perf_id)) AS Activo FROM PerfilXOpcion AS PXO LEFT OUTER JOIN Perfil AS P ON PXO.perf_id = P.perf_id AND P.perf_id = @perf_id FULL OUTER JOIN Opcion AS O ON PXO.opc_id = O.opc_id WHERE (O.opc_id IS NOT NULL AND O.opc_tipo_opcion_id = 2 AND O.opc_opcion_padre_id = @opc_opcion_padre_id)) AS T">
            <SelectParameters>
                <asp:ControlParameter ControlID="txtProfileID" Name="perf_id" 
                    PropertyName="Text" />
                <asp:ControlParameter ControlID="txtWebModuleID" Name="opc_opcion_padre_id" 
                    PropertyName="Text" />
            </SelectParameters>
        </asp:SqlDataSource>        
        
        <br />
        <br />        
        <asp:Button ID="btnActivateOption" runat="server" Text="Guardar" CssClass="button-aqua" />
        <br />

        <asp:LinkButton ID="lnkCancel" runat="server" Font-Bold="True" OnClientClick="SetZIndexUp();"
            Font-Size="Small">Cancelar</asp:LinkButton>
    </asp:Panel>
    
    <asp:Button ID="btnPopUp" runat="server" Style="display: none" Text="Invisible Button" />
    
    <ajaxtoolkit:ModalPopupExtender ID="modalPopupExt" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="lnkCancel" TargetControlID="btnPopUp" PopupControlID="pnlPopupActivateOptions">
    </ajaxtoolkit:ModalPopupExtender>
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">
    
    <!-- Validator -->
    <script src="../js/ValidatorSecurity.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

