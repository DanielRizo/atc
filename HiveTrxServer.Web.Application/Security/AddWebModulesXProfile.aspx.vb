﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users

Partial Class Security_AddWebModulesXProfile
    Inherits TeleLoader.Web.BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            txtProfileID.Text = objSessionParams.intSelectedProfile
            txtProfileName.Text = objSessionParams.strSelectedProfile

            ddlWebModules.Focus()
        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub btnAddWebModule_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddWebModule.Click

        If ddlWebModules.SelectedIndex <> -1 Then

            'Validar Acceso a Función
            Try
                objAccessToken.Validate(getCurrentPage(), "Save")
            Catch ex As Exception
                HandleErrorRedirect(ex)
            End Try

            If dsWebModulesXProfile.Insert() >= 1 Then
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Módulo Web Adicionado al Perfil. Datos[ " & getFormDataLog() & " ]", "")
                pnlError.Visible = False
                pnlMsg.Visible = True
                lblMsg.Text = "Módulo Web adicionado correctamente al perfil"

                ddlWebModules.DataBind()
                grdWebModulesXProfile.DataBind()

            Else
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Adicionando Módulo Web al Perfil. Datos[ " & getFormDataLog() & " ]", "")
                pnlMsg.Visible = False
                pnlError.Visible = True
                lblError.Text = "Error adicionando módulo web al perfil"
            End If

        Else
            pnlMsg.Visible = False
            pnlError.Visible = True
            lblError.Text = "No hay módulos web para adicionar"
        End If


    End Sub

    Protected Sub grdWebModulesXProfile_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdWebModulesXProfile.RowCommand
        Try

            Select Case e.CommandName

                Case "Delete"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    grdWebModulesXProfile.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    Dim strData As String = "Opción ID.: " & grdWebModulesXProfile.SelectedDataKey.Values.Item("opc_id") & ", Perfil ID.:" & txtProfileID.Text

                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Opción Eliminada del Módulo Web. Datos[ " & strData & " ]", "")

                    pnlError.Visible = False
                    pnlMsg.Visible = True
                    lblMsg.Text = "Opción eliminada del Módulo Web"

                    grdWebModulesXProfile.SelectedIndex = -1

                Case "ActivateOptions"
                    grdWebModulesXProfile.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    lblProfileName.Text = txtProfileName.Text
                    txtWebModuleID.Text = grdWebModulesXProfile.SelectedDataKey.Values.Item("opc_id")
                    lblWebModuleID.Text = grdWebModulesXProfile.SelectedDataKey.Values.Item("opc_id")
                    lblWebModuleName.Text = CType(grdWebModulesXProfile.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("opc_titulo"), Label).Text()

                    grdOptionsXWebModules.DataBind()
                    dsOptionsXModule.DataBind()

                    ltrScript.Text = "<script language='javascript'>$(function(){ SetZIndexDown(); });</script>"

                    modalPopupExt.Show()

                    grdWebModulesXProfile.SelectedIndex = -1

            End Select

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub

    Protected Sub btnHidRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHidRefresh.Click
        ddlWebModules.DataBind()
    End Sub

    Protected Sub btnActivateOption_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActivateOption.Click

        Dim connection As New SqlConnection(strConnectionString)        

        Try
            'Abrir Conexion
            connection.Open()

            For Each fila As GridViewRow In grdOptionsXWebModules.Rows
                Dim optionID As String = CType(fila.Cells(0).FindControl("lblID1"), Label).Text
                Dim chkActivateOption As CheckBox = CType(fila.Cells(0).FindControl("chkActive"), CheckBox)

                Dim command As New SqlCommand()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webActivarOpcionWebXPerfil"

                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("profileID", txtProfileID.Text))
                command.Parameters.Add(New SqlParameter("optionID", optionID))
                command.Parameters.Add(New SqlParameter("active", chkActivateOption.Checked))

                'Leer Resultado
                command.ExecuteNonQuery()

                'Cerrar
                command = Nothing
            Next

            pnlMsg.Visible = True
            pnlError.Visible = False
            lblMsg.Text = "Opciones activadas en Módulo Web"

            grdWebModulesXProfile.DataBind()
            dsWebModulesXProfile.DataBind()

            ltrScript.Text = "<script language='javascript'>$(function(){ SetZIndexUp(); });</script>"

        Catch ex As Exception
            pnlMsg.Visible = False
            pnlError.Visible = True
            lblError.Text = "Error Activando Opciones X Módulo Web"
        Finally
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try

    End Sub
End Class
