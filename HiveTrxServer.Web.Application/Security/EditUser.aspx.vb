﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users
Imports System.Net
Imports TeleLoader.Security

Partial Class Security_EditUser
    Inherits TeleLoader.Web.BasePage

    Private ReadOnly SEPARATOR As String = " "
    Private ReadOnly SEPARATOR_LOGIN As String = "."

    Public userObj As User
    Dim Correo As String
    Dim Password As String

    Public Shared Function GetIP4Address() As String
        Dim IP4Address As String = String.Empty

        For Each IPA As IPAddress In Dns.GetHostAddresses(HttpContext.Current.Request.UserHostAddress)
            If IPA.AddressFamily.ToString() = "InterNetwork" Then
                IP4Address = IPA.ToString()
                Exit For
            End If
        Next

        If IP4Address <> String.Empty Then
            Return IP4Address
        End If

        For Each IPA As IPAddress In Dns.GetHostAddresses(Dns.GetHostName())
            If IPA.AddressFamily.ToString() = "InterNetwork" Then
                IP4Address = IPA.ToString()
                Exit For
            End If
        Next

        Return IP4Address
    End Function

    Private Function getDatosCorreo()
        Dim Json As String
        Dim configurationSection As ConnectionStringsSection = System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
        Dim conexion As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString
        Dim db As SqlConnection = New SqlConnection(conexion)
        Dim cmd As SqlCommand
        Dim sqlBuilder As StringBuilder = New StringBuilder
        Dim results As SqlDataReader

        Dim retVal As Boolean

        Dim strRsp As String
        strRsp = ""
        sqlBuilder.Append("SELECT CAST(DecryptByPassPhrase( 'Clave',correo_clave ) as varchar(max)) as 'password' ,CAST(DecryptByPassPhrase( 'Clave',correo_nombre ) as varchar(max)) as 'correo'  FROM dbo.correo")

        Try
            db.Open()
        Catch ex As Exception

            db.Close()
        End Try

        Try
            cmd = New SqlCommand()
            cmd.Connection = db
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sqlBuilder.ToString
            'cmd.Parameters.AddWithValue("@TRN", Data)

            Try

            Catch ex As Exception
            End Try
            'Ejecutar SP
            results = cmd.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    Password = results.GetString(0)
                    Correo = results.GetString(1)
                End While
            Else
                retVal = False
            End If

        Catch ex As Exception
            retVal = False
        Finally
            'Cerrar data reader por Default
            If Not (results Is Nothing) Then
                results.Close()
            End If
        End Try
    End Function
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        getDatosCorreo()

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Update")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        userObj = New User(strConnectionString, objAccessToken.UserID)

        'Set New Data
        userObj.DisplayName = txtUserName.Text
        userObj.Email = txtEmail.Text
        userObj.LoginName = txtLogin.Text
        userObj.Profile = ddlUserProfile.SelectedValue
        userObj.Status = userStatus.Value
        userObj.PerfilUser = ddlUserProfile.SelectedValue
        userObj.Customer = ddlCustomer.SelectedValue

        If rbResetPasswordLocal.Checked = True Then
            userObj.Password = txtPassword.Text
        Else
            userObj.Password = ""
        End If

        'Update Data
        If userObj.editUser() Then
            'User Edited OK
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Update", "Usuario Editado. Datos[ " & getFormDataLog() & " ]", "")

            'Send Instructions Via Mail
            If rbResetPasswordMail.Checked = True Then
                '1. Start Reset Password Process
                If userObj.StartResetPasswordProcess(GetIP4Address()) Then

                    If MailSender.SendEmail(userObj.Email, userObj.ActivationCode, 1, "", Correo, Password) Then  '1 = Reset Password  
                        pnlMsg.Visible = True
                        pnlError.Visible = False
                        lblMsg.Text = "Usuario editado correctamente. Se ha enviado un mensaje a su cuenta de correo para continuar con el proceso de restauración de clave."
                    Else
                        pnlMsg.Visible = False
                        pnlError.Visible = True
                        lblError.Text = "Estamos presentando interrupciones en nuestro servicio. Por favor intente mas tarde."
                    End If
                End If
            Else
                pnlMsg.Visible = True
                pnlError.Visible = False
                lblMsg.Text = "Usuario editado correctamente."
            End If

            setSwitchStatus(userObj.Status)

        Else
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Update", "Error Editando Usuario. Datos[ " & getFormDataLog() & " ]", "")
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error editando usuario."
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        SetCssClasses()

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            If objSessionParams.strUserLogin <> "" Then
                'Edit Mode
                userObj = New User(strConnectionString, objAccessToken.UserID)
                userObj.getUserData(objSessionParams.strUserLogin)

                'Set Data
                txtUserName.Text = userObj.DisplayName
                txtEmail.Text = userObj.Email
                txtLogin.Text = userObj.LoginName
                txtPassword.Text = userObj.Password
                ddlUserProfile.SelectedValue = userObj.Profile
                ddlCustomer.SelectedValue = userObj.Customer
                userStatus.Value = userObj.Status
                setSwitchStatus(userObj.Status)

            End If

            txtUserName.Focus()
        End If
    End Sub

    Private Sub setSwitchStatus(status As Integer)
        Dim htmlSwitch As String

        If status = 1 Then
            htmlSwitch = "<p class='field switch' id='switchStatus'><label for='radio1' id='radio11' class='cb-enable selected'><span>Activo</span></label><label for='radio2' id='radio12' class='cb-disable'><span>Inactivo</span></label></p>"
        Else
            htmlSwitch = "<p class='field switch' id='switchStatus'><label for='radio1' id='radio11' class='cb-enable'><span>Activo</span></label><label for='radio2' id='radio12' class='cb-disable selected'><span>Inactivo</span></label></p>"
        End If
        ltrSwitch.Text = htmlSwitch

        ltrScript.Text = "<script language='javascript' type='text/javascript'> $(function() { $('#radio11').click(function() { $('#ctl00_MainContent_userStatus').attr('value', '1'); }); $('#radio12').click(function() { $('#ctl00_MainContent_userStatus').attr('value', '2'); }); });</script>"
    End Sub

    Protected Sub rbResetPasswordLocal_CheckedChanged(sender As Object, e As EventArgs) Handles rbResetPasswordLocal.CheckedChanged
        If rbResetPasswordLocal.Checked = True Then
            pnlPassword.Visible = True
            txtPassword.Enabled = True
            txtPassword.Text = ""
            txtPassword.Focus()

            pnlMsg.Visible = True
            pnlError.Visible = False
            lblMsg.Text = "Recuerde que la clave debe contener: <br /><br /><li>10 Caracteres Mínimo.</li><li>Números, Letras Letras minúsculas y mayúsculas.</li><li>Al menos 1 caracter especial (*, /, -, +, etc...)</li>"

        End If
    End Sub

    Protected Sub rbResetPasswordMail_CheckedChanged(sender As Object, e As EventArgs) Handles rbResetPasswordMail.CheckedChanged
        If rbResetPasswordMail.Checked = True Then
            pnlPassword.Visible = False
            txtPassword.Enabled = False

            pnlMsg.Visible = False
            pnlError.Visible = False

            txtPassword.Text = ""
        End If
    End Sub

    Protected Sub rbResetPasswordNo_CheckedChanged(sender As Object, e As EventArgs) Handles rbResetPasswordNo.CheckedChanged
        If rbResetPasswordNo.Checked = True Then
            pnlPassword.Visible = False
            txtPassword.Enabled = False

            pnlMsg.Visible = False
            pnlError.Visible = False

            txtPassword.Text = ""
        End If
    End Sub

    Private Sub SetCssClasses()

        rbResetPasswordLocal.InputAttributes("class") = "uniform"
        rbResetPasswordNo.InputAttributes("class") = "uniform"
        rbResetPasswordMail.InputAttributes("class") = "uniform"

    End Sub

End Class
