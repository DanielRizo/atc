﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Functions.aspx.vb" Inherits="Security_Functions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Administración de Funciones
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script>     
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed" 
            PostBackUrl="~/Security/Manager.aspx">Seguridad</asp:LinkButton>
    </li>
    <li>Admin. Funciones</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Administración de Funciones
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite administrar los funciones del sistema Polaris:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
    
        <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel> 

        <asp:Panel ID="pnlError" runat="server" Visible="False">    
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel> 

        <asp:Panel ID="pnlInsert" runat="server" CssClass="dialogbox">
            <br />
            <b>&nbsp;Descripción de la Función:&nbsp;&nbsp; </b>
            <asp:TextBox ID="txtFunction" 
                runat="server" Width="300px" MaxLength="50" CssClass="st-forminput-active" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
            <br />
            <br />

            <b>&nbsp;Objeto:&nbsp;&nbsp; </b>            
            <asp:DropDownList ID="ddlObjects" runat="server" class="uniform" DataSourceID="dsObjects" 
                DataTextField="nombre" DataValueField="id" Width="300px" 
                AutoPostBack="True">
            </asp:DropDownList>
            <b>&nbsp;Método:&nbsp;&nbsp; </b>            
            <asp:DropDownList ID="ddlMethods" runat="server" class="uniform" DataSourceID="dsMethods" 
                DataTextField="nombre" DataValueField="id" Width="300px">
            </asp:DropDownList>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="btnInsert" runat="server" CssClass="button-aqua" 
                Text="Crear" />
            <asp:SqlDataSource ID="dsObjects" runat="server" 
                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" 
                SelectCommand="SELECT * FROM [Objeto]"></asp:SqlDataSource>
            <asp:SqlDataSource ID="dsMethods" runat="server" 
                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" 
                
                SelectCommand="SELECT id, nombre FROM Metodo AS M WHERE (NOT (id IN (SELECT func_metodo_id FROM Funcion WHERE (func_objeto_id = @objectIDddl))))">
                <SelectParameters>
                    <asp:ControlParameter ControlID="ddlObjects" Name="objectIDddl" 
                        PropertyName="SelectedValue" />
                </SelectParameters>
            </asp:SqlDataSource>

        </asp:Panel>

        <asp:GridView ID="grdFunctions" runat="server"  AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
            ForeColor="#333333" CssClass="mGrid"
            EmptyDataText="No existen Objetos creados."
            DataSourceID="dsFunctions" DataKeyNames="func_id">
            <RowStyle BackColor="White" ForeColor="White" />
            <Columns>
                <asp:BoundField DataField="func_id" HeaderText="Código" InsertVisible="False" 
                    ReadOnly="True" SortExpression="func_id" >
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="func_descripcion" HeaderText="Descripción" 
                    SortExpression="func_descripcion" />
                <asp:BoundField DataField="nombre" HeaderText="Objeto" 
                    SortExpression="nombre" />
                <asp:BoundField DataField="nombre1" HeaderText="Método" 
                    SortExpression="nombre1" />
            </Columns>
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
                CssClass="pgr" Font-Underline="False" />
            <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid" 
                BorderWidth="1px" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />            
        </asp:GridView>
    
	    <asp:SqlDataSource ID="dsFunctions" runat="server" 
            ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" 
            SelectCommand="SELECT F.func_id, F.func_descripcion, O.nombre, M.nombre FROM Funcion F INNER JOIN Objeto O ON (O.id = F.func_objeto_id) INNER JOIN Metodo M ON (M.id = F.func_metodo_id)" 
            InsertCommand="INSERT INTO Funcion(func_descripcion, func_objeto_id, func_metodo_id) VALUES (@descripcion, @objetoID, @metodoID)" >
            <InsertParameters>
                <asp:ControlParameter ControlID="txtFunction" Name="descripcion" PropertyName="Text" Type="String" />                
                <asp:ControlParameter ControlID="ddlObjects" Name="objetoID" PropertyName="SelectedValue" Type="Int64" />
                <asp:ControlParameter ControlID="ddlMethods" Name="metodoID" PropertyName="SelectedValue" Type="Int64" />
            </InsertParameters>
        </asp:SqlDataSource>

    </div>
   
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Security/Manager.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

