﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users

Partial Class Security_Functions
    Inherits TeleLoader.Web.BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then
            clearForm()
        Else
            pnlMsg.Visible = False
            pnlError.Visible = False
        End If
    End Sub

    Protected Sub btnInsert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInsert.Click

        If validateField() Then
            If dsFunctions.Insert() >= 1 Then
                pnlError.Visible = False
                pnlMsg.Visible = True
                lblMsg.Text = "Función creada correctamente"
                clearForm()

                ddlObjects.DataBind()

            Else
                pnlMsg.Visible = False
                pnlError.Visible = True
                lblError.Text = "Error creando función"
            End If
        Else
            pnlMsg.Visible = False
            pnlError.Visible = True
            lblError.Text = "Complete el formulario correctamente"
        End If

    End Sub

    Function validateField() As Boolean
        Dim retVal As Boolean

        If txtFunction.Text.Length <= 5 Then
            retVal = False
        Else
            retVal = True
        End If

        Return retVal

    End Function

    Sub clearForm()
        txtFunction.Text = ""
        txtFunction.Focus()
    End Sub

    Protected Sub ddlObjects_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlObjects.SelectedIndexChanged
        dsMethods.DataBind()
    End Sub
End Class
