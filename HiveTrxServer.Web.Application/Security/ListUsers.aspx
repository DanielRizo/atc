﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ListUsers.aspx.vb" Inherits="Security_ListUsers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Listar Usuarios
    </title>

    <script type="text/javascript" src="../js/toogle.js"></script>

    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>
    <%--<link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" />--%>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <%-- Agregado para ver contraseña --%>
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" />
    <%-- Script para ver contraseña input texbox --%>
    <script type="text/javascript">
        //esta js me permite ver la contraseña  usando un hover 
        $(function () {
            $('#show_password').hover(function show() {
                //Cambiar el atributo a texto
                $('#Password').attr('type', 'text');
                $('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
                $('#ctl00_MainContent_Password').get(0).type = 'text';
            },
                function () {
                    //Cambiar el atributo a contraseña
                    $('#Password').attr('type', 'password');
                    $('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
                    $('#ctl00_MainContent_Password').get(0).type = 'password';
                });
        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed"
            PostBackUrl="~/Security/Manager.aspx">Seguridad</asp:LinkButton>
    </li>
    <li>Listar Usuarios</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="Server">
    Listar Usuarios
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:Panel ID="pnlMsg" runat="server" Visible="False">
        <div class="albox succesbox">
            <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
            <a href="#" class="close tips" title="Cerrar">Cerrar</a>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlError" runat="server" Visible="False">
        <div class="albox errorbox">
            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
            <a href="#" class="close tips" title="Cerrar">Cerrar</a>
        </div>
    </asp:Panel>

    <div class="card">
        <div class="card-footer">
            <div class="simplebox grid960">
                <h2 align="center">Editar datos del correo Administrador</h2>

                <form action="/action_page.php">

                    <div class="form-group" style="text-align: center">
                        <div class="col-md-4" style="left: 33%">
                            <label for="email">Correo administrativo:</label>
                            <div class="input-group">
                                <asp:TextBox ID="Email" CssClass="form-control" Style="text-align: center"
                                    runat="server" TabIndex="0" MaxLength="50"
                                    ToolTip="Email" onkeydown="return (event.keyCode!=13);" type="email" placeholder="Enter email"></asp:TextBox>
                            </div>
                        </div>
                        <br />
                        <div class="col-md-4" style="left: 33%">
                            <p style="color: #c2c2c2;">Desplácese por el ojo para mostrar / ocultar la contraseña</p>
                            <label for="email">Contraseña administrativa:</label>
                            <div class="input-group">
                                <asp:TextBox ID="Password" CssClass="form-control" Style="text-align: center"
                                    runat="server" MaxLength="50"
                                    ToolTip="password" onkeydown="return (event.keyCode!=13);" type="password" placeholder="Enter password"></asp:TextBox>
                                <div class="input-group-append">
                                    <button id="show_password" class="btn btn-info" type="button">
                                        <span class="fa fa-eye icon"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="left: 40%; position: relative;">
                        <asp:Button ID="guardar" runat="server" Text="Guardar"
                            CssClass="btn btn-info" Style="width: 20%" TabIndex="0" ToolTip="Guardar cambios..." />
                    </div>
                </form>
            </div>
        </div>
    </div>
    <br />

    <p>Este módulo le permite ver el listado de usuarios del sistema Polaris:</p>

    <br />
    <div class="toggle-message" style="z-index: 590; top: 0px; left: 0px;">
        <h3 class="title">Filtro de B&uacute;squeda...     ↓ ↓ 
            <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
        <div class="hide-message" id="hide-message" style="display: none;">
            <div class="st-form-line">
                <span class="st-labeltext"><b>Nombre: </b></span>
                <asp:TextBox ID="txtNombre" CssClass="st-forminput"
                    runat="server" TabIndex="1" MaxLength="100"
                    ToolTip="Nombre de usuario" onkeydown="return (event.keyCode!=13);" Width="250px"></asp:TextBox>
            </div>
            <div class="st-form-line">
                <span class="st-labeltext"><b>Login: </b></span>
                <asp:TextBox ID="txtLogin" CssClass="st-forminput"
                    runat="server" TabIndex="2" MaxLength="50"
                    ToolTip="Login usuario" onkeydown="return (event.keyCode!=13);" Width="250px"></asp:TextBox>
            </div>
            <div class="button-box">
                <asp:Button ID="btnConsultar" runat="server" Text="Filtrar"
                    CssClass="button-aqua" TabIndex="3" ToolTip="Filtrar Usuarios..." />
            </div>
        </div>
    </div>

    <br />

    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:GridView ID="grdUsers" runat="server" AllowPaging="True"
            AllowSorting="True" AutoGenerateColumns="False" CellPadding="4"
            DataSourceID="dsListUsers" ForeColor="#333333" Width="100%"
            CssClass="mGrid" DataKeyNames="usu_login,usu_nombre"
            EmptyDataText="No existen Usuarios creados o con el filtro aplicado.">
            <RowStyle BackColor="White" ForeColor="White" />
            <Columns>
                <asp:BoundField DataField="usu_nombre" HeaderText="Nombre de Usuario"
                    SortExpression="usu_nombre" />
                <asp:BoundField DataField="usu_email" HeaderText="Correo Electrónico"
                    SortExpression="usu_email" />
                <asp:BoundField DataField="usu_login" HeaderText="Login"
                    SortExpression="usu_login" />
                <asp:BoundField DataField="perf_nombre" HeaderText="Perfil"
                    SortExpression="perf_nombre" />

                <asp:TemplateField HeaderText="Estado" SortExpression="usu_estado">
                    <ItemTemplate>
                        <asp:CheckBox ID="editChk" runat="server" Checked='<%# Bind("usu_estado")%>' Enabled="false" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>

                <asp:BoundField DataField="cli_nombre" HeaderText="Cliente"
                    SortExpression="cli_nombre" />
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False"
                            CommandName="EditUser" ImageUrl="~/img/icons/16x16/edit.png" Text="Editar"
                            ToolTip="Editar Usuario" CommandArgument='<%# grdUsers.Rows.Count %>' />
                        <asp:ImageButton ID="imgLogout" runat="server" CausesValidation="False"
                            CommandName="LogoutUser" ImageUrl="~/img/icons/16x16/lock_open.png" Text="Cerrar Sesión Web"
                            ToolTip="Cerrar Sesión Web del Usuario" CommandArgument='<%# grdUsers.Rows.Count %>' />

                        <%--    <asp:ImageButton ID="imgDeleteUser" runat="server" CausesValidation="false"
                            CommandName="DeleteUser" ImageUrl="~/img/icons/16x16/delete.png" AlternateText="Eliminar Usuario"
                            ToolTip="Elimiar Usuario" CommandArgument='<%# grdUsers.Rows.Count %>' />--%>
                    </ItemTemplate>
                </asp:TemplateField>

            </Columns>
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center"
                CssClass="pgr" Font-Underline="False" />
            <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#999999" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>

        <asp:SqlDataSource ID="dsListUsers" runat="server"
            ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"
            SelectCommand="sp_webConsultarUsuarios" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:Parameter Name="userName" Type="String" DefaultValue="-1" />
                <asp:Parameter Name="login" Type="String" DefaultValue="-1" />
            </SelectParameters>
        </asp:SqlDataSource>

    </div>

    <asp:HyperLink ID="lnkBack" runat="server"
        NavigateUrl="~/Security/Manager.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>

    <ajaxtoolkit:ToolkitScriptManager ID="scrMgrAsp" runat="server">
    </ajaxtoolkit:ToolkitScriptManager>

    <asp:Panel ID="pnlPopupLogout" runat="server" CssClass="modalPopup">
        <asp:Label ID="lblTitulo" runat="server"
            Text="Confirma que desea cerrar la sesión web?" Font-Bold="True"
            Font-Size="Medium"></asp:Label>
        <br />
        <br />
        <b>Nombre de Usuario:</b>&nbsp;&nbsp;<asp:Label ID="lblUserName" runat="server" Text="Label"></asp:Label>
        <br />
        <b>Login de Usuario:</b>&nbsp;&nbsp;<asp:Label ID="lblLogin" runat="server" Text="Label"></asp:Label>
        <br />
        <br />
        <asp:Button ID="btnLogoutUser" runat="server" Text="Aceptar" CssClass="st-button" />
        <br />

        <asp:LinkButton ID="lnkCancel" runat="server" Font-Bold="True" OnClientClick="SetZIndexUp();"
            Font-Size="Small">Cancelar</asp:LinkButton>
    </asp:Panel>

    <asp:Button ID="btnPopUp" runat="server" Style="display: none" Text="Invisible Button" />

    <ajaxtoolkit:ModalPopupExtender ID="modalPopupExt" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="lnkCancel" TargetControlID="btnPopUp" PopupControlID="pnlPopupLogout">
    </ajaxtoolkit:ModalPopupExtender>

</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" runat="Server">

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>

</asp:Content>

