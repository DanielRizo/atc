﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users

Partial Class Security_ListUsers
    Inherits TeleLoader.Web.BasePage

    Dim userObj As User



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtNombre.Text = ""
            txtLogin.Text = ""
            txtNombre.Focus()
            userObj = New User(strConnectionString, objAccessToken.UserID)
            'Leer datos BD
            userObj.getDatosCorreo()

            userObj = New User(strConnectionString, objAccessToken.UserID)

            Dim configurationSection As ConnectionStringsSection =
                System.Web.Configuration.WebConfigurationManager.GetSection("connectionStrings")
            Dim strConnString As String = configurationSection.ConnectionStrings("TeleLoaderConnectionString").ConnectionString


            Using Conn As New SqlConnection(strConnString)
                Conn.Open()
                Dim ConsultaSQL = "SELECT CAST(DecryptByPassPhrase( 'Clave',correo_clave ) as varchar(max)) as 'password'  FROM dbo.correo  "
                Password.Text = New SqlCommand(ConsultaSQL, Conn).ExecuteScalar().ToString()
            End Using

            Using Conn As New SqlConnection(strConnString)
                Conn.Open()
                Dim ConsultaSQL = "SELECT CAST(DecryptByPassPhrase( 'Clave',correo_nombre ) as varchar(max)) as 'Correo'  FROM dbo.correo  "
                Email.Text = New SqlCommand(ConsultaSQL, Conn).ExecuteScalar().ToString()
            End Using

        End If
    End Sub

    Protected Sub grdUsers_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdUsers.RowCommand
        Try

            Select Case e.CommandName

                Case "EditUser"
                    grdUsers.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    objSessionParams.strUserLogin = grdUsers.SelectedDataKey.Values.Item("usu_login")

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("EditUser.aspx", False)

                Case "LogoutUser"
                    grdUsers.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    lblUserName.Text = grdUsers.SelectedDataKey.Values.Item("usu_nombre")
                    lblLogin.Text = grdUsers.SelectedDataKey.Values.Item("usu_login")

                    grdUsers.SelectedIndex = -1

                    ltrScript.Text = "<script language='javascript'>$(function(){ SetZIndexDown(); });</script>"

                    modalPopupExt.Show()

                Case "DeleteUser"

                    grdUsers.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    objSessionParams.strUserLogin = grdUsers.SelectedDataKey.Values.Item("usu_nombre")

                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    userObj = New User(strConnectionString, grdUsers.SelectedDataKey.Values.Item("usu_id"))
                    'Delete user
                    Dim result As Integer = userObj.DeleteUser()

                    Select Case result
                        Case 1
                            'USER Deleted OK
                            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "usuario Eliminado. Datos[ Grupo ID=" & grdUsers.SelectedDataKey.Values.Item("usu_id") & " ]", "")

                            pnlMsg.Visible = True

                            lblMsg.Text = "Usuario eliminado correctamente."

                        Case Else
                            objSessionParams.strUserLogin = ""
                            'Set Data into Session
                            Session("SessionParameters") = objSessionParams

                            grdUsers.SelectedIndex = -1

                    End Select
            End Select

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub

    Protected Sub btnLogoutUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogoutUser.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "CloseSession")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        userObj = New User(strConnectionString, objAccessToken.UserID)

        'Close Session
        userObj.closeUserSession(lblLogin.Text.Trim())

        'Session Closed OK
        objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "CloseSession", "Sesión Web de Usuario Cerrada. Datos[ " & lblLogin.Text.Trim() & " ]", "")

        pnlMsg.Visible = True
        lblMsg.Text = "Sesión Web de Usuario cerrada correctamente."

        ltrScript.Text = "<script language='javascript'>$(function(){ SetZIndexUp(); });</script>"

    End Sub

    Protected Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click

        If txtNombre.Text <> "" Then
            dsListUsers.SelectParameters("userName").DefaultValue = txtNombre.Text
        Else
            dsListUsers.SelectParameters("userName").DefaultValue = "-1"
        End If

        If txtLogin.Text <> "" Then
            dsListUsers.SelectParameters("login").DefaultValue = txtLogin.Text
        Else
            dsListUsers.SelectParameters("login").DefaultValue = "-1"
        End If

        grdUsers.DataBind()

        If txtNombre.Text = "" And txtLogin.Text = "" Then
            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"
        Else
            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
        End If

    End Sub
    Protected Sub guardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles guardar.Click


        userObj = New User(strConnectionString, objAccessToken.UserID)
        userObj.getUserData(objSessionParams.strUserLogin)

        userObj = New User(strConnectionString, objAccessToken.UserID)

        'Set New Data
        userObj.EmailAdmin = Email.Text
        userObj.passwordEmail = Password.Text

        'Update Data
        If userObj.editEmail() Then
            'New Group Edited OK
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "update", "Correo Editado. Datos:  ( " & getFormDataLog() & " )", "")

            pnlMsg.Visible = True
            pnlError.Visible = False
            lblMsg.Text = "Correo editado correctamente."
        Else
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "update", "Correo Editado. Datos:  ( " & getFormDataLog() & " )", "")

            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error editando Email. Por favor valide los datos."
        End If
    End Sub
End Class
