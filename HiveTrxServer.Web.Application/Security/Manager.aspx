﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Manager.aspx.vb" Inherits="Security_Manager" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Seguridad
    </title>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Path" Runat="Server">
    <li>Seguridad</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" Runat="Server">
    Módulo de Seguridad
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>A continuación encontrará las opciones principales de este módulo:</p>
    <br />
    <div style="text-align: center;">    
        <asp:Repeater ID="repOpcion" runat="server">
            <ItemTemplate>
                <asp:LinkButton ID="lnkOpcionItem" runat="server" CssClass="dashbutton" CommandArgument='<%# Eval("opcionLnkUrl") %>' OnCommand="lnkOpcionItem_Command" CausesValidation="false">
                    <asp:Image ID="lnkImg" runat="server" ImageUrl='<%# Eval("opcionImgUrl") %>' /> <b><%#Eval("opcionLabel")%></b>
                </asp:LinkButton>
            </ItemTemplate>
        </asp:Repeater>    
    </div>
    
    <div class="clear"></div>
    <div style="height: 50px;"></div>             
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">
    
    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>
