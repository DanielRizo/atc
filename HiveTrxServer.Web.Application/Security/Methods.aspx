﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Methods.aspx.vb" Inherits="Security_Methods" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Administración de Métodos
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed" 
            PostBackUrl="~/Security/Manager.aspx">Seguridad</asp:LinkButton>
    </li>
    <li>Admin. Métodos</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Administración de Métodos
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite administrar los métodos del sistema Polaris:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
    
        <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel> 

        <asp:Panel ID="pnlError" runat="server" Visible="False">    
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlInsert" runat="server"
            CssClass="dialogbox" >
            <br />
            <b>&nbsp;Nombre de Método:&nbsp;&nbsp; </b>
            <asp:TextBox ID="txtMethod" 
                runat="server" Width="300px" MaxLength="50" CssClass="st-forminput-active" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
            &nbsp;&nbsp;
            <asp:Button ID="btnInsert" runat="server" CssClass="button-aqua" 
                Text="Crear" />
        </asp:Panel>    

        <asp:GridView ID="grdMethods" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
            CssClass="mGrid" DataKeyNames="id" DataSourceID="dsMethods" 
            EmptyDataText="No existen Métodos creados." ForeColor="#333333">
            <RowStyle BackColor="White" ForeColor="White" />
            <Columns>
                <asp:TemplateField HeaderText="Código" InsertVisible="False" 
                    SortExpression="id">
                    <ItemTemplate>
                        <asp:Label ID="lblID1" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:Label ID="lblID2" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                    </EditItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Nombre" SortExpression="nombre">
                    <ItemTemplate>
                        <asp:Label ID="lblID3" runat="server" Text='<%# Bind("nombre") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtMethodGrid" runat="server" CssClass="st-forminput-active" 
                            MaxLength="50" Text='<%# Bind("nombre") %>' onkeydown = "return (event.keyCode!=13);" Width="300"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqFieldValidator" runat="server" 
                            ControlToValidate="txtMethodGrid" 
                            ErrorMessage="Debe digitar un valor correcto."></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" 
                            CommandName="Edit" ImageUrl="~/img/icons/16x16/edit.png" Text="Editar" 
                            ToolTip="Editar Método" />                                
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:LinkButton ID="lnkButtonUpdate" runat="server" CausesValidation="True" 
                            CommandArgument="<%# grdMethods.Rows.Count %>" CommandName="Update" 
                            Text="Actualizar"></asp:LinkButton>
                        &nbsp;<asp:LinkButton ID="lnkButtonCancel" runat="server" CausesValidation="False" 
                            CommandName="Cancel" Text="Cancelar"></asp:LinkButton>
                    </EditItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" CssClass="pgr" Font-Underline="False" 
                ForeColor="Black" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid" 
                BorderWidth="1px" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>

	    <asp:SqlDataSource ID="dsMethods" runat="server" 
            ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" 
            SelectCommand="SELECT id, nombre FROM Metodo" 
            InsertCommand="INSERT INTO Metodo(nombre) VALUES (@nombreNuevo)" 
            UpdateCommand="UPDATE Metodo SET nombre = @nombre WHERE (id = @id)">
            <UpdateParameters>
                <asp:Parameter Name="nombre" />
                <asp:Parameter Name="id" />
            </UpdateParameters>
            <InsertParameters>
                <asp:ControlParameter ControlID="txtMethod" Name="nombreNuevo" PropertyName="Text" Type="String" />
            </InsertParameters>
        </asp:SqlDataSource>

    </div>
   
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Security/Manager.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

