﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users

Partial Class Security_Methods
    Inherits TeleLoader.Web.BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then
            clearForm()
        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub btnInsert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInsert.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Save")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        If validateField() Then
            If dsMethods.Insert() >= 1 Then
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Método Creado. Datos[ " & getFormDataLog() & " ]", "")
                pnlError.Visible = False
                pnlMsg.Visible = True
                lblMsg.Text = "Método creado correctamente"
                clearForm()
            Else
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Creando Método. Datos[ " & getFormDataLog() & " ]", "")
                pnlMsg.Visible = False
                pnlError.Visible = True
                lblError.Text = "Error creando método"
            End If
        Else
            pnlMsg.Visible = False
            pnlError.Visible = True
            lblError.Text = "Complete el formulario correctamente"
        End If

    End Sub

    Function validateField() As Boolean
        Dim retVal As Boolean

        If txtMethod.Text.Length <= 5 Then
            retVal = False
        Else
            retVal = True
        End If

        Return retVal

    End Function

    Sub clearForm()
        txtMethod.Text = ""
        txtMethod.Focus()
    End Sub

    Protected Sub grdMethods_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdMethods.RowCommand
        Try

            Select Case e.CommandName

                Case "Update"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Update")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    Dim strData As String = "Nuevo Nombre: " & CType(grdMethods.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("txtMethodGrid"), TextBox).Text

                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Update", "Nombre del Método Actualizado. Datos[ " & strData & " ]", "")

                    pnlError.Visible = False
                    pnlMsg.Visible = True
                    lblMsg.Text = "Nombre de Método Modificado"

                    dsMethods.Update()

            End Select

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub

End Class
