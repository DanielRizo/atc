﻿Imports System.Data
Imports System.Data.SqlClient
Imports TeleLoader.Users

Partial Class Security_Profiles
    Inherits TeleLoader.Web.BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then
            clearForm()
        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub btnInsert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInsert.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Save")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        If validateField() Then
            If dsProfiles.Insert() >= 1 Then
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Perfil Creado. Datos[ " & getFormDataLog() & " ]", "")
                pnlError.Visible = False
                pnlMsg.Visible = True
                lblMsg.Text = "Perfil creado correctamente"
                clearForm()
            Else
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Creando Perfil. Datos[ " & getFormDataLog() & " ]", "")
                pnlMsg.Visible = False
                pnlError.Visible = True
                lblError.Text = "Error creando perfil"
            End If
        Else
            pnlMsg.Visible = False
            pnlError.Visible = True
            lblError.Text = "Complete el formulario correctamente"
        End If

    End Sub

    Function validateField() As Boolean
        Dim retVal As Boolean

        If txtProfile.Text.Length <= 5 Then
            retVal = False
        Else
            retVal = True
        End If

        Return retVal

    End Function

    Sub clearForm()
        txtProfile.Text = ""
        txtProfile.Focus()
    End Sub

    Protected Sub grdProfiles_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdProfiles.RowCommand
        Try

            Select Case e.CommandName

                Case "AddWebModules"
                    grdProfiles.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    objSessionParams.intSelectedProfile = grdProfiles.SelectedDataKey.Values.Item("perf_id")
                    objSessionParams.strSelectedProfile = CType(grdProfiles.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblID3"), Label).Text

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("AddWebModulesXProfile.aspx", False)

                Case "AddFunctions"
                    grdProfiles.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    objSessionParams.intSelectedProfile = grdProfiles.SelectedDataKey.Values.Item("perf_id")
                    objSessionParams.strSelectedProfile = CType(grdProfiles.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblID3"), Label).Text

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("AddFunctionsXProfile.aspx", False)

                Case "Update"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Update")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    Dim strData As String = "Nuevo Nombre: " & CType(grdProfiles.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("txtProfileGrid"), TextBox).Text

                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Update", "Nombre del Perfil Actualizado. Datos[ " & strData & " ]", "")

                    pnlError.Visible = False
                    pnlMsg.Visible = True
                    lblMsg.Text = "Nombre de Perfil Modificado"
                    dsProfiles.Update()

            End Select

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub

End Class
