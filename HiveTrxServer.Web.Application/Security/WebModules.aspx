﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="WebModules.aspx.vb" Inherits="Security_WebModules" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Módulos Web
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   

	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script>     
        
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed" 
            PostBackUrl="~/Security/Manager.aspx">Seguridad</asp:LinkButton>
    </li>
    <li>Módulos Web</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Módulos Web
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite agregar Módulos Web(Menú Izquierdo) al sistema:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>   
            
        <asp:Panel ID="pnlError" runat="server" Visible="False">    
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>  

        <div class="toggle-message" style="z-index: 590;">
            <h3 class="title">Nuevo Módulo...
                <img src="../img/icons/mini/arrow-down.png" alt="icon" class="d-icon" /></h3>
            <div class="hide-message" id="hide-message" style="display: none;">
        
                <div class="st-form-line">	
                    <span class="st-labeltext">Título del Módulo:</span>	
                    <asp:TextBox ID="txtModuleName" CssClass="st-forminput" style="width:510px" 
                        runat="server" TabIndex="1" MaxLength="50" 
                        ToolTip="Nombre del Módulo." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                    <div class="clear"></div>
                </div>        
        
                <div class="st-form-line">	
                    <span class="st-labeltext">URL del Módulo:</span>	
                    <asp:DropDownList ID="ddlURLModules" runat="server" class="uniform" DataSourceID="dsURLModules" 
                        DataTextField="nombre" DataValueField="nombre" Width="300px">
                    </asp:DropDownList>

                    <asp:SqlDataSource ID="dsURLModules" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>" 
                        SelectCommand="SELECT id, nombre FROM Objeto WHERE nombre LIKE '%aspx%' ORDER BY 1"></asp:SqlDataSource>                    
                    
                    <div class="clear"></div>
                </div>

                <div class="st-form-line">	
                    <span class="st-labeltext">Imagen a Mostrar:</span>	
                    <asp:DropDownList ID="ddlUrlImage" class="uniform" runat="server" 
                        Width="200px" 
                        ToolTip="Seleccione la imagen a mostrar para el módulo." TabIndex="3" 
                        AutoPostBack="True">
                    </asp:DropDownList>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Image ID="imgIcon" runat="server" />
                    <div class="clear"></div>
                </div>
                        
                <div class="button-box">
                    <asp:Button ID="btnAddWebModule" runat="server" Text="Adicionar Módulo Web" 
                        CssClass="button-aqua" TabIndex="4" OnClientClick="return validateAddWebModule();" />
                    <asp:Button ID="btnFinalize" runat="server" Text="Finalizar" 
                        CssClass="st-button" TabIndex="5" Visible="false"  />
                </div>            
            </div>
        </div>

    </div>
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
	    <div class="titleh">
    	    <h3>Módulos Web Creados</h3>
        </div>
        <div class="body">    
            <br />            
            <asp:GridView ID="grdWebModules" runat="server" AllowPaging="True" 
                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                DataSourceID="dsWebModules" ForeColor="#333333"
                CssClass="mGridCenter" DataKeyNames="opc_id" 
                EmptyDataText="No existen Módulos Web creados." 
                HorizontalAlign="Left" Width="1060px">
                <RowStyle BackColor="White" ForeColor="White" />
                <Columns>
                    <asp:TemplateField HeaderText="Código" InsertVisible="False" 
                        SortExpression="opc_id">
                        <ItemTemplate>
                            <asp:Label ID="lblID1" runat="server" Text='<%# Bind("opc_id") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Título" SortExpression="opc_titulo">
                        <ItemTemplate>
                            <asp:Label ID="lblID2" runat="server" Text='<%# Bind("opc_titulo") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="URL del Módulo" SortExpression="opc_url">
                        <ItemTemplate>
                            <asp:Label ID="lblID3" runat="server" Text='<%# Bind("opc_url") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>                    
                    <asp:TemplateField HeaderText="URL Imagen" SortExpression="opc_img_url">
                        <ItemTemplate>
                            <asp:Image ID="imgUrlIcon" runat="server" ImageUrl='<%# Bind("opc_img_url") %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Orden" SortExpression="opc_orden">
                        <ItemTemplate>
                            <asp:Label ID="lblID5" runat="server" Text='<%# Bind("opc_orden") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Nro. Opciones" SortExpression="TotalOpciones">
                        <ItemTemplate>
                            <asp:Label ID="lblID6" runat="server" Text='<%# Bind("TotalOpciones") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>                        
                            <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False" 
                                CommandName="Delete" ImageUrl="~/img/icons/16x16/delete.png" Text="Eliminar" 
                                ToolTip="Eliminar Módulo Web" CommandArgument='<%# grdWebModules.Rows.Count %>' />                                
                            <asp:ImageButton ID="imgAddOptions" runat="server" CausesValidation="False" 
                                CommandName="AddOptions" ImageUrl="~/img/icons/16x16/add_options.png" Text="Adicionar Opciones" 
                                ToolTip="Adicionar Opciones Web" CommandArgument='<%# grdWebModules.Rows.Count %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    
                </Columns>
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
                    CssClass="pgr" Font-Underline="False" />
                <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid" 
                    BorderWidth="1px" />
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>            
            <br />
	        <asp:SqlDataSource ID="dsWebModules" runat="server" 
                ConnectionString="<%$ ConnectionStrings:TeleLoaderConnectionString %>"                 
                SelectCommand="SELECT O.opc_id, O.opc_titulo, O.opc_url, '../' + O.opc_img_url AS opc_img_url, O.opc_orden, (SELECT COUNT(*) FROM Opcion WHERE opc_tipo_opcion_id = 2 AND opc_opcion_padre_id = O.opc_id) AS TotalOpciones FROM Opcion O WHERE O.opc_tipo_opcion_id = 1 ORDER BY opc_orden" 
                InsertCommand="sp_webCrearOpcion" InsertCommandType="StoredProcedure"
                DeleteCommand="DELETE FROM Opcion WHERE opc_id = @opc_id">
                <InsertParameters>
                    <asp:Parameter Name="optionTitle" Type="String" />
                    <asp:Parameter Name="optionURL" Type="String" />
                    <asp:Parameter Name="optionIMGURL" Type="String" />
                    <asp:Parameter Name="optionType" Type="Int32" />
                    <asp:Parameter Name="optionParentID" Type="Int32" />
                </InsertParameters>
                <DeleteParameters>
                    <asp:Parameter Name="opc_id" Type="String" />
                </DeleteParameters>
            </asp:SqlDataSource> 
        </div>
    </div>
    
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Security/Manager.aspx" onClick="ShowProgressWindow();"><img src="../img/previous.png" width="12px" height="12px" alt="Atrás"/> Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">
    
    <!-- Validator -->
    <script src="../js/ValidatorSecurity.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

