﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class Security_WebModules
    Inherits TeleLoader.Web.BasePage

    Public ReadOnly IMG_PATH As String = "img/icons/sidemenu/"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlError.Visible = False
        pnlMsg.Visible = False

        If Not IsPostBack Then

            txtModuleName.Focus()

            fillDropDownListImages()

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Private Sub fillDropDownListImages()
        Dim DataDirectory As String = "~/" & IMG_PATH

        Try
            Dim files() As FileInfo = New DirectoryInfo(Server.MapPath(DataDirectory)).GetFiles

            ddlUrlImage.DataSource = files
            ddlUrlImage.DataTextField = "Name"
            ddlUrlImage.DataValueField = "Name"
            ddlUrlImage.DataBind()
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub

    Protected Sub grdWebModules_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdWebModules.RowCommand
        Try

            Select Case e.CommandName

                Case "AddOptions"
                    grdWebModules.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    objSessionParams.intSelectedModule = grdWebModules.SelectedDataKey.Values.Item("opc_id")
                    objSessionParams.strSelectedModule = CType(grdWebModules.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblID2"), Label).Text

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("AddOptionsXWebModule.aspx", False)

                Case "Delete"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try


                    Dim OptionsCounter As Integer = Integer.Parse(CType(grdWebModules.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblID6"), Label).Text)

                    If OptionsCounter = 0 Then
                        grdWebModules.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                        Dim strData As String = "Opción ID.: " & grdWebModules.SelectedDataKey.Values.Item("opc_id") _
                                    & ", Título: " & CType(grdWebModules.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblID2"), Label).Text _
                                    & ", URL: " & CType(grdWebModules.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblID3"), Label).Text _
                                    & ", Imagen URL: " & CType(grdWebModules.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("imgUrlIcon"), Image).ImageUrl _
                                    & ", Orden: " & CType(grdWebModules.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblID5"), Label).Text

                        objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Módulo Web Eliminado. Datos[ " & strData & " ]", "")

                        pnlError.Visible = False
                        pnlMsg.Visible = True
                        lblMsg.Text = "Módulo Web eliminado"
                    Else
                        pnlError.Visible = True
                        pnlMsg.Visible = False
                        lblError.Text = "No puede eliminar un módulo que contenga opciones."
                    End If

                    grdWebModules.SelectedIndex = -1
            End Select

            'Set Invisible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub

    Protected Sub btnAddWebModule_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddWebModule.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Save")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        Try
            'Set Parameters
            dsWebModules.InsertParameters.Clear()
            dsWebModules.InsertParameters.Add("optionTitle", TypeCode.String, txtModuleName.Text)
            dsWebModules.InsertParameters.Add("optionURL", TypeCode.String, ddlURLModules.SelectedItem.Text)
            dsWebModules.InsertParameters.Add("optionIMGURL", TypeCode.String, IMG_PATH + ddlUrlImage.SelectedValue)
            dsWebModules.InsertParameters.Add("optionType", TypeCode.Int32, "1") 'Fixed Data, Module Type
            dsWebModules.InsertParameters.Add("optionParentID", TypeCode.String, "")

            If dsWebModules.Insert() >= 1 Then
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Módulo Web Creado. Datos[ " & getFormDataLog() & " ]", "")
                pnlError.Visible = False
                pnlMsg.Visible = True
                lblMsg.Text = "Módulo Web creado correctamente"
                deactivateForm()
                grdWebModules.DataBind()
                dsWebModules.DataBind()
            Else
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Creando Módulo Web. Datos[ " & getFormDataLog() & " ]", "")
                pnlMsg.Visible = False
                pnlError.Visible = True
                lblError.Text = "Error creando Módulo Web"
            End If

            'Set Visible Toggle Panel
            ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub

    Private Sub deactivateForm()
        ddlUrlImage.Enabled = False
        txtModuleName.Enabled = False
        ddlURLModules.Enabled = False
        btnAddWebModule.Enabled = False
        btnFinalize.Visible = True
        btnFinalize.Enabled = True
    End Sub

    Private Sub activateForm()
        ddlUrlImage.Enabled = True
        txtModuleName.Enabled = True
        txtModuleName.Text = ""
        ddlURLModules.Enabled = True
        btnAddWebModule.Enabled = True
        btnFinalize.Visible = False
        btnFinalize.Enabled = False
    End Sub

    Protected Sub btnFinalize_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinalize.Click
        activateForm()
        txtModuleName.Focus()

        'Set Invisible Toggle Panel
        ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'none'); });</script>"

    End Sub

    Protected Sub dsWebModules_Deleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceCommandEventArgs) Handles dsWebModules.Deleting
        If pnlError.Visible = True Then
            e.Cancel = True
        End If
    End Sub

    Protected Sub ddlUrlImage_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUrlImage.DataBound
        imgIcon.ImageUrl = "../" & IMG_PATH & ddlUrlImage.SelectedValue
    End Sub

    Protected Sub ddlUrlImage_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUrlImage.SelectedIndexChanged
        imgIcon.ImageUrl = "../" & IMG_PATH & ddlUrlImage.SelectedValue
        'Set Visible Toggle Panel
        ltrScript.Text = "<script language='javascript' type='text/javascript'> $(document).ready(function () { $('#hide-message').css('z-index', 750); $('#hide-message').css('display', 'block'); });</script>"
    End Sub
End Class
