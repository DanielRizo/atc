﻿window.onload = function () {
    grafica1();
    grafica2();
    var datos1 = [];
    var datos2 = [];


    datos1 = this.obtenerDatos();
    datos2 = this.obtenerDatos2();


    this.grafica3(datos1);
    this.grafica4(datos2);
    grafica5();
    grafica6();
    grafica7();
};
function obtenerDatos() {
    var datos = [];
    var JsonRsp;

    $.ajax({
        type: "POST",
        url: "../Group/wsadd.asmx/" + "getDatosDescargasApk",
        data: "",
        contentType: "application/Json; charset=utf-8",
        dataType: 'Json',


        success: function (r) {

            if (r.d != "") {
                JsonRsp = JSON.parse(r.d);


                for (i in JsonRsp.registroTerminal) {

                    datos.push({
                        label: JsonRsp.registroTerminal[i].por_serialTerminal + '\n' + JsonRsp.registroTerminal[i].por_nombre_aplicacion,
                        y: parseInt(JsonRsp.registroTerminal[i].por_porcentaje_descarga),
                        color: "#FF6000"
                    });

                }
            }


        },
        error: function (r) {

            console.log("Error: " + r);
        },
        failure: function (r) {
            console.log("Falla: " + r);
        }


    });


    return (datos);

}
function obtenerDatos2() {
    var datos1 = [];
    var JsonRsp;

    $.ajax({
        type: "POST",
        url: "../Group/wsadd.asmx/" + "getDatosDescargasStis",
        data: "",
        contentType: "application/Json; charset=utf-8",
        dataType: 'Json',


        success: function (r) {

            if (r.d != "") {
                JsonRsp = JSON.parse(r.d);


                for (i in JsonRsp.registroTerminal) {

                    datos1.push({
                        label: JsonRsp.registroTerminal[i].TERMINAL_ID + '\n' + JsonRsp.registroTerminal[i].ARCHIVO_DESCARGA,
                        y: parseInt(JsonRsp.registroTerminal[i].PORCENTAJE),
                        color: "#FF6000"
                    });

                }
            }


        },
        error: function (r) {

            console.log("Error: " + r);
        },
        failure: function (r) {
            console.log("Falla: " + r);
        }


    });


    return (datos1);

}

grafica1 = function () {
    var JsonRsp;
    var data1 = [];
    var data2 = [];

    $.ajax({
        type: "POST",
        url: "../Group/wsadd.asmx/" + "getDatosDescargas",
        data: "",
        contentType: "application/Json; charset=utf-8",
        dataType: 'Json',


        success: function (r) {


            JsonRsp = JSON.parse(r.d);

            for (i in JsonRsp.registroTerminal) {
                if (JsonRsp.registroTerminal[i].tipo == "1") {
                    data1.push({
                        y: parseInt(JsonRsp.registroTerminal[i].tipo),
                        label: JsonRsp.registroTerminal[i].fecha
                    });
                } else {
                    data2.push({
                        y: parseInt(JsonRsp.registroTerminal[i].tipo),
                        label: JsonRsp.registroTerminal[i].fecha
                    })
                }
            }

            var chart = new CanvasJS.Chart("chartContainer", {
                animationEnabled: true, exportEnabled: true, theme: "", exportFileName: "Apk por Grupo Desplegado",
                title: {
                    text: "10 ultimos Grupos "
                },
                axisY: {
                    title: "cantidad"
                },
                legend: {
                    cursor: "pointer",
                    itemclick: toggleDataSeries
                },
                toolTip: {
                    shared: true,
                    content: toolTipFormatter
                },
                data: [{
                    type: "bar",
                    showInLegend: true,
                   
                    color: "#D0A00C",
                    dataPoints: data1
                },
                {
                    type: "bar",
                    showInLegend: true,
                   
                    color: "#08C8C1",
                    dataPoints: data2
                }]
            });
            chart.render();

            function toolTipFormatter(e) {
                var str = "";
                var total = 0;
                var str3;
                var str2;
                for (var i = 0; i < e.entries.length; i++) {
                    var str1 = "<span style= \"color:" + e.entries[i].dataSeries.color + "\">" + e.entries[i].dataSeries.name + "</span>: <strong>" + e.entries[i].dataPoint.y + "</strong> <br/>";
                    total = e.entries[i].dataPoint.y + total;
                    str = str.concat(str1);
                }
                str2 = "<strong>" + e.entries[0].dataPoint.label + "</strong> <br/>";
                str3 = "<span style = \"color:Tomato\">Total: </span><strong>" + total + "</strong><br/>";
                return (str2.concat(str)).concat(str3);
            }

            function toggleDataSeries(e) {
                if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                    e.dataSeries.visible = false;
                }
                else {
                    e.dataSeries.visible = true;
                }
                chart.render();
            }


        },
        error: function (r) {


        },
        failure: function (r) {

        }
    });
};
grafica2 = function () {
    var JsonRsp;
    var data1 = [];
    var data2 = [];

    $.ajax({
        type: "POST",
        url: "../Group/wsadd.asmx/" + "getDatosTransacciones",
        data: "",
        contentType: "application/Json; charset=utf-8",
        dataType: 'Json',


        success: function (r) {


            JsonRsp = JSON.parse(r.d);

            for (i in JsonRsp.registroTerminal) {

                data1.push({
                    y: parseInt(JsonRsp.registroTerminal[i].valor),
                    name: JsonRsp.registroTerminal[i].tipo
                });

            }
            var chart2 = new CanvasJS.Chart("chartContainer2", {
                theme: "light2",
                exportFileName: "Transacciones Pos",
                exportEnabled: true,
                animationEnabled: true,
                title: {
                    text: "Transacciones POS"
                },
                legend: {
                    cursor: "pointer"

                },
                data: [{
                    type: "doughnut",
                    innerRadius: 90,
                    showInLegend: true,
                    toolTipContent: "<b>{name}</b>: ${y} (#percent%)",
                    indexLabel: "#percent%",
                    dataPoints: data1
                }]
            });
            chart2.render();

            function explodePie(e) {
                if (typeof (e.dataSeries.dataPoints[e.dataPointIndex].exploded) === "undefined" || !e.dataSeries.dataPoints[e.dataPointIndex].exploded) {
                    e.dataSeries.dataPoints[e.dataPointIndex].exploded = true;
                } else {
                    e.dataSeries.dataPoints[e.dataPointIndex].exploded = false;
                }
                e.chart2.render();
            }
        },
        error: function (r) {


        },
        failure: function (r) {

        }
    });
};

function grafica3(e) {


    var chart1 = new CanvasJS.Chart("chartContainer3", {
        theme: "light2",
        title: {
            text: "Descarga Grupo: "
        },
        axisY: {
            title: "",
            suffix: " %"
        },
        axisX: {
            labelFontSize: 15,
            reversed: true
        },
        data: [{
            type: "bar",
            yValueFormatString: "#,### ",
            indexLabel: "{y}",
            dataPoints: e
        }]
    });

    function updateChart() {


        var JsonRsp;
        var data = [];
        var col;
        var idGrupo = $("#ctl00_MainContent_lblGrupo").text();
        $.ajax({
            type: "POST",
            url: "../Group/wsadd.asmx/" + "getDatosDescargasApk",
            data: "",
            contentType: "application/Json; charset=utf-8",
            dataType: 'Json',


            success: function (r) {

                if (r.d != "") {


                    JsonRsp = JSON.parse(r.d);


                    for (i in JsonRsp.registroTerminal) {

                        if (parseInt(JsonRsp.registroTerminal[i].por_porcentaje_descarga) == 100) {
                            col = "#088A08";
                        } else {
                            col = "#FF2500";
                        }

                        data.push({
                            label: JsonRsp.registroTerminal[i].por_serialTerminal + '\n' + JsonRsp.registroTerminal[i].por_nombre_aplicacion,
                            y: parseInt(JsonRsp.registroTerminal[i].por_porcentaje_descarga),
                            color: col
                        });

                    }

                    chart1.options.data[0].dataPoints = data;
                    chart1.render();

                }

            },
            error: function (r) {

                console.log("Error: " + r);
            },
            failure: function (r) {
                console.log("Falla: " + r);
            }


        });
    };

    updateChart();

    setInterval(function () { updateChart() }, 500);
}

function grafica4(e) {


    var chart1 = new CanvasJS.Chart("chartContainer4", {
        title: {
            text: "Inicialización de parametros"
        },
        axisY: {
            title: "Porcentaje",
            suffix: " %"
        },
        axisX: {
            labelFontSize: 15,
            reversed: true
        },
        data: [{
            type: "bar",
            yValueFormatString: "#,### ",
            indexLabel: "{y}",
            dataPoints: e
        }]
    });

    function updateChart() {


        var JsonRsp;
        var data = [];
        var col;
        var idGrupo = $("#ctl00_MainContent_lblGrupo").text();
        $.ajax({
            type: "POST",
            url: "../Group/wsadd.asmx/" + "getDatosDescargasStis",
            data: "",
            contentType: "application/Json; charset=utf-8",
            dataType: 'Json',


            success: function (r) {

                if (r.d != "") {


                    JsonRsp = JSON.parse(r.d);


                    for (i in JsonRsp.registroTerminal) {

                        if (parseInt(JsonRsp.registroTerminal[i].PORCENTAJE) == 100) {
                            col = "#2E86C1";
                        } else {
                            col = "#F5B041";
                        }

                        data.push({
                            label: JsonRsp.registroTerminal[i].TERMINAL_ID + '\n' + JsonRsp.registroTerminal[i].ARCHIVO_DESCARGA,
                            y: parseInt(JsonRsp.registroTerminal[i].PORCENTAJE),
                            color: col
                        });

                    }

                    chart1.options.data[0].dataPoints = data;
                    chart1.render();

                }

            },
            error: function (r) {

                console.log("Error: " + r);
            },
            failure: function (r) {
                console.log("Falla: " + r);
            }


        });
    }

    updateChart();

    setInterval(function () { updateChart() }, 500);


    grafica5 = function () {

        var JsonRsp;
        var data1 = [];
        var data2 = [];

        $.ajax({
            type: "POST",
            url: "../Group/wsadd.asmx/" + "getDatosTerminalesActualizadas",
            data: "",
            contentType: "application/Json; charset=utf-8",
            dataType: 'Json',


            success: function (r) {


                JsonRsp = JSON.parse(r.d);

                for (i in JsonRsp.registroTerminal) {

                    data1.push({
                        y: parseInt(JsonRsp.registroTerminal[i].ter_totales),
                        label: JsonRsp.registroTerminal[i].terminal_tipo
                    });

                }

                var chart8 = new CanvasJS.Chart("chartContainer5", {
                    animationEnabled: true,
                    theme: "light2", exportEnabled: false, exportFileName: "Terminales Actualizadas",// "light1", "light2", "light2", "dark2"
                    title: {
                        text: "Cantidad de terminales Actualizadas"
                    },
                    axisY: {
                        title: "Cantidad"
                    },
                    data: [{
                        type: "column",
                        showInLegend: true,
                        legendMarkerColor: "grey",
                        dataPoints: data1
                    }]
                });
                chart8.render();
                function explodePie(e) {
                    if (typeof (e.dataSeries.dataPoints[e.dataPointIndex].exploded) === "undefined" || !e.dataSeries.dataPoints[e.dataPointIndex].exploded) {
                        e.dataSeries.dataPoints[e.dataPointIndex].exploded = true;
                    } else {
                        e.dataSeries.dataPoints[e.dataPointIndex].exploded = false;
                    }
                    e.chart2.render();
                }
            },
            error: function (r) {


            },
            failure: function (r) {

            }
        });
    };

    grafica6 = function () {

        var JsonRsp;
        var data1 = [];
        var data2 = [];

        $.ajax({
            type: "POST",
            url: "../Group/wsadd.asmx/" + "getDatosTerminalesActualizadasTotal",
            data: "",
            contentType: "application/Json; charset=utf-8",
            dataType: 'Json',


            success: function (r) {


                JsonRsp = JSON.parse(r.d);

                for (i in JsonRsp.registroTerminal) {

                    data1.push({
                        y: parseInt(JsonRsp.registroTerminal[i].VALOR),
                        name: JsonRsp.registroTerminal[i].TIPO
                    });

                }

                var chart = new CanvasJS.Chart("chartContainer7", {
                    theme: "light2",
                    exportFileName: "Doughnut Chart",
                    exportEnabled: true,
                    animationEnabled: true,
                    title: {
                        text: "Consultas y descargas MDM"
                    },
                    legend: {
                        cursor: "pointer",
                        itemclick: explodePie
                    },
                    data: [{
                        type: "doughnut",
                        innerRadius: 90,
                        showInLegend: true,
                        toolTipContent: "<b>{name}</b>: ${y} (#percent%)",
                        indexLabel: "#percent%",
                        dataPoints: data1
                    }]
                });
                chart.render();
                function explodePie(e) {
                    if (typeof (e.dataSeries.dataPoints[e.dataPointIndex].exploded) === "undefined" || !e.dataSeries.dataPoints[e.dataPointIndex].exploded) {
                        e.dataSeries.dataPoints[e.dataPointIndex].exploded = true;
                    } else {
                        e.dataSeries.dataPoints[e.dataPointIndex].exploded = false;
                    }
                    e.chart2.render();
                }
            },
            error: function (r) {


            },
            failure: function (r) {

            }
        });
    };
    grafica7 = function () {

        var JsonRsp;
        var data3 = [];
        var data2 = [];

        $.ajax({
            type: "POST",
            url: "../Group/wsadd.asmx/" + "getDatosTerminalesFrecuenciaSeñal",
            data: "",
            contentType: "application/Json; charset=utf-8",
            dataType: 'Json',


            success: function (r) {


                JsonRsp = JSON.parse(r.d);

                for (i in JsonRsp.registroTerminal) {

                    data3.push({
                        y: parseInt(JsonRsp.registroTerminal[i].VALOR),
                        label: JsonRsp.registroTerminal[i].TIPO
                    });

                }


                var chart10 = new CanvasJS.Chart("chartContainer8", {
                    animationEnabled: true, exportEnabled: true,
                    theme: "light2", // "light1", "light2", "light2", "dark2"
                    title: {
                        text: "Señal y bateria"
                    },
                    subtitles: [{
                        text: "Promedio",
                        fontSize: 16
                    }],
                    axisY: {
                        prefix: "$",
                        scaleBreaks: {
                            customBreaks: [{
                                startValue: 50,
                                endValue: 30
                            }]
                        }
                    },
                    data: [{
                        type: "column",
                        yValueFormatString: "$#,##0.00",
                        dataPoints: data3
                    }]
                });
                chart10.render();
                function explodePie(e) {
                    if (typeof (e.dataSeries.dataPoints[e.dataPointIndex].exploded) === "undefined" || !e.dataSeries.dataPoints[e.dataPointIndex].exploded) {
                        e.dataSeries.dataPoints[e.dataPointIndex].exploded = true;
                    } else {
                        e.dataSeries.dataPoints[e.dataPointIndex].exploded = false;
                    }
                    e.chart2.render();
                }
            },
            error: function (r) {


            },
            failure: function (r) {

            }
        });
    };
};
