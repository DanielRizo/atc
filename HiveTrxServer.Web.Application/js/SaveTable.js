﻿var cont = 0;
var tid;
var ownerName;
var merchantName;
var shopName;
var detailAddres;
var shopContact;
var shopTelNo;
var officeContact;
var officeTelNo;
var status;
var msg = "Debe llenar todos los campos";
var $Texth;
var $selTh;
var row = 1;
var date;
var ColmName = "";
function gup(name, url) {
    if (!url) url = location.href;
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(url);
    return results === null ? null : results[1];
}
function RecorrerTable(idTable, Metodo) {
    if (Metodo === "AddTerminalStis") {

        $Texth = $(idTable).find("tr").eq(row).find("td").eq(0);
        tid = $("input[name*='ctl00$MainContent$txtTid']").val();
        ownerName = $("input[name*='ctl00$MainContent$txtOwnerName']").val();
        merchantName = $("input[name*='ctl00$MainContent$txtMerchantName']").val();
        shopName = $("input[name*='ctl00$MainContent$txtShopName']").val();
        detailAddres = $("input[name*='ctl00$MainContent$txtDetailAddres']").val();
        shopContact = $("input[name*='ctl00$MainContent$txtShopContact']").val();
        shopTelNo = $("input[name*='ctl00$MainContent$txtShopTelNo']").val();
        officeContact = $("input[name*='ctl00$MainContent$txtOfficeContact']").val();
        officeTelNo = $("input[name*='ctl00$MainContent$txtOfficeTelNo']").val();
        status = document.getElementById('ctl00_MainContent_ddlOpc').options[document.getElementById('ctl00_MainContent_ddlOpc').selectedIndex].text;
        if (!(tid === "" || ownerName === "" || merchantName === "" || shopName === "" || detailAddres === "" || shopContact === "" || shopTelNo === "" || officeContact === "" || officeTelNo === "" || status === "")) {
            var registro = $.trim($Texth.text())
            if (registro.length > 0) {
                llenar_table(idTable, Metodo);
            }
            else if (row > 1) {

                //document.getElementById('ctl00_MainContent_succes2').style.display = "inline";
                $("#succes2").attr('style', 'display : inline');
            }

        }
        else {
            alertEmptyField(msg, true);

            return;
        }


    }
    else if (Metodo === "createNewAcquirer" || Metodo === "createNewIssuer" || Metodo === "createNewCardRange" || Metodo === "createNewEmvApplication" || Metodo === "createNewEmvKey" || Metodo === "createNewEmvExtraApplication") {

        $Texth = $(idTable).find("tr").eq(row).find("td").eq(0);
        tid = $("input[name*='ctl00$MainContent$txtCode']").val();
        if (Metodo === "createNewEmvKey" || Metodo === "createNewEmvApplication") {
            ownerName = $("input[name*='ctl00$MainContent$txtCode']").val();

        }
        else {
            ownerName = document.getElementById('ctl00_MainContent_ddlOpc').options[document.getElementById('ctl00_MainContent_ddlOpc').selectedIndex].text;

        }
        merchantName = document.getElementById('recNumber').innerText;

        if (ownerName === "" || date === "") {
            msg += "- Display Name ";
            msg += "- Revoke Date ";

            alertEmptyField(msg, true);

            return;
        }
        else {
            var registro = $.trim($Texth.text())
            if (registro.length > 0) {
                llenar_table(idTable, Metodo);
            }
            else if (row > 1) {

                //document.getElementById('ctl00_MainContent_succes2').style.display = "inline";
                $("#succes2").attr('style', 'display : inline');
            }
        }

    }

    else if (Metodo === "addAcquirerTbl") {

        console.log("entro..............");
        
        tid = $("#txtCode").val();
        ownerName = $("#txtName").val();
        merchantName = $("#txtNii").val();

        var msg;
        if (tid === "" || ownerName === "" || merchantName === "") {
            msg = "Debe llenar los campos";
            msg += "- Code ";
            msg += "- Display Name ";
            msg += "- NII ";
            alertEmptyField(msg, true);

            return;

        }
        else if (tid.length != 5) {
            msg = "Code solo puede tener 5 caracteres";
            alertEmptyField(msg, true);

            return;
        }
        $Texth = $(idTable).find("tr").eq(row).find("td").eq(0);

        var registro = $.trim($Texth.text())
        if (registro.length > 0) {
            console.log("va a llenar tabla");
            llenar_table(idTable, Metodo);
        }
        else if (row > 1) {

            //document.getElementById('ctl00_MainContent_succes2').style.display = "inline";
            $("#succes2").attr('style', 'display : inline');
        }

    }
    else if (Metodo === "addIssuerTbl") {

        tid = $("#txtCode").val();
        ownerName = $("#txtName").val();

        var msg = "Debe llenar los campos";
        if (tid === "" || ownerName === "") {
            msg += "- Code ";
            msg += "- Display Name ";
            alertEmptyField(msg, true);

            return;
        }
        else if (tid.length != 5) {
            msg = "Code solo puede tener 5 caracteres";
            alertEmptyField(msg, true);

            return;
        }
        $Texth = $(idTable).find("tr").eq(row).find("td").eq(0);

        var registro = $.trim($Texth.text())
        if (registro.length > 0) {
            llenar_table(idTable, Metodo);
        }
        else if (row > 1) {

            //document.getElementById('ctl00_MainContent_succes2').style.display = "inline";
            $("#succes2").attr('style', 'display : inline');
        }

    }
    else if (Metodo === "addCardRangeTbl") {

        tid = $("#txtCode").val();
        ownerName = $("#txtName").val();
        merchantName = $("#txtDescription").val();


        var msg = "Debe llenar los campos";

        if (tid === "" || ownerName === "" || merchantName === "") {
            msg += "- Code ";
            msg += "- Display Name ";
            msg += "- Description ";
            alertEmptyField(msg, true);

            return;
        }
        else if (tid.length != 5) {
            msg = "Code solo puede tener 5 caracteres";
            alertEmptyField(msg, true);

            return;
        }

        $Texth = $(idTable).find("tr").eq(row).find("td").eq(0);

        var registro = $.trim($Texth.text())
        if (registro.length > 0) {
            llenar_table(idTable, Metodo);
        }
        else if (row > 1) {

            //document.getElementById('ctl00_MainContent_succes2').style.display = "inline";
            $("#succes2").attr('style', 'display : inline');
        }

    }

    else if (Metodo === "AddEmvApplicationTbl") {

        ownerName = $("#txtName").val();

        var msg = "Debe llenar los campos";
        if (ownerName === "") {
            msg += "- Display Name ";
            alertEmptyField(msg, true);

            return;
        }

        $Texth = $(idTable).find("tr").eq(row).find("td").eq(0);

        var registro = $.trim($Texth.text())
        if (registro.length > 0) {
            llenar_table(idTable, Metodo);
        }
        else if (row > 1) {

            //document.getElementById('ctl00_MainContent_succes2').style.display = "inline";
            $("#succes2").attr('style', 'display : inline');
        }

    }


    else if (Metodo === "AddEmvKeyTbl") {

        merchantName = $("#txtName").val();
        ownerName = $("#txtDate").val();

        var msg = "Debe llenar los campos";
        if (merchantName === "" || ownerName === "") {
            msg += "- Display Name ";
            msg += "- Revoke Date ";

            alertEmptyField(msg, true);

            return;
        }
        $Texth = $(idTable).find("tr").eq(row).find("td").eq(0);

        var registro = $.trim($Texth.text())
        if (registro.length > 0) {
            llenar_table(idTable, Metodo);
        }
        else if (row > 1) {

            //document.getElementById('ctl00_MainContent_succes2').style.display = "inline";
            $("#succes2").attr('style', 'display : inline');
        }


    }

    else if (Metodo === "addHostConfigurationTbl") {


        tid = $("#txtCode").val();
        ownerName = $("#txtName").val();
        merchantName = $("#txtDescription").val();

        var msg = "Debe llenar los campos";

        if (tid === "" || ownerName === "" || merchantName === "") {
            msg += "- Code ";
            msg += "- Display Name ";
            msg += "- Description ";
            alertEmptyField(msg, true);

            return;
        }

        $Texth = $(idTable).find("tr").eq(row).find("td").eq(0);

        var registro = $.trim($Texth.text())
        if (registro.length > 0) {
            llenar_table(idTable, Metodo);
        }
        else if (row > 1) {

            //document.getElementById('ctl00_MainContent_succes2').style.display = "inline";
            $("#succes2").attr('style', 'display : inline');
        }
    }
    else if (Metodo === "addPromptConfigurationTbl") {


        tid = $("#txtCode").val();
        ownerName = $("#txtName").val();
        merchantName = $("#txtDescription").val();

        var msg = "Debe llenar los campos";

        if (tid === "" || ownerName === "" || merchantName === "") {
            msg += "- Code ";
            msg += "- Display Name ";
            msg += "- Description ";
            alertEmptyField(msg, true);

            return;

        }

        $Texth = $(idTable).find("tr").eq(row).find("td").eq(0);

        var registro = $.trim($Texth.text())
        if (registro.length > 0) {
            llenar_table(idTable, Metodo);
        }
        else if (row > 1) {

            //document.getElementById('ctl00_MainContent_succes2').style.display = "inline";
            $("#succes2").attr('style', 'display : inline');
        }
    }
    else if (Metodo === "addPagoElectronicoConfigurationTbl") {


        tid = $("#txtCode").val();
        ownerName = $("#txtName").val();
        merchantName = $("#txtDescription").val();

        var msg = "Debe llenar los campos";

        if (tid === "" || ownerName === "" || merchantName === "") {
            msg += "- Code ";
            msg += "- Display Name ";
            msg += "- Description ";
            alertEmptyField(msg, true);

            return;

        }

        $Texth = $(idTable).find("tr").eq(row).find("td").eq(0);

        var registro = $.trim($Texth.text())
        if (registro.length > 0) {
            llenar_table(idTable, Metodo);
        }
        else if (row > 1) {

            //document.getElementById('ctl00_MainContent_succes2').style.display = "inline";
            $("#succes2").attr('style', 'display : inline');
        }
    }

    else if (Metodo === "addPagoVarioConfigurationTbl") {

         
        tid = $("#txtCode").val();
        ownerName = $("#txtName").val();
        merchantName = $("#txtDescription").val();

        var msg = "Debe llenar los campos";

        if (tid === "" || ownerName === "" || merchantName === "") {
            msg += "- Code ";
            msg += "- Display Name ";
            msg += "- Description ";
            alertEmptyField(msg, true);

            return;

        }

        $Texth = $(idTable).find("tr").eq(row).find("td").eq(0);

        var registro = $.trim($Texth.text())
        if (registro.length > 0) {
            llenar_table(idTable, Metodo);
        }
        else if (row > 1) {

            //document.getElementById('ctl00_MainContent_succes2').style.display = "inline";
            $("#succes2").attr('style', 'display : inline');
        }
    }
    else if (Metodo === "addIpConfigurationTbl") {

        tid = $("#txtCode").val();
        ownerName = $("#txtName").val();
        merchantName = $("#txtDescription").val();

        var msg = "Debe llenar los campos";

        if (tid === "" || ownerName === "" || merchantName === "") {
            msg += "- Code ";
            msg += "- Display Name ";
            msg += "- Description ";
            alertEmptyField(msg, true);

            return;
        }
        $Texth = $(idTable).find("tr").eq(row).find("td").eq(0);

        var registro = $.trim($Texth.text())
        if (registro.length > 0) {
            llenar_table(idTable, Metodo);
        }
        else if (row > 1) {

            //document.getElementById('ctl00_MainContent_succes2').style.display = "inline";
            $("#succes2").attr('style', 'display : inline');
        }

    }
    else if (Metodo === "EditPromptConfigurationTbl") {

        $Texth = $(idTable).find("tr").eq(row).find("td").eq(0);

        var registro = $.trim($Texth.text())
        if (registro.length > 0) {
            llenar_table(idTable, Metodo);
        }
        else if (row > 1) {

            //document.getElementById('ctl00_MainContent_succes2').style.display = "inline";
            $("#succes2").attr('style', 'display : inline');
            setTimeout('document.location.reload()', 3000);
        }


    }


    else {

        $Texth = $(idTable).find("tr").eq(row).find("td").eq(0);
        var registro = $.trim($Texth.text())
        if (registro.length > 0) {
            llenar_table(idTable, Metodo);
        }
        else if (row > 1) {

            //document.getElementById('ctl00_MainContent_succes2').style.display = "inline";
            $("#succes2").attr('style', 'display : inline');
            setTimeout('document.location.reload()', 3000);
        }
    }


}





function llenar_table(idTable, Metodo) {
    var columnsObj = {};
    if (Metodo === "AddTerminalStis") {
        columnsObj["Tid"] = tid;
        columnsObj["OwnerName"] = ownerName;
        columnsObj["MerchantName"] = merchantName;
        columnsObj["ShopName"] = shopName;
        columnsObj["DetailAddres"] = detailAddres;
        columnsObj["ShopContact"] = shopContact;
        columnsObj["ShopTelNo"] = shopTelNo;
        columnsObj["OfficeContact"] = officeContact;
        columnsObj["OfficeTelNo"] = officeTelNo;
        columnsObj["Status"] = status;
        columnsObj["Contador"] = row;
      


    }
    else if (Metodo === "addAcquirerTbl") {
        columnsObj["Code"] = tid;
        columnsObj["DisplayName"] = ownerName;
        columnsObj["Nii"] = merchantName;

        console.log(columnsObj);

    }

    else if (Metodo === "addIssuerTbl") {

        columnsObj["Code"] = tid;
        columnsObj["DisplayName"] = ownerName;




    }
    else if (Metodo === "addCardRangeTbl") {
        columnsObj["Code"] = tid;
        columnsObj["DisplayName"] = ownerName;
        columnsObj["Description"] = merchantName;


    }
    else if (Metodo === "AddEmvApplicationTbl") {

        columnsObj["DisplayName"] = ownerName;

        var count = 0;
        for (count = 0; count < 3; count++) {
            $selTd = $(idTable).find("tr").eq(count).find("td").eq(1);
            textVal = $.trim($selTd.text());
            if (textVal === "AID") {
                $selTd = $(idTable).find("tr").eq(count).find("td").eq(2);
                textVal = $.trim($selTd.text());
                if (textVal.length <= 0) {
                    alertEmptyField("Debe llenar el campo AID", false);
                    $selTd.css("background", "Pink");
                    $selTd = $(idTable).find("tr").eq(row).find("td").eq(2);
                    $selTd.text("");
                    return;
                }
                else { $selTd.css("background", "White"); }

            }
        }





    }


    else if (Metodo === "AddEmvKeyTbl") {
        columnsObj["DisplayName"] = merchantName;
        columnsObj["Date"] = ownerName;

        var count = 0;
        for (count = 0; count < 3; count++) {
            $selTd = $(idTable).find("tr").eq(count).find("td").eq(1);
            textVal = $.trim($selTd.text());
            if (textVal === "RID") {
                $selTd = $(idTable).find("tr").eq(count).find("td").eq(2);
                textVal = $.trim($selTd.text());
                if (textVal.length <= 0) {
                    alertEmptyField("Debe llenar el campo RID", false);
                    $selTd.css("background", "Pink");
                    $selTd = $(idTable).find("tr").eq(row).find("td").eq(2);
                    $selTd.text("");
                    return;
                }
                else { $selTd.css("background", "White"); }

            }
        }

    }
    else if (Metodo === "createNewAcquirer" || Metodo === "createNewIssuer" || Metodo === "createNewCardRange" || Metodo === "createNewEmvApplication" || Metodo === "createNewEmvKey" || Metodo === "createNewEmvExtraApplication") {
        console.log(ownerName + tid + merchantName);

        columnsObj["DisplayName"] = ownerName;
        columnsObj["Code"] = tid;
        columnsObj["TerminalRecNo"] = merchantName;
        detailAddres = gup('cod', location);

        if (detailAddres.includes("ACQ") === true && detailAddres.includes("ISS") === false && detailAddres.includes("CDR") === false) {

            columnsObj["ACQ"] = detailAddres.substring(3, 8);

        }
        if (detailAddres.includes("ACQ") === true && detailAddres.includes("ISS") === true) {
            shopName = detailAddres.substring(detailAddres.indexOf("%5c%5c") + 6, detailAddres.indexOf("%5c%5c") + 22);
            columnsObj["ISS"] = shopName.substring(0, 8).substring(3, 8);
            columnsObj["ACQ"] = shopName.substring(11);
        }
                            
    }


    else if (Metodo === "addHostConfigurationTbl") {
        columnsObj["Code"] = tid;
        columnsObj["DisplayName"] = ownerName;
        columnsObj["Description"] = merchantName;



    }


    else if (Metodo === "addPromptConfigurationTbl") {
        columnsObj["Code"] = tid;
        columnsObj["DisplayName"] = ownerName;
        columnsObj["Description"] = merchantName;



    }
    else if (Metodo === "addPagoElectronicoConfigurationTbl") {
        columnsObj["Code"] = tid;
        columnsObj["DisplayName"] = ownerName;
        columnsObj["Description"] = merchantName;



    }
    else if (Metodo === "addPagoVarioConfigurationTbl") {
        columnsObj["Code"] = tid;
        columnsObj["DisplayName"] = ownerName;
        columnsObj["Description"] = merchantName;



    }

    else if (Metodo === "addIpConfigurationTbl") {
        columnsObj["Code"] = tid;
        columnsObj["DisplayName"] = ownerName;
        columnsObj["Description"] = merchantName;



    }

    i = 0;
    do {
        $selTh = $(idTable).find("tr").eq(0).find("th").eq(i);
        if ($selTh.text().length === 0)
            break;
        ColmName = $.trim($selTh.text());
        var $selTd = $(idTable).find("tr").eq(row).find("td").eq(i);
        var textVal = $.trim($selTd[0].innerText);
        columnsObj[ColmName] = textVal;

        i++;

    } while ($selTh.text().length > 0);

    row++;



    


    json = JSON.stringify(columnsObj);
    json = json.replace(/"/g, '\\"');
    $.ajax({
        type: "POST",
        url: "wsadd.asmx/" + Metodo,
        data: '{"data":"' + json + '"}',
        contentType: "application/Json; charset=utf-8",
        dataType: 'Json',
        success: function (r) {
            var r2 = "" + r;
            r2.substring(0, 1);
            if (r2 === "0") {
                $("#error2").attr('style', 'display : inline');
            }
            else {
                RecorrerTable(idTable, Metodo);
            }
        },
        error: function (r) {
            var r2 = "" + r;
            r2.substring(0, 1);
            $("#error2").attr('style', 'display : inline');
          //  break;
        },
        failure: function (r) {
            var r2 = "" + r;
            r2.substring(0, 1);
            $("#error2").attr('style', 'display : inline');
         //   break;
        }
    });

}



function alertEmptyField(msg, reload) {
    $.msgAlert({
        type: "error"
        , title: "Los campos son obligatorios"
        , text: msg
        , callback: function () {
            if (reload === true) {
                location.reload();

            }
        }
    });

}