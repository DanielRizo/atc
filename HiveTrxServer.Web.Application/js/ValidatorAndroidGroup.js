﻿$(function () {

    $('#ctl00_MainContent_changePasswordFlag').attr('value', '0');
    $('#ctl00_MainContent_lockTerminalsFlag').attr('value', '0');

    $('#radio25').click(function () {
        $('#ctl00_MainContent_changePasswordFlag').attr('value', '1');
        $('#ctl00_MainContent_txtLockPassword').removeAttr('disabled');
        $('#ctl00_MainContent_txtLockPassword').focus();
    });

    $('#radio26').click(function () {
        $('#ctl00_MainContent_changePasswordFlag').attr('value', '0');
        $('#ctl00_MainContent_txtLockPassword').val("");
        $('#ctl00_MainContent_txtLockPassword').attr('disabled', 'disabled');
        $("#ctl00_MainContent_txtLockPassword").removeClass("st-error-input");
    });

    $('#radio27').click(function () { $('#ctl00_MainContent_lockTerminalsFlag').attr('value', '1'); });
    $('#radio28').click(function () { $('#ctl00_MainContent_lockTerminalsFlag').attr('value', '0'); });

});

function validateExecuteActions() {
    var flagField = true;

    //if ($('#ctl00_MainContent_changePasswordFlag').attr('value') == 1) {
    //    if ($('#ctl00_MainContent_txtLockPassword').val().length <= 0) {
    //        $("#ctl00_MainContent_txtLockPassword").attr("class", "st-error-input");
    //        flagField = false;
    //    }
    //}

    if (!flagField) {
        $.msgAlert({
            type: "error"
            , title: "Mensaje del sistema"
            , text: "Por favor complete los datos del formulario correctamente"
            , callback: function () {
                $.msgAlert.close();
            }
        });
    }

    return flagField;

}

function validateSetValues() {
    var flagField = true;

    if (!validateOnlyDigitsAndLength($("#ctl00_MainContent_txtQueriesNumber").val())) {
        $("#ctl00_MainContent_txtQueriesNumber").attr("class", "st-error-input");
        flagField = false;
    }

    if (!validateOnlyDigitsAndLength($("#ctl00_MainContent_txtFrequency").val())) {
        $("#ctl00_MainContent_txtFrequency").attr("class", "st-error-input");
        flagField = false;
    }

    if (!flagField) {
        $.msgAlert({
            type: "error"
            , title: "Mensaje del sistema"
            , text: "Por favor complete los datos del formulario correctamente"
            , callback: function () {
                $.msgAlert.close();
            }
        });
    }

    return flagField;

}

function validateSetAllowedApps() {
    var flagField = true;

    if ($("#ctl00_MainContent_txtAllowedApps").val().length <= 0) {
        $("#ctl00_MainContent_txtAllowedApps").attr("class", "st-error-input");
        flagField = false;
    }

    if (!flagField) {
        $.msgAlert({
            type: "error"
            , title: "Mensaje del sistema"
            , text: "Por favor complete los datos del formulario correctamente"
            , callback: function () {
                $.msgAlert.close();
            }
        });
    }

    return flagField;

}

function validateAddGroupAndroid() {
    var flagField = true;

    if ($("#ctl00_MainContent_txtGroupName").val().length < 3) {
        $("#ctl00_MainContent_txtGroupName").attr("class", "st-error-input");
        flagField = false;
    }

    if (!flagField) {
        $.msgAlert({
            type: "error"
            , title: "Mensaje del sistema"
            , text: "Por favor complete los datos del formulario correctamente"
            , callback: function () {
                $.msgAlert.close();
            }
        });
    }

    return flagField;

}

$(window).on('beforeunload', function () {
    $("#ctl00_MainContent_btnSave").attr('disabled', 'disabled');
});
