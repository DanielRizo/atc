﻿$(function () {
    var obj = $("#dragandrophandler");

    obj.on('dragenter', function (e) {
        e.stopPropagation();
        e.preventDefault();
        $(this).css('border', '2px solid #0B85A1');
    });

    obj.on('dragover', function (e) {
        e.stopPropagation();
        e.preventDefault();
    });

    obj.on('drop', function (e) {

        $(this).css('border', '2px dotted #0B85A1');
        e.preventDefault();
        var files = e.originalEvent.dataTransfer.files;

        handleFileUpload(files, obj);
    });

    $(document).on('dragenter', function (e) {
        e.stopPropagation();
        e.preventDefault();
    });

    $(document).on('dragover', function (e) {
        e.stopPropagation();
        e.preventDefault();
        obj.css('border', '2px dotted #0B85A1');
    });

    $(document).on('drop', function (e) {
        e.stopPropagation();
        e.preventDefault();
    });

});

function handleFiles(files) {
    var obj = $("#dragandrophandler");

    handleFileUpload(files, obj);
}

function validateAddApplication() {
    var flagField = true;

    if ($("#ctl00_MainContent_fileApp").val().length <= 0 && rowCount == 0) {
        flagField = false;
    }

    if ($("#ctl00_MainContent_txtPackageName").val().length <= 0) {
        flagField = false;
    }

    if ($("#ctl00_MainContent_txtPackageVersion").val().length <= 0) {
        flagField = false;
    }

    if (!flagField) {
        $.msgAlert({
            type: "error"
            , title: "Mensaje del Sistema"
            , text: "Por favor seleccione un archivo de aplicación."
            , callback: function () {
                $.msgAlert.close();
            }
        });
    }

    return flagField;

}

function sendFileToServer(formData, status) {
    var uploadURL = "FileUploaderAndroidAPK.aspx";
    var extraData = {};
    var jqXHR = $.ajax({
        xhr: function () {
            var xhrobj = $.ajaxSettings.xhr();
            if (xhrobj.upload) {
                xhrobj.upload.addEventListener('progress', function (event) {
                    var percent = 0;
                    var position = event.loaded || event.position;
                    var total = event.total;
                    if (event.lengthComputable) {
                        percent = Math.ceil(position / total * 100);
                    }
                    //Set progress
                    status.setProgress(percent);
                }, false);
            }
            return xhrobj;
        },
        url: uploadURL,
        type: "POST",
        contentType: false,
        processData: false,
        cache: false,
        data: formData,
        success: function (data) {
            status.setProgress(100);

            //extract filename, packagename and versionname
            var tokens = data.split("|");

            $("#ctl00_MainContent_txtPackageName").val(tokens[1]);
            $("#ctl00_MainContent_txtPackageVersion").val(tokens[2]);

            $("#ctl00_MainContent_txtPackageNameHidden").val(tokens[1]);
            $("#ctl00_MainContent_txtPackageVersionHidden").val(tokens[2]);

            //Set input Type File null
            $("#ctl00_MainContent_fileApp").val('');

            //Enable Button
            $("#ctl00_MainContent_btnAddApplication").prop('disabled', false);
            $("#ctl00_MainContent_btnAddApplication").removeClass('button-gray').addClass('button-aqua');

        }
    });

    status.setAbort(jqXHR);
}

var rowCount = 0;

function createStatusbar(obj) {
    rowCount++;
    var row = "odd";
    if (rowCount % 2 == 0) row = "even";
    this.statusbar = $("<div class='statusbar " + row + "'></div>");
    this.filename = $("<div class='filename'></div>").appendTo(this.statusbar);
    this.size = $("<div class='filesize'></div>").appendTo(this.statusbar);
    this.progressBar = $("<div class='progressBar'><div></div></div>").appendTo(this.statusbar);
    this.abort = $("<div class='abort'>Cancelar</div>").appendTo(this.statusbar);
    obj.after(this.statusbar);

    this.setFileNameSize = function (name, size) {
        var sizeStr = "";
        var sizeKB = size / 1024;
        if (parseInt(sizeKB) > 1024) {
            var sizeMB = sizeKB / 1024;
            sizeStr = sizeMB.toFixed(2) + " MB";
        }
        else {
            sizeStr = sizeKB.toFixed(2) + " KB";
        }

        this.filename.html(name);
        this.size.html(sizeStr);
    }

    this.setProgress = function (progress) {
        var progressBarWidth = progress * this.progressBar.width() / 100;
        this.progressBar.find('div').animate({ width: progressBarWidth }, 10).html(progress + "% ");
        if (parseInt(progress) >= 100) {
            this.abort.hide();
        }
    }

    this.setAbort = function (jqxhr) {
        var sb = this.statusbar;
        this.abort.click(function () {
            jqxhr.abort();
            sb.hide();
            rowCount = 0;
            $(".filename").html("No hay archivo seleccionado...");
        });
    }
}

function handleFileUpload(files, obj) {

    //Validate only one File per drop or per process
    if (files.length > 1 || rowCount == 1) {

        $.msgAlert({
            type: "error"
                    , title: "Mensaje del Sistema"
                    , text: "Solo se permite subir 1 archivo de aplicación a la vez."
                    , callback: function () {
                        $.msgAlert.close();
                    }
        });

        return;
    }

    //Check file extension
    if (!files[0].name.includes(".apk")) {

        $.msgAlert({
            type: "error"
                    , title: "Mensaje del Sistema"
                    , text: "Tipo de archivo no permitido. Por favor revise los datos."
                    , callback: function () {
                        $.msgAlert.close();
                    }
        });

        return;
    }

    //Check file name
    var deployedFilePattern = /[^a-zA-Z0-9_\.]/;
    if (deployedFilePattern.test(files[0].name)) {

        $.msgAlert({
            type: "error"
                    , title: "Mensaje del Sistema"
                    , text: "Nombre de Archivo no permitido. Solo letras, números y caracter guión (_). Por favor revise los datos."
                    , callback: function () {
                        $.msgAlert.close();
                    }
        });

        return;
    }

    //Send file
    var fd = new FormData();
    fd.append('file', files[0]);

    var status = new createStatusbar(obj);
    status.setFileNameSize(files[0].name, files[0].size);
    sendFileToServer(fd, status);

    //Set Filename
    $(".filename").html(files[0].name);

}

$(window).on('beforeunload', function () {
    $("#ctl00_MainContent_btnAddApplication").attr('disabled', 'disabled');
});

var globalcontrol = '';

function ConfirmAction(control) {

    globalcontrol = control;

    $.msgAlert({
        type: "warning"
                , title: "Mensaje del Sistema"
                , text: "Realmente desea eliminar la aplicación?"
                , callback: function () {
                    __doPostBack($(globalcontrol).attr('name'), '');
                }
    });

    return false;
}