﻿$(function () {

    $('#radio21').click(function () { $('#ctl00_MainContent_customerFlagLearningMode').attr('value', '1'); });
    $('#radio22').click(function () { $('#ctl00_MainContent_customerFlagLearningMode').attr('value', '0'); });

});


function validateAddCustomer() {
    var flagField = true;

    if ($("#ctl00_MainContent_txtCustomerName").val().length < 3) {
        $("#ctl00_MainContent_txtCustomerName").attr("class", "st-error-input");
        flagField = false;
    }

    if (!validateIPAddress($("#ctl00_MainContent_txtIP").val())) {
        $("#ctl00_MainContent_txtIP").attr("class", "st-error-input");
        flagField = false;
    }

    if (!validateOnlyDigitsAndLength($("#ctl00_MainContent_txtPort").val())) {
        $("#ctl00_MainContent_txtPort").attr("class", "st-error-input");
        flagField = false;
    } else if ($("#ctl00_MainContent_txtPort").val() > 65535) {
        $("#ctl00_MainContent_txtPort").attr("class", "st-error-input");
        flagField = false;    
    }

    //Validar Datos de Contacto
    if ($("#ctl00_MainContent_txtContactNumId").val().length > 0) {

        if (!validateOnlyDigitsAndLength($("#ctl00_MainContent_txtContactNumId").val())) {
            $("#ctl00_MainContent_txtContactNumId").attr("class", "st-error-input");
            flagField = false;
        }

        if ($("#ctl00_MainContent_ddlContactIdType").val() <= 0) {
            $("#ctl00_MainContent_ddlContactIdType").parent().addClass("st-error-input");
            flagField = false;
        }

        if ($("#ctl00_MainContent_txtContactName").val().length <= 5) {
            $("#ctl00_MainContent_txtContactName").attr("class", "st-error-input");
            flagField = false;
        }

        if (!validateOnlyDigits($("#ctl00_MainContent_txtContactMobile").val())) {
            $("#ctl00_MainContent_txtContactMobile").attr("class", "st-error-input");
            flagField = false;
        }

        if (!validateOnlyDigits($("#ctl00_MainContent_txtContactPhone").val())) {
            $("#ctl00_MainContent_txtContactPhone").attr("class", "st-error-input");
            flagField = false;
        }

        if ($("#ctl00_MainContent_txtContactEmail").val().length > 0) {
            if (!validateEmailWithNoLength($("#ctl00_MainContent_txtContactEmail").val())) {
                $("#ctl00_MainContent_txtContactEmail").attr("class", "st-error-input");
                flagField = false;
            }
        }

    }

    if (!flagField) {
        $.msgAlert({
            type: "error"
            , title: "Mensaje del sistema"
            , text: "Por favor complete los datos del formulario correctamente"
            , callback: function () {
                $.msgAlert.close();
            }
        });
    }

    return flagField;

}

function validateEditCustomer() {
    var flagField = true;

    if ($("#ctl00_MainContent_txtCustomerName").val().length < 3) {
        $("#ctl00_MainContent_txtCustomerName").attr("class", "st-error-input");
        flagField = false;
    }

    if (!validateIPAddress($("#ctl00_MainContent_txtIP").val())) {
        $("#ctl00_MainContent_txtIP").attr("class", "st-error-input");
        flagField = false;
    }

    if (!validateOnlyDigitsAndLength($("#ctl00_MainContent_txtPort").val())) {
        $("#ctl00_MainContent_txtPort").attr("class", "st-error-input");
        flagField = false;
    } else if ($("#ctl00_MainContent_txtPort").val() > 65535) {
        $("#ctl00_MainContent_txtPort").attr("class", "st-error-input");
        flagField = false;
    }

    //Validar Datos de Contacto
    if ($("#ctl00_MainContent_txtContactNumId").val().length > 0) {

        if (!validateOnlyDigitsAndLength($("#ctl00_MainContent_txtContactNumId").val())) {
            $("#ctl00_MainContent_txtContactNumId").attr("class", "st-error-input");
            flagField = false;
        }

        if ($("#ctl00_MainContent_ddlContactIdType").val() <= 0) {
            $("#ctl00_MainContent_ddlContactIdType").parent().addClass("st-error-input");
            flagField = false;
        }

        if ($("#ctl00_MainContent_txtContactName").val().length <= 5) {
            $("#ctl00_MainContent_txtContactName").attr("class", "st-error-input");
            flagField = false;
        }

        if (!validateOnlyDigits($("#ctl00_MainContent_txtContactMobile").val())) {
            $("#ctl00_MainContent_txtContactMobile").attr("class", "st-error-input");
            flagField = false;
        }

        if (!validateOnlyDigits($("#ctl00_MainContent_txtContactPhone").val())) {
            $("#ctl00_MainContent_txtContactPhone").attr("class", "st-error-input");
            flagField = false;
        }

        if ($("#ctl00_MainContent_txtContactEmail").val().length > 0) {
            if (!validateEmailWithNoLength($("#ctl00_MainContent_txtContactEmail").val())) {
                $("#ctl00_MainContent_txtContactEmail").attr("class", "st-error-input");
                flagField = false;
            }
        }

    }

    if (!flagField) {
        $.msgAlert({
            type: "error"
            , title: "Mensaje del sistema"
            , text: "Por favor complete los datos del formulario correctamente"
            , callback: function () {
                $.msgAlert.close();
            }
        });
    }

    return flagField;

}

function validateAddLicense() {
    var flagField = true;

    if ($("#ctl00_MainContent_ddlLicenseType").val() <= 0) {
        $("#ctl00_MainContent_ddlLicenseType").parent().addClass("st-error-input");
        flagField = false;
    }

    if (!flagField) {
        $.msgAlert({
            type: "error"
            , title: "Mensaje del sistema"
            , text: "Por favor complete los datos del formulario correctamente"
            , callback: function () {
                $.msgAlert.close();
            }
        });
    }

    return flagField;

}

$(document).ready(function () {
    $("#ctl00_MainContent_ddlContactIdType").change(function () {
        $("#ctl00_MainContent_ddlContactIdType").parent().removeClass("st-error-input");
    });

    $("#ctl00_MainContent_ddlLicenseType").change(function () {
        $("#ctl00_MainContent_ddlLicenseType").parent().removeClass("st-error-input");
    });

});

$(window).on('beforeunload', function () {
    $("#ctl00_MainContent_btnSave").attr('disabled', 'disabled');
});