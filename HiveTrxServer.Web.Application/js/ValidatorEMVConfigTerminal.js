﻿
function validateAddOrEditEMVConfig() {
    var flagField = true;

    if ($("#ctl00_MainContent_txtEMVType").val().length !=2 ||
            !validateOnlyHEXDigits($("#ctl00_MainContent_txtEMVType").val())) {
        $("#ctl00_MainContent_txtEMVType").attr("class", "st-error-input");
        flagField = false;
    }

    if ($("#ctl00_MainContent_txtEMVConfig").val().length != 2 ||
            !validateOnlyHEXDigits($("#ctl00_MainContent_txtEMVConfig").val())) {
        $("#ctl00_MainContent_txtEMVConfig").attr("class", "st-error-input");
        flagField = false;
    }

    if ($("#ctl00_MainContent_txtThresholdRS").val().length != 8 ||
            !validateOnlyHEXDigits($("#ctl00_MainContent_txtThresholdRS").val())) {
        $("#ctl00_MainContent_txtThresholdRS").attr("class", "st-error-input");
        flagField = false;
    }

    if ($("#ctl00_MainContent_txtTargetRS").val().length != 2 ||
            !validateOnlyHEXDigits($("#ctl00_MainContent_txtTargetRS").val())) {
        $("#ctl00_MainContent_txtTargetRS").attr("class", "st-error-input");
        flagField = false;
    }

    if ($("#ctl00_MainContent_txtMaxTargetRS").val().length != 2 ||
            !validateOnlyHEXDigits($("#ctl00_MainContent_txtMaxTargetRS").val())) {
        $("#ctl00_MainContent_txtMaxTargetRS").attr("class", "st-error-input");
        flagField = false;
    }

    if ($("#ctl00_MainContent_txtTACDenial").val().length != 10 ||
            !validateOnlyHEXDigits($("#ctl00_MainContent_txtTACDenial").val())) {
        $("#ctl00_MainContent_txtTACDenial").attr("class", "st-error-input");
        flagField = false;
    }

    if ($("#ctl00_MainContent_txtTACOnline").val().length != 10 ||
            !validateOnlyHEXDigits($("#ctl00_MainContent_txtTACOnline").val())) {
        $("#ctl00_MainContent_txtTACOnline").attr("class", "st-error-input");
        flagField = false;
    }

    if ($("#ctl00_MainContent_txtTACDefault").val().length != 10 ||
            !validateOnlyHEXDigits($("#ctl00_MainContent_txtTACDefault").val())) {
        $("#ctl00_MainContent_txtTACDefault").attr("class", "st-error-input");
        flagField = false;
    }
    
    if ($("#ctl00_MainContent_txtAppConfig").val().length > 1024 || 
            $("#ctl00_MainContent_txtAppConfig").val().length <= 0 ||
            !validateOnlyHEXDigits($("#ctl00_MainContent_txtAppConfig").val()) ||
            $("#ctl00_MainContent_txtAppConfig").val().length % 2 != 0) {
        $("#ctl00_MainContent_txtAppConfig").attr("class", "st-error-input");
        flagField = false;
    }

    if (!flagField) {
        $.msgAlert({
            type: "error"
            , title: "Mensaje del sistema"
            , text: "Por favor complete los datos del formulario correctamente"
            , callback: function () {
                $.msgAlert.close();
            }
        });
    }

    return flagField;

}


function validateAddOrEditEMVKey() {
    var flagField = true;

    if ($("#ctl00_MainContent_txtKeyIndex").val().length != 2 ||
            !validateOnlyHEXDigits($("#ctl00_MainContent_txtKeyIndex").val())) {
        $("#ctl00_MainContent_txtKeyIndex").attr("class", "st-error-input");
        flagField = false;
    }

    if ($("#ctl00_MainContent_txtAppId").val().length != 10 ||
            !validateOnlyHEXDigits($("#ctl00_MainContent_txtAppId").val())) {
        $("#ctl00_MainContent_txtAppId").attr("class", "st-error-input");
        flagField = false;
    }

    if ($("#ctl00_MainContent_txtKeyExponent").val().length != 8 ||
            !validateOnlyHEXDigits($("#ctl00_MainContent_txtKeyExponent").val())) {
        $("#ctl00_MainContent_txtKeyExponent").attr("class", "st-error-input");
        flagField = false;
    }

    if ($("#ctl00_MainContent_txtKeySize").val().length != 8 ||
            !validateOnlyHEXDigits($("#ctl00_MainContent_txtKeySize").val())) {
        $("#ctl00_MainContent_txtKeySize").attr("class", "st-error-input");
        flagField = false;
    }

    if ($("#ctl00_MainContent_txtKeyExpiryDate").val().length != 4 ||
            !validateOnlyHEXDigits($("#ctl00_MainContent_txtKeyExpiryDate").val())) {
        $("#ctl00_MainContent_txtKeyExpiryDate").attr("class", "st-error-input");
        flagField = false;
    }

    if ($("#ctl00_MainContent_txtKeyEffectiveDate").val().length != 4 ||
            !validateOnlyHEXDigits($("#ctl00_MainContent_txtKeyEffectiveDate").val())) {
        $("#ctl00_MainContent_txtKeyEffectiveDate").attr("class", "st-error-input");
        flagField = false;
    }

    if ($("#ctl00_MainContent_txtChecksum").val().length != 2 ||
            !validateOnlyHEXDigits($("#ctl00_MainContent_txtChecksum").val())) {
        $("#ctl00_MainContent_txtChecksum").attr("class", "st-error-input");
        flagField = false;
    }

    if ($("#ctl00_MainContent_txtKeyContent").val().length > 512 ||
            $("#ctl00_MainContent_txtKeyContent").val().length <= 0 ||
            !validateOnlyHEXDigits($("#ctl00_MainContent_txtKeyContent").val()) ||
            $("#ctl00_MainContent_txtKeyContent").val().length % 2 != 0) {
        $("#ctl00_MainContent_txtKeyContent").attr("class", "st-error-input");
        flagField = false;
    }

    if (!flagField) {
        $.msgAlert({
            type: "error"
            , title: "Mensaje del sistema"
            , text: "Por favor complete los datos del formulario correctamente"
            , callback: function () {
                $.msgAlert.close();
            }
        });
    }

    return flagField;

}