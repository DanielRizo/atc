﻿$(function() {

    $.datepicker.regional['en'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };

    $.datepicker.setDefaults($.datepicker.regional['en']);

    $("#ctl00_MainContent_txtStartDate").datepicker();
    $("#ctl00_MainContent_txtEndDate").datepicker();

    $('#radio11').click(function() { $('#ctl00_MainContent_groupFlagUpdate').attr('value', '1'); }); 
    $('#radio12').click(function() { $('#ctl00_MainContent_groupFlagUpdate').attr('value', '0'); }); 
    $('#radio13').click(function () { $('#ctl00_MainContent_groupFlagDateRange').attr('value', '1'); setTimeout("__doPostBack('groupFlagDateRangeYES','')", 50); });
    $('#radio14').click(function () { $('#ctl00_MainContent_groupFlagDateRange').attr('value', '0'); setTimeout("__doPostBack('groupFlagDateRangeNO','')", 50); });
    $('#radio21').click(function () { $('#ctl00_MainContent_groupFlagBlockingMode').attr('value', '1'); });
    $('#radio22').click(function () { $('#ctl00_MainContent_groupFlagBlockingMode').attr('value', '0'); });
    $('#radio23').click(function () { $('#ctl00_MainContent_groupFlagUpdateNow').attr('value', '1'); });
    $('#radio24').click(function () { $('#ctl00_MainContent_groupFlagUpdateNow').attr('value', '0'); });
    $('#radio29').click(function () { $('#ctl00_MainContent_groupFlagInitialize').attr('value', '1'); });
    $('#radio30').click(function () { $('#ctl00_MainContent_groupFlagInitialize').attr('value', '0'); });
    $('#radio31').click(function () { $('#ctl00_MainContent_GroupKeyDownload').attr('value', '0'); });
    $('#radio55').click(function () { $('#ctl00_MainContent_GroupKeyDownload').attr('value', '1'); });
});


function validateAddOrEditGroup() {
    var flagField = true;

    if ($("#ctl00_MainContent_txtGroupName").val().length < 3) {
        $("#ctl00_MainContent_txtGroupName").attr("class", "st-error-input");
        flagField = false;
    }

    if (!validateIPAddress($("#ctl00_MainContent_txtIP").val())) {
        $("#ctl00_MainContent_txtIP").attr("class", "st-error-input");
        flagField = false;
    }

    if (!validateOnlyDigitsAndLength($("#ctl00_MainContent_txtPort").val())) {
        $("#ctl00_MainContent_txtPort").attr("class", "st-error-input");
        flagField = false;
    } else if ($("#ctl00_MainContent_txtPort").val() > 65535) {
        $("#ctl00_MainContent_txtPort").attr("class", "st-error-input");
        flagField = false;
    }
    
    if ($("#ctl00_MainContent_pnlDateRange").length) {
        if ($("#ctl00_MainContent_txtStartDate").val().length != 10) {
            $("#ctl00_MainContent_txtStartDate").attr("class", "st-error-input");
            flagField = false;
        }

        if ($("#ctl00_MainContent_txtEndDate").val().length != 10) {
            $("#ctl00_MainContent_txtEndDate").attr("class", "st-error-input");
            flagField = false;
        }
    }

    if ($("#ctl00_MainContent_txtRange1").val().length != 5) {
        $("#ctl00_MainContent_txtRange1").attr("class", "st-error-input");
        flagField = false;
    }

    if ($("#ctl00_MainContent_txtRange2").val().length != 5) {
        $("#ctl00_MainContent_txtRange2").attr("class", "st-error-input");
        flagField = false;
    }
    
    if (!flagField) {
        $.msgAlert({
            type: "error"
            , title: "Mensaje del sistema"
            , text: "Por favor complete los datos del formulario correctamente"
            , callback: function () {
                $.msgAlert.close();
            }
        });
    }

    return flagField;

}

$(window).on('beforeunload', function () {
    $("#ctl00_MainContent_btnSave").attr('disabled', 'disabled');
});
