﻿$(function () {
    $('#ctl00_MainContent_changePasswordFlag').attr('value', '0');
    $('#ctl00_MainContent_lockTerminalsFlag').attr('value', '0');

    $('#radio25').click(function () {
        $('#ctl00_MainContent_changePasswordFlag').attr('value', '1');
        $('#ctl00_MainContent_txtLockPassword').removeAttr('disabled');
        $('#ctl00_MainContent_txtLockPassword').focus();
    });

    $('#radio26').click(function () {
        $('#ctl00_MainContent_changePasswordFlag').attr('value', '0');
        $('#ctl00_MainContent_txtLockPassword').val("");
        $('#ctl00_MainContent_txtLockPassword').attr('disabled', 'disabled');
        $("#ctl00_MainContent_txtLockPassword").removeClass("st-error-input");
    });

    $('#radio27').click(function () { $('#ctl00_MainContent_lockTerminalsFlag').attr('value', '1'); });
    $('#radio28').click(function () { $('#ctl00_MainContent_lockTerminalsFlag').attr('value', '0'); });

    $('#radio15').click(function () { $('#ctl00_MainContent_reloadKeyFlag').attr('value', '1'); });
    $('#radio16').click(function () { $('#ctl00_MainContent_reloadKeyFlag').attr('value', '0'); });

    $('#radio29').click(function () { $('#ctl00_MainContent_groupFlagInitialize').attr('value', '1'); });
    $('#radio30').click(function () { $('#ctl00_MainContent_groupFlagInitialize').attr('value', '0'); });
});

function validateExecuteActions() {
    var flagField = true;

    if ($('#ctl00_MainContent_changePasswordFlag').attr('value') == 1) {
        if ($('#ctl00_MainContent_txtLockPassword').val().length <= 0) {
            $("#ctl00_MainContent_txtLockPassword").attr("class", "st-error-input");
            flagField = false;
        }
    }

    if (!flagField) {
        $.msgAlert({
            type: "error"
            , title: "Mensaje del sistema"
            , text: "Por favor complete los datos del formulario correctamente"
            , callback: function () {
                $.msgAlert.close();
            }
        });
    }

    return flagField;

}

function validateEditGroupAndroidTerminal() {
    var flagField = true;

    if ($("#ctl00_MainContent_txtAllowedApps").val().length < 3) {
        $("#ctl00_MainContent_txtAllowedApps").attr("class", "st-error-input");
        flagField = false;
    }

    //Validar Datos de Contacto
    if ($("#ctl00_MainContent_txtContactNumId").val().length > 0) {

        if (!validateOnlyDigitsAndLength($("#ctl00_MainContent_txtContactNumId").val())) {
            $("#ctl00_MainContent_txtContactNumId").attr("class", "st-error-input");
            flagField = false;
        }

        if ($("#ctl00_MainContent_ddlContactIdType").val() <= 0) {
            $("#ctl00_MainContent_ddlContactIdType").parent().addClass("st-error-input");
            flagField = false;
        }

        if ($("#ctl00_MainContent_txtContactName").val().length <= 5) {
            $("#ctl00_MainContent_txtContactName").attr("class", "st-error-input");
            flagField = false;
        }

        if (!validateOnlyDigits($("#ctl00_MainContent_txtContactMobile").val())) {
            $("#ctl00_MainContent_txtContactMobile").attr("class", "st-error-input");
            flagField = false;
        }

        if (!validateOnlyDigits($("#ctl00_MainContent_txtContactPhone").val())) {
            $("#ctl00_MainContent_txtContactPhone").attr("class", "st-error-input");
            flagField = false;
        }

        if ($("#ctl00_MainContent_txtContactEmail").val().length > 0) {
            if (!validateEmailWithNoLength($("#ctl00_MainContent_txtContactEmail").val())) {
                $("#ctl00_MainContent_txtContactEmail").attr("class", "st-error-input");
                flagField = false;
            }
        }

    }

    if (!flagField) {
        $.msgAlert({
            type: "error"
            , title: "Mensaje del sistema"
            , text: "Por favor complete los datos del formulario correctamente"
            , callback: function () {
                $.msgAlert.close();
            }
        });
    }

    return flagField;

}

$(window).on('beforeunload', function () {
    $("#ctl00_MainContent_btnEdit").attr('disabled', 'disabled');
});
