﻿


$(function () {

    $('#ctl00_MainContent_txtComponent1_1').get(0).type = 'password';
    $('#ctl00_MainContent_txtComponent1_2').get(0).type = 'password';
    $('#ctl00_MainContent_txtComponent1_3').get(0).type = 'password';
    $('#ctl00_MainContent_txtComponent1_4').get(0).type = 'password';
    $('#ctl00_MainContent_txtComponent1_5').get(0).type = 'password';
    $('#ctl00_MainContent_txtComponent1_6').get(0).type = 'password';
    $('#ctl00_MainContent_txtComponent1_7').get(0).type = 'password';
    $('#ctl00_MainContent_txtComponent1_8').get(0).type = 'password';
    $('#ctl00_MainContent_btnVer').css("display", "inline");
    $('#ctl00_MainContent_btnOcultar').css("display", "none");
 

    $('#ctl00_MainContent_btnOcultar').css("display", "none");
    $('#ctl00_MainContent_txtComponent1_1').bind("cut copy paste", function (e) {
        e.preventDefault();
    });
    $('#ctl00_MainContent_txtComponent1_1').keyup(function () {
        $(this).val($(this).val().toUpperCase());
        if ($(this).val().length >= 4) {
            $('#ctl00_MainContent_txtComponent1_2').focus();
            $("#ctl00_MainContent_txtComponent1_1").attr("class", "hg-yellow");
        }
    });

    $('#ctl00_MainContent_txtComponent1_2').bind("cut copy paste", function (e) {
        e.preventDefault();
    });
    $('#ctl00_MainContent_txtComponent1_2').keyup(function () {
        $(this).val($(this).val().toUpperCase());
        if ($(this).val().length >= 4) {
            $('#ctl00_MainContent_txtComponent1_3').focus();
            $("#ctl00_MainContent_txtComponent1_2").attr("class", "hg-yellow");
        }
    });

    $('#ctl00_MainContent_txtComponent1_3').bind("cut copy paste", function (e) {
        e.preventDefault();
    });
    $('#ctl00_MainContent_txtComponent1_3').keyup(function () {
        $(this).val($(this).val().toUpperCase());
        if ($(this).val().length >= 4) {
            $('#ctl00_MainContent_txtComponent1_4').focus();
            $("#ctl00_MainContent_txtComponent1_3").attr("class", "hg-yellow");
        }
    });

    $('#ctl00_MainContent_txtComponent1_4').bind("cut copy paste", function (e) {
        e.preventDefault();
    });
    $('#ctl00_MainContent_txtComponent1_4').keyup(function () {
        $(this).val($(this).val().toUpperCase());
        if ($(this).val().length >= 4) {
            $('#ctl00_MainContent_txtComponent1_5').focus();
            $("#ctl00_MainContent_txtComponent1_4").attr("class", "hg-yellow");
        }
    });

    $('#ctl00_MainContent_txtComponent1_5').bind("cut copy paste", function (e) {
        e.preventDefault();
    });
    $('#ctl00_MainContent_txtComponent1_5').keyup(function () {
        $(this).val($(this).val().toUpperCase());
        if ($(this).val().length >= 4) {
            $('#ctl00_MainContent_txtComponent1_6').focus();
            $("#ctl00_MainContent_txtComponent1_5").attr("class", "hg-yellow");
        }
    });

    $('#ctl00_MainContent_txtComponent1_6').bind("cut copy paste", function (e) {
        e.preventDefault();
    });
    $('#ctl00_MainContent_txtComponent1_6').keyup(function () {
        $(this).val($(this).val().toUpperCase());
        if ($(this).val().length >= 4) {
            $('#ctl00_MainContent_txtComponent1_7').focus();
            $("#ctl00_MainContent_txtComponent1_6").attr("class", "hg-yellow");
        }
    });

    $('#ctl00_MainContent_txtComponent1_7').bind("cut copy paste", function (e) {
        e.preventDefault();
    });
    $('#ctl00_MainContent_txtComponent1_7').keyup(function () {
        $(this).val($(this).val().toUpperCase());
        if ($(this).val().length >= 4) {
            $('#ctl00_MainContent_txtComponent1_8').focus();
            $("#ctl00_MainContent_txtComponent1_7").attr("class", "hg-yellow");
        }
    });

    $('#ctl00_MainContent_txtComponent1_8').bind("cut copy paste", function (e) {
        e.preventDefault();
    });
    $('#ctl00_MainContent_txtComponent1_8').keyup(function () {
        $(this).val($(this).val().toUpperCase());
        if ($(this).val().length >= 4) {
            $('#ctl00_MainContent_btnStep1').focus();
            $("#ctl00_MainContent_txtComponent1_8").attr("class", "hg-yellow");
        }
    });

    $('#ctl00_MainContent_btnVer').click(function (e) {
        e.preventDefault();
        $('#ctl00_MainContent_txtComponent1_1').get(0).type = 'text';
        $('#ctl00_MainContent_txtComponent1_2').get(0).type = 'text';
        $('#ctl00_MainContent_txtComponent1_3').get(0).type = 'text';
        $('#ctl00_MainContent_txtComponent1_4').get(0).type = 'text';
        $('#ctl00_MainContent_txtComponent1_5').get(0).type = 'text';
        $('#ctl00_MainContent_txtComponent1_6').get(0).type = 'text';
        $('#ctl00_MainContent_txtComponent1_7').get(0).type = 'text';
        $('#ctl00_MainContent_txtComponent1_8').get(0).type = 'text';
        $('#ctl00_MainContent_btnVer').css("display", "none");
        $('#ctl00_MainContent_btnOcultar').css("display", "inline");
    });

    $('#ctl00_MainContent_btnOcultar').click(function (e) {
        e.preventDefault();
        $('#ctl00_MainContent_txtComponent1_1').get(0).type = 'password';
        $('#ctl00_MainContent_txtComponent1_2').get(0).type = 'password';
        $('#ctl00_MainContent_txtComponent1_3').get(0).type = 'password';
        $('#ctl00_MainContent_txtComponent1_4').get(0).type = 'password';
        $('#ctl00_MainContent_txtComponent1_5').get(0).type = 'password';
        $('#ctl00_MainContent_txtComponent1_6').get(0).type = 'password';
        $('#ctl00_MainContent_txtComponent1_7').get(0).type = 'password';
        $('#ctl00_MainContent_txtComponent1_8').get(0).type = 'password';
        $('#ctl00_MainContent_btnVer').css("display", "inline");
        $('#ctl00_MainContent_btnOcultar').css("display", "none");
    });

});

function isHexKey(evt) {
    var charCode = evt.key.toUpperCase();
    var reg_ex = /^[0-9A-F]+$/;
    return reg_ex.test(charCode);
}

function isHexText(data) {
    var reg_ex = /^[0-9A-F]+$/;
    return reg_ex.test(data);
}

var firstControlError;

function validateComponent1() {

    var flagField = true;

    if ($("#ctl00_MainContent_txtComponent1_1").val().length < 4 || !isHexText($("#ctl00_MainContent_txtComponent1_1").val())) {
        $("#ctl00_MainContent_txtComponent1_1").attr("class", "hg-red");
        firstControlError = $("#ctl00_MainContent_txtComponent1_1");
        flagField = false;
    } else {
        firstControlError = null;
    }

    if ($("#ctl00_MainContent_txtComponent1_2").val().length < 4 || !isHexText($("#ctl00_MainContent_txtComponent1_2").val())) {
        $("#ctl00_MainContent_txtComponent1_2").attr("class", "hg-red");
        if (typeof firstControlError === 'undefined' || firstControlError === null) {
            firstControlError = $("#ctl00_MainContent_txtComponent1_2");
        }
        flagField = false;
    } else {
        firstControlError = null;
    }

    if ($("#ctl00_MainContent_txtComponent1_3").val().length < 4 || !isHexText($("#ctl00_MainContent_txtComponent1_3").val())) {
        $("#ctl00_MainContent_txtComponent1_3").attr("class", "hg-red");
        if (typeof firstControlError === 'undefined' || firstControlError === null) {
            firstControlError = $("#ctl00_MainContent_txtComponent1_3");
        }
        flagField = false;
    } else {
        firstControlError = null;
    }

    if ($("#ctl00_MainContent_txtComponent1_4").val().length < 4 || !isHexText($("#ctl00_MainContent_txtComponent1_4").val())) {
        $("#ctl00_MainContent_txtComponent1_4").attr("class", "hg-red");
        if (typeof firstControlError === 'undefined' || firstControlError === null) {
            firstControlError = $("#ctl00_MainContent_txtComponent1_4");
        }
        flagField = false;
    } else {
        firstControlError = null;
    }

    if ($("#ctl00_MainContent_txtComponent1_5").val().length < 4 || !isHexText($("#ctl00_MainContent_txtComponent1_5").val())) {
        $("#ctl00_MainContent_txtComponent1_5").attr("class", "hg-red");
        if (typeof firstControlError === 'undefined' || firstControlError === null) {
            firstControlError = $("#ctl00_MainContent_txtComponent1_5");
        }
        flagField = false;
    } else {
        firstControlError = null;
    }

    if ($("#ctl00_MainContent_txtComponent1_6").val().length < 4 || !isHexText($("#ctl00_MainContent_txtComponent1_6").val())) {
        $("#ctl00_MainContent_txtComponent1_6").attr("class", "hg-red");
        if (typeof firstControlError === 'undefined' || firstControlError === null) {
            firstControlError = $("#ctl00_MainContent_txtComponent1_6");
        }
        flagField = false;
    } else {
        firstControlError = null;
    }

    if ($("#ctl00_MainContent_txtComponent1_7").val().length < 4 || !isHexText($("#ctl00_MainContent_txtComponent1_7").val())) {
        $("#ctl00_MainContent_txtComponent1_7").attr("class", "hg-red");
        if (typeof firstControlError === 'undefined' || firstControlError === null) {
            firstControlError = $("#ctl00_MainContent_txtComponent1_7");
        }
        flagField = false;
    } else {
        firstControlError = null;
    }

    if ($("#ctl00_MainContent_txtComponent1_8").val().length < 4 || !isHexText($("#ctl00_MainContent_txtComponent1_8").val())) {
        $("#ctl00_MainContent_txtComponent1_8").attr("class", "hg-red");
        if (typeof firstControlError === 'undefined' || firstControlError === null) {
            firstControlError = $("#ctl00_MainContent_txtComponent1_8");
        }
        flagField = false;
    } else {
        firstControlError = null;
    }

    if (!flagField) {
        $.msgAlert({
            type: "error"
            , title: "Mensaje del Sistema"
            , text: "Por favor complete los datos del componente correctamente"
            , callback: function () {
                $.msgAlert.close();
                $(firstControlError).focus();
            }
        });

    }

    return flagField;

}