﻿$(function () {

    $.datepicker.regional['en'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };

    $.datepicker.setDefaults($.datepicker.regional['en']);

    $("#ctl00_MainContent_txtScheduleDate").datepicker();

    $('#radio11').click(function () { $('#ctl00_MainContent_terminalFlagPriority').attr('value', '1'); setTimeout("__doPostBack('terminalFlagPriorityGroupYES','')", 50); });
    $('#radio12').click(function () { $('#ctl00_MainContent_terminalFlagPriority').attr('value', '0'); setTimeout("__doPostBack('terminalFlagPriorityGroupNO','')", 50); });
    $('#radio13').click(function () { $('#ctl00_MainContent_terminalFlagUpdate').attr('value', '1'); setTimeout("__doPostBack('terminalFlagUpdateYES','')", 50); });
    $('#radio14').click(function () { $('#ctl00_MainContent_terminalFlagUpdate').attr('value', '0'); setTimeout("__doPostBack('terminalFlagUpdateNO','')", 50); });
    $('#radio15').click(function () { $('#ctl00_MainContent_terminalIMEIFlag').attr('value', '1'); });
    $('#radio16').click(function () { $('#ctl00_MainContent_terminalIMEIFlag').attr('value', '0'); });
    $('#radio17').click(function () { $('#ctl00_MainContent_terminalSIMFlag').attr('value', '1'); });
    $('#radio18').click(function () { $('#ctl00_MainContent_terminalSIMFlag').attr('value', '0'); });
    $('#radio19').click(function () { $('#ctl00_MainContent_terminalFlagStatus').attr('value', '1'); });
    $('#radio20').click(function () { $('#ctl00_MainContent_terminalFlagStatus').attr('value', '0'); });
    $('#radio21').click(function () { $('#ctl00_MainContent_terminalFlagBlockingMode').attr('value', '1'); });
    $('#radio22').click(function () { $('#ctl00_MainContent_terminalFlagBlockingMode').attr('value', '0'); });
    $('#radio23').click(function () { $('#ctl00_MainContent_terminalFlagUpdateNow').attr('value', '1'); });
    $('#radio24').click(function () { $('#ctl00_MainContent_terminalFlagUpdateNow').attr('value', '0'); });
    $('#radio89').click(function () { $('#ctl00_MainContent_groupDpwnloadKey').attr('value', '1'); });
    $('#radio38').click(function () { $('#ctl00_MainContent_groupDpwnloadKey').attr('value', '0'); });

    $("#ctl00_MainContent_ddlTerminalType").change(function () {
        $("#ctl00_MainContent_ddlTerminalType").parent().removeClass("st-error-input");
    });

    $("#ctl00_MainContent_ddlTerminalBrand").change(function () {
        $("#ctl00_MainContent_ddlTerminalBrand").parent().removeClass("st-error-input");
    });

    $("#ctl00_MainContent_ddlTerminalModel").change(function () {
        $("#ctl00_MainContent_ddlTerminalModel").parent().removeClass("st-error-input");
    });

    $("#ctl00_MainContent_ddlTerminalInterface").change(function () {
        $("#ctl00_MainContent_ddlTerminalInterface").parent().removeClass("st-error-input");
    });

});

function validateAddOrEditGroupTerminal() {
    var flagField = true;

    if ($("#ctl00_MainContent_txtSerial").val().length < 5) {
        $("#ctl00_MainContent_txtSerial").attr("class", "st-error-input");
        flagField = false;
    }
    
    if ($("#ctl00_MainContent_ddlTerminalType").val() <= 0) {
        $("#ctl00_MainContent_ddlTerminalType").parent().addClass("st-error-input");
        flagField = false;
    }

    if ($("#ctl00_MainContent_ddlTerminalBrand").val() <= 0) {
        $("#ctl00_MainContent_ddlTerminalBrand").parent().addClass("st-error-input");
        flagField = false;
    }

    if ($("#ctl00_MainContent_ddlTerminalModel").val() <= 0) {
        $("#ctl00_MainContent_ddlTerminalModel").parent().addClass("st-error-input");
        flagField = false;
    }

    if ($("#ctl00_MainContent_ddlTerminalInterface").val() <= 0) {
        $("#ctl00_MainContent_ddlTerminalInterface").parent().addClass("st-error-input");
        flagField = false;
    }

    if ($("#ctl00_MainContent_pnlSchedulePerTerminal").length) {
        if ($("#ctl00_MainContent_txtScheduleDate").val().length != 10) {
            $("#ctl00_MainContent_txtScheduleDate").attr("class", "st-error-input");
            flagField = false;
        }

        if (!validateIPAddress($("#ctl00_MainContent_txtIP").val())) {
            $("#ctl00_MainContent_txtIP").attr("class", "st-error-input");
            flagField = false;
        }

        if (!validateOnlyDigitsAndLength($("#ctl00_MainContent_txtPort").val())) {
            $("#ctl00_MainContent_txtPort").attr("class", "st-error-input");
            flagField = false;
        } else if ($("#ctl00_MainContent_txtPort").val() > 65535) {
            $("#ctl00_MainContent_txtPort").attr("class", "st-error-input");
            flagField = false;
        }

        if ($("#ctl00_MainContent_txtRange1").val().length != 5) {
            $("#ctl00_MainContent_txtRange1").attr("class", "st-error-input");
            flagField = false;
        }

    }
    
    //Validar Datos de Contacto
    if ($("#ctl00_MainContent_txtContactNumId").val().length > 0) {

        if (!validateOnlyDigitsAndLength($("#ctl00_MainContent_txtContactNumId").val())) {
            $("#ctl00_MainContent_txtContactNumId").attr("class", "st-error-input");
            flagField = false;
        }

        if ($("#ctl00_MainContent_ddlContactIdType").val() <= 0) {
            $("#ctl00_MainContent_ddlContactIdType").parent().addClass("st-error-input");
            flagField = false;
        }

        if ($("#ctl00_MainContent_txtContactName").val().length <= 5) {
            $("#ctl00_MainContent_txtContactName").attr("class", "st-error-input");
            flagField = false;
        }

        if (!validateOnlyDigits($("#ctl00_MainContent_txtContactMobile").val())) {
            $("#ctl00_MainContent_txtContactMobile").attr("class", "st-error-input");
            flagField = false;
        }

        if (!validateOnlyDigits($("#ctl00_MainContent_txtContactPhone").val())) {
            $("#ctl00_MainContent_txtContactPhone").attr("class", "st-error-input");
            flagField = false;
        }

        if ($("#ctl00_MainContent_txtContactEmail").val().length > 0) {
            if (!validateEmailWithNoLength($("#ctl00_MainContent_txtContactEmail").val())) {
                $("#ctl00_MainContent_txtContactEmail").attr("class", "st-error-input");
                flagField = false;
            }
        }

    }

    if (!flagField) {
        $.msgAlert({
            type: "error"
            , title: "Mensaje del Sistema"
            , text: "Por favor complete los datos del formulario correctamente"
            , callback: function () {
                $.msgAlert.close();
            }
        });
    }

    return flagField;

}

$(window).on('beforeunload', function () {
    $("#ctl00_MainContent_btnSave").attr('disabled', 'disabled');
});
