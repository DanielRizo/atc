﻿$(function () {
    $("#ctl00_MainContent_txtNewPassword").keyup(function () {
        var regexPassword = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[|°!\"#$%&\/\\(\)=\?'\¿¡*~{}\[\]^\-_.:,;<>+@]).{8,}$/;
        var resultValidation = regexPassword.test($("#ctl00_MainContent_txtNewPassword").val());
        if (resultValidation == true) {
            $("#ctl00_MainContent_imgPassword").attr("src", "../img/icons/16x16/ok.png");
        } else {
            $("#ctl00_MainContent_imgPassword").attr("src", "../img/icons/16x16/cancel.png");
        }
    });

    $("#ctl00_MainContent_txtNewPassword2").keyup(function () {        
        if ($("#ctl00_MainContent_txtNewPassword").val() == $("#ctl00_MainContent_txtNewPassword2").val()) {
            $("#ctl00_MainContent_imgPassword2").attr("src", "../img/icons/16x16/ok.png");
        } else {
            $("#ctl00_MainContent_imgPassword2").attr("src", "../img/icons/16x16/cancel.png");
        }
    });
});

function validateLogin() {
    var retVal = false;
    if ($("#ctl00_MainContent_txtUser").val().length <= 0) {
        $("#ctl00_MainContent_txtUser").focus();
        retVal = false;
    } else {
        if ($("#ctl00_MainContent_txtPassword").val().length <= 0) {
            $("#ctl00_MainContent_txtPassword").focus();
            retVal = false;
        } else {
            retVal = true;
        }
    };
    if (!retVal) {
        $.msgAlert({
            type: "error"
            , title: "Mensaje del sistema"
            , text: "Por favor complete los datos del formulario correctamente"
            , callback: function () {
                $.msgAlert.close();
                if ($("#ctl00_MainContent_txtUser").val().length <= 0) {
                    $("#ctl00_MainContent_txtUser").focus();
                } else {
                    if ($("#ctl00_MainContent_txtPassword").val().length <= 0) {
                        $("#ctl00_MainContent_txtPassword").focus();
                    };
                };
            }
        });
    } else {
        $("#ctl00_MainContent_txtPassword").val(hex_md5($("#ctl00_MainContent_txtPassword").val() + hex_sha1(hex_md5($("#ctl00_MainContent_txtPassword").val()))));
    }
    return retVal;
};

function validateForgotPassword() {
    var retVal = false;
    if ($("#ctl00_MainContent_txtEmailForgotPwd").val().length <= 5) {
        $("#ctl00_MainContent_txtEmailForgotPwd").focus();
        retVal = false;
    } else {
        if (!validateEmail($("#ctl00_MainContent_txtEmailForgotPwd").val())) {
            $("#ctl00_MainContent_txtEmailForgotPwd").focus();
            retVal = false;
        } else {
            retVal = true;
        }
    }

    if (!retVal) {
        $.msgAlert({
            type: "error"
            , title: "Mensaje del sistema"
            , text: "Por favor complete los datos del formulario correctamente"
            , callback: function () {
                $.msgAlert.close();
                $("#ctl00_MainContent_txtEmailForgotPwd").focus();
            }
        });
    }

    return retVal;
}


function validateResetPassword() {
    var retVal = false;

    if ($("#ctl00_MainContent_txtNewPassword").val().length < 8) {
        $("#ctl00_MainContent_txtNewPassword").focus();
        retVal = false;
    }
    else if ($("#ctl00_MainContent_txtNewPassword2").val().length < 8) {
        $("#ctl00_MainContent_txtNewPassword2").focus();
        retVal = false;
    }
    else if ($("#ctl00_MainContent_txtNewPassword").val() != $("#ctl00_MainContent_txtNewPassword2").val()) {
        $("#ctl00_MainContent_txtNewPassword").focus();
        retVal = false;
    }
    else {
        retVal = true;
    }

    if (!retVal) {
        $.msgAlert({
            type: "error"
            , title: "Mensaje del sistema"
            , text: "Por favor complete los datos del formulario correctamente"
            , callback: function () {
                $.msgAlert.close();
                if ($("#ctl00_MainContent_txtNewPassword").val().length < 8) {
                    $("#ctl00_MainContent_txtNewPassword").focus();
                } else {
                    if ($("#ctl00_MainContent_txtNewPassword2").val().length < 8) {
                        $("#ctl00_MainContent_txtNewPassword2").focus();
                    }
                }
            }
        });
    } else {
        $("#ctl00_MainContent_txtNewPassword").val(hex_md5($("#ctl00_MainContent_txtNewPassword").val() + hex_sha1(hex_md5($("#ctl00_MainContent_txtNewPassword").val()))));
        $("#ctl00_MainContent_txtNewPassword2").val(hex_md5($("#ctl00_MainContent_txtNewPassword2").val() + hex_sha1(hex_md5($("#ctl00_MainContent_txtNewPassword2").val()))));
    };

    return retVal;

}
