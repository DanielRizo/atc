﻿$(function () {
    var Modelo = document.getElementById('ctl00_MainContent_ddlTipoParametro').options[document.getElementById('ctl00_MainContent_ddlTipoParametro').selectedIndex].text;
    const ContentDiv = document.querySelector('#dragAndDropArea')



    if (Modelo !== "NEW9220") {
        ContentDiv.innerHTML = "";
    }



});

function validateOnlyDigits1(value) {
    //Solo valores numéricos
    var digitsPattern = /^[0-9]*$/;
    return digitsPattern.test(value);
}

function validateAddOrEditParameters() {
    var flagField = true;

    monto = $("#ctl00_MainContent_txtMontoMax").val()

    if (validateOnlyDigits1(monto) == false) {
        $("#ctl00_MainContent_txtMontoMax").attr("class", "st-error-input");
        flagField = false;
    }
    if ($("#ctl00_MainContent_TxtCuotas").val().length < 0) {
        $("#ctl00_MainContent_TxtCuotas").attr("class", "st-error-input");
        flagField = false;
    }
    //TID
    if ($("#ctl00_MainContent_txtTIDIni").val().length < 0) {
        $("#ctl00_MainContent_txtTIDIni").attr("class", "st-error-input");
        flagField = false;
    }
    if ($("#ctl00_MainContent_TxtCuotas").val() < 0) {
        $("#ctl00_MainContent_TxtCuotas").attr("class", "st-error-input");
        flagField = false;
    }

    if ($("#ctl00_MainContent_ddlTipoParametro").val() == 7) {  // MODELO NEW9220

        //  CUOTAS A 
        if ($("#ctl00_MainContent_RadioInteresesSi").is(':checked')) {
            if ($("#ctl00_MainContent_TxtCuotas").val().length == 0) {
                $("#ctl00_MainContent_TxtCuotas").attr("class", "st-error-input");
                flagField = false;
            }
        }
        // NIT PAGO 
        if ($("#ctl00_MainContent_RadioBCPSi").is(':checked')) {
            if ($("#ctl00_MainContent_TxtNiiPago").val().length == 0) {
                $("#ctl00_MainContent_TxtNiiPago").attr("class", "st-error-input");
                flagField = false;
            }
        }

        // SOAT  
        if ($("#ctl00_MainContent_RadioSoatSi").is(':checked')) {
            if ($("#ctl00_MainContent_TextsoatNii").val().length == 0) {
                $("#ctl00_MainContent_TextsoatNii").attr("class", "st-error-input");
                flagField = false;
            }
        }
        // PROMOCIONES 
        if ($("#ctl00_MainContent_RadioPromocionesSi").is(':checked')) {
            if ($("#ctl00_MainContent_TexPromocionesNii").val().length == 0) {
                $("#ctl00_MainContent_TexPromocionesNii").attr("class", "st-error-input");
                flagField = false;
            }
        }
        // BANCA JOVEN 
        if ($("#ctl00_MainContent_RadioBancaJovenSi").is(':checked')) {
            if ($("#ctl00_MainContent_TxtBancaJovenPorcentaje").val().length == 0) {
                $("#ctl00_MainContent_TxtBancaJovenPorcentaje").attr("class", "st-error-input");
                flagField = false;
            }
        }
        // ACTIVAR EFECTIVO 
        if ($("#ctl00_MainContent_RadioEfectivoSi").is(':checked')) {
            if ($("#ctl00_MainContent_txtMontoMax").val().length == 0) {
                $("#ctl00_MainContent_txtMontoMax").attr("class", "st-error-input");
                flagField = false;
            }
        }
        if (!validateIPAddress($("#ctl00_MainContent_IpPolaris").val())) {
            $("#ctl00_MainContent_IpPolaris").attr("class", "st-error-input");
            flagField = false;
        }
        if ($("#ctl00_MainContent_PuertoPolaris").val().length < 4) {
            $("#ctl00_MainContent_PuertoPolaris").attr("class", "st-error-input");
            flagField = false;
        }
        if (!validateIPAddress($("#ctl00_MainContent_IpIni").val())) {
            $("#ctl00_MainContent_IpIni").attr("class", "st-error-input");
            flagField = false;
        }
        if ($("#ctl00_MainContent_PortIni").val().length < 4) {
            $("#ctl00_MainContent_PortIni").attr("class", "st-error-input");
            flagField = false;
        }
        if ($("#ctl00_MainContent_TIDPOS").val().length < 8) {
            $("#ctl00_MainContent_TIDPOS").attr("class", "st-error-input");
            flagField = false;
        }
        if (!validateIPAddress($("#ctl00_MainContent_TextiPCajas").val())) {
            $("#ctl00_MainContent_TextiPCajas").attr("class", "st-error-input");
            flagField = false;
        }
        // CAJAS 
        if ($("#ctl00_MainContent_RadioCajasSi").is(':checked')) {
            if (!validateIPAddress($("#ctl00_MainContent_TextiPCajas").val())) {
                $("#ctl00_MainContent_TextiPCajas").attr("class", "st-error-input");
                flagField = false;
            }
            if ($("#ctl00_MainContent_txtPortCajas").val().length < 4) {
                $("#ctl00_MainContent_txtPortCajas").attr("class", "st-error-input");
                flagField = false;
            }
        }
    }
    if ($("#ctl00_MainContent_ddlTipoParametro").val() == 3 || 5 || 9 || 11) {                               // MODELO T1000 - A5-T1000
        //Cuotas
        if ($("#ctl00_MainContent_RadioInteresesSi").is(':enabled')) {
            if ($("#ctl00_MainContent_RadioInteresesSi").is(':checked')) {
                if ($("#ctl00_MainContent_TxtCuotas").val().length == 0) {
                    $("#ctl00_MainContent_TxtCuotas").attr("class", "st-error-input");
                    flagField = false;
                }
            }
        }
        //NII Pagos
        if ($("#ctl00_MainContent_RadioBISASi").is(':enabled')) {
            if ($("#ctl00_MainContent_RadioBISASi").is(':checked')) {
                if ($("#ctl00_MainContent_TxtNiiPago").val().length == 0) {
                    $("#ctl00_MainContent_TxtNiiPago").attr("class", "st-error-input");
                    flagField = false;
                }
            }
        }

        //banca joven
        if ($("#ctl00_MainContent_RadioBancaJovenSi").is(':enabled')) {
            if ($("#ctl00_MainContent_RadioBancaJovenSi").is(':checked')) {
                if ($("#ctl00_MainContent_TxtBancaJovenPorcentaje").val().length == 0) {
                    $("#ctl00_MainContent_TxtBancaJovenPorcentaje").attr("class", "st-error-input");
                    flagField = false;
                }
            }
        }

        //promociones
        if ($("#ctl00_MainContent_RadioPromocionesSi").is(':enabled')) {
            if ($("#ctl00_MainContent_RadioPromocionesSi").is(':checked')) {
                if ($("#ctl00_MainContent_TexPromocionesNii").val().length == 0) {
                    $("#ctl00_MainContent_TexPromocionesNii").attr("class", "st-error-input");
                    flagField = false;
                }
            }
        }
        //Soat
        if ($("#ctl00_MainContent_RadioSoatSi").is(':enabled')) {

            if ($("#ctl00_MainContent_RadioSoatSi").is(':checked')) {
                if ($("#ctl00_MainContent_TextsoatNii").val().length == 0) {
                    $("#ctl00_MainContent_TextsoatNii").attr("class", "st-error-input");
                    flagField = false;
                }
            }
        }
        //SoatNew
        $("#ctl00_MainContent_RadioNewSoatSi").on('change', function () {
            if ($(this).is(':checked')) {
                $("#ctl00_MainContent_TextNewsoatNii").prop("disabled", true);
            } else {
                $("#ctl00_MainContent_TextNewsoatNii").prop("disabled", false);
            }
        });

        //Monto Maximo
        if ($("#ctl00_MainContent_RadioEfectivoSi").is(':enabled')) {
            if ($("#ctl00_MainContent_RadioEfectivoSi").is(':checked')) {
                if ($("#ctl00_MainContent_txtMontoMax").val().length == 0) {
                    $("#ctl00_MainContent_txtMontoMax").attr("class", "st-error-input");
                    flagField = false;
                }
            }
        }

        if (!validateIPAddress($("#ctl00_MainContent_IpPolaris").val())) {
            $("#ctl00_MainContent_IpPolaris").attr("class", "st-error-input");
            flagField = false;
        }
        if ($("#ctl00_MainContent_PuertoPolaris").val().length < 4) {
            $("#ctl00_MainContent_PuertoPolaris").attr("class", "st-error-input");
            flagField = false;
        }
        if (!validateIPAddress($("#ctl00_MainContent_IpIni").val())) {
            $("#ctl00_MainContent_IpIni").attr("class", "st-error-input");
            flagField = false;
        }
        if ($("#ctl00_MainContent_PortIni").val().length < 4) {
            $("#ctl00_MainContent_PortIni").attr("class", "st-error-input");
            flagField = false;
        }
        if ($("#ctl00_MainContent_TIDPOS").val().length < 8) {
            $("#ctl00_MainContent_TIDPOS").attr("class", "st-error-input");
            flagField = false;
        }
        //Cajas

        if ($("#ctl00_MainContent_RadioCajasSi").is(':checked')) {
            if (!validateIPAddress($("#ctl00_MainContent_TextiPCajas").val())) {
                $("#ctl00_MainContent_TextiPCajas").attr("class", "st-error-input");
                flagField = false;
            }
            if ($("#ctl00_MainContent_txtPortCajas").val().length < 4) {
                $("#ctl00_MainContent_txtPortCajas").attr("class", "st-error-input");
                flagField = false;
            }
        }
    }

    if (!flagField) {
        $.msgAlert({
            type: "error"
            , title: "Mensaje del Sistema"
            , text: "Por favor complete los datos del formulario correctamente"
            , callback: function () {
                $.msgAlert.close();
            }
        });
    }
    return flagField;
}

$(window).on('beforeunload', function () {
    $("#ctl00_MainContent_btnAsistente").attr('disabled', 'disabled');
});