﻿$(function () {
    $("#ctl00_MainContent_txtPassword").keyup(function () {
        var regexPassword = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[|°!\"#$%&\/\\(\)=\?'\¿¡*~{}\[\]^\-_.:,;<>+@]).{10,}$/;
        var resultValidation = regexPassword.test($("#ctl00_MainContent_txtPassword").val());
        if (resultValidation == true) {
            $("#ctl00_MainContent_imgPassword").attr("src", "../img/icons/16x16/ok.png");
        } else {
            $("#ctl00_MainContent_imgPassword").attr("src", "../img/icons/16x16/cancel.png");
        };
    });
});

function validatePasswordIcon() {
    var regexPassword = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[|°!\"#$%&\/\\(\)=\?'\¿¡*~{}\[\]^\-_.:,;<>+@]).{10,}$/;
    var resultValidation = regexPassword.test($("#ctl00_MainContent_txtPassword").val());
    if (resultValidation == true) {
        $("#ctl00_MainContent_imgPassword").attr("src", "../img/icons/16x16/ok.png");
    } else {
        $("#ctl00_MainContent_imgPassword").attr("src", "../img/icons/16x16/cancel.png");
    };
}

function validateAddUser() {
    var flagField = true;
    var regexPassword = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[|°!\"#$%&\/\\(\)=\?'\¿¡*~{}\[\]^\-_.:,;<>+@]).{8,}$/;

    if ($("#ctl00_MainContent_chkLocalPassword").is(':checked')) {
        if (!regexPassword.test($("#ctl00_MainContent_txtPassword").val())) {
            $("#ctl00_MainContent_txtPassword").attr("class", "st-error-input");
            flagField = false;
        }
    }

    if ($("#ctl00_MainContent_txtUserName").val().length < 8) {
        $("#ctl00_MainContent_txtUserName").attr("class", "st-error-input");
        flagField = false;
    }

    if ($("#ctl00_MainContent_txtLogin").val().length < 5) {
        $("#ctl00_MainContent_txtLogin").attr("class", "st-error-input");
        flagField = false;
    }

    if (!validateTxtTeleLoader($("#ctl00_MainContent_txtUserName").val())) {
        $("#ctl00_MainContent_txtUserName").attr("class", "st-error-input");
        flagField = false;
    }

    if ($("#ctl00_MainContent_txtEmail").val().length <= 0) {
        $("#ctl00_MainContent_txtEmail").attr("class", "st-error-input");
        flagField = false;
    }

    if (!validateEmail($("#ctl00_MainContent_txtEmail").val())) {
        $("#ctl00_MainContent_txtEmail").attr("class", "st-error-input");
        flagField = false;
    }

    if ($("#ctl00_MainContent_ddlUserProfile").val() <= 0) {
        $("#ctl00_MainContent_ddlUserProfile").parent().addClass("st-error-input");
        flagField = false;
    } else {
        //Validar Perfil Cliente
        if ($("#ctl00_MainContent_ddlUserProfile").val() == 8 &&
                $("#ctl00_MainContent_ddlCustomer").val() == 0) {
            $("#ctl00_MainContent_ddlCustomer").parent().addClass("st-error-input");
            flagField = false;
        }
    }

    if (!flagField) {
        $.msgAlert({
            type: "error"
            , title: "Mensaje del Sistema"
            , text: "Por favor complete los datos del formulario correctamente"
            , callback: function () {
                $.msgAlert.close();
            }
        });
    } else {
        if ($("#ctl00_MainContent_chkLocalPassword").is(':checked')) {
            $("#ctl00_MainContent_txtPassword").val(hex_md5($("#ctl00_MainContent_txtPassword").val() + hex_sha1(hex_md5($("#ctl00_MainContent_txtPassword").val()))));
        }
    };

    return flagField;

}

function validateEditUser() {

    var flagField = true;
    var regexPassword = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[|°!\"#$%&\/\\(\)=\?'\¿¡*~{}\[\]^\-_.:,;<>+@]).{8,}$/;

    if ($("#ctl00_MainContent_rbResetPasswordLocal").is(':checked')) {
        if (!regexPassword.test($("#ctl00_MainContent_txtPassword").val())) {
            $("#ctl00_MainContent_txtPassword").attr("class", "st-error-input");
            flagField = false;
        }
    }

    if ($("#ctl00_MainContent_txtUserName").val().length <= 10) {
        $("#ctl00_MainContent_txtUserName").attr("class", "st-error-input");
        flagField = false;
    }

    if (!validateTxtTeleLoader($("#ctl00_MainContent_txtUserName").val())) {
        $("#ctl00_MainContent_txtUserName").attr("class", "st-error-input");
        flagField = false;
    }

    if ($("#ctl00_MainContent_txtEmail").val().length <= 0) {
        $("#ctl00_MainContent_txtEmail").attr("class", "st-error-input");
        flagField = false;
    }

    if (!validateEmail($("#ctl00_MainContent_txtEmail").val())) {
        $("#ctl00_MainContent_txtEmail").attr("class", "st-error-input");
        flagField = false;
    }

    if ($("#ctl00_MainContent_ddlUserProfile").val() <= 0) {
        $("#ctl00_MainContent_ddlUserProfile").parent().addClass("st-error-input");
        flagField = false;
    }

    if (!flagField) {
        $.msgAlert({
            type: "error"
            , title: "Mensaje del Sistema"
            , text: "Por favor complete los datos del formulario correctamente"
            , callback: function () {
                $.msgAlert.close();
            }
        });
    } else {
        if ($("#ctl00_MainContent_rbResetPasswordLocal").is(':checked')) {
            $("#ctl00_MainContent_txtPassword").val(hex_md5($("#ctl00_MainContent_txtPassword").val() + hex_sha1(hex_md5($("#ctl00_MainContent_txtPassword").val()))));
        }

    }

    return flagField;

}

function validateAddWebModule() {
    var flagField = true;

    if ($("#ctl00_MainContent_txtModuleName").val().length <= 3) {
        $("#ctl00_MainContent_txtModuleName").attr("class", "st-error-input");
        flagField = false;
    }

    if (!flagField) {
        $.msgAlert({
            type: "error"
            , title: "Mensaje del Sistema"
            , text: "Por favor complete los datos del formulario correctamente"
            , callback: function () {
                $.msgAlert.close();
            }
        });
    }
    return flagField;
}

function validateAddWebOption() {

    var flagField = true;

    if ($("#ctl00_MainContent_txtOptionName").val().length <= 3) {
        $("#ctl00_MainContent_txtOptionName").attr("class", "st-error-input");
        flagField = false;
    }

    if (!flagField) {
        $.msgAlert({
            type: "error"
            , title: "Mensaje del Sistema"
            , text: "Por favor complete los datos del formulario correctamente"
            , callback: function () {
                $.msgAlert.close();
            }
        });
    }
    return flagField;
}

$(document).ready(function () {
    $("#ctl00_MainContent_ddlUserProfile").change(function () {
        $("#ctl00_MainContent_ddlUserProfile").parent().removeClass("st-error-input");
    });

    $("#ctl00_MainContent_ddlUserStatus").change(function () {
        $("#ctl00_MainContent_ddlUserStatus").parent().removeClass("st-error-input");
    });

});

$(window).on('beforeunload', function () {
    $("#ctl00_MainContent_btnSave").attr('disabled', 'disabled');
});