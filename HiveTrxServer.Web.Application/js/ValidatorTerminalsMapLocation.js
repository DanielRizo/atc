﻿//Init Google Map

var map;
var markerId = 0;
var infoWindow;
var markerCluster;
var updateTimeout = 60000;  //1 minute
var locationsFinal = [];

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: 0, lng: 0 },
        zoom: 1
    });

    setTimeout(updateMap, 50);
}


$(function () {

    $(document).on('mozfullscreenchange webkitfullscreenchange fullscreenchange', function () {
        setTimeout(setMarkers, 50);
    });

    //setTimeout(updateMap, updateTimeout);

});

function setMarkers() {

    var infoWindow = new google.maps.InfoWindow();

    checkRepeteatedMarkers(locations);

    if (typeof (markerCluster) != 'undefined') {
        markerCluster.clearMarkers();
    }

    if (typeof (locationsFinal) != 'undefined') {
        var bounds = new google.maps.LatLngBounds();
        var marker, i, markers = [];
        for (i = 0; i < locationsFinal.length; i++) {
            markerId = i;
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(locationsFinal[i][2], locationsFinal[i][3]),
                map: map,
                title: locationsFinal[i][1],
                icon: locationsFinal[i][4],
                terminalID: locationsFinal[i][0]
            });
            bounds.extend(marker.position);
            markers.push(marker);

            google.maps.event.addListener(marker, 'click', function (evt) {

                infoWindow.close();
                var localMarker = this;

                $.ajax({
                    url: "GetTerminalData.aspx?terminalID=" + this.terminalID,
                    success: function (result) {
                        infoWindow.setContent(result);
                        infoWindow.open(map, localMarker);
                    }
                });

            });
        }

        if (locationsFinal.length > 0) {
            map.fitBounds(bounds);
            markerCluster = new MarkerClusterer(map, markers, { maxZoom: 20, minZoom: 20, imagePath: '../img/m' });
        }
    }

}

function updateMap() {

    $.ajax({
        url: "GetTerminalsData.aspx",
        success: function (result) {
            locations = [];
            locationsFinal = [];
            eval("locations = " + result + ";");
            setMarkers();
        }
    });

    //setTimeout(updateMap, updateTimeout);
}

function extractRepeated(locArray, item) {

    var locationsLocal = [];

    locationsLocal.push(item);

    for (i = 0; i < locArray.length; i++) {
        if (locArray[i][2] == item[2] && locArray[i][3] == item[3]) {
            locationsLocal.push(locArray[i]);
            locArray.splice(i, 1);
            i--;
        }
    }

    if (locationsLocal.length > 1) {
        for (i = 0; i < locationsLocal.length; i++) {
            var a = 360.0 / locationsLocal.length;
            var newLat = locationsLocal[i][2] + -.00012 * Math.cos((+a * i) / 180 * Math.PI);  //X
            var newLng = locationsLocal[i][3] + -.00012 * Math.sin((+a * i) / 180 * Math.PI);  //Y
            locationsLocal[i][2] = newLat;
            locationsLocal[i][3] = newLng;

            locationsFinal.push(locationsLocal[i]);
        }
    } else {
        locationsFinal.push(locationsLocal[0]);
    }
}

function checkRepeteatedMarkers(locArray) {

    if (locArray.length <= 0) {
        return false;
    }
    else {
        var itemZero = locArray[0];
        locArray.splice(0, 1);

        extractRepeated(locArray, itemZero);

        return checkRepeteatedMarkers(locArray);
    }

}