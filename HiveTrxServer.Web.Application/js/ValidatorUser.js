﻿$(function () {
    $("#ctl00_MainContent_txtNewPassword").keyup(function () {
        var regex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[|°!\"#$%&\/\\(\)=\?'\¿¡*~{}\[\]^\-_.:,;<>+@]).{10,}$/;
        var result = regex.test($("#ctl00_MainContent_txtNewPassword").val());
        if (result == true) {
            $("#ctl00_MainContent_imgNewPassword").attr("src", "../img/icons/16x16/ok.png");
        } else {
            $("#ctl00_MainContent_imgNewPassword").attr("src", "../img/icons/16x16/cancel.png");
        };
    });
    $("#ctl00_MainContent_txtNewPassword2").keyup(function () {
        if ($("#ctl00_MainContent_txtNewPassword").val() == $("#ctl00_MainContent_txtNewPassword2").val()) {
            $("#ctl00_MainContent_imgNewPassword2").attr("src", "../img/icons/16x16/ok.png");
        } else {
            $("#ctl00_MainContent_imgNewPassword2").attr("src", "../img/icons/16x16/cancel.png");
        };
    });
});

function validateEditUser() {
    var flagField = true;

    if ($("#ctl00_MainContent_txtUserName").val().length <= 10) {
        $("#ctl00_MainContent_txtUserName").attr("class", "st-error-input");
        flagField = false;
    }

    if (!validateTxtTeleLoader($("#ctl00_MainContent_txtUserName").val())) {
        $("#ctl00_MainContent_txtUserName").attr("class", "st-error-input");
        flagField = false;
    }  
        
    if ($("#ctl00_MainContent_txtEmail").val().length <= 0) {
        $("#ctl00_MainContent_txtEmail").attr("class", "st-error-input");
        flagField = false;
    }

    if (!validateEmail($("#ctl00_MainContent_txtEmail").val())) {
        $("#ctl00_MainContent_txtEmail").attr("class", "st-error-input");
        flagField = false;
    }

    if (!flagField) {
        $.msgAlert({
            type: "error"
            , title: "Mensaje del Sistema"
            , text: "Por favor complete los datos del formulario correctamente"
            , callback: function () {
                $.msgAlert.close();
            }
        });
    }
    return flagField;
}

function validateNewPassword() {
    var retVal = false;
    var errorCode = 0;
    var msgTxt = "";
    var regex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[|°!\"#$%&\/\\(\)=\?'\¿¡*~{}\[\]^\-_.:,;<>+@]).{10,}$/;
    if (!regex.test($("#ctl00_MainContent_txtNewPassword").val())) {
        errorCode = 2;
        retVal = false;
    } else {
        if ($("#ctl00_MainContent_txtCurrentPassword").val().length < 10 || $("#ctl00_MainContent_txtCurrentPassword").val().indexOf(" ") > 0) {
            errorCode = 1;
            retVal = false;
        } else {
            if ($("#ctl00_MainContent_txtNewPassword").val().length < 10 || $("#ctl00_MainContent_txtNewPassword").val().indexOf(" ") > 0) {
                errorCode = 2;
                retVal = false;
            } else {
                if ($("#ctl00_MainContent_txtNewPassword2").val().length < 10 || $("#ctl00_MainContent_txtNewPassword2").val().indexOf(" ") > 0) {
                    errorCode = 3;
                    retVal = false;
                } else {
                    if ($("#ctl00_MainContent_txtNewPassword").val() != $("#ctl00_MainContent_txtNewPassword2").val()) {
                        errorCode = 4;
                        retVal = false;
                    } else {
                        if (($("#ctl00_MainContent_txtNewPassword").val() == $("#ctl00_MainContent_txtNewPassword2").val()) && ($("#ctl00_MainContent_txtNewPassword").val() == $("#ctl00_MainContent_txtCurrentPassword").val())) {
                            errorCode = 5;
                            retVal = false;
                        } else {
                            retVal = true;
                        }
                    }
                }
            }
        }
    }
    if (!retVal) {
        switch (errorCode) {
            case 1:
                msgTxt = "Por favor digite la clave actual correctamente.";
                $("#ctl00_MainContent_txtCurrentPassword").attr("class", "st-error-input");
                break;
            case 2:
                msgTxt = "Por favor digite la nueva clave correctamente.";
                $("#ctl00_MainContent_txtNewPassword").attr("class", "st-error-input");
                break;
            case 3:
                msgTxt = "Por favor digite otra vez la nueva clave.";
                $("#ctl00_MainContent_txtNewPassword2").attr("class", "st-error-input");
                break;
            case 4:
                msgTxt = "La nueva clave debe ser igual en los dos campos.";
                $("#ctl00_MainContent_txtNewPassword").attr("class", "st-error-input");
                $("#ctl00_MainContent_txtNewPassword2").attr("class", "st-error-input");
                break;
            case 5:
                msgTxt = "La nueva clave debe ser diferente a la clave actual.";
                $("#ctl00_MainContent_txtNewPassword").attr("class", "st-error-input");
                $("#ctl00_MainContent_txtNewPassword2").attr("class", "st-error-input");
                break;
        }
        $.msgAlert({
            type: "error"
            , title: "Mensaje del Sistema"
            , text: msgTxt
            , callback: function () {
                $.msgAlert.close();
            }
        });
    } else {
        $("#ctl00_MainContent_txtCurrentPassword").val(hex_md5($("#ctl00_MainContent_txtCurrentPassword").val() + hex_sha1(hex_md5($("#ctl00_MainContent_txtCurrentPassword").val()))));
        $("#ctl00_MainContent_txtNewPassword").val(hex_md5($("#ctl00_MainContent_txtNewPassword").val() + hex_sha1(hex_md5($("#ctl00_MainContent_txtNewPassword").val()))));
        $("#ctl00_MainContent_txtNewPassword2").val(hex_md5($("#ctl00_MainContent_txtNewPassword2").val() + hex_sha1(hex_md5($("#ctl00_MainContent_txtNewPassword2").val()))));
    }
    return retVal;
}