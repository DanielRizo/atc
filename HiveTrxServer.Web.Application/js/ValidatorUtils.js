﻿function validateEmail(mailValue) {
    //Solo direcciones de correo correctas
    if (mailValue.length > 0) {
        var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        return emailPattern.test(mailValue);
    }

    return false;
}

function validateEmailWithNoLength(mailValue) {
    //Solo direcciones de correo correctas
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    return emailPattern.test(mailValue);
}

function isAlfaNum(dataValue) {

    //Solo Caracteres a-z, A-Z, Dígitos 0-9
    var patron = new RegExp("\\w+$", "i");

    if (!patron.test(dataValue))
        return false;
    else
        return true;
}

function validateTxtTeleLoader(strValue) {
    
    //Solo Caracteres a-z, A-Z y Espacio
    var patron = /^[a-zA-ZáéíóúÑñÁÉÍÓÚ ]*$/;

    if (!strValue.search(patron))
        return true;
    else
        return false;
}

function validateDecimal(value) {
    //Solo valores decimales
    var decimalPattern = /^[0-9,]*$/;
    return decimalPattern.test(value);
}

function validateOnlyDigits(value) {
    //Solo valores numéricos
    var digitsPattern = /^[0-9]*$/;
    return digitsPattern.test(value);
}

function validateOnlyDigitsAndLength(value) {
    //Solo valores numéricos
    if (value.length > 0) {
        var digitsPattern = /^[0-9]*$/;
        return digitsPattern.test(value);
    }

    return false;
}

function validateOnlyHEXDigits(value) {
    //Solo valores Hexadecimal
    var digitsPattern = /^[0-9A-F]+$/;
    return digitsPattern.test(value);
}

function validateIPAddress(value) {
    //Solo Direcciones IP Válidas
    if (value.length > 0) {
        var digitsPattern = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
        return digitsPattern.test(value);
    }

    return false;
}

function zeroPad(num, size) {
    var s = num + "";
    while (s.length < size) s = "0" + s;
    return s;
}