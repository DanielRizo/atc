﻿
//Created by Julian Acosta

//   $("#posStis table tr").contextMenu({ menu: 'myMenu' }, function (action, el, pos) { contextMenuWork(action, el, pos); });

function gup(name, url) {
    if (!url) url = location.href;
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(url);
    return results === null ? null : results[1];
}


function enableEditTable(idTable, showDeleteBtn, showEditiBUtton, editFromParam, columsToEdit, methodPost) {
    var columnsObj = {};

    $(idTable).Tabledit({
        editButton: showEditiBUtton,
        deleteButton: showDeleteBtn,
        saveButton: true,
        restoreButton: true,
        editableFromParam: editFromParam,
        columns: {
            identifier: [0, 'id'],
            // editable: [[2, 'Value'], [3, 'City', '{"1": "@mdo", "2": "@fat", "3": "@twitter"}']]
            editable: columsToEdit

        },
        buttons: {
            edit: {
                class: 'btn btn-sm btn-info',
                html: '<img src="../img/icons/16x16/edit.png" alt="icon" class="d-icon" />',
                action: 'edit'
            },
            delete: {
                class: 'btn btn-sm btn-danger',
                html: '<img src="../img/icons/16x16/delete.png" alt="icon" class="d-icon" />',
                action: 'delete'
            },
            save: {
                class: 'btn btn-sm btn-success',
                html: '<img src="../img/icons/16x16/ok.png" alt="icon" class="d-icon" />',

            },
            restore: {
                class: 'btn btn-sm btn-warning',
                html: 'Restore',
                action: 'restore'
            },
            confirm: {
                class: 'btn btn-sm btn-danger',
                html: 'Confirmaaaaar'
            }
        },
        onSuccess: function (data, textStatus, jqXHR) {
            console.log('onSuccess(data, textStatus, jqXHR)');
            console.log(data);
            console.log(textStatus);
            console.log(jqXHR);
        },
        onEdit: function () {

            //Aditional data
            if (methodPost === "addAcquirerTbl") {
                var code;
                var disName;
                var nii;



                code = $("#txtCode").val();
                disName = $("#txtName").val();
                nii = $("#txtNii").val();

                var msg = "Debe llenar los campos";
                if (code === "" || disName === "" || nii === "") {
                    msg += "- Code ";
                    msg += "- Display Name ";
                    msg += "- NII ";
                    alertEmptyField(msg, true);

                    return;
                }

                columnsObj["Code"] = code;
                columnsObj["DisplayName"] = disName;
                columnsObj["Nii"] = nii;

            }

            //Aditional data
            if (methodPost === "addIssuerTbl") {
                var code;
                var disName;

                code = $("#txtCode").val();
                disName = $("#txtName").val();
                var msg = "Debe llenar los campos";

                if (code === "" || disName === "") {
                    msg += "- Code ";
                    msg += "- Display Name ";

                    alertEmptyField(msg, true);

                    return;
                }
                columnsObj["Code"] = code;
                columnsObj["DisplayName"] = disName;

            }

            //Aditional data
            if (methodPost === "addCardRangeTbl") {
                var code;
                var disName;
                var description;

                code = $("#txtCode").val();
                disName = $("#txtName").val();
                description = $("#txtDescription").val();

                var msg = "Debe llenar los campos";

                if (code === "" || disName === "" || description === "") {
                    msg += "- Code ";
                    msg += "- Display Name ";
                    msg += "- Description ";
                    alertEmptyField(msg, true);

                    return;
                }

                columnsObj["Code"] = code;
                columnsObj["DisplayName"] = disName;
                columnsObj["Description"] = description;

            }

            //Aditional data
            if (methodPost === "AddEmvApplicationTbl") {
                var disName;
                var msg = "Debe llenar los campos";
                disName = $("#txtName").val();

                if (disName === "") {
                    msg += "- Display Name ";
                    alertEmptyField(msg, true);

                    return;
                }
                columnsObj["DisplayName"] = disName;

            }

            //Aditional data
            if (methodPost === "AddEmvKeyTbl") {
                var disName;
                var date;
                var msg = "Debe llenar los campos";

                disName = $("#txtName").val();
                date = $("#txtDate").val();
                if (disName === "" || date === "") {
                    msg += "- Display Name ";
                    msg += "- Revoke Date ";

                    alertEmptyField(msg, true);

                    return;
                }
                columnsObj["DisplayName"] = disName;
                columnsObj["Date"] = date;

            }
            //Aditional data
            if (methodPost === "addExtraApp") {
                var appId;
                var appName;
                var description;
                var inputMask;

                var msg = "Debe llenar todos los campos";

                appId = $("#ctl00_MainContent_txtId").val();
                appName = $("#ctl00_MainContent_txtName").val();
                description = $("#ctl00_MainContent_txtDescription").val();
                inputMask = $("#ctl00_MainContent_txtMask").val();


                if (appId === "" || appName === "" || description === "" || inputMask === "") {


                    alertEmptyField(msg, true);

                    return;
                }
                columnsObj["AppId"] = appId;
                columnsObj["AppName"] = appName;
                columnsObj["Desc"] = description;
                columnsObj["InputMask"] = inputMask;

            }
            //Aditional data
            if (methodPost === "addTerminalStis") {
                var tid;
                var ownerName;
                var merchantName;
                var shopName;
                var detailAddres;
                var shopContact;
                var shopTelNo;
                var officeContact;
                var officeTelNo;
                var status;
                var msg = "Debe llenar todos los campos";
                tid = $("input[name*='ctl00$MainContent$txtTid']").val();
                ownerName = $("input[name*='ctl00$MainContent$txtOwnerName']").val();
                merchantName = $("input[name*='ctl00$MainContent$txtMerchantName']").val();
                shopName = $("input[name*='ctl00$MainContent$txtShopName']").val();
                detailAddres = $("input[name*='ctl00$MainContent$txtDetailAddres']").val();
                shopContact = $("input[name*='ctl00$MainContent$txtShopContact']").val();
                shopTelNo = $("input[name*='ctl00$MainContent$txtShopTelNo']").val();
                officeContact = $("input[name*='ctl00$MainContent$txtOfficeContact']").val();
                officeTelNo = $("input[name*='ctl00$MainContent$txtOfficeTelNo']").val();
                status = document.getElementById('ctl00_MainContent_ddlOpc').options[document.getElementById('ctl00_MainContent_ddlOpc').selectedIndex].text;
                if (tid === "" || ownerName === "" || merchantName === "" || shopName === "" || detailAddres === "" || shopContact === "" || shopTelNo === "" || officeContact === "" || officeTelNo === "" || status === "") {


                    alertEmptyField(msg, true);

                    return;
                }
                columnsObj["Tid"] = tid;
                columnsObj["OwnerName"] = ownerName;
                columnsObj["MerchantName"] = merchantName;
                columnsObj["ShopName"] = shopName;
                columnsObj["DetailAddres"] = detailAddres;
                columnsObj["ShopContact"] = shopContact;
                columnsObj["ShopTelNo"] = shopTelNo;
                columnsObj["OfficeContact"] = officeContact;
                columnsObj["OfficeTelNo"] = officeTelNo;
                columnsObj["Status"] = status;

            }
            //Aditional data
            if (methodPost === "createNewAcquirer" || methodPost === "createNewIssuer" || methodPost === "createNewCardRange" || methodPost === "createNewEmvApplication" || methodPost === "createNewEmvKey" || methodPost === "createNewEmvExtraApplication") {
                var code;
                var disName;
                var recNo;
                var msg = "Debe llenar los campos";
                var cdo;
                code = $("input[name*='ctl00$MainContent$txtCode']").val();
                if (methodPost === "createNewEmvKey" || methodPost === "createNewEmvApplication") {
                    disName = $("input[name*='ctl00$MainContent$txtCode']").val();

                }
                else {
                    disName = document.getElementById('ctl00_MainContent_ddlOpc').options[document.getElementById('ctl00_MainContent_ddlOpc').selectedIndex].text;

                }
                recNo = document.getElementById('recNumber').innerText;

                if (disName === "" || date === "") {
                    msg += "- Display Name ";
                    msg += "- Revoke Date ";

                    alertEmptyField(msg, true);

                    return;
                }
                columnsObj["DisplayName"] = disName;
                columnsObj["Code"] = code;
                columnsObj["TerminalRecNo"] = recNo;
                var strHref = gup('cod', location);

                if (strHref.includes("ACQ") === true && strHref.includes("ISS") === false && strHref.includes("CDR") === false) {

                    columnsObj["ACQ"] = strHref.substring(3, 8);

                }
                if (strHref.includes("ACQ") === true && strHref.includes("ISS") === true) {
                    cdo = strHref.substring(strHref.indexOf("%5c%5c") + 6, strHref.indexOf("%5c%5c") + 22);
                    columnsObj["ISS"] = cdo.substring(0, 8).substring(3, 8);
                    columnsObj["ACQ"] = cdo.substring(11);
                }

            }
            return;
        },
        onClickSave: function (row, txtSelected) {

            if (methodPost === "editAcquirer" || methodPost === "addAcquirerTbl") {
                var count = 0;
                for (count = parseInt(row, 10); count < 100; count++) {
                    var $selTd = $(idTable).find("tr").eq(count).find("td").eq(1);
                    var textVal = $.trim($selTd.text());
                    var textValContent = "";
                    if (textVal === "Acquirer Name" || textVal === "Name_and_locations2" || textVal === "Name _and_locations1") {
                        $selTd = $(idTable).find("tr").eq(count).find("td").eq(2);
                        textValContent = $.trim($selTd.text());

                        if (textValContent.length > 23) {
                            alertEmptyField("La longitud maxima del campo \"" + textVal + "\" es de 23 caracteres", false);

                            $selTd.css("background", "Pink");
                            $selTd = $(idTable).find("tr").eq(row).find("td").eq(2);
                            $selTd.text("");
                            return;
                        }

                        else { $selTd.css("background", "White"); }

                    }

                    if (textVal === "Hora Cierre") {
                        $selTd = $(idTable).find("tr").eq(count).find("td").eq(2);
                        textValContent = $.trim($selTd.text());

                        if (textValContent.length > 5) {
                            alertEmptyField("La longitud maxima del campo \"" + textVal + "\" es de 5 caracteres formato(HH:MM)", false);

                            $selTd.css("background", "Pink");
                            $selTd = $(idTable).find("tr").eq(row).find("td").eq(2);
                            $selTd.text("");
                            return;
                        } else if (textValContent.includes(":")) {                                                                                
                            $selTd.text(textValContent.replace(":",""));                                             
                        } else { $selTd.css("background", "White"); }

                    }
                }
            }




            if (methodPost === "AddEmvApplicationTbl") {
                var count = 0;
                for (count = 0; count < 3; count++) {
                    var $selTd = $(idTable).find("tr").eq(count).find("td").eq(1);
                    var textVal = $.trim($selTd.text());
                    if (textVal === "AID") {
                        $selTd = $(idTable).find("tr").eq(count).find("td").eq(2);
                        textVal = $.trim($selTd.text());
                        if (textVal.length <= 0) {
                            alertEmptyField("Debe llenar el campo AID", false);
                            $selTd.css("background", "Pink");
                            $selTd = $(idTable).find("tr").eq(row).find("td").eq(2);
                            $selTd.text("");
                            return;
                        }
                        else { $selTd.css("background", "White"); }

                    }
                }
            }
            if (methodPost === "AddEmvKeyTbl") {
                var count = 0;
                for (count = 0; count < 3; count++) {
                    var $selTd = $(idTable).find("tr").eq(count).find("td").eq(1);
                    var textVal = $.trim($selTd.text());
                    if (textVal === "RID") {
                        $selTd = $(idTable).find("tr").eq(count).find("td").eq(2);
                        textVal = $.trim($selTd.text());
                        if (textVal.length <= 0) {
                            alertEmptyField("Debe llenar el campo RID", false);
                            $selTd.css("background", "Pink");
                            $selTd = $(idTable).find("tr").eq(row).find("td").eq(2);
                            $selTd.text("");
                            return;
                        }
                        else { $selTd.css("background", "White"); }

                    }
                }
            } var $sel = $(idTable).find("tr").eq(0).find("td").eq(i).find("span");

            if (!$sel.length) {
                $sel = $(idTable).find("tr").eq(1).find("td").eq(1);
            }
            var i = 0;





            do {
                var elementObj =
                {
                    name: "",
                    value: ""
                }
                    ;
                var $selTh = $(idTable).find("tr").eq(0).find("th").eq(i);
                if ($selTh.text().length <= 0)
                    break;
                elementObj.name = $.trim($selTh.text());
                var $selTd = $(idTable).find("tr").eq(row).find("td").eq(i);
                var textVal = $.trim($selTd.text());

                textVal = textVal.replace(txtSelected, "");

                columnsObj[$.trim($selTh.text())] = $.trim(textVal);

                i++;

            } while ($selTh.text().length > 0);




            var numColumnas = i - 1;
            json = JSON.stringify(columnsObj);
            json = json.replace(/"/g, '\\"');

            $.ajax({
                type: "POST",
                url: "wsadd.asmx/" + methodPost,
                data: '{"data":"' + json + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                success: function (r) {

                },
                error: function (r) {

                },
                failure: function (r) {

                }
            });
            return;
        },
        onClickConfirm: function () { //toDelete
            console.log("save");
            return;
        }


    });
    // para poner el AID en la primera Fila
    if (methodPost === "AddEmvApplicationTbl") {
        var count = 0;
        for (count = 0; count < 100; count++) {
            var $selTd = $(idTable).find("tr").eq(count).find("td").eq(1);
            var textVal = $.trim($selTd.text());
            if (textVal === "AID") {
                $selTd.parent().prependTo(idTable);
                $selTd.parent().css('background', 'white');
            }
        }
        for (count = 0; count < 3; count++) {
            var $selTd2 = $(idTable).find("tr").eq(count).find("th").eq(1);
            var textVal2 = $.trim($selTd2.text());
            if (textVal2 === "Parameter Name") {
                $selTd2.parent().prependTo(idTable);
            }
        }
    }
    // para poner el AID en la primera Fila
    if (methodPost === "AddEmvKeyTbl") {
        var count = 0;
        for (count = 0; count < 100; count++) {
            var $selTd = $(idTable).find("tr").eq(count).find("td").eq(1);
            var textVal = $.trim($selTd.text());
            if (textVal === "RID") {
                $selTd.parent().prependTo(idTable);
                $selTd.parent().css('background', 'white');
            }
        }
        for (count = 0; count < 3; count++) {
            var $selTd2 = $(idTable).find("tr").eq(count).find("th").eq(1);
            var textVal2 = $.trim($selTd2.text());
            if (textVal2 === "Parameter Name") {
                $selTd2.parent().prependTo(idTable);
            }
        }
    }






}





function alertEmptyField(msg, reload) {
    $.msgAlert({
        type: "error"
        , title: "Los campos son obligatorios"
        , text: msg
        , callback: function () {
            if (reload === true) {
                location.reload();

            }
        }
    });

}
$(document).ready(function () {


    function getRow(idTable) {
        $(idTable).find('tr').hover(function () {
            alert('You clicked row ' + ($(this).index() + 1));
            var $row = $(this).closest("tr"),       // Finds the closest row <tr> 
                $tds = $row.find("td");             // Finds all children <td> elements

            $.each($tds, function () {               // Visits every single <td> element
                console.log($(this).text());        // Prints out the text within the <td>
            });
        });
    }



    function delrow(rowindex) {

        var gridID = document.getElementById("<%=InjGrid.ClientID %>");

        gridID.deleteRow(rowindex + 1);

        document.getElementById("<%=txthidnoofrows.ClientID %>").value = gridID.rows.length - 1;

        return false;

    }


});

