﻿var morris1 = new Morris.Line({
    // ID of the element in which to draw the chart.
    element: 'myfirstchart',
    // Chart data records -- each entry in this array corresponds to a point on
    // the chart.
    data: [
        { year: '2008', value: 20, value2: 50 },
        { year: '2009', value: 10, value2: 80 },
        { year: '2010', value: 5, value2: 100 },
        { year: '2011', value: 5, value2: 50 },
        { year: '2012', value: 20, value2: 88 }
    ],
    // The name of the data record attribute that contains x-values.
    xkey: 'year',
    // A list of names of data record attributes that contain y-values.
    ykeys: ['value', 'value2'],
    lineWidth: 2.5,
    // Labels for the ykeys -- will be displayed when you hover over the
    // chart.
    labels: ['Value', 'value2'],
    resize: true,
    lineColors: ['#C14D9F', '#2CB4AC']
});

Morris.Area({
    element: 'graph',
    data: [
        { x: '2010 Q4', y: 3, z: 7 },
        { x: '2011 Q1', y: 3, z: 4 },
        { x: '2011 Q2', y: null, z: 1 },
        { x: '2011 Q3', y: 2, z: 5 },
        { x: '2011 Q4', y: 8, z: 2 },
        { x: '2012 Q1', y: 4, z: 4 }
    ],
    xkey: 'x',
    ykeys: ['y', 'z'],
    labels: ['Y', 'Z'],
    lineColors: ['#C14D9F', '#2CB4AC'],
    lineWidth: 2.5,
    resize: true
}).on('click', function (i, row) {
    console.log(i, row);
});
//jquery
$("#btnData").on("click", function () {
    var nuevaData = [
        { year: '2015', value: 88, value2: 53 },
        { year: '2020', value: 35, value2: 99 },
        { year: '2019', value: 15, value2: 10 },
        { year: '2018', value: 50, value2: 53 },
        { year: '2014', value: 20, value2: 18 }
    ];
    //llenado de data 

    morris1.setData(nuevaData);

});

Morris.Donut({
    element: 'graph1',

    data: [
        { value: 70, label: 'foo' },
        { value: 15, label: 'bar' },
        { value: 10, label: 'baz' },
        { value: 5, label: 'A really really long label' }
    ],
    colores: ["#C14D9F", "#f39c12", "#3c8dbc", "#dd4b39", "#555299"],
    formatter: function (x) { return x + "%" }
}).on('click', function (i, row) {
    console.log(i, row);

});


// Use Morris.Bar
Morris.Bar({
    element: 'graph2',
    data: [
        { x: '2011 Q1', y: 100 },
        { x: '2011 Q2', y: 90 },
        { x: '2011 Q3', y: 80 },
        { x: '2011 Q4', y: 70 },
        { x: '2012 Q1', y: 60 },
        { x: '2012 Q2', y: 50 },
        { x: '2012 Q3', y: 40 },
        { x: '2012 Q4', y: 30 },
        { x: '2013 Q1', y: 20 }
    ],
    xkey: 'x',
    ykeys: ['y'],
    labels: ['Y'],
    barColors: function (row, series, type) {
        if (type === 'bar') {
            var red = Math.ceil(255 * row.y / this.ymax);
            return 'rgb(' + red + ',0,0)';
        }
        else {
            return '#000';
        }
    }
});

