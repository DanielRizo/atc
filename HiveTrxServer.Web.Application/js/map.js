﻿$(document).ready(function () {

    var longitud = $("#ctl00_MainContent_longitud").text();
    var latitud = $("#ctl00_MainContent_latitud").text();
    var mcc = $("#ctl00_MainContent_MCC").text();
    var mnc = $("#ctl00_MainContent_MNC").text();
    var lca = $("#ctl00_MainContent_LCA").text();
    var cid = $("#ctl00_MainContent_CID").text();

    if (longitud == "0.0" && latitud == "0.0") {

        var JsonRsp;

        $.ajax({
            type: "POST",
            url: "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyA-3RIdtYYbldWG9M6X-RCQ8Gn1ZDIUhmA",
            data: JSON.stringify({ "cellTowers": [{ "cellId": cid, "locationAreaCode": lca, "mobileCountryCode": mcc, "mobileNetworkCode": mnc }] }),
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            success: function (r) {
                JsonRsp = JSON.parse(JSON.stringify(r));

                //console.log(r);

                latitud = JsonRsp.location.lat;
                longitud = JsonRsp.location.lng;

                //console.log(latitud, longitud);

                let myMap = L.map('myMap').setView([latitud, longitud], 13);

                L.tileLayer(`https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png`, {
                    maxZoom: 18
                }).addTo(myMap);

                let marker = L.marker([latitud, longitud]).addTo(myMap);



            },
            error: function (r) {
                $("#ctl00_MainContent_longitud").text('No se han obtenido datos de geolocalización.');
                $("#ctl00_MainContent_latitud").text('');
                $("#ctl00_MainContent_MCC").text('');
                $("#ctl00_MainContent_MNC").text('');
                $("#ctl00_MainContent_LCA").text('');
                $("#ctl00_MainContent_CID").text('');
            },
            failure: function (r) {

            }
        });
    } else {
        let myMap = L.map('myMap').setView([latitud, longitud], 13);

        L.tileLayer(`https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png`, {
            maxZoom: 18
        }).addTo(myMap);

        let marker = L.marker([latitud, longitud]).addTo(myMap);
    }



});
