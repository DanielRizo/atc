﻿$(document).ready(function () {
    $("#ctl00_TreeView1n0Nodes table ").contextMenu({ menu: 'myMenu' }, function (action, el, pos) { contextMenuWork(action, el, pos); });


        $("#btnActualizar").click(function () {
            __doPostBack('refreshTree');
        });

    
function contextMenuWork(action, el, pos) {
    var separator = "|";
    switch (action) {
        case "delete":
            {
                var strHtml = $(el).html();
                var n = strHtml.indexOf("<a href");
                var strHtml = strHtml.substring(n);
                var n = strHtml.indexOf("')");
                var n1 = strHtml.indexOf("s0\\\\0\\\\");
                if (n1 == -1) { return; }
                var strHref = strHtml.substring(n1 + 7, n);
                var array = strHref.split("\\\\");

                var acqAppKey = array[0];
                var iss = array[1];
                var cdr = array[2];
                var cnt = 0;

                msg = "";
                if (acqAppKey != null) {
                    msg += "--" + acqAppKey + "\n";
                    cnt++;
                }
                if (iss != null) {
                    msg += "----" + iss + "\n";
                    cnt++;
                }
                if (cdr != null) {
                    msg += "--------" + cdr;
                    cnt++;
                }
                if (cnt == 3)
                    msg = "Realmente desea eliminar Card Range? \n\n" + msg;
                if (cnt == 2)
                    msg = "Realmente desea eliminar Issuer? \n" + msg;
                if (cnt == 1 && acqAppKey.startsWith("ACQ"))
                    msg = "Realmente desea eliminar Acquirer? \n" + msg;
                else if (cnt == 1 && acqAppKey.startsWith("KEY"))
                    msg = "Realmente desea eliminar KEY? \n" + msg;
                else if (cnt == 1 && acqAppKey.startsWith("APP"))
                    msg = "Realmente desea eliminar Application? \n" + msg;
                else if (cnt == 1 && acqAppKey.startsWith("EXT"))
                    msg = "Realmente desea eliminar Extra Aplication? \n" + msg;

                $.msgAlert({
                    type: "warning"
                    , title: "Mensaje del Sistema"
                    , text: msg
                    , callback: function () {
                        if (cnt == 3)
                            __doPostBack('deleteCDR', acqAppKey + separator + iss + separator + cdr);
                        if (cnt == 2)
                            __doPostBack('deleteISS', acqAppKey + separator + iss + separator + cdr);
                        if (cnt == 1 && acqAppKey.startsWith("ACQ"))
                            __doPostBack('deleteACQ', acqAppKey + separator + iss + separator + cdr);
                        else if (cnt == 1 && acqAppKey.startsWith("KEY"))
                            __doPostBack('deleteKEY', acqAppKey + separator + iss + separator + cdr);
                        else if (cnt == 1 && acqAppKey.startsWith("APP"))
                            __doPostBack('deleteAPP', acqAppKey + separator + iss + separator + cdr);
                        else if (cnt == 1 && acqAppKey.startsWith("EXT"))
                            __doPostBack('deleteEXT', acqAppKey + separator + iss + separator + cdr);
                        }
                    });
                //if (confirm(msg)) {
                    
                //}
                break;
            }

        case "addAcquirer":
        case "addIssuer":
        case "addCardRange":
        case "addApp":
        case "addKey":
        case "addExt":
            {
                var strHtml = $(el).html();
                var n = strHtml.indexOf("<a href");
                var strHtml = strHtml.substring(n);
                var n = strHtml.indexOf("')");
                var n1 = strHtml.indexOf("s0\\\\0\\\\");
                var strHref;
                //if (n1 == -1) { }
                try {
                     strHref = strHtml.substring(n1 + 7, n);
                } catch (err) {
                    err.message;
                }
                if (strHref.includes("javascript")) {
                     strHtml = $(el).html();
                    n = strHtml.indexOf("<a href");
                    strHtml = strHtml.substring(n);
                    n = strHtml.indexOf("')");
                    n1 = strHtml.indexOf("s0\\\\");
                    strHref;
                    //if (n1 == -1) { }
                    try {
                        strHref = strHtml.substring(n1 + 9, n);
                    } catch (err) {
                        err.message;
                    }
                }
                var msg = "Agregar ";
                if (action === "addAcquirer") {
                 //   if (strHref.includes("ACQ") === true && strHref.includes("ISS") === false && strHref.includes("CDR") === false)
                    {
                        window.location.replace("/Group/CreateNewAcquirer.aspx?cod=");
                    }
                }
                if (action === "addIssuer") {
                    if (strHref.includes("ACQ") === true) {
                        window.location.replace("/Group/CreateNewIssuer.aspx?cod=" + strHref);
                    }
                }
                if (action === "addCardRange") {
                    if (strHref.includes("ACQ") === true && strHref.includes("ISS") === true ) {
                        window.location.replace("/Group/CreateNewCardRange.aspx?cod=" + strHref);
                    }
                }
                if (action === "addApp") {
                    //if (strHref.includes("APP", 0) === true)
                    {
                        window.location.replace("/Group/CreateNewEmvApplication.aspx?cod=" );
                    }
                }
                if (action === "addKey") {
                    //if (strHref.includes("KEY", 0) === true)
                    {
                        window.location.replace("/Group/CreateNewEmvKey.aspx?cod=" );
                    }
                }
                if (action === "addExt") {
                    //if (strHref.includes("KEY", 0) === true)
                    {
                        window.location.replace("/Group/CreateNewEmvExtraApplication.aspx?cod=" );
                    }
                }
                break;
            }
    }
}
});