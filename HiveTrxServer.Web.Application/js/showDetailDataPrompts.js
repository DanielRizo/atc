﻿
const listRight = document.getElementById('multiselect_right');
const listLeft = document.getElementById('multiselect_left');

function getInfoById(id, tableType, callback, method) {
    console.log("id: " + id);
    $.ajax({
        type: "POST",
        url: "wsadd.asmx/" + method,
        data: '{"data":"' + id + '","tableType":"' + tableType + '"}',
        contentType: "application/Json; charset=utf-8",
        dataType: 'Json',
        success: function (r) {

            var JsonRsp;
            try {
                JsonRsp = JSON.parse(r.d);
                callback(JsonRsp);
            }
            catch (e) {
                callback(null);

            }

        },
        error: function (r) {

        },
        failure: function (r) {

        }
    });
}

function packDeatailToShow(json, tableType) {
    if (json) {
        if (tableType === "PROMPTS") {

            let prompt = "<b>Prompt:</b> " + json["CODIGO PROMPTS"];
            let codigo = "<b>Codigo:</b> " + json["IdReal"];
            let tipoDato = "<b>Tipo de dato:</b> " + json["TIPO DATO"];
            let lonMax = "<b>Longitud maxima:</b> " + json["LONGITUD MAXIMA"];
            let lonMin = "<b>Longitud Minima:</b> " + json["LONGITUD MINIMA"];
            let imprimirPrompt = "<b>Imprimir prompt:</b> " + json["IMPRIMIR PROMPT"];
            return `${codigo}</br>${tipoDato}</br>${lonMax}</br>${lonMin}</br>${imprimirPrompt}`;
        }
        if (tableType === "PAGOS_ELEC") {
            let codigoElec = "<b>Codigo:</b> " + json["IdReal"];
            let lonMaxElec = "<b>Longitud maxima:</b> " + json["LONGITUD MAXIMA"];
            let lonMinElec = "<b>Longitud Minima:</b> " + json["LONGITUD MINIMA"];
            let tiempEsperaElec = "<b>Tiempo espera:</b> " + json["TIEMPO ESPERA"];
            return `${codigoElec}</br>${lonMaxElec}</br>${lonMinElec}</br>${tiempEsperaElec}`;
        }
        if (tableType === "PAGOS_VAR") {
            let codigoVar = "<b>Codigo:</b> " + json["IdReal"];
            let entradaManual = "<b>Entrada manual codigo:</b> " + json["ENTRADA MANUAL CODIGO"];
            let grupoPrompt = "<b>Grupo prompts:</b> " + json["GRUPO PROMPTS"];
            return `${codigoVar}</br>${entradaManual}</br>${grupoPrompt}</br>`;
        }
    }
    return ``;

}
function promptsAvailable(event) {
    let optionSel = event.target.value;
    let uri = event.target.baseURI;
    let tableType;

    if (uri.includes("EditarGroupPrompts") || uri.includes("CreateGroupPrompts")) {
        tableType = "PROMPTS";
    }
    if (uri.includes("EditarGroupVariousPayments") || uri.includes("CreateGroupVariousPayments")) {
        tableType = "PAGOS_VAR";

    }
    if (uri.includes("EditarGroupElectronicsPayments") || uri.includes("CreateGroupElectronicsPayments")) {
        tableType = "PAGOS_ELEC";

    }
    if (optionSel) {
        getInfoById(optionSel, tableType, function (json) {
            let datailPrompt = document.getElementById("detailPromptLeft");

            datailPrompt.innerHTML = packDeatailToShow(json, tableType);
        }, "getInfoById");
    }

}
function promptsAssigned(event) {
    let optionSel = event.target.value;
    let uri = event.target.baseURI;
    let tableType;

    if (uri.includes("EditarGroupPrompts") || uri.includes("CreateGroupPrompts")) {
        tableType = "PROMPTS";
    }
    if (uri.includes("EditarGroupVariousPayments") || uri.includes("CreateGroupVariousPayments")) {
        tableType = "PAGOS_VAR";

    }
    if (uri.includes("EditarGroupElectronicsPayments") || uri.includes("CreateGroupElectronicsPayments")) {
        tableType = "PAGOS_ELEC";

    }
    if (optionSel) {
        getInfoById(optionSel, tableType, function (json) {
            let datailPrompt = document.getElementById("detailPromptRight");
            datailPrompt.innerHTML = packDeatailToShow(json, tableType);
        }, "getInfoById");
    }
}

listRight.addEventListener('click', promptsAssigned);
listLeft.addEventListener('click', promptsAvailable);
listRight.addEventListener('keyup', promptsAssigned);
listLeft.addEventListener('keyup', promptsAvailable);

